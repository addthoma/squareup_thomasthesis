.class public final Lcom/squareup/comms/protos/seller/CartShowCartBanner;
.super Lcom/squareup/wire/AndroidMessage;
.source "CartShowCartBanner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;,
        Lcom/squareup/comms/protos/seller/CartShowCartBanner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/seller/CartShowCartBanner;",
        "Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCartShowCartBanner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CartShowCartBanner.kt\ncom/squareup/comms/protos/seller/CartShowCartBanner\n*L\n1#1,238:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u00172\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0016\u0017B;\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0004\u0012\u0006\u0010\u0008\u001a\u00020\u0004\u0012\u0006\u0010\t\u001a\u00020\u0004\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJD\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u00042\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00042\u0008\u0008\u0002\u0010\t\u001a\u00020\u00042\u0008\u0008\u0002\u0010\n\u001a\u00020\u000bJ\u0013\u0010\u000e\u001a\u00020\u00042\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0096\u0002J\u0008\u0010\u0011\u001a\u00020\u0012H\u0016J\u0008\u0010\u0013\u001a\u00020\u0002H\u0016J\u0008\u0010\u0014\u001a\u00020\u0015H\u0016R\u0010\u0010\u0007\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0008\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/CartShowCartBanner;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;",
        "isPreviousCardInserted",
        "",
        "swipedCard",
        "Lcom/squareup/comms/protos/common/Card;",
        "isChipCardInserted",
        "isSwipeFailure",
        "isChipCardSwiped",
        "unknownFields",
        "Lokio/ByteString;",
        "(ZLcom/squareup/comms/protos/common/Card;ZZZLokio/ByteString;)V",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "newBuilder",
        "toString",
        "",
        "Builder",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/seller/CartShowCartBanner;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/seller/CartShowCartBanner;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/seller/CartShowCartBanner$Companion;


# instance fields
.field public final isChipCardInserted:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation
.end field

.field public final isChipCardSwiped:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation
.end field

.field public final isPreviousCardInserted:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation
.end field

.field public final isSwipeFailure:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation
.end field

.field public final swipedCard:Lcom/squareup/comms/protos/common/Card;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.common.Card#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->Companion:Lcom/squareup/comms/protos/seller/CartShowCartBanner$Companion;

    .line 177
    new-instance v0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Companion$ADAPTER$1;

    .line 178
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 179
    const-class v2, Lcom/squareup/comms/protos/seller/CartShowCartBanner;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 235
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ZLcom/squareup/comms/protos/common/Card;ZZZLokio/ByteString;)V
    .locals 1

    const-string/jumbo v0, "unknownFields"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    sget-object v0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    iput-boolean p1, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isPreviousCardInserted:Z

    iput-object p2, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->swipedCard:Lcom/squareup/comms/protos/common/Card;

    iput-boolean p3, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isChipCardInserted:Z

    iput-boolean p4, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isSwipeFailure:Z

    iput-boolean p5, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isChipCardSwiped:Z

    return-void
.end method

.method public synthetic constructor <init>(ZLcom/squareup/comms/protos/common/Card;ZZZLokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_0

    const/4 p2, 0x0

    .line 40
    check-cast p2, Lcom/squareup/comms/protos/common/Card;

    :cond_0
    move-object v2, p2

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_1

    .line 62
    sget-object p6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    :cond_1
    move-object v6, p6

    move-object v0, p0

    move v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/comms/protos/seller/CartShowCartBanner;-><init>(ZLcom/squareup/comms/protos/common/Card;ZZZLokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/seller/CartShowCartBanner;ZLcom/squareup/comms/protos/common/Card;ZZZLokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/seller/CartShowCartBanner;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    .line 111
    iget-boolean p1, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isPreviousCardInserted:Z

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    .line 112
    iget-object p2, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->swipedCard:Lcom/squareup/comms/protos/common/Card;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    .line 113
    iget-boolean p3, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isChipCardInserted:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    .line 114
    iget-boolean p4, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isSwipeFailure:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    .line 115
    iget-boolean p5, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isChipCardSwiped:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    .line 116
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->unknownFields()Lokio/ByteString;

    move-result-object p6

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move p3, p1

    move-object p4, p8

    move p5, v0

    move p6, v1

    move p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->copy(ZLcom/squareup/comms/protos/common/Card;ZZZLokio/ByteString;)Lcom/squareup/comms/protos/seller/CartShowCartBanner;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(ZLcom/squareup/comms/protos/common/Card;ZZZLokio/ByteString;)Lcom/squareup/comms/protos/seller/CartShowCartBanner;
    .locals 8

    const-string/jumbo v0, "unknownFields"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    new-instance v0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;

    move-object v1, v0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/comms/protos/seller/CartShowCartBanner;-><init>(ZLcom/squareup/comms/protos/common/Card;ZZZLokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 76
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 77
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/seller/CartShowCartBanner;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 83
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/seller/CartShowCartBanner;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isPreviousCardInserted:Z

    iget-boolean v3, p1, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isPreviousCardInserted:Z

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->swipedCard:Lcom/squareup/comms/protos/common/Card;

    iget-object v3, p1, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->swipedCard:Lcom/squareup/comms/protos/common/Card;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isChipCardInserted:Z

    iget-boolean v3, p1, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isChipCardInserted:Z

    if-ne v0, v3, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isSwipeFailure:Z

    iget-boolean v3, p1, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isSwipeFailure:Z

    if-ne v0, v3, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isChipCardSwiped:Z

    iget-boolean p1, p1, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isChipCardSwiped:Z

    if-ne v0, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 2

    .line 87
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 90
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isPreviousCardInserted:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 91
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->swipedCard:Lcom/squareup/comms/protos/common/Card;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 92
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isChipCardInserted:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 93
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isSwipeFailure:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 94
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isChipCardSwiped:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;
    .locals 2

    .line 65
    new-instance v0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;-><init>()V

    .line 66
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isPreviousCardInserted:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->isPreviousCardInserted:Ljava/lang/Boolean;

    .line 67
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->swipedCard:Lcom/squareup/comms/protos/common/Card;

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->swipedCard:Lcom/squareup/comms/protos/common/Card;

    .line 68
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isChipCardInserted:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->isChipCardInserted:Ljava/lang/Boolean;

    .line 69
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isSwipeFailure:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->isSwipeFailure:Ljava/lang/Boolean;

    .line 70
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isChipCardSwiped:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->isChipCardSwiped:Ljava/lang/Boolean;

    .line 71
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->newBuilder()Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 102
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isPreviousCardInserted="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isPreviousCardInserted:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 103
    iget-object v2, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->swipedCard:Lcom/squareup/comms/protos/common/Card;

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "swipedCard="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->swipedCard:Lcom/squareup/comms/protos/common/Card;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 104
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isChipCardInserted="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isChipCardInserted:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 105
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isSwipeFailure="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isSwipeFailure:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 106
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isChipCardSwiped="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner;->isChipCardSwiped:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 107
    move-object v3, v0

    check-cast v3, Ljava/lang/Iterable;

    const-string v0, "CartShowCartBanner{"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const-string v0, ", "

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string/jumbo v0, "}"

    move-object v6, v0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
