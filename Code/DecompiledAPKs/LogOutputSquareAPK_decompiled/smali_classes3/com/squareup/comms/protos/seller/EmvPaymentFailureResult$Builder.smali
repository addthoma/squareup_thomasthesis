.class public final Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EmvPaymentFailureResult.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult;",
        "Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0008\u0010\t\u001a\u00020\u0002H\u0016J\u0010\u0010\u0006\u001a\u00020\u00002\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u0008\u001a\u00020\u00002\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0007R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult;",
        "()V",
        "arpc",
        "Lokio/ByteString;",
        "decline_subtitle",
        "",
        "decline_title",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public arpc:Lokio/ByteString;

.field public decline_subtitle:Ljava/lang/String;

.field public decline_title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public final arpc(Lokio/ByteString;)Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult$Builder;->arpc:Lokio/ByteString;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult;
    .locals 5

    .line 131
    new-instance v0, Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult;

    .line 132
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult$Builder;->arpc:Lokio/ByteString;

    .line 133
    iget-object v2, p0, Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult$Builder;->decline_title:Ljava/lang/String;

    .line 134
    iget-object v3, p0, Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult$Builder;->decline_subtitle:Ljava/lang/String;

    .line 135
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    .line 131
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult;-><init>(Lokio/ByteString;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult$Builder;->build()Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final decline_subtitle(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult$Builder;->decline_subtitle:Ljava/lang/String;

    return-object p0
.end method

.method public final decline_title(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/EmvPaymentFailureResult$Builder;->decline_title:Ljava/lang/String;

    return-object p0
.end method
