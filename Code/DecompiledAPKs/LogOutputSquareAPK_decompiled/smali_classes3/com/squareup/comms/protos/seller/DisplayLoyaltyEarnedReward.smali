.class public final Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;
.super Lcom/squareup/wire/AndroidMessage;
.source "DisplayLoyaltyEarnedReward.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;,
        Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisplayLoyaltyEarnedReward.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisplayLoyaltyEarnedReward.kt\ncom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward\n*L\n1#1,450:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0000\n\u0002\u0008\u0006\u0018\u0000 !2\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0002 !B{\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0004\u0012\u0006\u0010\t\u001a\u00020\u0004\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\r\u0012\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u000b\u0012\n\u0008\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0004\u0012\n\u0008\u0002\u0010\u0011\u001a\u0004\u0018\u00010\r\u0012\u0006\u0010\u0012\u001a\u00020\r\u0012\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0015J\u0093\u0001\u0010\u0018\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00042\u0008\u0008\u0002\u0010\t\u001a\u00020\u00042\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\r2\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u000b2\n\u0008\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00042\n\u0008\u0002\u0010\u0011\u001a\u0004\u0018\u00010\r2\u0008\u0008\u0002\u0010\u0012\u001a\u00020\r2\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0019J\u0013\u0010\u001a\u001a\u00020\r2\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0096\u0002J\u0008\u0010\u001d\u001a\u00020\u0004H\u0016J\u0008\u0010\u001e\u001a\u00020\u0002H\u0016J\u0008\u0010\u001f\u001a\u00020\u000bH\u0016R\u0010\u0010\u0012\u001a\u00020\r8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\r8\u0006X\u0087\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0016R\u0010\u0010\u000c\u001a\u00020\r8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u00020\r8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0008\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u000b8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00048\u0006X\u0087\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0017R\u0010\u0010\t\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;",
        "new_points",
        "",
        "total_points",
        "points_terminology",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;",
        "new_eligible_reward_tiers",
        "total_eligible_reward_tiers",
        "unit_name",
        "",
        "is_enrolled",
        "",
        "is_newly_enrolled",
        "phone_token",
        "timeout_duration_millis",
        "is_cash_integration_enabled",
        "dark_theme",
        "unknownFields",
        "Lokio/ByteString;",
        "(IILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;IILjava/lang/String;ZZLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;ZLokio/ByteString;)V",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Integer;",
        "copy",
        "(IILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;IILjava/lang/String;ZZLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;ZLokio/ByteString;)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;",
        "equals",
        "other",
        "",
        "hashCode",
        "newBuilder",
        "toString",
        "Builder",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Companion;


# instance fields
.field public final dark_theme:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0xd
    .end annotation
.end field

.field public final is_cash_integration_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final is_enrolled:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation
.end field

.field public final is_newly_enrolled:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x9
    .end annotation
.end field

.field public final new_eligible_reward_tiers:I
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation
.end field

.field public final new_points:I
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation
.end field

.field public final phone_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.seller.DisplayLoyaltyEarnRewards$PointsTerminology#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation
.end field

.field public final timeout_duration_millis:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xb
    .end annotation
.end field

.field public final total_eligible_reward_tiers:I
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation
.end field

.field public final total_points:I
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation
.end field

.field public final unit_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->Companion:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Companion;

    .line 348
    new-instance v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Companion$ADAPTER$1;

    .line 350
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 351
    const-class v2, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 447
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;IILjava/lang/String;ZZLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;ZLokio/ByteString;)V
    .locals 1

    const-string v0, "points_terminology"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unit_name"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unknownFields"

    invoke-static {p13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    sget-object v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p13}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    iput p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->new_points:I

    iput p2, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->total_points:I

    iput-object p3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    iput p4, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->new_eligible_reward_tiers:I

    iput p5, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->total_eligible_reward_tiers:I

    iput-object p6, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->unit_name:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_enrolled:Z

    iput-boolean p8, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_newly_enrolled:Z

    iput-object p9, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->phone_token:Ljava/lang/String;

    iput-object p10, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->timeout_duration_millis:Ljava/lang/Integer;

    iput-object p11, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_cash_integration_enabled:Ljava/lang/Boolean;

    iput-boolean p12, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->dark_theme:Z

    return-void
.end method

.method public synthetic constructor <init>(IILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;IILjava/lang/String;ZZLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;ZLokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 17

    move/from16 v0, p14

    and-int/lit16 v1, v0, 0x100

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 93
    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    move-object v12, v1

    goto :goto_0

    :cond_0
    move-object/from16 v12, p9

    :goto_0
    and-int/lit16 v1, v0, 0x200

    if-eqz v1, :cond_1

    .line 102
    move-object v1, v2

    check-cast v1, Ljava/lang/Integer;

    move-object v13, v1

    goto :goto_1

    :cond_1
    move-object/from16 v13, p10

    :goto_1
    and-int/lit16 v1, v0, 0x400

    if-eqz v1, :cond_2

    .line 111
    move-object v1, v2

    check-cast v1, Ljava/lang/Boolean;

    move-object v14, v1

    goto :goto_2

    :cond_2
    move-object/from16 v14, p11

    :goto_2
    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_3

    .line 119
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object/from16 v16, v0

    goto :goto_3

    :cond_3
    move-object/from16 v16, p13

    :goto_3
    move-object/from16 v3, p0

    move/from16 v4, p1

    move/from16 v5, p2

    move-object/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move-object/from16 v9, p6

    move/from16 v10, p7

    move/from16 v11, p8

    move/from16 v15, p12

    invoke-direct/range {v3 .. v16}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;-><init>(IILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;IILjava/lang/String;ZZLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;ZLokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;IILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;IILjava/lang/String;ZZLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;ZLokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;
    .locals 14

    move-object v0, p0

    move/from16 v1, p14

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    .line 200
    iget v2, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->new_points:I

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    .line 201
    iget v3, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->total_points:I

    goto :goto_1

    :cond_1
    move/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    .line 202
    iget-object v4, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    goto :goto_2

    :cond_2
    move-object/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    .line 203
    iget v5, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->new_eligible_reward_tiers:I

    goto :goto_3

    :cond_3
    move/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    .line 204
    iget v6, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->total_eligible_reward_tiers:I

    goto :goto_4

    :cond_4
    move/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    .line 205
    iget-object v7, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->unit_name:Ljava/lang/String;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    .line 206
    iget-boolean v8, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_enrolled:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    .line 207
    iget-boolean v9, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_newly_enrolled:Z

    goto :goto_7

    :cond_7
    move/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    .line 208
    iget-object v10, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->phone_token:Ljava/lang/String;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    .line 209
    iget-object v11, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->timeout_duration_millis:Ljava/lang/Integer;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    .line 210
    iget-object v12, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_cash_integration_enabled:Ljava/lang/Boolean;

    goto :goto_a

    :cond_a
    move-object/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v1, 0x800

    if-eqz v13, :cond_b

    .line 211
    iget-boolean v13, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->dark_theme:Z

    goto :goto_b

    :cond_b
    move/from16 v13, p12

    :goto_b
    and-int/lit16 v1, v1, 0x1000

    if-eqz v1, :cond_c

    .line 212
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->unknownFields()Lokio/ByteString;

    move-result-object v1

    goto :goto_c

    :cond_c
    move-object/from16 v1, p13

    :goto_c
    move p1, v2

    move/from16 p2, v3

    move-object/from16 p3, v4

    move/from16 p4, v5

    move/from16 p5, v6

    move-object/from16 p6, v7

    move/from16 p7, v8

    move/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v11

    move-object/from16 p11, v12

    move/from16 p12, v13

    move-object/from16 p13, v1

    invoke-virtual/range {p0 .. p13}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->copy(IILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;IILjava/lang/String;ZZLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;ZLokio/ByteString;)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final copy(IILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;IILjava/lang/String;ZZLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;ZLokio/ByteString;)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;
    .locals 15

    const-string v0, "points_terminology"

    move-object/from16 v4, p3

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unit_name"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unknownFields"

    move-object/from16 v14, p13

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    new-instance v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;

    move-object v1, v0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move/from16 v13, p12

    invoke-direct/range {v1 .. v14}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;-><init>(IILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;IILjava/lang/String;ZZLjava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;ZLokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 141
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 142
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 155
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->new_points:I

    iget v3, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->new_points:I

    if-ne v0, v3, :cond_2

    iget v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->total_points:I

    iget v3, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->total_points:I

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    iget-object v3, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->new_eligible_reward_tiers:I

    iget v3, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->new_eligible_reward_tiers:I

    if-ne v0, v3, :cond_2

    iget v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->total_eligible_reward_tiers:I

    iget v3, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->total_eligible_reward_tiers:I

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->unit_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->unit_name:Ljava/lang/String;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_enrolled:Z

    iget-boolean v3, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_enrolled:Z

    if-ne v0, v3, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_newly_enrolled:Z

    iget-boolean v3, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_newly_enrolled:Z

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->phone_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->phone_token:Ljava/lang/String;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->timeout_duration_millis:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->timeout_duration_millis:Ljava/lang/Integer;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_cash_integration_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_cash_integration_enabled:Ljava/lang/Boolean;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->dark_theme:Z

    iget-boolean p1, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->dark_theme:Z

    if-ne v0, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 159
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_3

    .line 161
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 162
    iget v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->new_points:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 163
    iget v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->total_points:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 164
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    invoke-virtual {v1}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 165
    iget v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->new_eligible_reward_tiers:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 166
    iget v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->total_eligible_reward_tiers:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 167
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->unit_name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 168
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_enrolled:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 169
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_newly_enrolled:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 170
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->phone_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->timeout_duration_millis:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_cash_integration_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->dark_theme:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;
    .locals 2

    .line 123
    new-instance v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;-><init>()V

    .line 124
    iget v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->new_points:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->new_points:Ljava/lang/Integer;

    .line 125
    iget v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->total_points:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->total_points:Ljava/lang/Integer;

    .line 126
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    .line 127
    iget v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->new_eligible_reward_tiers:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->new_eligible_reward_tiers:Ljava/lang/Integer;

    .line 128
    iget v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->total_eligible_reward_tiers:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->total_eligible_reward_tiers:Ljava/lang/Integer;

    .line 129
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->unit_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->unit_name:Ljava/lang/String;

    .line 130
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_enrolled:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->is_enrolled:Ljava/lang/Boolean;

    .line 131
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_newly_enrolled:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->is_newly_enrolled:Ljava/lang/Boolean;

    .line 132
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->phone_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->phone_token:Ljava/lang/String;

    .line 133
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->timeout_duration_millis:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->timeout_duration_millis:Ljava/lang/Integer;

    .line 134
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_cash_integration_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->is_cash_integration_enabled:Ljava/lang/Boolean;

    .line 135
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->dark_theme:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->dark_theme:Ljava/lang/Boolean;

    .line 136
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->newBuilder()Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .line 180
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 181
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "new_points="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->new_points:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 182
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "total_points="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->total_points:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 183
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "points_terminology="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 184
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "new_eligible_reward_tiers="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->new_eligible_reward_tiers:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 185
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "total_eligible_reward_tiers="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->total_eligible_reward_tiers:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 186
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unit_name="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->unit_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 187
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "is_enrolled="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_enrolled:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 188
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "is_newly_enrolled="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_newly_enrolled:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 189
    iget-object v2, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->phone_token:Ljava/lang/String;

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "phone_token="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->phone_token:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 190
    :cond_0
    iget-object v2, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->timeout_duration_millis:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 191
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "timeout_duration_millis="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->timeout_duration_millis:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 190
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 192
    :cond_1
    iget-object v2, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_cash_integration_enabled:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 193
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "is_cash_integration_enabled="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->is_cash_integration_enabled:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 192
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 194
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dark_theme="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnedReward;->dark_theme:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 195
    move-object v3, v0

    check-cast v3, Ljava/lang/Iterable;

    const-string v0, "DisplayLoyaltyEarnedReward{"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const-string v0, ", "

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string/jumbo v0, "}"

    .line 196
    move-object v6, v0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    .line 195
    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
