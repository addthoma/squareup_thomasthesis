.class public final Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DisplayLoyaltyEarnRewards.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0011\u001a\u00020\u0002H\u0016J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u000e\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\u0005J\u000e\u0010\u0008\u001a\u00020\u00002\u0006\u0010\u0008\u001a\u00020\u0005J\u000e\u0010\t\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u000c\u001a\u00020\u00002\u0006\u0010\u000c\u001a\u00020\rJ\u000e\u0010\u000e\u001a\u00020\u00002\u0006\u0010\u000e\u001a\u00020\u000fJ\u000e\u0010\u0010\u001a\u00020\u00002\u0006\u0010\u0010\u001a\u00020\u000fR\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0008\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\t\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000bR\u0014\u0010\u000c\u001a\u0004\u0018\u00010\r8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;",
        "()V",
        "dark_theme",
        "",
        "Ljava/lang/Boolean;",
        "is_enrolled",
        "is_newly_enrolled",
        "points_earned",
        "",
        "Ljava/lang/Integer;",
        "points_terminology",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;",
        "qualifying_purchase_description",
        "",
        "unit_name",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public dark_theme:Ljava/lang/Boolean;

.field public is_enrolled:Ljava/lang/Boolean;

.field public is_newly_enrolled:Ljava/lang/Boolean;

.field public points_earned:Ljava/lang/Integer;

.field public points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

.field public qualifying_purchase_description:Ljava/lang/String;

.field public unit_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 160
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;
    .locals 13

    .line 230
    new-instance v9, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;

    .line 231
    iget-object v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->points_earned:Ljava/lang/Integer;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 232
    iget-object v5, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    if-eqz v5, :cond_5

    .line 234
    iget-object v6, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->qualifying_purchase_description:Ljava/lang/String;

    if-eqz v6, :cond_4

    .line 236
    iget-object v7, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->unit_name:Ljava/lang/String;

    if-eqz v7, :cond_3

    .line 237
    iget-object v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->is_enrolled:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 238
    iget-object v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->is_newly_enrolled:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    .line 240
    iget-object v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->dark_theme:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    .line 241
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v9

    move v1, v4

    move-object v2, v5

    move-object v3, v6

    move-object v4, v7

    move v5, v8

    move v6, v10

    move v7, v11

    move-object v8, v12

    .line 230
    invoke-direct/range {v0 .. v8}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;-><init>(ILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;Ljava/lang/String;Ljava/lang/String;ZZZLokio/ByteString;)V

    return-object v9

    :cond_0
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "dark_theme"

    aput-object v0, v3, v1

    .line 240
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "is_newly_enrolled"

    aput-object v0, v3, v1

    .line 238
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_2
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "is_enrolled"

    aput-object v0, v3, v1

    .line 237
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_3
    new-array v0, v3, [Ljava/lang/Object;

    aput-object v7, v0, v2

    const-string/jumbo v2, "unit_name"

    aput-object v2, v0, v1

    .line 236
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_4
    new-array v0, v3, [Ljava/lang/Object;

    aput-object v6, v0, v2

    const-string v2, "qualifying_purchase_description"

    aput-object v2, v0, v1

    .line 235
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_5
    new-array v0, v3, [Ljava/lang/Object;

    aput-object v5, v0, v2

    const-string v2, "points_terminology"

    aput-object v2, v0, v1

    .line 232
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_6
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "points_earned"

    aput-object v0, v3, v1

    .line 231
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 160
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final dark_theme(Z)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;
    .locals 0

    .line 226
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->dark_theme:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final is_enrolled(Z)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;
    .locals 0

    .line 216
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->is_enrolled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final is_newly_enrolled(Z)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;
    .locals 0

    .line 221
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->is_newly_enrolled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final points_earned(I)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;
    .locals 0

    .line 187
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->points_earned:Ljava/lang/Integer;

    return-object p0
.end method

.method public final points_terminology(Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;
    .locals 1

    const-string v0, "points_terminology"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 195
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    return-object p0
.end method

.method public final qualifying_purchase_description(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;
    .locals 1

    const-string v0, "qualifying_purchase_description"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->qualifying_purchase_description:Ljava/lang/String;

    return-object p0
.end method

.method public final unit_name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;
    .locals 1

    const-string/jumbo v0, "unit_name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->unit_name:Ljava/lang/String;

    return-object p0
.end method
