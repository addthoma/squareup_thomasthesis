.class public final Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DisplayLoyaltySignup.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup;",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u000b\u001a\u00020\u0002H\u0016J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u0006\u001a\u00020\u00002\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0005J\u0015\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0002\u0010\u000cJ\u000e\u0010\n\u001a\u00020\u00002\u0006\u0010\n\u001a\u00020\u0005R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0014\u0010\n\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup;",
        "()V",
        "default_phone_id",
        "",
        "default_phone_number",
        "default_phone_source",
        "",
        "Ljava/lang/Integer;",
        "legal_text",
        "build",
        "(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public default_phone_id:Ljava/lang/String;

.field public default_phone_number:Ljava/lang/String;

.field public default_phone_source:Ljava/lang/Integer;

.field public legal_text:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 115
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup;
    .locals 7

    .line 158
    new-instance v6, Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup;

    .line 159
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;->default_phone_number:Ljava/lang/String;

    .line 160
    iget-object v2, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;->default_phone_id:Ljava/lang/String;

    .line 161
    iget-object v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;->default_phone_source:Ljava/lang/Integer;

    .line 162
    iget-object v4, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;->legal_text:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 163
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    .line 158
    invoke-direct/range {v0 .. v5}, Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v4, v0, v1

    const/4 v1, 0x1

    const-string v2, "legal_text"

    aput-object v2, v0, v1

    .line 162
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 115
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final default_phone_id(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;->default_phone_id:Ljava/lang/String;

    return-object p0
.end method

.method public final default_phone_number(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;->default_phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public final default_phone_source(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;->default_phone_source:Ljava/lang/Integer;

    return-object p0
.end method

.method public final legal_text(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;
    .locals 1

    const-string v0, "legal_text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltySignup$Builder;->legal_text:Ljava/lang/String;

    return-object p0
.end method
