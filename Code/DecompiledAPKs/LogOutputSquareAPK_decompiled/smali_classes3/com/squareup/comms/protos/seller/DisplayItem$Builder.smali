.class public final Lcom/squareup/comms/protos/seller/DisplayItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DisplayItem.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/DisplayItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/DisplayItem;",
        "Lcom/squareup/comms/protos/seller/DisplayItem$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0016\u001a\u00020\u0002H\u0016J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u0006\u001a\u00020\u00002\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0005J\u0014\u0010\u0007\u001a\u00020\u00002\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008J\u0015\u0010\n\u001a\u00020\u00002\u0008\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u0017J\u0015\u0010\r\u001a\u00020\u00002\u0008\u0010\r\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u0017J\u0010\u0010\u000e\u001a\u00020\u00002\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u000f\u001a\u00020\u00002\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u0010\u001a\u00020\u00002\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0005J\u0015\u0010\u0011\u001a\u00020\u00002\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u00a2\u0006\u0002\u0010\u0018J\u0014\u0010\u0014\u001a\u00020\u00002\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0008R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000cR\u0016\u0010\r\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000cR\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0011\u001a\u0004\u0018\u00010\u00128\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0013R\u0018\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/DisplayItem$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/DisplayItem;",
        "()V",
        "client_id",
        "",
        "description",
        "discounts",
        "",
        "Lcom/squareup/comms/protos/seller/Discount;",
        "is_discount_row",
        "",
        "Ljava/lang/Boolean;",
        "is_section_header_row",
        "name",
        "per_unit_price_and_quantity",
        "price",
        "quantity",
        "",
        "Ljava/lang/Integer;",
        "taxes",
        "Lcom/squareup/comms/protos/seller/Tax;",
        "build",
        "(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;",
        "(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public client_id:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public discounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/Discount;",
            ">;"
        }
    .end annotation
.end field

.field public is_discount_row:Ljava/lang/Boolean;

.field public is_section_header_row:Ljava/lang/Boolean;

.field public name:Ljava/lang/String;

.field public per_unit_price_and_quantity:Ljava/lang/String;

.field public price:Ljava/lang/String;

.field public quantity:Ljava/lang/Integer;

.field public taxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/Tax;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 183
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 197
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->taxes:Ljava/util/List;

    .line 209
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->discounts:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/seller/DisplayItem;
    .locals 13

    .line 278
    new-instance v12, Lcom/squareup/comms/protos/seller/DisplayItem;

    .line 279
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->name:Ljava/lang/String;

    .line 280
    iget-object v2, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->price:Ljava/lang/String;

    .line 281
    iget-object v3, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->description:Ljava/lang/String;

    .line 282
    iget-object v4, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->client_id:Ljava/lang/String;

    .line 283
    iget-object v5, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->taxes:Ljava/util/List;

    .line 284
    iget-object v6, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_discount_row:Ljava/lang/Boolean;

    .line 285
    iget-object v7, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->quantity:Ljava/lang/Integer;

    .line 286
    iget-object v8, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->per_unit_price_and_quantity:Ljava/lang/String;

    .line 287
    iget-object v9, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->discounts:Ljava/util/List;

    .line 288
    iget-object v10, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_section_header_row:Ljava/lang/Boolean;

    .line 289
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    .line 278
    invoke-direct/range {v0 .. v11}, Lcom/squareup/comms/protos/seller/DisplayItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 183
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final client_id(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->client_id:Ljava/lang/String;

    return-object p0
.end method

.method public final description(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;
    .locals 0

    .line 225
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public final discounts(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/Discount;",
            ">;)",
            "Lcom/squareup/comms/protos/seller/DisplayItem$Builder;"
        }
    .end annotation

    const-string v0, "discounts"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 263
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 264
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->discounts:Ljava/util/List;

    return-object p0
.end method

.method public final is_discount_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;
    .locals 0

    .line 241
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_discount_row:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final is_section_header_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;
    .locals 0

    .line 274
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_section_header_row:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public final per_unit_price_and_quantity(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;
    .locals 0

    .line 258
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->per_unit_price_and_quantity:Ljava/lang/String;

    return-object p0
.end method

.method public final price(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->price:Ljava/lang/String;

    return-object p0
.end method

.method public final quantity(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;
    .locals 0

    .line 249
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->quantity:Ljava/lang/Integer;

    return-object p0
.end method

.method public final taxes(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/Tax;",
            ">;)",
            "Lcom/squareup/comms/protos/seller/DisplayItem$Builder;"
        }
    .end annotation

    const-string v0, "taxes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 235
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 236
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->taxes:Ljava/util/List;

    return-object p0
.end method
