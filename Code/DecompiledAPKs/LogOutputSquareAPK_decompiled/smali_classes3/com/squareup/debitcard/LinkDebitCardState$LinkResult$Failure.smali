.class public final Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;
.super Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;
.source "LinkDebitCardState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Failure"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0014\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\u0008\u001a\u00020\u0006\u0012\u0006\u0010\t\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0006H\u00c6\u0003JE\u0010\u0019\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00062\u0008\u0008\u0002\u0010\t\u001a\u00020\u0006H\u00c6\u0001J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\u0013\u0010\u001c\u001a\u00020\u00062\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u00d6\u0003J\t\u0010\u001f\u001a\u00020\u001bH\u00d6\u0001J\t\u0010 \u001a\u00020\u0003H\u00d6\u0001J\u0019\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020\u001bH\u00d6\u0001R\u0011\u0010\u0008\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0011\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000cR\u0011\u0010\t\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0011\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;",
        "Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;",
        "title",
        "",
        "message",
        "cardUnsupported",
        "",
        "clientUpgradeRequired",
        "attemptFailed",
        "linkDebitCardFailed",
        "(Ljava/lang/String;Ljava/lang/String;ZZZZ)V",
        "getAttemptFailed",
        "()Z",
        "getCardUnsupported",
        "getClientUpgradeRequired",
        "getLinkDebitCardFailed",
        "getMessage",
        "()Ljava/lang/String;",
        "getTitle",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final attemptFailed:Z

.field private final cardUnsupported:Z

.field private final clientUpgradeRequired:Z

.field private final linkDebitCardFailed:Z

.field private final message:Ljava/lang/String;

.field private final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure$Creator;

    invoke-direct {v0}, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure$Creator;-><init>()V

    sput-object v0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZZZ)V
    .locals 1

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, v0}, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->message:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->cardUnsupported:Z

    iput-boolean p4, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->clientUpgradeRequired:Z

    iput-boolean p5, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->attemptFailed:Z

    iput-boolean p6, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->linkDebitCardFailed:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;Ljava/lang/String;Ljava/lang/String;ZZZZILjava/lang/Object;)Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->title:Ljava/lang/String;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->message:Ljava/lang/String;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->cardUnsupported:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->clientUpgradeRequired:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->attemptFailed:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->linkDebitCardFailed:Z

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move p5, v0

    move p6, v1

    move p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->copy(Ljava/lang/String;Ljava/lang/String;ZZZZ)Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->cardUnsupported:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->clientUpgradeRequired:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->attemptFailed:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->linkDebitCardFailed:Z

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;ZZZZ)Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;
    .locals 8

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->message:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->message:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->cardUnsupported:Z

    iget-boolean v1, p1, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->cardUnsupported:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->clientUpgradeRequired:Z

    iget-boolean v1, p1, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->clientUpgradeRequired:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->attemptFailed:Z

    iget-boolean v1, p1, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->attemptFailed:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->linkDebitCardFailed:Z

    iget-boolean p1, p1, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->linkDebitCardFailed:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAttemptFailed()Z
    .locals 1

    .line 27
    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->attemptFailed:Z

    return v0
.end method

.method public final getCardUnsupported()Z
    .locals 1

    .line 25
    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->cardUnsupported:Z

    return v0
.end method

.method public final getClientUpgradeRequired()Z
    .locals 1

    .line 26
    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->clientUpgradeRequired:Z

    return v0
.end method

.method public final getLinkDebitCardFailed()Z
    .locals 1

    .line 28
    iget-boolean v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->linkDebitCardFailed:Z

    return v0
.end method

.method public final getMessage()Ljava/lang/String;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->title:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->message:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->cardUnsupported:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->clientUpgradeRequired:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->attemptFailed:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->linkDebitCardFailed:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failure(title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", cardUnsupported="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->cardUnsupported:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", clientUpgradeRequired="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->clientUpgradeRequired:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", attemptFailed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->attemptFailed:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", linkDebitCardFailed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->linkDebitCardFailed:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->title:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->message:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean p2, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->cardUnsupported:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean p2, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->clientUpgradeRequired:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean p2, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->attemptFailed:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean p2, p0, Lcom/squareup/debitcard/LinkDebitCardState$LinkResult$Failure;->linkDebitCardFailed:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
