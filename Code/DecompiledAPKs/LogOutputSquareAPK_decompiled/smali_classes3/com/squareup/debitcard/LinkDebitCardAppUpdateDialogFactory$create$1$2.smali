.class final Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1$2;
.super Ljava/lang/Object;
.source "LinkDebitCardAppUpdateDialogFactory.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/content/DialogInterface;",
        "kotlin.jvm.PlatformType",
        "onCancel"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogScreen;


# direct methods
.method constructor <init>(Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1$2;->$screen:Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .line 49
    iget-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$create$1$2;->$screen:Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogScreen;

    invoke-virtual {p1}, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogScreen;->getOnDismissed()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method
