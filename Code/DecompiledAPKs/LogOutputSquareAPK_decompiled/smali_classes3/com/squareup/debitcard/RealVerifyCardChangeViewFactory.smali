.class public final Lcom/squareup/debitcard/RealVerifyCardChangeViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealVerifyCardChangeViewFactory.kt"

# interfaces
.implements Lcom/squareup/debitcard/VerifyCardChangeViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "Lcom/squareup/debitcard/RealVerifyCardChangeViewFactory;",
        "Lcom/squareup/debitcard/VerifyCardChangeViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 9
    sget-object v1, Lcom/squareup/debitcard/VerifyingCardChangeLayoutRunner;->Companion:Lcom/squareup/debitcard/VerifyingCardChangeLayoutRunner$Companion;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 10
    sget-object v1, Lcom/squareup/debitcard/VerifyCardChangeSuccessLayoutRunner;->Companion:Lcom/squareup/debitcard/VerifyCardChangeSuccessLayoutRunner$Companion;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 11
    sget-object v1, Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner;->Companion:Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner$Companion;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 8
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
