.class final Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealVerifyCardChangeWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow;->render(Lcom/squareup/debitcard/VerifyCardChangeProps;Lcom/squareup/debitcard/VerifyCardChangeState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$Finish;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$Finish;",
        "it",
        "",
        "invoke",
        "(Lkotlin/Unit;)Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$Finish;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$render$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$render$2;

    invoke-direct {v0}, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$render$2;-><init>()V

    sput-object v0, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$render$2;->INSTANCE:Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$render$2;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lkotlin/Unit;)Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$Finish;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    sget-object p1, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$Finish;->INSTANCE:Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$Finish;

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$render$2;->invoke(Lkotlin/Unit;)Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$Finish;

    move-result-object p1

    return-object p1
.end method
