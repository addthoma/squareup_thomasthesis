.class public final Lcom/squareup/debitcard/LinkDebitCardScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "LinkDebitCardScope.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/debitcard/LinkDebitCardScope$ParentComponent;,
        Lcom/squareup/debitcard/LinkDebitCardScope$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLinkDebitCardScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LinkDebitCardScope.kt\ncom/squareup/debitcard/LinkDebitCardScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,40:1\n35#2:41\n*E\n*S KotlinDebug\n*F\n+ 1 LinkDebitCardScope.kt\ncom/squareup/debitcard/LinkDebitCardScope\n*L\n16#1:41\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0018\u0000 \u00122\u00020\u0001:\u0002\u0012\u0013B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0014J\u0008\u0010\u0010\u001a\u00020\u0011H\u0016R\u0011\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/debitcard/LinkDebitCardScope;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "parent",
        "(Lcom/squareup/ui/main/RegisterTreeKey;)V",
        "getParent",
        "()Lcom/squareup/ui/main/RegisterTreeKey;",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "getParentKey",
        "",
        "Companion",
        "ParentComponent",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/debitcard/LinkDebitCardScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/debitcard/LinkDebitCardScope$Companion;


# instance fields
.field private final parent:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/debitcard/LinkDebitCardScope$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/debitcard/LinkDebitCardScope$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/debitcard/LinkDebitCardScope;->Companion:Lcom/squareup/debitcard/LinkDebitCardScope$Companion;

    .line 35
    sget-object v0, Lcom/squareup/debitcard/LinkDebitCardScope$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardScope$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "PathCreator.fromParcel {\u2026ava.classLoader)!!)\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/debitcard/LinkDebitCardScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardScope;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    const-class v0, Lcom/squareup/debitcard/LinkDebitCardScope$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 16
    check-cast v0, Lcom/squareup/debitcard/LinkDebitCardScope$ParentComponent;

    .line 17
    invoke-interface {v0}, Lcom/squareup/debitcard/LinkDebitCardScope$ParentComponent;->linkDebitCardWorkflowRunner()Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner;

    move-result-object v0

    .line 19
    invoke-super {p0, p1}, Lcom/squareup/ui/main/RegisterTreeKey;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object p1

    const-string v1, "it"

    .line 20
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    const-string v0, "super.buildScope(parentS\u2026er.registerServices(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardScope;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public final getParent()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardScope;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardScope;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method
