.class public final Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner$Companion;
.super Ljava/lang/Object;
.source "LinkDebitCardWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLinkDebitCardWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LinkDebitCardWorkflowRunner.kt\ncom/squareup/debitcard/LinkDebitCardWorkflowRunner$Companion\n*L\n1#1,53:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner$Companion;",
        "",
        "()V",
        "NAME",
        "",
        "startNewWorkflow",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "startArg",
        "Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final startNewWorkflow(Lmortar/MortarScope;Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "startArg"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-static {}, Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner;->access$getNAME$cp()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/container/PosWorkflowRunnerKt;->getWorkflowRunner(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/PosWorkflowRunner;

    move-result-object p1

    .line 46
    check-cast p1, Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner;

    .line 47
    invoke-static {p1, p2}, Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner;->access$setProps$p(Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner;Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V

    .line 48
    invoke-static {p1}, Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner;->access$ensureWorkflow(Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner;)V

    return-void
.end method
