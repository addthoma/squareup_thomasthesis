.class public final Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$Companion;
.super Ljava/lang/Object;
.source "LinkDebitCardAppUpdateDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u0004B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0005JI\u0010\u000e\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00100\u000f2\u0006\u0010\u0011\u001a\u00020\u00122\u0012\u0010\u0013\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000bj\u0002`\u00142\u001a\u0010\u0015\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0017j\u0002`\u00180\u0016H\u0096\u0001R\u0012\u0010\u0006\u001a\u00020\u0007X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u001e\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u000bX\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$Companion;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;",
        "Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogScreen;",
        "",
        "Lcom/squareup/workflow/V2DialogBinding;",
        "()V",
        "hint",
        "Lcom/squareup/workflow/ScreenHint;",
        "getHint",
        "()Lcom/squareup/workflow/ScreenHint;",
        "key",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "getKey",
        "()Lcom/squareup/workflow/legacy/Screen$Key;",
        "promiseDialog",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "contextForNewDialog",
        "Landroid/content/Context;",
        "screenKey",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;


# direct methods
.method private constructor <init>()V
    .locals 3

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 56
    const-class v1, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    .line 57
    sget-object v2, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$Companion$1;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$Companion$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 55
    invoke-virtual {v0, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$Companion;->$$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 55
    invoke-direct {p0}, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public getHint()Lcom/squareup/workflow/ScreenHint;
    .locals 1

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$Companion;->$$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    invoke-interface {v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;->getHint()Lcom/squareup/workflow/ScreenHint;

    move-result-object v0

    return-object v0
.end method

.method public getKey()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$Companion;->$$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    invoke-interface {v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;->getKey()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    return-object v0
.end method

.method public promiseDialog(Landroid/content/Context;Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;)",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "contextForNewDialog"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenKey"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardAppUpdateDialogFactory$Companion;->$$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;->promiseDialog(Landroid/content/Context;Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
