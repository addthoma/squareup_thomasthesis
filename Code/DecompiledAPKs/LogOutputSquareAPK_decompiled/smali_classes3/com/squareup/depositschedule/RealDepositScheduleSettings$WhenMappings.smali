.class public final synthetic Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;->values()[Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;->UNCHANGED_WEEKEND_BALANCE:Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;->ENABLE_WEEKEND_BALANCE:Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;->DISABLE_WEEKEND_BALANCE:Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;->values()[Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;->UNCHANGED_AUTOMATIC_DEPOSITS:Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;->ENABLE_AUTOMATIC_DEPOSITS:Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;->DISABLE_AUTOMATIC_DEPOSITS:Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;->ordinal()I

    move-result v1

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->values()[Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->UNCHANGED_DEPOSIT_SPEED:Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->SET_ALL_TO_SAME_DAY:Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->SET_ALL_TO_ONE_TO_TWO_BUSINESS_DAYS:Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->SET_TO_SAME_DAY:Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->SET_TO_ONE_TO_TWO_BUSINESS_DAYS:Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    return-void
.end method
