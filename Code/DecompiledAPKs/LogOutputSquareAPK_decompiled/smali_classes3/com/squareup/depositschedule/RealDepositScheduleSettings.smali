.class public final Lcom/squareup/depositschedule/RealDepositScheduleSettings;
.super Ljava/lang/Object;
.source "RealDepositScheduleSettings.kt"

# interfaces
.implements Lcom/squareup/depositschedule/DepositScheduleSettings;


# annotations
.annotation runtime Lcom/squareup/depositschedule/RealDepositScheduleSettings$SharedScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/depositschedule/RealDepositScheduleSettings$SharedScope;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealDepositScheduleSettings.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealDepositScheduleSettings.kt\ncom/squareup/depositschedule/RealDepositScheduleSettings\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,348:1\n1360#2:349\n1429#2,3:350\n1577#2,4:353\n*E\n*S KotlinDebug\n*F\n+ 1 RealDepositScheduleSettings.kt\ncom/squareup/depositschedule/RealDepositScheduleSettings\n*L\n290#1:349\n290#1,3:350\n330#1,4:353\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0098\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0008\n\u0002\u0008\u0012\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u0001GB\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ2\u0010\u0013\u001a\u00020\u00142\u0008\u0008\u0002\u0010\u0015\u001a\u00020\u00162\n\u0008\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0008\u0008\u0002\u0010\u0019\u001a\u00020\u001a2\u0008\u0008\u0002\u0010\u001b\u001a\u00020\u001cH\u0002J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0002J\u000e\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\r0\"H\u0016J\u0008\u0010#\u001a\u00020$H\u0016J\u001c\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\r0\"2\u000c\u0010&\u001a\u0008\u0012\u0004\u0012\u00020(0\'H\u0002J\u000e\u0010)\u001a\u0008\u0012\u0004\u0012\u00020\r0\"H\u0002J\u0008\u0010*\u001a\u00020+H\u0002J\u000e\u0010,\u001a\u0008\u0012\u0004\u0012\u00020\r0\"H\u0002J\u000e\u0010-\u001a\u0008\u0012\u0004\u0012\u00020\r0\"H\u0002J\u0016\u0010.\u001a\u0008\u0012\u0004\u0012\u00020\r0\"2\u0006\u0010/\u001a\u00020$H\u0002J\u0016\u00100\u001a\u0008\u0012\u0004\u0012\u00020\r0\"2\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0010\u00101\u001a\u00020+2\u0006\u00102\u001a\u000203H\u0002J\u0016\u00104\u001a\u0008\u0012\u0004\u0012\u00020\r0\"2\u0006\u00105\u001a\u00020$H\u0002J\u0016\u00106\u001a\u0008\u0012\u0004\u0012\u00020\r0\"2\u0006\u0010/\u001a\u00020$H\u0002J\u001e\u00107\u001a\u0008\u0012\u0004\u0012\u00020\r0\"2\u0006\u00108\u001a\u00020(2\u0006\u00109\u001a\u00020\u001eH\u0002J\u0008\u0010:\u001a\u00020+H\u0016J\u000e\u0010;\u001a\u0008\u0012\u0004\u0012\u00020\r0\"H\u0016J\u000e\u0010<\u001a\u0008\u0012\u0004\u0012\u00020\r0\"H\u0016J\u0016\u0010=\u001a\u0008\u0012\u0004\u0012\u00020\r0\"2\u0006\u0010/\u001a\u00020$H\u0016J\u001e\u0010>\u001a\u0008\u0012\u0004\u0012\u00020\r0\"2\u0006\u00102\u001a\u0002032\u0006\u0010?\u001a\u000203H\u0016J\u0010\u0010@\u001a\u00020+2\u0006\u00102\u001a\u000203H\u0016J\u001e\u0010A\u001a\u0008\u0012\u0004\u0012\u00020\r0\"2\u0006\u0010B\u001a\u00020\u00142\u0006\u00109\u001a\u00020\u001eH\u0002J\u0016\u0010C\u001a\u0008\u0012\u0004\u0012\u00020\r0\"2\u0006\u00105\u001a\u00020$H\u0016J\u0016\u0010D\u001a\u0008\u0012\u0004\u0012\u00020\r0\"2\u0006\u0010/\u001a\u00020$H\u0016J\u000e\u0010E\u001a\u0008\u0012\u0004\u0012\u00020\r0FH\u0016J\u0018\u0010\u001f\u001a\n \u000e*\u0004\u0018\u00010 0 2\u0006\u00108\u001a\u00020(H\u0002R\u001c\u0010\u000b\u001a\u0010\u0012\u000c\u0012\n \u000e*\u0004\u0018\u00010\r0\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006H"
    }
    d2 = {
        "Lcom/squareup/depositschedule/RealDepositScheduleSettings;",
        "Lcom/squareup/depositschedule/DepositScheduleSettings;",
        "depositScheduleService",
        "Lcom/squareup/depositschedule/DepositScheduleService;",
        "messageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "(Lcom/squareup/depositschedule/DepositScheduleService;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Clock;)V",
        "_state",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
        "kotlin.jvm.PlatformType",
        "getDepositScheduleRequest",
        "Lcom/squareup/protos/client/depositsettings/GetDepositScheduleRequest;",
        "timezone",
        "",
        "createRequest",
        "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;",
        "setDepositSpeed",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;",
        "cutoffAt",
        "Lcom/squareup/protos/common/time/DayTime;",
        "weekendBalance",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;",
        "automaticDeposits",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;",
        "currentDepositSpeed",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;",
        "weeklyDepositSchedule",
        "Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;",
        "getDepositSchedule",
        "Lio/reactivex/Single;",
        "isVisible",
        "",
        "onFailure",
        "failure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "",
        "onGetDepositSchedule",
        "onSetAllToCustom",
        "",
        "onSetAllToOneToTwoBusinessDays",
        "onSetAllToSameDay",
        "onSetAutomaticDeposits",
        "enabled",
        "onSetCustom",
        "onSetDayOfWeek",
        "dayOfWeek",
        "",
        "onSetSameDayDeposit",
        "sameDay",
        "onSetWeekendBalance",
        "onSuccess",
        "response",
        "depositSpeed",
        "setAllToCustom",
        "setAllToOneToTwoBusinessDays",
        "setAllToSameDay",
        "setAutomaticDepositsEnabled",
        "setCustom",
        "hourOfDay",
        "setDayOfWeek",
        "setDepositSchedule",
        "request",
        "setSameDayDeposit",
        "setWeekendBalanceEnabled",
        "state",
        "Lio/reactivex/Observable;",
        "SharedScope",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final _state:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation
.end field

.field private final depositScheduleService:Lcom/squareup/depositschedule/DepositScheduleService;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final getDepositScheduleRequest:Lcom/squareup/protos/client/depositsettings/GetDepositScheduleRequest;

.field private final messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final timezone:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/depositschedule/DepositScheduleService;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Clock;)V
    .locals 10
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "depositScheduleService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messageFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->depositScheduleService:Lcom/squareup/depositschedule/DepositScheduleService;

    iput-object p2, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    iput-object p3, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->features:Lcom/squareup/settings/server/Features;

    .line 65
    new-instance p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3f

    const/4 v9, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v9}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;-><init>(Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(State())"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 66
    invoke-interface {p4}, Lcom/squareup/util/Clock;->getTimeZone()Ljava/util/TimeZone;

    move-result-object p1

    const-string p2, "clock.timeZone"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object p1

    const-string p2, "clock.timeZone.id"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->timezone:Ljava/lang/String;

    .line 68
    new-instance p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleRequest$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleRequest$Builder;-><init>()V

    .line 69
    iget-object p2, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->timezone:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleRequest$Builder;->timezone(Ljava/lang/String;)Lcom/squareup/protos/client/depositsettings/GetDepositScheduleRequest$Builder;

    move-result-object p1

    .line 70
    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleRequest$Builder;->build()Lcom/squareup/protos/client/depositsettings/GetDepositScheduleRequest;

    move-result-object p1

    const-string p2, "GetDepositScheduleReques\u2026imezone)\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->getDepositScheduleRequest:Lcom/squareup/protos/client/depositsettings/GetDepositScheduleRequest;

    return-void
.end method

.method public static final synthetic access$currentDepositSpeed(Lcom/squareup/depositschedule/RealDepositScheduleSettings;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;)Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->currentDepositSpeed(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;)Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onFailure(Lcom/squareup/depositschedule/RealDepositScheduleSettings;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lio/reactivex/Single;
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->onFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onSuccess(Lcom/squareup/depositschedule/RealDepositScheduleSettings;Ljava/lang/Object;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;)Lio/reactivex/Single;
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->onSuccess(Ljava/lang/Object;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$weeklyDepositSchedule(Lcom/squareup/depositschedule/RealDepositScheduleSettings;Ljava/lang/Object;)Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->weeklyDepositSchedule(Ljava/lang/Object;)Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    move-result-object p0

    return-object p0
.end method

.method private final createRequest(Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;Lcom/squareup/protos/common/time/DayTime;Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;
    .locals 11

    .line 268
    sget-object v0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p3}, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;->ordinal()I

    move-result p3

    aget p3, v0, p3

    const/4 v0, 0x0

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eq p3, v4, :cond_2

    if-eq p3, v2, :cond_1

    if-ne p3, v1, :cond_0

    .line 271
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 270
    :cond_1
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    goto :goto_0

    .line 269
    :cond_2
    iget-object p3, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p3}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p3

    if-nez p3, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    check-cast p3, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    iget-object p3, p3, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->weeklyDepositSchedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    if-eqz p3, :cond_4

    iget-object p3, p3, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->settle_if_optional:Ljava/lang/Boolean;

    goto :goto_0

    :cond_4
    move-object p3, v0

    .line 274
    :goto_0
    sget-object v5, Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p4}, Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;->ordinal()I

    move-result p4

    aget p4, v5, p4

    if-eq p4, v4, :cond_7

    if-eq p4, v2, :cond_6

    if-ne p4, v1, :cond_5

    .line 277
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 276
    :cond_6
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 275
    :cond_7
    iget-object p4, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p4}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p4

    if-nez p4, :cond_8

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_8
    check-cast p4, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    iget-object p4, p4, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->weeklyDepositSchedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    if-eqz p4, :cond_9

    iget-object v0, p4, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->paused_settlement:Ljava/lang/Boolean;

    .line 280
    :cond_9
    :goto_1
    sget-object p4, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->SET_ALL_TO_SAME_DAY:Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;

    if-ne p1, p4, :cond_a

    .line 281
    new-instance p1, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;-><init>()V

    .line 282
    iget-object p2, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->timezone:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->timezone(Ljava/lang/String;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;

    move-result-object p1

    .line 283
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->enable_daily_same_day_settlement(Ljava/lang/Boolean;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;

    move-result-object p1

    .line 284
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->settle_if_optional(Ljava/lang/Boolean;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;

    move-result-object p1

    .line 285
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->pause_settlement(Ljava/lang/Boolean;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;

    move-result-object p1

    .line 286
    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->build()Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;

    move-result-object p1

    const-string p2, "SetDepositScheduleReques\u2026ement)\n          .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 289
    :cond_a
    iget-object p4, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p4}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p4

    if-nez p4, :cond_b

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_b
    check-cast p4, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    invoke-virtual {p4}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->dayOfWeekDepositSettings()Ljava/util/List;

    move-result-object p4

    check-cast p4, Ljava/lang/Iterable;

    .line 349
    new-instance v5, Ljava/util/ArrayList;

    const/16 v6, 0xa

    invoke-static {p4, v6}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v5, Ljava/util/Collection;

    .line 350
    invoke-interface {p4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :goto_2
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_14

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 351
    check-cast v6, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    .line 291
    iget-object v7, v6, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->day_of_week:Lcom/squareup/protos/common/time/DayOfWeek;

    invoke-virtual {v7}, Lcom/squareup/protos/common/time/DayOfWeek;->ordinal()I

    move-result v7

    iget-object v8, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v8}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_c

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_c
    check-cast v8, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    iget v8, v8, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->dayOfWeek:I

    add-int/2addr v8, v4

    if-ne v7, v8, :cond_d

    const/4 v7, 0x1

    goto :goto_3

    :cond_d
    const/4 v7, 0x0

    .line 293
    :goto_3
    iget-object v8, v6, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    iget-object v8, v8, Lcom/squareup/protos/client/depositsettings/DepositInterval;->same_day_settlement_enabled:Ljava/lang/Boolean;

    .line 294
    sget-object v9, Lcom/squareup/depositschedule/RealDepositScheduleSettings$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p1}, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->ordinal()I

    move-result v10

    aget v9, v9, v10

    if-eq v9, v4, :cond_12

    if-eq v9, v2, :cond_11

    if-eq v9, v1, :cond_10

    const/4 v10, 0x4

    if-eq v9, v10, :cond_f

    const/4 v10, 0x5

    if-ne v9, v10, :cond_e

    if-eqz v7, :cond_12

    .line 299
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    goto :goto_4

    :cond_e
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_f
    if-eqz v7, :cond_12

    .line 298
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    goto :goto_4

    .line 297
    :cond_10
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    goto :goto_4

    .line 296
    :cond_11
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    :cond_12
    :goto_4
    if-eqz p2, :cond_13

    if-eqz v7, :cond_13

    move-object v7, p2

    goto :goto_5

    .line 303
    :cond_13
    iget-object v7, v6, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    iget-object v7, v7, Lcom/squareup/protos/client/depositsettings/DepositInterval;->cutoff_at:Lcom/squareup/protos/common/time/DayTime;

    .line 305
    :goto_5
    new-instance v9, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;

    invoke-direct {v9}, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;-><init>()V

    .line 306
    invoke-virtual {v9, v7}, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->cutoff_at(Lcom/squareup/protos/common/time/DayTime;)Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;

    move-result-object v7

    .line 307
    invoke-virtual {v7, v8}, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->same_day_settlement_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;

    move-result-object v7

    .line 308
    invoke-virtual {v7}, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->build()Lcom/squareup/protos/client/depositsettings/DepositInterval;

    move-result-object v7

    .line 309
    new-instance v8, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;

    invoke-direct {v8}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;-><init>()V

    .line 310
    iget-object v6, v6, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->day_of_week:Lcom/squareup/protos/common/time/DayOfWeek;

    invoke-virtual {v8, v6}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->day_of_week(Lcom/squareup/protos/common/time/DayOfWeek;)Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;

    move-result-object v6

    .line 311
    invoke-virtual {v6, v7}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->deposit_interval(Lcom/squareup/protos/client/depositsettings/DepositInterval;)Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;

    move-result-object v6

    .line 312
    invoke-virtual {v6}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->build()Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 352
    :cond_14
    check-cast v5, Ljava/util/List;

    .line 315
    new-instance p1, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;-><init>()V

    .line 316
    iget-object p2, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->timezone:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->timezone(Ljava/lang/String;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;

    move-result-object p1

    .line 317
    invoke-virtual {p1, v5}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->day_of_week_deposit_settings(Ljava/util/List;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;

    move-result-object p1

    .line 318
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->settle_if_optional(Ljava/lang/Boolean;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;

    move-result-object p1

    .line 319
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->pause_settlement(Ljava/lang/Boolean;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;

    move-result-object p1

    .line 320
    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest$Builder;->build()Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;

    move-result-object p1

    const-string p2, "SetDepositScheduleReques\u2026tlement)\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method static synthetic createRequest$default(Lcom/squareup/depositschedule/RealDepositScheduleSettings;Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;Lcom/squareup/protos/common/time/DayTime;Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;ILjava/lang/Object;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    .line 263
    sget-object p1, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->UNCHANGED_DEPOSIT_SPEED:Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    const/4 p2, 0x0

    .line 264
    check-cast p2, Lcom/squareup/protos/common/time/DayTime;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    .line 265
    sget-object p3, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;->UNCHANGED_WEEKEND_BALANCE:Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    .line 266
    sget-object p4, Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;->UNCHANGED_AUTOMATIC_DEPOSITS:Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;

    :cond_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->createRequest(Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;Lcom/squareup/protos/common/time/DayTime;Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;

    move-result-object p0

    return-object p0
.end method

.method private final currentDepositSpeed(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;)Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;
    .locals 3

    .line 324
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SAME_DAY_DEPOSIT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 326
    sget-object p1, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->CUSTOM:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    return-object p1

    .line 329
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->day_of_week_deposit_settings:Ljava/util/List;

    const-string/jumbo v0, "weeklyDepositSchedule.day_of_week_deposit_settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 353
    instance-of v0, p1, Ljava/util/Collection;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    .line 355
    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    .line 330
    iget-object v0, v0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    iget-object v0, v0, Lcom/squareup/protos/client/depositsettings/DepositInterval;->same_day_settlement_enabled:Ljava/lang/Boolean;

    const-string v2, "it.deposit_interval.same_day_settlement_enabled"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit8 v1, v1, 0x1

    if-gez v1, :cond_2

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_0

    :cond_3
    :goto_1
    if-eqz v1, :cond_5

    const/4 p1, 0x7

    if-eq v1, p1, :cond_4

    .line 335
    sget-object p1, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->CUSTOM:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    goto :goto_2

    .line 334
    :cond_4
    sget-object p1, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->SAME_DAY:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    goto :goto_2

    .line 333
    :cond_5
    sget-object p1, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->ONE_TO_TWO_BUSINESS_DAYS:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    :goto_2
    return-object p1
.end method

.method private final onFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lio/reactivex/Single;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+",
            "Ljava/lang/Object;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 253
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v1, Lcom/squareup/depositschedule/impl/R$string;->instant_deposits_could_not_load_deposit_schedule:I

    sget-object v2, Lcom/squareup/depositschedule/RealDepositScheduleSettings$onFailure$failureMessage$1;->INSTANCE:Lcom/squareup/depositschedule/RealDepositScheduleSettings$onFailure$failureMessage$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object v7

    .line 257
    iget-object p1, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v3, p1

    check-cast v3, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    sget-object v4, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->FAILED:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x36

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->copy$default(Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;IILjava/lang/Object;)Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-result-object p1

    .line 258
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 259
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "just(state)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onGetDepositSchedule()Lio/reactivex/Single;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v2, v1

    check-cast v2, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    sget-object v3, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->LOADING:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->NO_SELECTION:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    const/4 v8, 0x0

    const/16 v9, 0x2e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->copy$default(Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;IILjava/lang/Object;)Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 115
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->depositScheduleService:Lcom/squareup/depositschedule/DepositScheduleService;

    iget-object v1, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->getDepositScheduleRequest:Lcom/squareup/protos/client/depositsettings/GetDepositScheduleRequest;

    invoke-interface {v0, v1}, Lcom/squareup/depositschedule/DepositScheduleService;->getDepositSchedule(Lcom/squareup/protos/client/depositsettings/GetDepositScheduleRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 117
    new-instance v1, Lcom/squareup/depositschedule/RealDepositScheduleSettings$onGetDepositSchedule$1;

    invoke-direct {v1, p0}, Lcom/squareup/depositschedule/RealDepositScheduleSettings$onGetDepositSchedule$1;-><init>(Lcom/squareup/depositschedule/RealDepositScheduleSettings;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "depositScheduleService.g\u2026it)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final onSetAllToCustom()V
    .locals 11

    .line 164
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v2, v1

    check-cast v2, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->CUSTOM:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    const/4 v8, 0x0

    const/16 v9, 0x2f

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->copy$default(Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;IILjava/lang/Object;)Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final onSetAllToOneToTwoBusinessDays()Lio/reactivex/Single;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 144
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    iget-object v0, v0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->weeklyDepositSchedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    if-eqz v0, :cond_2

    .line 145
    invoke-direct {p0, v0}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->currentDepositSpeed(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;)Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    move-result-object v0

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->ONE_TO_TWO_BUSINESS_DAYS:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    if-ne v0, v1, :cond_2

    .line 146
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    move-object v1, v0

    check-cast v1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->ONE_TO_TWO_BUSINESS_DAYS:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    const/4 v7, 0x0

    const/16 v8, 0x2f

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->copy$default(Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;IILjava/lang/Object;)Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-result-object v0

    .line 147
    iget-object v1, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 148
    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "just(state)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 152
    :cond_2
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 153
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    move-object v2, v1

    check-cast v2, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    sget-object v3, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->LOADING:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->ONE_TO_TWO_BUSINESS_DAYS:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    const/4 v8, 0x0

    const/16 v9, 0x2e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->copy$default(Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;IILjava/lang/Object;)Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-result-object v1

    .line 152
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 157
    sget-object v3, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->SET_ALL_TO_ONE_TO_TWO_BUSINESS_DAYS:Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;

    const/16 v7, 0xe

    const/4 v8, 0x0

    move-object v2, p0

    .line 156
    invoke-static/range {v2 .. v8}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->createRequest$default(Lcom/squareup/depositschedule/RealDepositScheduleSettings;Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;Lcom/squareup/protos/common/time/DayTime;Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;ILjava/lang/Object;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;

    move-result-object v0

    .line 160
    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->ONE_TO_TWO_BUSINESS_DAYS:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    invoke-direct {p0, v0, v1}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->setDepositSchedule(Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method private final onSetAllToSameDay()Lio/reactivex/Single;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    iget-object v0, v0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->weeklyDepositSchedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    if-eqz v0, :cond_2

    .line 129
    invoke-direct {p0, v0}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->currentDepositSpeed(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;)Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    move-result-object v0

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->SAME_DAY:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    if-ne v0, v1, :cond_2

    .line 130
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    move-object v1, v0

    check-cast v1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->SAME_DAY:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    const/4 v7, 0x0

    const/16 v8, 0x2f

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->copy$default(Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;IILjava/lang/Object;)Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-result-object v0

    .line 131
    iget-object v1, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 132
    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "just(state)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 136
    :cond_2
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    move-object v2, v1

    check-cast v2, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    sget-object v3, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->LOADING:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->SAME_DAY:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    const/4 v8, 0x0

    const/16 v9, 0x2e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->copy$default(Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;IILjava/lang/Object;)Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 138
    sget-object v3, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->SET_ALL_TO_SAME_DAY:Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;

    const/16 v7, 0xe

    const/4 v8, 0x0

    move-object v2, p0

    invoke-static/range {v2 .. v8}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->createRequest$default(Lcom/squareup/depositschedule/RealDepositScheduleSettings;Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;Lcom/squareup/protos/common/time/DayTime;Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;ILjava/lang/Object;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;

    move-result-object v0

    .line 140
    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->SAME_DAY:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    invoke-direct {p0, v0, v1}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->setDepositSchedule(Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method private final onSetAutomaticDeposits(Z)Lio/reactivex/Single;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 205
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    invoke-virtual {v0}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->pausedSettlement()Z

    move-result v0

    if-eq p1, v0, :cond_1

    .line 206
    iget-object p1, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "_state.firstOrError()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 209
    :cond_1
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    move-object v2, v1

    check-cast v2, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    sget-object v3, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->LOADING:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x3e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->copy$default(Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;IILjava/lang/Object;)Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    if-eqz p1, :cond_3

    .line 211
    sget-object p1, Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;->ENABLE_AUTOMATIC_DEPOSITS:Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;

    goto :goto_0

    :cond_3
    sget-object p1, Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;->DISABLE_AUTOMATIC_DEPOSITS:Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;

    :goto_0
    move-object v4, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v0, p0

    .line 212
    invoke-static/range {v0 .. v6}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->createRequest$default(Lcom/squareup/depositschedule/RealDepositScheduleSettings;Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;Lcom/squareup/protos/common/time/DayTime;Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;ILjava/lang/Object;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;

    move-result-object p1

    .line 214
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    check-cast v0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    iget-object v0, v0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositSpeed:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    invoke-direct {p0, p1, v0}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->setDepositSchedule(Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method private final onSetCustom(Lcom/squareup/protos/common/time/DayTime;)Lio/reactivex/Single;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/time/DayTime;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 182
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v2, v1

    check-cast v2, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    sget-object v3, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->LOADING:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->CUSTOM:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    const/4 v8, 0x0

    const/16 v9, 0x2e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->copy$default(Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;IILjava/lang/Object;)Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    const/4 v3, 0x0

    const/16 v7, 0xd

    const/4 v8, 0x0

    move-object v2, p0

    move-object v4, p1

    .line 184
    invoke-static/range {v2 .. v8}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->createRequest$default(Lcom/squareup/depositschedule/RealDepositScheduleSettings;Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;Lcom/squareup/protos/common/time/DayTime;Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;ILjava/lang/Object;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;

    move-result-object p1

    .line 188
    sget-object v0, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->CUSTOM:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    invoke-direct {p0, p1, v0}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->setDepositSchedule(Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method private final onSetDayOfWeek(I)V
    .locals 11

    .line 168
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v2, v1

    check-cast v2, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1f

    const/4 v10, 0x0

    move v8, p1

    invoke-static/range {v2 .. v10}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->copy$default(Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;IILjava/lang/Object;)Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final onSetSameDayDeposit(Z)Lio/reactivex/Single;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 172
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v2, v1

    check-cast v2, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    sget-object v3, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->LOADING:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->CUSTOM:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    const/4 v8, 0x0

    const/16 v9, 0x2e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->copy$default(Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;IILjava/lang/Object;)Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    .line 175
    sget-object p1, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->SET_TO_SAME_DAY:Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;->SET_TO_ONE_TO_TWO_BUSINESS_DAYS:Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;

    :goto_0
    move-object v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p0

    .line 176
    invoke-static/range {v0 .. v6}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->createRequest$default(Lcom/squareup/depositschedule/RealDepositScheduleSettings;Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;Lcom/squareup/protos/common/time/DayTime;Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;ILjava/lang/Object;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;

    move-result-object p1

    .line 178
    sget-object v0, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->CUSTOM:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    invoke-direct {p0, p1, v0}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->setDepositSchedule(Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method private final onSetWeekendBalance(Z)Lio/reactivex/Single;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 192
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    invoke-virtual {v0}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->settleIfOptional()Z

    move-result v0

    if-eq p1, v0, :cond_1

    .line 193
    iget-object p1, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "_state.firstOrError()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 196
    :cond_1
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    move-object v2, v1

    check-cast v2, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    sget-object v3, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->LOADING:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->CUSTOM:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    const/4 v8, 0x0

    const/16 v9, 0x2e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->copy$default(Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;IILjava/lang/Object;)Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    if-eqz p1, :cond_3

    .line 198
    sget-object p1, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;->ENABLE_WEEKEND_BALANCE:Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    goto :goto_0

    :cond_3
    sget-object p1, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;->DISABLE_WEEKEND_BALANCE:Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    :goto_0
    move-object v3, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xb

    const/4 v6, 0x0

    move-object v0, p0

    .line 199
    invoke-static/range {v0 .. v6}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->createRequest$default(Lcom/squareup/depositschedule/RealDepositScheduleSettings;Lcom/squareup/depositschedule/DepositScheduleSettings$SetDepositSpeed;Lcom/squareup/protos/common/time/DayTime;Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;Lcom/squareup/depositschedule/DepositScheduleSettings$AutomaticDeposits;ILjava/lang/Object;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;

    move-result-object p1

    .line 201
    sget-object v0, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->CUSTOM:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    invoke-direct {p0, p1, v0}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->setDepositSchedule(Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method private final onSuccess(Ljava/lang/Object;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;)Lio/reactivex/Single;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 235
    instance-of v0, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;

    if-eqz v0, :cond_0

    .line 236
    move-object v0, p1

    check-cast v0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;

    :goto_0
    move-object v4, v0

    goto :goto_1

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast v0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    iget-object v0, v0, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->defaultCutoffTime:Lcom/squareup/protos/common/time/LocalTime;

    goto :goto_0

    .line 241
    :goto_1
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    move-object v1, v0

    check-cast v1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    .line 242
    sget-object v2, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->SUCCEEDED:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    .line 243
    invoke-direct {p0, p1}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->weeklyDepositSchedule(Ljava/lang/Object;)Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x28

    const/4 v9, 0x0

    move-object v6, p2

    .line 241
    invoke-static/range {v1 .. v9}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->copy$default(Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/receiving/FailureMessage;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;IILjava/lang/Object;)Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    move-result-object p1

    .line 247
    iget-object p2, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 248
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "just(state)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final setDepositSchedule(Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 221
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->depositScheduleService:Lcom/squareup/depositschedule/DepositScheduleService;

    invoke-interface {v0, p1}, Lcom/squareup/depositschedule/DepositScheduleService;->setDepositSchedule(Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 222
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 223
    new-instance v0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$setDepositSchedule$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/depositschedule/RealDepositScheduleSettings$setDepositSchedule$1;-><init>(Lcom/squareup/depositschedule/RealDepositScheduleSettings;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "depositScheduleService.s\u2026it)\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final weeklyDepositSchedule(Ljava/lang/Object;)Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;
    .locals 1

    .line 341
    instance-of v0, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;

    iget-object v0, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 342
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    goto :goto_0

    .line 343
    :cond_1
    instance-of v0, p1, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;

    iget-object v0, p1, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    if-eqz v0, :cond_2

    goto :goto_0

    .line 344
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    :goto_0
    return-object v0

    .line 345
    :cond_3
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "response not supported."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public getDepositSchedule()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 79
    invoke-direct {p0}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->onGetDepositSchedule()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public isVisible()Z
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_DEPOSIT_SETTINGS_DEPOSIT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_STORED_BALANCE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setAllToCustom()V
    .locals 0

    .line 85
    invoke-direct {p0}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->onSetAllToCustom()V

    return-void
.end method

.method public setAllToOneToTwoBusinessDays()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 83
    invoke-direct {p0}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->onSetAllToOneToTwoBusinessDays()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public setAllToSameDay()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 81
    invoke-direct {p0}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->onSetAllToSameDay()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public setAutomaticDepositsEnabled(Z)Lio/reactivex/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 110
    invoke-direct {p0, p1}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->onSetAutomaticDeposits(Z)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public setCustom(II)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 95
    new-instance v0, Lcom/squareup/protos/common/time/LocalTime$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/LocalTime$Builder;-><init>()V

    .line 96
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/protos/common/time/LocalTime$Builder;->hour_of_day(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/LocalTime$Builder;

    move-result-object p2

    const/4 v0, 0x0

    .line 97
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/protos/common/time/LocalTime$Builder;->minute_of_hour(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/LocalTime$Builder;

    move-result-object p2

    .line 98
    invoke-virtual {p2, v0}, Lcom/squareup/protos/common/time/LocalTime$Builder;->second_of_minute(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/LocalTime$Builder;

    move-result-object p2

    .line 99
    invoke-virtual {p2, v0}, Lcom/squareup/protos/common/time/LocalTime$Builder;->millis_of_second(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/LocalTime$Builder;

    move-result-object p2

    .line 100
    invoke-virtual {p2}, Lcom/squareup/protos/common/time/LocalTime$Builder;->build()Lcom/squareup/protos/common/time/LocalTime;

    move-result-object p2

    .line 101
    new-instance v0, Lcom/squareup/protos/common/time/DayTime$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/DayTime$Builder;-><init>()V

    .line 102
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/common/time/DayTime$Builder;->day_of_week(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/DayTime$Builder;

    move-result-object p1

    .line 103
    invoke-virtual {p1, p2}, Lcom/squareup/protos/common/time/DayTime$Builder;->time_at(Lcom/squareup/protos/common/time/LocalTime;)Lcom/squareup/protos/common/time/DayTime$Builder;

    move-result-object p1

    .line 104
    invoke-virtual {p1}, Lcom/squareup/protos/common/time/DayTime$Builder;->build()Lcom/squareup/protos/common/time/DayTime;

    move-result-object p1

    const-string p2, "cutoffAt"

    .line 105
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->onSetCustom(Lcom/squareup/protos/common/time/DayTime;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public setDayOfWeek(I)V
    .locals 0

    .line 87
    invoke-direct {p0, p1}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->onSetDayOfWeek(I)V

    return-void
.end method

.method public setSameDayDeposit(Z)Lio/reactivex/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 89
    invoke-direct {p0, p1}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->onSetSameDayDeposit(Z)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public setWeekendBalanceEnabled(Z)Lio/reactivex/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 108
    invoke-direct {p0, p1}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->onSetWeekendBalance(Z)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public state()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    .line 77
    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
