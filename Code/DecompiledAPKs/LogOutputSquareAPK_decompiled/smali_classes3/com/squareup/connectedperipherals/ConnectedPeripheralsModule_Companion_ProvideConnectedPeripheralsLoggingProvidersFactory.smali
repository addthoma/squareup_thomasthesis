.class public final Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule_Companion_ProvideConnectedPeripheralsLoggingProvidersFactory;
.super Ljava/lang/Object;
.source "ConnectedPeripheralsModule_Companion_ProvideConnectedPeripheralsLoggingProvidersFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/Set<",
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final cashDrawersLoggingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final connectedBarcodeScannersLoggingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final connectedCardReadersLoggingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectedperipherals/ConnectedCardReadersLoggingProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final connectedPrintersLoggingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectedperipherals/ConnectedCardReadersLoggingProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule_Companion_ProvideConnectedPeripheralsLoggingProvidersFactory;->connectedCardReadersLoggingProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule_Companion_ProvideConnectedPeripheralsLoggingProvidersFactory;->cashDrawersLoggingProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule_Companion_ProvideConnectedPeripheralsLoggingProvidersFactory;->connectedPrintersLoggingProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule_Companion_ProvideConnectedPeripheralsLoggingProvidersFactory;->connectedBarcodeScannersLoggingProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule_Companion_ProvideConnectedPeripheralsLoggingProvidersFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectedperipherals/ConnectedCardReadersLoggingProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;",
            ">;)",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule_Companion_ProvideConnectedPeripheralsLoggingProvidersFactory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule_Companion_ProvideConnectedPeripheralsLoggingProvidersFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule_Companion_ProvideConnectedPeripheralsLoggingProvidersFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideConnectedPeripheralsLoggingProviders(Lcom/squareup/connectedperipherals/ConnectedCardReadersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/connectedperipherals/ConnectedCardReadersLoggingProvider;",
            "Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;",
            "Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;",
            "Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;",
            ">;"
        }
    .end annotation

    .line 55
    sget-object v0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule;->Companion:Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule$Companion;->provideConnectedPeripheralsLoggingProviders(Lcom/squareup/connectedperipherals/ConnectedCardReadersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;)Ljava/util/Set;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule_Companion_ProvideConnectedPeripheralsLoggingProvidersFactory;->get()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;",
            ">;"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule_Companion_ProvideConnectedPeripheralsLoggingProvidersFactory;->connectedCardReadersLoggingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/connectedperipherals/ConnectedCardReadersLoggingProvider;

    iget-object v1, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule_Companion_ProvideConnectedPeripheralsLoggingProvidersFactory;->cashDrawersLoggingProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;

    iget-object v2, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule_Companion_ProvideConnectedPeripheralsLoggingProvidersFactory;->connectedPrintersLoggingProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;

    iget-object v3, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule_Companion_ProvideConnectedPeripheralsLoggingProvidersFactory;->connectedBarcodeScannersLoggingProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule_Companion_ProvideConnectedPeripheralsLoggingProvidersFactory;->provideConnectedPeripheralsLoggingProviders(Lcom/squareup/connectedperipherals/ConnectedCardReadersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedCashDrawersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;Lcom/squareup/connectedperipherals/ConnectedBarcodeScannersLoggingProvider;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
