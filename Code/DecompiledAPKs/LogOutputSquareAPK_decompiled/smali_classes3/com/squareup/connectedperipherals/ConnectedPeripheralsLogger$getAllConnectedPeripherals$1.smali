.class final Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger$getAllConnectedPeripherals$1;
.super Ljava/lang/Object;
.source "ConnectedPeripheralsLogger.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->getAllConnectedPeripherals()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralData;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;


# direct methods
.method constructor <init>(Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger$getAllConnectedPeripherals$1;->this$0:Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger$getAllConnectedPeripherals$1;->call()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final call()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralData;",
            ">;"
        }
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger$getAllConnectedPeripherals$1;->this$0:Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;

    invoke-static {v0}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->access$mapProvidersToList(Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
