.class public final Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider_Factory;
.super Ljava/lang/Object;
.source "ConnectedPrintersLoggingProvider_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;",
        ">;"
    }
.end annotation


# instance fields
.field private final hardwarePrinterTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider_Factory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;)",
            "Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/print/HardwarePrinterTracker;)Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;

    invoke-direct {v0, p0}, Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;-><init>(Lcom/squareup/print/HardwarePrinterTracker;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider_Factory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/HardwarePrinterTracker;

    invoke-static {v0}, Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider_Factory;->newInstance(Lcom/squareup/print/HardwarePrinterTracker;)Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider_Factory;->get()Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;

    move-result-object v0

    return-object v0
.end method
