.class public final Lcom/squareup/locale/RealLocaleChangedNotifier;
.super Ljava/lang/Object;
.source "RealLocaleChangedNotifier.kt"

# interfaces
.implements Lcom/squareup/locale/LocaleChangedNotifier;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/AppScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001B\u0019\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\n\u001a\u00020\tH\u0002R\u001a\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/locale/RealLocaleChangedNotifier;",
        "Lcom/squareup/locale/LocaleChangedNotifier;",
        "context",
        "Landroid/app/Application;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Landroid/app/Application;Lio/reactivex/Scheduler;)V",
        "locale",
        "Lio/reactivex/Observable;",
        "Ljava/util/Locale;",
        "getLocale",
        "()Lio/reactivex/Observable;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final locale:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/locale/RealLocaleChangedNotifier;->mainScheduler:Lio/reactivex/Scheduler;

    .line 24
    check-cast p1, Landroid/content/Context;

    const-string p2, "android.intent.action.LOCALE_CHANGED"

    filled-new-array {p2}, [Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2ContextsKt;->registerForIntents(Landroid/content/Context;[Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    .line 25
    iget-object p2, p0, Lcom/squareup/locale/RealLocaleChangedNotifier;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 26
    new-instance p2, Lcom/squareup/locale/RealLocaleChangedNotifier$locale$1;

    invoke-direct {p2, p0}, Lcom/squareup/locale/RealLocaleChangedNotifier$locale$1;-><init>(Lcom/squareup/locale/RealLocaleChangedNotifier;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 27
    invoke-direct {p0}, Lcom/squareup/locale/RealLocaleChangedNotifier;->getLocale()Ljava/util/Locale;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "context.registerForInten\u2026  .startWith(getLocale())"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/locale/RealLocaleChangedNotifier;->locale:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getLocale(Lcom/squareup/locale/RealLocaleChangedNotifier;)Ljava/util/Locale;
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/locale/RealLocaleChangedNotifier;->getLocale()Ljava/util/Locale;

    move-result-object p0

    return-object p0
.end method

.method private final getLocale()Ljava/util/Locale;
    .locals 3

    .line 30
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const-string v1, "Resources.getSystem()"

    const/16 v2, 0x18

    if-lt v0, v2, :cond_0

    .line 31
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    const-string v1, "Resources.getSystem()\n          .configuration"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLocales()Landroid/os/LocaleList;

    move-result-object v0

    const/4 v1, 0x0

    .line 32
    invoke-virtual {v0, v1}, Landroid/os/LocaleList;->get(I)Ljava/util/Locale;

    move-result-object v0

    const-string v1, "Resources.getSystem()\n  \u2026figuration.locales.get(0)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 34
    :cond_0
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    const-string v1, "Resources.getSystem()\n  \u2026    .configuration.locale"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method


# virtual methods
.method public getLocale()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/locale/RealLocaleChangedNotifier;->locale:Lio/reactivex/Observable;

    return-object v0
.end method
