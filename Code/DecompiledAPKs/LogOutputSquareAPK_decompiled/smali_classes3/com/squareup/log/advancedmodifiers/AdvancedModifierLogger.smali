.class public Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;
.super Ljava/lang/Object;
.source "AdvancedModifierLogger.java"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cogs/Cogs;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 27
    iput-object p2, p0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->cogs:Lcom/squareup/cogs/Cogs;

    .line 28
    iput-object p3, p0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method static hasMaxModifierLimit(Ljava/util/Map;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/ItemModifierOverride;",
            ">;)Z"
        }
    .end annotation

    .line 53
    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/ItemModifierOverride;

    .line 54
    iget v0, v0, Lcom/squareup/shared/catalog/models/ItemModifierOverride;->maxSelectedModifiers:I

    if-lez v0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method static hasMinModifierRequirement(Ljava/util/Map;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/ItemModifierOverride;",
            ">;)Z"
        }
    .end annotation

    .line 43
    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/ItemModifierOverride;

    .line 44
    iget v0, v0, Lcom/squareup/shared/catalog/models/ItemModifierOverride;->minSelectedModifiers:I

    if-lez v0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method public static interestingItems(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 33
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 34
    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 35
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method


# virtual methods
.method allOverrides(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/List;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/checkout/CartItem;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/ItemModifierOverride;",
            ">;>;"
        }
    .end annotation

    .line 129
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 130
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 135
    iget-object v2, v1, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 136
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 137
    iget-object v3, v1, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    .line 138
    invoke-interface {p1, v3}, Lcom/squareup/shared/catalog/Catalog$Local;->findItemModifierMemberships(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 139
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    .line 140
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->isAdvancedModifierEnabled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 141
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->getModifierListId()Ljava/lang/String;

    move-result-object v5

    .line 142
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->getItemModifierOverride()Lcom/squareup/shared/catalog/models/ItemModifierOverride;

    move-result-object v4

    .line 141
    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 146
    :cond_2
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method doLogMinMaxModifiers(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/checkout/CartItem;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/ItemModifierOverride;",
            ">;>;)V"
        }
    .end annotation

    if-eqz p1, :cond_4

    .line 160
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 161
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 163
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    iget-object v2, v2, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-static {v2}, Lcom/squareup/util/BigDecimals;->asIntOr1(Ljava/math/BigDecimal;)I

    move-result v2

    .line 165
    invoke-static {v3}, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->hasMinModifierRequirement(Ljava/util/Map;)Z

    move-result v4

    if-eqz v4, :cond_1

    add-int/2addr v0, v2

    .line 168
    :cond_1
    invoke-static {v3}, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->hasMaxModifierLimit(Ljava/util/Map;)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/2addr v1, v2

    goto :goto_0

    :cond_2
    if-gtz v0, :cond_3

    if-lez v1, :cond_4

    .line 173
    :cond_3
    iget-object p1, p0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/log/advancedmodifiers/MinMaxModifierUsedEvent;

    invoke-direct {v2, v0, v1}, Lcom/squareup/log/advancedmodifiers/MinMaxModifierUsedEvent;-><init>(II)V

    invoke-interface {p1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_4
    return-void
.end method

.method doLogSkipModifierDetailScreen(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/checkout/CartItem;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    const/4 v0, 0x0

    .line 200
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 201
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    .line 203
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-static {v1}, Lcom/squareup/util/BigDecimals;->asIntOr1(Ljava/math/BigDecimal;)I

    move-result v1

    .line 205
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/2addr v0, v1

    goto :goto_0

    :cond_1
    if-lez v0, :cond_2

    .line 210
    iget-object p1, p0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/advancedmodifiers/SkipItemModalEvent;

    invoke-direct {v1, v0}, Lcom/squareup/log/advancedmodifiers/SkipItemModalEvent;-><init>(I)V

    invoke-interface {p1, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_2
    return-void
.end method

.method public synthetic lambda$logMinMaxModifiers$0$AdvancedModifierLogger(Ljava/util/List;Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/Map;
    .locals 0

    .line 96
    invoke-virtual {p0, p2, p1}, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->allOverrides(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/List;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$logMinMaxModifiers$1$AdvancedModifierLogger(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 97
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->doLogMinMaxModifiers(Ljava/util/Map;)V

    return-void
.end method

.method public synthetic lambda$logSkipModifierDetailScreen$2$AdvancedModifierLogger(Ljava/util/List;Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/Map;
    .locals 0

    .line 106
    invoke-virtual {p0, p2, p1}, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->skipMap(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/List;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$logSkipModifierDetailScreen$3$AdvancedModifierLogger(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 107
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->doLogSkipModifierDetailScreen(Ljava/util/Map;)V

    return-void
.end method

.method logHideModifiersFromCustomer(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)V"
        }
    .end annotation

    .line 112
    iget-object v0, p0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->isHideModifiersOnReceiptsEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 116
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 117
    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->hasHiddenModifier()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 119
    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-static {v1}, Lcom/squareup/util/BigDecimals;->asIntOr1(Ljava/math/BigDecimal;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    :cond_2
    if-lez v0, :cond_3

    .line 123
    iget-object p1, p0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/advancedmodifiers/HideModifierFromCustomerEvent;

    invoke-direct {v1, v0}, Lcom/squareup/log/advancedmodifiers/HideModifierFromCustomerEvent;-><init>(I)V

    invoke-interface {p1, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_3
    return-void
.end method

.method logMinMaxModifiers(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)V"
        }
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/log/advancedmodifiers/-$$Lambda$AdvancedModifierLogger$MKqL9mh7a3HwqUOc_604HjZyrs8;

    invoke-direct {v1, p0, p1}, Lcom/squareup/log/advancedmodifiers/-$$Lambda$AdvancedModifierLogger$MKqL9mh7a3HwqUOc_604HjZyrs8;-><init>(Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;Ljava/util/List;)V

    new-instance p1, Lcom/squareup/log/advancedmodifiers/-$$Lambda$AdvancedModifierLogger$VeXtdbjGNqmUBWxLT2wcJ2xtdjs;

    invoke-direct {p1, p0}, Lcom/squareup/log/advancedmodifiers/-$$Lambda$AdvancedModifierLogger$VeXtdbjGNqmUBWxLT2wcJ2xtdjs;-><init>(Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;)V

    invoke-interface {v0, v1, p1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public logModifiers(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)V"
        }
    .end annotation

    .line 68
    invoke-virtual {p0, p1}, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->logPreSelectedModifiers(Ljava/util/List;)V

    .line 69
    invoke-virtual {p0, p1}, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->logMinMaxModifiers(Ljava/util/List;)V

    .line 70
    invoke-virtual {p0, p1}, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->logSkipModifierDetailScreen(Ljava/util/List;)V

    .line 71
    invoke-virtual {p0, p1}, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->logHideModifiersFromCustomer(Ljava/util/List;)V

    return-void
.end method

.method logPreSelectedModifiers(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)V"
        }
    .end annotation

    .line 79
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 81
    iget-object v3, v2, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-static {v3}, Lcom/squareup/util/BigDecimals;->asIntOr1(Ljava/math/BigDecimal;)I

    move-result v3

    .line 82
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->preSelectedModifierCountForSelectedModifiers()I

    move-result v2

    mul-int v2, v2, v3

    if-lez v2, :cond_0

    add-int/2addr v0, v3

    add-int/2addr v1, v2

    goto :goto_0

    :cond_1
    if-lez v0, :cond_2

    .line 89
    iget-object p1, p0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/log/advancedmodifiers/PreSelectedModifierUsedEvent;

    invoke-direct {v2, v0, v1}, Lcom/squareup/log/advancedmodifiers/PreSelectedModifierUsedEvent;-><init>(II)V

    invoke-interface {p1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_2
    return-void
.end method

.method logSkipModifierDetailScreen(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)V"
        }
    .end annotation

    .line 102
    iget-object v0, p0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->isSkipModifierDetailScreenEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/log/advancedmodifiers/-$$Lambda$AdvancedModifierLogger$oLkhfkneSmLRA-rcAY25q3XfW8Y;

    invoke-direct {v1, p0, p1}, Lcom/squareup/log/advancedmodifiers/-$$Lambda$AdvancedModifierLogger$oLkhfkneSmLRA-rcAY25q3XfW8Y;-><init>(Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;Ljava/util/List;)V

    new-instance p1, Lcom/squareup/log/advancedmodifiers/-$$Lambda$AdvancedModifierLogger$H8ca9bjrd0COY4AbOOKPscpy0x4;

    invoke-direct {p1, p0}, Lcom/squareup/log/advancedmodifiers/-$$Lambda$AdvancedModifierLogger$H8ca9bjrd0COY4AbOOKPscpy0x4;-><init>(Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;)V

    invoke-interface {v0, v1, p1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method skipMap(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/List;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/checkout/CartItem;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 179
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 181
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 186
    iget-object v2, v1, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 187
    const-class v2, Lcom/squareup/shared/catalog/models/CatalogItem;

    iget-object v3, v1, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    invoke-interface {p1, v2, v3}, Lcom/squareup/shared/catalog/Catalog$Local;->findByIdOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItem;

    if-eqz v2, :cond_1

    .line 188
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItem;->skipsModifierScreen()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-object v0
.end method
