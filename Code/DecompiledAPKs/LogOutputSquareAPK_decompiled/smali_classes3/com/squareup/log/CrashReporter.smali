.class public interface abstract Lcom/squareup/log/CrashReporter;
.super Ljava/lang/Object;
.source "CrashReporter.java"


# virtual methods
.method public abstract crashThrowable(Ljava/lang/Throwable;)V
.end method

.method public abstract crashThrowableBlocking(Ljava/lang/Throwable;Ljava/lang/Thread;)V
.end method

.method public abstract crashnadoMiniDump(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract getLastEventsAsString()Ljava/lang/String;
.end method

.method public abstract log(Lcom/squareup/log/OhSnapEvent;)V
.end method

.method public abstract log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract logToDeviceTab(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract logToUserTab(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract setMortarScopeHierarchy(Ljava/lang/String;)V
.end method

.method public abstract setReleaseStage(Ljava/lang/String;)V
.end method

.method public abstract setUserIdentifier(Ljava/lang/String;)V
.end method

.method public abstract setViewHierarchy(Ljava/lang/String;)V
.end method

.method public abstract warningThrowable(Ljava/lang/Throwable;)V
.end method

.method public abstract warningThrowable(Ljava/lang/Throwable;Lcom/bugsnag/android/MetaData;)V
.end method
