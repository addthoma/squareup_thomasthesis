.class public Lcom/squareup/log/RealCheckoutInformationEventLogger;
.super Ljava/lang/Object;
.source "RealCheckoutInformationEventLogger.java"

# interfaces
.implements Lcom/squareup/log/CheckoutInformationEventLogger;


# static fields
.field private static final END_TIME_THRESHOLD:J = 0x1388L


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private builder:Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

.field private final clock:Lcom/squareup/util/Clock;

.field private final signatureSettings:Lcom/squareup/settings/server/SignatureSettings;

.field private final skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->clock:Lcom/squareup/util/Clock;

    .line 39
    iput-object p2, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    .line 40
    invoke-virtual {p3}, Lcom/squareup/settings/server/AccountStatusSettings;->getSignatureSettings()Lcom/squareup/settings/server/SignatureSettings;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->signatureSettings:Lcom/squareup/settings/server/SignatureSettings;

    .line 41
    iput-object p4, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private expectBuilder()Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->builder:Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    if-nez v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/squareup/log/RealCheckoutInformationEventLogger;->startCheckoutIfHasNot()V

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->builder:Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    return-object v0
.end method


# virtual methods
.method public cancelCheckout()V
    .locals 1

    const/4 v0, 0x0

    .line 120
    iput-object v0, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->builder:Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    return-void
.end method

.method public finishCheckout(Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;Ljava/lang/String;Z)V
    .locals 6

    .line 95
    iget-object v0, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->builder:Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    if-nez v0, :cond_0

    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    .line 100
    iget-object v2, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->builder:Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    invoke-virtual {v2}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->getCheckoutEndTime()J

    move-result-wide v2

    if-nez p3, :cond_1

    const-wide/16 v4, 0x1388

    add-long/2addr v2, v4

    cmp-long p3, v0, v2

    if-gez p3, :cond_2

    .line 106
    :cond_1
    iget-object p3, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->builder:Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    invoke-virtual {p3, v0, v1}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->setCheckoutEndTime(J)V

    .line 108
    :cond_2
    iget-object p3, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->builder:Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    invoke-virtual {p3, p2}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->setPaymentToken(Ljava/lang/String;)V

    .line 110
    iget-object p2, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->builder:Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    invoke-virtual {p2}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->isValid()Z

    move-result p2

    if-eqz p2, :cond_3

    .line 111
    iget-object p2, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object p3, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->builder:Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    invoke-virtual {p3, p1}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->build(Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;)Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_3
    const/4 p1, 0x0

    .line 113
    iput-object p1, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->builder:Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    return-void
.end method

.method public setEmailOnFile(Z)V
    .locals 1

    .line 81
    invoke-direct {p0}, Lcom/squareup/log/RealCheckoutInformationEventLogger;->expectBuilder()Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->setEmailOnFile(Z)V

    return-void
.end method

.method public showScreen()V
    .locals 1

    .line 76
    invoke-direct {p0}, Lcom/squareup/log/RealCheckoutInformationEventLogger;->expectBuilder()Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->incrementScreenCount()V

    .line 77
    invoke-virtual {p0}, Lcom/squareup/log/RealCheckoutInformationEventLogger;->updateTentativeEndTime()V

    return-void
.end method

.method public startCheckoutIfHasNot()V
    .locals 5

    .line 49
    iget-object v0, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->builder:Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    iget-object v1, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    .line 51
    invoke-interface {v3}, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;->skipReceiptScreenForFastCheckout()Z

    move-result v3

    iget-object v4, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->signatureSettings:Lcom/squareup/settings/server/SignatureSettings;

    .line 52
    invoke-virtual {v4}, Lcom/squareup/settings/server/SignatureSettings;->merchantOptedInToSkipSignaturesForSmallPayments()Z

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;-><init>(JZZ)V

    iput-object v0, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->builder:Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    :cond_0
    return-void
.end method

.method public tap()V
    .locals 1

    .line 69
    invoke-direct {p0}, Lcom/squareup/log/RealCheckoutInformationEventLogger;->expectBuilder()Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->incrementTap()V

    return-void
.end method

.method public updateTentativeEndTime()V
    .locals 3

    .line 90
    invoke-direct {p0}, Lcom/squareup/log/RealCheckoutInformationEventLogger;->expectBuilder()Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->setCheckoutEndTime(J)V

    return-void
.end method

.method public userProvideEmail(JLjava/lang/String;)V
    .locals 2

    .line 85
    invoke-direct {p0}, Lcom/squareup/log/RealCheckoutInformationEventLogger;->expectBuilder()Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->setEmailOnFile(Z)V

    .line 86
    iget-object v0, p0, Lcom/squareup/log/RealCheckoutInformationEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/fastcheckout/EmailProvidedByUserEvent;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/log/fastcheckout/EmailProvidedByUserEvent;-><init>(JLjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
