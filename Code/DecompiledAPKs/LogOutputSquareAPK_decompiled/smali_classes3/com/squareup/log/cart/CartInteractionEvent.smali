.class public abstract Lcom/squareup/log/cart/CartInteractionEvent;
.super Lcom/squareup/analytics/event/v1/TimingEvent;
.source "CartInteractionEvent.java"


# instance fields
.field private final destination:Ljava/lang/String;

.field private duration_ms:J

.field private final source:Ljava/lang/String;

.field private final tap:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterTimingName;Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterViewName;JLcom/squareup/analytics/RegisterTapName;)V
    .locals 2

    .line 23
    sget-wide v0, Lcom/squareup/log/cart/TransactionInteractionsLogger;->SAMPLING_RATE:D

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/analytics/event/v1/TimingEvent;-><init>(Lcom/squareup/analytics/RegisterTimingName;D)V

    const/4 p1, 0x0

    if-nez p2, :cond_0

    move-object p2, p1

    goto :goto_0

    .line 24
    :cond_0
    iget-object p2, p2, Lcom/squareup/analytics/RegisterViewName;->value:Ljava/lang/String;

    :goto_0
    iput-object p2, p0, Lcom/squareup/log/cart/CartInteractionEvent;->source:Ljava/lang/String;

    .line 25
    iget-object p2, p3, Lcom/squareup/analytics/RegisterViewName;->value:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/log/cart/CartInteractionEvent;->destination:Ljava/lang/String;

    .line 26
    iput-wide p4, p0, Lcom/squareup/log/cart/CartInteractionEvent;->duration_ms:J

    if-nez p6, :cond_1

    goto :goto_1

    .line 27
    :cond_1
    iget-object p1, p6, Lcom/squareup/analytics/RegisterTapName;->value:Ljava/lang/String;

    :goto_1
    iput-object p1, p0, Lcom/squareup/log/cart/CartInteractionEvent;->tap:Ljava/lang/String;

    return-void
.end method
