.class public final Lcom/squareup/log/RegisterExceptionHandler_Deps_Factory;
.super Ljava/lang/Object;
.source "RegisterExceptionHandler_Deps_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/log/RegisterExceptionHandler$Deps;",
        ">;"
    }
.end annotation


# instance fields
.field private final additionalLoggersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/log/CrashAdditionalLogger;",
            ">;>;"
        }
    .end annotation
.end field

.field private final anrChaperoneProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/anrchaperone/AnrChaperone;",
            ">;"
        }
    .end annotation
.end field

.field private final featureFlagsForLogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/FeatureFlagsForLogs;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/log/CrashAdditionalLogger;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/FeatureFlagsForLogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/anrchaperone/AnrChaperone;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/log/RegisterExceptionHandler_Deps_Factory;->additionalLoggersProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/log/RegisterExceptionHandler_Deps_Factory;->featureFlagsForLogsProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/log/RegisterExceptionHandler_Deps_Factory;->anrChaperoneProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/log/RegisterExceptionHandler_Deps_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/log/CrashAdditionalLogger;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/FeatureFlagsForLogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/anrchaperone/AnrChaperone;",
            ">;)",
            "Lcom/squareup/log/RegisterExceptionHandler_Deps_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/log/RegisterExceptionHandler_Deps_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/log/RegisterExceptionHandler_Deps_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljava/util/Set;Lcom/squareup/log/FeatureFlagsForLogs;Lcom/squareup/anrchaperone/AnrChaperone;)Lcom/squareup/log/RegisterExceptionHandler$Deps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/log/CrashAdditionalLogger;",
            ">;",
            "Lcom/squareup/log/FeatureFlagsForLogs;",
            "Lcom/squareup/anrchaperone/AnrChaperone;",
            ")",
            "Lcom/squareup/log/RegisterExceptionHandler$Deps;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/log/RegisterExceptionHandler$Deps;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/log/RegisterExceptionHandler$Deps;-><init>(Ljava/util/Set;Lcom/squareup/log/FeatureFlagsForLogs;Lcom/squareup/anrchaperone/AnrChaperone;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/log/RegisterExceptionHandler$Deps;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler_Deps_Factory;->additionalLoggersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler_Deps_Factory;->featureFlagsForLogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/log/FeatureFlagsForLogs;

    iget-object v2, p0, Lcom/squareup/log/RegisterExceptionHandler_Deps_Factory;->anrChaperoneProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-static {v0, v1, v2}, Lcom/squareup/log/RegisterExceptionHandler_Deps_Factory;->newInstance(Ljava/util/Set;Lcom/squareup/log/FeatureFlagsForLogs;Lcom/squareup/anrchaperone/AnrChaperone;)Lcom/squareup/log/RegisterExceptionHandler$Deps;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/log/RegisterExceptionHandler_Deps_Factory;->get()Lcom/squareup/log/RegisterExceptionHandler$Deps;

    move-result-object v0

    return-object v0
.end method
