.class public final Lcom/squareup/log/deposits/DebitVerificationEmailEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "DebitVerificationEmailEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B#\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0007R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/log/deposits/DebitVerificationEmailEvent;",
        "Lcom/squareup/eventstream/v2/AppEvent;",
        "deposits_debit_verification_email_description",
        "",
        "deposits_debit_verification_email_is_successful",
        "",
        "deposits_debit_verification_email_error",
        "(Ljava/lang/String;ZLjava/lang/String;)V",
        "feature-analytics_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final deposits_debit_verification_email_description:Ljava/lang/String;

.field private final deposits_debit_verification_email_error:Ljava/lang/String;

.field private final deposits_debit_verification_email_is_successful:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    const-string v0, "deposits_debit_verification_email_description"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deposits_debit_verification_email"

    .line 16
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/log/deposits/DebitVerificationEmailEvent;->deposits_debit_verification_email_description:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/squareup/log/deposits/DebitVerificationEmailEvent;->deposits_debit_verification_email_is_successful:Z

    iput-object p3, p0, Lcom/squareup/log/deposits/DebitVerificationEmailEvent;->deposits_debit_verification_email_error:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;ZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    const-string p1, "Settings Instant Deposit: Resend Email Attempt"

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 15
    check-cast p3, Ljava/lang/String;

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/log/deposits/DebitVerificationEmailEvent;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    return-void
.end method
