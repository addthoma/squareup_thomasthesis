.class public Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;
.super Ljava/lang/Object;
.source "RssiLoggingHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/RssiLoggingHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RssiMeasurementStatistics"
.end annotation


# instance fields
.field private count:I

.field private max:I

.field private final mean:D

.field private min:I

.field private final stdDev:D

.field private sum:I

.field private final variance:D


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;",
            ">;)V"
        }
    .end annotation

    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 188
    iput v0, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->min:I

    const/16 v1, -0x64

    .line 189
    iput v1, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->max:I

    .line 190
    iput v0, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->count:I

    .line 191
    iput v0, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->sum:I

    .line 194
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;

    if-eqz v2, :cond_0

    .line 196
    invoke-static {v2}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;->access$000(Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;)I

    move-result v3

    iget v4, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->min:I

    if-ge v3, v4, :cond_1

    .line 197
    invoke-static {v2}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;->access$000(Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;)I

    move-result v3

    iput v3, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->min:I

    .line 199
    :cond_1
    invoke-static {v2}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;->access$000(Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;)I

    move-result v3

    iget v4, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->max:I

    if-le v3, v4, :cond_2

    .line 200
    invoke-static {v2}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;->access$000(Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;)I

    move-result v3

    iput v3, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->max:I

    .line 202
    :cond_2
    iget v3, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->sum:I

    invoke-static {v2}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;->access$000(Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;)I

    move-result v2

    add-int/2addr v3, v2

    iput v3, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->sum:I

    .line 203
    iget v2, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->count:I

    goto :goto_0

    .line 207
    :cond_3
    iget v1, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->sum:I

    iget v2, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->count:I

    div-int/2addr v1, v2

    int-to-double v1, v1

    iput-wide v1, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->mean:D

    .line 211
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;

    if-eqz v1, :cond_4

    int-to-double v2, v0

    .line 213
    invoke-static {v1}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;->access$000(Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;)I

    move-result v0

    int-to-double v4, v0

    iget-wide v6, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->mean:D

    sub-double/2addr v4, v6

    invoke-static {v1}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;->access$000(Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;)I

    move-result v0

    int-to-double v0, v0

    iget-wide v6, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->mean:D

    sub-double/2addr v0, v6

    mul-double v4, v4, v0

    add-double/2addr v2, v4

    double-to-int v0, v2

    goto :goto_1

    .line 216
    :cond_5
    iget p1, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->count:I

    div-int/2addr v0, p1

    int-to-double v0, v0

    iput-wide v0, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->variance:D

    .line 218
    iget-wide v0, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->variance:D

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->stdDev:D

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 222
    iget v0, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->count:I

    return v0
.end method

.method public getMax()I
    .locals 1

    .line 230
    iget v0, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->max:I

    return v0
.end method

.method public getMean()D
    .locals 2

    .line 234
    iget-wide v0, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->mean:D

    return-wide v0
.end method

.method public getMin()I
    .locals 1

    .line 226
    iget v0, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->min:I

    return v0
.end method

.method public getStdDev()D
    .locals 2

    .line 242
    iget-wide v0, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->stdDev:D

    return-wide v0
.end method

.method public getVariance()D
    .locals 2

    .line 238
    iget-wide v0, p0, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->variance:D

    return-wide v0
.end method
