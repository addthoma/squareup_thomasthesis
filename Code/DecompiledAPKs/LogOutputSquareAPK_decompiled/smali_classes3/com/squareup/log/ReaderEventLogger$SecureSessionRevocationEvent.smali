.class public Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent;
.super Lcom/squareup/log/ReaderEvent;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SecureSessionRevocationEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;
    }
.end annotation


# instance fields
.field public final description:Ljava/lang/String;

.field public final title:Ljava/lang/String;

.field public final uxHintString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;)V
    .locals 1

    .line 1273
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEvent;-><init>(Lcom/squareup/log/ReaderEvent$Builder;)V

    .line 1274
    invoke-static {p1}, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;->access$500(Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent;->title:Ljava/lang/String;

    .line 1275
    invoke-static {p1}, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;->access$600(Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent;->description:Ljava/lang/String;

    .line 1276
    invoke-static {p1}, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;->access$700(Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent$Builder;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$SecureSessionRevocationEvent;->uxHintString:Ljava/lang/String;

    return-void
.end method
