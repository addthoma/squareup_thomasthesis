.class public Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent;
.super Lcom/squareup/log/ReaderEvent;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CommsProtocolVersionReaderEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;
    }
.end annotation


# instance fields
.field public final commsVersionResult:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

.field public final readerProtocolVersion:Ljava/lang/String;

.field public final registerProtocolVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;)V
    .locals 1

    .line 1369
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEvent;-><init>(Lcom/squareup/log/ReaderEvent$Builder;)V

    .line 1370
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;->readerProtocolVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent;->readerProtocolVersion:Ljava/lang/String;

    .line 1371
    iget-object v0, p1, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;->registerProtocolVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent;->registerProtocolVersion:Ljava/lang/String;

    .line 1372
    iget-object p1, p1, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent$Builder;->commsVersionResult:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$CommsProtocolVersionReaderEvent;->commsVersionResult:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    return-void
.end method
