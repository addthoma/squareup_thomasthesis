.class public Lcom/squareup/log/AppUpgradeDetector;
.super Ljava/lang/Object;
.source "AppUpgradeDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;
    }
.end annotation


# static fields
.field private static final ALL_VERSION_NAMES_KEY:Ljava/lang/String; = "app_all_version_names"

.field private static final PREF_NAME:Ljava/lang/String; = "AppUpgradeDetector"

.field private static final UPGRADE_INFO_TAB:Ljava/lang/String; = "UPGRADE INFO"

.field private static final VERSION_CODE_KEY:Ljava/lang/String; = "app_version_code"

.field private static final VERSION_NAME_KEY:Ljava/lang/String; = "app_version_name"


# instance fields
.field private allVersionNames:Ljava/lang/String;

.field private appPackageInfo:Landroid/content/pm/PackageInfo;

.field private final application:Landroid/app/Application;

.field private final executorService:Ljava/util/concurrent/ExecutorService;

.field private upgradeInfo:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/log/AppUpgradeDetector;->application:Landroid/app/Application;

    const-string p1, "app-upgrade-detector"

    .line 49
    invoke-static {p1}, Lcom/squareup/thread/executor/Executors;->newSingleThreadExecutor(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/AppUpgradeDetector;->executorService:Ljava/util/concurrent/ExecutorService;

    .line 50
    invoke-direct {p0}, Lcom/squareup/log/AppUpgradeDetector;->init()V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/log/AppUpgradeDetector;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/log/AppUpgradeDetector;->readAndUpdate()V

    return-void
.end method

.method private getAppPackageInfo()Landroid/content/pm/PackageInfo;
    .locals 3

    .line 120
    :try_start_0
    iget-object v0, p0, Lcom/squareup/log/AppUpgradeDetector;->application:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/log/AppUpgradeDetector;->application:Landroid/app/Application;

    invoke-virtual {v1}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 122
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private init()V
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/log/AppUpgradeDetector;->executorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/log/AppUpgradeDetector$1;

    invoke-direct {v1, p0}, Lcom/squareup/log/AppUpgradeDetector$1;-><init>(Lcom/squareup/log/AppUpgradeDetector;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private declared-synchronized readAndUpdate()V
    .locals 6

    monitor-enter p0

    .line 81
    :try_start_0
    iget-object v0, p0, Lcom/squareup/log/AppUpgradeDetector;->upgradeInfo:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 82
    monitor-exit p0

    return-void

    .line 85
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/squareup/log/AppUpgradeDetector;->getAppPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/log/AppUpgradeDetector;->appPackageInfo:Landroid/content/pm/PackageInfo;

    .line 88
    iget-object v0, p0, Lcom/squareup/log/AppUpgradeDetector;->application:Landroid/app/Application;

    const-string v1, "AppUpgradeDetector"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Application;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "app_version_code"

    .line 91
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 92
    iget-object v1, p0, Lcom/squareup/log/AppUpgradeDetector;->appPackageInfo:Landroid/content/pm/PackageInfo;

    iget-wide v1, v1, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    iget-object v3, p0, Lcom/squareup/log/AppUpgradeDetector;->appPackageInfo:Landroid/content/pm/PackageInfo;

    iget-wide v3, v3, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_1

    .line 95
    sget-object v1, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;->FIRST_START_AFTER_UPGRADE:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    iput-object v1, p0, Lcom/squareup/log/AppUpgradeDetector;->upgradeInfo:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    goto :goto_0

    .line 97
    :cond_1
    sget-object v1, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;->FIRST_START_AFTER_FRESH_INSTALL:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    iput-object v1, p0, Lcom/squareup/log/AppUpgradeDetector;->upgradeInfo:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    .line 99
    :goto_0
    iget-object v1, p0, Lcom/squareup/log/AppUpgradeDetector;->appPackageInfo:Landroid/content/pm/PackageInfo;

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v1, p0, Lcom/squareup/log/AppUpgradeDetector;->allVersionNames:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const-string v1, "app_version_code"

    const/4 v2, -0x1

    .line 101
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "app_all_version_names"

    .line 102
    iget-object v3, p0, Lcom/squareup/log/AppUpgradeDetector;->appPackageInfo:Landroid/content/pm/PackageInfo;

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/log/AppUpgradeDetector;->allVersionNames:Ljava/lang/String;

    .line 103
    iget-object v2, p0, Lcom/squareup/log/AppUpgradeDetector;->appPackageInfo:Landroid/content/pm/PackageInfo;

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    if-eq v1, v2, :cond_3

    .line 104
    sget-object v1, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;->FIRST_START_AFTER_UPGRADE:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    iput-object v1, p0, Lcom/squareup/log/AppUpgradeDetector;->upgradeInfo:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    .line 105
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/squareup/log/AppUpgradeDetector;->appPackageInfo:Landroid/content/pm/PackageInfo;

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/log/AppUpgradeDetector;->allVersionNames:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/log/AppUpgradeDetector;->allVersionNames:Ljava/lang/String;

    goto :goto_1

    .line 107
    :cond_3
    sget-object v1, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;->NORMAL_START:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    iput-object v1, p0, Lcom/squareup/log/AppUpgradeDetector;->upgradeInfo:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    .line 111
    :goto_1
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "app_version_code"

    iget-object v2, p0, Lcom/squareup/log/AppUpgradeDetector;->appPackageInfo:Landroid/content/pm/PackageInfo;

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 112
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "app_version_name"

    iget-object v2, p0, Lcom/squareup/log/AppUpgradeDetector;->appPackageInfo:Landroid/content/pm/PackageInfo;

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 113
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "app_all_version_names"

    iget-object v2, p0, Lcom/squareup/log/AppUpgradeDetector;->allVersionNames:Ljava/lang/String;

    .line 114
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 115
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public logAppInfo(Lcom/squareup/log/CrashReporter;)V
    .locals 4

    .line 62
    invoke-direct {p0}, Lcom/squareup/log/AppUpgradeDetector;->readAndUpdate()V

    .line 63
    iget-object v0, p0, Lcom/squareup/log/AppUpgradeDetector;->upgradeInfo:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    invoke-virtual {v0}, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;->name()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UPGRADE INFO"

    const-string v2, "Upgrade Info"

    invoke-interface {p1, v1, v2, v0}, Lcom/squareup/log/CrashReporter;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    new-instance v0, Ljava/util/Date;

    iget-object v2, p0, Lcom/squareup/log/AppUpgradeDetector;->appPackageInfo:Landroid/content/pm/PackageInfo;

    iget-wide v2, v2, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 65
    invoke-static {v0}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "First Install Time"

    .line 64
    invoke-interface {p1, v1, v2, v0}, Lcom/squareup/log/CrashReporter;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    new-instance v0, Ljava/util/Date;

    iget-object v2, p0, Lcom/squareup/log/AppUpgradeDetector;->appPackageInfo:Landroid/content/pm/PackageInfo;

    iget-wide v2, v2, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 67
    invoke-static {v0}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "Last Update Time"

    .line 66
    invoke-interface {p1, v1, v2, v0}, Lcom/squareup/log/CrashReporter;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/log/AppUpgradeDetector;->upgradeInfo:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    sget-object v2, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;->FIRST_START_AFTER_FRESH_INSTALL:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    if-eq v0, v2, :cond_0

    .line 69
    iget-object v0, p0, Lcom/squareup/log/AppUpgradeDetector;->allVersionNames:Ljava/lang/String;

    const-string v2, "All version names"

    invoke-interface {p1, v1, v2, v0}, Lcom/squareup/log/CrashReporter;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public shutdown()V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/log/AppUpgradeDetector;->executorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    return-void
.end method
