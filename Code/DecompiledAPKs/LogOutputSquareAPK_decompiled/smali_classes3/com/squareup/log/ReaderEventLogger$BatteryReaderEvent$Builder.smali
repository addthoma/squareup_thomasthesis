.class public Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;
.super Lcom/squareup/log/ReaderEvent$Builder;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field public percentage:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1126
    invoke-direct {p0}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    const/4 v0, 0x0

    .line 1127
    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;->percentage(I)Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;

    return-void
.end method


# virtual methods
.method public bridge synthetic buildReaderEvent()Lcom/squareup/log/ReaderEvent;
    .locals 1

    .line 1123
    invoke-virtual {p0}, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;->buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent;

    move-result-object v0

    return-object v0
.end method

.method public buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent;
    .locals 1

    .line 1136
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent;

    invoke-direct {v0, p0}, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent;-><init>(Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;)V

    return-object v0
.end method

.method public percentage(I)Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;
    .locals 0

    .line 1131
    iput p1, p0, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;->percentage:I

    return-object p0
.end method
