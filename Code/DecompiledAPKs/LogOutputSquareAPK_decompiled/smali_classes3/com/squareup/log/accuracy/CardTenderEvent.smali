.class public Lcom/squareup/log/accuracy/CardTenderEvent;
.super Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;
.source "CardTenderEvent.java"


# instance fields
.field public final _card_brand:Ljava/lang/String;

.field public final _card_number_last_four:Ljava/lang/String;

.field public final _has_sent_add_tender:Z

.field public final _is_emv_capable_reader_present:Z

.field public final _payment_id:Ljava/lang/String;

.field public final _reader_firmware_version:Ljava/lang/String;

.field public final _reader_hardware_serial_number:Ljava/lang/String;

.field public final _reader_type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/BaseCardTender;Lcom/squareup/payment/BillPayment;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/payment/BillPayment;)V

    const/4 p1, 0x0

    .line 24
    iput-boolean p1, p0, Lcom/squareup/log/accuracy/CardTenderEvent;->_has_sent_add_tender:Z

    .line 25
    iget-object p1, p3, Lcom/squareup/payment/tender/BaseCardTender;->clientId:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/log/accuracy/CardTenderEvent;->_payment_id:Ljava/lang/String;

    .line 27
    invoke-direct {p0, p3}, Lcom/squareup/log/accuracy/CardTenderEvent;->getCard(Lcom/squareup/payment/tender/BaseCardTender;)Lcom/squareup/Card;

    move-result-object p1

    const/4 p2, 0x0

    if-nez p1, :cond_0

    .line 29
    iput-object p2, p0, Lcom/squareup/log/accuracy/CardTenderEvent;->_card_brand:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/squareup/log/accuracy/CardTenderEvent;->_card_number_last_four:Ljava/lang/String;

    goto :goto_0

    .line 33
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object p4

    invoke-virtual {p4}, Lcom/squareup/Card$Brand;->getHumanName()Ljava/lang/String;

    move-result-object p4

    iput-object p4, p0, Lcom/squareup/log/accuracy/CardTenderEvent;->_card_brand:Ljava/lang/String;

    .line 34
    invoke-virtual {p1}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/accuracy/CardTenderEvent;->_card_number_last_four:Ljava/lang/String;

    .line 36
    :goto_0
    invoke-virtual {p3}, Lcom/squareup/payment/tender/BaseCardTender;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    if-nez p1, :cond_1

    .line 37
    invoke-virtual {p3}, Lcom/squareup/payment/tender/BaseCardTender;->isEmvCapableReaderPresent()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/log/accuracy/CardTenderEvent;->_is_emv_capable_reader_present:Z

    .line 38
    iput-object p2, p0, Lcom/squareup/log/accuracy/CardTenderEvent;->_reader_firmware_version:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/squareup/log/accuracy/CardTenderEvent;->_reader_hardware_serial_number:Ljava/lang/String;

    const-string p1, "CNP"

    .line 40
    iput-object p1, p0, Lcom/squareup/log/accuracy/CardTenderEvent;->_reader_type:Ljava/lang/String;

    goto :goto_1

    :cond_1
    const/4 p1, 0x1

    .line 42
    iput-boolean p1, p0, Lcom/squareup/log/accuracy/CardTenderEvent;->_is_emv_capable_reader_present:Z

    .line 43
    invoke-virtual {p3}, Lcom/squareup/payment/tender/BaseCardTender;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getFirmwareVersion()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/accuracy/CardTenderEvent;->_reader_firmware_version:Ljava/lang/String;

    .line 44
    invoke-virtual {p3}, Lcom/squareup/payment/tender/BaseCardTender;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getHardwareSerialNumber()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/accuracy/CardTenderEvent;->_reader_hardware_serial_number:Ljava/lang/String;

    .line 45
    invoke-virtual {p3}, Lcom/squareup/payment/tender/BaseCardTender;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/accuracy/CardTenderEvent;->_reader_type:Ljava/lang/String;

    :goto_1
    return-void
.end method

.method private getCard(Lcom/squareup/payment/tender/BaseCardTender;)Lcom/squareup/Card;
    .locals 1

    .line 54
    instance-of v0, p1, Lcom/squareup/payment/tender/SmartCardTender;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/squareup/payment/tender/SmartCardTender;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTender;->hasResponseCardTender()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 57
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseCardTender;->getCard()Lcom/squareup/Card;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method protected paymentType()Ljava/lang/String;
    .locals 1

    const-string v0, "card"

    return-object v0
.end method
