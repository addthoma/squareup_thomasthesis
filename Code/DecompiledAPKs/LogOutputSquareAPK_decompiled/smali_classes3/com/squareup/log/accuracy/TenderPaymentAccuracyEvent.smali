.class public abstract Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;
.super Lcom/squareup/log/accuracy/PaymentAccuracyEvent;
.source "TenderPaymentAccuracyEvent.java"


# instance fields
.field public final _client_id:Ljava/lang/String;

.field public final _currency:Lcom/squareup/protos/common/CurrencyCode;

.field public final _is_delayed_captured:Z

.field public final _is_split_tender:Z

.field public final _payment_type:Ljava/lang/String;

.field public final _total_amount:Ljava/lang/Long;

.field public final contains_unit_priced_item:Z


# direct methods
.method public constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/payment/BillPayment;)V
    .locals 9

    .line 29
    invoke-virtual {p3}, Lcom/squareup/payment/tender/BaseTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-virtual {p4}, Lcom/squareup/payment/BillPayment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v4

    .line 30
    invoke-virtual {p4}, Lcom/squareup/payment/BillPayment;->isSingleTender()Z

    move-result v0

    xor-int/lit8 v5, v0, 0x1

    iget-object v6, p3, Lcom/squareup/payment/tender/BaseTender;->clientId:Ljava/lang/String;

    invoke-static {p3}, Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;->isTenderDelayedCapture(Lcom/squareup/payment/tender/BaseTender;)Z

    move-result v7

    .line 31
    invoke-virtual {p4}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/payment/OrderSnapshot;->hasAtLeastOneUnitPricedItem()Z

    move-result v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 29
    invoke-direct/range {v0 .. v8}, Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/IdPair;ZLjava/lang/String;ZZ)V

    return-void
.end method

.method private constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/IdPair;ZLjava/lang/String;ZZ)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2, p4}, Lcom/squareup/log/accuracy/PaymentAccuracyEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/protos/client/IdPair;)V

    .line 18
    invoke-virtual {p0}, Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;->paymentType()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;->_payment_type:Ljava/lang/String;

    .line 39
    invoke-static {p3}, Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;->optionalMoney(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 40
    iget-object p2, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p2, p0, Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;->_currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 41
    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    iput-object p1, p0, Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;->_total_amount:Ljava/lang/Long;

    .line 42
    iput-boolean p5, p0, Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;->_is_split_tender:Z

    .line 43
    iput-object p6, p0, Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;->_client_id:Ljava/lang/String;

    .line 44
    iput-boolean p7, p0, Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;->_is_delayed_captured:Z

    .line 45
    iput-boolean p8, p0, Lcom/squareup/log/accuracy/TenderPaymentAccuracyEvent;->contains_unit_priced_item:Z

    return-void
.end method

.method private static isTenderDelayedCapture(Lcom/squareup/payment/tender/BaseTender;)Z
    .locals 1

    .line 63
    instance-of v0, p0, Lcom/squareup/payment/AcceptsTips;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return p0

    .line 67
    :cond_0
    check-cast p0, Lcom/squareup/payment/AcceptsTips;

    .line 68
    invoke-interface {p0}, Lcom/squareup/payment/AcceptsTips;->tipOnPrintedReceipt()Z

    move-result p0

    return p0
.end method

.method protected static optionalMoney(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 0

    if-nez p0, :cond_0

    .line 58
    new-instance p0, Lcom/squareup/protos/common/Money$Builder;

    invoke-direct {p0}, Lcom/squareup/protos/common/Money$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/protos/common/Money$Builder;->build()Lcom/squareup/protos/common/Money;

    move-result-object p0

    :cond_0
    return-object p0
.end method


# virtual methods
.method protected abstract paymentType()Ljava/lang/String;
.end method
