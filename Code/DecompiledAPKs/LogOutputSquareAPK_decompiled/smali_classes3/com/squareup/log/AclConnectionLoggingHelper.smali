.class public Lcom/squareup/log/AclConnectionLoggingHelper;
.super Ljava/lang/Object;
.source "AclConnectionLoggingHelper.java"


# instance fields
.field private macAddressesSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/log/AclConnectionLoggingHelper;->macAddressesSet:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public addConnectedDevices(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/log/AclConnectionLoggingHelper;->macAddressesSet:Ljava/util/Set;

    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public connectedDevicesCount(Ljava/lang/String;)I
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/log/AclConnectionLoggingHelper;->macAddressesSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 32
    iget-object v1, p0, Lcom/squareup/log/AclConnectionLoggingHelper;->macAddressesSet:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    add-int/lit8 v0, v0, -0x1

    :cond_0
    return v0
.end method

.method public removeConnectedDevices(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/log/AclConnectionLoggingHelper;->macAddressesSet:Ljava/util/Set;

    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method
