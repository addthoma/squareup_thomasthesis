.class public final Lcom/squareup/log/FeatureFlagsForLogs_Factory;
.super Ljava/lang/Object;
.source "FeatureFlagsForLogs_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/log/FeatureFlagsForLogs;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final userIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/log/FeatureFlagsForLogs_Factory;->userIdProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/log/FeatureFlagsForLogs_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/log/FeatureFlagsForLogs_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/log/FeatureFlagsForLogs_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/log/FeatureFlagsForLogs_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/log/FeatureFlagsForLogs_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;)Lcom/squareup/log/FeatureFlagsForLogs;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/log/FeatureFlagsForLogs;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/log/FeatureFlagsForLogs;

    invoke-direct {v0, p0, p1}, Lcom/squareup/log/FeatureFlagsForLogs;-><init>(Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/log/FeatureFlagsForLogs;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/log/FeatureFlagsForLogs_Factory;->userIdProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/log/FeatureFlagsForLogs_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1}, Lcom/squareup/log/FeatureFlagsForLogs_Factory;->newInstance(Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;)Lcom/squareup/log/FeatureFlagsForLogs;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/log/FeatureFlagsForLogs_Factory;->get()Lcom/squareup/log/FeatureFlagsForLogs;

    move-result-object v0

    return-object v0
.end method
