.class public Lcom/squareup/log/tickets/ListResponseToTicketList;
.super Lcom/squareup/analytics/event/v1/TimingEvent;
.source "ListResponseToTicketList.java"


# instance fields
.field private final duration_ms:I

.field private final number_of_tickets_in_response:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/analytics/RegisterTimingName;->OPEN_TICKETS_LIST_RESPONSE_TO_LIST_UI:Lcom/squareup/analytics/RegisterTimingName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/TimingEvent;-><init>(Lcom/squareup/analytics/RegisterTimingName;)V

    .line 14
    iput p1, p0, Lcom/squareup/log/tickets/ListResponseToTicketList;->duration_ms:I

    .line 15
    iput p2, p0, Lcom/squareup/log/tickets/ListResponseToTicketList;->number_of_tickets_in_response:I

    return-void
.end method
