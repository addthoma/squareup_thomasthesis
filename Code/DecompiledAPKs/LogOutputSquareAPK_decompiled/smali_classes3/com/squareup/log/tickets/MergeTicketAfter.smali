.class public Lcom/squareup/log/tickets/MergeTicketAfter;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "MergeTicketAfter.java"


# instance fields
.field private final merged_transaction_client_id:Ljava/lang/String;

.field private final merged_transaction_total_amount:Ljava/lang/String;

.field private final number_of_items_in_merged_transaction:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_MERGE_TICKETS_AFTER:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 16
    iput p1, p0, Lcom/squareup/log/tickets/MergeTicketAfter;->number_of_items_in_merged_transaction:I

    .line 17
    iput-object p2, p0, Lcom/squareup/log/tickets/MergeTicketAfter;->merged_transaction_client_id:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/squareup/log/tickets/MergeTicketAfter;->merged_transaction_total_amount:Ljava/lang/String;

    return-void
.end method
