.class public abstract enum Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;
.super Ljava/lang/Enum;
.source "OpenTicketsLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/tickets/OpenTicketsLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4409
    name = "ExitEditTicketResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

.field public static final enum BOTH:Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

.field public static final enum CANCEL:Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

.field public static final enum NAME:Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

.field public static final enum NOTE:Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 23
    new-instance v0, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult$1;

    const/4 v1, 0x0

    const-string v2, "NAME"

    invoke-direct {v0, v2, v1}, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;->NAME:Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    .line 27
    new-instance v0, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult$2;

    const/4 v2, 0x1

    const-string v3, "NOTE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;->NOTE:Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    .line 31
    new-instance v0, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult$3;

    const/4 v3, 0x2

    const-string v4, "BOTH"

    invoke-direct {v0, v4, v3}, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;->BOTH:Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    .line 35
    new-instance v0, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult$4;

    const/4 v4, 0x3

    const-string v5, "CANCEL"

    invoke-direct {v0, v5, v4}, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult$4;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;->CANCEL:Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    .line 22
    sget-object v5, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;->NAME:Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;->NOTE:Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;->BOTH:Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;->CANCEL:Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;->$VALUES:[Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/squareup/log/tickets/OpenTicketsLogger$1;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;
    .locals 1

    .line 22
    const-class v0, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;
    .locals 1

    .line 22
    sget-object v0, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;->$VALUES:[Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    invoke-virtual {v0}, [Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    return-object v0
.end method


# virtual methods
.method abstract stringValue()Ljava/lang/String;
.end method
