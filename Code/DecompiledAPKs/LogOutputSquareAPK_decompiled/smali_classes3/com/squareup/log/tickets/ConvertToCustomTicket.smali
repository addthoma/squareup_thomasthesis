.class public Lcom/squareup/log/tickets/ConvertToCustomTicket;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "ConvertToCustomTicket.java"


# instance fields
.field public final belongs_to_ticket_group:Z

.field public final moved_to_another_group:Z


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V
    .locals 3

    .line 16
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PREDEFINED_TICKET_CONVERT_TO_CUSTOM:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 17
    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    if-eqz p2, :cond_1

    .line 18
    iget-object v2, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    iput-boolean v2, p0, Lcom/squareup/log/tickets/ConvertToCustomTicket;->belongs_to_ticket_group:Z

    .line 19
    iget-boolean v2, p0, Lcom/squareup/log/tickets/ConvertToCustomTicket;->belongs_to_ticket_group:Z

    if-eqz v2, :cond_3

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    iget-object p1, p1, Lcom/squareup/api/items/TicketGroup;->id:Ljava/lang/String;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    iget-object p2, p2, Lcom/squareup/api/items/TicketGroup;->id:Ljava/lang/String;

    .line 20
    invoke-static {p1, p2}, Lcom/squareup/util/Objects;->eq(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :cond_3
    :goto_2
    iput-boolean v0, p0, Lcom/squareup/log/tickets/ConvertToCustomTicket;->moved_to_another_group:Z

    return-void
.end method
