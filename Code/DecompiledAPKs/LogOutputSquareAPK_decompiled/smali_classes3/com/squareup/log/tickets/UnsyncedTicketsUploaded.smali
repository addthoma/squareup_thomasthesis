.class public Lcom/squareup/log/tickets/UnsyncedTicketsUploaded;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "UnsyncedTicketsUploaded.java"


# instance fields
.field private final number_of_unsynced_tickets:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_UNSYNCED_TICKETS_UPLOADED_VIA_SWEEPER:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 13
    iput p1, p0, Lcom/squareup/log/tickets/UnsyncedTicketsUploaded;->number_of_unsynced_tickets:I

    return-void
.end method
