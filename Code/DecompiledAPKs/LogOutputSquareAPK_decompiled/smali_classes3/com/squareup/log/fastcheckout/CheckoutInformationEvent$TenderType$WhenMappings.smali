.class public final synthetic Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 4

    invoke-static {}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->values()[Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->CARD:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    invoke-virtual {v1}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->CARD_NP:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    invoke-virtual {v1}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->values()[Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->CARD:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    invoke-virtual {v1}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->CARD_NP:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    invoke-virtual {v1}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->CASH:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    invoke-virtual {v1}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->INVOICE:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    invoke-virtual {v1}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->NO_SALE:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    invoke-virtual {v1}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ON_FILE:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    invoke-virtual {v1}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->OTHER:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    invoke-virtual {v1}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->SPLIT:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    invoke-virtual {v1}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ZERO_AMOUNT:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    invoke-virtual {v1}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->EMONEY:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    invoke-virtual {v1}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1

    return-void
.end method
