.class public Lcom/squareup/log/fastcheckout/CollectTipsToggleEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "CollectTipsToggleEvent.java"


# static fields
.field private static final OFF:Ljava/lang/String; = "OFF"

.field private static final ON:Ljava/lang/String; = "ON"


# instance fields
.field private final detail:Ljava/lang/String;


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->TIP_SETTINGS_COLLECT_TIPS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    if-eqz p1, :cond_0

    const-string p1, "ON"

    goto :goto_0

    :cond_0
    const-string p1, "OFF"

    .line 15
    :goto_0
    iput-object p1, p0, Lcom/squareup/log/fastcheckout/CollectTipsToggleEvent;->detail:Ljava/lang/String;

    return-void
.end method
