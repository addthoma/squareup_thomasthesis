.class public final enum Lcom/squareup/log/OhSnapLogger$EventType;
.super Ljava/lang/Enum;
.source "OhSnapLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/OhSnapLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EventType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/log/OhSnapLogger$EventType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum API:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum APOS:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum AUTO_CAPTURE:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum AUTO_VOID:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum BACKGROUND_JOB:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum BLE:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum BUYER_DEVICE:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum CAMERA:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum CAPTURE_FAILURE:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum CORRUPT_QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum CRASH_REPORTER:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum CREATE:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum CREATE_VIEW:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum DESTROY:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum DESTROY_SCOPE:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum FINISH:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum HANDLE_INTENT:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum LOGGED_EXCEPTION:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum LOG_OUT:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum MINESWEEPER:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum NATIVE_LIBRARY:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum ON_SAVE:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum OTTO_EVENT:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum PAUSE:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum READER:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum RESUME:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum RETAIN_NONCONFIG:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum SAMSUNG_CRASH:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum SERVER_CALL:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum SHOW_SCREEN:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum SSO:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum START_ACTIVITY:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum WARNING:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum X2_BRAN:Lcom/squareup/log/OhSnapLogger$EventType;

.field public static final enum X2_RPC:Lcom/squareup/log/OhSnapLogger$EventType;


# instance fields
.field public final color:I

.field public final prettyName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 7
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v1, 0x0

    const-string v2, "API"

    const-string v3, "api"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->API:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 8
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v2, 0x1

    const-string v3, "APOS"

    const-string v4, "apos"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->APOS:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 9
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v3, 0x2

    const-string v4, "AUTO_CAPTURE"

    const-string v5, "auto cap"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->AUTO_CAPTURE:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 10
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v4, 0x3

    const-string v5, "AUTO_VOID"

    const-string v6, "auto void"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->AUTO_VOID:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 11
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v5, 0x4

    const-string v6, "BACKGROUND_JOB"

    const-string v7, "background job"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->BACKGROUND_JOB:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 12
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v6, 0x5

    const-string v7, "BLE"

    const-string v8, "ble"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->BLE:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 13
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v7, 0x6

    const-string v8, "BUYER_DEVICE"

    const-string v9, "buyer_device"

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->BUYER_DEVICE:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 14
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v8, 0x7

    const-string v9, "CAMERA"

    const-string v10, "camera"

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->CAMERA:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 15
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v9, 0x8

    const-string v10, "CAPTURE_FAILURE"

    const-string v11, "cap fail"

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->CAPTURE_FAILURE:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 16
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v10, 0x9

    const-string v11, "CORRUPT_QUEUE"

    const-string v12, "corrupt queue"

    invoke-direct {v0, v11, v10, v12}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->CORRUPT_QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 17
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v11, 0xa

    const-string v12, "CRASH_REPORTER"

    const-string v13, "crash report"

    invoke-direct {v0, v12, v11, v13}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->CRASH_REPORTER:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 18
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const v12, -0xf87a00

    const/16 v13, 0xb

    const-string v14, "CREATE"

    const-string v15, "create"

    invoke-direct {v0, v14, v13, v15, v12}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->CREATE:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 19
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v14, 0xc

    const-string v15, "CREATE_VIEW"

    const-string v13, "create view"

    invoke-direct {v0, v15, v14, v13}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->CREATE_VIEW:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 20
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v13, 0xd

    const-string v15, "DESTROY"

    const-string v14, "destroy"

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->DESTROY:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 21
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v14, "DESTROY_SCOPE"

    const/16 v15, 0xe

    const-string v13, "destroy scope"

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->DESTROY_SCOPE:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 22
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v13, "FINISH"

    const/16 v14, 0xf

    const-string v15, "finish"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->FINISH:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 23
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v13, "HANDLE_INTENT"

    const/16 v14, 0x10

    const-string v15, "intent"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->HANDLE_INTENT:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 24
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v13, "LOG_OUT"

    const/16 v14, 0x11

    const-string v15, "log out"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->LOG_OUT:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 25
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v13, "LOGGED_EXCEPTION"

    const/16 v14, 0x12

    const-string v15, "exception"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->LOGGED_EXCEPTION:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 26
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v13, "MINESWEEPER"

    const/16 v14, 0x13

    const-string v15, "minesweeper"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->MINESWEEPER:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 27
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v13, "NATIVE_LIBRARY"

    const/16 v14, 0x14

    const-string v15, "native"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->NATIVE_LIBRARY:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 28
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v13, "ON_SAVE"

    const/16 v14, 0x15

    const-string v15, "save"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->ON_SAVE:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 29
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v13, "OTTO_EVENT"

    const/16 v14, 0x16

    const-string v15, "bus"

    const v11, -0xfcc195

    invoke-direct {v0, v13, v14, v15, v11}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->OTTO_EVENT:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 30
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v11, "PAUSE"

    const/16 v13, 0x17

    const-string v14, "pause"

    const v15, -0xdef98e

    invoke-direct {v0, v11, v13, v14, v15}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->PAUSE:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 31
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v11, "QUEUE"

    const/16 v13, 0x18

    const-string v14, "queue"

    const v15, -0xfd3b46

    invoke-direct {v0, v11, v13, v14, v15}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 32
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v11, "READER"

    const/16 v13, 0x19

    const-string v14, "reader"

    invoke-direct {v0, v11, v13, v14}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 33
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v11, "RESUME"

    const/16 v13, 0x1a

    const-string v14, "resume"

    invoke-direct {v0, v11, v13, v14, v12}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->RESUME:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 34
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v11, "RETAIN_NONCONFIG"

    const/16 v12, 0x1b

    const-string v13, "retain"

    invoke-direct {v0, v11, v12, v13}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->RETAIN_NONCONFIG:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 35
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v11, "SAMSUNG_CRASH"

    const/16 v12, 0x1c

    const-string v13, "samsung crash"

    invoke-direct {v0, v11, v12, v13}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->SAMSUNG_CRASH:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 36
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v11, "SERVER_CALL"

    const/16 v12, 0x1d

    const-string v13, "server"

    const v14, -0x5bfffc

    invoke-direct {v0, v11, v12, v13, v14}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->SERVER_CALL:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 37
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v11, "SHOW_SCREEN"

    const/16 v12, 0x1e

    const-string v13, "show"

    invoke-direct {v0, v11, v12, v13}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->SHOW_SCREEN:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 38
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v11, "SSO"

    const/16 v12, 0x1f

    const-string v13, "single sign on"

    invoke-direct {v0, v11, v12, v13}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->SSO:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 39
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v11, "START_ACTIVITY"

    const/16 v12, 0x20

    const-string v13, "start act"

    invoke-direct {v0, v11, v12, v13}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->START_ACTIVITY:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 40
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v11, "WARNING"

    const/16 v12, 0x21

    const-string/jumbo v13, "warning"

    const/16 v14, -0x6700

    invoke-direct {v0, v11, v12, v13, v14}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->WARNING:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 41
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v11, "X2_BRAN"

    const/16 v12, 0x22

    const-string/jumbo v13, "x2_bran"

    invoke-direct {v0, v11, v12, v13}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->X2_BRAN:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 42
    new-instance v0, Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v11, "X2_RPC"

    const/16 v12, 0x23

    const-string/jumbo v13, "x2_rpc"

    invoke-direct {v0, v11, v12, v13}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->X2_RPC:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v0, 0x24

    new-array v0, v0, [Lcom/squareup/log/OhSnapLogger$EventType;

    .line 6
    sget-object v11, Lcom/squareup/log/OhSnapLogger$EventType;->API:Lcom/squareup/log/OhSnapLogger$EventType;

    aput-object v11, v0, v1

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->APOS:Lcom/squareup/log/OhSnapLogger$EventType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->AUTO_CAPTURE:Lcom/squareup/log/OhSnapLogger$EventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->AUTO_VOID:Lcom/squareup/log/OhSnapLogger$EventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->BACKGROUND_JOB:Lcom/squareup/log/OhSnapLogger$EventType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->BLE:Lcom/squareup/log/OhSnapLogger$EventType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->BUYER_DEVICE:Lcom/squareup/log/OhSnapLogger$EventType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->CAMERA:Lcom/squareup/log/OhSnapLogger$EventType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->CAPTURE_FAILURE:Lcom/squareup/log/OhSnapLogger$EventType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->CORRUPT_QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->CRASH_REPORTER:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->CREATE:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->CREATE_VIEW:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->DESTROY:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->DESTROY_SCOPE:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->FINISH:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->HANDLE_INTENT:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->LOG_OUT:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->LOGGED_EXCEPTION:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->MINESWEEPER:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->NATIVE_LIBRARY:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->ON_SAVE:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->OTTO_EVENT:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->PAUSE:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->RESUME:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->RETAIN_NONCONFIG:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->SAMSUNG_CRASH:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->SERVER_CALL:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->SHOW_SCREEN:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->SSO:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->START_ACTIVITY:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->WARNING:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->X2_BRAN:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->X2_RPC:Lcom/squareup/log/OhSnapLogger$EventType;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->$VALUES:[Lcom/squareup/log/OhSnapLogger$EventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/high16 v0, -0x1000000

    .line 48
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/log/OhSnapLogger$EventType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iput-object p3, p0, Lcom/squareup/log/OhSnapLogger$EventType;->prettyName:Ljava/lang/String;

    .line 53
    iput p4, p0, Lcom/squareup/log/OhSnapLogger$EventType;->color:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/log/OhSnapLogger$EventType;
    .locals 1

    .line 6
    const-class v0, Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/log/OhSnapLogger$EventType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/log/OhSnapLogger$EventType;
    .locals 1

    .line 6
    sget-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->$VALUES:[Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-virtual {v0}, [Lcom/squareup/log/OhSnapLogger$EventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/log/OhSnapLogger$EventType;

    return-object v0
.end method
