.class public Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;
.super Ljava/lang/Object;
.source "RssiLoggingHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/RssiLoggingHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RssiLogRingBuffer"
.end annotation


# instance fields
.field private buffer:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;",
            ">;"
        }
    .end annotation
.end field

.field private index:I

.field private final size:I

.field final synthetic this$0:Lcom/squareup/log/RssiLoggingHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/log/RssiLoggingHelper;I)V
    .locals 1

    .line 114
    iput-object p1, p0, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->this$0:Lcom/squareup/log/RssiLoggingHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput p2, p0, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->size:I

    const/4 p1, 0x0

    .line 116
    iput p1, p0, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->index:I

    .line 117
    new-instance p1, Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-static {p2, v0}, Ljava/util/Collections;->nCopies(ILjava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->buffer:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addMeasurement(Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;)V
    .locals 3

    .line 153
    iget-object v0, p0, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->buffer:Ljava/util/List;

    iget v1, p0, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->index:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->index:I

    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 154
    iget p1, p0, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->index:I

    iget v0, p0, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->size:I

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    .line 155
    iput p1, p0, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->index:I

    :cond_0
    return-void
.end method

.method public getMeasurements()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;",
            ">;"
        }
    .end annotation

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 164
    iget-object v1, p0, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->buffer:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;

    if-eqz v2, :cond_0

    .line 166
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 169
    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 146
    sget v0, Lcom/squareup/log/RssiLoggingHelper;->MAX_BUFFER_SIZE:I

    invoke-virtual {p0, v0}, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 5

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 128
    iget v1, p0, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->index:I

    if-nez v1, :cond_0

    iget v1, p0, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->size:I

    :cond_0
    add-int/lit8 v1, v1, -0x1

    .line 130
    iget v2, p0, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->size:I

    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_3

    .line 133
    iget-object v3, p0, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->buffer:Ljava/util/List;

    add-int/lit8 v4, v1, -0x1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;

    if-eqz v1, :cond_1

    .line 135
    invoke-virtual {v1}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurement;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    if-gez v4, :cond_2

    .line 138
    iget v1, p0, Lcom/squareup/log/RssiLoggingHelper$RssiLogRingBuffer;->size:I

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_2
    move v1, v4

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 142
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object p1

    const-string v0, "; "

    invoke-static {p1, v0}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
