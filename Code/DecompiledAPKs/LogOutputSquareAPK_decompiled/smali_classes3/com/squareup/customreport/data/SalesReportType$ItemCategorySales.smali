.class public final Lcom/squareup/customreport/data/SalesReportType$ItemCategorySales;
.super Lcom/squareup/customreport/data/SalesReportType;
.source "SalesReportType.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/customreport/data/SalesReportType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ItemCategorySales"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/customreport/data/SalesReportType$ItemCategorySales$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\t\u0010\u0003\u001a\u00020\u0004H\u00d6\u0001J\u0019\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0004H\u00d6\u0001\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/customreport/data/SalesReportType$ItemCategorySales;",
        "Lcom/squareup/customreport/data/SalesReportType;",
        "()V",
        "describeContents",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final INSTANCE:Lcom/squareup/customreport/data/SalesReportType$ItemCategorySales;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 63
    new-instance v0, Lcom/squareup/customreport/data/SalesReportType$ItemCategorySales;

    invoke-direct {v0}, Lcom/squareup/customreport/data/SalesReportType$ItemCategorySales;-><init>()V

    sput-object v0, Lcom/squareup/customreport/data/SalesReportType$ItemCategorySales;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$ItemCategorySales;

    new-instance v0, Lcom/squareup/customreport/data/SalesReportType$ItemCategorySales$Creator;

    invoke-direct {v0}, Lcom/squareup/customreport/data/SalesReportType$ItemCategorySales$Creator;-><init>()V

    sput-object v0, Lcom/squareup/customreport/data/SalesReportType$ItemCategorySales;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 63
    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM_CATEGORY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/customreport/data/SalesReportType;-><init>(Ljava/util/List;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
