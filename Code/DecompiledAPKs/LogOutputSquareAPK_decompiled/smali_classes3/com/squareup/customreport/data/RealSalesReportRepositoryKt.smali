.class public final Lcom/squareup/customreport/data/RealSalesReportRepositoryKt;
.super Ljava/lang/Object;
.source "RealSalesReportRepository.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSalesReportRepository.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSalesReportRepository.kt\ncom/squareup/customreport/data/RealSalesReportRepositoryKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,394:1\n1360#2:395\n1429#2,3:396\n704#2:399\n777#2,2:400\n950#2:402\n1360#2:403\n1429#2,3:404\n959#2:407\n1360#2:408\n1429#2,3:409\n959#2:412\n1360#2:413\n1429#2,3:414\n959#2:417\n*E\n*S KotlinDebug\n*F\n+ 1 RealSalesReportRepository.kt\ncom/squareup/customreport/data/RealSalesReportRepositoryKt\n*L\n336#1:395\n336#1,3:396\n337#1:399\n337#1,2:400\n338#1:402\n351#1:403\n351#1,3:404\n352#1:407\n365#1:408\n365#1,3:409\n366#1:412\n378#1:413\n378#1,3:414\n378#1:417\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\n\u001a\u00020\u000b*\u00020\u0002H\u0002\u001a\u000c\u0010\u000c\u001a\u00020\r*\u00020\u0002H\u0002\u001a\u000c\u0010\u000e\u001a\u00020\u000f*\u00020\u0002H\u0002\u001a\u000c\u0010\u0010\u001a\u00020\u0011*\u00020\u0002H\u0002\"\u001a\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0018\u0010\u0005\u001a\u00020\u0006*\u00020\u00078BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u0012"
    }
    d2 = {
        "aggregateReport",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
        "getAggregateReport",
        "(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
        "chartReportType",
        "Lcom/squareup/customreport/data/SalesReportType$Charted;",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "getChartReportType",
        "(Lcom/squareup/customreport/data/ReportConfig;)Lcom/squareup/customreport/data/SalesReportType$Charted;",
        "discountResult",
        "Lcom/squareup/customreport/data/SalesDiscountsReport;",
        "paymentMethodsResult",
        "Lcom/squareup/customreport/data/SalesPaymentMethodsReport;",
        "topCategoriesResult",
        "Lcom/squareup/customreport/data/SalesTopCategoriesReport;",
        "topItemsResult",
        "Lcom/squareup/customreport/data/SalesTopItemsReport;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$discountResult(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/customreport/data/SalesDiscountsReport;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/customreport/data/RealSalesReportRepositoryKt;->discountResult(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/customreport/data/SalesDiscountsReport;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAggregateReport$p(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/customreport/data/RealSalesReportRepositoryKt;->getAggregateReport(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getChartReportType$p(Lcom/squareup/customreport/data/ReportConfig;)Lcom/squareup/customreport/data/SalesReportType$Charted;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/customreport/data/RealSalesReportRepositoryKt;->getChartReportType(Lcom/squareup/customreport/data/ReportConfig;)Lcom/squareup/customreport/data/SalesReportType$Charted;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$paymentMethodsResult(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/customreport/data/SalesPaymentMethodsReport;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/customreport/data/RealSalesReportRepositoryKt;->paymentMethodsResult(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/customreport/data/SalesPaymentMethodsReport;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$topCategoriesResult(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/customreport/data/SalesTopCategoriesReport;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/customreport/data/RealSalesReportRepositoryKt;->topCategoriesResult(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/customreport/data/SalesTopCategoriesReport;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$topItemsResult(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/customreport/data/SalesTopItemsReport;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/customreport/data/RealSalesReportRepositoryKt;->topItemsResult(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/customreport/data/SalesTopItemsReport;

    move-result-object p0

    return-object p0
.end method

.method private static final discountResult(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/customreport/data/SalesDiscountsReport;
    .locals 4

    .line 335
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DISCOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-static {p0, v0}, Lcom/squareup/customreport/data/util/CustomReportResponsesKt;->getReportsByGroupType(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 395
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 396
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 397
    check-cast v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    .line 336
    invoke-static {v1}, Lcom/squareup/customreport/data/util/CustomReportsKt;->toSalesDiscount(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Lcom/squareup/customreport/data/SalesDiscount;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 398
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 399
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    check-cast p0, Ljava/util/Collection;

    .line 400
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/customreport/data/SalesDiscount;

    .line 337
    invoke-virtual {v2}, Lcom/squareup/customreport/data/SalesDiscount;->getName()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->translation_type:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    sget-object v3, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_DISCOUNT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    if-eq v2, v3, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_1

    invoke-interface {p0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 401
    :cond_3
    check-cast p0, Ljava/util/List;

    check-cast p0, Ljava/lang/Iterable;

    .line 402
    new-instance v0, Lcom/squareup/customreport/data/RealSalesReportRepositoryKt$discountResult$$inlined$sortedBy$1;

    invoke-direct {v0}, Lcom/squareup/customreport/data/RealSalesReportRepositoryKt$discountResult$$inlined$sortedBy$1;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    invoke-static {p0, v0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p0

    .line 339
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 340
    sget-object p0, Lcom/squareup/customreport/data/NoSalesDiscountsReport;->INSTANCE:Lcom/squareup/customreport/data/NoSalesDiscountsReport;

    check-cast p0, Lcom/squareup/customreport/data/SalesDiscountsReport;

    goto :goto_3

    .line 342
    :cond_4
    new-instance v0, Lcom/squareup/customreport/data/WithSalesDiscountsReport;

    invoke-direct {v0, p0}, Lcom/squareup/customreport/data/WithSalesDiscountsReport;-><init>(Ljava/util/List;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/customreport/data/SalesDiscountsReport;

    :goto_3
    return-object p0
.end method

.method private static final getAggregateReport(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;
    .locals 1

    .line 308
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->NONE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-static {p0, v0}, Lcom/squareup/customreport/data/util/CustomReportResponsesKt;->getReportsByGroupType(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)Ljava/util/List;

    move-result-object p0

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    return-object p0
.end method

.method private static final getChartReportType(Lcom/squareup/customreport/data/ReportConfig;)Lcom/squareup/customreport/data/SalesReportType$Charted;
    .locals 8

    .line 314
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object v0

    .line 315
    sget-object v1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneDay;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    sget-object p0, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;

    check-cast p0, Lcom/squareup/customreport/data/SalesReportType$Charted;

    goto/16 :goto_4

    .line 316
    :cond_1
    sget-object v1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    sget-object v1, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneWeek;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_1
    sget-object p0, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDayOfWeek;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDayOfWeek;

    check-cast p0, Lcom/squareup/customreport/data/SalesReportType$Charted;

    goto/16 :goto_4

    .line 317
    :cond_3
    sget-object v1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_2

    :cond_4
    sget-object v1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    goto :goto_2

    :cond_5
    sget-object v1, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneMonth;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    goto :goto_2

    :cond_6
    sget-object v1, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    :goto_2
    sget-object p0, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDay;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDay;

    check-cast p0, Lcom/squareup/customreport/data/SalesReportType$Charted;

    goto :goto_4

    .line 318
    :cond_7
    sget-object v1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    sget-object p0, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByMonth;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByMonth;

    check-cast p0, Lcom/squareup/customreport/data/SalesReportType$Charted;

    goto :goto_4

    .line 319
    :cond_8
    sget-object v1, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 320
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    check-cast v1, Lorg/threeten/bp/temporal/Temporal;

    sget-object v2, Lorg/threeten/bp/temporal/ChronoUnit;->DAYS:Lorg/threeten/bp/temporal/ChronoUnit;

    check-cast v2, Lorg/threeten/bp/temporal/TemporalUnit;

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/LocalDate;->until(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    .line 321
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v4

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object p0

    check-cast p0, Lorg/threeten/bp/temporal/Temporal;

    sget-object v5, Lorg/threeten/bp/temporal/ChronoUnit;->MONTHS:Lorg/threeten/bp/temporal/ChronoUnit;

    check-cast v5, Lorg/threeten/bp/temporal/TemporalUnit;

    invoke-virtual {v4, p0, v5}, Lorg/threeten/bp/LocalDate;->until(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide v4

    add-long/2addr v4, v2

    cmp-long p0, v0, v2

    if-nez p0, :cond_9

    .line 323
    sget-object p0, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;

    check-cast p0, Lcom/squareup/customreport/data/SalesReportType$Charted;

    goto :goto_4

    :cond_9
    const/4 p0, 0x7

    int-to-long v2, p0

    const/4 p0, 0x2

    int-to-long v6, p0

    cmp-long p0, v6, v0

    if-lez p0, :cond_a

    goto :goto_3

    :cond_a
    cmp-long p0, v2, v0

    if-ltz p0, :cond_b

    .line 324
    sget-object p0, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDayOfWeek;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDayOfWeek;

    check-cast p0, Lcom/squareup/customreport/data/SalesReportType$Charted;

    goto :goto_4

    :cond_b
    :goto_3
    const/4 p0, 0x3

    int-to-long v0, p0

    cmp-long p0, v4, v0

    if-lez p0, :cond_c

    .line 325
    sget-object p0, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByMonth;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByMonth;

    check-cast p0, Lcom/squareup/customreport/data/SalesReportType$Charted;

    goto :goto_4

    .line 326
    :cond_c
    sget-object p0, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDay;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDay;

    check-cast p0, Lcom/squareup/customreport/data/SalesReportType$Charted;

    :goto_4
    return-object p0

    .line 322
    :cond_d
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method private static final paymentMethodsResult(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/customreport/data/SalesPaymentMethodsReport;
    .locals 6

    .line 378
    invoke-static {p0}, Lcom/squareup/customreport/data/RealSalesReportRepositoryKt;->getAggregateReport(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    move-result-object p0

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 379
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_collected_money:Lcom/squareup/protos/common/Money;

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    if-eqz p0, :cond_4

    if-eqz v1, :cond_4

    .line 380
    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    if-eqz v2, :cond_4

    .line 382
    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    const-string/jumbo v3, "this.sub_report"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Iterable;

    .line 413
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 414
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 415
    check-cast v4, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    const-string v5, "customReport"

    .line 383
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4, v1}, Lcom/squareup/customreport/data/util/CustomReportsKt;->toSalesPaymentMethod(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;Lcom/squareup/protos/common/Money;)Lcom/squareup/customreport/data/SalesPaymentMethod;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 416
    :cond_1
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 417
    new-instance v2, Lcom/squareup/customreport/data/RealSalesReportRepositoryKt$$special$$inlined$sortedByDescending$1;

    invoke-direct {v2}, Lcom/squareup/customreport/data/RealSalesReportRepositoryKt$$special$$inlined$sortedByDescending$1;-><init>()V

    check-cast v2, Ljava/util/Comparator;

    invoke-static {v3, v2}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v2

    .line 385
    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    if-eqz v3, :cond_2

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v3, :cond_2

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_money:Lcom/squareup/protos/common/Money;

    goto :goto_2

    :cond_2
    move-object v3, v0

    .line 387
    :goto_2
    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    if-eqz p0, :cond_3

    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz p0, :cond_3

    iget-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_total_money:Lcom/squareup/protos/common/Money;

    .line 381
    :cond_3
    new-instance p0, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;

    invoke-direct {p0, v2, v3, v1, v0}, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;-><init>(Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    check-cast p0, Lcom/squareup/customreport/data/SalesPaymentMethodsReport;

    goto :goto_3

    .line 390
    :cond_4
    sget-object p0, Lcom/squareup/customreport/data/NoSalesPaymentMethodsReport;->INSTANCE:Lcom/squareup/customreport/data/NoSalesPaymentMethodsReport;

    check-cast p0, Lcom/squareup/customreport/data/SalesPaymentMethodsReport;

    :goto_3
    return-object p0
.end method

.method private static final topCategoriesResult(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/customreport/data/SalesTopCategoriesReport;
    .locals 2

    .line 364
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM_CATEGORY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-static {p0, v0}, Lcom/squareup/customreport/data/util/CustomReportResponsesKt;->getReportsByGroupType(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 408
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 409
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 410
    check-cast v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    .line 365
    invoke-static {v1}, Lcom/squareup/customreport/data/util/CustomReportsKt;->toSalesCategory(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Lcom/squareup/customreport/data/SalesItem;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 411
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 412
    new-instance p0, Lcom/squareup/customreport/data/RealSalesReportRepositoryKt$topCategoriesResult$$inlined$sortedByDescending$1;

    invoke-direct {p0}, Lcom/squareup/customreport/data/RealSalesReportRepositoryKt$topCategoriesResult$$inlined$sortedByDescending$1;-><init>()V

    check-cast p0, Ljava/util/Comparator;

    invoke-static {v0, p0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p0

    .line 367
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 368
    sget-object p0, Lcom/squareup/customreport/data/NoSalesTopCategoriesReport;->INSTANCE:Lcom/squareup/customreport/data/NoSalesTopCategoriesReport;

    check-cast p0, Lcom/squareup/customreport/data/SalesTopCategoriesReport;

    goto :goto_1

    .line 370
    :cond_1
    new-instance v0, Lcom/squareup/customreport/data/WithSalesTopCategoriesReport;

    invoke-direct {v0, p0}, Lcom/squareup/customreport/data/WithSalesTopCategoriesReport;-><init>(Ljava/util/List;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/customreport/data/SalesTopCategoriesReport;

    :goto_1
    return-object p0
.end method

.method private static final topItemsResult(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/customreport/data/SalesTopItemsReport;
    .locals 2

    .line 350
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-static {p0, v0}, Lcom/squareup/customreport/data/util/CustomReportResponsesKt;->getReportsByGroupType(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 403
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 404
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 405
    check-cast v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    .line 351
    invoke-static {v1}, Lcom/squareup/customreport/data/util/CustomReportsKt;->toSalesItem(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;)Lcom/squareup/customreport/data/SalesItem;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 406
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 407
    new-instance p0, Lcom/squareup/customreport/data/RealSalesReportRepositoryKt$topItemsResult$$inlined$sortedByDescending$1;

    invoke-direct {p0}, Lcom/squareup/customreport/data/RealSalesReportRepositoryKt$topItemsResult$$inlined$sortedByDescending$1;-><init>()V

    check-cast p0, Ljava/util/Comparator;

    invoke-static {v0, p0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p0

    .line 353
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 354
    sget-object p0, Lcom/squareup/customreport/data/NoSalesTopItemsReport;->INSTANCE:Lcom/squareup/customreport/data/NoSalesTopItemsReport;

    check-cast p0, Lcom/squareup/customreport/data/SalesTopItemsReport;

    goto :goto_1

    .line 356
    :cond_1
    new-instance v0, Lcom/squareup/customreport/data/WithSalesTopItemsReport;

    invoke-direct {v0, p0}, Lcom/squareup/customreport/data/WithSalesTopItemsReport;-><init>(Ljava/util/List;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/customreport/data/SalesTopItemsReport;

    :goto_1
    return-object p0
.end method
