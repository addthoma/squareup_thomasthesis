.class public interface abstract Lcom/squareup/eventstream/EventStore$Clock;
.super Ljava/lang/Object;
.source "EventStore.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/eventstream/EventStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Clock"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/eventstream/EventStore$Clock$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\u0008`\u0018\u0000 \u00042\u00020\u0001:\u0001\u0004J\u0008\u0010\u0002\u001a\u00020\u0003H&\u0082\u0002\u0007\n\u0005\u0008\u0091F0\u0001\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/eventstream/EventStore$Clock;",
        "",
        "uptimeMillis",
        "",
        "Companion",
        "eventstream-common_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/eventstream/EventStore$Clock$Companion;

.field public static final REAL:Lcom/squareup/eventstream/EventStore$Clock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/eventstream/EventStore$Clock$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/eventstream/EventStore$Clock$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/eventstream/EventStore$Clock;->Companion:Lcom/squareup/eventstream/EventStore$Clock$Companion;

    .line 59
    new-instance v0, Lcom/squareup/eventstream/EventStore$Clock$Companion$REAL$1;

    invoke-direct {v0}, Lcom/squareup/eventstream/EventStore$Clock$Companion$REAL$1;-><init>()V

    check-cast v0, Lcom/squareup/eventstream/EventStore$Clock;

    sput-object v0, Lcom/squareup/eventstream/EventStore$Clock;->REAL:Lcom/squareup/eventstream/EventStore$Clock;

    return-void
.end method


# virtual methods
.method public abstract uptimeMillis()J
.end method
