.class public Lcom/squareup/eventstream/QueueFactory;
.super Ljava/lang/Object;
.source "QueueFactory.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0010\u0003\n\u0000\u0008\u0010\u0018\u0000*\u0004\u0008\u0000\u0010\u00012\u00020\u0002B\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006\u00a2\u0006\u0002\u0010\u0007J\u000e\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00028\u00000\tH\u0016J\u0008\u0010\n\u001a\u00020\u000bH\u0016J\u0018\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00028\u00000\t2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/eventstream/QueueFactory;",
        "T",
        "",
        "file",
        "Ljava/io/File;",
        "converter",
        "Lcom/squareup/tape/FileObjectQueue$Converter;",
        "(Ljava/io/File;Lcom/squareup/tape/FileObjectQueue$Converter;)V",
        "create",
        "Lcom/squareup/tape/FileObjectQueue;",
        "queueFileLength",
        "",
        "recreate",
        "wrappedThrowable",
        "",
        "eventstream-common_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final converter:Lcom/squareup/tape/FileObjectQueue$Converter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final file:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;Lcom/squareup/tape/FileObjectQueue$Converter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "file"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "converter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/eventstream/QueueFactory;->file:Ljava/io/File;

    iput-object p2, p0, Lcom/squareup/eventstream/QueueFactory;->converter:Lcom/squareup/tape/FileObjectQueue$Converter;

    return-void
.end method


# virtual methods
.method public create()Lcom/squareup/tape/FileObjectQueue;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/tape/FileObjectQueue<",
            "TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 17
    new-instance v0, Lcom/squareup/tape/FileObjectQueue;

    iget-object v1, p0, Lcom/squareup/eventstream/QueueFactory;->file:Ljava/io/File;

    iget-object v2, p0, Lcom/squareup/eventstream/QueueFactory;->converter:Lcom/squareup/tape/FileObjectQueue$Converter;

    invoke-direct {v0, v1, v2}, Lcom/squareup/tape/FileObjectQueue;-><init>(Ljava/io/File;Lcom/squareup/tape/FileObjectQueue$Converter;)V

    return-object v0
.end method

.method public queueFileLength()J
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/eventstream/QueueFactory;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public recreate(Ljava/lang/Throwable;)Lcom/squareup/tape/FileObjectQueue;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/squareup/tape/FileObjectQueue<",
            "TT;>;"
        }
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/squareup/eventstream/QueueFactory;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 22
    :try_start_0
    new-instance v0, Lcom/squareup/tape/FileObjectQueue;

    iget-object v1, p0, Lcom/squareup/eventstream/QueueFactory;->file:Ljava/io/File;

    iget-object v2, p0, Lcom/squareup/eventstream/QueueFactory;->converter:Lcom/squareup/tape/FileObjectQueue$Converter;

    invoke-direct {v0, v1, v2}, Lcom/squareup/tape/FileObjectQueue;-><init>(Ljava/io/File;Lcom/squareup/tape/FileObjectQueue$Converter;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 24
    :catch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 25
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IOException when trying to recreate queue file "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/eventstream/QueueFactory;->file:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 24
    invoke-direct {v0, v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
