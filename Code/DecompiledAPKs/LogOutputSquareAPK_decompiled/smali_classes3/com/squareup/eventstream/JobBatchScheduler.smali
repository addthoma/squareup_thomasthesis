.class public Lcom/squareup/eventstream/JobBatchScheduler;
.super Ljava/lang/Object;
.source "JobBatchScheduler.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/eventstream/JobBatchScheduler$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0010\u0018\u0000 \u00132\u00020\u0001:\u0001\u0013B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\u0010\u0006\u001a\u0006\u0012\u0002\u0008\u00030\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\u0010\u001a\u00020\u0011H\u0016J\u0008\u0010\u0012\u001a\u00020\u0011H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/eventstream/JobBatchScheduler;",
        "",
        "context",
        "Landroid/content/Context;",
        "jobLabel",
        "",
        "uploadBatcher",
        "Lcom/squareup/eventstream/UploadBatcher;",
        "(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/UploadBatcher;)V",
        "jobCreator",
        "Lcom/evernote/android/job/JobCreator;",
        "jobCreatorTag",
        "jobManager",
        "Lcom/evernote/android/job/JobManager;",
        "jobRequestBuilder",
        "Lcom/evernote/android/job/JobRequest$Builder;",
        "shutdown",
        "",
        "startNow",
        "Companion",
        "eventstream-common_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/eventstream/JobBatchScheduler$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final IMMEDIATELY_MS:J = 0x1L


# instance fields
.field private final jobCreator:Lcom/evernote/android/job/JobCreator;

.field private final jobCreatorTag:Ljava/lang/String;

.field private final jobManager:Lcom/evernote/android/job/JobManager;

.field private final jobRequestBuilder:Lcom/evernote/android/job/JobRequest$Builder;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/eventstream/JobBatchScheduler$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/eventstream/JobBatchScheduler$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/eventstream/JobBatchScheduler;->Companion:Lcom/squareup/eventstream/JobBatchScheduler$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/UploadBatcher;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/squareup/eventstream/UploadBatcher<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jobLabel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "uploadBatcher"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/evernote/android/job/JobManager;->create(Landroid/content/Context;)Lcom/evernote/android/job/JobManager;

    move-result-object p1

    const-string v0, "JobManager.create(context.applicationContext)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/eventstream/JobBatchScheduler;->jobManager:Lcom/evernote/android/job/JobManager;

    .line 21
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ":eventstream-job"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/eventstream/JobBatchScheduler;->jobCreatorTag:Ljava/lang/String;

    const/4 p1, 0x0

    .line 27
    invoke-static {p1}, Lcom/evernote/android/job/JobConfig;->setLogcatEnabled(Z)V

    .line 32
    new-instance p2, Lcom/evernote/android/job/JobRequest$Builder;

    iget-object v0, p0, Lcom/squareup/eventstream/JobBatchScheduler;->jobCreatorTag:Ljava/lang/String;

    invoke-direct {p2, v0}, Lcom/evernote/android/job/JobRequest$Builder;-><init>(Ljava/lang/String;)V

    .line 33
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x5

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    invoke-virtual {p2, v2, v3, v0, v1}, Lcom/evernote/android/job/JobRequest$Builder;->setExecutionWindow(JJ)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p2

    .line 34
    sget-object v0, Lcom/evernote/android/job/JobRequest$NetworkType;->CONNECTED:Lcom/evernote/android/job/JobRequest$NetworkType;

    invoke-virtual {p2, v0}, Lcom/evernote/android/job/JobRequest$Builder;->setRequiredNetworkType(Lcom/evernote/android/job/JobRequest$NetworkType;)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p2

    .line 35
    invoke-virtual {p2, p1}, Lcom/evernote/android/job/JobRequest$Builder;->setRequiresDeviceIdle(Z)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p1

    const/4 p2, 0x1

    .line 37
    invoke-virtual {p1, p2}, Lcom/evernote/android/job/JobRequest$Builder;->setUpdateCurrent(Z)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p1

    .line 40
    sget-object p2, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v0, 0x2

    invoke-virtual {p2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sget-object p2, Lcom/evernote/android/job/JobRequest$BackoffPolicy;->LINEAR:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    invoke-virtual {p1, v0, v1, p2}, Lcom/evernote/android/job/JobRequest$Builder;->setBackoffCriteria(JLcom/evernote/android/job/JobRequest$BackoffPolicy;)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p1

    const-string p2, "JobRequest.Builder(jobCr\u2026UTES.toMillis(2), LINEAR)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/eventstream/JobBatchScheduler;->jobRequestBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    .line 42
    new-instance p1, Lcom/squareup/eventstream/JobBatchScheduler$1;

    invoke-direct {p1, p0, p3}, Lcom/squareup/eventstream/JobBatchScheduler$1;-><init>(Lcom/squareup/eventstream/JobBatchScheduler;Lcom/squareup/eventstream/UploadBatcher;)V

    check-cast p1, Lcom/evernote/android/job/JobCreator;

    iput-object p1, p0, Lcom/squareup/eventstream/JobBatchScheduler;->jobCreator:Lcom/evernote/android/job/JobCreator;

    .line 50
    iget-object p1, p0, Lcom/squareup/eventstream/JobBatchScheduler;->jobManager:Lcom/evernote/android/job/JobManager;

    iget-object p2, p0, Lcom/squareup/eventstream/JobBatchScheduler;->jobCreator:Lcom/evernote/android/job/JobCreator;

    invoke-virtual {p1, p2}, Lcom/evernote/android/job/JobManager;->addJobCreator(Lcom/evernote/android/job/JobCreator;)V

    return-void
.end method

.method public static final synthetic access$getJobCreatorTag$p(Lcom/squareup/eventstream/JobBatchScheduler;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/eventstream/JobBatchScheduler;->jobCreatorTag:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public shutdown()V
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/eventstream/JobBatchScheduler;->jobManager:Lcom/evernote/android/job/JobManager;

    iget-object v1, p0, Lcom/squareup/eventstream/JobBatchScheduler;->jobCreatorTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/evernote/android/job/JobManager;->cancelAllForTag(Ljava/lang/String;)I

    .line 61
    iget-object v0, p0, Lcom/squareup/eventstream/JobBatchScheduler;->jobManager:Lcom/evernote/android/job/JobManager;

    iget-object v1, p0, Lcom/squareup/eventstream/JobBatchScheduler;->jobCreator:Lcom/evernote/android/job/JobCreator;

    invoke-virtual {v0, v1}, Lcom/evernote/android/job/JobManager;->removeJobCreator(Lcom/evernote/android/job/JobCreator;)V

    return-void
.end method

.method public startNow()V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/eventstream/JobBatchScheduler;->jobRequestBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    .line 55
    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest$Builder;->build()Lcom/evernote/android/job/JobRequest;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->schedule()I

    return-void
.end method
