.class public final Lcom/squareup/eventstream/UploadBatcher;
.super Ljava/lang/Object;
.source "UploadBatcher.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUploadBatcher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UploadBatcher.kt\ncom/squareup/eventstream/UploadBatcher\n*L\n1#1,82:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u0000*\u0004\u0008\u0000\u0010\u00012\u00020\u0002B?\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0004\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000c\u00a2\u0006\u0002\u0010\rJ\u0006\u0010\u000f\u001a\u00020\u0010R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0002X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/eventstream/UploadBatcher;",
        "T",
        "",
        "queue",
        "Lcom/squareup/eventstream/EventQueue;",
        "batchUploader",
        "Lcom/squareup/eventstream/EventBatchUploader;",
        "logger",
        "Lcom/squareup/eventstream/EventStreamLog;",
        "droppedEventCounter",
        "Lcom/squareup/eventstream/DroppedEventCounter;",
        "droppedEventsFactory",
        "Lcom/squareup/eventstream/DroppedEventsFactory;",
        "(Lcom/squareup/eventstream/EventQueue;Lcom/squareup/eventstream/EventBatchUploader;Lcom/squareup/eventstream/EventStreamLog;Lcom/squareup/eventstream/DroppedEventCounter;Lcom/squareup/eventstream/DroppedEventsFactory;)V",
        "uploadLock",
        "upload",
        "Lcom/evernote/android/job/Job$Result;",
        "eventstream-common_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final batchUploader:Lcom/squareup/eventstream/EventBatchUploader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/eventstream/EventBatchUploader<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final droppedEventCounter:Lcom/squareup/eventstream/DroppedEventCounter;

.field private final droppedEventsFactory:Lcom/squareup/eventstream/DroppedEventsFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/eventstream/DroppedEventsFactory<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final logger:Lcom/squareup/eventstream/EventStreamLog;

.field private final queue:Lcom/squareup/eventstream/EventQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/eventstream/EventQueue<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final uploadLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/squareup/eventstream/EventQueue;Lcom/squareup/eventstream/EventBatchUploader;Lcom/squareup/eventstream/EventStreamLog;Lcom/squareup/eventstream/DroppedEventCounter;Lcom/squareup/eventstream/DroppedEventsFactory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/eventstream/EventQueue<",
            "TT;>;",
            "Lcom/squareup/eventstream/EventBatchUploader<",
            "TT;>;",
            "Lcom/squareup/eventstream/EventStreamLog;",
            "Lcom/squareup/eventstream/DroppedEventCounter;",
            "Lcom/squareup/eventstream/DroppedEventsFactory<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "queue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "batchUploader"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "logger"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "droppedEventCounter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "droppedEventsFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/eventstream/UploadBatcher;->queue:Lcom/squareup/eventstream/EventQueue;

    iput-object p2, p0, Lcom/squareup/eventstream/UploadBatcher;->batchUploader:Lcom/squareup/eventstream/EventBatchUploader;

    iput-object p3, p0, Lcom/squareup/eventstream/UploadBatcher;->logger:Lcom/squareup/eventstream/EventStreamLog;

    iput-object p4, p0, Lcom/squareup/eventstream/UploadBatcher;->droppedEventCounter:Lcom/squareup/eventstream/DroppedEventCounter;

    iput-object p5, p0, Lcom/squareup/eventstream/UploadBatcher;->droppedEventsFactory:Lcom/squareup/eventstream/DroppedEventsFactory;

    .line 23
    new-instance p1, Ljava/lang/Object;

    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/eventstream/UploadBatcher;->uploadLock:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final upload()Lcom/evernote/android/job/Job$Result;
    .locals 8

    .line 37
    iget-object v0, p0, Lcom/squareup/eventstream/UploadBatcher;->uploadLock:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_5

    .line 40
    :try_start_0
    iget-object v2, p0, Lcom/squareup/eventstream/UploadBatcher;->queue:Lcom/squareup/eventstream/EventQueue;

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Lcom/squareup/eventstream/EventQueue;->peekBlocking(I)Ljava/util/List;

    move-result-object v2

    .line 42
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 43
    sget-object v1, Lcom/evernote/android/job/Job$Result;->SUCCESS:Lcom/evernote/android/job/Job$Result;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 46
    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/squareup/eventstream/UploadBatcher;->droppedEventCounter:Lcom/squareup/eventstream/DroppedEventCounter;

    invoke-virtual {v4}, Lcom/squareup/eventstream/DroppedEventCounter;->getCounts()Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 50
    new-instance v5, Ljava/util/ArrayList;

    move-object v6, v2

    check-cast v6, Ljava/util/Collection;

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    check-cast v5, Ljava/util/List;

    .line 52
    iget-object v6, p0, Lcom/squareup/eventstream/UploadBatcher;->droppedEventsFactory:Lcom/squareup/eventstream/DroppedEventsFactory;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    invoke-interface {v6, v4, v7}, Lcom/squareup/eventstream/DroppedEventsFactory;->createDroppedEventsEvent(Ljava/util/Map;I)Ljava/lang/Object;

    move-result-object v6

    .line 53
    move-object v7, v5

    check-cast v7, Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v5, v2

    .line 59
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-ge v6, v3, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    .line 63
    :goto_2
    :try_start_2
    iget-object v6, p0, Lcom/squareup/eventstream/UploadBatcher;->batchUploader:Lcom/squareup/eventstream/EventBatchUploader;

    invoke-interface {v6, v5}, Lcom/squareup/eventstream/EventBatchUploader;->upload(Ljava/util/List;)Lcom/squareup/eventstream/EventBatchUploader$Result;

    move-result-object v5
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :catch_0
    move-exception v5

    .line 65
    :try_start_3
    iget-object v6, p0, Lcom/squareup/eventstream/UploadBatcher;->logger:Lcom/squareup/eventstream/EventStreamLog;

    check-cast v5, Ljava/lang/Throwable;

    invoke-interface {v6, v5}, Lcom/squareup/eventstream/EventStreamLog;->report(Ljava/lang/Throwable;)V

    .line 66
    sget-object v5, Lcom/squareup/eventstream/EventBatchUploader$Result;->FAILURE:Lcom/squareup/eventstream/EventBatchUploader$Result;

    .line 69
    :goto_3
    sget-object v6, Lcom/squareup/eventstream/EventBatchUploader$Result;->RETRY:Lcom/squareup/eventstream/EventBatchUploader$Result;

    if-ne v5, v6, :cond_3

    .line 70
    sget-object v1, Lcom/evernote/android/job/Job$Result;->RESCHEDULE:Lcom/evernote/android/job/Job$Result;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit v0

    return-object v1

    :cond_3
    if-eqz v4, :cond_4

    .line 72
    :try_start_4
    sget-object v6, Lcom/squareup/eventstream/EventBatchUploader$Result;->SUCCESS:Lcom/squareup/eventstream/EventBatchUploader$Result;

    if-ne v5, v6, :cond_4

    .line 73
    iget-object v5, p0, Lcom/squareup/eventstream/UploadBatcher;->droppedEventCounter:Lcom/squareup/eventstream/DroppedEventCounter;

    invoke-virtual {v5, v4}, Lcom/squareup/eventstream/DroppedEventCounter;->remove(Ljava/util/Map;)V

    .line 76
    :cond_4
    iget-object v4, p0, Lcom/squareup/eventstream/UploadBatcher;->queue:Lcom/squareup/eventstream/EventQueue;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v4, v2}, Lcom/squareup/eventstream/EventQueue;->remove(I)V

    move v2, v3

    goto :goto_0

    .line 78
    :cond_5
    sget-object v1, Lcom/evernote/android/job/Job$Result;->SUCCESS:Lcom/evernote/android/job/Job$Result;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 37
    monitor-exit v0

    throw v1
.end method
