.class final Lcom/squareup/eventstream/BaseEventstream$log$1;
.super Ljava/lang/Object;
.source "BaseEventstream.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/eventstream/BaseEventstream;->log(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0004\u0008\u0001\u0010\u0003\"\u0004\u0008\u0002\u0010\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "AppEventTypeT",
        "ServerEventTypeT",
        "StateT",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $eventToLog:Ljava/lang/Object;

.field final synthetic $logTimeMillis:J

.field final synthetic this$0:Lcom/squareup/eventstream/BaseEventstream;


# direct methods
.method constructor <init>(Lcom/squareup/eventstream/BaseEventstream;Ljava/lang/Object;J)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/eventstream/BaseEventstream$log$1;->this$0:Lcom/squareup/eventstream/BaseEventstream;

    iput-object p2, p0, Lcom/squareup/eventstream/BaseEventstream$log$1;->$eventToLog:Ljava/lang/Object;

    iput-wide p3, p0, Lcom/squareup/eventstream/BaseEventstream$log$1;->$logTimeMillis:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .line 52
    iget-object v0, p0, Lcom/squareup/eventstream/BaseEventstream$log$1;->this$0:Lcom/squareup/eventstream/BaseEventstream;

    invoke-static {v0}, Lcom/squareup/eventstream/BaseEventstream;->access$isShutdown$p(Lcom/squareup/eventstream/BaseEventstream;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/squareup/eventstream/BaseEventstream$log$1;->this$0:Lcom/squareup/eventstream/BaseEventstream;

    invoke-static {v0}, Lcom/squareup/eventstream/BaseEventstream;->access$getEventFactory$p(Lcom/squareup/eventstream/BaseEventstream;)Lcom/squareup/eventstream/EventFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/eventstream/BaseEventstream$log$1;->$eventToLog:Ljava/lang/Object;

    iget-wide v2, p0, Lcom/squareup/eventstream/BaseEventstream$log$1;->$logTimeMillis:J

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/eventstream/EventFactory;->create(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lcom/squareup/eventstream/BaseEventstream$log$1;->this$0:Lcom/squareup/eventstream/BaseEventstream;

    invoke-static {v1}, Lcom/squareup/eventstream/BaseEventstream;->access$getEventStore$p(Lcom/squareup/eventstream/BaseEventstream;)Lcom/squareup/eventstream/EventStore;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/eventstream/EventStore;->log(Ljava/lang/Object;)V

    return-void
.end method
