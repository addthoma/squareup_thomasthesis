.class public abstract Lcom/squareup/eventstream/BaseEventstream$Builder;
.super Ljava/lang/Object;
.source "BaseEventstream.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/eventstream/BaseEventstream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<AppEventT:",
        "Ljava/lang/Object;",
        "ServerEventT:",
        "Ljava/lang/Object;",
        "StateT:",
        "Ljava/lang/Object;",
        "EventStreamT:",
        "Lcom/squareup/eventstream/BaseEventstream<",
        "TAppEventT;TServerEventT;TStateT;>;SerializerT:",
        "Ljava/lang/Object;",
        "BuilderT:",
        "Lcom/squareup/eventstream/BaseEventstream$Builder<",
        "TAppEventT;TServerEventT;TStateT;TEventStreamT;TSerializerT;TBuilderT;>;>",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBaseEventstream.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BaseEventstream.kt\ncom/squareup/eventstream/BaseEventstream$Builder\n*L\n1#1,288:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008&\u0018\u0000*\u0004\u0008\u0003\u0010\u0001*\u0004\u0008\u0004\u0010\u0002*\u0004\u0008\u0005\u0010\u0003*\u001a\u0008\u0006\u0010\u0004*\u0014\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0005*\u0004\u0008\u0007\u0010\u0006*.\u0008\u0008\u0010\u0007*(\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0018\u00010\u00002\u00020\u0008BE\u0008\u0004\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00028\u0007\u0012\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00028\u00040\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u000c\u0012\u0006\u0010\u0013\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\u0014J\u000b\u0010\'\u001a\u00028\u0006\u00a2\u0006\u0002\u0010(J\u000e\u0010)\u001a\u0008\u0012\u0004\u0012\u00028\u00040*H$J(\u0010+\u001a\u0008\u0012\u0004\u0012\u00028\u00040,2\u0018\u0010-\u001a\u0014\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u00050.H$Ju\u0010/\u001a\u0014\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u00050.2\u0006\u0010\u0015\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00028\u00072\u0006\u0010&\u001a\u00020\u000c2\u0006\u0010$\u001a\u00020\u000c2\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u000c2\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u000c2\u0008\u0010#\u001a\u0004\u0018\u00010\u000c2\u0006\u0010\"\u001a\u00020\u000cH$\u00a2\u0006\u0002\u00100J=\u00101\u001a\u00028\u00062\u0006\u0010\u0016\u001a\u00020\u00172\u0018\u0010-\u001a\u0014\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u00050.2\u000c\u00102\u001a\u0008\u0012\u0004\u0012\u00028\u000403H$\u00a2\u0006\u0002\u00104J\u0013\u0010\u0016\u001a\u00028\u00082\u0006\u0010\u0016\u001a\u00020\u0017\u00a2\u0006\u0002\u00105J\u0013\u0010\u0018\u001a\u00028\u00082\u0006\u0010\u0018\u001a\u00020\u000c\u00a2\u0006\u0002\u00106J\u0013\u0010\u0019\u001a\u00028\u00082\u0006\u0010\u0019\u001a\u00020\u000c\u00a2\u0006\u0002\u00106J\u0013\u0010\u001b\u001a\u00028\u00082\u0006\u0010\u001b\u001a\u00020\u001c\u00a2\u0006\u0002\u00107J\u0013\u0010\u001d\u001a\u00028\u00082\u0006\u0010\u001d\u001a\u00020\u001e\u00a2\u0006\u0002\u00108J\u0013\u0010\"\u001a\u00028\u00082\u0006\u0010\"\u001a\u00020\u000c\u00a2\u0006\u0002\u00106J\u0013\u0010#\u001a\u00028\u00082\u0006\u0010#\u001a\u00020\u000c\u00a2\u0006\u0002\u00106J\u0013\u0010$\u001a\u00028\u00082\u0006\u0010$\u001a\u00020%\u00a2\u0006\u0002\u00109J\u0013\u0010&\u001a\u00028\u00082\u0006\u0010&\u001a\u00020\u000c\u00a2\u0006\u0002\u00106R\u000e\u0010\u0015\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00028\u00040\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u00028\u0007X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u001aR\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001f\u001a\u00028\u00088BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008 \u0010!R\u0010\u0010\"\u001a\u0004\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010#\u001a\u0004\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010&\u001a\u0004\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006:"
    }
    d2 = {
        "Lcom/squareup/eventstream/BaseEventstream$Builder;",
        "AppEventT",
        "ServerEventT",
        "StateT",
        "EventStreamT",
        "Lcom/squareup/eventstream/BaseEventstream;",
        "SerializerT",
        "BuilderT",
        "",
        "context",
        "Landroid/content/Context;",
        "appName",
        "",
        "buildType",
        "Lcom/squareup/eventstream/BaseEventstream$BuildType;",
        "jsonSerializer",
        "batchUploader",
        "Lcom/squareup/eventstream/EventBatchUploader;",
        "defaultQueueFileName",
        "loggingName",
        "(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/BaseEventstream$BuildType;Ljava/lang/Object;Lcom/squareup/eventstream/EventBatchUploader;Ljava/lang/String;Ljava/lang/String;)V",
        "appContext",
        "eventFactoryExecutor",
        "Ljava/util/concurrent/ExecutorService;",
        "gitSha",
        "installationId",
        "Ljava/lang/Object;",
        "log",
        "Lcom/squareup/eventstream/EventStreamLog;",
        "queueFile",
        "Ljava/io/File;",
        "self",
        "getSelf",
        "()Lcom/squareup/eventstream/BaseEventstream$Builder;",
        "sessionToken",
        "userAgent",
        "versionCode",
        "",
        "versionName",
        "build",
        "()Lcom/squareup/eventstream/BaseEventstream;",
        "createConverter",
        "Lcom/squareup/tape/FileObjectQueue$Converter;",
        "createDroppedEventsFactory",
        "Lcom/squareup/eventstream/DroppedEventsFactory;",
        "eventFactory",
        "Lcom/squareup/eventstream/EventFactory;",
        "createEventFactory",
        "(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/BaseEventstream$BuildType;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/EventFactory;",
        "createEventStream",
        "eventStore",
        "Lcom/squareup/eventstream/EventStore;",
        "(Ljava/util/concurrent/ExecutorService;Lcom/squareup/eventstream/EventFactory;Lcom/squareup/eventstream/EventStore;)Lcom/squareup/eventstream/BaseEventstream;",
        "(Ljava/util/concurrent/ExecutorService;)Lcom/squareup/eventstream/BaseEventstream$Builder;",
        "(Ljava/lang/String;)Lcom/squareup/eventstream/BaseEventstream$Builder;",
        "(Lcom/squareup/eventstream/EventStreamLog;)Lcom/squareup/eventstream/BaseEventstream$Builder;",
        "(Ljava/io/File;)Lcom/squareup/eventstream/BaseEventstream$Builder;",
        "(I)Lcom/squareup/eventstream/BaseEventstream$Builder;",
        "eventstream-common_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final appContext:Landroid/content/Context;

.field private final appName:Ljava/lang/String;

.field private final batchUploader:Lcom/squareup/eventstream/EventBatchUploader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/eventstream/EventBatchUploader<",
            "TServerEventT;>;"
        }
    .end annotation
.end field

.field private final buildType:Lcom/squareup/eventstream/BaseEventstream$BuildType;

.field private final defaultQueueFileName:Ljava/lang/String;

.field private eventFactoryExecutor:Ljava/util/concurrent/ExecutorService;

.field private gitSha:Ljava/lang/String;

.field private installationId:Ljava/lang/String;

.field private final jsonSerializer:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TSerializerT;"
        }
    .end annotation
.end field

.field private log:Lcom/squareup/eventstream/EventStreamLog;

.field private final loggingName:Ljava/lang/String;

.field private queueFile:Ljava/io/File;

.field private sessionToken:Ljava/lang/String;

.field private userAgent:Ljava/lang/String;

.field private versionCode:I

.field private versionName:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/BaseEventstream$BuildType;Ljava/lang/Object;Lcom/squareup/eventstream/EventBatchUploader;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/squareup/eventstream/BaseEventstream$BuildType;",
            "TSerializerT;",
            "Lcom/squareup/eventstream/EventBatchUploader<",
            "TServerEventT;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buildType"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "batchUploader"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultQueueFileName"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loggingName"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->appName:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->buildType:Lcom/squareup/eventstream/BaseEventstream$BuildType;

    iput-object p4, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->jsonSerializer:Ljava/lang/Object;

    iput-object p5, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->batchUploader:Lcom/squareup/eventstream/EventBatchUploader;

    iput-object p6, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->defaultQueueFileName:Ljava/lang/String;

    iput-object p7, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->loggingName:Ljava/lang/String;

    .line 120
    sget-object p2, Lcom/squareup/eventstream/EventStreamLog;->NONE:Lcom/squareup/eventstream/EventStreamLog;

    iput-object p2, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->log:Lcom/squareup/eventstream/EventStreamLog;

    .line 121
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "context.applicationContext"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->appContext:Landroid/content/Context;

    const/4 p1, -0x1

    .line 129
    iput p1, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->versionCode:I

    return-void
.end method

.method private final getSelf()Lcom/squareup/eventstream/BaseEventstream$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TBuilderT;"
        }
    .end annotation

    .line 184
    move-object v0, p0

    check-cast v0, Lcom/squareup/eventstream/BaseEventstream$Builder;

    return-object v0
.end method


# virtual methods
.method public final build()Lcom/squareup/eventstream/BaseEventstream;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TEventStreamT;"
        }
    .end annotation

    move-object/from16 v11, p0

    const/4 v0, 0x0

    .line 187
    move-object v1, v0

    check-cast v1, Landroid/content/pm/PackageInfo;

    .line 188
    iget-object v2, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->versionName:Ljava/lang/String;

    const/4 v3, -0x1

    const/4 v12, 0x0

    if-eqz v2, :cond_0

    iget v2, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->versionCode:I

    if-ne v2, v3, :cond_1

    .line 190
    :cond_0
    :try_start_0
    iget-object v2, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->appContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v4, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->appContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v12}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    .line 195
    :cond_1
    :goto_0
    iget-object v2, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->versionName:Ljava/lang/String;

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_3

    .line 196
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    goto :goto_1

    :cond_3
    move-object v2, v0

    :goto_1
    const-string/jumbo v4, "unknown"

    if-eqz v2, :cond_4

    move-object v5, v2

    goto :goto_2

    :cond_4
    move-object v5, v4

    .line 200
    :goto_2
    iget v2, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->versionCode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v3, :cond_5

    const/4 v3, 0x1

    goto :goto_3

    :cond_5
    const/4 v3, 0x0

    :goto_3
    if-eqz v3, :cond_6

    goto :goto_4

    :cond_6
    move-object v2, v0

    :goto_4
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    move-object v0, v2

    goto :goto_5

    :cond_7
    if-eqz v1, :cond_8

    .line 202
    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :cond_8
    :goto_5
    if-eqz v0, :cond_9

    move-object v6, v0

    goto :goto_6

    :cond_9
    move-object v6, v4

    .line 205
    :goto_6
    iget-object v0, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->eventFactoryExecutor:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_a

    goto :goto_7

    :cond_a
    sget-object v0, Lcom/squareup/eventstream/BaseEventstream;->Companion:Lcom/squareup/eventstream/BaseEventstream$Companion;

    iget-object v1, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->loggingName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/eventstream/BaseEventstream$Companion;->defaultExecutorService(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    :goto_7
    move-object v13, v0

    .line 206
    iget-object v0, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->queueFile:Ljava/io/File;

    if-eqz v0, :cond_b

    goto :goto_8

    :cond_b
    new-instance v0, Ljava/io/File;

    iget-object v1, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->appContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    iget-object v2, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->defaultQueueFileName:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :goto_8
    move-object v14, v0

    .line 207
    iget-object v0, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->log:Lcom/squareup/eventstream/EventStreamLog;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v14, v1, v12

    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v7

    const-string v2, "ES: Opening queue at %s. already exists? %s"

    invoke-interface {v0, v2, v1}, Lcom/squareup/eventstream/EventStreamLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 210
    new-instance v0, Lcom/squareup/eventstream/BaseEventstream$LoggingProcessor;

    iget-object v1, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->batchUploader:Lcom/squareup/eventstream/EventBatchUploader;

    iget-object v2, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->log:Lcom/squareup/eventstream/EventStreamLog;

    iget-object v3, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->loggingName:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/eventstream/BaseEventstream$LoggingProcessor;-><init>(Lcom/squareup/eventstream/EventBatchUploader;Lcom/squareup/eventstream/EventStreamLog;Ljava/lang/String;)V

    .line 209
    move-object/from16 v17, v0

    check-cast v17, Lcom/squareup/eventstream/EventBatchUploader;

    .line 212
    new-instance v15, Lcom/squareup/eventstream/QueueFactory;

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/eventstream/BaseEventstream$Builder;->createConverter()Lcom/squareup/tape/FileObjectQueue$Converter;

    move-result-object v0

    invoke-direct {v15, v14, v0}, Lcom/squareup/eventstream/QueueFactory;-><init>(Ljava/io/File;Lcom/squareup/tape/FileObjectQueue$Converter;)V

    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "batcher-queue-thread-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 215
    new-instance v1, Lcom/squareup/eventstream/BaseEventstream$Builder$build$queueExecutor$1;

    invoke-direct {v1, v0}, Lcom/squareup/eventstream/BaseEventstream$Builder$build$queueExecutor$1;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/util/concurrent/ThreadFactory;

    invoke-static {v1}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v10

    .line 219
    iget-object v0, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->sessionToken:Ljava/lang/String;

    if-eqz v0, :cond_c

    goto :goto_9

    :cond_c
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UUID.randomUUID().toString()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_9
    move-object/from16 v16, v0

    .line 222
    iget-object v1, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->appContext:Landroid/content/Context;

    .line 223
    iget-object v2, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->appName:Ljava/lang/String;

    .line 224
    iget-object v3, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->buildType:Lcom/squareup/eventstream/BaseEventstream$BuildType;

    .line 225
    iget-object v4, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->jsonSerializer:Ljava/lang/Object;

    .line 228
    iget-object v7, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->gitSha:Ljava/lang/String;

    .line 229
    iget-object v8, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->installationId:Ljava/lang/String;

    .line 230
    iget-object v9, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->userAgent:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v21, v10

    move-object/from16 v10, v16

    .line 221
    invoke-virtual/range {v0 .. v10}, Lcom/squareup/eventstream/BaseEventstream$Builder;->createEventFactory(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/BaseEventstream$BuildType;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/EventFactory;

    move-result-object v0

    .line 234
    invoke-virtual {v11, v0}, Lcom/squareup/eventstream/BaseEventstream$Builder;->createDroppedEventsFactory(Lcom/squareup/eventstream/EventFactory;)Lcom/squareup/eventstream/DroppedEventsFactory;

    move-result-object v20

    .line 236
    iget-object v1, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->appContext:Landroid/content/Context;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "-dropped"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 237
    new-instance v2, Lcom/squareup/eventstream/DroppedEventCounter;

    const-string v3, "droppedCountStorage"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->log:Lcom/squareup/eventstream/EventStreamLog;

    invoke-direct {v2, v1, v3}, Lcom/squareup/eventstream/DroppedEventCounter;-><init>(Landroid/content/SharedPreferences;Lcom/squareup/eventstream/EventStreamLog;)V

    .line 238
    new-instance v1, Lcom/squareup/eventstream/EventQueue;

    iget-object v3, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->log:Lcom/squareup/eventstream/EventStreamLog;

    const-string v4, "queueExecutor"

    move-object/from16 v5, v21

    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v15, v3, v5, v2}, Lcom/squareup/eventstream/EventQueue;-><init>(Lcom/squareup/eventstream/QueueFactory;Lcom/squareup/eventstream/EventStreamLog;Ljava/util/concurrent/ExecutorService;Lcom/squareup/eventstream/DroppedEventCounter;)V

    .line 239
    new-instance v3, Lcom/squareup/eventstream/UploadBatcher;

    .line 240
    iget-object v4, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->log:Lcom/squareup/eventstream/EventStreamLog;

    move-object v15, v3

    move-object/from16 v16, v1

    move-object/from16 v18, v4

    move-object/from16 v19, v2

    .line 239
    invoke-direct/range {v15 .. v20}, Lcom/squareup/eventstream/UploadBatcher;-><init>(Lcom/squareup/eventstream/EventQueue;Lcom/squareup/eventstream/EventBatchUploader;Lcom/squareup/eventstream/EventStreamLog;Lcom/squareup/eventstream/DroppedEventCounter;Lcom/squareup/eventstream/DroppedEventsFactory;)V

    .line 244
    new-instance v2, Lcom/squareup/eventstream/JobBatchScheduler;

    iget-object v4, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->appContext:Landroid/content/Context;

    iget-object v5, v11, Lcom/squareup/eventstream/BaseEventstream$Builder;->loggingName:Ljava/lang/String;

    invoke-direct {v2, v4, v5, v3}, Lcom/squareup/eventstream/JobBatchScheduler;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/UploadBatcher;)V

    .line 245
    new-instance v3, Lcom/squareup/eventstream/EventStore;

    sget-object v4, Lcom/squareup/eventstream/EventStore$Clock;->REAL:Lcom/squareup/eventstream/EventStore$Clock;

    invoke-direct {v3, v1, v2, v4}, Lcom/squareup/eventstream/EventStore;-><init>(Lcom/squareup/eventstream/EventQueue;Lcom/squareup/eventstream/JobBatchScheduler;Lcom/squareup/eventstream/EventStore$Clock;)V

    .line 247
    invoke-virtual {v11, v13, v0, v3}, Lcom/squareup/eventstream/BaseEventstream$Builder;->createEventStream(Ljava/util/concurrent/ExecutorService;Lcom/squareup/eventstream/EventFactory;Lcom/squareup/eventstream/EventStore;)Lcom/squareup/eventstream/BaseEventstream;

    move-result-object v0

    return-object v0
.end method

.method protected abstract createConverter()Lcom/squareup/tape/FileObjectQueue$Converter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "TServerEventT;>;"
        }
    .end annotation
.end method

.method protected abstract createDroppedEventsFactory(Lcom/squareup/eventstream/EventFactory;)Lcom/squareup/eventstream/DroppedEventsFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/eventstream/EventFactory<",
            "TServerEventT;TAppEventT;TStateT;>;)",
            "Lcom/squareup/eventstream/DroppedEventsFactory<",
            "TServerEventT;>;"
        }
    .end annotation
.end method

.method protected abstract createEventFactory(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/eventstream/BaseEventstream$BuildType;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/EventFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/squareup/eventstream/BaseEventstream$BuildType;",
            "TSerializerT;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/eventstream/EventFactory<",
            "TServerEventT;TAppEventT;TStateT;>;"
        }
    .end annotation
.end method

.method protected abstract createEventStream(Ljava/util/concurrent/ExecutorService;Lcom/squareup/eventstream/EventFactory;Lcom/squareup/eventstream/EventStore;)Lcom/squareup/eventstream/BaseEventstream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/squareup/eventstream/EventFactory<",
            "TServerEventT;TAppEventT;TStateT;>;",
            "Lcom/squareup/eventstream/EventStore<",
            "TServerEventT;>;)TEventStreamT;"
        }
    .end annotation
.end method

.method public final eventFactoryExecutor(Ljava/util/concurrent/ExecutorService;)Lcom/squareup/eventstream/BaseEventstream$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            ")TBuilderT;"
        }
    .end annotation

    const-string v0, "eventFactoryExecutor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    iput-object p1, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->eventFactoryExecutor:Ljava/util/concurrent/ExecutorService;

    .line 165
    invoke-direct {p0}, Lcom/squareup/eventstream/BaseEventstream$Builder;->getSelf()Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object p1

    return-object p1
.end method

.method public final gitSha(Ljava/lang/String;)Lcom/squareup/eventstream/BaseEventstream$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TBuilderT;"
        }
    .end annotation

    const-string v0, "gitSha"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    iput-object p1, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->gitSha:Ljava/lang/String;

    .line 138
    invoke-direct {p0}, Lcom/squareup/eventstream/BaseEventstream$Builder;->getSelf()Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object p1

    return-object p1
.end method

.method public final installationId(Ljava/lang/String;)Lcom/squareup/eventstream/BaseEventstream$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TBuilderT;"
        }
    .end annotation

    const-string v0, "installationId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    iput-object p1, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->installationId:Ljava/lang/String;

    .line 143
    invoke-direct {p0}, Lcom/squareup/eventstream/BaseEventstream$Builder;->getSelf()Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object p1

    return-object p1
.end method

.method public final log(Lcom/squareup/eventstream/EventStreamLog;)Lcom/squareup/eventstream/BaseEventstream$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/eventstream/EventStreamLog;",
            ")TBuilderT;"
        }
    .end annotation

    const-string v0, "log"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    iput-object p1, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->log:Lcom/squareup/eventstream/EventStreamLog;

    .line 133
    invoke-direct {p0}, Lcom/squareup/eventstream/BaseEventstream$Builder;->getSelf()Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object p1

    return-object p1
.end method

.method public final queueFile(Ljava/io/File;)Lcom/squareup/eventstream/BaseEventstream$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")TBuilderT;"
        }
    .end annotation

    const-string v0, "queueFile"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    iput-object p1, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->queueFile:Ljava/io/File;

    .line 170
    invoke-direct {p0}, Lcom/squareup/eventstream/BaseEventstream$Builder;->getSelf()Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object p1

    return-object p1
.end method

.method public final sessionToken(Ljava/lang/String;)Lcom/squareup/eventstream/BaseEventstream$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TBuilderT;"
        }
    .end annotation

    const-string v0, "sessionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    iput-object p1, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->sessionToken:Ljava/lang/String;

    .line 179
    invoke-direct {p0}, Lcom/squareup/eventstream/BaseEventstream$Builder;->getSelf()Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object p1

    return-object p1
.end method

.method public final userAgent(Ljava/lang/String;)Lcom/squareup/eventstream/BaseEventstream$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TBuilderT;"
        }
    .end annotation

    const-string/jumbo v0, "userAgent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    iput-object p1, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->userAgent:Ljava/lang/String;

    .line 148
    invoke-direct {p0}, Lcom/squareup/eventstream/BaseEventstream$Builder;->getSelf()Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object p1

    return-object p1
.end method

.method public final versionCode(I)Lcom/squareup/eventstream/BaseEventstream$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TBuilderT;"
        }
    .end annotation

    .line 159
    iput p1, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->versionCode:I

    .line 160
    invoke-direct {p0}, Lcom/squareup/eventstream/BaseEventstream$Builder;->getSelf()Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object p1

    return-object p1
.end method

.method public final versionName(Ljava/lang/String;)Lcom/squareup/eventstream/BaseEventstream$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TBuilderT;"
        }
    .end annotation

    const-string/jumbo v0, "versionName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    iput-object p1, p0, Lcom/squareup/eventstream/BaseEventstream$Builder;->versionName:Ljava/lang/String;

    .line 154
    invoke-direct {p0}, Lcom/squareup/eventstream/BaseEventstream$Builder;->getSelf()Lcom/squareup/eventstream/BaseEventstream$Builder;

    move-result-object p1

    return-object p1
.end method
