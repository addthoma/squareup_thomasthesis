.class public final Lcom/squareup/eventstream/v2/EventstreamV2;
.super Lcom/squareup/eventstream/BaseEventstream;
.source "EventstreamV2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/eventstream/v2/EventstreamV2$AppState;,
        Lcom/squareup/eventstream/v2/EventstreamV2$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/eventstream/BaseEventstream<",
        "Lcom/squareup/eventstream/v2/AppEvent;",
        "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
        "Lcom/squareup/eventstream/v2/EventstreamV2$AppState;",
        ">;"
    }
.end annotation


# static fields
.field private static final EVENTSTREAM2_QUEUE_FILENAME:Ljava/lang/String; = "eventstreamv2.queue"


# direct methods
.method constructor <init>(Ljava/util/concurrent/ExecutorService;Lcom/squareup/eventstream/EventFactory;Lcom/squareup/eventstream/EventStore;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/squareup/eventstream/EventFactory<",
            "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
            "Lcom/squareup/eventstream/v2/AppEvent;",
            "Lcom/squareup/eventstream/v2/EventstreamV2$AppState;",
            ">;",
            "Lcom/squareup/eventstream/EventStore<",
            "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/eventstream/BaseEventstream;-><init>(Ljava/util/concurrent/ExecutorService;Lcom/squareup/eventstream/EventFactory;Lcom/squareup/eventstream/EventStore;)V

    return-void
.end method
