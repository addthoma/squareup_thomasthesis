.class public Lcom/squareup/giftcard/GiftCards;
.super Ljava/lang/Object;
.source "GiftCards.java"


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/giftcard/GiftCards;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 25
    iput-object p2, p0, Lcom/squareup/giftcard/GiftCards;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private matchesBin(Ljava/lang/String;)Z
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/squareup/giftcard/GiftCards;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardBins()Ljava/util/List;

    move-result-object v0

    .line 115
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 116
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public getCardTypeWarning(Lcom/squareup/Card;Z)Lcom/squareup/widgets/warning/Warning;
    .locals 1

    .line 92
    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result p1

    if-eqz p2, :cond_0

    if-nez p1, :cond_0

    .line 95
    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    sget p2, Lcom/squareup/giftcard/R$string;->cnp_invalid_gift_card_title:I

    sget v0, Lcom/squareup/giftcard/R$string;->cnp_invalid_gift_card_message:I

    invoke-direct {p1, p2, v0}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    return-object p1

    :cond_0
    if-nez p2, :cond_1

    if-eqz p1, :cond_1

    .line 100
    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    sget p2, Lcom/squareup/giftcard/R$string;->cnp_invalid_credit_card_title:I

    sget v0, Lcom/squareup/giftcard/R$string;->cnp_invalid_credit_card_message:I

    invoke-direct {p1, p2, v0}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public isPossiblyGiftCard(Lcom/squareup/Card;)Z
    .locals 5

    .line 37
    iget-object v0, p0, Lcom/squareup/giftcard/GiftCards;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/GiftCardSettings;->canUseGiftCards()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 42
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v0

    .line 43
    sget-object v2, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    if-eq v0, v2, :cond_1

    return v1

    .line 48
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/giftcard/GiftCards;->isValidThirdPartyGiftCardPan(Ljava/lang/String;)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    return v2

    .line 54
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 55
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x4

    if-le v3, v4, :cond_3

    invoke-direct {p0, v0}, Lcom/squareup/giftcard/GiftCards;->matchesBin(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    return v1

    .line 60
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/Card;->getName()Ljava/lang/String;

    move-result-object p1

    .line 61
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 63
    iget-object v0, p0, Lcom/squareup/giftcard/GiftCards;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardNames()Ljava/util/List;

    move-result-object v0

    .line 64
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_6

    .line 66
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v3, 0x0

    :cond_4
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 67
    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v3, 0x1

    goto :goto_0

    :cond_5
    if-nez v3, :cond_6

    return v1

    :cond_6
    return v2
.end method

.method public isSquareGiftCard(Ljava/lang/String;)Z
    .locals 2

    if-eqz p1, :cond_0

    .line 78
    sget-object v0, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/Card$Brand;->isValidNumberLength(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    .line 79
    invoke-virtual {v0, p1}, Lcom/squareup/Card$Brand;->validateLuhnIfRequired(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/squareup/giftcard/GiftCards;->matchesBin(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isThirdPartyGiftCardFeatureEnabled()Z
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/giftcard/GiftCards;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_THIRD_PARTY_REFACTOR:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/giftcard/GiftCards;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lcom/squareup/settings/server/GiftCardSettings;->canAcceptThirdPartyGiftCards()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isValidThirdPartyGiftCardPan(Ljava/lang/String;)Z
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/giftcard/GiftCards;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/GiftCardSettings;->canAcceptThirdPartyGiftCards()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
