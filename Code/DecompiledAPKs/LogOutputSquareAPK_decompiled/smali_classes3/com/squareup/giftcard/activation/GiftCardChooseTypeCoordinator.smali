.class public final Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "GiftCardChooseTypeCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGiftCardChooseTypeCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GiftCardChooseTypeCoordinator.kt\ncom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,54:1\n1103#2,7:55\n1103#2,7:62\n1103#2,7:69\n*E\n*S KotlinDebug\n*F\n+ 1 GiftCardChooseTypeCoordinator.kt\ncom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator\n*L\n37#1,7:55\n38#1,7:62\n39#1,7:69\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen$Runner;",
        "res",
        "Lcom/squareup/util/Res;",
        "maybeX2SellerScreenRunner",
        "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
        "(Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)V",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maybeX2SellerScreenRunner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator;->runner:Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen$Runner;

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    return-void
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator;)Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen$Runner;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator;->runner:Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen$Runner;

    return-object p0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 6

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 27
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/librarylist/R$string;->item_library_all_gift_cards:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 26
    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 29
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator$attach$1;-><init>(Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 31
    sget v0, Lcom/squareup/giftcard/activation/R$id;->gift_card_type_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 32
    sget v1, Lcom/squareup/giftcard/activation/R$id;->gift_card_progress_bar:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 33
    sget v2, Lcom/squareup/giftcard/activation/R$id;->gift_card_type_physical:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/noho/NohoRow;

    .line 34
    sget v3, Lcom/squareup/giftcard/activation/R$id;->gift_card_type_egc:I

    invoke-static {p1, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/noho/NohoRow;

    .line 35
    sget v4, Lcom/squareup/giftcard/activation/R$id;->gift_card_type_check_balance:I

    invoke-static {p1, v4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/squareup/noho/NohoRow;

    .line 37
    check-cast v2, Landroid/view/View;

    .line 55
    new-instance v5, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator$attach$$inlined$onClickDebounced$1;

    invoke-direct {v5, p0}, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator$attach$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator;)V

    check-cast v5, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    check-cast v3, Landroid/view/View;

    .line 62
    new-instance v2, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator$attach$$inlined$onClickDebounced$2;

    invoke-direct {v2, p0}, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator$attach$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    check-cast v4, Landroid/view/View;

    .line 69
    new-instance v2, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator$attach$$inlined$onClickDebounced$3;

    invoke-direct {v2, p0}, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator$attach$$inlined$onClickDebounced$3;-><init>(Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator;->runner:Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen$Runner;

    invoke-interface {v2}, Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen$Runner;->chooseTypeBusy()Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator$attach$5;

    invoke-direct {v3, v0, v1}, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator$attach$5;-><init>(Landroid/view/View;Landroid/view/View;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, p1, v3}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    .line 48
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator;->runner:Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen$Runner;->onCardSwipedInChooseTypeScreen()Lrx/Observable;

    move-result-object v0

    .line 49
    sget-object v1, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator$attach$6;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator$attach$6;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    .line 51
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardChooseTypeCoordinator;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->displayGiftCardBalanceCheck()Z

    return-void
.end method
