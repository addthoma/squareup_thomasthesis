.class final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$goBackUntilChooseTypeScreen$1;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeRunner.kt"

# interfaces
.implements Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->goBackUntilChooseTypeScreen(Lflow/Flow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGiftCardLoadingScopeRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GiftCardLoadingScopeRunner.kt\ncom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$goBackUntilChooseTypeScreen$1\n*L\n1#1,655:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/Command;",
        "kotlin.jvm.PlatformType",
        "history",
        "Lflow/History;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$goBackUntilChooseTypeScreen$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$goBackUntilChooseTypeScreen$1;

    invoke-direct {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$goBackUntilChooseTypeScreen$1;-><init>()V

    sput-object v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$goBackUntilChooseTypeScreen$1;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$goBackUntilChooseTypeScreen$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lflow/History;)Lcom/squareup/container/Command;
    .locals 3

    const-string v0, "history"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 361
    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    .line 362
    const-class v1, Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lcom/squareup/container/Histories;->popUntil(Lflow/History$Builder;[Ljava/lang/Class;)Lflow/History$Builder;

    .line 363
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    sget-object v0, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-static {p1, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method
