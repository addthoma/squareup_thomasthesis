.class public final Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinatorKt;
.super Ljava/lang/Object;
.source "GiftCardHistoryCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\"\u0019\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "EMPTY_GIFT_CARD_HISTORY",
        "Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;",
        "kotlin.jvm.PlatformType",
        "getEMPTY_GIFT_CARD_HISTORY",
        "()Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;",
        "giftcard-activation_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final EMPTY_GIFT_CARD_HISTORY:Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse$Builder;->build()Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;

    move-result-object v0

    sput-object v0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinatorKt;->EMPTY_GIFT_CARD_HISTORY:Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;

    return-void
.end method

.method public static final getEMPTY_GIFT_CARD_HISTORY()Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;
    .locals 1

    .line 32
    sget-object v0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinatorKt;->EMPTY_GIFT_CARD_HISTORY:Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;

    return-object v0
.end method
