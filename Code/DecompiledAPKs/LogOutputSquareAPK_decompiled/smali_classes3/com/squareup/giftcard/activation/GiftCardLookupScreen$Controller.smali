.class public interface abstract Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;
.super Ljava/lang/Object;
.source "GiftCardLookupScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/giftcard/activation/GiftCardLookupScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Controller"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&J\u000e\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H&J\u0008\u0010\t\u001a\u00020\u0005H&J\u0010\u0010\n\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0008H&J\u0008\u0010\u000b\u001a\u00020\u0003H&J\u0010\u0010\u000c\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0008H&J\u000e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H&J\u0010\u0010\u000e\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0008H&\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;",
        "",
        "actionBarHasX",
        "",
        "cancelGiftCardLookup",
        "",
        "card",
        "Lrx/Observable;",
        "Lcom/squareup/Card;",
        "continueGiftCardLookup",
        "continueGiftCardLookupIfValid",
        "isPlasticCardFeatureEnabled",
        "isValidCard",
        "onCardSwiped",
        "setCard",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract actionBarHasX()Z
.end method

.method public abstract cancelGiftCardLookup()V
.end method

.method public abstract card()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/Card;",
            ">;"
        }
    .end annotation
.end method

.method public abstract continueGiftCardLookup()V
.end method

.method public abstract continueGiftCardLookupIfValid(Lcom/squareup/Card;)V
.end method

.method public abstract isPlasticCardFeatureEnabled()Z
.end method

.method public abstract isValidCard(Lcom/squareup/Card;)Z
.end method

.method public abstract onCardSwiped()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/Card;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setCard(Lcom/squareup/Card;)V
.end method
