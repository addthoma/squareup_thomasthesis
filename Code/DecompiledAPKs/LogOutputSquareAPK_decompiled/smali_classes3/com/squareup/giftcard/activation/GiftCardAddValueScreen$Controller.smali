.class public interface abstract Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;
.super Ljava/lang/Object;
.source "GiftCardAddValueScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/giftcard/activation/GiftCardAddValueScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Controller"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH&J\u000e\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH&J\u0008\u0010\r\u001a\u00020\u0003H&J\u0008\u0010\u000e\u001a\u00020\u0003H&J\u000e\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000bH&J\u000e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u000bH&\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;",
        "",
        "addGiftCardToTransaction",
        "",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "giftCard",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        "activityType",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;",
        "busy",
        "Lrx/Observable;",
        "",
        "cancelAddValueScreen",
        "finishAddValueAndCloseActivationFlow",
        "giftCardName",
        "",
        "registerGiftCard",
        "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract addGiftCardToTransaction(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;)V
.end method

.method public abstract busy()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract cancelAddValueScreen()V
.end method

.method public abstract finishAddValueAndCloseActivationFlow()V
.end method

.method public abstract giftCardName()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract registerGiftCard()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
            ">;"
        }
    .end annotation
.end method
