.class public final Lcom/squareup/giftcard/activation/GiftCardLoadingScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "GiftCardLoadingScope.kt"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;
.implements Lcom/squareup/cart/util/PartOfCartScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/giftcard/activation/GiftCardLoadingScope$ParentComponent;,
        Lcom/squareup/giftcard/activation/GiftCardLoadingScope$Component;,
        Lcom/squareup/giftcard/activation/GiftCardLoadingScope$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGiftCardLoadingScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GiftCardLoadingScope.kt\ncom/squareup/giftcard/activation/GiftCardLoadingScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,80:1\n35#2:81\n*E\n*S KotlinDebug\n*F\n+ 1 GiftCardLoadingScope.kt\ncom/squareup/giftcard/activation/GiftCardLoadingScope\n*L\n50#1:81\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0008\u0087\u0008\u0018\u0000 \u001c2\u00020\u00012\u00020\u00022\u00020\u0003:\u0003\u001c\u001d\u001eB\u0017\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0007H\u00c2\u0003J\u001f\u0010\u0011\u001a\u00020\u00002\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0018\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0014J\u0010\u0010\u0018\u001a\u00020\u00132\u0006\u0010\u0019\u001a\u00020\u000eH\u0016J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/GiftCardLoadingScope;",
        "Lcom/squareup/ui/main/InMainActivityScope;",
        "Lcom/squareup/container/RegistersInScope;",
        "Lcom/squareup/cart/util/PartOfCartScope;",
        "maybeGiftCard",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        "parentKey",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/ui/main/RegisterTreeKey;)V",
        "getMaybeGiftCard",
        "()Lcom/squareup/protos/client/giftcards/GiftCard;",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "component1",
        "component2",
        "copy",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "register",
        "scope",
        "toString",
        "",
        "Companion",
        "Component",
        "ParentComponent",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/giftcard/activation/GiftCardLoadingScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/giftcard/activation/GiftCardLoadingScope$Companion;


# instance fields
.field private final maybeGiftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScope$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->Companion:Lcom/squareup/giftcard/activation/GiftCardLoadingScope$Companion;

    .line 72
    sget-object v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScope$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "PathCreator.fromParcel {\u2026ssLoader)!!\n      )\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const-string v0, "parentKey"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->maybeGiftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method private final component2()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/giftcard/activation/GiftCardLoadingScope;Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/ui/main/RegisterTreeKey;ILjava/lang/Object;)Lcom/squareup/giftcard/activation/GiftCardLoadingScope;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->maybeGiftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->copy(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/ui/main/RegisterTreeKey;)Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-super {p0, p1}, Lcom/squareup/ui/main/InMainActivityScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 56
    const-class v1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner$ParentComponent;

    .line 57
    invoke-interface {p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner$ParentComponent;->activateEGiftCardWorkflowRunner()Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;

    move-result-object p1

    const-string v1, "builder"

    .line 58
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    return-object v0
.end method

.method public final component1()Lcom/squareup/protos/client/giftcards/GiftCard;
    .locals 1

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->maybeGiftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/ui/main/RegisterTreeKey;)Lcom/squareup/giftcard/activation/GiftCardLoadingScope;
    .locals 1

    const-string v0, "parentKey"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;

    invoke-direct {v0, p1, p2}, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;-><init>(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method

.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->maybeGiftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public final getMaybeGiftCard()Lcom/squareup/protos/client/giftcards/GiftCard;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->maybeGiftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    const-class v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope$Component;

    .line 50
    invoke-interface {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScope$Component;->GiftCardLoadingScopeRunner()Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    move-result-object v0

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GiftCardLoadingScope(maybeGiftCard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->maybeGiftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", parentKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
