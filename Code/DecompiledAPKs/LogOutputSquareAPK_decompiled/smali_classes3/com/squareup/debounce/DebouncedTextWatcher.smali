.class public abstract Lcom/squareup/debounce/DebouncedTextWatcher;
.super Ljava/lang/Object;
.source "DebouncedTextWatcher.java"

# interfaces
.implements Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 38
    invoke-static {}, Lcom/squareup/debounce/Debouncers;->canPerform()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {p0, p1}, Lcom/squareup/debounce/DebouncedTextWatcher;->doAfterTextChanged(Landroid/text/Editable;)V

    :cond_0
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .line 26
    invoke-static {}, Lcom/squareup/debounce/Debouncers;->canPerform()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/debounce/DebouncedTextWatcher;->doBeforeTextChanged(Ljava/lang/CharSequence;III)V

    :cond_0
    return-void
.end method

.method public doAfterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public doBeforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public doTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .line 32
    invoke-static {}, Lcom/squareup/debounce/Debouncers;->canPerform()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/debounce/DebouncedTextWatcher;->doTextChanged(Ljava/lang/CharSequence;III)V

    :cond_0
    return-void
.end method
