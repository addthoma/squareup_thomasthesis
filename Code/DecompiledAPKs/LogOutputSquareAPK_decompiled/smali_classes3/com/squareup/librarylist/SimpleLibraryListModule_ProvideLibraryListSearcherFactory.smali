.class public final Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListSearcherFactory;
.super Ljava/lang/Object;
.source "SimpleLibraryListModule_ProvideLibraryListSearcherFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/librarylist/LibraryListSearcher;",
        ">;"
    }
.end annotation


# instance fields
.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/librarylist/SimpleLibraryListModule;


# direct methods
.method public constructor <init>(Lcom/squareup/librarylist/SimpleLibraryListModule;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/SimpleLibraryListModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListSearcherFactory;->module:Lcom/squareup/librarylist/SimpleLibraryListModule;

    .line 25
    iput-object p2, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListSearcherFactory;->cogsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/librarylist/SimpleLibraryListModule;Ljavax/inject/Provider;)Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListSearcherFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/SimpleLibraryListModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;)",
            "Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListSearcherFactory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListSearcherFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListSearcherFactory;-><init>(Lcom/squareup/librarylist/SimpleLibraryListModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideLibraryListSearcher(Lcom/squareup/librarylist/SimpleLibraryListModule;Lcom/squareup/cogs/Cogs;)Lcom/squareup/librarylist/LibraryListSearcher;
    .locals 0

    .line 40
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/SimpleLibraryListModule;->provideLibraryListSearcher(Lcom/squareup/cogs/Cogs;)Lcom/squareup/librarylist/LibraryListSearcher;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/librarylist/LibraryListSearcher;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/librarylist/LibraryListSearcher;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListSearcherFactory;->module:Lcom/squareup/librarylist/SimpleLibraryListModule;

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListSearcherFactory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cogs/Cogs;

    invoke-static {v0, v1}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListSearcherFactory;->provideLibraryListSearcher(Lcom/squareup/librarylist/SimpleLibraryListModule;Lcom/squareup/cogs/Cogs;)Lcom/squareup/librarylist/LibraryListSearcher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListSearcherFactory;->get()Lcom/squareup/librarylist/LibraryListSearcher;

    move-result-object v0

    return-object v0
.end method
