.class public final Lcom/squareup/librarylist/ThumbnailLoader;
.super Ljava/lang/Object;
.source "ThumbnailLoader.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nThumbnailLoader.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ThumbnailLoader.kt\ncom/squareup/librarylist/ThumbnailLoader\n*L\n1#1,154:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0008\u0018\u00002\u00020\u0001:\u0001\"B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017J\u0016\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bJ\u0010\u0010\u0014\u001a\u00020\u00152\u0008\u0008\u0001\u0010\u001c\u001a\u00020\u0010J\u000e\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u001d\u001a\u00020\u001eJ\u0008\u0010\u001f\u001a\u00020\u0015H\u0002J\u000c\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u00130!R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0010\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\tR\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000c\u001a\n \u000e*\u0004\u0018\u00010\r0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0011\u001a\u0010\u0012\u000c\u0012\n \u000e*\u0004\u0018\u00010\u00130\u00130\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/librarylist/ThumbnailLoader;",
        "",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "getContext",
        "()Landroid/content/Context;",
        "imageLoadingTarget",
        "com/squareup/librarylist/ThumbnailLoader$imageLoadingTarget$1",
        "Lcom/squareup/librarylist/ThumbnailLoader$imageLoadingTarget$1;",
        "itemPhoto",
        "Lcom/squareup/ui/photo/ItemPhoto;",
        "resources",
        "Landroid/content/res/Resources;",
        "kotlin.jvm.PlatformType",
        "thumbSize",
        "",
        "thumbnailRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;",
        "loadThumbnail",
        "",
        "drawable",
        "Landroid/graphics/drawable/Drawable;",
        "entry",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "itemPhotoFactory",
        "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
        "drawableRes",
        "text",
        "",
        "maybeCancelImageLoading",
        "thumbnailData",
        "Lio/reactivex/Observable;",
        "ThumbnailData",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final imageLoadingTarget:Lcom/squareup/librarylist/ThumbnailLoader$imageLoadingTarget$1;

.field private itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

.field private final resources:Landroid/content/res/Resources;

.field private final thumbSize:I

.field private final thumbnailRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/ThumbnailLoader;->context:Landroid/content/Context;

    .line 47
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string v0, "BehaviorRelay.create<ThumbnailData>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/librarylist/ThumbnailLoader;->thumbnailRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 48
    iget-object p1, p0, Lcom/squareup/librarylist/ThumbnailLoader;->context:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/librarylist/ThumbnailLoader;->resources:Landroid/content/res/Resources;

    .line 50
    iget-object p1, p0, Lcom/squareup/librarylist/ThumbnailLoader;->resources:Landroid/content/res/Resources;

    sget v0, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/librarylist/ThumbnailLoader;->thumbSize:I

    .line 52
    new-instance p1, Lcom/squareup/librarylist/ThumbnailLoader$imageLoadingTarget$1;

    invoke-direct {p1, p0}, Lcom/squareup/librarylist/ThumbnailLoader$imageLoadingTarget$1;-><init>(Lcom/squareup/librarylist/ThumbnailLoader;)V

    iput-object p1, p0, Lcom/squareup/librarylist/ThumbnailLoader;->imageLoadingTarget:Lcom/squareup/librarylist/ThumbnailLoader$imageLoadingTarget$1;

    return-void
.end method

.method public static final synthetic access$getResources$p(Lcom/squareup/librarylist/ThumbnailLoader;)Landroid/content/res/Resources;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/librarylist/ThumbnailLoader;->resources:Landroid/content/res/Resources;

    return-object p0
.end method

.method public static final synthetic access$getThumbnailRelay$p(Lcom/squareup/librarylist/ThumbnailLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/librarylist/ThumbnailLoader;->thumbnailRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method private final maybeCancelImageLoading()V
    .locals 2

    .line 148
    iget-object v0, p0, Lcom/squareup/librarylist/ThumbnailLoader;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    if-eqz v0, :cond_1

    if-nez v0, :cond_0

    .line 149
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v1, p0, Lcom/squareup/librarylist/ThumbnailLoader;->imageLoadingTarget:Lcom/squareup/librarylist/ThumbnailLoader$imageLoadingTarget$1;

    check-cast v1, Lcom/squareup/picasso/Target;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/photo/ItemPhoto;->cancel(Lcom/squareup/picasso/Target;)V

    const/4 v0, 0x0

    .line 150
    check-cast v0, Lcom/squareup/ui/photo/ItemPhoto;

    iput-object v0, p0, Lcom/squareup/librarylist/ThumbnailLoader;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    :cond_1
    return-void
.end method


# virtual methods
.method public final getContext()Landroid/content/Context;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/librarylist/ThumbnailLoader;->context:Landroid/content/Context;

    return-object v0
.end method

.method public final loadThumbnail(I)V
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/librarylist/ThumbnailLoader;->thumbnailRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 98
    new-instance v1, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$ResourceThumbnailData;

    invoke-direct {v1, p1}, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$ResourceThumbnailData;-><init>(I)V

    .line 97
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final loadThumbnail(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    const-string v0, "drawable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/librarylist/ThumbnailLoader;->thumbnailRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 89
    new-instance v1, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$LiteralThumbnailData;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$LiteralThumbnailData;-><init>(Landroid/graphics/drawable/Drawable;Z)V

    .line 88
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final loadThumbnail(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V
    .locals 4

    const-string v0, "entry"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemPhotoFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-direct {p0}, Lcom/squareup/librarylist/ThumbnailLoader;->maybeCancelImageLoading()V

    .line 110
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v0

    if-eqz v0, :cond_4

    sget-object v1, Lcom/squareup/librarylist/ThumbnailLoader$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 118
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->isGiftCard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getColor()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/librarylist/ThumbnailLoader;->resources:Landroid/content/res/Resources;

    invoke-static {v0, v1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forGiftCard(Ljava/lang/String;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 122
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getAbbreviation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getColor()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/squareup/librarylist/ThumbnailLoader;->thumbSize:I

    iget-object v3, p0, Lcom/squareup/librarylist/ThumbnailLoader;->context:Landroid/content/Context;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forItem(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    :goto_0
    const-string v1, "if (entry.isGiftCard) {\n\u2026bSize, context)\n        }"

    .line 118
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 115
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getAbbreviation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getColor()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/librarylist/ThumbnailLoader;->context:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forItem(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    move-result-object v0

    const-string v1, "forItem(entry.abbreviation, entry.color, context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/graphics/drawable/Drawable;

    goto :goto_1

    .line 112
    :cond_2
    iget-object v0, p0, Lcom/squareup/librarylist/ThumbnailLoader;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forDiscount(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const-string v1, "forDiscount(context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    :goto_1
    iget-object v1, p0, Lcom/squareup/librarylist/ThumbnailLoader;->thumbnailRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 130
    new-instance v2, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$LiteralThumbnailData;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$LiteralThumbnailData;-><init>(Landroid/graphics/drawable/Drawable;Z)V

    .line 129
    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 138
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v0, v1, :cond_3

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->isGiftCard()Z

    move-result v0

    if-nez v0, :cond_3

    .line 139
    invoke-virtual {p2, p1}, Lcom/squareup/ui/photo/ItemPhoto$Factory;->get(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Lcom/squareup/ui/photo/ItemPhoto;

    move-result-object p1

    .line 141
    iget p2, p0, Lcom/squareup/librarylist/ThumbnailLoader;->thumbSize:I

    iget-object v0, p0, Lcom/squareup/librarylist/ThumbnailLoader;->imageLoadingTarget:Lcom/squareup/librarylist/ThumbnailLoader$imageLoadingTarget$1;

    check-cast v0, Lcom/squareup/picasso/Target;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/photo/ItemPhoto;->into(ILcom/squareup/picasso/Target;)V

    .line 140
    iput-object p1, p0, Lcom/squareup/librarylist/ThumbnailLoader;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    :cond_3
    return-void

    .line 126
    :cond_4
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unrecognized entry type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public final loadThumbnail(Ljava/lang/String;)V
    .locals 2

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/librarylist/ThumbnailLoader;->thumbnailRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 81
    new-instance v1, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$TextThumbnailData;

    invoke-direct {v1, p1}, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$TextThumbnailData;-><init>(Ljava/lang/String;)V

    .line 80
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final thumbnailData()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;",
            ">;"
        }
    .end annotation

    .line 77
    iget-object v0, p0, Lcom/squareup/librarylist/ThumbnailLoader;->thumbnailRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
