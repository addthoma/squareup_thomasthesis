.class public final Lcom/squareup/librarylist/LibraryListResults;
.super Ljava/lang/Object;
.source "LibraryListManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0010\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\u000f\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\nH\u00c6\u0003J7\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\n2\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0010R\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryListResults;",
        "",
        "currentState",
        "Lcom/squareup/librarylist/LibraryListState;",
        "cursor",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
        "placeholders",
        "",
        "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
        "isLibraryEmpty",
        "",
        "(Lcom/squareup/librarylist/LibraryListState;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/List;Z)V",
        "getCurrentState",
        "()Lcom/squareup/librarylist/LibraryListState;",
        "getCursor",
        "()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
        "()Z",
        "getPlaceholders",
        "()Ljava/util/List;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentState:Lcom/squareup/librarylist/LibraryListState;

.field private final cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

.field private final isLibraryEmpty:Z

.field private final placeholders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/librarylist/LibraryListState;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/LibraryListState;",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "currentState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cursor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "placeholders"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/LibraryListResults;->currentState:Lcom/squareup/librarylist/LibraryListState;

    iput-object p2, p0, Lcom/squareup/librarylist/LibraryListResults;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    iput-object p3, p0, Lcom/squareup/librarylist/LibraryListResults;->placeholders:Ljava/util/List;

    iput-boolean p4, p0, Lcom/squareup/librarylist/LibraryListResults;->isLibraryEmpty:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/librarylist/LibraryListResults;Lcom/squareup/librarylist/LibraryListState;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/List;ZILjava/lang/Object;)Lcom/squareup/librarylist/LibraryListResults;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/librarylist/LibraryListResults;->currentState:Lcom/squareup/librarylist/LibraryListState;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/librarylist/LibraryListResults;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/librarylist/LibraryListResults;->placeholders:Ljava/util/List;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-boolean p4, p0, Lcom/squareup/librarylist/LibraryListResults;->isLibraryEmpty:Z

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/librarylist/LibraryListResults;->copy(Lcom/squareup/librarylist/LibraryListState;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/List;Z)Lcom/squareup/librarylist/LibraryListResults;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/librarylist/LibraryListState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListResults;->currentState:Lcom/squareup/librarylist/LibraryListState;

    return-object v0
.end method

.method public final component2()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListResults;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListResults;->placeholders:Ljava/util/List;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/librarylist/LibraryListResults;->isLibraryEmpty:Z

    return v0
.end method

.method public final copy(Lcom/squareup/librarylist/LibraryListState;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/List;Z)Lcom/squareup/librarylist/LibraryListResults;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/LibraryListState;",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;Z)",
            "Lcom/squareup/librarylist/LibraryListResults;"
        }
    .end annotation

    const-string v0, "currentState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cursor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "placeholders"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/librarylist/LibraryListResults;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/librarylist/LibraryListResults;-><init>(Lcom/squareup/librarylist/LibraryListState;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/List;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/librarylist/LibraryListResults;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/librarylist/LibraryListResults;

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListResults;->currentState:Lcom/squareup/librarylist/LibraryListState;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListResults;->currentState:Lcom/squareup/librarylist/LibraryListState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListResults;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListResults;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListResults;->placeholders:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListResults;->placeholders:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/librarylist/LibraryListResults;->isLibraryEmpty:Z

    iget-boolean p1, p1, Lcom/squareup/librarylist/LibraryListResults;->isLibraryEmpty:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCurrentState()Lcom/squareup/librarylist/LibraryListState;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListResults;->currentState:Lcom/squareup/librarylist/LibraryListState;

    return-object v0
.end method

.method public final getCursor()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListResults;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    return-object v0
.end method

.method public final getPlaceholders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListResults;->placeholders:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListResults;->currentState:Lcom/squareup/librarylist/LibraryListState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListResults;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListResults;->placeholders:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/librarylist/LibraryListResults;->isLibraryEmpty:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public final isLibraryEmpty()Z
    .locals 1

    .line 42
    iget-boolean v0, p0, Lcom/squareup/librarylist/LibraryListResults;->isLibraryEmpty:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LibraryListResults(currentState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListResults;->currentState:Lcom/squareup/librarylist/LibraryListState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cursor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListResults;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", placeholders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListResults;->placeholders:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isLibraryEmpty="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/librarylist/LibraryListResults;->isLibraryEmpty:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
