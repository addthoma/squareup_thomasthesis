.class public interface abstract Lcom/squareup/librarylist/LibraryListStateManager;
.super Ljava/lang/Object;
.source "CheckoutLibraryListStateManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0016\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00122\u0006\u0010\u0014\u001a\u00020\tH&J\u000e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012H&J\u0016\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00122\u0006\u0010\u0014\u001a\u00020\tH&J\u0008\u0010\u0017\u001a\u00020\tH&J\u000e\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0019H&J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH&J\u0018\u0010\u001f\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020\u00132\u0006\u0010!\u001a\u00020\"H&J\u0010\u0010#\u001a\u00020\u001c2\u0006\u0010$\u001a\u00020%H&R\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0005R\u0012\u0010\u0008\u001a\u00020\tX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\nR\u0018\u0010\u000b\u001a\u00020\u000cX\u00a6\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryListStateManager;",
        "",
        "currentCategoryId",
        "",
        "getCurrentCategoryId",
        "()Ljava/lang/String;",
        "currentCategoryName",
        "getCurrentCategoryName",
        "isTopLevel",
        "",
        "()Z",
        "libraryFilter",
        "Lcom/squareup/librarylist/LibraryListState$Filter;",
        "getLibraryFilter",
        "()Lcom/squareup/librarylist/LibraryListState$Filter;",
        "setLibraryFilter",
        "(Lcom/squareup/librarylist/LibraryListState$Filter;)V",
        "buildAllItemsPlaceholders",
        "",
        "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
        "shouldHideRewardsAndGiftCards",
        "buildCategoryPlaceholders",
        "buildGiftCardPlaceholder",
        "goBack",
        "holder",
        "Lrx/Observable;",
        "Lcom/squareup/librarylist/CheckoutLibraryListState;",
        "onCogsUpdate",
        "",
        "event",
        "Lcom/squareup/cogs/CatalogUpdateEvent;",
        "placeholderClicked",
        "placeholder",
        "flow",
        "Lflow/Flow;",
        "setModeToSingleCategory",
        "entry",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract buildAllItemsPlaceholders(Z)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation
.end method

.method public abstract buildCategoryPlaceholders()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation
.end method

.method public abstract buildGiftCardPlaceholder(Z)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCurrentCategoryId()Ljava/lang/String;
.end method

.method public abstract getCurrentCategoryName()Ljava/lang/String;
.end method

.method public abstract getLibraryFilter()Lcom/squareup/librarylist/LibraryListState$Filter;
.end method

.method public abstract goBack()Z
.end method

.method public abstract holder()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/librarylist/CheckoutLibraryListState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isTopLevel()Z
.end method

.method public abstract onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V
.end method

.method public abstract placeholderClicked(Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;Lflow/Flow;)V
.end method

.method public abstract setLibraryFilter(Lcom/squareup/librarylist/LibraryListState$Filter;)V
.end method

.method public abstract setModeToSingleCategory(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V
.end method
