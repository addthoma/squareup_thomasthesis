.class public final Lcom/squareup/librarylist/LibraryListState;
.super Ljava/lang/Object;
.source "LibraryListState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/librarylist/LibraryListState$Filter;,
        Lcom/squareup/librarylist/LibraryListState$SearchQuery;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0016\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0002%&BS\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u0007\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u0007\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u0007\u0012\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0005H\u00c6\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u000cH\u00c6\u0003JW\u0010\u001f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u00072\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u00072\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u00072\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000cH\u00c6\u0001J\u0013\u0010 \u001a\u00020\u000c2\u0008\u0010!\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\"\u001a\u00020#H\u00d6\u0001J\t\u0010$\u001a\u00020\u0007H\u00d6\u0001R\u0013\u0010\t\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0013\u0010\n\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000fR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u000fR\u0013\u0010\u0008\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u0015R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryListState;",
        "",
        "filter",
        "Lcom/squareup/librarylist/LibraryListState$Filter;",
        "searchQuery",
        "Lcom/squareup/librarylist/LibraryListState$SearchQuery;",
        "currentCategoryId",
        "",
        "currentCategoryName",
        "currentCategoryAbbreviation",
        "currentCategoryColor",
        "isTopLevel",
        "",
        "(Lcom/squareup/librarylist/LibraryListState$Filter;Lcom/squareup/librarylist/LibraryListState$SearchQuery;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V",
        "getCurrentCategoryAbbreviation",
        "()Ljava/lang/String;",
        "getCurrentCategoryColor",
        "getCurrentCategoryId",
        "getCurrentCategoryName",
        "getFilter",
        "()Lcom/squareup/librarylist/LibraryListState$Filter;",
        "()Z",
        "getSearchQuery",
        "()Lcom/squareup/librarylist/LibraryListState$SearchQuery;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "Filter",
        "SearchQuery",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentCategoryAbbreviation:Ljava/lang/String;

.field private final currentCategoryColor:Ljava/lang/String;

.field private final currentCategoryId:Ljava/lang/String;

.field private final currentCategoryName:Ljava/lang/String;

.field private final filter:Lcom/squareup/librarylist/LibraryListState$Filter;

.field private final isTopLevel:Z

.field private final searchQuery:Lcom/squareup/librarylist/LibraryListState$SearchQuery;


# direct methods
.method public constructor <init>()V
    .locals 10

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7f

    const/4 v9, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/squareup/librarylist/LibraryListState;-><init>(Lcom/squareup/librarylist/LibraryListState$Filter;Lcom/squareup/librarylist/LibraryListState$SearchQuery;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/librarylist/LibraryListState$Filter;Lcom/squareup/librarylist/LibraryListState$SearchQuery;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    const-string v0, "filter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchQuery"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/LibraryListState;->filter:Lcom/squareup/librarylist/LibraryListState$Filter;

    iput-object p2, p0, Lcom/squareup/librarylist/LibraryListState;->searchQuery:Lcom/squareup/librarylist/LibraryListState$SearchQuery;

    iput-object p3, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryId:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryName:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryAbbreviation:Ljava/lang/String;

    iput-object p6, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryColor:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/squareup/librarylist/LibraryListState;->isTopLevel:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/librarylist/LibraryListState$Filter;Lcom/squareup/librarylist/LibraryListState$SearchQuery;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    .line 11
    sget-object p1, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_CATEGORIES:Lcom/squareup/librarylist/LibraryListState$Filter;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    .line 12
    sget-object p2, Lcom/squareup/librarylist/LibraryListState$SearchQuery;->Companion:Lcom/squareup/librarylist/LibraryListState$SearchQuery$Companion;

    invoke-virtual {p2}, Lcom/squareup/librarylist/LibraryListState$SearchQuery$Companion;->getEMPTY()Lcom/squareup/librarylist/LibraryListState$SearchQuery;

    move-result-object p2

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    .line 13
    move-object p3, v0

    check-cast p3, Ljava/lang/String;

    :cond_2
    move-object v1, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    .line 14
    move-object p4, v0

    check-cast p4, Ljava/lang/String;

    :cond_3
    move-object v2, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    .line 15
    move-object p5, v0

    check-cast p5, Ljava/lang/String;

    :cond_4
    move-object v3, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    .line 16
    move-object p6, v0

    check-cast p6, Ljava/lang/String;

    :cond_5
    move-object v0, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    const/4 p7, 0x1

    const/4 v4, 0x1

    goto :goto_0

    :cond_6
    move v4, p7

    :goto_0
    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-object p5, v1

    move-object p6, v2

    move-object p7, v3

    move-object p8, v0

    move p9, v4

    .line 17
    invoke-direct/range {p2 .. p9}, Lcom/squareup/librarylist/LibraryListState;-><init>(Lcom/squareup/librarylist/LibraryListState$Filter;Lcom/squareup/librarylist/LibraryListState$SearchQuery;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/librarylist/LibraryListState;Lcom/squareup/librarylist/LibraryListState$Filter;Lcom/squareup/librarylist/LibraryListState$SearchQuery;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/librarylist/LibraryListState;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/librarylist/LibraryListState;->filter:Lcom/squareup/librarylist/LibraryListState$Filter;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/squareup/librarylist/LibraryListState;->searchQuery:Lcom/squareup/librarylist/LibraryListState$SearchQuery;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryId:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryName:Ljava/lang/String;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryAbbreviation:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryColor:Ljava/lang/String;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-boolean p7, p0, Lcom/squareup/librarylist/LibraryListState;->isTopLevel:Z

    :cond_6
    move v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    move p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/librarylist/LibraryListState;->copy(Lcom/squareup/librarylist/LibraryListState$Filter;Lcom/squareup/librarylist/LibraryListState$SearchQuery;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/squareup/librarylist/LibraryListState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/librarylist/LibraryListState$Filter;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->filter:Lcom/squareup/librarylist/LibraryListState$Filter;

    return-object v0
.end method

.method public final component2()Lcom/squareup/librarylist/LibraryListState$SearchQuery;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->searchQuery:Lcom/squareup/librarylist/LibraryListState$SearchQuery;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryId:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryName:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryAbbreviation:Ljava/lang/String;

    return-object v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryColor:Ljava/lang/String;

    return-object v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/librarylist/LibraryListState;->isTopLevel:Z

    return v0
.end method

.method public final copy(Lcom/squareup/librarylist/LibraryListState$Filter;Lcom/squareup/librarylist/LibraryListState$SearchQuery;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/squareup/librarylist/LibraryListState;
    .locals 9

    const-string v0, "filter"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchQuery"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/librarylist/LibraryListState;

    move-object v1, v0

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/squareup/librarylist/LibraryListState;-><init>(Lcom/squareup/librarylist/LibraryListState$Filter;Lcom/squareup/librarylist/LibraryListState$SearchQuery;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/librarylist/LibraryListState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/librarylist/LibraryListState;

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->filter:Lcom/squareup/librarylist/LibraryListState$Filter;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListState;->filter:Lcom/squareup/librarylist/LibraryListState$Filter;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->searchQuery:Lcom/squareup/librarylist/LibraryListState$SearchQuery;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListState;->searchQuery:Lcom/squareup/librarylist/LibraryListState$SearchQuery;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListState;->currentCategoryId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListState;->currentCategoryName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryAbbreviation:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListState;->currentCategoryAbbreviation:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryColor:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListState;->currentCategoryColor:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/librarylist/LibraryListState;->isTopLevel:Z

    iget-boolean p1, p1, Lcom/squareup/librarylist/LibraryListState;->isTopLevel:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCurrentCategoryAbbreviation()Ljava/lang/String;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryAbbreviation:Ljava/lang/String;

    return-object v0
.end method

.method public final getCurrentCategoryColor()Ljava/lang/String;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryColor:Ljava/lang/String;

    return-object v0
.end method

.method public final getCurrentCategoryId()Ljava/lang/String;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryId:Ljava/lang/String;

    return-object v0
.end method

.method public final getCurrentCategoryName()Ljava/lang/String;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryName:Ljava/lang/String;

    return-object v0
.end method

.method public final getFilter()Lcom/squareup/librarylist/LibraryListState$Filter;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->filter:Lcom/squareup/librarylist/LibraryListState$Filter;

    return-object v0
.end method

.method public final getSearchQuery()Lcom/squareup/librarylist/LibraryListState$SearchQuery;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->searchQuery:Lcom/squareup/librarylist/LibraryListState$SearchQuery;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListState;->filter:Lcom/squareup/librarylist/LibraryListState$Filter;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListState;->searchQuery:Lcom/squareup/librarylist/LibraryListState$SearchQuery;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryId:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryName:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryAbbreviation:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryColor:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/librarylist/LibraryListState;->isTopLevel:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public final isTopLevel()Z
    .locals 1

    .line 17
    iget-boolean v0, p0, Lcom/squareup/librarylist/LibraryListState;->isTopLevel:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LibraryListState(filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListState;->filter:Lcom/squareup/librarylist/LibraryListState$Filter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", searchQuery="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListState;->searchQuery:Lcom/squareup/librarylist/LibraryListState$SearchQuery;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", currentCategoryId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", currentCategoryName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", currentCategoryAbbreviation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryAbbreviation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", currentCategoryColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListState;->currentCategoryColor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", isTopLevel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/librarylist/LibraryListState;->isTopLevel:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
