.class final Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$1;
.super Ljava/lang/Object;
.source "LibraryListManager.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/librarylist/RealLibraryListManager;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "TT;TU;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000e\u0010\u0003\u001a\n \u0002*\u0004\u0018\u00010\u00040\u00042\u000e\u0010\u0005\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/librarylist/LibraryListState;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 0>",
        "Lcom/squareup/librarylist/RealLibraryListManager$Action$Back;",
        "state",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$1;

    invoke-direct {v0}, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$1;-><init>()V

    sput-object v0, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$1;->INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/librarylist/RealLibraryListManager$Action$Back;Lcom/squareup/librarylist/LibraryListState;)Lcom/squareup/librarylist/LibraryListState;
    .locals 0

    return-object p2
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/librarylist/RealLibraryListManager$Action$Back;

    check-cast p2, Lcom/squareup/librarylist/LibraryListState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$1;->call(Lcom/squareup/librarylist/RealLibraryListManager$Action$Back;Lcom/squareup/librarylist/LibraryListState;)Lcom/squareup/librarylist/LibraryListState;

    move-result-object p1

    return-object p1
.end method
