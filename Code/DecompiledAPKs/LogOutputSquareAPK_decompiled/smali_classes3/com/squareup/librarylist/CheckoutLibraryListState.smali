.class public final Lcom/squareup/librarylist/CheckoutLibraryListState;
.super Ljava/lang/Object;
.source "CheckoutLibraryListState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/librarylist/CheckoutLibraryListState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0012\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003JC\u0010\u0016\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u0005H\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00182\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u0005H\u00d6\u0001R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0013\u0010\u0008\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000bR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000bR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/librarylist/CheckoutLibraryListState;",
        "",
        "filter",
        "Lcom/squareup/librarylist/LibraryListState$Filter;",
        "currentCategoryId",
        "",
        "currentCategoryName",
        "currentCategoryAbbreviation",
        "currentCategoryColor",
        "(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
        "getCurrentCategoryAbbreviation",
        "()Ljava/lang/String;",
        "getCurrentCategoryColor",
        "getCurrentCategoryId",
        "getCurrentCategoryName",
        "getFilter",
        "()Lcom/squareup/librarylist/LibraryListState$Filter;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "Companion",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/librarylist/CheckoutLibraryListState$Companion;


# instance fields
.field private final currentCategoryAbbreviation:Ljava/lang/String;

.field private final currentCategoryColor:Ljava/lang/String;

.field private final currentCategoryId:Ljava/lang/String;

.field private final currentCategoryName:Ljava/lang/String;

.field private final filter:Lcom/squareup/librarylist/LibraryListState$Filter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/librarylist/CheckoutLibraryListState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/librarylist/CheckoutLibraryListState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/librarylist/CheckoutLibraryListState;->Companion:Lcom/squareup/librarylist/CheckoutLibraryListState$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "filter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->filter:Lcom/squareup/librarylist/LibraryListState$Filter;

    iput-object p2, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryName:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryAbbreviation:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryColor:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p7, p6, 0x2

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    .line 17
    move-object p2, v0

    check-cast p2, Ljava/lang/String;

    :cond_0
    move-object v3, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_1

    .line 18
    move-object p3, v0

    check-cast p3, Ljava/lang/String;

    :cond_1
    move-object v4, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_2

    .line 19
    move-object p4, v0

    check-cast p4, Ljava/lang/String;

    :cond_2
    move-object v5, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_3

    .line 20
    move-object p5, v0

    check-cast p5, Ljava/lang/String;

    :cond_3
    move-object v6, p5

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/librarylist/CheckoutLibraryListState;-><init>(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/librarylist/CheckoutLibraryListState;Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/librarylist/CheckoutLibraryListState;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->filter:Lcom/squareup/librarylist/LibraryListState$Filter;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryId:Ljava/lang/String;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryName:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryAbbreviation:Ljava/lang/String;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryColor:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/librarylist/CheckoutLibraryListState;->copy(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/librarylist/CheckoutLibraryListState;

    move-result-object p0

    return-object p0
.end method

.method public static final getDefaultHolder(Lcom/squareup/util/Device;)Lcom/squareup/librarylist/CheckoutLibraryListState;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/librarylist/CheckoutLibraryListState;->Companion:Lcom/squareup/librarylist/CheckoutLibraryListState$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/librarylist/CheckoutLibraryListState$Companion;->getDefaultHolder(Lcom/squareup/util/Device;)Lcom/squareup/librarylist/CheckoutLibraryListState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/librarylist/LibraryListState$Filter;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->filter:Lcom/squareup/librarylist/LibraryListState$Filter;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryId:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryName:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryAbbreviation:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryColor:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/librarylist/CheckoutLibraryListState;
    .locals 7

    const-string v0, "filter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/librarylist/CheckoutLibraryListState;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/librarylist/CheckoutLibraryListState;-><init>(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/librarylist/CheckoutLibraryListState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/librarylist/CheckoutLibraryListState;

    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->filter:Lcom/squareup/librarylist/LibraryListState$Filter;

    iget-object v1, p1, Lcom/squareup/librarylist/CheckoutLibraryListState;->filter:Lcom/squareup/librarylist/LibraryListState$Filter;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryAbbreviation:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryAbbreviation:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryColor:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryColor:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCurrentCategoryAbbreviation()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryAbbreviation:Ljava/lang/String;

    return-object v0
.end method

.method public final getCurrentCategoryColor()Ljava/lang/String;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryColor:Ljava/lang/String;

    return-object v0
.end method

.method public final getCurrentCategoryId()Ljava/lang/String;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryId:Ljava/lang/String;

    return-object v0
.end method

.method public final getCurrentCategoryName()Ljava/lang/String;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryName:Ljava/lang/String;

    return-object v0
.end method

.method public final getFilter()Lcom/squareup/librarylist/LibraryListState$Filter;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->filter:Lcom/squareup/librarylist/LibraryListState$Filter;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->filter:Lcom/squareup/librarylist/LibraryListState$Filter;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryId:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryName:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryAbbreviation:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryColor:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CheckoutLibraryListState(filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->filter:Lcom/squareup/librarylist/LibraryListState$Filter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", currentCategoryId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", currentCategoryName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", currentCategoryAbbreviation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryAbbreviation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", currentCategoryColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/CheckoutLibraryListState;->currentCategoryColor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
