.class public final Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;
.super Ljava/lang/Object;
.source "SimpleLibraryListModule_ProvideLibraryListManagerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/librarylist/RealLibraryListManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final configurationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleLibraryListConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryListSearcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListSearcher;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/librarylist/SimpleLibraryListModule;


# direct methods
.method public constructor <init>(Lcom/squareup/librarylist/SimpleLibraryListModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/SimpleLibraryListModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleLibraryListConfiguration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListSearcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;->module:Lcom/squareup/librarylist/SimpleLibraryListModule;

    .line 38
    iput-object p2, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;->analyticsProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;->deviceProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;->configurationProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;->libraryListSearcherProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;->mainSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/librarylist/SimpleLibraryListModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/SimpleLibraryListModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleLibraryListConfiguration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListSearcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)",
            "Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;-><init>(Lcom/squareup/librarylist/SimpleLibraryListModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static provideLibraryListManager(Lcom/squareup/librarylist/SimpleLibraryListModule;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/librarylist/SimpleLibraryListConfiguration;Lcom/squareup/librarylist/LibraryListSearcher;Lrx/Scheduler;)Lcom/squareup/librarylist/RealLibraryListManager;
    .locals 0

    .line 62
    invoke-virtual/range {p0 .. p5}, Lcom/squareup/librarylist/SimpleLibraryListModule;->provideLibraryListManager(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/librarylist/SimpleLibraryListConfiguration;Lcom/squareup/librarylist/LibraryListSearcher;Lrx/Scheduler;)Lcom/squareup/librarylist/RealLibraryListManager;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/librarylist/RealLibraryListManager;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/librarylist/RealLibraryListManager;
    .locals 6

    .line 47
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;->module:Lcom/squareup/librarylist/SimpleLibraryListModule;

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v2, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Device;

    iget-object v3, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;->configurationProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/librarylist/SimpleLibraryListConfiguration;

    iget-object v4, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;->libraryListSearcherProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/librarylist/LibraryListSearcher;

    iget-object v5, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lrx/Scheduler;

    invoke-static/range {v0 .. v5}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;->provideLibraryListManager(Lcom/squareup/librarylist/SimpleLibraryListModule;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/librarylist/SimpleLibraryListConfiguration;Lcom/squareup/librarylist/LibraryListSearcher;Lrx/Scheduler;)Lcom/squareup/librarylist/RealLibraryListManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListManagerFactory;->get()Lcom/squareup/librarylist/RealLibraryListManager;

    move-result-object v0

    return-object v0
.end method
