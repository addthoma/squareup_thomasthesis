.class public interface abstract Lcom/squareup/librarylist/LibraryListConfiguration;
.super Ljava/lang/Object;
.source "LibraryListConfiguration.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;,
        Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001:\u0002\u0012\u0013R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0005R\u0012\u0010\u0008\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\u0005R\u0018\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR\u0018\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u000e\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryListConfiguration;",
        "",
        "canAlwaysShowCategoryPlaceholders",
        "",
        "getCanAlwaysShowCategoryPlaceholders",
        "()Z",
        "canLongClickOnItem",
        "getCanLongClickOnItem",
        "canShowEditItemsButton",
        "getCanShowEditItemsButton",
        "topLevelItemSuggestions",
        "",
        "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
        "getTopLevelItemSuggestions",
        "()Ljava/util/List;",
        "topLevelPlaceholders",
        "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
        "getTopLevelPlaceholders",
        "ItemSuggestion",
        "Placeholder",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getCanAlwaysShowCategoryPlaceholders()Z
.end method

.method public abstract getCanLongClickOnItem()Z
.end method

.method public abstract getCanShowEditItemsButton()Z
.end method

.method public abstract getTopLevelItemSuggestions()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTopLevelPlaceholders()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation
.end method
