.class public Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;
.super Lcom/squareup/experiments/ExperimentProfile;
.source "ShowMultipleRewardsCopyExperiment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;
    }
.end annotation


# static fields
.field public static final CONTROL:Lcom/squareup/server/ExperimentsResponse$Bucket;

.field public static final DEFAULT_CONFIGURATION:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 16
    new-instance v0, Lcom/squareup/server/ExperimentsResponse$Bucket;

    const/16 v1, 0x32

    const/16 v2, 0x940

    const/16 v3, 0x1737

    const-string v4, "control"

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/squareup/server/ExperimentsResponse$Bucket;-><init>(IILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;->CONTROL:Lcom/squareup/server/ExperimentsResponse$Bucket;

    .line 17
    new-instance v0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    invoke-direct {v0}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;-><init>()V

    sget-object v3, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;->CONTROL:Lcom/squareup/server/ExperimentsResponse$Bucket;

    .line 18
    invoke-virtual {v0, v3}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->addBucket(Lcom/squareup/server/ExperimentsResponse$Bucket;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    new-instance v3, Lcom/squareup/server/ExperimentsResponse$Bucket;

    const/16 v4, 0x1738

    const-string v5, "show"

    invoke-direct {v3, v2, v4, v5, v1}, Lcom/squareup/server/ExperimentsResponse$Bucket;-><init>(IILjava/lang/String;I)V

    .line 19
    invoke-virtual {v0, v3}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->addBucket(Lcom/squareup/server/ExperimentsResponse$Bucket;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    const-string v1, "Show the user the multiple rewards type copy"

    .line 20
    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setDescription(Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    .line 21
    invoke-virtual {v0, v2}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setId(I)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    const-string v1, "show_multiple_rewards_copy"

    .line 22
    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setName(Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    const-string v1, "NOT_RUNNING"

    .line 23
    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setStatus(Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    const-wide/16 v2, 0x0

    invoke-direct {v1, v2, v3}, Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;-><init>(J)V

    .line 24
    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setUpdatedAt(Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    const-string v1, "1"

    .line 25
    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setVersion(Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->build()Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    move-result-object v0

    sput-object v0, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;->DEFAULT_CONFIGURATION:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    return-void
.end method

.method public constructor <init>(Ldagger/Lazy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/experiments/ExperimentStorage;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    sget-object v0, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;->CONTROL:Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;

    invoke-virtual {v0}, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;->DEFAULT_CONFIGURATION:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/experiments/ExperimentProfile;-><init>(Ldagger/Lazy;Ljava/lang/String;Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;)V

    return-void
.end method

.method static synthetic lambda$showMultipleRewardsCopyBehavior$0(Lcom/squareup/server/ExperimentsResponse$Bucket;)Ljava/lang/Boolean;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 41
    iget-object p0, p0, Lcom/squareup/server/ExperimentsResponse$Bucket;->name:Ljava/lang/String;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;->valueOf(Ljava/lang/String;)Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment$Behavior;->shouldShow()Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public showMultipleRewardsCopyBehavior()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 41
    invoke-virtual {p0}, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;->bucket()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/experiments/-$$Lambda$ShowMultipleRewardsCopyExperiment$uWLuvTvm10AL7zhVr0FR2lWgDoY;->INSTANCE:Lcom/squareup/experiments/-$$Lambda$ShowMultipleRewardsCopyExperiment$uWLuvTvm10AL7zhVr0FR2lWgDoY;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
