.class public Lcom/squareup/notifications/NoNotificationsModule;
.super Ljava/lang/Object;
.source "NoNotificationsModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideAutoCaptureNotifier()Lcom/squareup/notifications/AutoCaptureNotifier;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/notifications/AutoCaptureNotifier$NoAutoCaptureNotifier;

    invoke-direct {v0}, Lcom/squareup/notifications/AutoCaptureNotifier$NoAutoCaptureNotifier;-><init>()V

    return-object v0
.end method

.method static provideNoAutoVoidNotifier()Lcom/squareup/notifications/AutoVoidNotifier;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 10
    new-instance v0, Lcom/squareup/notifications/AutoVoidNotifier$NoAutoVoidNotifier;

    invoke-direct {v0}, Lcom/squareup/notifications/AutoVoidNotifier$NoAutoVoidNotifier;-><init>()V

    return-object v0
.end method

.method static provideNoPendingPaymentsNotifier()Lcom/squareup/notifications/PendingPaymentNotifier;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 14
    new-instance v0, Lcom/squareup/notifications/PendingPaymentNotifier$NoPendingPaymentNotifier;

    invoke-direct {v0}, Lcom/squareup/notifications/PendingPaymentNotifier$NoPendingPaymentNotifier;-><init>()V

    return-object v0
.end method

.method static provideNoStoredPaymentNotifier()Lcom/squareup/notifications/StoredPaymentNotifier;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 18
    new-instance v0, Lcom/squareup/notifications/StoredPaymentNotifier$NoStoredPaymentNotifier;

    invoke-direct {v0}, Lcom/squareup/notifications/StoredPaymentNotifier$NoStoredPaymentNotifier;-><init>()V

    return-object v0
.end method

.method static providePaymentIncompleteNotifier()Lcom/squareup/notifications/PaymentIncompleteNotifier;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 22
    new-instance v0, Lcom/squareup/notifications/PaymentIncompleteNotifier$NoPaymentIncompleteNotifier;

    invoke-direct {v0}, Lcom/squareup/notifications/PaymentIncompleteNotifier$NoPaymentIncompleteNotifier;-><init>()V

    return-object v0
.end method
