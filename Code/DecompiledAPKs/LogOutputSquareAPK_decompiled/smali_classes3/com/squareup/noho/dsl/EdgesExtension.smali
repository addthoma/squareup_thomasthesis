.class public final Lcom/squareup/noho/dsl/EdgesExtension;
.super Ljava/lang/Object;
.source "RecyclerEdges.kt"

# interfaces
.implements Lcom/squareup/cycler/Extension;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/dsl/EdgesExtension$Provider;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/cycler/Extension<",
        "TI;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecyclerEdges.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecyclerEdges.kt\ncom/squareup/noho/dsl/EdgesExtension\n+ 2 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,186:1\n37#2,6:187\n*E\n*S KotlinDebug\n*F\n+ 1 RecyclerEdges.kt\ncom/squareup/noho/dsl/EdgesExtension\n*L\n140#1,6:187\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003:\u0001\u0011B\u0015\u0008\u0001\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0005\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\r\u001a\u00020\u000e2\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0010H\u0016R \u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0008X\u0096.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/noho/dsl/EdgesExtension;",
        "I",
        "",
        "Lcom/squareup/cycler/Extension;",
        "spec",
        "Lcom/squareup/noho/dsl/EdgesExtensionSpec;",
        "(Lcom/squareup/noho/dsl/EdgesExtensionSpec;)V",
        "data",
        "Lcom/squareup/cycler/RecyclerData;",
        "getData",
        "()Lcom/squareup/cycler/RecyclerData;",
        "setData",
        "(Lcom/squareup/cycler/RecyclerData;)V",
        "attach",
        "",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Provider",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public data:Lcom/squareup/cycler/RecyclerData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/RecyclerData<",
            "TI;>;"
        }
    .end annotation
.end field

.field private final spec:Lcom/squareup/noho/dsl/EdgesExtensionSpec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/noho/dsl/EdgesExtensionSpec<",
            "TI;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/noho/dsl/EdgesExtensionSpec;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/dsl/EdgesExtensionSpec<",
            "TI;>;)V"
        }
    .end annotation

    const-string v0, "spec"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/noho/dsl/EdgesExtension;->spec:Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    return-void
.end method

.method public static final synthetic access$getSpec$p(Lcom/squareup/noho/dsl/EdgesExtension;)Lcom/squareup/noho/dsl/EdgesExtensionSpec;
    .locals 0

    .line 135
    iget-object p0, p0, Lcom/squareup/noho/dsl/EdgesExtension;->spec:Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    return-object p0
.end method


# virtual methods
.method public attach(Lcom/squareup/cycler/Recycler;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Recycler<",
            "TI;>;)V"
        }
    .end annotation

    const-string v0, "recycler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "recycler.view.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    sget-object v1, Lcom/squareup/noho/R$styleable;->NohoRecyclerEdges:[I

    const-string v2, "R.styleable.NohoRecyclerEdges"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    iget-object v2, p0, Lcom/squareup/noho/dsl/EdgesExtension;->spec:Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-virtual {v2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->getDefStyleAttr()I

    move-result v2

    .line 144
    iget-object v3, p0, Lcom/squareup/noho/dsl/EdgesExtension;->spec:Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-virtual {v3}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->getDefStyleRes()I

    move-result v3

    const/4 v4, 0x0

    .line 187
    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    :try_start_0
    const-string v1, "a"

    .line 189
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    new-instance v1, Landroid/graphics/Rect;

    .line 146
    sget v2, Lcom/squareup/noho/R$styleable;->NohoRecyclerEdges_android_paddingLeft:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 147
    sget v4, Lcom/squareup/noho/R$styleable;->NohoRecyclerEdges_android_paddingTop:I

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 148
    sget v5, Lcom/squareup/noho/R$styleable;->NohoRecyclerEdges_android_paddingRight:I

    invoke-virtual {v0, v5, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    .line 149
    sget v6, Lcom/squareup/noho/R$styleable;->NohoRecyclerEdges_android_paddingBottom:I

    invoke-virtual {v0, v6, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 145
    invoke-direct {v1, v2, v4, v5, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 150
    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v2

    invoke-virtual {v2}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/squareup/noho/R$color;->noho_divider_hairline:I

    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 152
    new-instance v3, Lcom/squareup/noho/NohoEdgeDecoration;

    .line 153
    sget v4, Lcom/squareup/noho/R$styleable;->NohoRecyclerEdges_sqEdgeWidth:I

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 154
    sget v5, Lcom/squareup/noho/R$styleable;->NohoRecyclerEdges_sqEdgeColor:I

    invoke-virtual {v0, v5, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 155
    new-instance v5, Lcom/squareup/noho/dsl/EdgesExtension$Provider;

    invoke-static {p0}, Lcom/squareup/noho/dsl/EdgesExtension;->access$getSpec$p(Lcom/squareup/noho/dsl/EdgesExtension;)Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    move-result-object v6

    invoke-virtual {v6}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->getDefault()I

    move-result v6

    invoke-direct {v5, p0, v6, v1}, Lcom/squareup/noho/dsl/EdgesExtension$Provider;-><init>(Lcom/squareup/noho/dsl/EdgesExtension;ILandroid/graphics/Rect;)V

    check-cast v5, Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;

    .line 152
    invoke-direct {v3, v4, v2, v5}, Lcom/squareup/noho/NohoEdgeDecoration;-><init>(IILcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 158
    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    check-cast v3, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {p1, v3}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    return-void

    :catchall_0
    move-exception p1

    .line 191
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method

.method public getData()Lcom/squareup/cycler/RecyclerData;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/RecyclerData<",
            "TI;>;"
        }
    .end annotation

    .line 161
    iget-object v0, p0, Lcom/squareup/noho/dsl/EdgesExtension;->data:Lcom/squareup/cycler/RecyclerData;

    if-nez v0, :cond_0

    const-string v1, "data"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public setData(Lcom/squareup/cycler/RecyclerData;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/RecyclerData<",
            "TI;>;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    iput-object p1, p0, Lcom/squareup/noho/dsl/EdgesExtension;->data:Lcom/squareup/cycler/RecyclerData;

    return-void
.end method
