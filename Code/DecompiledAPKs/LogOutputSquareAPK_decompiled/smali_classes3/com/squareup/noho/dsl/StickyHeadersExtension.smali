.class public final Lcom/squareup/noho/dsl/StickyHeadersExtension;
.super Ljava/lang/Object;
.source "StickyHeaders.kt"

# interfaces
.implements Lcom/squareup/cycler/Extension;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        "H:",
        "Ljava/lang/Object;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/cycler/Extension<",
        "TI;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStickyHeaders.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StickyHeaders.kt\ncom/squareup/noho/dsl/StickyHeadersExtension\n+ 2 Delegates.kt\nkotlin/properties/Delegates\n*L\n1#1,193:1\n33#2,3:194\n*E\n*S KotlinDebug\n*F\n+ 1 StickyHeaders.kt\ncom/squareup/noho/dsl/StickyHeadersExtension\n*L\n128#1,3:194\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u0002*\u0008\u0008\u0002\u0010\u0004*\u00020\u00052\u0008\u0012\u0004\u0012\u0002H\u00010\u0006B\u001f\u0012\u0018\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0016\u0010\u0019\u001a\u00020\u001a2\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u001cH\u0016R \u0010\n\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R#\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR7\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000f2\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000f8V@VX\u0096\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0015\u0010\u0016\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/noho/dsl/StickyHeadersExtension;",
        "I",
        "",
        "H",
        "V",
        "Landroid/view/View;",
        "Lcom/squareup/cycler/Extension;",
        "config",
        "Lcom/squareup/noho/dsl/StickyHeadersSpec;",
        "(Lcom/squareup/noho/dsl/StickyHeadersSpec;)V",
        "adapter",
        "Lcom/squareup/noho/dsl/StickyHeaderAdapter;",
        "getConfig",
        "()Lcom/squareup/noho/dsl/StickyHeadersSpec;",
        "<set-?>",
        "Lcom/squareup/cycler/RecyclerData;",
        "data",
        "getData",
        "()Lcom/squareup/cycler/RecyclerData;",
        "setData",
        "(Lcom/squareup/cycler/RecyclerData;)V",
        "data$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "decoration",
        "Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;",
        "attach",
        "",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final adapter:Lcom/squareup/noho/dsl/StickyHeaderAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/noho/dsl/StickyHeaderAdapter<",
            "TI;TH;TV;>;"
        }
    .end annotation
.end field

.field private final config:Lcom/squareup/noho/dsl/StickyHeadersSpec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/noho/dsl/StickyHeadersSpec<",
            "TI;TH;TV;>;"
        }
    .end annotation
.end field

.field private final data$delegate:Lkotlin/properties/ReadWriteProperty;

.field private final decoration:Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    const-class v2, Lcom/squareup/noho/dsl/StickyHeadersExtension;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "data"

    const-string v4, "getData()Lcom/squareup/cycler/RecyclerData;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/noho/dsl/StickyHeadersExtension;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/noho/dsl/StickyHeadersSpec;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/dsl/StickyHeadersSpec<",
            "TI;TH;TV;>;)V"
        }
    .end annotation

    const-string v0, "config"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/noho/dsl/StickyHeadersExtension;->config:Lcom/squareup/noho/dsl/StickyHeadersSpec;

    .line 121
    new-instance p1, Lcom/squareup/noho/dsl/StickyHeaderAdapter;

    iget-object v0, p0, Lcom/squareup/noho/dsl/StickyHeadersExtension;->config:Lcom/squareup/noho/dsl/StickyHeadersSpec;

    invoke-direct {p1, v0}, Lcom/squareup/noho/dsl/StickyHeaderAdapter;-><init>(Lcom/squareup/noho/dsl/StickyHeadersSpec;)V

    iput-object p1, p0, Lcom/squareup/noho/dsl/StickyHeadersExtension;->adapter:Lcom/squareup/noho/dsl/StickyHeaderAdapter;

    .line 122
    new-instance p1, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;

    iget-object v0, p0, Lcom/squareup/noho/dsl/StickyHeadersExtension;->adapter:Lcom/squareup/noho/dsl/StickyHeaderAdapter;

    check-cast v0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;

    invoke-direct {p1, v0}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;-><init>(Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;)V

    iput-object p1, p0, Lcom/squareup/noho/dsl/StickyHeadersExtension;->decoration:Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;

    .line 128
    sget-object p1, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    sget-object p1, Lcom/squareup/cycler/RecyclerData;->Companion:Lcom/squareup/cycler/RecyclerData$Companion;

    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData$Companion;->empty()Lcom/squareup/cycler/RecyclerData;

    move-result-object p1

    .line 194
    new-instance v0, Lcom/squareup/noho/dsl/StickyHeadersExtension$$special$$inlined$observable$1;

    invoke-direct {v0, p1, p1, p0}, Lcom/squareup/noho/dsl/StickyHeadersExtension$$special$$inlined$observable$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/noho/dsl/StickyHeadersExtension;)V

    check-cast v0, Lkotlin/properties/ReadWriteProperty;

    .line 196
    iput-object v0, p0, Lcom/squareup/noho/dsl/StickyHeadersExtension;->data$delegate:Lkotlin/properties/ReadWriteProperty;

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/squareup/noho/dsl/StickyHeadersExtension;)Lcom/squareup/noho/dsl/StickyHeaderAdapter;
    .locals 0

    .line 118
    iget-object p0, p0, Lcom/squareup/noho/dsl/StickyHeadersExtension;->adapter:Lcom/squareup/noho/dsl/StickyHeaderAdapter;

    return-object p0
.end method

.method public static final synthetic access$getDecoration$p(Lcom/squareup/noho/dsl/StickyHeadersExtension;)Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;
    .locals 0

    .line 118
    iget-object p0, p0, Lcom/squareup/noho/dsl/StickyHeadersExtension;->decoration:Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;

    return-object p0
.end method


# virtual methods
.method public attach(Lcom/squareup/cycler/Recycler;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Recycler<",
            "TI;>;)V"
        }
    .end annotation

    const-string v0, "recycler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/noho/dsl/StickyHeadersExtension;->decoration:Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    return-void
.end method

.method public final getConfig()Lcom/squareup/noho/dsl/StickyHeadersSpec;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/noho/dsl/StickyHeadersSpec<",
            "TI;TH;TV;>;"
        }
    .end annotation

    .line 119
    iget-object v0, p0, Lcom/squareup/noho/dsl/StickyHeadersExtension;->config:Lcom/squareup/noho/dsl/StickyHeadersSpec;

    return-object v0
.end method

.method public getData()Lcom/squareup/cycler/RecyclerData;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/RecyclerData<",
            "TI;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/noho/dsl/StickyHeadersExtension;->data$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/dsl/StickyHeadersExtension;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cycler/RecyclerData;

    return-object v0
.end method

.method public setData(Lcom/squareup/cycler/RecyclerData;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/RecyclerData<",
            "TI;>;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/noho/dsl/StickyHeadersExtension;->data$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/dsl/StickyHeadersExtension;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method
