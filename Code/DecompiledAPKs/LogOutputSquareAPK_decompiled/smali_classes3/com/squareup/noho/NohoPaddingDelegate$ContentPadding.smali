.class public final enum Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;
.super Ljava/lang/Enum;
.source "NohoPaddingDelegate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoPaddingDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContentPadding"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

.field public static final enum CARD:Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

.field public static final enum DETAIL:Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

.field public static final enum MASTER:Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

.field public static final enum SHEET:Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

.field public static final enum SHEET_PAYMENT_FLOW:Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;


# instance fields
.field private final alertPaddingClass:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

.field private final defaultPaddingClass:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 149
    new-instance v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->MASTER:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    const/4 v2, 0x0

    const-string v3, "MASTER"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;-><init>(Ljava/lang/String;ILcom/squareup/noho/NohoPaddingDelegate$PaddingClass;)V

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->MASTER:Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    .line 150
    new-instance v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->DETAIL:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    const/4 v3, 0x1

    const-string v4, "DETAIL"

    invoke-direct {v0, v4, v3, v1}, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;-><init>(Ljava/lang/String;ILcom/squareup/noho/NohoPaddingDelegate$PaddingClass;)V

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->DETAIL:Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    .line 151
    new-instance v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->CARD:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    sget-object v4, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->CARD_ALERT:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    const/4 v5, 0x2

    const-string v6, "CARD"

    invoke-direct {v0, v6, v5, v1, v4}, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;-><init>(Ljava/lang/String;ILcom/squareup/noho/NohoPaddingDelegate$PaddingClass;Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;)V

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->CARD:Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    .line 152
    new-instance v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->SHEET:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    sget-object v4, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->SHEET_ALERT:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    const/4 v6, 0x3

    const-string v7, "SHEET"

    invoke-direct {v0, v7, v6, v1, v4}, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;-><init>(Ljava/lang/String;ILcom/squareup/noho/NohoPaddingDelegate$PaddingClass;Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;)V

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->SHEET:Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    .line 153
    new-instance v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->SHEET_PAYMENT_FLOW:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    sget-object v4, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->SHEET_PAYMENT_FLOW_ALERT:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    const/4 v7, 0x4

    const-string v8, "SHEET_PAYMENT_FLOW"

    invoke-direct {v0, v8, v7, v1, v4}, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;-><init>(Ljava/lang/String;ILcom/squareup/noho/NohoPaddingDelegate$PaddingClass;Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;)V

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->SHEET_PAYMENT_FLOW:Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    .line 148
    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->MASTER:Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->DETAIL:Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->CARD:Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->SHEET:Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->SHEET_PAYMENT_FLOW:Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->$VALUES:[Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/noho/NohoPaddingDelegate$PaddingClass;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 162
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;-><init>(Ljava/lang/String;ILcom/squareup/noho/NohoPaddingDelegate$PaddingClass;Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/noho/NohoPaddingDelegate$PaddingClass;Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;",
            "Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;",
            ")V"
        }
    .end annotation

    .line 165
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 166
    iput-object p3, p0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->defaultPaddingClass:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    .line 167
    iput-object p4, p0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->alertPaddingClass:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;
    .locals 1

    .line 148
    const-class v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    return-object p0
.end method

.method public static values()[Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;
    .locals 1

    .line 148
    sget-object v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->$VALUES:[Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    invoke-virtual {v0}, [Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    return-object v0
.end method


# virtual methods
.method getPaddingClass(Z)Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;
    .locals 0

    if-eqz p1, :cond_0

    .line 171
    iget-object p1, p0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->alertPaddingClass:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->defaultPaddingClass:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    :goto_0
    return-object p1
.end method

.method supportsAlert()Z
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;->alertPaddingClass:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
