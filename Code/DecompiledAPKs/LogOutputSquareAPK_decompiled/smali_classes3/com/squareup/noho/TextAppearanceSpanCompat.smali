.class public final Lcom/squareup/noho/TextAppearanceSpanCompat;
.super Landroid/text/style/TextAppearanceSpan;
.source "TextAppearanceSpanCompat.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTextAppearanceSpanCompat.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TextAppearanceSpanCompat.kt\ncom/squareup/noho/TextAppearanceSpanCompat\n*L\n1#1,56:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u0013H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0016\u0010\r\u001a\u0004\u0018\u00010\u00078BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/noho/TextAppearanceSpanCompat;",
        "Landroid/text/style/TextAppearanceSpan;",
        "context",
        "Landroid/content/Context;",
        "appearanceId",
        "",
        "colorList",
        "Landroid/content/res/ColorStateList;",
        "(Landroid/content/Context;ILandroid/content/res/ColorStateList;)V",
        "appearance",
        "Lcom/squareup/textappearance/SquareTextAppearance;",
        "getContext",
        "()Landroid/content/Context;",
        "effectiveColorList",
        "getEffectiveColorList",
        "()Landroid/content/res/ColorStateList;",
        "updateDrawState",
        "",
        "ds",
        "Landroid/text/TextPaint;",
        "updateMeasureState",
        "paint",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final appearance:Lcom/squareup/textappearance/SquareTextAppearance;

.field private final colorList:Landroid/content/res/ColorStateList;

.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/content/res/ColorStateList;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object p1, p0, Lcom/squareup/noho/TextAppearanceSpanCompat;->context:Landroid/content/Context;

    iput-object p3, p0, Lcom/squareup/noho/TextAppearanceSpanCompat;->colorList:Landroid/content/res/ColorStateList;

    .line 31
    sget-object p1, Lcom/squareup/textappearance/SquareTextAppearance;->Companion:Lcom/squareup/textappearance/SquareTextAppearance$Companion;

    iget-object p3, p0, Lcom/squareup/noho/TextAppearanceSpanCompat;->context:Landroid/content/Context;

    invoke-virtual {p1, p3, p2}, Lcom/squareup/textappearance/SquareTextAppearance$Companion;->loadFromStyle(Landroid/content/Context;I)Lcom/squareup/textappearance/SquareTextAppearance;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/noho/TextAppearanceSpanCompat;->appearance:Lcom/squareup/textappearance/SquareTextAppearance;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;ILandroid/content/res/ColorStateList;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 28
    check-cast p3, Landroid/content/res/ColorStateList;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/TextAppearanceSpanCompat;-><init>(Landroid/content/Context;ILandroid/content/res/ColorStateList;)V

    return-void
.end method

.method private final getEffectiveColorList()Landroid/content/res/ColorStateList;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/noho/TextAppearanceSpanCompat;->colorList:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/noho/TextAppearanceSpanCompat;->appearance:Lcom/squareup/textappearance/SquareTextAppearance;

    invoke-virtual {v0}, Lcom/squareup/textappearance/SquareTextAppearance;->getTextColor()Landroid/content/res/ColorStateList;

    move-result-object v0

    :goto_0
    return-object v0
.end method


# virtual methods
.method public final getContext()Landroid/content/Context;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/noho/TextAppearanceSpanCompat;->context:Landroid/content/Context;

    return-object v0
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 3

    const-string v0, "ds"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-super {p0, p1}, Landroid/text/style/TextAppearanceSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 39
    invoke-direct {p0}, Lcom/squareup/noho/TextAppearanceSpanCompat;->getEffectiveColorList()Landroid/content/res/ColorStateList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 40
    iget-object v1, p1, Landroid/text/TextPaint;->drawableState:[I

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    :cond_0
    return-void
.end method

.method public updateMeasureState(Landroid/text/TextPaint;)V
    .locals 2

    const-string v0, "paint"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-super {p0, p1}, Landroid/text/style/TextAppearanceSpan;->updateMeasureState(Landroid/text/TextPaint;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/noho/TextAppearanceSpanCompat;->appearance:Lcom/squareup/textappearance/SquareTextAppearance;

    iget-object v1, p0, Lcom/squareup/noho/TextAppearanceSpanCompat;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/squareup/textappearance/SquareTextAppearance;->getTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 53
    check-cast p1, Landroid/graphics/Paint;

    invoke-static {p1}, Lcom/squareup/marketfont/MarketUtils;->configureOptimalPaintFlags(Landroid/graphics/Paint;)V

    return-void
.end method
