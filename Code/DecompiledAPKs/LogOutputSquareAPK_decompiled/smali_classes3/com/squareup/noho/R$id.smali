.class public final Lcom/squareup/noho/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final accessory:I = 0x7f0a0128

.field public static final actionIcon:I = 0x7f0a0132

.field public static final actionLink:I = 0x7f0a0133

.field public static final action_button:I = 0x7f0a013f

.field public static final action_icon:I = 0x7f0a0144

.field public static final alertTitle:I = 0x7f0a01ad

.field public static final ampm_picker:I = 0x7f0a01c1

.field public static final arrow:I = 0x7f0a01cf

.field public static final barrier:I = 0x7f0a021f

.field public static final body:I = 0x7f0a023a

.field public static final body2:I = 0x7f0a023b

.field public static final buttonContainer:I = 0x7f0a0275

.field public static final buttonPanel:I = 0x7f0a0276

.field public static final card:I = 0x7f0a02b1

.field public static final center:I = 0x7f0a02fa

.field public static final check:I = 0x7f0a030f

.field public static final collapse:I = 0x7f0a0361

.field public static final colon:I = 0x7f0a0365

.field public static final contentPanel:I = 0x7f0a03a4

.field public static final custom:I = 0x7f0a0524

.field public static final customPanel:I = 0x7f0a0525

.field public static final day_picker:I = 0x7f0a0558

.field public static final description:I = 0x7f0a0589

.field public static final destructive:I = 0x7f0a0591

.field public static final detail:I = 0x7f0a0592

.field public static final details:I = 0x7f0a05ac

.field public static final disclosure:I = 0x7f0a05c3

.field public static final display:I = 0x7f0a05d7

.field public static final display2:I = 0x7f0a05d8

.field public static final edit:I = 0x7f0a060c

.field public static final expand:I = 0x7f0a0731

.field public static final extras:I = 0x7f0a073e

.field public static final heading:I = 0x7f0a07cc

.field public static final heading2:I = 0x7f0a07cd

.field public static final hour_picker:I = 0x7f0a07e6

.field public static final icon:I = 0x7f0a0818

.field public static final label:I = 0x7f0a090d

.field public static final label2:I = 0x7f0a090e

.field public static final left:I = 0x7f0a0920

.field public static final link:I = 0x7f0a0941

.field public static final master:I = 0x7f0a09ad

.field public static final message:I = 0x7f0a09d3

.field public static final middle_guideline:I = 0x7f0a09dc

.field public static final minute_picker:I = 0x7f0a09de

.field public static final month_picker:I = 0x7f0a09f2

.field public static final noho_message_help:I = 0x7f0a0a3b

.field public static final noho_message_image:I = 0x7f0a0a3c

.field public static final noho_message_link_text:I = 0x7f0a0a3d

.field public static final noho_message_message:I = 0x7f0a0a3e

.field public static final noho_message_primary_button:I = 0x7f0a0a3f

.field public static final noho_message_secondary_button:I = 0x7f0a0a40

.field public static final noho_message_title:I = 0x7f0a0a41

.field public static final noho_title_value_row_title:I = 0x7f0a0a42

.field public static final noho_title_value_row_value:I = 0x7f0a0a43

.field public static final none:I = 0x7f0a0a45

.field public static final parentPanel:I = 0x7f0a0bae

.field public static final primary:I = 0x7f0a0c59

.field public static final radio:I = 0x7f0a0cd9

.field public static final right:I = 0x7f0a0d8d

.field public static final right_barrier:I = 0x7f0a0d8e

.field public static final scrollView:I = 0x7f0a0e12

.field public static final secondary:I = 0x7f0a0e2c

.field public static final selectable_color:I = 0x7f0a0e54

.field public static final selectable_label:I = 0x7f0a0e55

.field public static final selectable_value:I = 0x7f0a0e5a

.field public static final sheet:I = 0x7f0a0e78

.field public static final sheet_payment_flow:I = 0x7f0a0e79

.field public static final simpleIcon:I = 0x7f0a0e96

.field public static final spacer:I = 0x7f0a0eb8

.field public static final spinner_item_selected:I = 0x7f0a0ebe

.field public static final subValue:I = 0x7f0a0f43

.field public static final switch_type:I = 0x7f0a0f59

.field public static final tertiary:I = 0x7f0a0f8d

.field public static final textIcon:I = 0x7f0a0f97

.field public static final title:I = 0x7f0a103f

.field public static final titleDivider:I = 0x7f0a1040

.field public static final titleDividerTop:I = 0x7f0a1042

.field public static final title_template:I = 0x7f0a1046

.field public static final topPanel:I = 0x7f0a104f

.field public static final up_button:I = 0x7f0a10c0

.field public static final value:I = 0x7f0a10de

.field public static final valuesBarrier:I = 0x7f0a10e2

.field public static final year_picker:I = 0x7f0a1128


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 590
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
