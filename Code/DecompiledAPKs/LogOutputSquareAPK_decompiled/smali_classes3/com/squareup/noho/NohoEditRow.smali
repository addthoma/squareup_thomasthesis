.class public final Lcom/squareup/noho/NohoEditRow;
.super Landroidx/appcompat/widget/AppCompatEditText;
.source "NohoEditRow.kt"

# interfaces
.implements Lcom/squareup/text/HasSelectableText;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/NohoEditRow$DrawingInfo;,
        Lcom/squareup/noho/NohoEditRow$Plugin;,
        Lcom/squareup/noho/NohoEditRow$PluginSize;,
        Lcom/squareup/noho/NohoEditRow$PositionInList;,
        Lcom/squareup/noho/NohoEditRow$Side;,
        Lcom/squareup/noho/NohoEditRow$TouchHelper;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoEditRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoEditRow.kt\ncom/squareup/noho/NohoEditRow\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Delegates.kt\nkotlin/properties/Delegates\n+ 4 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,583:1\n194#1,4:586\n194#1,4:590\n194#1,4:594\n250#2,2:584\n1360#2:598\n1429#2,3:599\n1642#2,2:602\n1360#2:604\n1429#2,3:605\n310#2,7:608\n1642#2,2:615\n1651#2,3:617\n33#3,3:620\n37#4,6:623\n*E\n*S KotlinDebug\n*F\n+ 1 NohoEditRow.kt\ncom/squareup/noho/NohoEditRow\n*L\n153#1,4:586\n154#1,4:590\n155#1,4:594\n125#1,2:584\n239#1:598\n239#1,3:599\n240#1,2:602\n251#1:604\n251#1,3:605\n280#1,7:608\n333#1,2:615\n355#1,3:617\n51#1,3:620\n129#1,6:623\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a8\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\r\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0015\n\u0002\u0008\u0017\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002:\u0006xyz{|}B%\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u000e\u0010:\u001a\u00020;2\u0006\u0010<\u001a\u00020*J\u0010\u0010=\u001a\u00020;2\u0006\u0010>\u001a\u00020?H\u0016J\u0010\u0010@\u001a\u00020\u001b2\u0006\u0010A\u001a\u00020\u0008H\u0002J\u0010\u0010B\u001a\u00020\u001b2\u0006\u0010C\u001a\u00020DH\u0014J\u0010\u0010E\u001a\u00020;2\u0006\u0010F\u001a\u00020GH\u0002J\u000e\u0010H\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018H\u0002J\u0006\u0010I\u001a\u00020;J\u0006\u0010J\u001a\u00020;J\u0008\u0010K\u001a\u00020;H\u0002J\u0010\u0010L\u001a\u00020M2\u0006\u0010N\u001a\u00020\u0008H\u0014J\u0010\u0010O\u001a\u00020;2\u0006\u0010F\u001a\u00020GH\u0014J\"\u0010P\u001a\u00020;2\u0006\u0010Q\u001a\u00020\u001b2\u0006\u0010R\u001a\u00020\u00082\u0008\u0010S\u001a\u0004\u0018\u00010\u0016H\u0014J\u0018\u0010T\u001a\u00020;2\u0006\u0010U\u001a\u00020\u00082\u0006\u0010V\u001a\u00020\u0008H\u0014J\u0018\u0010W\u001a\u00020;2\u0006\u0010X\u001a\u00020\u00082\u0006\u0010Y\u001a\u00020\u0008H\u0014J(\u0010Z\u001a\u00020;2\u0006\u0010[\u001a\u00020\u00082\u0006\u0010\\\u001a\u00020\u00082\u0006\u0010]\u001a\u00020\u00082\u0006\u0010^\u001a\u00020\u0008H\u0014J\u0010\u0010_\u001a\u00020\u001b2\u0006\u0010C\u001a\u00020DH\u0016J\u0010\u0010`\u001a\u00020;2\u0006\u0010a\u001a\u00020\u001bH\u0016J\u001c\u0010<\u001a\u0004\u0018\u0001Hb\"\n\u0008\u0000\u0010b\u0018\u0001*\u00020*H\u0086\u0008\u00a2\u0006\u0002\u0010cJ%\u0010<\u001a\u0004\u0018\u0001Hb\"\u0008\u0008\u0000\u0010b*\u00020*2\u000c\u0010d\u001a\u0008\u0012\u0004\u0012\u0002Hb0e\u00a2\u0006\u0002\u0010fJ\u0010\u0010g\u001a\u00020\u00082\u0006\u0010C\u001a\u00020DH\u0002J\u0008\u0010h\u001a\u00020;H\u0016J\u000e\u0010i\u001a\u00020;2\u0006\u0010<\u001a\u00020*J\u0010\u0010j\u001a\u00020;2\u0006\u0010>\u001a\u00020?H\u0016J\u0010\u0010k\u001a\u00020;2\u0006\u0010l\u001a\u00020\u001bH\u0016J\u0008\u0010m\u001a\u00020;H\u0002J\u0014\u0010n\u001a\u00020\u001b*\u00020o2\u0006\u0010p\u001a\u00020qH\u0002J\u0014\u0010r\u001a\u00020\u001b*\u00020o2\u0006\u0010p\u001a\u00020qH\u0002J\u000c\u0010s\u001a\u00020;*\u00020oH\u0002J%\u0010t\u001a\u00020;*\u00020o2\u0008\u0008\u0001\u0010u\u001a\u00020\u00082\u000c\u0010v\u001a\u0008\u0012\u0004\u0012\u00020*0wH\u0082\u0008R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R+\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000c\u001a\u00020\r8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0013\u0010\u0014\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0019\u0018\u00010\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001a\u001a\u00020\u001b@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001c\u0010\u001d\"\u0004\u0008\u001e\u0010\u001fR$\u0010 \u001a\u00020\u001b2\u0006\u0010\u001a\u001a\u00020\u001b@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008 \u0010\u001d\"\u0004\u0008!\u0010\u001fR\u000e\u0010\"\u001a\u00020\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010#\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010%R\u0011\u0010&\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u0010%R\u0014\u0010(\u001a\u0008\u0012\u0004\u0012\u00020*0)X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010+\u001a\u00020\u000b2\u0006\u0010\u001a\u001a\u00020\u000b8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008,\u0010-\"\u0004\u0008.\u0010/R(\u00101\u001a\u0004\u0018\u0001002\u0008\u0010\u001a\u001a\u0004\u0018\u000100@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00082\u00103\"\u0004\u00084\u00105R\u000e\u00106\u001a\u00020\u001bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u00107\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u00108\u001a\u000609R\u00020\u0000X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006~"
    }
    d2 = {
        "Lcom/squareup/noho/NohoEditRow;",
        "Landroidx/appcompat/widget/AppCompatEditText;",
        "Lcom/squareup/text/HasSelectableText;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "_positionInList",
        "Lcom/squareup/noho/NohoEditRow$PositionInList;",
        "<set-?>",
        "Lcom/squareup/noho/NohoEditRow$Side;",
        "alignment",
        "getAlignment",
        "()Lcom/squareup/noho/NohoEditRow$Side;",
        "setAlignment",
        "(Lcom/squareup/noho/NohoEditRow$Side;)V",
        "alignment$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "currentRect",
        "Landroid/graphics/Rect;",
        "drawingInfos",
        "",
        "Lcom/squareup/noho/NohoEditRow$DrawingInfo;",
        "value",
        "",
        "isError",
        "()Z",
        "setError",
        "(Z)V",
        "isValidated",
        "setValidated",
        "lastTouchArea",
        "originalPaddingLeft",
        "getOriginalPaddingLeft",
        "()I",
        "originalPaddingRight",
        "getOriginalPaddingRight",
        "plugins",
        "",
        "Lcom/squareup/noho/NohoEditRow$Plugin;",
        "positionInList",
        "getPositionInList",
        "()Lcom/squareup/noho/NohoEditRow$PositionInList;",
        "setPositionInList",
        "(Lcom/squareup/noho/NohoEditRow$PositionInList;)V",
        "Lcom/squareup/text/ScrubbingTextWatcher;",
        "scrubber",
        "getScrubber",
        "()Lcom/squareup/text/ScrubbingTextWatcher;",
        "setScrubber",
        "(Lcom/squareup/text/ScrubbingTextWatcher;)V",
        "showKeyboardDelayed",
        "totalRect",
        "touchHelper",
        "Lcom/squareup/noho/NohoEditRow$TouchHelper;",
        "addPlugin",
        "",
        "plugin",
        "addSelectionWatcher",
        "watcher",
        "Lcom/squareup/text/SelectionWatcher;",
        "clickPlugin",
        "pluginId",
        "dispatchHoverEvent",
        "event",
        "Landroid/view/MotionEvent;",
        "drawPlugins",
        "canvas",
        "Landroid/graphics/Canvas;",
        "ensureDrawingInfo",
        "focusAndShowKeyboard",
        "invalidateDrawingInfo",
        "maybeShowKeyboard",
        "onCreateDrawableState",
        "",
        "extraSpace",
        "onDraw",
        "onFocusChanged",
        "focused",
        "direction",
        "previouslyFocusedRect",
        "onMeasure",
        "widthMeasureSpec",
        "heightMeasureSpec",
        "onSelectionChanged",
        "selectionStart",
        "selectionEnd",
        "onSizeChanged",
        "w",
        "h",
        "oldw",
        "oldh",
        "onTouchEvent",
        "onWindowFocusChanged",
        "hasWindowFocus",
        "T",
        "()Lcom/squareup/noho/NohoEditRow$Plugin;",
        "clazz",
        "Ljava/lang/Class;",
        "(Ljava/lang/Class;)Lcom/squareup/noho/NohoEditRow$Plugin;",
        "pluginAreaFromMotionEvent",
        "postRestartInput",
        "removePlugin",
        "removeSelectionWatcher",
        "setSingleLine",
        "singleLine",
        "updateGravity",
        "initFlexibleLabel",
        "Landroid/content/res/TypedArray;",
        "labelWeight",
        "",
        "initLabel",
        "initPlugins",
        "maybeAddPlugin",
        "styleableId",
        "pluginBlock",
        "Lkotlin/Function0;",
        "DrawingInfo",
        "Plugin",
        "PluginSize",
        "PositionInList",
        "Side",
        "TouchHelper",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private _positionInList:Lcom/squareup/noho/NohoEditRow$PositionInList;

.field private final alignment$delegate:Lkotlin/properties/ReadWriteProperty;

.field private final currentRect:Landroid/graphics/Rect;

.field private drawingInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/noho/NohoEditRow$DrawingInfo;",
            ">;"
        }
    .end annotation
.end field

.field private isError:Z

.field private isValidated:Z

.field private lastTouchArea:I

.field private final originalPaddingLeft:I

.field private final originalPaddingRight:I

.field private final plugins:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/noho/NohoEditRow$Plugin;",
            ">;"
        }
    .end annotation
.end field

.field private scrubber:Lcom/squareup/text/ScrubbingTextWatcher;

.field private showKeyboardDelayed:Z

.field private final totalRect:Landroid/graphics/Rect;

.field private final touchHelper:Lcom/squareup/noho/NohoEditRow$TouchHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    const-class v2, Lcom/squareup/noho/NohoEditRow;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "alignment"

    const-string v4, "getAlignment()Lcom/squareup/noho/NohoEditRow$Side;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/noho/NohoEditRow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoEditRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoEditRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/AppCompatEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    sget-object v0, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    sget-object v0, Lcom/squareup/noho/NohoEditRow$Side;->START:Lcom/squareup/noho/NohoEditRow$Side;

    .line 620
    new-instance v1, Lcom/squareup/noho/NohoEditRow$$special$$inlined$observable$1;

    invoke-direct {v1, v0, v0, p0}, Lcom/squareup/noho/NohoEditRow$$special$$inlined$observable$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/noho/NohoEditRow;)V

    check-cast v1, Lkotlin/properties/ReadWriteProperty;

    .line 622
    iput-object v1, p0, Lcom/squareup/noho/NohoEditRow;->alignment$delegate:Lkotlin/properties/ReadWriteProperty;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/noho/NohoEditRow;->plugins:Ljava/util/List;

    .line 105
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/squareup/noho/NohoEditRow;->totalRect:Landroid/graphics/Rect;

    .line 106
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/squareup/noho/NohoEditRow;->currentRect:Landroid/graphics/Rect;

    .line 107
    new-instance v0, Lcom/squareup/noho/NohoEditRow$TouchHelper;

    invoke-direct {v0, p0}, Lcom/squareup/noho/NohoEditRow$TouchHelper;-><init>(Lcom/squareup/noho/NohoEditRow;)V

    iput-object v0, p0, Lcom/squareup/noho/NohoEditRow;->touchHelper:Lcom/squareup/noho/NohoEditRow$TouchHelper;

    .line 109
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->getPaddingLeft()I

    move-result v0

    iput v0, p0, Lcom/squareup/noho/NohoEditRow;->originalPaddingLeft:I

    .line 110
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->getPaddingRight()I

    move-result v0

    iput v0, p0, Lcom/squareup/noho/NohoEditRow;->originalPaddingRight:I

    const/4 v0, -0x1

    .line 112
    iput v0, p0, Lcom/squareup/noho/NohoEditRow;->lastTouchArea:I

    .line 131
    sget-object v0, Lcom/squareup/noho/R$styleable;->NohoEditRow:[I

    const-string v1, "R.styleable.NohoEditRow"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    sget v1, Lcom/squareup/noho/R$style;->Widget_Noho_Edit:I

    .line 623
    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    const-string p2, "a"

    .line 625
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    sget p2, Lcom/squareup/noho/R$styleable;->NohoEditRow_sqPositionInList:I

    invoke-static {}, Lcom/squareup/noho/NohoEditRow$PositionInList;->values()[Lcom/squareup/noho/NohoEditRow$PositionInList;

    move-result-object p3

    check-cast p3, [Ljava/lang/Enum;

    sget-object v0, Lcom/squareup/noho/NohoEditRow$PositionInList;->MIDDLE:Lcom/squareup/noho/NohoEditRow$PositionInList;

    check-cast v0, Ljava/lang/Enum;

    invoke-static {p1, p2, p3, v0}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;I[Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p2

    if-nez p2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast p2, Lcom/squareup/noho/NohoEditRow$PositionInList;

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoEditRow;->setPositionInList(Lcom/squareup/noho/NohoEditRow$PositionInList;)V

    .line 138
    sget p2, Lcom/squareup/noho/R$styleable;->NohoEditRow_sqTextAlignment:I

    invoke-static {}, Lcom/squareup/noho/NohoEditRow$Side;->values()[Lcom/squareup/noho/NohoEditRow$Side;

    move-result-object p3

    check-cast p3, [Ljava/lang/Enum;

    sget-object v0, Lcom/squareup/noho/NohoEditRow$Side;->START:Lcom/squareup/noho/NohoEditRow$Side;

    check-cast v0, Ljava/lang/Enum;

    invoke-static {p1, p2, p3, v0}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;I[Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p2

    if-nez p2, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast p2, Lcom/squareup/noho/NohoEditRow$Side;

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoEditRow;->setAlignment(Lcom/squareup/noho/NohoEditRow$Side;)V

    .line 139
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEditRow;->access$initPlugins(Lcom/squareup/noho/NohoEditRow;Landroid/content/res/TypedArray;)V

    .line 140
    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 627
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 142
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->getPaint()Landroid/text/TextPaint;

    move-result-object p1

    check-cast p1, Landroid/graphics/Paint;

    invoke-static {p1}, Lcom/squareup/marketfont/MarketUtils;->configureOptimalPaintFlags(Landroid/graphics/Paint;)V

    .line 143
    move-object p1, p0

    check-cast p1, Landroid/view/View;

    iget-object p2, p0, Lcom/squareup/noho/NohoEditRow;->touchHelper:Lcom/squareup/noho/NohoEditRow$TouchHelper;

    check-cast p2, Landroidx/core/view/AccessibilityDelegateCompat;

    invoke-static {p1, p2}, Landroidx/core/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroidx/core/view/AccessibilityDelegateCompat;)V

    return-void

    :catchall_0
    move-exception p2

    .line 627
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 42
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 43
    sget p3, Lcom/squareup/noho/R$attr;->nohoEditStyle:I

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoEditRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$clickPlugin(Lcom/squareup/noho/NohoEditRow;I)Z
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoEditRow;->clickPlugin(I)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$ensureDrawingInfo(Lcom/squareup/noho/NohoEditRow;)Ljava/util/List;
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/squareup/noho/NohoEditRow;->ensureDrawingInfo()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getPlugins$p(Lcom/squareup/noho/NohoEditRow;)Ljava/util/List;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/noho/NohoEditRow;->plugins:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$initPlugins(Lcom/squareup/noho/NohoEditRow;Landroid/content/res/TypedArray;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoEditRow;->initPlugins(Landroid/content/res/TypedArray;)V

    return-void
.end method

.method public static final synthetic access$mergeDrawableStates$s-1884569510([I[I)[I
    .locals 0

    .line 40
    invoke-static {p0, p1}, Landroidx/appcompat/widget/AppCompatEditText;->mergeDrawableStates([I[I)[I

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$mergeDrawableStates$s2666181([I[I)[I
    .locals 0

    .line 40
    invoke-static {p0, p1}, Landroid/view/View;->mergeDrawableStates([I[I)[I

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateGravity(Lcom/squareup/noho/NohoEditRow;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/squareup/noho/NohoEditRow;->updateGravity()V

    return-void
.end method

.method private final clickPlugin(I)Z
    .locals 1

    .line 324
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow;->plugins:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-interface {p1}, Lcom/squareup/noho/NohoEditRow$Plugin;->onClick()Z

    move-result p1

    return p1
.end method

.method private final drawPlugins(Landroid/graphics/Canvas;)V
    .locals 6

    .line 351
    invoke-direct {p0}, Lcom/squareup/noho/NohoEditRow;->ensureDrawingInfo()Ljava/util/List;

    move-result-object v0

    .line 352
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 354
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->getScrollX()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->getScrollY()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 355
    iget-object v2, p0, Lcom/squareup/noho/NohoEditRow;->plugins:Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 618
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    add-int/lit8 v5, v3, 0x1

    if-gez v3, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v4, Lcom/squareup/noho/NohoEditRow$Plugin;

    .line 355
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/noho/NohoEditRow$DrawingInfo;

    invoke-interface {v4, p1, v3}, Lcom/squareup/noho/NohoEditRow$Plugin;->onDraw(Landroid/graphics/Canvas;Lcom/squareup/noho/NohoEditRow$DrawingInfo;)V

    move v3, v5

    goto :goto_0

    .line 356
    :cond_1
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    return-void
.end method

.method private final ensureDrawingInfo()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/noho/NohoEditRow$DrawingInfo;",
            ">;"
        }
    .end annotation

    .line 234
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow;->drawingInfos:Ljava/util/List;

    if-eqz v0, :cond_0

    return-object v0

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow;->totalRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->getHeight()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 238
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/squareup/noho/NohoEditRow;->totalRect:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 239
    iget-object v1, p0, Lcom/squareup/noho/NohoEditRow;->plugins:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 598
    new-instance v2, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v1, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 599
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 600
    check-cast v5, Lcom/squareup/noho/NohoEditRow$Plugin;

    .line 239
    iget-object v6, p0, Lcom/squareup/noho/NohoEditRow;->totalRect:Landroid/graphics/Rect;

    invoke-interface {v5, v6}, Lcom/squareup/noho/NohoEditRow$Plugin;->measure(Landroid/graphics/Rect;)Lcom/squareup/noho/NohoEditRow$PluginSize;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 601
    :cond_1
    check-cast v2, Ljava/util/List;

    .line 240
    check-cast v2, Ljava/lang/Iterable;

    .line 602
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v6, 0x2

    const/4 v7, 0x1

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/noho/NohoEditRow$PluginSize;

    .line 241
    invoke-virtual {v5}, Lcom/squareup/noho/NohoEditRow$PluginSize;->getSide()Lcom/squareup/noho/NohoEditRow$Side;

    move-result-object v8

    sget-object v9, Lcom/squareup/noho/NohoEditRow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v8}, Lcom/squareup/noho/NohoEditRow$Side;->ordinal()I

    move-result v8

    aget v8, v9, v8

    if-eq v8, v7, :cond_3

    if-eq v8, v6, :cond_2

    goto :goto_1

    .line 243
    :cond_2
    iget v6, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {v5}, Lcom/squareup/noho/NohoEditRow$PluginSize;->getMarginOnSide()I

    move-result v7

    invoke-virtual {v5}, Lcom/squareup/noho/NohoEditRow$PluginSize;->getWidth()I

    move-result v5

    add-int/2addr v7, v5

    sub-int/2addr v6, v7

    iput v6, v0, Landroid/graphics/Rect;->right:I

    goto :goto_1

    .line 242
    :cond_3
    iget v6, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {v5}, Lcom/squareup/noho/NohoEditRow$PluginSize;->getMarginOnSide()I

    move-result v7

    invoke-virtual {v5}, Lcom/squareup/noho/NohoEditRow$PluginSize;->getWidth()I

    move-result v5

    add-int/2addr v7, v5

    add-int/2addr v6, v7

    iput v6, v0, Landroid/graphics/Rect;->left:I

    goto :goto_1

    .line 246
    :cond_4
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v5, p0, Lcom/squareup/noho/NohoEditRow;->originalPaddingLeft:I

    add-int/2addr v1, v5

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 247
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v5, p0, Lcom/squareup/noho/NohoEditRow;->originalPaddingRight:I

    sub-int/2addr v1, v5

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 250
    iget-object v1, p0, Lcom/squareup/noho/NohoEditRow;->currentRect:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/squareup/noho/NohoEditRow;->totalRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 604
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 605
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 606
    check-cast v4, Lcom/squareup/noho/NohoEditRow$PluginSize;

    .line 252
    invoke-virtual {v4}, Lcom/squareup/noho/NohoEditRow$PluginSize;->getSide()Lcom/squareup/noho/NohoEditRow$Side;

    move-result-object v5

    sget-object v8, Lcom/squareup/noho/NohoEditRow$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v5}, Lcom/squareup/noho/NohoEditRow$Side;->ordinal()I

    move-result v5

    aget v5, v8, v5

    if-eq v5, v7, :cond_6

    if-ne v5, v6, :cond_5

    .line 261
    iget-object v5, p0, Lcom/squareup/noho/NohoEditRow;->currentRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    invoke-virtual {v4}, Lcom/squareup/noho/NohoEditRow$PluginSize;->getMarginOnSide()I

    move-result v8

    sub-int/2addr v5, v8

    .line 262
    invoke-virtual {v4}, Lcom/squareup/noho/NohoEditRow$PluginSize;->getWidth()I

    move-result v4

    sub-int v4, v5, v4

    .line 263
    new-instance v8, Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/squareup/noho/NohoEditRow;->totalRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v8, v4, v3, v5, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 264
    iget-object v5, p0, Lcom/squareup/noho/NohoEditRow;->currentRect:Landroid/graphics/Rect;

    iput v4, v5, Landroid/graphics/Rect;->right:I

    .line 265
    new-instance v4, Lcom/squareup/noho/NohoEditRow$DrawingInfo;

    iget-object v5, p0, Lcom/squareup/noho/NohoEditRow;->totalRect:Landroid/graphics/Rect;

    invoke-direct {v4, v5, v8, v0}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;-><init>(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    goto :goto_3

    :cond_5
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 254
    :cond_6
    iget-object v5, p0, Lcom/squareup/noho/NohoEditRow;->currentRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v4}, Lcom/squareup/noho/NohoEditRow$PluginSize;->getMarginOnSide()I

    move-result v8

    add-int/2addr v5, v8

    .line 255
    invoke-virtual {v4}, Lcom/squareup/noho/NohoEditRow$PluginSize;->getWidth()I

    move-result v4

    add-int/2addr v4, v5

    .line 256
    new-instance v8, Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/squareup/noho/NohoEditRow;->totalRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v8, v5, v3, v4, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 257
    iget-object v5, p0, Lcom/squareup/noho/NohoEditRow;->currentRect:Landroid/graphics/Rect;

    iput v4, v5, Landroid/graphics/Rect;->left:I

    .line 258
    new-instance v4, Lcom/squareup/noho/NohoEditRow$DrawingInfo;

    iget-object v5, p0, Lcom/squareup/noho/NohoEditRow;->totalRect:Landroid/graphics/Rect;

    invoke-direct {v4, v5, v8, v0}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;-><init>(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 267
    :goto_3
    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 607
    :cond_7
    check-cast v1, Ljava/util/List;

    .line 272
    iget v2, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->getPaddingTop()I

    move-result v3

    iget-object v4, p0, Lcom/squareup/noho/NohoEditRow;->totalRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v4, v0

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->getPaddingBottom()I

    move-result v0

    invoke-virtual {p0, v2, v3, v4, v0}, Lcom/squareup/noho/NohoEditRow;->setPadding(IIII)V

    .line 274
    iput-object v1, p0, Lcom/squareup/noho/NohoEditRow;->drawingInfos:Ljava/util/List;

    return-object v1
.end method

.method private final initFlexibleLabel(Landroid/content/res/TypedArray;F)Z
    .locals 3

    .line 162
    sget v0, Lcom/squareup/noho/R$styleable;->NohoEditRow_sqFlexibleLabel:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 165
    new-instance v0, Lcom/squareup/noho/FlexibleLabelPlugin;

    .line 166
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    sget v2, Lcom/squareup/noho/R$style;->TextAppearance_Widget_Noho_Edit_Label:I

    .line 165
    invoke-direct {v0, v1, p1, v2, p2}, Lcom/squareup/noho/FlexibleLabelPlugin;-><init>(Landroid/content/Context;Ljava/lang/String;IF)V

    check-cast v0, Lcom/squareup/noho/NohoEditRow$Plugin;

    .line 164
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    :cond_0
    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final initLabel(Landroid/content/res/TypedArray;F)Z
    .locals 9

    .line 176
    sget v0, Lcom/squareup/noho/R$styleable;->NohoEditRow_sqLabelText:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v8, 0x0

    if-eqz v0, :cond_1

    .line 178
    sget v1, Lcom/squareup/noho/R$styleable;->NohoEditRow_sqFillAvailableArea:I

    invoke-virtual {p1, v1, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p1

    const-string v1, "context"

    if-eqz p1, :cond_0

    .line 181
    new-instance p1, Lcom/squareup/noho/LabelPlugin;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v4, Lcom/squareup/noho/R$style;->TextAppearance_Widget_Noho_Edit_Label:I

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, p1

    move-object v3, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/noho/LabelPlugin;-><init>(Landroid/content/Context;Ljava/lang/String;IFILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    .line 183
    :cond_0
    new-instance p1, Lcom/squareup/noho/LabelPlugin;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v1, Lcom/squareup/noho/R$style;->TextAppearance_Widget_Noho_Edit_Label:I

    invoke-direct {p1, v2, v0, v1, p2}, Lcom/squareup/noho/LabelPlugin;-><init>(Landroid/content/Context;Ljava/lang/String;IF)V

    .line 180
    :goto_0
    check-cast p1, Lcom/squareup/noho/NohoEditRow$Plugin;

    .line 179
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    :cond_1
    if-eqz v0, :cond_2

    const/4 v8, 0x1

    :cond_2
    return v8
.end method

.method private final initPlugins(Landroid/content/res/TypedArray;)V
    .locals 10

    .line 148
    sget v0, Lcom/squareup/noho/R$styleable;->NohoEditRow_sqLabelLayoutWeight:I

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    .line 149
    invoke-direct {p0, p1, v0}, Lcom/squareup/noho/NohoEditRow;->initFlexibleLabel(Landroid/content/res/TypedArray;F)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 150
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/squareup/noho/NohoEditRow;->initLabel(Landroid/content/res/TypedArray;F)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 153
    :cond_1
    sget v0, Lcom/squareup/noho/R$styleable;->NohoEditRow_sqClearButton:I

    const/4 v1, 0x0

    .line 586
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    const-string v2, "context"

    if-eqz v0, :cond_2

    .line 153
    new-instance v0, Lcom/squareup/noho/ClearPlugin;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xe

    const/4 v9, 0x0

    move-object v3, v0

    invoke-direct/range {v3 .. v9}, Lcom/squareup/noho/ClearPlugin;-><init>(Landroid/content/Context;ILjava/util/Set;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 154
    :cond_2
    sget v0, Lcom/squareup/noho/R$styleable;->NohoEditRow_sqViewPasswordButton:I

    .line 590
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 154
    new-instance v0, Lcom/squareup/noho/ViewPasswordPlugin;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v3}, Lcom/squareup/noho/ViewPasswordPlugin;-><init>(Landroid/content/Context;)V

    check-cast v0, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 155
    :cond_3
    sget v0, Lcom/squareup/noho/R$styleable;->NohoEditRow_sqSearchIcon:I

    .line 594
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 155
    new-instance p1, Lcom/squareup/noho/SearchIconPlugin;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/squareup/noho/SearchIconPlugin;-><init>(Landroid/content/Context;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    :cond_4
    return-void
.end method

.method private final maybeAddPlugin(Landroid/content/res/TypedArray;ILkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/TypedArray;",
            "I",
            "Lkotlin/jvm/functions/Function0<",
            "+",
            "Lcom/squareup/noho/NohoEditRow$Plugin;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 194
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 195
    invoke-interface {p3}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    :cond_0
    return-void
.end method

.method private final maybeShowKeyboard()V
    .locals 2

    .line 419
    iget-boolean v0, p0, Lcom/squareup/noho/NohoEditRow;->showKeyboardDelayed:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 420
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 421
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/squareup/noho/NohoEditRow$maybeShowKeyboard$1;

    invoke-direct {v1, p0}, Lcom/squareup/noho/NohoEditRow$maybeShowKeyboard$1;-><init>(Lcom/squareup/noho/NohoEditRow;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    const/4 v0, 0x0

    .line 426
    iput-boolean v0, p0, Lcom/squareup/noho/NohoEditRow;->showKeyboardDelayed:Z

    :cond_1
    return-void
.end method

.method private final pluginAreaFromMotionEvent(Landroid/view/MotionEvent;)I
    .locals 5

    .line 279
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    float-to-int p1, p1

    .line 280
    invoke-direct {p0}, Lcom/squareup/noho/NohoEditRow;->ensureDrawingInfo()Ljava/util/List;

    move-result-object v0

    .line 609
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, -0x1

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 610
    check-cast v3, Lcom/squareup/noho/NohoEditRow$DrawingInfo;

    .line 280
    invoke-virtual {v3}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->getPluginRect()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3, p1, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    :goto_1
    if-ltz v2, :cond_2

    .line 281
    iget-object p1, p0, Lcom/squareup/noho/NohoEditRow;->plugins:Ljava/util/List;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-interface {p1}, Lcom/squareup/noho/NohoEditRow$Plugin;->isClickable()Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, -0x1

    :goto_2
    return v2
.end method

.method private final updateGravity()V
    .locals 2

    .line 365
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->getMaxLines()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x10

    goto :goto_0

    :cond_0
    const/16 v0, 0x30

    .line 366
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->getAlignment()Lcom/squareup/noho/NohoEditRow$Side;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoEditRow$Side;->getAssociatedGravity()I

    move-result v1

    or-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoEditRow;->setGravity(I)V

    .line 367
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->invalidateDrawingInfo()V

    return-void
.end method


# virtual methods
.method public final addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V
    .locals 1

    const-string v0, "plugin"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    invoke-interface {p1, p0}, Lcom/squareup/noho/NohoEditRow$Plugin;->attach(Lcom/squareup/noho/NohoEditRow;)V

    .line 212
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow;->plugins:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->invalidateDrawingInfo()V

    .line 214
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->invalidate()V

    return-void
.end method

.method public addSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V
    .locals 1

    const-string/jumbo v0, "watcher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 373
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Don\'t use NohoEditRow as a HasSelectionText. See DESIGNSYS-285."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method protected dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 315
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow;->touchHelper:Lcom/squareup/noho/NohoEditRow$TouchHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditRow$TouchHelper;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatEditText;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public final focusAndShowKeyboard()V
    .locals 1

    .line 408
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->requestFocus()Z

    const/4 v0, 0x1

    .line 409
    iput-boolean v0, p0, Lcom/squareup/noho/NohoEditRow;->showKeyboardDelayed:Z

    .line 410
    invoke-direct {p0}, Lcom/squareup/noho/NohoEditRow;->maybeShowKeyboard()V

    return-void
.end method

.method public final getAlignment()Lcom/squareup/noho/NohoEditRow$Side;
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow;->alignment$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoEditRow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditRow$Side;

    return-object v0
.end method

.method public final getOriginalPaddingLeft()I
    .locals 1

    .line 109
    iget v0, p0, Lcom/squareup/noho/NohoEditRow;->originalPaddingLeft:I

    return v0
.end method

.method public final getOriginalPaddingRight()I
    .locals 1

    .line 110
    iget v0, p0, Lcom/squareup/noho/NohoEditRow;->originalPaddingRight:I

    return v0
.end method

.method public final getPositionInList()Lcom/squareup/noho/NohoEditRow$PositionInList;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow;->_positionInList:Lcom/squareup/noho/NohoEditRow$PositionInList;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object v0
.end method

.method public final getScrubber()Lcom/squareup/text/ScrubbingTextWatcher;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow;->scrubber:Lcom/squareup/text/ScrubbingTextWatcher;

    return-object v0
.end method

.method public final invalidateDrawingInfo()V
    .locals 1

    const/4 v0, 0x0

    .line 230
    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/noho/NohoEditRow;->drawingInfos:Ljava/util/List;

    return-void
.end method

.method public final isError()Z
    .locals 1

    .line 63
    iget-boolean v0, p0, Lcom/squareup/noho/NohoEditRow;->isError:Z

    return v0
.end method

.method public final isValidated()Z
    .locals 1

    .line 69
    iget-boolean v0, p0, Lcom/squareup/noho/NohoEditRow;->isValidated:Z

    return v0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 1

    add-int/lit8 p1, p1, 0x3

    .line 200
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatEditText;->onCreateDrawableState(I)[I

    move-result-object p1

    .line 203
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow;->_positionInList:Lcom/squareup/noho/NohoEditRow$PositionInList;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/noho/NohoEditRow$PositionInList;->MIDDLE:Lcom/squareup/noho/NohoEditRow$PositionInList;

    :goto_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow$PositionInList;->getStateIds()[I

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/noho/NohoEditRow;->access$mergeDrawableStates$s-1884569510([I[I)[I

    .line 204
    iget-boolean v0, p0, Lcom/squareup/noho/NohoEditRow;->isError:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/squareup/noho/NohoEditRowKt;->access$getERROR_DRAWABLE_STATE$p()[I

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/noho/NohoEditRow;->access$mergeDrawableStates$s-1884569510([I[I)[I

    .line 205
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/noho/NohoEditRow;->isValidated:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/squareup/noho/NohoEditRowKt;->access$getVALIDATED_DRAWABLE_STATE$p()[I

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/noho/NohoEditRow;->access$mergeDrawableStates$s2666181([I[I)[I

    :cond_2
    const-string v0, "super.onCreateDrawableSt\u2026TED_DRAWABLE_STATE)\n    }"

    .line 200
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 345
    invoke-direct {p0}, Lcom/squareup/noho/NohoEditRow;->ensureDrawingInfo()Ljava/util/List;

    .line 346
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatEditText;->onDraw(Landroid/graphics/Canvas;)V

    .line 347
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoEditRow;->drawPlugins(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 0

    .line 332
    invoke-super {p0, p1, p2, p3}, Landroidx/appcompat/widget/AppCompatEditText;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 333
    iget-object p1, p0, Lcom/squareup/noho/NohoEditRow;->plugins:Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 615
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/noho/NohoEditRow$Plugin;

    .line 333
    invoke-interface {p2}, Lcom/squareup/noho/NohoEditRow$Plugin;->focusChanged()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    .line 339
    invoke-direct {p0}, Lcom/squareup/noho/NohoEditRow;->ensureDrawingInfo()Ljava/util/List;

    .line 340
    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatEditText;->onMeasure(II)V

    return-void
.end method

.method protected onSelectionChanged(II)V
    .locals 1

    .line 395
    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatEditText;->onSelectionChanged(II)V

    .line 397
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow;->scrubber:Lcom/squareup/text/ScrubbingTextWatcher;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/squareup/text/ScrubbingTextWatcher;->onSelectionChanged(II)V

    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 226
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->invalidateDrawingInfo()V

    .line 227
    invoke-super {p0, p1, p2, p3, p4}, Landroidx/appcompat/widget/AppCompatEditText;->onSizeChanged(IIII)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 285
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_4

    const/4 v2, -0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_1

    .line 295
    :cond_0
    iput v2, p0, Lcom/squareup/noho/NohoEditRow;->lastTouchArea:I

    goto :goto_1

    .line 297
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoEditRow;->pluginAreaFromMotionEvent(Landroid/view/MotionEvent;)I

    move-result v0

    if-ltz v0, :cond_2

    .line 298
    iget v3, p0, Lcom/squareup/noho/NohoEditRow;->lastTouchArea:I

    if-ne v3, v0, :cond_2

    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    .line 299
    :goto_0
    iput v2, p0, Lcom/squareup/noho/NohoEditRow;->lastTouchArea:I

    if-eqz v3, :cond_5

    .line 301
    invoke-direct {p0, v0}, Lcom/squareup/noho/NohoEditRow;->clickPlugin(I)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 304
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->requestFocus()Z

    :cond_3
    return v1

    .line 287
    :cond_4
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoEditRow;->pluginAreaFromMotionEvent(Landroid/view/MotionEvent;)I

    move-result v0

    iput v0, p0, Lcom/squareup/noho/NohoEditRow;->lastTouchArea:I

    .line 288
    iget v0, p0, Lcom/squareup/noho/NohoEditRow;->lastTouchArea:I

    if-ltz v0, :cond_5

    return v1

    .line 310
    :cond_5
    :goto_1
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatEditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    .line 414
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatEditText;->onWindowFocusChanged(Z)V

    .line 415
    invoke-direct {p0}, Lcom/squareup/noho/NohoEditRow;->maybeShowKeyboard()V

    return-void
.end method

.method public final synthetic plugin()Lcom/squareup/noho/NohoEditRow$Plugin;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/squareup/noho/NohoEditRow$Plugin;",
            ">()TT;"
        }
    .end annotation

    const/4 v0, 0x4

    const-string v1, "T"

    .line 121
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoEditRow;->plugin(Ljava/lang/Class;)Lcom/squareup/noho/NohoEditRow$Plugin;

    move-result-object v0

    return-object v0
.end method

.method public final plugin(Ljava/lang/Class;)Lcom/squareup/noho/NohoEditRow$Plugin;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/squareup/noho/NohoEditRow$Plugin;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "clazz"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow;->plugins:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 584
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/noho/NohoEditRow$Plugin;

    .line 125
    invoke-virtual {p1, v2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 585
    :goto_0
    check-cast v1, Lcom/squareup/noho/NohoEditRow$Plugin;

    return-object v1
.end method

.method public postRestartInput()V
    .locals 1

    .line 388
    new-instance v0, Lcom/squareup/noho/NohoEditRow$postRestartInput$1;

    invoke-direct {v0, p0}, Lcom/squareup/noho/NohoEditRow$postRestartInput$1;-><init>(Lcom/squareup/noho/NohoEditRow;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoEditRow;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final removePlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V
    .locals 1

    const-string v0, "plugin"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow;->plugins:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    invoke-interface {p1, p0}, Lcom/squareup/noho/NohoEditRow$Plugin;->detach(Lcom/squareup/noho/NohoEditRow;)V

    .line 220
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->invalidateDrawingInfo()V

    .line 221
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->invalidate()V

    :cond_0
    return-void
.end method

.method public removeSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V
    .locals 1

    const-string/jumbo v0, "watcher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 378
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Don\'t use NohoEditRow as a HasSelectionText. See DESIGNSYS-285."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final setAlignment(Lcom/squareup/noho/NohoEditRow$Side;)V
    .locals 3

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow;->alignment$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoEditRow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setError(Z)V
    .locals 0

    .line 65
    iput-boolean p1, p0, Lcom/squareup/noho/NohoEditRow;->isError:Z

    .line 66
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->refreshDrawableState()V

    return-void
.end method

.method public final setPositionInList(Lcom/squareup/noho/NohoEditRow$PositionInList;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iput-object p1, p0, Lcom/squareup/noho/NohoEditRow;->_positionInList:Lcom/squareup/noho/NohoEditRow$PositionInList;

    .line 60
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->refreshDrawableState()V

    return-void
.end method

.method public final setScrubber(Lcom/squareup/text/ScrubbingTextWatcher;)V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow;->scrubber:Lcom/squareup/text/ScrubbingTextWatcher;

    if-eqz v0, :cond_0

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoEditRow;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 86
    :cond_0
    iput-object p1, p0, Lcom/squareup/noho/NohoEditRow;->scrubber:Lcom/squareup/text/ScrubbingTextWatcher;

    .line 87
    iget-object p1, p0, Lcom/squareup/noho/NohoEditRow;->scrubber:Lcom/squareup/text/ScrubbingTextWatcher;

    if-eqz p1, :cond_1

    check-cast p1, Landroid/text/TextWatcher;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoEditRow;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_1
    return-void
.end method

.method public setSingleLine(Z)V
    .locals 0

    .line 360
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatEditText;->setSingleLine(Z)V

    .line 361
    invoke-direct {p0}, Lcom/squareup/noho/NohoEditRow;->updateGravity()V

    return-void
.end method

.method public final setValidated(Z)V
    .locals 0

    .line 71
    iput-boolean p1, p0, Lcom/squareup/noho/NohoEditRow;->isValidated:Z

    .line 72
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow;->refreshDrawableState()V

    return-void
.end method
