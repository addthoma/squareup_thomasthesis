.class public Lcom/squareup/noho/NohoCheckableGroup;
.super Lcom/squareup/noho/NohoLinearLayout;
.source "NohoCheckableGroup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;,
        Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;,
        Lcom/squareup/noho/NohoCheckableGroup$OnCheckedClickListener;,
        Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;,
        Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;
    }
.end annotation


# static fields
.field private static final CHECKED_IDS:Ljava/lang/String; = "checkedId"

.field private static final SUPER_STATE:Ljava/lang/String; = "super"


# instance fields
.field private addingColumns:Z

.field private final checkedIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private childOnCheckedChangeListener:Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;

.field private leftColumnLayout:Landroid/widget/LinearLayout;

.field private onCheckedChangeListener:Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

.field private onCheckedClickListener:Lcom/squareup/noho/NohoCheckableGroup$OnCheckedClickListener;

.field private passThroughListener:Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;

.field private protectFromCheckedChange:Z

.field private restoringState:Z

.field private rightColumnLayout:Landroid/widget/LinearLayout;

.field private singleChoice:Z

.field private final twoColumns:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    .line 125
    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/NohoLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 104
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    const/4 v0, 0x0

    .line 115
    iput-boolean v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->protectFromCheckedChange:Z

    .line 121
    iput-boolean v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->addingColumns:Z

    .line 126
    sget-object v1, Lcom/squareup/noho/R$styleable;->NohoCheckableGroup:[I

    const v2, 0x101007e

    invoke-virtual {p1, p2, v1, v2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 129
    sget v1, Lcom/squareup/noho/R$styleable;->NohoCheckableGroup_sqSingleChoice:I

    const/4 v2, 0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/squareup/noho/NohoCheckableGroup;->singleChoice:Z

    .line 130
    sget v1, Lcom/squareup/noho/R$styleable;->NohoCheckableGroup_sqTwoColumns:I

    invoke-virtual {p2, v1, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/squareup/noho/NohoCheckableGroup;->twoColumns:Z

    .line 131
    sget v1, Lcom/squareup/noho/R$styleable;->NohoCheckableGroup_sqCheckedButton:I

    const/4 v3, -0x1

    invoke-virtual {p2, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    if-eq v1, v3, :cond_0

    .line 133
    iget-object v3, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 136
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;-><init>(Lcom/squareup/noho/NohoCheckableGroup;Lcom/squareup/noho/NohoCheckableGroup$1;)V

    iput-object v1, p0, Lcom/squareup/noho/NohoCheckableGroup;->childOnCheckedChangeListener:Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;

    .line 137
    new-instance v1, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;

    invoke-direct {v1, p0, v3}, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;-><init>(Lcom/squareup/noho/NohoCheckableGroup;Lcom/squareup/noho/NohoCheckableGroup$1;)V

    iput-object v1, p0, Lcom/squareup/noho/NohoCheckableGroup;->passThroughListener:Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;

    .line 139
    iget-boolean v1, p0, Lcom/squareup/noho/NohoCheckableGroup;->twoColumns:Z

    if-eqz v1, :cond_2

    .line 140
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoCheckableGroup;->setOrientation(I)V

    .line 142
    invoke-virtual {p0}, Lcom/squareup/noho/NohoCheckableGroup;->getShowDividers()I

    move-result v1

    .line 143
    sget v4, Lcom/squareup/noho/R$styleable;->NohoCheckableGroup_android_divider:I

    invoke-virtual {p2, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 145
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    .line 148
    :goto_0
    new-instance v6, Lcom/squareup/noho/NohoLinearLayout;

    invoke-direct {v6, p1, v3}, Lcom/squareup/noho/NohoLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v6, p0, Lcom/squareup/noho/NohoCheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    .line 149
    iget-object v6, p0, Lcom/squareup/noho/NohoCheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 150
    iget-object v6, p0, Lcom/squareup/noho/NohoCheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v4}, Landroid/widget/LinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 151
    iget-object v6, p0, Lcom/squareup/noho/NohoCheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v1}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    .line 152
    new-instance v6, Lcom/squareup/noho/NohoLinearLayout;

    invoke-direct {v6, p1, v3}, Lcom/squareup/noho/NohoLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v6, p0, Lcom/squareup/noho/NohoCheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    .line 153
    iget-object p1, p0, Lcom/squareup/noho/NohoCheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 154
    iget-object p1, p0, Lcom/squareup/noho/NohoCheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v4}, Landroid/widget/LinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 155
    iget-object p1, p0, Lcom/squareup/noho/NohoCheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    .line 157
    new-instance p1, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v3, -0x2

    invoke-direct {p1, v0, v3, v1}, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;-><init>(IIF)V

    .line 158
    new-instance v4, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;

    invoke-direct {v4, v0, v3, v1}, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;-><init>(IIF)V

    .line 159
    invoke-virtual {v4, v5, v0, v0, v0}, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;->setMargins(IIII)V

    .line 160
    iput-boolean v2, p0, Lcom/squareup/noho/NohoCheckableGroup;->addingColumns:Z

    .line 161
    iget-object v1, p0, Lcom/squareup/noho/NohoCheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1, p1}, Lcom/squareup/noho/NohoCheckableGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 162
    iget-object p1, p0, Lcom/squareup/noho/NohoCheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, p1, v4}, Lcom/squareup/noho/NohoCheckableGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    iput-boolean v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->addingColumns:Z

    .line 165
    iget-object p1, p0, Lcom/squareup/noho/NohoCheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->passThroughListener:Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 166
    iget-object p1, p0, Lcom/squareup/noho/NohoCheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->passThroughListener:Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    goto :goto_1

    .line 168
    :cond_2
    sget p1, Lcom/squareup/noho/R$styleable;->NohoCheckableGroup_sqIsHorizontal:I

    .line 169
    invoke-virtual {p2, p1, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p1

    xor-int/2addr p1, v2

    .line 170
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoCheckableGroup;->setOrientation(I)V

    .line 171
    iget-object p1, p0, Lcom/squareup/noho/NohoCheckableGroup;->passThroughListener:Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;

    invoke-super {p0, p1}, Lcom/squareup/noho/NohoLinearLayout;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 174
    :goto_1
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/noho/NohoCheckableGroup;)Landroid/widget/LinearLayout;
    .locals 0

    .line 94
    iget-object p0, p0, Lcom/squareup/noho/NohoCheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/noho/NohoCheckableGroup;)Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;
    .locals 0

    .line 94
    iget-object p0, p0, Lcom/squareup/noho/NohoCheckableGroup;->childOnCheckedChangeListener:Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/noho/NohoCheckableGroup;)Z
    .locals 0

    .line 94
    iget-boolean p0, p0, Lcom/squareup/noho/NohoCheckableGroup;->protectFromCheckedChange:Z

    return p0
.end method

.method static synthetic access$302(Lcom/squareup/noho/NohoCheckableGroup;Z)Z
    .locals 0

    .line 94
    iput-boolean p1, p0, Lcom/squareup/noho/NohoCheckableGroup;->protectFromCheckedChange:Z

    return p1
.end method

.method static synthetic access$400(Lcom/squareup/noho/NohoCheckableGroup;)Ljava/util/Set;
    .locals 0

    .line 94
    iget-object p0, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/noho/NohoCheckableGroup;)Z
    .locals 0

    .line 94
    iget-boolean p0, p0, Lcom/squareup/noho/NohoCheckableGroup;->singleChoice:Z

    return p0
.end method

.method static synthetic access$600(Lcom/squareup/noho/NohoCheckableGroup;IZ)Z
    .locals 0

    .line 94
    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/NohoCheckableGroup;->setCheckedStateForView(IZ)Z

    move-result p0

    return p0
.end method

.method static synthetic access$700(Lcom/squareup/noho/NohoCheckableGroup;I)V
    .locals 0

    .line 94
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoCheckableGroup;->setCheckedId(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/squareup/noho/NohoCheckableGroup;)Lcom/squareup/noho/NohoCheckableGroup$OnCheckedClickListener;
    .locals 0

    .line 94
    iget-object p0, p0, Lcom/squareup/noho/NohoCheckableGroup;->onCheckedClickListener:Lcom/squareup/noho/NohoCheckableGroup$OnCheckedClickListener;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/noho/NohoCheckableGroup;)Landroid/widget/LinearLayout;
    .locals 0

    .line 94
    iget-object p0, p0, Lcom/squareup/noho/NohoCheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method private setCheckedId(I)V
    .locals 3

    .line 297
    iget-boolean v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->singleChoice:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 299
    iget-object v1, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    .line 303
    :goto_0
    iget-object v1, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 306
    iget-object v1, p0, Lcom/squareup/noho/NohoCheckableGroup;->onCheckedChangeListener:Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    if-eqz v1, :cond_1

    iget-boolean v2, p0, Lcom/squareup/noho/NohoCheckableGroup;->restoringState:Z

    if-nez v2, :cond_1

    .line 307
    invoke-interface {v1, p0, p1, v0}, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;->onCheckedChanged(Lcom/squareup/noho/NohoCheckableGroup;II)V

    :cond_1
    return-void
.end method

.method private setCheckedStateForView(IZ)Z
    .locals 1

    .line 313
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoCheckableGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 314
    instance-of v0, p1, Landroid/widget/Checkable;

    if-eqz v0, :cond_0

    .line 315
    check-cast p1, Landroid/widget/Checkable;

    invoke-interface {p1, p2}, Landroid/widget/Checkable;->setChecked(Z)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .line 196
    iget-boolean v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->addingColumns:Z

    if-eqz v0, :cond_0

    .line 197
    invoke-super {p0, p1, p2, p3}, Lcom/squareup/noho/NohoLinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void

    .line 200
    :cond_0
    instance-of v0, p1, Landroid/widget/Checkable;

    if-eqz v0, :cond_2

    .line 201
    move-object v0, p1

    check-cast v0, Landroid/widget/Checkable;

    .line 202
    invoke-interface {v0}, Landroid/widget/Checkable;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 203
    iput-boolean v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->protectFromCheckedChange:Z

    .line 205
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->singleChoice:Z

    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 207
    invoke-direct {p0, v0, v1}, Lcom/squareup/noho/NohoCheckableGroup;->setCheckedStateForView(IZ)Z

    .line 210
    :cond_1
    iput-boolean v1, p0, Lcom/squareup/noho/NohoCheckableGroup;->protectFromCheckedChange:Z

    .line 211
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/noho/NohoCheckableGroup;->setCheckedId(I)V

    .line 215
    :cond_2
    iget-boolean v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->twoColumns:Z

    if-eqz v0, :cond_4

    .line 216
    iget-object p2, p0, Lcom/squareup/noho/NohoCheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result p2

    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ne p2, v0, :cond_3

    .line 217
    iget-object p2, p0, Lcom/squareup/noho/NohoCheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p2, p1, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 219
    :cond_3
    iget-object p2, p0, Lcom/squareup/noho/NohoCheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p2, p1, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 222
    :cond_4
    invoke-super {p0, p1, p2, p3}, Lcom/squareup/noho/NohoLinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    return-void
.end method

.method public check(I)V
    .locals 3

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 249
    iget-object v1, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 254
    :cond_0
    iget-object v1, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/squareup/noho/NohoCheckableGroup;->singleChoice:Z

    if-eqz v1, :cond_1

    .line 255
    iget-object v1, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    .line 256
    invoke-direct {p0, v1, v2}, Lcom/squareup/noho/NohoCheckableGroup;->setCheckedStateForView(IZ)Z

    :cond_1
    if-eq p1, v0, :cond_2

    const/4 v0, 0x1

    .line 260
    invoke-direct {p0, p1, v0}, Lcom/squareup/noho/NohoCheckableGroup;->setCheckedStateForView(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 261
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoCheckableGroup;->setCheckedId(I)V

    goto :goto_0

    .line 263
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/noho/NohoCheckableGroup;->clearChecked()V

    :goto_0
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 0

    .line 400
    instance-of p1, p1, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;

    return p1
.end method

.method public clearChecked()V
    .locals 3

    .line 364
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    .line 365
    invoke-direct {p0, v1, v2}, Lcom/squareup/noho/NohoCheckableGroup;->setCheckedStateForView(IZ)Z

    goto :goto_0

    .line 367
    :cond_0
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/noho/NohoCheckableGroup;->generateDefaultLayoutParams()Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/noho/NohoCheckableGroup;->generateDefaultLayoutParams()Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;
    .locals 2

    .line 404
    new-instance v0, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 0

    .line 94
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoCheckableGroup;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 0

    .line 94
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoCheckableGroup;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;

    move-result-object p1

    return-object p1
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;
    .locals 2

    .line 395
    new-instance v0, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoCheckableGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public getCheckedId()I
    .locals 2

    .line 344
    iget-boolean v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->singleChoice:Z

    if-eqz v0, :cond_1

    .line 347
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    return v0

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    .line 345
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t get single checked Id in multiChoice mode!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCheckedIds()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 330
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    return-object v0

    .line 331
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getOnCheckedChangeListener()Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;
    .locals 1

    .line 380
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->onCheckedChangeListener:Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    .line 353
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 185
    invoke-super {p0}, Lcom/squareup/noho/NohoLinearLayout;->onFinishInflate()V

    const/4 v0, 0x1

    .line 188
    iput-boolean v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->protectFromCheckedChange:Z

    .line 189
    iget-object v1, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 190
    invoke-direct {p0, v2, v0}, Lcom/squareup/noho/NohoCheckableGroup;->setCheckedStateForView(IZ)Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 192
    iput-boolean v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->protectFromCheckedChange:Z

    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .line 408
    invoke-super {p0, p1}, Lcom/squareup/noho/NohoLinearLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 409
    const-class v0, Lcom/squareup/noho/NohoCheckableGroup;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .line 413
    invoke-super {p0, p1}, Lcom/squareup/noho/NohoLinearLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 414
    const-class v0, Lcom/squareup/noho/NohoCheckableGroup;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 4

    .line 431
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "super"

    .line 432
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/squareup/noho/NohoLinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v0, "checkedId"

    .line 433
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object p1

    const/4 v0, 0x1

    .line 434
    iput-boolean v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->restoringState:Z

    .line 435
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget v3, p1, v2

    .line 436
    invoke-virtual {p0, v3}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 438
    :cond_0
    iput-boolean v1, p0, Lcom/squareup/noho/NohoCheckableGroup;->restoringState:Z

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 6

    .line 418
    invoke-super {p0}, Lcom/squareup/noho/NohoLinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 419
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "super"

    .line 420
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 421
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [I

    .line 423
    iget-object v2, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v5, v3, 0x1

    .line 424
    aput v4, v0, v3

    move v3, v5

    goto :goto_0

    :cond_0
    const-string v2, "checkedId"

    .line 426
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    return-object v1
.end method

.method public setOnCheckedChangeListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;)V
    .locals 0

    .line 376
    iput-object p1, p0, Lcom/squareup/noho/NohoCheckableGroup;->onCheckedChangeListener:Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    return-void
.end method

.method public setOnCheckedClickListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedClickListener;)V
    .locals 0

    .line 390
    iput-object p1, p0, Lcom/squareup/noho/NohoCheckableGroup;->onCheckedClickListener:Lcom/squareup/noho/NohoCheckableGroup$OnCheckedClickListener;

    return-void
.end method

.method public setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->passThroughListener:Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;

    invoke-static {v0, p1}, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->access$202(Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;Landroid/view/ViewGroup$OnHierarchyChangeListener;)Landroid/view/ViewGroup$OnHierarchyChangeListener;

    return-void
.end method

.method public setSingleChoiceMode(Z)V
    .locals 0

    .line 234
    iput-boolean p1, p0, Lcom/squareup/noho/NohoCheckableGroup;->singleChoice:Z

    if-eqz p1, :cond_0

    .line 235
    invoke-virtual {p0}, Lcom/squareup/noho/NohoCheckableGroup;->clearChecked()V

    :cond_0
    return-void
.end method

.method public startAnimationOnCheckable(ILandroid/view/animation/Animation;)V
    .locals 0

    .line 287
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoCheckableGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 291
    invoke-virtual {p1, p2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void

    .line 289
    :cond_0
    new-instance p1, Ljava/security/InvalidParameterException;

    const-string p2, "The checkable doesn\'t exist."

    invoke-direct {p1, p2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public toggle(I)V
    .locals 2

    .line 279
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoCheckableGroup;->uncheck(I)V

    goto :goto_0

    .line 282
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    :goto_0
    return-void
.end method

.method public uncheck(I)V
    .locals 2

    const/4 v0, 0x0

    .line 268
    invoke-direct {p0, p1, v0}, Lcom/squareup/noho/NohoCheckableGroup;->setCheckedStateForView(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 272
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup;->onCheckedChangeListener:Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    if-eqz v0, :cond_0

    const/4 v1, -0x1

    .line 273
    invoke-interface {v0, p0, p1, v1}, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;->onCheckedChanged(Lcom/squareup/noho/NohoCheckableGroup;II)V

    :cond_0
    return-void
.end method
