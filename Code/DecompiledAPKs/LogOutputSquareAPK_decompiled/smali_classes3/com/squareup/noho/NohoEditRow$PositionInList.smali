.class public final enum Lcom/squareup/noho/NohoEditRow$PositionInList;
.super Ljava/lang/Enum;
.source "NohoEditRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoEditRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PositionInList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/noho/NohoEditRow$PositionInList;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0015\n\u0002\u0008\u0007\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/noho/NohoEditRow$PositionInList;",
        "",
        "stateIds",
        "",
        "(Ljava/lang/String;I[I)V",
        "getStateIds",
        "()[I",
        "FIRST",
        "MIDDLE",
        "LAST",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/noho/NohoEditRow$PositionInList;

.field public static final enum FIRST:Lcom/squareup/noho/NohoEditRow$PositionInList;

.field public static final enum LAST:Lcom/squareup/noho/NohoEditRow$PositionInList;

.field public static final enum MIDDLE:Lcom/squareup/noho/NohoEditRow$PositionInList;


# instance fields
.field private final stateIds:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/noho/NohoEditRow$PositionInList;

    new-instance v1, Lcom/squareup/noho/NohoEditRow$PositionInList;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v4, 0x0

    const v5, 0x10100a4

    aput v5, v3, v4

    const-string v5, "FIRST"

    .line 504
    invoke-direct {v1, v5, v4, v3}, Lcom/squareup/noho/NohoEditRow$PositionInList;-><init>(Ljava/lang/String;I[I)V

    sput-object v1, Lcom/squareup/noho/NohoEditRow$PositionInList;->FIRST:Lcom/squareup/noho/NohoEditRow$PositionInList;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/noho/NohoEditRow$PositionInList;

    new-array v3, v2, [I

    const v5, 0x10100a5

    aput v5, v3, v4

    const-string v5, "MIDDLE"

    .line 505
    invoke-direct {v1, v5, v2, v3}, Lcom/squareup/noho/NohoEditRow$PositionInList;-><init>(Ljava/lang/String;I[I)V

    sput-object v1, Lcom/squareup/noho/NohoEditRow$PositionInList;->MIDDLE:Lcom/squareup/noho/NohoEditRow$PositionInList;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/noho/NohoEditRow$PositionInList;

    new-array v2, v2, [I

    const v3, 0x10100a6

    aput v3, v2, v4

    const/4 v3, 0x2

    const-string v4, "LAST"

    .line 506
    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/noho/NohoEditRow$PositionInList;-><init>(Ljava/lang/String;I[I)V

    sput-object v1, Lcom/squareup/noho/NohoEditRow$PositionInList;->LAST:Lcom/squareup/noho/NohoEditRow$PositionInList;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/noho/NohoEditRow$PositionInList;->$VALUES:[Lcom/squareup/noho/NohoEditRow$PositionInList;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I[I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)V"
        }
    .end annotation

    .line 503
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/noho/NohoEditRow$PositionInList;->stateIds:[I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/noho/NohoEditRow$PositionInList;
    .locals 1

    const-class v0, Lcom/squareup/noho/NohoEditRow$PositionInList;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/noho/NohoEditRow$PositionInList;

    return-object p0
.end method

.method public static values()[Lcom/squareup/noho/NohoEditRow$PositionInList;
    .locals 1

    sget-object v0, Lcom/squareup/noho/NohoEditRow$PositionInList;->$VALUES:[Lcom/squareup/noho/NohoEditRow$PositionInList;

    invoke-virtual {v0}, [Lcom/squareup/noho/NohoEditRow$PositionInList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/noho/NohoEditRow$PositionInList;

    return-object v0
.end method


# virtual methods
.method public final getStateIds()[I
    .locals 1

    .line 503
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow$PositionInList;->stateIds:[I

    return-object v0
.end method
