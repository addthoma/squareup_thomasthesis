.class public final Lcom/squareup/noho/NohoInputBox$Suggestion$span$1;
.super Landroid/text/style/ClickableSpan;
.source "NohoInputBox.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/noho/NohoInputBox$Suggestion;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoInputBox.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoInputBox.kt\ncom/squareup/noho/NohoInputBox$Suggestion$span$1\n*L\n1#1,330:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/noho/NohoInputBox$Suggestion$span$1",
        "Landroid/text/style/ClickableSpan;",
        "onClick",
        "",
        "view",
        "Landroid/view/View;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/noho/NohoInputBox$Suggestion;


# direct methods
.method constructor <init>(Lcom/squareup/noho/NohoInputBox$Suggestion;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 312
    iput-object p1, p0, Lcom/squareup/noho/NohoInputBox$Suggestion$span$1;->this$0:Lcom/squareup/noho/NohoInputBox$Suggestion;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 314
    invoke-static {p1}, Lcom/squareup/debounce/Debouncers;->attemptPerform(Landroid/view/View;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 315
    iget-object p1, p0, Lcom/squareup/noho/NohoInputBox$Suggestion$span$1;->this$0:Lcom/squareup/noho/NohoInputBox$Suggestion;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoInputBox$Suggestion;->getAction()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 316
    iget-object v0, p0, Lcom/squareup/noho/NohoInputBox$Suggestion$span$1;->this$0:Lcom/squareup/noho/NohoInputBox$Suggestion;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoInputBox$Suggestion;->getEditToFix$noho_release()Lcom/squareup/noho/NohoEditRow;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/noho/NohoInputBox$Suggestion$span$1;->this$0:Lcom/squareup/noho/NohoInputBox$Suggestion;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoInputBox$Suggestion;->getEditToFix$noho_release()Lcom/squareup/noho/NohoEditRow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoEditRow;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    sget-object v1, Landroid/widget/TextView$BufferType;->EDITABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/noho/NohoEditRow;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    :cond_0
    return-void
.end method
