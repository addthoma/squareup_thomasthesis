.class public final Lcom/squareup/noho/utils/ScrollViewUtils;
.super Ljava/lang/Object;
.source "ScrollViewUtils.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/utils/ScrollViewUtils$NeedsMeasurementFix;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u000fB\u0005\u00a2\u0006\u0002\u0010\u0002J(\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000c2\u0006\u0010\r\u001a\u00020\u000eR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/noho/utils/ScrollViewUtils;",
        "",
        "()V",
        "paddingDelegate",
        "Lcom/squareup/noho/NohoPaddingDelegate;",
        "setPadding",
        "",
        "view",
        "Landroid/widget/FrameLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "NeedsMeasurementFix",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private paddingDelegate:Lcom/squareup/noho/NohoPaddingDelegate;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final setPadding(Landroid/widget/FrameLayout;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    new-instance v0, Lcom/squareup/noho/NohoPaddingDelegate;

    check-cast p1, Landroid/view/View;

    invoke-direct {v0, p1}, Lcom/squareup/noho/NohoPaddingDelegate;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/squareup/noho/utils/ScrollViewUtils;->paddingDelegate:Lcom/squareup/noho/NohoPaddingDelegate;

    .line 40
    sget-object p1, Lcom/squareup/noho/R$styleable;->NohoScrollView:[I

    .line 41
    sget v0, Lcom/squareup/noho/R$style;->Widget_Noho_ScrollView:I

    .line 39
    invoke-virtual {p2, p3, p1, p4, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 43
    sget p2, Lcom/squareup/noho/R$styleable;->NohoScrollView_sqContentPaddingType:I

    invoke-static {p1, p2}, Lcom/squareup/noho/NohoPaddingDelegate;->getEnum(Landroid/content/res/TypedArray;I)Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;

    move-result-object p2

    .line 44
    sget p3, Lcom/squareup/noho/R$styleable;->NohoScrollView_sqIsAlert:I

    const/4 p4, 0x0

    invoke-virtual {p1, p3, p4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p3

    .line 45
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 46
    iget-object p1, p0, Lcom/squareup/noho/utils/ScrollViewUtils;->paddingDelegate:Lcom/squareup/noho/NohoPaddingDelegate;

    if-nez p1, :cond_0

    const-string p4, "paddingDelegate"

    invoke-static {p4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1, p2, p3}, Lcom/squareup/noho/NohoPaddingDelegate;->setContentPadding(Lcom/squareup/noho/NohoPaddingDelegate$ContentPadding;Z)V

    return-void
.end method
