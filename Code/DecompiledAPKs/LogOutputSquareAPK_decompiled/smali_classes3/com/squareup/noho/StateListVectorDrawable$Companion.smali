.class public final Lcom/squareup/noho/StateListVectorDrawable$Companion;
.super Ljava/lang/Object;
.source "StateListVectorDrawable.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/StateListVectorDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStateListVectorDrawable.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StateListVectorDrawable.kt\ncom/squareup/noho/StateListVectorDrawable$Companion\n+ 2 XmlResourceParsing.kt\ncom/squareup/android/xml/XmlResourceParsingKt\n+ 3 XmlResourceParsing.kt\ncom/squareup/android/xml/TagVisitor\n*L\n1#1,150:1\n22#2,3:151\n38#2:154\n26#2,2:167\n38#2:169\n180#3:155\n165#3,6:156\n181#3,5:162\n207#3:170\n192#3,5:171\n208#3,4:176\n197#3,2:180\n212#3:182\n*E\n*S KotlinDebug\n*F\n+ 1 StateListVectorDrawable.kt\ncom/squareup/noho/StateListVectorDrawable$Companion\n*L\n78#1,3:151\n78#1:154\n78#1,2:167\n97#1:169\n78#1:155\n78#1,6:156\n78#1,5:162\n97#1:170\n97#1,5:171\n97#1,4:176\n97#1,2:180\n97#1:182\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0015\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J$\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u000b2\n\u0010\u0017\u001a\u00060\u0018R\u00020\u0015H\u0007J9\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 2\n\u0010\u0017\u001a\u00060\u0018R\u00020\u0015H\u0000\u00a2\u0006\u0002\u0008!R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\n \t*\u0004\u0018\u00010\u00080\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000bX\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/noho/StateListVectorDrawable$Companion;",
        "",
        "()V",
        "DEFAULT_STYLE",
        "",
        "DRAWABLE",
        "ITEM",
        "ITEM_ATTRIBUTES",
        "",
        "kotlin.jvm.PlatformType",
        "ITEM_DEFAULT_STYLE",
        "",
        "ITEM_DRAWABLE",
        "ITEM_STYLE",
        "NO_ID",
        "SELECTOR",
        "STYLE",
        "VECTOR",
        "create",
        "Landroid/graphics/drawable/Drawable;",
        "resources",
        "Landroid/content/res/Resources;",
        "drawableId",
        "theme",
        "Landroid/content/res/Resources$Theme;",
        "inflate",
        "",
        "stateListDrawable",
        "Landroid/graphics/drawable/StateListDrawable;",
        "parser",
        "Lorg/xmlpull/v1/XmlPullParser;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "inflate$noho_release",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 57
    invoke-direct {p0}, Lcom/squareup/noho/StateListVectorDrawable$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final create(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
    .locals 7
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "theme"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 151
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object p2

    const-string v1, "getXml(resourceId)"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    :try_start_0
    new-instance v1, Lcom/squareup/android/xml/TagVisitor;

    move-object v2, p2

    check-cast v2, Lorg/xmlpull/v1/XmlPullParser;

    move-object v3, p2

    check-cast v3, Landroid/util/AttributeSet;

    invoke-direct {v1, v2, v3}, Lcom/squareup/android/xml/TagVisitor;-><init>(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V

    const-string v2, "selector"

    .line 156
    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    if-nez v3, :cond_2

    .line 160
    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/android/xml/XmlResourceParsingKt;->nextChildTag(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 162
    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-nez v3, :cond_0

    .line 80
    sget-object v2, Lcom/squareup/noho/StateListVectorDrawable;->Companion:Lcom/squareup/noho/StateListVectorDrawable$Companion;

    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getAttrs()Landroid/util/AttributeSet;

    move-result-object v5

    move-object v1, v2

    move-object v2, v0

    move-object v3, p1

    move-object v6, p3

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/noho/StateListVectorDrawable$Companion;->inflate$noho_release(Landroid/graphics/drawable/StateListDrawable;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V

    .line 82
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167
    invoke-interface {p2}, Landroid/content/res/XmlResourceParser;->close()V

    .line 83
    check-cast v0, Landroid/graphics/drawable/Drawable;

    return-object v0

    .line 163
    :cond_0
    :try_start_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Root differs: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " != "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 160
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p3, "No root element found"

    invoke-direct {p1, p3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 158
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p3, "Not at beginning of visitRoot"

    invoke-direct {p1, p3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    .line 167
    invoke-interface {p2}, Landroid/content/res/XmlResourceParser;->close()V

    throw p1
.end method

.method public final inflate$noho_release(Landroid/graphics/drawable/StateListDrawable;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
    .locals 24

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    move-object/from16 v15, p4

    const-string v0, "stateListDrawable"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parser"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "theme"

    move-object/from16 v11, p5

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    new-instance v10, Landroid/util/SparseArray;

    invoke-direct {v10}, Landroid/util/SparseArray;-><init>()V

    .line 169
    new-instance v9, Lcom/squareup/android/xml/TagVisitor;

    invoke-direct {v9, v14, v15}, Lcom/squareup/android/xml/TagVisitor;-><init>(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V

    .line 99
    new-instance v8, Lkotlin/jvm/internal/Ref$BooleanRef;

    invoke-direct {v8}, Lkotlin/jvm/internal/Ref$BooleanRef;-><init>()V

    const/4 v7, 0x1

    iput-boolean v7, v8, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    .line 171
    invoke-virtual {v9}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/android/xml/XmlResourceParsingKt;->nextChildTag(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v8

    const/4 v3, 0x1

    goto/16 :goto_5

    .line 176
    :cond_0
    :goto_0
    invoke-virtual {v9}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "item"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v16, v0

    check-cast v16, Ljava/util/List;

    .line 103
    new-instance v6, Lkotlin/jvm/internal/Ref$IntRef;

    invoke-direct {v6}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    const/4 v5, 0x0

    iput v5, v6, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 104
    new-instance v4, Lkotlin/jvm/internal/Ref$IntRef;

    invoke-direct {v4}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    iput v5, v4, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 105
    new-instance v3, Lkotlin/jvm/internal/Ref$IntRef;

    invoke-direct {v3}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    iput v5, v3, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 107
    invoke-static {}, Lcom/squareup/noho/StateListVectorDrawable;->access$getITEM_ATTRIBUTES$cp()[I

    move-result-object v2

    const-string v0, "ITEM_ATTRIBUTES"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v17, 0x0

    const/16 v18, 0x0

    new-instance v19, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;

    move-object/from16 v0, v19

    move-object v1, v6

    move-object/from16 v20, v2

    move-object v2, v4

    move-object/from16 v21, v3

    move-object v14, v4

    move-object/from16 v4, v16

    const/4 v15, 0x0

    move-object v5, v8

    move-object v15, v6

    move-object/from16 v6, p5

    const/4 v12, 0x1

    move-object/from16 v7, p4

    move-object/from16 v22, v8

    move-object/from16 v8, p3

    move-object/from16 v23, v9

    move-object v9, v10

    move-object v12, v10

    move-object/from16 v10, p2

    move-object/from16 v11, p1

    invoke-direct/range {v0 .. v11}, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;-><init>(Lkotlin/jvm/internal/Ref$IntRef;Lkotlin/jvm/internal/Ref$IntRef;Lkotlin/jvm/internal/Ref$IntRef;Ljava/util/List;Lkotlin/jvm/internal/Ref$BooleanRef;Landroid/content/res/Resources$Theme;Landroid/util/AttributeSet;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/SparseArray;Landroid/content/res/Resources;Landroid/graphics/drawable/StateListDrawable;)V

    move-object/from16 v6, v19

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/16 v7, 0xc

    const/4 v8, 0x0

    move-object/from16 v1, v23

    move-object/from16 v2, p5

    move-object/from16 v3, v20

    move/from16 v4, v17

    move/from16 v5, v18

    invoke-static/range {v1 .. v8}, Lcom/squareup/android/xml/TagVisitor;->visitStyledAttributes$default(Lcom/squareup/android/xml/TagVisitor;Landroid/content/res/Resources$Theme;[IIILkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    move-object/from16 v0, v21

    .line 121
    iget v1, v0, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_5

    .line 124
    iget v1, v15, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    if-nez v1, :cond_2

    iget v1, v14, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    if-nez v1, :cond_2

    move-object/from16 v1, p5

    :goto_2
    const/4 v3, 0x1

    goto :goto_3

    .line 131
    :cond_2
    iget v1, v15, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    if-nez v1, :cond_3

    .line 132
    iget v1, v14, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    iput v1, v15, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 135
    :cond_3
    iget v1, v15, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    invoke-virtual {v12, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources$Theme;

    if-eqz v1, :cond_4

    goto :goto_2

    :cond_4
    invoke-virtual/range {p2 .. p2}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 136
    iget v2, v15, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 137
    iget v2, v15, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    invoke-virtual {v12, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 141
    :goto_3
    iget v0, v0, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    invoke-static {v13, v0, v1}, Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;->create(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;

    move-result-object v0

    .line 142
    check-cast v16, Ljava/util/Collection;

    invoke-static/range {v16 .. v16}, Lkotlin/collections/CollectionsKt;->toIntArray(Ljava/util/Collection;)[I

    move-result-object v1

    check-cast v0, Landroid/graphics/drawable/Drawable;

    move-object/from16 v2, p1

    invoke-virtual {v2, v1, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, v22

    const/4 v1, 0x0

    .line 143
    iput-boolean v1, v0, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    goto :goto_4

    .line 121
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "drawable cannot be undefined at "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface/range {p3 .. p3}, Lorg/xmlpull/v1/XmlPullParser;->getLineNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_6
    move-object v0, v8

    move-object/from16 v23, v9

    move-object v2, v12

    const/4 v3, 0x1

    move-object v12, v10

    .line 180
    :goto_4
    invoke-virtual/range {v23 .. v23}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/android/xml/XmlResourceParsingKt;->nextSiblingTag(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 145
    :goto_5
    iget-boolean v0, v0, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    xor-int/2addr v0, v3

    if-eqz v0, :cond_7

    return-void

    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "There are no items"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_8
    move-object/from16 v14, p3

    move-object/from16 v15, p4

    move-object/from16 v11, p5

    move-object v8, v0

    move-object v10, v12

    move-object/from16 v9, v23

    const/4 v7, 0x1

    move-object v12, v2

    goto/16 :goto_0
.end method
