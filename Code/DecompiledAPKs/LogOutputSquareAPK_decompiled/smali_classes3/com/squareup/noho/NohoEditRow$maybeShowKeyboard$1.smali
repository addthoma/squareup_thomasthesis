.class final Lcom/squareup/noho/NohoEditRow$maybeShowKeyboard$1;
.super Ljava/lang/Object;
.source "NohoEditRow.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/noho/NohoEditRow;->maybeShowKeyboard()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/noho/NohoEditRow;


# direct methods
.method constructor <init>(Lcom/squareup/noho/NohoEditRow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/noho/NohoEditRow$maybeShowKeyboard$1;->this$0:Lcom/squareup/noho/NohoEditRow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 422
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow$maybeShowKeyboard$1;->this$0:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 423
    iget-object v1, p0, Lcom/squareup/noho/NohoEditRow$maybeShowKeyboard$1;->this$0:Lcom/squareup/noho/NohoEditRow;

    check-cast v1, Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    return-void

    .line 422
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.view.inputmethod.InputMethodManager"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
