.class final Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StateListVectorDrawable.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/noho/StateListVectorDrawable$Companion;->inflate$noho_release(Landroid/graphics/drawable/StateListDrawable;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/android/xml/StyledAttributesVisitor;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003\u00a8\u0006\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/android/xml/StyledAttributesVisitor;",
        "invoke",
        "com/squareup/noho/StateListVectorDrawable$Companion$inflate$1$1$1",
        "com/squareup/noho/StateListVectorDrawable$Companion$$special$$inlined$visitChildren$lambda$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $attrs$inlined:Landroid/util/AttributeSet;

.field final synthetic $defaultStyleId:Lkotlin/jvm/internal/Ref$IntRef;

.field final synthetic $empty$inlined:Lkotlin/jvm/internal/Ref$BooleanRef;

.field final synthetic $parser$inlined:Lorg/xmlpull/v1/XmlPullParser;

.field final synthetic $resources$inlined:Landroid/content/res/Resources;

.field final synthetic $stateListDrawable$inlined:Landroid/graphics/drawable/StateListDrawable;

.field final synthetic $states:Ljava/util/List;

.field final synthetic $styleId:Lkotlin/jvm/internal/Ref$IntRef;

.field final synthetic $stylesCache$inlined:Landroid/util/SparseArray;

.field final synthetic $theme$inlined:Landroid/content/res/Resources$Theme;

.field final synthetic $vectorId:Lkotlin/jvm/internal/Ref$IntRef;


# direct methods
.method constructor <init>(Lkotlin/jvm/internal/Ref$IntRef;Lkotlin/jvm/internal/Ref$IntRef;Lkotlin/jvm/internal/Ref$IntRef;Ljava/util/List;Lkotlin/jvm/internal/Ref$BooleanRef;Landroid/content/res/Resources$Theme;Landroid/util/AttributeSet;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/SparseArray;Landroid/content/res/Resources;Landroid/graphics/drawable/StateListDrawable;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;->$styleId:Lkotlin/jvm/internal/Ref$IntRef;

    iput-object p2, p0, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;->$defaultStyleId:Lkotlin/jvm/internal/Ref$IntRef;

    iput-object p3, p0, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;->$vectorId:Lkotlin/jvm/internal/Ref$IntRef;

    iput-object p4, p0, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;->$states:Ljava/util/List;

    iput-object p5, p0, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;->$empty$inlined:Lkotlin/jvm/internal/Ref$BooleanRef;

    iput-object p6, p0, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;->$theme$inlined:Landroid/content/res/Resources$Theme;

    iput-object p7, p0, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;->$attrs$inlined:Landroid/util/AttributeSet;

    iput-object p8, p0, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;->$parser$inlined:Lorg/xmlpull/v1/XmlPullParser;

    iput-object p9, p0, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;->$stylesCache$inlined:Landroid/util/SparseArray;

    iput-object p10, p0, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;->$resources$inlined:Landroid/content/res/Resources;

    iput-object p11, p0, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;->$stateListDrawable$inlined:Landroid/graphics/drawable/StateListDrawable;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Lcom/squareup/android/xml/StyledAttributesVisitor;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;->invoke(Lcom/squareup/android/xml/StyledAttributesVisitor;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/android/xml/StyledAttributesVisitor;)V
    .locals 4

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-interface {p1}, Lcom/squareup/android/xml/StyledAttributesVisitor;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x31437f62

    const/4 v3, 0x0

    if-eq v1, v2, :cond_2

    const v2, -0x276ae810

    if-eq v1, v2, :cond_1

    const v2, 0x22079e2e

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string/jumbo v1, "vectorStyle"

    .line 109
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;->$styleId:Lkotlin/jvm/internal/Ref$IntRef;

    invoke-interface {p1}, Lcom/squareup/android/xml/StyledAttributesVisitor;->getStyled()Landroid/content/res/TypedArray;

    move-result-object p1

    invoke-static {}, Lcom/squareup/noho/StateListVectorDrawable;->access$getITEM_STYLE$cp()I

    move-result v1

    invoke-virtual {p1, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p1

    iput p1, v0, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    goto :goto_2

    :cond_1
    const-string v1, "defaultStyle"

    .line 110
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;->$defaultStyleId:Lkotlin/jvm/internal/Ref$IntRef;

    invoke-interface {p1}, Lcom/squareup/android/xml/StyledAttributesVisitor;->getStyled()Landroid/content/res/TypedArray;

    move-result-object p1

    invoke-static {}, Lcom/squareup/noho/StateListVectorDrawable;->access$getITEM_DEFAULT_STYLE$cp()I

    move-result v1

    invoke-virtual {p1, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p1

    iput p1, v0, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    goto :goto_2

    :cond_2
    const-string v1, "drawable"

    .line 111
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;->$vectorId:Lkotlin/jvm/internal/Ref$IntRef;

    invoke-interface {p1}, Lcom/squareup/android/xml/StyledAttributesVisitor;->getStyled()Landroid/content/res/TypedArray;

    move-result-object p1

    invoke-static {}, Lcom/squareup/noho/StateListVectorDrawable;->access$getITEM_DRAWABLE$cp()I

    move-result v1

    invoke-virtual {p1, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p1

    iput p1, v0, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    goto :goto_2

    .line 113
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;->$attrs$inlined:Landroid/util/AttributeSet;

    invoke-interface {p1}, Lcom/squareup/android/xml/StyledAttributesVisitor;->getIndex()I

    move-result v1

    invoke-interface {v0, v1, v3}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    move-result v0

    .line 116
    iget-object v1, p0, Lcom/squareup/noho/StateListVectorDrawable$Companion$inflate$$inlined$visitXml$lambda$1;->$states:Ljava/util/List;

    invoke-interface {p1}, Lcom/squareup/android/xml/StyledAttributesVisitor;->getNameResource()I

    move-result p1

    if-eqz v0, :cond_4

    goto :goto_1

    :cond_4
    neg-int p1, p1

    :goto_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    return-void
.end method
