.class public Lcom/squareup/noho/NohoRow;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "NohoRow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/NohoRow$Arrangeable;,
        Lcom/squareup/noho/NohoRow$ArrangeableProvider;,
        Lcom/squareup/noho/NohoRow$FeatureInstance;,
        Lcom/squareup/noho/NohoRow$FeatureProperty;,
        Lcom/squareup/noho/NohoRow$ValueFeature;,
        Lcom/squareup/noho/NohoRow$SubValueFeature;,
        Lcom/squareup/noho/NohoRow$DescriptionFeature;,
        Lcom/squareup/noho/NohoRow$Action;,
        Lcom/squareup/noho/NohoRow$ActionIconFeature;,
        Lcom/squareup/noho/NohoRow$ActionLinkFeature;,
        Lcom/squareup/noho/NohoRow$AccessoryFeature;,
        Lcom/squareup/noho/NohoRow$IconFeature;,
        Lcom/squareup/noho/NohoRow$Icon;,
        Lcom/squareup/noho/NohoRow$SimpleIconFeature;,
        Lcom/squareup/noho/NohoRow$TextIconFeature;,
        Lcom/squareup/noho/NohoRow$ExtraArrangeable;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoRow.kt\ncom/squareup/noho/NohoRow\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n+ 4 Delegates.kt\ncom/squareup/util/DelegatesKt\n+ 5 Delegates.kt\nkotlin/properties/Delegates\n+ 6 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,1059:1\n1642#2,2:1060\n1642#2,2:1062\n11416#3,2:1064\n11416#3,2:1066\n45#4:1068\n33#5,3:1069\n37#6,6:1072\n*E\n*S KotlinDebug\n*F\n+ 1 NohoRow.kt\ncom/squareup/noho/NohoRow\n*L\n351#1,2:1060\n436#1,2:1062\n1008#1,2:1064\n1011#1,2:1066\n156#1:1068\n156#1,3:1069\n263#1,6:1072\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\n\n\u0002\u0010\r\n\u0002\u0008\u000f\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0018\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0010\u0002\n\u0002\u0008\u001a\u0008\u0016\u0018\u00002\u00020\u0001:\u0018xyz{|}~\u007f\u0080\u0001\u0081\u0001\u0082\u0001\u0083\u0001\u0084\u0001\u0085\u0001\u0086\u0001\u0087\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010m\u001a\u00020nH\u0002J\u0008\u0010o\u001a\u00020nH\u0002J\u0018\u0010p\u001a\u00020n2\u0006\u0010q\u001a\u00020\u00072\u0006\u0010r\u001a\u00020\u0007H\u0014J(\u0010s\u001a\u00020n2\u0006\u0010t\u001a\u00020\u00072\u0006\u0010u\u001a\u00020\u00072\u0006\u0010v\u001a\u00020\u00072\u0006\u0010w\u001a\u00020\u0007H\u0016R+\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0010\u0010\u0011\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR(\u0010\u0014\u001a\u0004\u0018\u00010\u00132\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u00138F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0015\u0010\u0016\"\u0004\u0008\u0017\u0010\u0018R+\u0010\u001a\u001a\u00020\u00192\u0006\u0010\t\u001a\u00020\u00198F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u001f\u0010 \u001a\u0004\u0008\u001b\u0010\u001c\"\u0004\u0008\u001d\u0010\u001eR\u000e\u0010!\u001a\u00020\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010#R/\u0010%\u001a\u0004\u0018\u00010$2\u0008\u0010\t\u001a\u0004\u0018\u00010$8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008*\u0010+\u001a\u0004\u0008&\u0010\'\"\u0004\u0008(\u0010)R$\u0010,\u001a\u00020\u00072\u0006\u0010\u0012\u001a\u00020\u00078F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008-\u0010#\"\u0004\u0008.\u0010/R&\u00101\u001a\u00020\u00072\u0008\u0008\u0001\u00100\u001a\u00020\u00078F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u00082\u0010#\"\u0004\u00083\u0010/R:\u00106\u001a\u000c\u0012\u0006\u0008\u0001\u0012\u000205\u0018\u0001042\u0010\u0010\u0012\u001a\u000c\u0012\u0006\u0008\u0001\u0012\u000205\u0018\u000104@FX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010;\u001a\u0004\u00087\u00108\"\u0004\u00089\u0010:R(\u0010=\u001a\u0004\u0018\u00010<2\u0008\u0010\u0012\u001a\u0004\u0018\u00010<8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008>\u0010?\"\u0004\u0008@\u0010AR$\u0010B\u001a\u00020\u00072\u0006\u0010\u0012\u001a\u00020\u00078F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008C\u0010#\"\u0004\u0008D\u0010/R$\u0010E\u001a\u00020\u00072\u0006\u0010\u0012\u001a\u00020\u0007@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008F\u0010#\"\u0004\u0008G\u0010/R$\u0010H\u001a\u00020\u00072\u0006\u0010\u0012\u001a\u00020\u0007@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008I\u0010#\"\u0004\u0008J\u0010/R(\u0010K\u001a\u0004\u0018\u00010$2\u0008\u0010\u0012\u001a\u0004\u0018\u00010$8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008L\u0010\'\"\u0004\u0008M\u0010)R$\u0010N\u001a\u00020\u00072\u0006\u0010\u0012\u001a\u00020\u00078F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008O\u0010#\"\u0004\u0008P\u0010/R&\u0010Q\u001a\u00020\u00072\u0008\u0008\u0001\u00100\u001a\u00020\u00078F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008R\u0010#\"\u0004\u0008S\u0010/R\u000e\u0010T\u001a\u00020UX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\"\u0010V\u001a\u0016\u0012\u0012\u0012\u0010\u0012\u0008\u0008\u0001\u0012\u0004\u0018\u00010<\u0012\u0002\u0008\u00030X0WX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010Y\u001a\u0008\u0012\u0004\u0012\u00020[0ZX\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\\\u0010]R/\u0010^\u001a\u0004\u0018\u00010$2\u0008\u0010\t\u001a\u0004\u0018\u00010$8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008a\u0010b\u001a\u0004\u0008_\u0010\'\"\u0004\u0008`\u0010)R/\u0010\u0012\u001a\u0004\u0018\u00010$2\u0008\u0010\t\u001a\u0004\u0018\u00010$8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008e\u0010f\u001a\u0004\u0008c\u0010\'\"\u0004\u0008d\u0010)R$\u0010g\u001a\u00020\u00072\u0006\u0010\u0012\u001a\u00020\u00078F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008h\u0010#\"\u0004\u0008i\u0010/R&\u0010j\u001a\u00020\u00072\u0008\u0008\u0001\u00100\u001a\u00020\u00078F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008k\u0010#\"\u0004\u0008l\u0010/\u00a8\u0006\u0088\u0001"
    }
    d2 = {
        "Lcom/squareup/noho/NohoRow;",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "<set-?>",
        "Lcom/squareup/noho/AccessoryType;",
        "accessory",
        "getAccessory",
        "()Lcom/squareup/noho/AccessoryType;",
        "setAccessory",
        "(Lcom/squareup/noho/AccessoryType;)V",
        "accessory$delegate",
        "Lcom/squareup/noho/NohoRow$AccessoryFeature;",
        "value",
        "Lcom/squareup/noho/NohoRow$Action;",
        "action",
        "getAction",
        "()Lcom/squareup/noho/NohoRow$Action;",
        "setAction",
        "(Lcom/squareup/noho/NohoRow$Action;)V",
        "",
        "alignToLabel",
        "getAlignToLabel",
        "()Z",
        "setAlignToLabel",
        "(Z)V",
        "alignToLabel$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "constraintsUpdated",
        "getDefStyleAttr",
        "()I",
        "",
        "description",
        "getDescription",
        "()Ljava/lang/CharSequence;",
        "setDescription",
        "(Ljava/lang/CharSequence;)V",
        "description$delegate",
        "Lcom/squareup/noho/NohoRow$DescriptionFeature;",
        "descriptionAppearanceId",
        "getDescriptionAppearanceId",
        "setDescriptionAppearanceId",
        "(I)V",
        "id",
        "descriptionId",
        "getDescriptionId",
        "setDescriptionId",
        "",
        "Landroid/view/View;",
        "extras",
        "getExtras",
        "()[Landroid/view/View;",
        "setExtras",
        "([Landroid/view/View;)V",
        "[Landroid/view/View;",
        "Lcom/squareup/noho/NohoRow$Icon;",
        "icon",
        "getIcon",
        "()Lcom/squareup/noho/NohoRow$Icon;",
        "setIcon",
        "(Lcom/squareup/noho/NohoRow$Icon;)V",
        "iconStyleId",
        "getIconStyleId",
        "setIconStyleId",
        "internalPaddingBottom",
        "getInternalPaddingBottom",
        "setInternalPaddingBottom",
        "internalPaddingTop",
        "getInternalPaddingTop",
        "setInternalPaddingTop",
        "label",
        "getLabel",
        "setLabel",
        "labelAppearanceId",
        "getLabelAppearanceId",
        "setLabelAppearanceId",
        "labelId",
        "getLabelId",
        "setLabelId",
        "labelView",
        "Lcom/squareup/noho/NohoLabel;",
        "leftArrangeableProviders",
        "",
        "Lcom/squareup/noho/NohoRow$FeatureProperty;",
        "rightArrangeableProviders",
        "",
        "Lcom/squareup/noho/NohoRow$ArrangeableProvider;",
        "getRightArrangeableProviders",
        "()Ljava/util/List;",
        "subValue",
        "getSubValue",
        "setSubValue",
        "subValue$delegate",
        "Lcom/squareup/noho/NohoRow$SubValueFeature;",
        "getValue",
        "setValue",
        "value$delegate",
        "Lcom/squareup/noho/NohoRow$ValueFeature;",
        "valueAppearanceId",
        "getValueAppearanceId",
        "setValueAppearanceId",
        "valueId",
        "getValueId",
        "setValueId",
        "invalidateConstraints",
        "",
        "maybeUpdateConstraints",
        "onMeasure",
        "widthMeasureSpec",
        "heightMeasureSpec",
        "setPadding",
        "left",
        "top",
        "right",
        "bottom",
        "AccessoryFeature",
        "Action",
        "ActionIconFeature",
        "ActionLinkFeature",
        "Arrangeable",
        "ArrangeableProvider",
        "DescriptionFeature",
        "ExtraArrangeable",
        "FeatureInstance",
        "FeatureProperty",
        "Icon",
        "IconFeature",
        "SimpleIconFeature",
        "SubValueFeature",
        "TextIconFeature",
        "ValueFeature",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final accessory$delegate:Lcom/squareup/noho/NohoRow$AccessoryFeature;

.field private final alignToLabel$delegate:Lkotlin/properties/ReadWriteProperty;

.field private constraintsUpdated:Z

.field private final defStyleAttr:I

.field private final description$delegate:Lcom/squareup/noho/NohoRow$DescriptionFeature;

.field private extras:[Landroid/view/View;

.field private internalPaddingBottom:I

.field private internalPaddingTop:I

.field private final labelView:Lcom/squareup/noho/NohoLabel;

.field private final leftArrangeableProviders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/noho/NohoRow$FeatureProperty<",
            "+",
            "Lcom/squareup/noho/NohoRow$Icon;",
            "*>;>;"
        }
    .end annotation
.end field

.field private final rightArrangeableProviders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/noho/NohoRow$ArrangeableProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final subValue$delegate:Lcom/squareup/noho/NohoRow$SubValueFeature;

.field private final value$delegate:Lcom/squareup/noho/NohoRow$ValueFeature;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/squareup/noho/NohoRow;

    const/4 v1, 0x5

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "alignToLabel"

    const-string v5, "getAlignToLabel()Z"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string/jumbo v4, "value"

    const-string v5, "getValue()Ljava/lang/CharSequence;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "subValue"

    const-string v5, "getSubValue()Ljava/lang/CharSequence;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "description"

    const-string v5, "getDescription()Ljava/lang/CharSequence;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v3, "accessory"

    const-string v4, "getAccessory()Lcom/squareup/noho/AccessoryType;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aput-object v0, v1, v2

    sput-object v1, Lcom/squareup/noho/NohoRow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-direct {p0, p1, p2, p3}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput p3, p0, Lcom/squareup/noho/NohoRow;->defStyleAttr:I

    const/4 p3, 0x0

    .line 156
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1068
    sget-object v1, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    .line 1069
    new-instance v1, Lcom/squareup/noho/NohoRow$$special$$inlined$observable$1;

    invoke-direct {v1, v0, v0, p0}, Lcom/squareup/noho/NohoRow$$special$$inlined$observable$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/noho/NohoRow;)V

    check-cast v1, Lkotlin/properties/ReadWriteProperty;

    .line 1068
    iput-object v1, p0, Lcom/squareup/noho/NohoRow;->alignToLabel$delegate:Lkotlin/properties/ReadWriteProperty;

    const/4 v0, 0x3

    new-array v1, v0, [Lcom/squareup/noho/NohoRow$FeatureProperty;

    .line 158
    sget-object v2, Lcom/squareup/noho/NohoRow$IconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$IconFeature;

    check-cast v2, Lcom/squareup/noho/NohoRow$FeatureProperty;

    aput-object v2, v1, p3

    sget-object v2, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$SimpleIconFeature;

    check-cast v2, Lcom/squareup/noho/NohoRow$FeatureProperty;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/noho/NohoRow$TextIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$TextIconFeature;

    check-cast v2, Lcom/squareup/noho/NohoRow$FeatureProperty;

    const/4 v4, 0x2

    aput-object v2, v1, v4

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/noho/NohoRow;->leftArrangeableProviders:Ljava/util/List;

    new-array v0, v0, [Lcom/squareup/noho/NohoRow$ArrangeableProvider;

    .line 161
    sget-object v1, Lcom/squareup/noho/NohoRow$AccessoryFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$AccessoryFeature;

    check-cast v1, Lcom/squareup/noho/NohoRow$ArrangeableProvider;

    aput-object v1, v0, p3

    .line 162
    sget-object v1, Lcom/squareup/noho/NohoRow$ActionIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ActionIconFeature;

    check-cast v1, Lcom/squareup/noho/NohoRow$ArrangeableProvider;

    aput-object v1, v0, v3

    .line 163
    sget-object v1, Lcom/squareup/noho/NohoRow$ActionLinkFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ActionLinkFeature;

    check-cast v1, Lcom/squareup/noho/NohoRow$ArrangeableProvider;

    aput-object v1, v0, v4

    .line 160
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->mutableListOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/noho/NohoRow;->rightArrangeableProviders:Ljava/util/List;

    .line 176
    sget-object v0, Lcom/squareup/noho/NohoRow$ValueFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ValueFeature;

    iput-object v0, p0, Lcom/squareup/noho/NohoRow;->value$delegate:Lcom/squareup/noho/NohoRow$ValueFeature;

    .line 177
    sget-object v0, Lcom/squareup/noho/NohoRow$SubValueFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$SubValueFeature;

    iput-object v0, p0, Lcom/squareup/noho/NohoRow;->subValue$delegate:Lcom/squareup/noho/NohoRow$SubValueFeature;

    .line 182
    sget-object v0, Lcom/squareup/noho/NohoRow$DescriptionFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$DescriptionFeature;

    iput-object v0, p0, Lcom/squareup/noho/NohoRow;->description$delegate:Lcom/squareup/noho/NohoRow$DescriptionFeature;

    .line 187
    sget-object v0, Lcom/squareup/noho/NohoRow$AccessoryFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$AccessoryFeature;

    iput-object v0, p0, Lcom/squareup/noho/NohoRow;->accessory$delegate:Lcom/squareup/noho/NohoRow$AccessoryFeature;

    .line 243
    sget v0, Lcom/squareup/noho/R$layout;->noho_row:I

    move-object v1, p0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-static {p1, v0, v1}, Landroidx/constraintlayout/widget/ConstraintLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 245
    sget v0, Lcom/squareup/noho/R$id;->label:I

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.label)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    iput-object v0, p0, Lcom/squareup/noho/NohoRow;->labelView:Lcom/squareup/noho/NohoLabel;

    .line 249
    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->labelView:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoLabel;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    check-cast v0, Landroid/graphics/Paint;

    invoke-static {v0}, Lcom/squareup/marketfont/MarketUtils;->configureOptimalPaintFlags(Landroid/graphics/Paint;)V

    .line 252
    sget-object v0, Lcom/squareup/noho/NohoRow$ValueFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ValueFeature;

    invoke-virtual {v0, p0, p2}, Lcom/squareup/noho/NohoRow$ValueFeature;->initializeInstance(Lcom/squareup/noho/NohoRow;Landroid/util/AttributeSet;)V

    .line 253
    sget-object v0, Lcom/squareup/noho/NohoRow$SubValueFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$SubValueFeature;

    invoke-virtual {v0, p0, p2}, Lcom/squareup/noho/NohoRow$SubValueFeature;->initializeInstance(Lcom/squareup/noho/NohoRow;Landroid/util/AttributeSet;)V

    .line 254
    sget-object v0, Lcom/squareup/noho/NohoRow$AccessoryFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$AccessoryFeature;

    invoke-virtual {v0, p0, p2}, Lcom/squareup/noho/NohoRow$AccessoryFeature;->initializeInstance(Lcom/squareup/noho/NohoRow;Landroid/util/AttributeSet;)V

    .line 255
    sget-object v0, Lcom/squareup/noho/NohoRow$DescriptionFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$DescriptionFeature;

    invoke-virtual {v0, p0, p2}, Lcom/squareup/noho/NohoRow$DescriptionFeature;->initializeInstance(Lcom/squareup/noho/NohoRow;Landroid/util/AttributeSet;)V

    .line 256
    sget-object v0, Lcom/squareup/noho/NohoRow$IconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$IconFeature;

    invoke-virtual {v0, p0, p2}, Lcom/squareup/noho/NohoRow$IconFeature;->initializeInstance(Lcom/squareup/noho/NohoRow;Landroid/util/AttributeSet;)V

    .line 257
    sget-object v0, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$SimpleIconFeature;

    invoke-virtual {v0, p0, p2}, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->initializeInstance(Lcom/squareup/noho/NohoRow;Landroid/util/AttributeSet;)V

    .line 258
    sget-object v0, Lcom/squareup/noho/NohoRow$TextIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$TextIconFeature;

    invoke-virtual {v0, p0, p2}, Lcom/squareup/noho/NohoRow$TextIconFeature;->initializeInstance(Lcom/squareup/noho/NohoRow;Landroid/util/AttributeSet;)V

    .line 259
    sget-object v0, Lcom/squareup/noho/NohoRow$ActionIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ActionIconFeature;

    invoke-virtual {v0, p0, p2}, Lcom/squareup/noho/NohoRow$ActionIconFeature;->initializeInstance(Lcom/squareup/noho/NohoRow;Landroid/util/AttributeSet;)V

    .line 260
    sget-object v0, Lcom/squareup/noho/NohoRow$ActionLinkFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ActionLinkFeature;

    invoke-virtual {v0, p0, p2}, Lcom/squareup/noho/NohoRow$ActionLinkFeature;->initializeInstance(Lcom/squareup/noho/NohoRow;Landroid/util/AttributeSet;)V

    .line 263
    sget-object v0, Lcom/squareup/noho/R$styleable;->NohoRow:[I

    const-string v1, "R.styleable.NohoRow"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget v1, p0, Lcom/squareup/noho/NohoRow;->defStyleAttr:I

    sget v2, Lcom/squareup/noho/R$style;->Widget_Noho_Row:I

    .line 1072
    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    const-string p2, "a"

    .line 1074
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 265
    sget p2, Lcom/squareup/noho/R$styleable;->NohoRow_android_minHeight:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoRow;->setMinHeight(I)V

    .line 268
    sget p2, Lcom/squareup/noho/R$styleable;->NohoRow_android_padding:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result p2

    .line 270
    sget v0, Lcom/squareup/noho/R$styleable;->NohoRow_android_paddingStart:I

    invoke-virtual {p1, v0, p2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    .line 271
    sget v1, Lcom/squareup/noho/R$styleable;->NohoRow_android_paddingEnd:I

    invoke-virtual {p1, v1, p2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    .line 272
    sget v2, Lcom/squareup/noho/R$styleable;->NohoRow_android_paddingTop:I

    invoke-virtual {p1, v2, p2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    .line 274
    sget v4, Lcom/squareup/noho/R$styleable;->NohoRow_android_paddingBottom:I

    invoke-virtual {p1, v4, p2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result p2

    .line 275
    invoke-virtual {p0, v0, v2, v1, p2}, Lcom/squareup/noho/NohoRow;->setPadding(IIII)V

    .line 276
    sget p2, Lcom/squareup/noho/R$styleable;->NohoRow_sqAlignToLabel:I

    invoke-virtual {p0}, Lcom/squareup/noho/NohoRow;->getAlignToLabel()Z

    move-result v0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoRow;->setAlignToLabel(Z)V

    .line 279
    sget p2, Lcom/squareup/noho/R$styleable;->NohoRow_sqLabelAppearance:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    .line 280
    invoke-static {p0}, Lcom/squareup/noho/NohoRow;->access$getLabelView$p(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoLabel;

    move-result-object v0

    .line 281
    sget-object v1, Lcom/squareup/textappearance/SquareTextAppearance;->Companion:Lcom/squareup/textappearance/SquareTextAppearance$Companion;

    invoke-static {p0}, Lcom/squareup/noho/NohoRow;->access$getLabelView$p(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoLabel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/noho/NohoLabel;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v4, "labelView.context"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2, p2}, Lcom/squareup/textappearance/SquareTextAppearance$Companion;->loadFromStyle(Landroid/content/Context;I)Lcom/squareup/textappearance/SquareTextAppearance;

    move-result-object p2

    .line 280
    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoLabel;->apply(Lcom/squareup/textappearance/SquareTextAppearance;)V

    .line 284
    sget p2, Lcom/squareup/noho/R$styleable;->NohoRow_sqLabel:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 285
    sget p2, Lcom/squareup/noho/R$styleable;->NohoRow_sqValue:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 286
    sget p2, Lcom/squareup/noho/R$styleable;->NohoRow_sqSubValue:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoRow;->setSubValue(Ljava/lang/CharSequence;)V

    .line 287
    sget p2, Lcom/squareup/noho/R$styleable;->NohoRow_sqDescription:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 288
    sget p2, Lcom/squareup/noho/R$styleable;->NohoRow_sqAccessoryType:I

    invoke-static {}, Lcom/squareup/noho/AccessoryType;->values()[Lcom/squareup/noho/AccessoryType;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    sget-object v1, Lcom/squareup/noho/AccessoryType;->NONE:Lcom/squareup/noho/AccessoryType;

    check-cast v1, Ljava/lang/Enum;

    invoke-static {p1, p2, v0, v1}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;I[Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p2

    if-nez p2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast p2, Lcom/squareup/noho/AccessoryType;

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoRow;->setAccessory(Lcom/squareup/noho/AccessoryType;)V

    .line 290
    sget p2, Lcom/squareup/noho/R$styleable;->NohoRow_sqIconBackground:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v6

    .line 291
    sget p2, Lcom/squareup/noho/R$styleable;->NohoRow_sqIcon:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    if-eqz p2, :cond_2

    if-nez v6, :cond_1

    .line 294
    new-instance p3, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;

    invoke-direct {p3, p2}, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;-><init>(I)V

    check-cast p3, Lcom/squareup/noho/NohoRow$Icon;

    goto :goto_0

    .line 296
    :cond_1
    new-instance p3, Lcom/squareup/noho/NohoRow$Icon$BoxedIcon;

    invoke-direct {p3, p2, v6}, Lcom/squareup/noho/NohoRow$Icon$BoxedIcon;-><init>(II)V

    check-cast p3, Lcom/squareup/noho/NohoRow$Icon;

    .line 293
    :goto_0
    invoke-virtual {p0, p3}, Lcom/squareup/noho/NohoRow;->setIcon(Lcom/squareup/noho/NohoRow$Icon;)V

    .line 299
    :cond_2
    sget p2, Lcom/squareup/noho/R$styleable;->NohoRow_sqTextIcon:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_3

    goto :goto_1

    :cond_3
    const-string p2, ""

    :goto_1
    move-object v5, p2

    .line 300
    move-object p2, v5

    check-cast p2, Ljava/lang/CharSequence;

    invoke-static {p2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    xor-int/2addr p2, v3

    if-eqz p2, :cond_4

    .line 301
    new-instance p2, Lcom/squareup/noho/NohoRow$Icon$TextIcon;

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, p2

    invoke-direct/range {v4 .. v9}, Lcom/squareup/noho/NohoRow$Icon$TextIcon;-><init>(Ljava/lang/String;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p2, Lcom/squareup/noho/NohoRow$Icon;

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoRow;->setIcon(Lcom/squareup/noho/NohoRow$Icon;)V

    .line 303
    :cond_4
    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1076
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 102
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 103
    sget p3, Lcom/squareup/noho/R$attr;->sqRowStyle:I

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$getLabelView$p(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoLabel;
    .locals 0

    .line 99
    iget-object p0, p0, Lcom/squareup/noho/NohoRow;->labelView:Lcom/squareup/noho/NohoLabel;

    return-object p0
.end method

.method public static final synthetic access$invalidateConstraints(Lcom/squareup/noho/NohoRow;)V
    .locals 0

    .line 99
    invoke-direct {p0}, Lcom/squareup/noho/NohoRow;->invalidateConstraints()V

    return-void
.end method

.method private final invalidateConstraints()V
    .locals 1

    const/4 v0, 0x0

    .line 321
    iput-boolean v0, p0, Lcom/squareup/noho/NohoRow;->constraintsUpdated:Z

    .line 322
    invoke-virtual {p0}, Lcom/squareup/noho/NohoRow;->requestLayout()V

    return-void
.end method

.method private final maybeUpdateConstraints()V
    .locals 21

    move-object/from16 v0, p0

    .line 336
    iget-boolean v1, v0, Lcom/squareup/noho/NohoRow;->constraintsUpdated:Z

    if-eqz v1, :cond_0

    return-void

    .line 339
    :cond_0
    new-instance v1, Landroidx/constraintlayout/widget/ConstraintSet;

    invoke-direct {v1}, Landroidx/constraintlayout/widget/ConstraintSet;-><init>()V

    .line 340
    move-object v8, v0

    check-cast v8, Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {v1, v8}, Landroidx/constraintlayout/widget/ConstraintSet;->clone(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    .line 350
    iget-object v2, v0, Lcom/squareup/noho/NohoRow;->rightArrangeableProviders:Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 1060
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x7

    const/4 v14, 0x0

    :cond_1
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v7, 0x4

    const/4 v6, 0x3

    if-eqz v2, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/noho/NohoRow$ArrangeableProvider;

    .line 352
    invoke-interface {v2, v0}, Lcom/squareup/noho/NohoRow$ArrangeableProvider;->get(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$Arrangeable;

    move-result-object v16

    if-eqz v16, :cond_1

    .line 354
    invoke-interface/range {v16 .. v16}, Lcom/squareup/noho/NohoRow$Arrangeable;->getId()I

    move-result v3

    const/4 v4, 0x7

    move-object v2, v1

    move v5, v12

    const/4 v12, 0x3

    move v6, v13

    const/4 v13, 0x4

    move v7, v14

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 355
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/noho/NohoRow;->getAlignToLabel()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 356
    invoke-interface/range {v16 .. v16}, Lcom/squareup/noho/NohoRow$Arrangeable;->getId()I

    move-result v2

    iget-object v3, v0, Lcom/squareup/noho/NohoRow;->labelView:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v3}, Lcom/squareup/noho/NohoLabel;->getId()I

    move-result v3

    invoke-virtual {v1, v2, v12, v3, v12}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 357
    invoke-interface/range {v16 .. v16}, Lcom/squareup/noho/NohoRow$Arrangeable;->getId()I

    move-result v2

    iget-object v3, v0, Lcom/squareup/noho/NohoRow;->labelView:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v3}, Lcom/squareup/noho/NohoLabel;->getId()I

    move-result v3

    invoke-virtual {v1, v2, v13, v3, v13}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    goto :goto_1

    .line 360
    :cond_2
    invoke-interface/range {v16 .. v16}, Lcom/squareup/noho/NohoRow$Arrangeable;->getId()I

    move-result v2

    invoke-virtual {v1, v2, v12, v11, v12}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 361
    invoke-interface/range {v16 .. v16}, Lcom/squareup/noho/NohoRow$Arrangeable;->getId()I

    move-result v2

    invoke-virtual {v1, v2, v13, v11, v13}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 363
    :goto_1
    invoke-interface/range {v16 .. v16}, Lcom/squareup/noho/NohoRow$Arrangeable;->getId()I

    move-result v12

    .line 365
    invoke-interface/range {v16 .. v16}, Lcom/squareup/noho/NohoRow$Arrangeable;->getMargin()I

    move-result v14

    const/4 v13, 0x6

    goto :goto_0

    .line 370
    :cond_3
    sget v3, Lcom/squareup/noho/R$id;->label:I

    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v9, 0x3

    iget v2, v0, Lcom/squareup/noho/NohoRow;->internalPaddingTop:I

    move/from16 v16, v2

    move-object v2, v1

    const/4 v10, 0x3

    move v6, v9

    const/4 v9, 0x4

    move/from16 v7, v16

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 371
    sget-object v2, Lcom/squareup/noho/NohoRow$DescriptionFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$DescriptionFeature;

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoRow$DescriptionFeature;->instanceOrNull(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$FeatureInstance;

    move-result-object v16

    const/16 v7, 0x50

    const/16 v6, 0x10

    const/4 v5, 0x1

    if-eqz v16, :cond_5

    .line 372
    invoke-virtual/range {v16 .. v16}, Lcom/squareup/noho/NohoRow$FeatureInstance;->isEnabled()Z

    move-result v2

    if-eq v2, v5, :cond_4

    goto :goto_2

    .line 377
    :cond_4
    iget-object v2, v0, Lcom/squareup/noho/NohoRow;->labelView:Lcom/squareup/noho/NohoLabel;

    check-cast v2, Landroid/widget/TextView;

    invoke-static {v2, v7}, Lcom/squareup/noho/NohoRowKt;->setVerticalGravity(Landroid/widget/TextView;I)V

    .line 378
    sget v3, Lcom/squareup/noho/R$id;->label:I

    const/4 v4, 0x4

    sget v17, Lcom/squareup/noho/R$id;->description:I

    const/16 v18, 0x3

    const/16 v19, 0x0

    move-object v2, v1

    const/4 v15, 0x1

    move/from16 v5, v17

    const/16 v9, 0x10

    move/from16 v6, v18

    const/16 v10, 0x50

    move/from16 v7, v19

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 379
    sget v3, Lcom/squareup/noho/R$id;->description:I

    const/4 v4, 0x3

    sget v5, Lcom/squareup/noho/R$id;->label:I

    const/4 v6, 0x4

    invoke-virtual/range {v16 .. v16}, Lcom/squareup/noho/NohoRow$FeatureInstance;->getMargin()I

    move-result v7

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 380
    sget v3, Lcom/squareup/noho/R$id;->description:I

    const/4 v4, 0x4

    const/4 v5, 0x0

    iget v7, v0, Lcom/squareup/noho/NohoRow;->internalPaddingBottom:I

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    goto :goto_3

    :cond_5
    :goto_2
    const/16 v9, 0x10

    const/16 v10, 0x50

    const/4 v15, 0x1

    .line 374
    iget-object v2, v0, Lcom/squareup/noho/NohoRow;->labelView:Lcom/squareup/noho/NohoLabel;

    check-cast v2, Landroid/widget/TextView;

    invoke-static {v2, v9}, Lcom/squareup/noho/NohoRowKt;->setVerticalGravity(Landroid/widget/TextView;I)V

    .line 375
    sget v3, Lcom/squareup/noho/R$id;->label:I

    const/4 v4, 0x4

    const/4 v5, 0x0

    const/4 v6, 0x4

    iget v7, v0, Lcom/squareup/noho/NohoRow;->internalPaddingBottom:I

    move-object v2, v1

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 387
    :goto_3
    sget-object v2, Lcom/squareup/noho/NohoRow$ValueFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ValueFeature;

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoRow$ValueFeature;->instanceOrNull(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$FeatureInstance;

    move-result-object v16

    if-eqz v16, :cond_a

    .line 388
    invoke-virtual/range {v16 .. v16}, Lcom/squareup/noho/NohoRow$FeatureInstance;->isEnabled()Z

    move-result v2

    if-ne v2, v15, :cond_a

    .line 389
    sget v3, Lcom/squareup/noho/R$id;->value:I

    const/4 v4, 0x7

    move-object v2, v1

    move v5, v12

    move v6, v13

    move v7, v14

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 390
    invoke-virtual/range {v16 .. v16}, Lcom/squareup/noho/NohoRow$FeatureInstance;->getView()Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_6

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_6
    check-cast v2, Lcom/squareup/noho/NohoLabel;

    .line 391
    sget-object v3, Lcom/squareup/noho/NohoRow$SubValueFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$SubValueFeature;

    invoke-virtual {v3, v0}, Lcom/squareup/noho/NohoRow$SubValueFeature;->instanceOrNull(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$FeatureInstance;

    move-result-object v19

    const/4 v7, 0x5

    if-eqz v19, :cond_8

    .line 393
    invoke-virtual/range {v19 .. v19}, Lcom/squareup/noho/NohoRow$FeatureInstance;->isEnabled()Z

    move-result v3

    if-eq v3, v15, :cond_7

    goto :goto_4

    .line 406
    :cond_7
    check-cast v2, Landroid/widget/TextView;

    invoke-static {v2, v10}, Lcom/squareup/noho/NohoRowKt;->setVerticalGravity(Landroid/widget/TextView;I)V

    .line 409
    sget v3, Lcom/squareup/noho/R$id;->value:I

    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x3

    iget v9, v0, Lcom/squareup/noho/NohoRow;->internalPaddingTop:I

    move-object v2, v1

    const/4 v10, 0x5

    move v7, v9

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 410
    sget v3, Lcom/squareup/noho/R$id;->value:I

    const/4 v4, 0x4

    sget v5, Lcom/squareup/noho/R$id;->subValue:I

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 411
    sget v3, Lcom/squareup/noho/R$id;->subValue:I

    const/4 v4, 0x3

    sget v5, Lcom/squareup/noho/R$id;->value:I

    const/4 v6, 0x4

    invoke-virtual/range {v19 .. v19}, Lcom/squareup/noho/NohoRow$FeatureInstance;->getMargin()I

    move-result v7

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 412
    sget v3, Lcom/squareup/noho/R$id;->subValue:I

    const/4 v4, 0x4

    const/4 v5, 0x0

    iget v7, v0, Lcom/squareup/noho/NohoRow;->internalPaddingBottom:I

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 414
    sget v3, Lcom/squareup/noho/R$id;->subValue:I

    const/4 v4, 0x7

    move v5, v12

    move v6, v13

    move v7, v14

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 416
    sget v2, Lcom/squareup/noho/R$id;->valuesBarrier:I

    const/4 v3, 0x2

    new-array v3, v3, [I

    sget v4, Lcom/squareup/noho/R$id;->value:I

    aput v4, v3, v11

    sget v4, Lcom/squareup/noho/R$id;->subValue:I

    aput v4, v3, v15

    invoke-virtual {v1, v2, v10, v3}, Landroidx/constraintlayout/widget/ConstraintSet;->createBarrier(II[I)V

    goto :goto_6

    :cond_8
    :goto_4
    const/4 v10, 0x5

    .line 394
    check-cast v2, Landroid/widget/TextView;

    invoke-static {v2, v9}, Lcom/squareup/noho/NohoRowKt;->setVerticalGravity(Landroid/widget/TextView;I)V

    .line 395
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/noho/NohoRow;->getAlignToLabel()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 396
    sget v3, Lcom/squareup/noho/R$id;->value:I

    const/4 v4, 0x3

    iget-object v2, v0, Lcom/squareup/noho/NohoRow;->labelView:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoLabel;->getId()I

    move-result v5

    const/4 v6, 0x3

    const/4 v7, 0x0

    move-object v2, v1

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 397
    sget v3, Lcom/squareup/noho/R$id;->value:I

    const/4 v4, 0x4

    iget-object v2, v0, Lcom/squareup/noho/NohoRow;->labelView:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoLabel;->getId()I

    move-result v5

    const/4 v6, 0x4

    move-object v2, v1

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    goto :goto_5

    .line 400
    :cond_9
    sget v3, Lcom/squareup/noho/R$id;->value:I

    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x3

    iget v7, v0, Lcom/squareup/noho/NohoRow;->internalPaddingTop:I

    move-object v2, v1

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 401
    sget v3, Lcom/squareup/noho/R$id;->value:I

    const/4 v4, 0x4

    const/4 v6, 0x4

    iget v7, v0, Lcom/squareup/noho/NohoRow;->internalPaddingBottom:I

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 404
    :goto_5
    sget v2, Lcom/squareup/noho/R$id;->valuesBarrier:I

    new-array v3, v15, [I

    sget v4, Lcom/squareup/noho/R$id;->value:I

    aput v4, v3, v11

    invoke-virtual {v1, v2, v10, v3}, Landroidx/constraintlayout/widget/ConstraintSet;->createBarrier(II[I)V

    .line 421
    :goto_6
    sget v2, Lcom/squareup/noho/R$id;->valuesBarrier:I

    .line 423
    invoke-virtual/range {v16 .. v16}, Lcom/squareup/noho/NohoRow$FeatureInstance;->getMargin()I

    move-result v3

    move v12, v2

    move v14, v3

    const/4 v13, 0x6

    .line 427
    :cond_a
    sget v3, Lcom/squareup/noho/R$id;->label:I

    const/4 v4, 0x7

    move-object v2, v1

    move v5, v12

    move v6, v13

    move v7, v14

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 428
    sget v3, Lcom/squareup/noho/R$id;->description:I

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 435
    iget-object v2, v0, Lcom/squareup/noho/NohoRow;->leftArrangeableProviders:Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 1062
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/16 v20, 0x6

    :goto_7
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/noho/NohoRow$FeatureProperty;

    .line 437
    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoRow$FeatureProperty;->get(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$Arrangeable;

    move-result-object v13

    if-eqz v13, :cond_b

    .line 439
    invoke-interface {v13}, Lcom/squareup/noho/NohoRow$Arrangeable;->getId()I

    move-result v3

    const/4 v4, 0x6

    move-object v2, v1

    move v5, v10

    move/from16 v6, v20

    move v7, v12

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 440
    invoke-interface {v13}, Lcom/squareup/noho/NohoRow$Arrangeable;->getId()I

    move-result v2

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3, v11, v3}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 441
    invoke-interface {v13}, Lcom/squareup/noho/NohoRow$Arrangeable;->getId()I

    move-result v2

    const/4 v4, 0x4

    invoke-virtual {v1, v2, v4, v11, v4}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    .line 442
    invoke-interface {v13}, Lcom/squareup/noho/NohoRow$Arrangeable;->getId()I

    move-result v10

    .line 444
    invoke-interface {v13}, Lcom/squareup/noho/NohoRow$Arrangeable;->getMargin()I

    move-result v12

    const/16 v20, 0x7

    goto :goto_7

    :cond_b
    const/4 v3, 0x3

    const/4 v4, 0x4

    goto :goto_7

    .line 449
    :cond_c
    sget v3, Lcom/squareup/noho/R$id;->label:I

    const/4 v4, 0x6

    move-object v2, v1

    move v5, v10

    move/from16 v6, v20

    move v7, v12

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 450
    sget v3, Lcom/squareup/noho/R$id;->description:I

    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 452
    invoke-virtual {v1, v8}, Landroidx/constraintlayout/widget/ConstraintSet;->applyTo(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    .line 453
    iput-boolean v15, v0, Lcom/squareup/noho/NohoRow;->constraintsUpdated:Z

    return-void
.end method


# virtual methods
.method public final getAccessory()Lcom/squareup/noho/AccessoryType;
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->accessory$delegate:Lcom/squareup/noho/NohoRow$AccessoryFeature;

    sget-object v1, Lcom/squareup/noho/NohoRow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/squareup/noho/NohoRow$AccessoryFeature;->getValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/AccessoryType;

    return-object v0
.end method

.method public final getAction()Lcom/squareup/noho/NohoRow$Action;
    .locals 3

    .line 222
    sget-object v0, Lcom/squareup/noho/NohoRow$ActionIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ActionIconFeature;

    new-instance v1, Lcom/squareup/noho/NohoRow$action$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/noho/NohoRow;

    invoke-direct {v1, v2}, Lcom/squareup/noho/NohoRow$action$1;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v1, Lkotlin/reflect/KProperty;

    invoke-virtual {v0, p0, v1}, Lcom/squareup/noho/NohoRow$ActionIconFeature;->getValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow$Action$ActionIcon;

    if-eqz v0, :cond_0

    check-cast v0, Lcom/squareup/noho/NohoRow$Action;

    goto :goto_0

    .line 223
    :cond_0
    sget-object v0, Lcom/squareup/noho/NohoRow$ActionLinkFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ActionLinkFeature;

    new-instance v1, Lcom/squareup/noho/NohoRow$action$2;

    invoke-direct {v1, v2}, Lcom/squareup/noho/NohoRow$action$2;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v1, Lkotlin/reflect/KProperty;

    invoke-virtual {v0, p0, v1}, Lcom/squareup/noho/NohoRow$ActionLinkFeature;->getValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow$Action;

    :goto_0
    return-object v0
.end method

.method public final getAlignToLabel()Z
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->alignToLabel$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoRow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final getDefStyleAttr()I
    .locals 1

    .line 103
    iget v0, p0, Lcom/squareup/noho/NohoRow;->defStyleAttr:I

    return v0
.end method

.method public final getDescription()Ljava/lang/CharSequence;
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->description$delegate:Lcom/squareup/noho/NohoRow$DescriptionFeature;

    sget-object v1, Lcom/squareup/noho/NohoRow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/squareup/noho/NohoRow$DescriptionFeature;->getValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getDescriptionAppearanceId()I
    .locals 1

    .line 140
    sget-object v0, Lcom/squareup/noho/NohoRow$DescriptionFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$DescriptionFeature;

    invoke-virtual {v0, p0}, Lcom/squareup/noho/NohoRow$DescriptionFeature;->getStyleId(Lcom/squareup/noho/NohoRow;)I

    move-result v0

    return v0
.end method

.method public final getDescriptionId()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 185
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot get a description id"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final getExtras()[Landroid/view/View;
    .locals 1

    .line 1006
    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->extras:[Landroid/view/View;

    return-object v0
.end method

.method public final getIcon()Lcom/squareup/noho/NohoRow$Icon;
    .locals 3

    .line 193
    sget-object v0, Lcom/squareup/noho/NohoRow$IconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$IconFeature;

    new-instance v1, Lcom/squareup/noho/NohoRow$icon$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/noho/NohoRow;

    invoke-direct {v1, v2}, Lcom/squareup/noho/NohoRow$icon$1;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v1, Lkotlin/reflect/KProperty;

    invoke-virtual {v0, p0, v1}, Lcom/squareup/noho/NohoRow$IconFeature;->getValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow$Icon;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 194
    :cond_0
    sget-object v0, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$SimpleIconFeature;

    new-instance v1, Lcom/squareup/noho/NohoRow$icon$2;

    invoke-direct {v1, v2}, Lcom/squareup/noho/NohoRow$icon$2;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v1, Lkotlin/reflect/KProperty;

    invoke-virtual {v0, p0, v1}, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->getValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow$Icon;

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    .line 195
    :cond_1
    sget-object v0, Lcom/squareup/noho/NohoRow$TextIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$TextIconFeature;

    new-instance v1, Lcom/squareup/noho/NohoRow$icon$3;

    invoke-direct {v1, v2}, Lcom/squareup/noho/NohoRow$icon$3;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v1, Lkotlin/reflect/KProperty;

    invoke-virtual {v0, p0, v1}, Lcom/squareup/noho/NohoRow$TextIconFeature;->getValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow$Icon;

    :goto_1
    return-object v0
.end method

.method public final getIconStyleId()I
    .locals 1

    .line 144
    sget-object v0, Lcom/squareup/noho/NohoRow$IconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$IconFeature;

    invoke-virtual {v0, p0}, Lcom/squareup/noho/NohoRow$IconFeature;->getStyleId(Lcom/squareup/noho/NohoRow;)I

    move-result v0

    return v0
.end method

.method public final getInternalPaddingBottom()I
    .locals 1

    .line 113
    iget v0, p0, Lcom/squareup/noho/NohoRow;->internalPaddingBottom:I

    return v0
.end method

.method public final getInternalPaddingTop()I
    .locals 1

    .line 108
    iget v0, p0, Lcom/squareup/noho/NohoRow;->internalPaddingTop:I

    return v0
.end method

.method public final getLabel()Ljava/lang/CharSequence;
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->labelView:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoLabel;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/noho/NohoRowKt;->access$nullIfBlank(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final getLabelAppearanceId()I
    .locals 2

    .line 127
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot get a labelAppearanceId"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final getLabelId()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 174
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot get a label id"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method protected final getRightArrangeableProviders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/noho/NohoRow$ArrangeableProvider;",
            ">;"
        }
    .end annotation

    .line 160
    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->rightArrangeableProviders:Ljava/util/List;

    return-object v0
.end method

.method public final getSubValue()Ljava/lang/CharSequence;
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->subValue$delegate:Lcom/squareup/noho/NohoRow$SubValueFeature;

    sget-object v1, Lcom/squareup/noho/NohoRow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/squareup/noho/NohoRow$SubValueFeature;->getValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getValue()Ljava/lang/CharSequence;
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->value$delegate:Lcom/squareup/noho/NohoRow$ValueFeature;

    sget-object v1, Lcom/squareup/noho/NohoRow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/squareup/noho/NohoRow$ValueFeature;->getValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getValueAppearanceId()I
    .locals 1

    .line 135
    sget-object v0, Lcom/squareup/noho/NohoRow$ValueFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ValueFeature;

    invoke-virtual {v0, p0}, Lcom/squareup/noho/NohoRow$ValueFeature;->getStyleId(Lcom/squareup/noho/NohoRow;)I

    move-result v0

    return v0
.end method

.method public final getValueId()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 180
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot get a value id"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method protected onMeasure(II)V
    .locals 0

    .line 326
    invoke-direct {p0}, Lcom/squareup/noho/NohoRow;->maybeUpdateConstraints()V

    .line 327
    invoke-super {p0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout;->onMeasure(II)V

    return-void
.end method

.method public final setAccessory(Lcom/squareup/noho/AccessoryType;)V
    .locals 3

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->accessory$delegate:Lcom/squareup/noho/NohoRow$AccessoryFeature;

    sget-object v1, Lcom/squareup/noho/NohoRow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1, p1}, Lcom/squareup/noho/NohoRow$AccessoryFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setAction(Lcom/squareup/noho/NohoRow$Action;)V
    .locals 4

    .line 227
    instance-of v0, p1, Lcom/squareup/noho/NohoRow$Action$ActionIcon;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 228
    sget-object v0, Lcom/squareup/noho/NohoRow$ActionIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ActionIconFeature;

    new-instance v2, Lcom/squareup/noho/NohoRow$action$3;

    move-object v3, p0

    check-cast v3, Lcom/squareup/noho/NohoRow;

    invoke-direct {v2, v3}, Lcom/squareup/noho/NohoRow$action$3;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v2, Lkotlin/reflect/KProperty;

    invoke-virtual {v0, p0, v2, p1}, Lcom/squareup/noho/NohoRow$ActionIconFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    .line 229
    sget-object p1, Lcom/squareup/noho/NohoRow$ActionLinkFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ActionLinkFeature;

    new-instance v0, Lcom/squareup/noho/NohoRow$action$4;

    invoke-direct {v0, v3}, Lcom/squareup/noho/NohoRow$action$4;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v0, Lkotlin/reflect/KProperty;

    invoke-virtual {p1, p0, v0, v1}, Lcom/squareup/noho/NohoRow$ActionLinkFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    goto :goto_0

    .line 231
    :cond_0
    instance-of v0, p1, Lcom/squareup/noho/NohoRow$Action$ActionLink;

    if-eqz v0, :cond_1

    .line 232
    sget-object v0, Lcom/squareup/noho/NohoRow$ActionIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ActionIconFeature;

    new-instance v2, Lcom/squareup/noho/NohoRow$action$5;

    move-object v3, p0

    check-cast v3, Lcom/squareup/noho/NohoRow;

    invoke-direct {v2, v3}, Lcom/squareup/noho/NohoRow$action$5;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v2, Lkotlin/reflect/KProperty;

    invoke-virtual {v0, p0, v2, v1}, Lcom/squareup/noho/NohoRow$ActionIconFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    .line 233
    sget-object v0, Lcom/squareup/noho/NohoRow$ActionLinkFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ActionLinkFeature;

    new-instance v1, Lcom/squareup/noho/NohoRow$action$6;

    invoke-direct {v1, v3}, Lcom/squareup/noho/NohoRow$action$6;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v1, Lkotlin/reflect/KProperty;

    invoke-virtual {v0, p0, v1, p1}, Lcom/squareup/noho/NohoRow$ActionLinkFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    goto :goto_0

    .line 236
    :cond_1
    sget-object p1, Lcom/squareup/noho/NohoRow$ActionIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ActionIconFeature;

    new-instance v0, Lcom/squareup/noho/NohoRow$action$7;

    move-object v2, p0

    check-cast v2, Lcom/squareup/noho/NohoRow;

    invoke-direct {v0, v2}, Lcom/squareup/noho/NohoRow$action$7;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v0, Lkotlin/reflect/KProperty;

    invoke-virtual {p1, p0, v0, v1}, Lcom/squareup/noho/NohoRow$ActionIconFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    .line 237
    sget-object p1, Lcom/squareup/noho/NohoRow$ActionLinkFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ActionLinkFeature;

    new-instance v0, Lcom/squareup/noho/NohoRow$action$8;

    invoke-direct {v0, v2}, Lcom/squareup/noho/NohoRow$action$8;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v0, Lkotlin/reflect/KProperty;

    invoke-virtual {p1, p0, v0, v1}, Lcom/squareup/noho/NohoRow$ActionLinkFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public final setAlignToLabel(Z)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->alignToLabel$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoRow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setDescription(Ljava/lang/CharSequence;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->description$delegate:Lcom/squareup/noho/NohoRow$DescriptionFeature;

    sget-object v1, Lcom/squareup/noho/NohoRow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1, p1}, Lcom/squareup/noho/NohoRow$DescriptionFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setDescriptionAppearanceId(I)V
    .locals 1

    .line 141
    sget-object v0, Lcom/squareup/noho/NohoRow$DescriptionFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$DescriptionFeature;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/noho/NohoRow$DescriptionFeature;->setStyleId(Lcom/squareup/noho/NohoRow;I)V

    return-void
.end method

.method public final setDescriptionId(I)V
    .locals 1

    .line 186
    invoke-virtual {p0}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setExtras([Landroid/view/View;)V
    .locals 6

    .line 1008
    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->extras:[Landroid/view/View;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 1064
    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v0, v3

    .line 1008
    invoke-virtual {p0, v4}, Lcom/squareup/noho/NohoRow;->removeView(Landroid/view/View;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1009
    :cond_0
    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->rightArrangeableProviders:Ljava/util/List;

    sget-object v2, Lcom/squareup/noho/NohoRow$extras$2;->INSTANCE:Lcom/squareup/noho/NohoRow$extras$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->removeAll(Ljava/util/List;Lkotlin/jvm/functions/Function1;)Z

    .line 1010
    iput-object p1, p0, Lcom/squareup/noho/NohoRow;->extras:[Landroid/view/View;

    .line 1011
    iget-object p1, p0, Lcom/squareup/noho/NohoRow;->extras:[Landroid/view/View;

    if-eqz p1, :cond_2

    .line 1014
    invoke-virtual {p0}, Lcom/squareup/noho/NohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/squareup/noho/R$dimen;->noho_gap_8:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 1066
    array-length v2, p1

    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v3, p1, v1

    .line 1016
    invoke-virtual {p0, v3}, Lcom/squareup/noho/NohoRow;->addView(Landroid/view/View;)V

    .line 1017
    iget-object v4, p0, Lcom/squareup/noho/NohoRow;->rightArrangeableProviders:Ljava/util/List;

    new-instance v5, Lcom/squareup/noho/NohoRow$ExtraArrangeable;

    invoke-direct {v5, v3, v0}, Lcom/squareup/noho/NohoRow$ExtraArrangeable;-><init>(Landroid/view/View;I)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1019
    :cond_1
    invoke-direct {p0}, Lcom/squareup/noho/NohoRow;->invalidateConstraints()V

    :cond_2
    return-void
.end method

.method public final setIcon(Lcom/squareup/noho/NohoRow$Icon;)V
    .locals 4

    .line 201
    instance-of v0, p1, Lcom/squareup/noho/NohoRow$Icon$TextIcon;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 202
    sget-object v0, Lcom/squareup/noho/NohoRow$IconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$IconFeature;

    new-instance v2, Lcom/squareup/noho/NohoRow$icon$4;

    move-object v3, p0

    check-cast v3, Lcom/squareup/noho/NohoRow;

    invoke-direct {v2, v3}, Lcom/squareup/noho/NohoRow$icon$4;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v2, Lkotlin/reflect/KProperty;

    invoke-virtual {v0, p0, v2, v1}, Lcom/squareup/noho/NohoRow$IconFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    .line 203
    sget-object v0, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$SimpleIconFeature;

    new-instance v2, Lcom/squareup/noho/NohoRow$icon$5;

    invoke-direct {v2, v3}, Lcom/squareup/noho/NohoRow$icon$5;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v2, Lkotlin/reflect/KProperty;

    invoke-virtual {v0, p0, v2, v1}, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    .line 204
    sget-object v0, Lcom/squareup/noho/NohoRow$TextIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$TextIconFeature;

    new-instance v1, Lcom/squareup/noho/NohoRow$icon$6;

    invoke-direct {v1, v3}, Lcom/squareup/noho/NohoRow$icon$6;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v1, Lkotlin/reflect/KProperty;

    invoke-virtual {v0, p0, v1, p1}, Lcom/squareup/noho/NohoRow$TextIconFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    goto :goto_0

    .line 206
    :cond_0
    instance-of v0, p1, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;

    if-eqz v0, :cond_1

    .line 207
    sget-object v0, Lcom/squareup/noho/NohoRow$TextIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$TextIconFeature;

    new-instance v2, Lcom/squareup/noho/NohoRow$icon$7;

    move-object v3, p0

    check-cast v3, Lcom/squareup/noho/NohoRow;

    invoke-direct {v2, v3}, Lcom/squareup/noho/NohoRow$icon$7;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v2, Lkotlin/reflect/KProperty;

    invoke-virtual {v0, p0, v2, v1}, Lcom/squareup/noho/NohoRow$TextIconFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    .line 208
    sget-object v0, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$SimpleIconFeature;

    new-instance v2, Lcom/squareup/noho/NohoRow$icon$8;

    invoke-direct {v2, v3}, Lcom/squareup/noho/NohoRow$icon$8;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v2, Lkotlin/reflect/KProperty;

    invoke-virtual {v0, p0, v2, p1}, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    .line 209
    sget-object p1, Lcom/squareup/noho/NohoRow$IconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$IconFeature;

    new-instance v0, Lcom/squareup/noho/NohoRow$icon$9;

    invoke-direct {v0, v3}, Lcom/squareup/noho/NohoRow$icon$9;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v0, Lkotlin/reflect/KProperty;

    invoke-virtual {p1, p0, v0, v1}, Lcom/squareup/noho/NohoRow$IconFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    goto :goto_0

    .line 213
    :cond_1
    sget-object v0, Lcom/squareup/noho/NohoRow$TextIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$TextIconFeature;

    new-instance v2, Lcom/squareup/noho/NohoRow$icon$10;

    move-object v3, p0

    check-cast v3, Lcom/squareup/noho/NohoRow;

    invoke-direct {v2, v3}, Lcom/squareup/noho/NohoRow$icon$10;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v2, Lkotlin/reflect/KProperty;

    invoke-virtual {v0, p0, v2, v1}, Lcom/squareup/noho/NohoRow$TextIconFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    .line 214
    sget-object v0, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$SimpleIconFeature;

    new-instance v2, Lcom/squareup/noho/NohoRow$icon$11;

    invoke-direct {v2, v3}, Lcom/squareup/noho/NohoRow$icon$11;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v2, Lkotlin/reflect/KProperty;

    invoke-virtual {v0, p0, v2, v1}, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    .line 215
    sget-object v0, Lcom/squareup/noho/NohoRow$IconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$IconFeature;

    new-instance v1, Lcom/squareup/noho/NohoRow$icon$12;

    invoke-direct {v1, v3}, Lcom/squareup/noho/NohoRow$icon$12;-><init>(Lcom/squareup/noho/NohoRow;)V

    check-cast v1, Lkotlin/reflect/KProperty;

    invoke-virtual {v0, p0, v1, p1}, Lcom/squareup/noho/NohoRow$IconFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public final setIconStyleId(I)V
    .locals 1

    .line 146
    sget-object v0, Lcom/squareup/noho/NohoRow$IconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$IconFeature;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/noho/NohoRow$IconFeature;->setStyleId(Lcom/squareup/noho/NohoRow;I)V

    .line 147
    sget-object v0, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$SimpleIconFeature;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/noho/NohoRow$SimpleIconFeature;->setStyleId(Lcom/squareup/noho/NohoRow;I)V

    .line 148
    sget-object v0, Lcom/squareup/noho/NohoRow$TextIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$TextIconFeature;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/noho/NohoRow$TextIconFeature;->setStyleId(Lcom/squareup/noho/NohoRow;I)V

    return-void
.end method

.method public final setInternalPaddingBottom(I)V
    .locals 0

    .line 115
    iput p1, p0, Lcom/squareup/noho/NohoRow;->internalPaddingBottom:I

    .line 116
    invoke-direct {p0}, Lcom/squareup/noho/NohoRow;->invalidateConstraints()V

    return-void
.end method

.method public final setInternalPaddingTop(I)V
    .locals 0

    .line 110
    iput p1, p0, Lcom/squareup/noho/NohoRow;->internalPaddingTop:I

    .line 111
    invoke-direct {p0}, Lcom/squareup/noho/NohoRow;->invalidateConstraints()V

    return-void
.end method

.method public final setLabel(Ljava/lang/CharSequence;)V
    .locals 1

    .line 171
    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->labelView:Lcom/squareup/noho/NohoLabel;

    if-eqz p1, :cond_0

    invoke-static {p1}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setLabelAppearanceId(I)V
    .locals 4

    .line 129
    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->labelView:Lcom/squareup/noho/NohoLabel;

    .line 130
    sget-object v1, Lcom/squareup/textappearance/SquareTextAppearance;->Companion:Lcom/squareup/textappearance/SquareTextAppearance$Companion;

    iget-object v2, p0, Lcom/squareup/noho/NohoRow;->labelView:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoLabel;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "labelView.context"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2, p1}, Lcom/squareup/textappearance/SquareTextAppearance$Companion;->loadFromStyle(Landroid/content/Context;I)Lcom/squareup/textappearance/SquareTextAppearance;

    move-result-object p1

    .line 129
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->apply(Lcom/squareup/textappearance/SquareTextAppearance;)V

    return-void
.end method

.method public final setLabelId(I)V
    .locals 1

    .line 175
    invoke-virtual {p0}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setPadding(IIII)V
    .locals 1

    const/4 v0, 0x0

    .line 314
    invoke-super {p0, p1, v0, p3, v0}, Landroidx/constraintlayout/widget/ConstraintLayout;->setPadding(IIII)V

    .line 315
    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoRow;->setInternalPaddingTop(I)V

    .line 316
    invoke-virtual {p0, p4}, Lcom/squareup/noho/NohoRow;->setInternalPaddingBottom(I)V

    .line 317
    invoke-direct {p0}, Lcom/squareup/noho/NohoRow;->invalidateConstraints()V

    return-void
.end method

.method public final setSubValue(Ljava/lang/CharSequence;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->subValue$delegate:Lcom/squareup/noho/NohoRow$SubValueFeature;

    sget-object v1, Lcom/squareup/noho/NohoRow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1, p1}, Lcom/squareup/noho/NohoRow$SubValueFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setValue(Ljava/lang/CharSequence;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoRow;->value$delegate:Lcom/squareup/noho/NohoRow$ValueFeature;

    sget-object v1, Lcom/squareup/noho/NohoRow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1, p1}, Lcom/squareup/noho/NohoRow$ValueFeature;->setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setValueAppearanceId(I)V
    .locals 1

    .line 136
    sget-object v0, Lcom/squareup/noho/NohoRow$ValueFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$ValueFeature;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/noho/NohoRow$ValueFeature;->setStyleId(Lcom/squareup/noho/NohoRow;I)V

    return-void
.end method

.method public final setValueId(I)V
    .locals 1

    .line 181
    invoke-virtual {p0}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method
