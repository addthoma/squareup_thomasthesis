.class Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;
.super Ljava/lang/Object;
.source "NohoCheckableGroup.java"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoCheckableGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PassThroughHierarchyChangeListener"
.end annotation


# instance fields
.field private onHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

.field final synthetic this$0:Lcom/squareup/noho/NohoCheckableGroup;


# direct methods
.method private constructor <init>(Lcom/squareup/noho/NohoCheckableGroup;)V
    .locals 0

    .line 575
    iput-object p1, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/noho/NohoCheckableGroup;Lcom/squareup/noho/NohoCheckableGroup$1;)V
    .locals 0

    .line 575
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;-><init>(Lcom/squareup/noho/NohoCheckableGroup;)V

    return-void
.end method

.method static synthetic access$202(Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;Landroid/view/ViewGroup$OnHierarchyChangeListener;)Landroid/view/ViewGroup$OnHierarchyChangeListener;
    .locals 0

    .line 575
    iput-object p1, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->onHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    return-object p1
.end method


# virtual methods
.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .line 581
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    if-eq p1, v0, :cond_0

    .line 582
    invoke-static {v0}, Lcom/squareup/noho/NohoCheckableGroup;->access$900(Lcom/squareup/noho/NohoCheckableGroup;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    .line 583
    invoke-static {v0}, Lcom/squareup/noho/NohoCheckableGroup;->access$1000(Lcom/squareup/noho/NohoCheckableGroup;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-ne p1, v0, :cond_5

    :cond_0
    instance-of v0, p2, Landroid/widget/Checkable;

    if-eqz v0, :cond_5

    .line 584
    move-object v0, p2

    check-cast v0, Landroid/widget/Checkable;

    .line 585
    invoke-interface {v0}, Landroid/widget/Checkable;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 586
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/noho/NohoCheckableGroup;->access$302(Lcom/squareup/noho/NohoCheckableGroup;Z)Z

    .line 588
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {v0}, Lcom/squareup/noho/NohoCheckableGroup;->access$400(Lcom/squareup/noho/NohoCheckableGroup;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {v0}, Lcom/squareup/noho/NohoCheckableGroup;->access$500(Lcom/squareup/noho/NohoCheckableGroup;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 589
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {v0}, Lcom/squareup/noho/NohoCheckableGroup;->access$400(Lcom/squareup/noho/NohoCheckableGroup;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 590
    iget-object v2, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {v2, v0, v1}, Lcom/squareup/noho/NohoCheckableGroup;->access$600(Lcom/squareup/noho/NohoCheckableGroup;IZ)Z

    .line 593
    :cond_1
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {v0, v1}, Lcom/squareup/noho/NohoCheckableGroup;->access$302(Lcom/squareup/noho/NohoCheckableGroup;Z)Z

    .line 594
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/noho/NohoCheckableGroup;->access$700(Lcom/squareup/noho/NohoCheckableGroup;I)V

    .line 597
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    .line 601
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v0

    .line 602
    invoke-virtual {p2, v0}, Landroid/view/View;->setId(I)V

    .line 605
    :cond_3
    instance-of v0, p2, Landroid/widget/CompoundButton;

    if-eqz v0, :cond_4

    .line 606
    move-object v0, p2

    check-cast v0, Landroid/widget/CompoundButton;

    iget-object v1, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {v1}, Lcom/squareup/noho/NohoCheckableGroup;->access$1100(Lcom/squareup/noho/NohoCheckableGroup;)Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    .line 609
    :cond_4
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {v0}, Lcom/squareup/noho/NohoCheckableGroup;->access$1100(Lcom/squareup/noho/NohoCheckableGroup;)Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 613
    :cond_5
    :goto_0
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->onHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    if-eqz v0, :cond_6

    .line 614
    invoke-interface {v0, p1, p2}, Landroid/view/ViewGroup$OnHierarchyChangeListener;->onChildViewAdded(Landroid/view/View;Landroid/view/View;)V

    :cond_6
    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .line 620
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    if-eq p1, v0, :cond_0

    .line 621
    invoke-static {v0}, Lcom/squareup/noho/NohoCheckableGroup;->access$900(Lcom/squareup/noho/NohoCheckableGroup;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    .line 622
    invoke-static {v0}, Lcom/squareup/noho/NohoCheckableGroup;->access$1000(Lcom/squareup/noho/NohoCheckableGroup;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-ne p1, v0, :cond_3

    .line 623
    :cond_0
    instance-of v0, p2, Landroid/widget/CompoundButton;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 624
    move-object v0, p2

    check-cast v0, Landroid/widget/CompoundButton;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    .line 625
    :cond_1
    instance-of v0, p2, Landroid/widget/Checkable;

    if-eqz v0, :cond_2

    .line 626
    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 629
    :cond_2
    :goto_0
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    .line 630
    iget-object v1, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {v1}, Lcom/squareup/noho/NohoCheckableGroup;->access$400(Lcom/squareup/noho/NohoCheckableGroup;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 631
    iget-object v1, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/squareup/noho/NohoCheckableGroup;->access$600(Lcom/squareup/noho/NohoCheckableGroup;IZ)Z

    .line 632
    iget-object v1, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {v1}, Lcom/squareup/noho/NohoCheckableGroup;->access$400(Lcom/squareup/noho/NohoCheckableGroup;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 636
    :cond_3
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$PassThroughHierarchyChangeListener;->onHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    if-eqz v0, :cond_4

    .line 637
    invoke-interface {v0, p1, p2}, Landroid/view/ViewGroup$OnHierarchyChangeListener;->onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V

    :cond_4
    return-void
.end method
