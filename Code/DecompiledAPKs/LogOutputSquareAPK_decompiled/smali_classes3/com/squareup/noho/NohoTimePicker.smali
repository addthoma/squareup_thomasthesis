.class public final Lcom/squareup/noho/NohoTimePicker;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "NohoTimePicker.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoTimePicker.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoTimePicker.kt\ncom/squareup/noho/NohoTimePicker\n+ 2 Delegates.kt\ncom/squareup/util/DelegatesKt\n+ 3 Delegates.kt\nkotlin/properties/Delegates\n*L\n1#1,154:1\n45#2:155\n45#2:159\n33#3,3:156\n33#3,3:160\n*E\n*S KotlinDebug\n*F\n+ 1 NohoTimePicker.kt\ncom/squareup/noho/NohoTimePicker\n*L\n31#1:155\n33#1:159\n31#1,3:156\n33#1,3:160\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0007\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010!\u001a\u00020\"H\u0002J\u0008\u0010#\u001a\u00020\"H\u0002J\u0018\u0010$\u001a\u00020\"2\u0006\u0010%\u001a\u00020\u00072\u0006\u0010&\u001a\u00020\u0007H\u0002J\u000c\u0010\'\u001a\u00020\u0007*\u00020\u0007H\u0002J\u000c\u0010(\u001a\u00020\u0007*\u00020\u0007H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000c\u001a\u00020\r8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012R\u000e\u0010\u0013\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R+\u0010\u0015\u001a\u00020\r2\u0006\u0010\u0014\u001a\u00020\r8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0018\u0010\u0019\u001a\u0004\u0008\u0016\u0010\u0010\"\u0004\u0008\u0017\u0010\u0012R+\u0010\u001a\u001a\u00020\r2\u0006\u0010\u0014\u001a\u00020\r8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u001d\u0010\u0019\u001a\u0004\u0008\u001b\u0010\u0010\"\u0004\u0008\u001c\u0010\u0012R\u000e\u0010\u001e\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/noho/NohoTimePicker;",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "amPmPicker",
        "Lcom/squareup/noho/NohoNumberPicker;",
        "colon",
        "time",
        "Lorg/threeten/bp/LocalTime;",
        "currentTime",
        "getCurrentTime",
        "()Lorg/threeten/bp/LocalTime;",
        "setCurrentTime",
        "(Lorg/threeten/bp/LocalTime;)V",
        "hourPicker",
        "<set-?>",
        "maxTime",
        "getMaxTime",
        "setMaxTime",
        "maxTime$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "minTime",
        "getMinTime",
        "setMinTime",
        "minTime$delegate",
        "minutePicker",
        "use24Hour",
        "",
        "recalculateMaxPickerValues",
        "",
        "setupListeners",
        "updatePickers",
        "hour",
        "minute",
        "to12Or24Hour",
        "to24Hour",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final amPmPicker:Lcom/squareup/noho/NohoNumberPicker;

.field private final colon:Lcom/squareup/noho/NohoNumberPicker;

.field private final hourPicker:Lcom/squareup/noho/NohoNumberPicker;

.field private final maxTime$delegate:Lkotlin/properties/ReadWriteProperty;

.field private final minTime$delegate:Lkotlin/properties/ReadWriteProperty;

.field private final minutePicker:Lcom/squareup/noho/NohoNumberPicker;

.field private final use24Hour:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/squareup/noho/NohoTimePicker;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "minTime"

    const-string v5, "getMinTime()Lorg/threeten/bp/LocalTime;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v3, "maxTime"

    const-string v4, "getMaxTime()Lorg/threeten/bp/LocalTime;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aput-object v0, v1, v2

    sput-object v1, Lcom/squareup/noho/NohoTimePicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoTimePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoTimePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x0

    .line 31
    invoke-static {p2, p2}, Lorg/threeten/bp/LocalTime;->of(II)Lorg/threeten/bp/LocalTime;

    move-result-object p2

    .line 155
    sget-object p3, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    .line 156
    new-instance p3, Lcom/squareup/noho/NohoTimePicker$$special$$inlined$observable$1;

    invoke-direct {p3, p2, p2, p0}, Lcom/squareup/noho/NohoTimePicker$$special$$inlined$observable$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/noho/NohoTimePicker;)V

    check-cast p3, Lkotlin/properties/ReadWriteProperty;

    .line 155
    iput-object p3, p0, Lcom/squareup/noho/NohoTimePicker;->minTime$delegate:Lkotlin/properties/ReadWriteProperty;

    const/16 p2, 0x17

    const/16 p3, 0x3b

    .line 33
    invoke-static {p2, p3}, Lorg/threeten/bp/LocalTime;->of(II)Lorg/threeten/bp/LocalTime;

    move-result-object p2

    .line 159
    sget-object p3, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    .line 160
    new-instance p3, Lcom/squareup/noho/NohoTimePicker$$special$$inlined$observable$2;

    invoke-direct {p3, p2, p2, p0}, Lcom/squareup/noho/NohoTimePicker$$special$$inlined$observable$2;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/noho/NohoTimePicker;)V

    check-cast p3, Lkotlin/properties/ReadWriteProperty;

    .line 159
    iput-object p3, p0, Lcom/squareup/noho/NohoTimePicker;->maxTime$delegate:Lkotlin/properties/ReadWriteProperty;

    .line 50
    sget p2, Lcom/squareup/noho/R$layout;->noho_time_picker_contents:I

    move-object p3, p0

    check-cast p3, Landroid/view/ViewGroup;

    const/4 v0, 0x1

    invoke-static {p2, p3, v0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 52
    sget p2, Lcom/squareup/noho/R$id;->hour_picker:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/noho/NohoNumberPicker;

    iput-object p2, p0, Lcom/squareup/noho/NohoTimePicker;->hourPicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 53
    sget p2, Lcom/squareup/noho/R$id;->colon:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/noho/NohoNumberPicker;

    iput-object p2, p0, Lcom/squareup/noho/NohoTimePicker;->colon:Lcom/squareup/noho/NohoNumberPicker;

    .line 54
    sget p2, Lcom/squareup/noho/R$id;->minute_picker:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/noho/NohoNumberPicker;

    iput-object p2, p0, Lcom/squareup/noho/NohoTimePicker;->minutePicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 55
    sget p2, Lcom/squareup/noho/R$id;->ampm_picker:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/noho/NohoNumberPicker;

    iput-object p2, p0, Lcom/squareup/noho/NohoTimePicker;->amPmPicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 58
    invoke-static {p1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/noho/NohoTimePicker;->use24Hour:Z

    .line 61
    invoke-static {}, Lorg/threeten/bp/LocalTime;->now()Lorg/threeten/bp/LocalTime;

    move-result-object p1

    .line 62
    invoke-virtual {p1}, Lorg/threeten/bp/LocalTime;->getHour()I

    move-result p2

    invoke-virtual {p1}, Lorg/threeten/bp/LocalTime;->getMinute()I

    move-result p1

    invoke-direct {p0, p2, p1}, Lcom/squareup/noho/NohoTimePicker;->updatePickers(II)V

    .line 65
    invoke-direct {p0}, Lcom/squareup/noho/NohoTimePicker;->setupListeners()V

    .line 66
    invoke-direct {p0}, Lcom/squareup/noho/NohoTimePicker;->recalculateMaxPickerValues()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 20
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 21
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoTimePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$getUse24Hour$p(Lcom/squareup/noho/NohoTimePicker;)Z
    .locals 0

    .line 17
    iget-boolean p0, p0, Lcom/squareup/noho/NohoTimePicker;->use24Hour:Z

    return p0
.end method

.method public static final synthetic access$recalculateMaxPickerValues(Lcom/squareup/noho/NohoTimePicker;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/noho/NohoTimePicker;->recalculateMaxPickerValues()V

    return-void
.end method

.method private final recalculateMaxPickerValues()V
    .locals 5

    .line 118
    iget-object v0, p0, Lcom/squareup/noho/NohoTimePicker;->amPmPicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 119
    invoke-virtual {p0}, Lcom/squareup/noho/NohoTimePicker;->getMinTime()Lorg/threeten/bp/LocalTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalTime;->getHour()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/16 v4, 0xc

    if-ge v1, v4, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    .line 120
    invoke-virtual {p0}, Lcom/squareup/noho/NohoTimePicker;->getMaxTime()Lorg/threeten/bp/LocalTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalTime;->getHour()I

    move-result v1

    if-ge v1, v4, :cond_1

    const/4 v2, 0x0

    :cond_1
    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    .line 123
    iget-object v0, p0, Lcom/squareup/noho/NohoTimePicker;->hourPicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 125
    iget-boolean v1, p0, Lcom/squareup/noho/NohoTimePicker;->use24Hour:Z

    if-eqz v1, :cond_2

    .line 126
    invoke-virtual {p0}, Lcom/squareup/noho/NohoTimePicker;->getMinTime()Lorg/threeten/bp/LocalTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalTime;->getHour()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    .line 127
    invoke-virtual {p0}, Lcom/squareup/noho/NohoTimePicker;->getMaxTime()Lorg/threeten/bp/LocalTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalTime;->getHour()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    goto :goto_3

    .line 129
    :cond_2
    iget-object v1, p0, Lcom/squareup/noho/NohoTimePicker;->amPmPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v1

    if-nez v1, :cond_4

    .line 130
    invoke-virtual {p0}, Lcom/squareup/noho/NohoTimePicker;->getMinTime()Lorg/threeten/bp/LocalTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalTime;->getHour()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/squareup/noho/NohoTimePicker;->to12Or24Hour(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    .line 131
    invoke-virtual {p0}, Lcom/squareup/noho/NohoTimePicker;->getMaxTime()Lorg/threeten/bp/LocalTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalTime;->getHour()I

    move-result v1

    if-ge v1, v4, :cond_3

    invoke-virtual {p0}, Lcom/squareup/noho/NohoTimePicker;->getMaxTime()Lorg/threeten/bp/LocalTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalTime;->getHour()I

    move-result v1

    goto :goto_1

    :cond_3
    const/16 v1, 0xb

    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    goto :goto_3

    .line 134
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/noho/NohoTimePicker;->getMinTime()Lorg/threeten/bp/LocalTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalTime;->getHour()I

    move-result v1

    if-lt v1, v4, :cond_5

    invoke-virtual {p0}, Lcom/squareup/noho/NohoTimePicker;->getMinTime()Lorg/threeten/bp/LocalTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalTime;->getHour()I

    move-result v1

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    :goto_2
    invoke-direct {p0, v1}, Lcom/squareup/noho/NohoTimePicker;->to12Or24Hour(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    .line 135
    invoke-virtual {p0}, Lcom/squareup/noho/NohoTimePicker;->getMaxTime()Lorg/threeten/bp/LocalTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalTime;->getHour()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/squareup/noho/NohoTimePicker;->to12Or24Hour(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    .line 140
    :goto_3
    iget-object v0, p0, Lcom/squareup/noho/NohoTimePicker;->minutePicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 141
    iget-object v1, p0, Lcom/squareup/noho/NohoTimePicker;->hourPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/squareup/noho/NohoTimePicker;->to24Hour(I)I

    move-result v1

    .line 142
    invoke-virtual {p0}, Lcom/squareup/noho/NohoTimePicker;->getMinTime()Lorg/threeten/bp/LocalTime;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/LocalTime;->getHour()I

    move-result v2

    if-ne v1, v2, :cond_6

    invoke-virtual {p0}, Lcom/squareup/noho/NohoTimePicker;->getMinTime()Lorg/threeten/bp/LocalTime;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/LocalTime;->getMinute()I

    move-result v3

    :cond_6
    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    .line 143
    invoke-virtual {p0}, Lcom/squareup/noho/NohoTimePicker;->getMaxTime()Lorg/threeten/bp/LocalTime;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/LocalTime;->getHour()I

    move-result v2

    if-ne v1, v2, :cond_7

    invoke-virtual {p0}, Lcom/squareup/noho/NohoTimePicker;->getMaxTime()Lorg/threeten/bp/LocalTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalTime;->getMinute()I

    move-result v1

    goto :goto_4

    :cond_7
    const/16 v1, 0x3b

    :goto_4
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    return-void
.end method

.method private final setupListeners()V
    .locals 5

    .line 70
    new-instance v0, Lcom/squareup/noho/NohoTimePicker$setupListeners$listener$1;

    invoke-direct {v0, p0}, Lcom/squareup/noho/NohoTimePicker$setupListeners$listener$1;-><init>(Lcom/squareup/noho/NohoTimePicker;)V

    check-cast v0, Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;

    .line 74
    iget-object v1, p0, Lcom/squareup/noho/NohoTimePicker;->hourPicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 75
    new-instance v2, Lcom/squareup/noho/NohoTimePicker$setupListeners$$inlined$with$lambda$1;

    invoke-direct {v2, p0, v0}, Lcom/squareup/noho/NohoTimePicker$setupListeners$$inlined$with$lambda$1;-><init>(Lcom/squareup/noho/NohoTimePicker;Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;)V

    check-cast v2, Lcom/squareup/noho/NohoNumberPicker$Formatter;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoNumberPicker;->setFormatter(Lcom/squareup/noho/NohoNumberPicker$Formatter;)V

    .line 82
    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoNumberPicker;->setOnValueChangedListener(Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;)V

    .line 85
    iget-object v1, p0, Lcom/squareup/noho/NohoTimePicker;->colon:Lcom/squareup/noho/NohoNumberPicker;

    const/4 v2, 0x0

    .line 86
    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    .line 87
    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    .line 88
    new-instance v3, Lcom/squareup/noho/NohoTimePicker$setupListeners$2$1;

    invoke-direct {v3, v1}, Lcom/squareup/noho/NohoTimePicker$setupListeners$2$1;-><init>(Lcom/squareup/noho/NohoNumberPicker;)V

    check-cast v3, Lcom/squareup/noho/NohoNumberPicker$Formatter;

    invoke-virtual {v1, v3}, Lcom/squareup/noho/NohoNumberPicker;->setFormatter(Lcom/squareup/noho/NohoNumberPicker$Formatter;)V

    .line 91
    iget-object v1, p0, Lcom/squareup/noho/NohoTimePicker;->minutePicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 92
    sget-object v3, Lcom/squareup/noho/NohoTimePicker$setupListeners$3$1;->INSTANCE:Lcom/squareup/noho/NohoTimePicker$setupListeners$3$1;

    check-cast v3, Lcom/squareup/noho/NohoNumberPicker$Formatter;

    invoke-virtual {v1, v3}, Lcom/squareup/noho/NohoNumberPicker;->setFormatter(Lcom/squareup/noho/NohoNumberPicker$Formatter;)V

    .line 93
    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoNumberPicker;->setOnValueChangedListener(Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;)V

    .line 96
    iget-object v1, p0, Lcom/squareup/noho/NohoTimePicker;->amPmPicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 97
    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    const/4 v2, 0x1

    .line 98
    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    .line 100
    new-instance v3, Ljava/text/DateFormatSymbols;

    invoke-direct {v3}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v3}, Ljava/text/DateFormatSymbols;->getAmPmStrings()[Ljava/lang/String;

    move-result-object v3

    const-string v4, "DateFormatSymbols().amPmStrings"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    new-instance v4, Lcom/squareup/noho/NohoTimePicker$setupListeners$4$1;

    invoke-direct {v4, v3}, Lcom/squareup/noho/NohoTimePicker$setupListeners$4$1;-><init>([Ljava/lang/String;)V

    check-cast v4, Lcom/squareup/noho/NohoNumberPicker$Formatter;

    invoke-virtual {v1, v4}, Lcom/squareup/noho/NohoNumberPicker;->setFormatter(Lcom/squareup/noho/NohoNumberPicker$Formatter;)V

    .line 102
    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoNumberPicker;->setOnValueChangedListener(Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;)V

    .line 104
    check-cast v1, Landroid/view/View;

    iget-boolean v0, p0, Lcom/squareup/noho/NohoTimePicker;->use24Hour:Z

    xor-int/2addr v0, v2

    invoke-static {v1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final to12Or24Hour(I)I
    .locals 1

    .line 148
    iget-boolean v0, p0, Lcom/squareup/noho/NohoTimePicker;->use24Hour:Z

    if-nez v0, :cond_0

    rem-int/lit8 p1, p1, 0xc

    :cond_0
    return p1
.end method

.method private final to24Hour(I)I
    .locals 2

    .line 152
    iget-boolean v0, p0, Lcom/squareup/noho/NohoTimePicker;->use24Hour:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/noho/NohoTimePicker;->amPmPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    add-int/lit8 p1, p1, 0xc

    :cond_0
    return p1
.end method

.method private final updatePickers(II)V
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/squareup/noho/NohoTimePicker;->hourPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoTimePicker;->to12Or24Hour(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setValue(I)V

    .line 113
    iget-object v0, p0, Lcom/squareup/noho/NohoTimePicker;->minutePicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoNumberPicker;->setValue(I)V

    .line 114
    iget-object p2, p0, Lcom/squareup/noho/NohoTimePicker;->amPmPicker:Lcom/squareup/noho/NohoNumberPicker;

    const/16 v0, 0xc

    if-ge p1, v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoNumberPicker;->setValue(I)V

    return-void
.end method


# virtual methods
.method public final getCurrentTime()Lorg/threeten/bp/LocalTime;
    .locals 3

    .line 37
    iget-object v0, p0, Lcom/squareup/noho/NohoTimePicker;->hourPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/noho/NohoTimePicker;->minutePicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v1

    invoke-static {v0, v1}, Lorg/threeten/bp/LocalTime;->of(II)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    .line 38
    iget-boolean v1, p0, Lcom/squareup/noho/NohoTimePicker;->use24Hour:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/squareup/noho/NohoTimePicker;->amPmPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const-wide/16 v1, 0xc

    .line 39
    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/LocalTime;->plusHours(J)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    const-string v1, "currentTime.plusHours(12)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v1, "currentTime"

    .line 41
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public final getMaxTime()Lorg/threeten/bp/LocalTime;
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoTimePicker;->maxTime$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoTimePicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method public final getMinTime()Lorg/threeten/bp/LocalTime;
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoTimePicker;->minTime$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoTimePicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method public final setCurrentTime(Lorg/threeten/bp/LocalTime;)V
    .locals 1

    const-string/jumbo v0, "time"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p1}, Lorg/threeten/bp/LocalTime;->getHour()I

    move-result v0

    invoke-virtual {p1}, Lorg/threeten/bp/LocalTime;->getMinute()I

    move-result p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/noho/NohoTimePicker;->updatePickers(II)V

    .line 46
    invoke-direct {p0}, Lcom/squareup/noho/NohoTimePicker;->recalculateMaxPickerValues()V

    return-void
.end method

.method public final setMaxTime(Lorg/threeten/bp/LocalTime;)V
    .locals 3

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/noho/NohoTimePicker;->maxTime$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoTimePicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setMinTime(Lorg/threeten/bp/LocalTime;)V
    .locals 3

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/noho/NohoTimePicker;->minTime$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoTimePicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method
