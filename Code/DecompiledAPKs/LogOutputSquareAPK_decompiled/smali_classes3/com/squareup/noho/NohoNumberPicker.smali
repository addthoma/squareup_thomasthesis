.class public Lcom/squareup/noho/NohoNumberPicker;
.super Landroid/widget/FrameLayout;
.source "NohoNumberPicker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/NohoNumberPicker$Formatter;,
        Lcom/squareup/noho/NohoNumberPicker$OnScrollListener;,
        Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;,
        Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;,
        Lcom/squareup/noho/NohoNumberPicker$Alignment;,
        Lcom/squareup/noho/NohoNumberPicker$TextAlignment;
    }
.end annotation


# static fields
.field private static final SELECTOR_ADJUSTMENT_DURATION_MILLIS:I = 0x320

.field private static final SELECTOR_MAX_FLING_VELOCITY_ADJUSTMENT:I = 0x8

.field private static final SELECTOR_MIDDLE_ITEM_INDEX:I = 0x2

.field private static final SELECTOR_WHEEL_ITEM_COUNT:I = 0x5

.field private static final SNAP_SCROLL_DURATION:I = 0x12c

.field private static final TOP_AND_BOTTOM_FADING_EDGE_STRENGTH:F = 0.9f

.field private static final UNSCALED_DEFAULT_SELECTION_DIVIDERS_DISTANCE:I = 0x30

.field private static final UNSCALED_DEFAULT_SELECTION_DIVIDER_HEIGHT:I = 0x2

.field private static final UNSCALED_DEFAULT_TEXT_SIZE:I = 0x10

.field private static final UNSPECIFIED:I = -0x1


# instance fields
.field private final adjustScroller:Landroid/widget/Scroller;

.field private bottomSelectionDividerBottom:I

.field private canWrapSelectorWheel:Z

.field private computeMaxWidth:Z

.field private currentScrollOffset:I

.field private decrementVirtualButtonPressed:Z

.field private final flingScroller:Landroid/widget/Scroller;

.field private formatter:Lcom/squareup/noho/NohoNumberPicker$Formatter;

.field private ignoreMoveEvents:Z

.field private incrementVirtualButtonPressed:Z

.field private initialScrollOffset:I

.field private lastDownEventY:F

.field private lastDownOrMoveEventY:F

.field private lastHandledDownDpadKeyCode:I

.field private maxValue:I

.field private maxWidth:I

.field private maximumFlingVelocity:I

.field private minValue:I

.field private minimumFlingVelocity:I

.field private onScrollListener:Lcom/squareup/noho/NohoNumberPicker$OnScrollListener;

.field private onValueChangeListener:Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;

.field private performClickOnTap:Z

.field private final pressedStateHelper:Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;

.field private previousScrollerY:I

.field private scrollState:I

.field private selectedColor:I

.field private final selectionDivider:Landroid/graphics/drawable/Drawable;

.field private final selectionDividerHeight:I

.field private selectionDividersDistance:I

.field private selectionItemGap:I

.field private selectorElementHeight:I

.field private final selectorIndexToStringCache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private selectorIndices:[I

.field private final selectorWheelPaint:Landroid/graphics/Paint;

.field private textColor:I

.field private final textGravity:I

.field private final textSize:I

.field private topSelectionDividerTop:I

.field private touchSlop:I

.field private value:I

.field private velocityTracker:Landroid/view/VelocityTracker;

.field private wrapSelectorWheel:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 241
    invoke-direct {p0, p1, v0}, Lcom/squareup/noho/NohoNumberPicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 245
    sget v0, Lcom/squareup/noho/R$attr;->nohoNumberPickerStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/noho/NohoNumberPicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .line 249
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 112
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndexToStringCache:Landroid/util/SparseArray;

    .line 115
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorWheelPaint:Landroid/graphics/Paint;

    const/4 v0, 0x5

    new-array v0, v0, [I

    .line 160
    iput-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndices:[I

    const/high16 v0, -0x80000000

    .line 166
    iput v0, p0, Lcom/squareup/noho/NohoNumberPicker;->initialScrollOffset:I

    const/4 v0, 0x0

    .line 211
    iput v0, p0, Lcom/squareup/noho/NohoNumberPicker;->scrollState:I

    const/4 v2, -0x1

    .line 238
    iput v2, p0, Lcom/squareup/noho/NohoNumberPicker;->lastHandledDownDpadKeyCode:I

    .line 252
    sget-object v3, Lcom/squareup/noho/R$styleable;->NohoNumberPicker:[I

    invoke-virtual {p1, p2, v3, p3, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 255
    sget p3, Lcom/squareup/noho/R$styleable;->NohoNumberPicker_android_divider:I

    invoke-virtual {p2, p3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p3

    if-eqz p3, :cond_0

    .line 257
    invoke-virtual {p3, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 258
    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 259
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getDrawableState()[I

    move-result-object v3

    invoke-virtual {p3, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 262
    :cond_0
    iput-object p3, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionDivider:Landroid/graphics/drawable/Drawable;

    const/high16 p3, 0x40000000    # 2.0f

    .line 266
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 264
    invoke-static {v1, p3, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p3

    float-to-int p3, p3

    .line 267
    sget v3, Lcom/squareup/noho/R$styleable;->NohoNumberPicker_android_dividerHeight:I

    invoke-virtual {p2, v3, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p3

    iput p3, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionDividerHeight:I

    const/high16 p3, 0x42400000    # 48.0f

    .line 272
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 270
    invoke-static {v1, p3, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p3

    float-to-int p3, p3

    .line 273
    sget v3, Lcom/squareup/noho/R$styleable;->NohoNumberPicker_sqSelectionDividersDistance:I

    invoke-virtual {p2, v3, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p3

    iput p3, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionDividersDistance:I

    const/4 p3, 0x2

    const/high16 v3, 0x41800000    # 16.0f

    .line 278
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 276
    invoke-static {p3, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p3

    float-to-int p3, p3

    .line 279
    sget v3, Lcom/squareup/noho/R$styleable;->NohoNumberPicker_android_textSize:I

    invoke-virtual {p2, v3, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p3

    iput p3, p0, Lcom/squareup/noho/NohoNumberPicker;->textSize:I

    .line 282
    sget p3, Lcom/squareup/noho/R$styleable;->NohoNumberPicker_sqTextGravity:I

    invoke-virtual {p2, p3, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p3

    iput p3, p0, Lcom/squareup/noho/NohoNumberPicker;->textGravity:I

    .line 285
    sget p3, Lcom/squareup/noho/R$styleable;->NohoNumberPicker_android_maxWidth:I

    invoke-virtual {p2, p3, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p3

    iput p3, p0, Lcom/squareup/noho/NohoNumberPicker;->maxWidth:I

    .line 287
    sget p3, Lcom/squareup/noho/R$styleable;->NohoNumberPicker_sqSelectionItemGap:I

    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p3

    iput p3, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionItemGap:I

    .line 290
    iget p3, p0, Lcom/squareup/noho/NohoNumberPicker;->textSize:I

    iget v3, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionItemGap:I

    add-int/2addr p3, v3

    iput p3, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorElementHeight:I

    .line 291
    iget p3, p0, Lcom/squareup/noho/NohoNumberPicker;->maxWidth:I

    if-ne p3, v2, :cond_1

    const/4 p3, 0x1

    goto :goto_0

    :cond_1
    const/4 p3, 0x0

    :goto_0
    iput-boolean p3, p0, Lcom/squareup/noho/NohoNumberPicker;->computeMaxWidth:Z

    .line 293
    sget p3, Lcom/squareup/noho/R$styleable;->NohoNumberPicker_sqSelectionTextColor:I

    const/high16 v2, -0x1000000

    invoke-virtual {p2, p3, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p3

    iput p3, p0, Lcom/squareup/noho/NohoNumberPicker;->selectedColor:I

    .line 294
    sget p3, Lcom/squareup/noho/R$styleable;->NohoNumberPicker_android_textColor:I

    invoke-virtual {p2, p3, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p3

    iput p3, p0, Lcom/squareup/noho/NohoNumberPicker;->textColor:I

    .line 296
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 298
    new-instance p2, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;

    invoke-direct {p2, p0}, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;-><init>(Lcom/squareup/noho/NohoNumberPicker;)V

    iput-object p2, p0, Lcom/squareup/noho/NohoNumberPicker;->pressedStateHelper:Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;

    .line 305
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoNumberPicker;->setWillNotDraw(Z)V

    .line 308
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p2

    .line 309
    invoke-virtual {p2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result p3

    iput p3, p0, Lcom/squareup/noho/NohoNumberPicker;->touchSlop:I

    .line 310
    invoke-virtual {p2}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result p3

    iput p3, p0, Lcom/squareup/noho/NohoNumberPicker;->minimumFlingVelocity:I

    .line 311
    invoke-virtual {p2}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result p2

    div-int/lit8 p2, p2, 0x8

    iput p2, p0, Lcom/squareup/noho/NohoNumberPicker;->maximumFlingVelocity:I

    .line 315
    iget-object p2, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorWheelPaint:Landroid/graphics/Paint;

    iget p3, p0, Lcom/squareup/noho/NohoNumberPicker;->textSize:I

    int-to-float p3, p3

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 316
    iget-object p2, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorWheelPaint:Landroid/graphics/Paint;

    sget-object p3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p1, p3}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 317
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorWheelPaint:Landroid/graphics/Paint;

    iget p2, p0, Lcom/squareup/noho/NohoNumberPicker;->textColor:I

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 320
    new-instance p1, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getContext()Landroid/content/Context;

    move-result-object p2

    const/4 p3, 0x0

    invoke-direct {p1, p2, p3, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V

    iput-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->flingScroller:Landroid/widget/Scroller;

    .line 321
    new-instance p1, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getContext()Landroid/content/Context;

    move-result-object p2

    new-instance p3, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v0, 0x40200000    # 2.5f

    invoke-direct {p3, v0}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-direct {p1, p2, p3}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->adjustScroller:Landroid/widget/Scroller;

    .line 324
    invoke-virtual {p0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setFocusable(Z)V

    .line 325
    invoke-virtual {p0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/noho/NohoNumberPicker;)Z
    .locals 0

    .line 68
    iget-boolean p0, p0, Lcom/squareup/noho/NohoNumberPicker;->incrementVirtualButtonPressed:Z

    return p0
.end method

.method static synthetic access$002(Lcom/squareup/noho/NohoNumberPicker;Z)Z
    .locals 0

    .line 68
    iput-boolean p1, p0, Lcom/squareup/noho/NohoNumberPicker;->incrementVirtualButtonPressed:Z

    return p1
.end method

.method static synthetic access$100(Lcom/squareup/noho/NohoNumberPicker;)I
    .locals 0

    .line 68
    iget p0, p0, Lcom/squareup/noho/NohoNumberPicker;->bottomSelectionDividerBottom:I

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/noho/NohoNumberPicker;)Z
    .locals 0

    .line 68
    iget-boolean p0, p0, Lcom/squareup/noho/NohoNumberPicker;->decrementVirtualButtonPressed:Z

    return p0
.end method

.method static synthetic access$202(Lcom/squareup/noho/NohoNumberPicker;Z)Z
    .locals 0

    .line 68
    iput-boolean p1, p0, Lcom/squareup/noho/NohoNumberPicker;->decrementVirtualButtonPressed:Z

    return p1
.end method

.method static synthetic access$300(Lcom/squareup/noho/NohoNumberPicker;)I
    .locals 0

    .line 68
    iget p0, p0, Lcom/squareup/noho/NohoNumberPicker;->topSelectionDividerTop:I

    return p0
.end method

.method private changeValueByOne(Z)V
    .locals 13

    .line 949
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->flingScroller:Landroid/widget/Scroller;

    invoke-direct {p0, v0}, Lcom/squareup/noho/NohoNumberPicker;->moveToFinalScrollerPosition(Landroid/widget/Scroller;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 950
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->adjustScroller:Landroid/widget/Scroller;

    invoke-direct {p0, v0}, Lcom/squareup/noho/NohoNumberPicker;->moveToFinalScrollerPosition(Landroid/widget/Scroller;)Z

    :cond_0
    const/4 v0, 0x0

    .line 952
    iput v0, p0, Lcom/squareup/noho/NohoNumberPicker;->previousScrollerY:I

    if-eqz p1, :cond_1

    .line 954
    iget-object v1, p0, Lcom/squareup/noho/NohoNumberPicker;->flingScroller:Landroid/widget/Scroller;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget p1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorElementHeight:I

    neg-int v5, p1

    const/16 v6, 0x12c

    invoke-virtual/range {v1 .. v6}, Landroid/widget/Scroller;->startScroll(IIIII)V

    goto :goto_0

    .line 956
    :cond_1
    iget-object v7, p0, Lcom/squareup/noho/NohoNumberPicker;->flingScroller:Landroid/widget/Scroller;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    iget v11, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorElementHeight:I

    const/16 v12, 0x12c

    invoke-virtual/range {v7 .. v12}, Landroid/widget/Scroller;->startScroll(IIIII)V

    :goto_0
    const/4 p1, 0x1

    .line 958
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoNumberPicker;->onScrollStateChange(I)V

    .line 959
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->invalidate()V

    return-void
.end method

.method private decrementSelectorIndices([I)V
    .locals 3

    .line 1039
    array-length v0, p1

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    const/4 v2, 0x0

    invoke-static {p1, v2, p1, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1040
    aget v0, p1, v1

    sub-int/2addr v0, v1

    .line 1041
    iget-boolean v1, p0, Lcom/squareup/noho/NohoNumberPicker;->wrapSelectorWheel:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->minValue:I

    if-ge v0, v1, :cond_0

    .line 1042
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->maxValue:I

    .line 1044
    :cond_0
    aput v0, p1, v2

    .line 1045
    invoke-direct {p0, v0}, Lcom/squareup/noho/NohoNumberPicker;->getOrComputeCachedValue(I)Ljava/lang/String;

    return-void
.end method

.method private ensureScrollWheelAdjusted()V
    .locals 7

    .line 1095
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->initialScrollOffset:I

    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    sub-int/2addr v0, v1

    if-eqz v0, :cond_2

    const/4 v1, 0x0

    .line 1097
    iput v1, p0, Lcom/squareup/noho/NohoNumberPicker;->previousScrollerY:I

    .line 1098
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v2, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorElementHeight:I

    div-int/lit8 v3, v2, 0x2

    if-le v1, v3, :cond_1

    if-lez v0, :cond_0

    neg-int v2, v2

    :cond_0
    add-int/2addr v0, v2

    :cond_1
    move v5, v0

    .line 1101
    iget-object v1, p0, Lcom/squareup/noho/NohoNumberPicker;->adjustScroller:Landroid/widget/Scroller;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0x320

    invoke-virtual/range {v1 .. v6}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 1102
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->invalidate()V

    :cond_2
    return-void
.end method

.method private fling(I)V
    .locals 10

    const/4 v0, 0x0

    .line 999
    iput v0, p0, Lcom/squareup/noho/NohoNumberPicker;->previousScrollerY:I

    if-lez p1, :cond_0

    .line 1002
    iget-object v1, p0, Lcom/squareup/noho/NohoNumberPicker;->flingScroller:Landroid/widget/Scroller;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7fffffff

    move v5, p1

    invoke-virtual/range {v1 .. v9}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    goto :goto_0

    .line 1004
    :cond_0
    iget-object v1, p0, Lcom/squareup/noho/NohoNumberPicker;->flingScroller:Landroid/widget/Scroller;

    const/4 v2, 0x0

    const v3, 0x7fffffff

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const v9, 0x7fffffff

    move v5, p1

    invoke-virtual/range {v1 .. v9}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 1007
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->invalidate()V

    return-void
.end method

.method private formatNumber(I)Ljava/lang/String;
    .locals 3

    .line 1068
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->formatter:Lcom/squareup/noho/NohoNumberPicker$Formatter;

    if-eqz v0, :cond_0

    .line 1069
    invoke-interface {v0, p1}, Lcom/squareup/noho/NohoNumberPicker$Formatter;->format(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 1070
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v2

    const-string p1, "%d"

    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private getOrComputeCachedValue(I)Ljava/lang/String;
    .locals 2

    .line 1053
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndexToStringCache:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    .line 1058
    :cond_0
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->minValue:I

    if-lt p1, v0, :cond_2

    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->maxValue:I

    if-le p1, v0, :cond_1

    goto :goto_0

    .line 1061
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoNumberPicker;->formatNumber(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    :goto_0
    const-string v0, ""

    .line 1063
    :goto_1
    iget-object v1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndexToStringCache:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-object v0
.end method

.method private getWrappedSelectorIndex(I)I
    .locals 2

    .line 1012
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->maxValue:I

    if-le p1, v0, :cond_0

    .line 1013
    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->minValue:I

    sub-int/2addr p1, v0

    sub-int/2addr v0, v1

    rem-int/2addr p1, v0

    add-int/2addr v1, p1

    add-int/lit8 v1, v1, -0x1

    return v1

    .line 1014
    :cond_0
    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->minValue:I

    if-ge p1, v1, :cond_1

    sub-int p1, v1, p1

    sub-int v1, v0, v1

    .line 1015
    rem-int/2addr p1, v1

    sub-int/2addr v0, p1

    add-int/lit8 v0, v0, 0x1

    return v0

    :cond_1
    return p1
.end method

.method private incrementSelectorIndices([I)V
    .locals 3

    .line 1025
    array-length v0, p1

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    const/4 v2, 0x0

    invoke-static {p1, v1, p1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1026
    array-length v0, p1

    add-int/lit8 v0, v0, -0x2

    aget v0, p1, v0

    add-int/2addr v0, v1

    .line 1027
    iget-boolean v2, p0, Lcom/squareup/noho/NohoNumberPicker;->wrapSelectorWheel:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/squareup/noho/NohoNumberPicker;->maxValue:I

    if-le v0, v2, :cond_0

    .line 1028
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->minValue:I

    .line 1030
    :cond_0
    array-length v2, p1

    sub-int/2addr v2, v1

    aput v0, p1, v2

    .line 1031
    invoke-direct {p0, v0}, Lcom/squareup/noho/NohoNumberPicker;->getOrComputeCachedValue(I)Ljava/lang/String;

    return-void
.end method

.method private initializeSelectorWheel()V
    .locals 2

    .line 963
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->initializeSelectorWheelIndices()V

    .line 966
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->textSize:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 967
    iget-object v1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorWheelPaint:Landroid/graphics/Paint;

    .line 968
    invoke-virtual {v1}, Landroid/graphics/Paint;->ascent()F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorWheelPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->descent()F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 970
    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorElementHeight:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/noho/NohoNumberPicker;->initialScrollOffset:I

    .line 972
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->initialScrollOffset:I

    iput v0, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    const/4 v0, 0x1

    .line 974
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoNumberPicker;->setVerticalFadingEdgeEnabled(Z)V

    .line 975
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionItemGap:I

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoNumberPicker;->setFadingEdgeLength(I)V

    return-void
.end method

.method private initializeSelectorWheelIndices()V
    .locals 4

    .line 905
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndexToStringCache:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 906
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v0

    const/4 v1, 0x0

    .line 907
    :goto_0
    iget-object v2, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndices:[I

    array-length v2, v2

    if-ge v1, v2, :cond_1

    add-int/lit8 v2, v1, -0x2

    add-int/2addr v2, v0

    .line 909
    iget-boolean v3, p0, Lcom/squareup/noho/NohoNumberPicker;->wrapSelectorWheel:Z

    if-eqz v3, :cond_0

    .line 910
    invoke-direct {p0, v2}, Lcom/squareup/noho/NohoNumberPicker;->getWrappedSelectorIndex(I)I

    move-result v2

    .line 912
    :cond_0
    iget-object v3, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndices:[I

    aput v2, v3, v1

    .line 913
    invoke-direct {p0, v2}, Lcom/squareup/noho/NohoNumberPicker;->getOrComputeCachedValue(I)Ljava/lang/String;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 915
    :cond_1
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->tryComputeMaxWidth()V

    return-void
.end method

.method private makeMeasureSpec(II)I
    .locals 4

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    return p1

    .line 870
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 871
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    const/high16 v2, -0x80000000

    const/high16 v3, 0x40000000    # 2.0f

    if-eq v1, v2, :cond_3

    if-eqz v1, :cond_2

    if-ne v1, v3, :cond_1

    return p1

    .line 880
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unknown measure mode: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 878
    :cond_2
    invoke-static {p2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    return p1

    .line 876
    :cond_3
    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {p1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    return p1
.end method

.method private moveToFinalScrollerPosition(Landroid/widget/Scroller;)Z
    .locals 6

    const/4 v0, 0x1

    .line 369
    invoke-virtual {p1, v0}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 370
    invoke-virtual {p1}, Landroid/widget/Scroller;->getFinalY()I

    move-result v1

    invoke-virtual {p1}, Landroid/widget/Scroller;->getCurrY()I

    move-result p1

    sub-int/2addr v1, p1

    .line 371
    iget p1, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    add-int/2addr p1, v1

    iget v2, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorElementHeight:I

    rem-int/2addr p1, v2

    .line 372
    iget v2, p0, Lcom/squareup/noho/NohoNumberPicker;->initialScrollOffset:I

    sub-int/2addr v2, p1

    const/4 p1, 0x0

    if-eqz v2, :cond_2

    .line 374
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget v4, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorElementHeight:I

    div-int/lit8 v5, v4, 0x2

    if-le v3, v5, :cond_1

    if-lez v2, :cond_0

    sub-int/2addr v2, v4

    goto :goto_0

    :cond_0
    add-int/2addr v2, v4

    :cond_1
    :goto_0
    add-int/2addr v1, v2

    .line 382
    invoke-virtual {p0, p1, v1}, Lcom/squareup/noho/NohoNumberPicker;->scrollBy(II)V

    return v0

    :cond_2
    return p1
.end method

.method private notifyChange(II)V
    .locals 1

    .line 1078
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->onValueChangeListener:Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;

    if-eqz v0, :cond_0

    .line 1079
    invoke-interface {v0, p0, p1, p2}, Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;->onValueChange(Lcom/squareup/noho/NohoNumberPicker;II)V

    :cond_0
    return-void
.end method

.method private onScrollStateChange(I)V
    .locals 1

    .line 988
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->scrollState:I

    if-ne v0, p1, :cond_0

    return-void

    .line 991
    :cond_0
    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->scrollState:I

    .line 992
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->onScrollListener:Lcom/squareup/noho/NohoNumberPicker$OnScrollListener;

    if-eqz v0, :cond_1

    .line 993
    invoke-interface {v0, p0, p1}, Lcom/squareup/noho/NohoNumberPicker$OnScrollListener;->onScrollStateChange(Lcom/squareup/noho/NohoNumberPicker;I)V

    :cond_1
    return-void
.end method

.method private onScrollerFinished(Landroid/widget/Scroller;)V
    .locals 1

    .line 980
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->flingScroller:Landroid/widget/Scroller;

    if-ne p1, v0, :cond_0

    .line 981
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->ensureScrollWheelAdjusted()V

    const/4 p1, 0x0

    .line 982
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoNumberPicker;->onScrollStateChange(I)V

    :cond_0
    return-void
.end method

.method private removeAllCallbacks()V
    .locals 1

    .line 1084
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->pressedStateHelper:Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->cancel()V

    return-void
.end method

.method private resolveSizeAndStateRespectingMinSize(III)I
    .locals 1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 896
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    const/4 p2, 0x0

    .line 897
    invoke-static {p1, p3, p2}, Lcom/squareup/noho/NohoNumberPicker;->resolveSizeAndState(III)I

    move-result p1

    return p1

    :cond_0
    return p2
.end method

.method private tryComputeMaxWidth()V
    .locals 4

    .line 638
    iget-boolean v0, p0, Lcom/squareup/noho/NohoNumberPicker;->computeMaxWidth:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 642
    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->minValue:I

    :goto_0
    iget v2, p0, Lcom/squareup/noho/NohoNumberPicker;->maxValue:I

    if-gt v1, v2, :cond_1

    .line 643
    iget-object v2, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorWheelPaint:Landroid/graphics/Paint;

    .line 645
    invoke-direct {p0, v1}, Lcom/squareup/noho/NohoNumberPicker;->getOrComputeCachedValue(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-int v2, v2

    .line 643
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 648
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getSuggestedMinimumWidth()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 649
    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->maxWidth:I

    if-eq v1, v0, :cond_2

    .line 650
    iput v0, p0, Lcom/squareup/noho/NohoNumberPicker;->maxWidth:I

    .line 651
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->requestLayout()V

    .line 652
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->invalidate()V

    :cond_2
    return-void
.end method

.method private updateWrapSelectorWheel()V
    .locals 2

    .line 682
    iget-boolean v0, p0, Lcom/squareup/noho/NohoNumberPicker;->canWrapSelectorWheel:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->maxValue:I

    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->minValue:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndices:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/squareup/noho/NohoNumberPicker;->wrapSelectorWheel:Z

    return-void
.end method


# virtual methods
.method public computeScroll()V
    .locals 4

    .line 548
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->flingScroller:Landroid/widget/Scroller;

    .line 549
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 550
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->adjustScroller:Landroid/widget/Scroller;

    .line 551
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 555
    :cond_0
    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    .line 556
    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    .line 557
    iget v2, p0, Lcom/squareup/noho/NohoNumberPicker;->previousScrollerY:I

    if-nez v2, :cond_1

    .line 558
    invoke-virtual {v0}, Landroid/widget/Scroller;->getStartY()I

    move-result v2

    iput v2, p0, Lcom/squareup/noho/NohoNumberPicker;->previousScrollerY:I

    :cond_1
    const/4 v2, 0x0

    .line 560
    iget v3, p0, Lcom/squareup/noho/NohoNumberPicker;->previousScrollerY:I

    sub-int v3, v1, v3

    invoke-virtual {p0, v2, v3}, Lcom/squareup/noho/NohoNumberPicker;->scrollBy(II)V

    .line 561
    iput v1, p0, Lcom/squareup/noho/NohoNumberPicker;->previousScrollerY:I

    .line 562
    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 563
    invoke-direct {p0, v0}, Lcom/squareup/noho/NohoNumberPicker;->onScrollerFinished(Landroid/widget/Scroller;)V

    goto :goto_0

    .line 565
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->invalidate()V

    :goto_0
    return-void
.end method

.method protected computeVerticalScrollExtent()I
    .locals 1

    .line 612
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getHeight()I

    move-result v0

    return v0
.end method

.method protected computeVerticalScrollOffset()I
    .locals 1

    .line 602
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    return v0
.end method

.method protected computeVerticalScrollRange()I
    .locals 2

    .line 607
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->maxValue:I

    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->minValue:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorElementHeight:I

    mul-int v0, v0, v1

    return v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 5

    .line 514
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x13

    const/16 v2, 0x14

    if-eq v0, v1, :cond_1

    if-eq v0, v2, :cond_1

    const/16 v1, 0x17

    if-eq v0, v1, :cond_0

    const/16 v1, 0x42

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 518
    :cond_0
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->removeAllCallbacks()V

    goto :goto_0

    .line 522
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    const/4 v3, 0x1

    if-eqz v1, :cond_3

    if-eq v1, v3, :cond_2

    goto :goto_0

    .line 536
    :cond_2
    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->lastHandledDownDpadKeyCode:I

    if-ne v1, v0, :cond_5

    const/4 p1, -0x1

    .line 537
    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->lastHandledDownDpadKeyCode:I

    return v3

    .line 524
    :cond_3
    iget-boolean v1, p0, Lcom/squareup/noho/NohoNumberPicker;->wrapSelectorWheel:Z

    if-nez v1, :cond_6

    if-ne v0, v2, :cond_4

    .line 525
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getMaxValue()I

    move-result v4

    if-ge v1, v4, :cond_5

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getMinValue()I

    move-result v4

    if-le v1, v4, :cond_5

    goto :goto_1

    .line 543
    :cond_5
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1

    .line 526
    :cond_6
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->requestFocus()Z

    .line 527
    iput v0, p0, Lcom/squareup/noho/NohoNumberPicker;->lastHandledDownDpadKeyCode:I

    .line 528
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->removeAllCallbacks()V

    .line 529
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->flingScroller:Landroid/widget/Scroller;

    invoke-virtual {p1}, Landroid/widget/Scroller;->isFinished()Z

    move-result p1

    if-eqz p1, :cond_8

    if-ne v0, v2, :cond_7

    const/4 p1, 0x1

    goto :goto_2

    :cond_7
    const/4 p1, 0x0

    .line 530
    :goto_2
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoNumberPicker;->changeValueByOne(Z)V

    :cond_8
    return v3
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 502
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 506
    :cond_0
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->removeAllCallbacks()V

    .line 509
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected drawableStateChanged()V
    .locals 2

    .line 797
    invoke-super {p0}, Landroid/widget/FrameLayout;->drawableStateChanged()V

    .line 799
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionDivider:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionDivider:Landroid/graphics/drawable/Drawable;

    .line 800
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 801
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionDivider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoNumberPicker;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method protected getBottomFadingEdgeStrength()F
    .locals 1

    const v0, 0x3f666666    # 0.9f

    return v0
.end method

.method public getFormattedValue()Ljava/lang/String;
    .locals 2

    .line 730
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndices:[I

    const/4 v1, 0x2

    aget v0, v0, v1

    invoke-direct {p0, v0}, Lcom/squareup/noho/NohoNumberPicker;->getOrComputeCachedValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMaxValue()I
    .locals 1

    .line 758
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->maxValue:I

    return v0
.end method

.method public getMinValue()I
    .locals 1

    .line 735
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->minValue:I

    return v0
.end method

.method protected getTopFadingEdgeStrength()F
    .locals 1

    const v0, 0x3f666666    # 0.9f

    return v0
.end method

.method public getValue()I
    .locals 1

    .line 725
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->value:I

    return v0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    .line 807
    invoke-super {p0}, Landroid/widget/FrameLayout;->jumpDrawablesToCurrentState()V

    .line 809
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionDivider:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 810
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .line 791
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 792
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->removeAllCallbacks()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .line 818
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->textGravity:I

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    const/4 v3, 0x1

    if-eq v0, v3, :cond_1

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 828
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getWidth()I

    move-result v0

    int-to-float v2, v0

    .line 829
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorWheelPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto :goto_0

    .line 824
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v0, v2

    .line 825
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorWheelPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto :goto_0

    .line 821
    :cond_2
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorWheelPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 833
    :goto_0
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    int-to-float v0, v0

    const/4 v3, 0x0

    move v4, v0

    const/4 v0, 0x0

    .line 836
    :goto_1
    iget-object v5, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndices:[I

    array-length v5, v5

    if-ge v0, v5, :cond_4

    .line 837
    iget-object v5, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorWheelPaint:Landroid/graphics/Paint;

    if-ne v0, v1, :cond_3

    iget v6, p0, Lcom/squareup/noho/NohoNumberPicker;->selectedColor:I

    goto :goto_2

    :cond_3
    iget v6, p0, Lcom/squareup/noho/NohoNumberPicker;->textColor:I

    :goto_2
    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 838
    iget-object v5, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndexToStringCache:Landroid/util/SparseArray;

    iget-object v6, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndices:[I

    aget v6, v6, v0

    invoke-virtual {v5, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 839
    iget-object v6, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorWheelPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v2, v4, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 840
    iget v5, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorElementHeight:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 844
    :cond_4
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionDivider:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_5

    .line 846
    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->topSelectionDividerTop:I

    .line 847
    iget v2, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionDividerHeight:I

    add-int/2addr v2, v1

    .line 848
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getRight()I

    move-result v4

    invoke-virtual {v0, v3, v1, v4, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 849
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionDivider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 852
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->bottomSelectionDividerBottom:I

    .line 853
    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionDividerHeight:I

    sub-int v1, v0, v1

    .line 854
    iget-object v2, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionDivider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getRight()I

    move-result v4

    invoke-virtual {v2, v3, v1, v4, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 855
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionDivider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_5
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 390
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 393
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-eqz v0, :cond_1

    return v1

    .line 396
    :cond_1
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->removeAllCallbacks()V

    .line 397
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->lastDownEventY:F

    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->lastDownOrMoveEventY:F

    .line 398
    iput-boolean v1, p0, Lcom/squareup/noho/NohoNumberPicker;->ignoreMoveEvents:Z

    .line 399
    iput-boolean v1, p0, Lcom/squareup/noho/NohoNumberPicker;->performClickOnTap:Z

    .line 401
    iget p1, p0, Lcom/squareup/noho/NohoNumberPicker;->lastDownEventY:F

    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->topSelectionDividerTop:I

    int-to-float v0, v0

    const/4 v2, 0x1

    cmpg-float v0, p1, v0

    if-gez v0, :cond_2

    .line 402
    iget p1, p0, Lcom/squareup/noho/NohoNumberPicker;->scrollState:I

    if-nez p1, :cond_3

    .line 403
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->pressedStateHelper:Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->buttonPressDelayed(I)V

    goto :goto_0

    .line 406
    :cond_2
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->bottomSelectionDividerBottom:I

    int-to-float v0, v0

    cmpl-float p1, p1, v0

    if-lez p1, :cond_3

    .line 407
    iget p1, p0, Lcom/squareup/noho/NohoNumberPicker;->scrollState:I

    if-nez p1, :cond_3

    .line 408
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->pressedStateHelper:Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;

    invoke-virtual {p1, v2}, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->buttonPressDelayed(I)V

    .line 413
    :cond_3
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    invoke-interface {p1, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 414
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->flingScroller:Landroid/widget/Scroller;

    invoke-virtual {p1}, Landroid/widget/Scroller;->isFinished()Z

    move-result p1

    if-nez p1, :cond_4

    .line 415
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->flingScroller:Landroid/widget/Scroller;

    invoke-virtual {p1, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 416
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->adjustScroller:Landroid/widget/Scroller;

    invoke-virtual {p1, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 417
    invoke-direct {p0, v1}, Lcom/squareup/noho/NohoNumberPicker;->onScrollStateChange(I)V

    goto :goto_1

    .line 418
    :cond_4
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->adjustScroller:Landroid/widget/Scroller;

    invoke-virtual {p1}, Landroid/widget/Scroller;->isFinished()Z

    move-result p1

    if-nez p1, :cond_5

    .line 419
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->flingScroller:Landroid/widget/Scroller;

    invoke-virtual {p1, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 420
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->adjustScroller:Landroid/widget/Scroller;

    invoke-virtual {p1, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    goto :goto_1

    .line 421
    :cond_5
    iget p1, p0, Lcom/squareup/noho/NohoNumberPicker;->lastDownEventY:F

    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->bottomSelectionDividerBottom:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_6

    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->topSelectionDividerTop:I

    int-to-float v0, v0

    cmpl-float p1, p1, v0

    if-ltz p1, :cond_6

    .line 423
    iput-boolean v2, p0, Lcom/squareup/noho/NohoNumberPicker;->performClickOnTap:Z

    :cond_6
    :goto_1
    return v2
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    if-eqz p1, :cond_0

    .line 332
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->initializeSelectorWheel()V

    .line 333
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getHeight()I

    move-result p1

    iget p2, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionDividersDistance:I

    sub-int/2addr p1, p2

    div-int/lit8 p1, p1, 0x2

    iget p3, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionDividerHeight:I

    sub-int/2addr p1, p3

    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->topSelectionDividerTop:I

    .line 335
    iget p1, p0, Lcom/squareup/noho/NohoNumberPicker;->topSelectionDividerTop:I

    mul-int/lit8 p3, p3, 0x2

    add-int/2addr p1, p3

    add-int/2addr p1, p2

    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->bottomSelectionDividerBottom:I

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .line 342
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->maxWidth:I

    .line 343
    iget-object v1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndices:[I

    array-length v1, v1

    iget v2, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorElementHeight:I

    mul-int v1, v1, v2

    .line 344
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    .line 347
    invoke-direct {p0, p1, v0}, Lcom/squareup/noho/NohoNumberPicker;->makeMeasureSpec(II)I

    move-result v0

    .line 348
    invoke-direct {p0, p2, v1}, Lcom/squareup/noho/NohoNumberPicker;->makeMeasureSpec(II)I

    move-result v1

    .line 349
    invoke-super {p0, v0, v1}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 353
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getMeasuredWidth()I

    move-result v1

    .line 352
    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/noho/NohoNumberPicker;->resolveSizeAndStateRespectingMinSize(III)I

    move-result p1

    .line 355
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getSuggestedMinimumHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getMeasuredHeight()I

    move-result v1

    .line 354
    invoke-direct {p0, v0, v1, p2}, Lcom/squareup/noho/NohoNumberPicker;->resolveSizeAndStateRespectingMinSize(III)I

    move-result p2

    .line 357
    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/NohoNumberPicker;->setMeasuredDimension(II)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .line 434
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 437
    :cond_0
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->velocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_1

    .line 438
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->velocityTracker:Landroid/view/VelocityTracker;

    .line 440
    :cond_1
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 441
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eq v0, v3, :cond_6

    if-eq v0, v2, :cond_2

    goto/16 :goto_3

    .line 444
    :cond_2
    iget-boolean v0, p0, Lcom/squareup/noho/NohoNumberPicker;->ignoreMoveEvents:Z

    if-eqz v0, :cond_3

    goto/16 :goto_3

    .line 447
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    .line 448
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->scrollState:I

    if-eq v0, v3, :cond_4

    .line 449
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->lastDownEventY:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v0, v0

    .line 450
    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->touchSlop:I

    if-le v0, v1, :cond_5

    .line 451
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->removeAllCallbacks()V

    .line 452
    invoke-direct {p0, v3}, Lcom/squareup/noho/NohoNumberPicker;->onScrollStateChange(I)V

    goto :goto_0

    .line 455
    :cond_4
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->lastDownOrMoveEventY:F

    sub-float v0, p1, v0

    float-to-int v0, v0

    .line 456
    invoke-virtual {p0, v1, v0}, Lcom/squareup/noho/NohoNumberPicker;->scrollBy(II)V

    .line 457
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->invalidate()V

    .line 459
    :cond_5
    :goto_0
    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->lastDownOrMoveEventY:F

    goto :goto_3

    .line 463
    :cond_6
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->pressedStateHelper:Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->cancel()V

    .line 464
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->velocityTracker:Landroid/view/VelocityTracker;

    const/16 v4, 0x3e8

    iget v5, p0, Lcom/squareup/noho/NohoNumberPicker;->maximumFlingVelocity:I

    int-to-float v5, v5

    invoke-virtual {v0, v4, v5}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 465
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    float-to-int v0, v0

    .line 466
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v5, p0, Lcom/squareup/noho/NohoNumberPicker;->minimumFlingVelocity:I

    if-le v4, v5, :cond_7

    .line 467
    invoke-direct {p0, v0}, Lcom/squareup/noho/NohoNumberPicker;->fling(I)V

    .line 468
    invoke-direct {p0, v2}, Lcom/squareup/noho/NohoNumberPicker;->onScrollStateChange(I)V

    goto :goto_2

    .line 470
    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    float-to-int p1, p1

    int-to-float v0, p1

    .line 471
    iget v4, p0, Lcom/squareup/noho/NohoNumberPicker;->lastDownEventY:F

    sub-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v0, v0

    .line 472
    iget v4, p0, Lcom/squareup/noho/NohoNumberPicker;->touchSlop:I

    if-gt v0, v4, :cond_a

    .line 473
    iget-boolean v0, p0, Lcom/squareup/noho/NohoNumberPicker;->performClickOnTap:Z

    if-eqz v0, :cond_8

    .line 474
    iput-boolean v1, p0, Lcom/squareup/noho/NohoNumberPicker;->performClickOnTap:Z

    .line 475
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->performClick()Z

    goto :goto_1

    .line 477
    :cond_8
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorElementHeight:I

    div-int/2addr p1, v0

    sub-int/2addr p1, v2

    if-lez p1, :cond_9

    .line 480
    invoke-direct {p0, v3}, Lcom/squareup/noho/NohoNumberPicker;->changeValueByOne(Z)V

    .line 481
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->pressedStateHelper:Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;

    invoke-virtual {p1, v3}, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->buttonTapped(I)V

    goto :goto_1

    :cond_9
    if-gez p1, :cond_b

    .line 483
    invoke-direct {p0, v1}, Lcom/squareup/noho/NohoNumberPicker;->changeValueByOne(Z)V

    .line 484
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->pressedStateHelper:Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;

    invoke-virtual {p1, v2}, Lcom/squareup/noho/NohoNumberPicker$PressedStateHelper;->buttonTapped(I)V

    goto :goto_1

    .line 488
    :cond_a
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->ensureScrollWheelAdjusted()V

    .line 490
    :cond_b
    :goto_1
    invoke-direct {p0, v1}, Lcom/squareup/noho/NohoNumberPicker;->onScrollStateChange(I)V

    .line 492
    :goto_2
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->recycle()V

    const/4 p1, 0x0

    .line 493
    iput-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->velocityTracker:Landroid/view/VelocityTracker;

    :goto_3
    return v3
.end method

.method public scrollBy(II)V
    .locals 2

    .line 571
    iget p1, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    add-int/2addr p1, p2

    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    .line 572
    iget-boolean p1, p0, Lcom/squareup/noho/NohoNumberPicker;->wrapSelectorWheel:Z

    const/4 v0, 0x2

    if-nez p1, :cond_0

    if-lez p2, :cond_0

    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndices:[I

    aget p1, p1, v0

    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->minValue:I

    if-gt p1, v1, :cond_0

    .line 574
    iget p1, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    iget p2, p0, Lcom/squareup/noho/NohoNumberPicker;->initialScrollOffset:I

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    return-void

    .line 577
    :cond_0
    iget-boolean p1, p0, Lcom/squareup/noho/NohoNumberPicker;->wrapSelectorWheel:Z

    if-nez p1, :cond_1

    if-gez p2, :cond_1

    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndices:[I

    aget p1, p1, v0

    iget p2, p0, Lcom/squareup/noho/NohoNumberPicker;->maxValue:I

    if-lt p1, p2, :cond_1

    .line 579
    iget p1, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    iget p2, p0, Lcom/squareup/noho/NohoNumberPicker;->initialScrollOffset:I

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    return-void

    .line 582
    :cond_1
    :goto_0
    iget p1, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    iget p2, p0, Lcom/squareup/noho/NohoNumberPicker;->initialScrollOffset:I

    sub-int p2, p1, p2

    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionItemGap:I

    if-le p2, v1, :cond_2

    .line 583
    iget p2, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorElementHeight:I

    sub-int/2addr p1, p2

    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    .line 584
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndices:[I

    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoNumberPicker;->decrementSelectorIndices([I)V

    .line 585
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndices:[I

    aget p1, p1, v0

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoNumberPicker;->setValue(I)V

    .line 586
    iget-boolean p1, p0, Lcom/squareup/noho/NohoNumberPicker;->wrapSelectorWheel:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndices:[I

    aget p1, p1, v0

    iget p2, p0, Lcom/squareup/noho/NohoNumberPicker;->minValue:I

    if-gt p1, p2, :cond_1

    .line 587
    iget p1, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    iget p2, p0, Lcom/squareup/noho/NohoNumberPicker;->initialScrollOffset:I

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    goto :goto_0

    .line 590
    :cond_2
    :goto_1
    iget p1, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    iget p2, p0, Lcom/squareup/noho/NohoNumberPicker;->initialScrollOffset:I

    sub-int p2, p1, p2

    iget v1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectionItemGap:I

    neg-int v1, v1

    if-ge p2, v1, :cond_3

    .line 591
    iget p2, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorElementHeight:I

    add-int/2addr p1, p2

    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    .line 592
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndices:[I

    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoNumberPicker;->incrementSelectorIndices([I)V

    .line 593
    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndices:[I

    aget p1, p1, v0

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoNumberPicker;->setValue(I)V

    .line 594
    iget-boolean p1, p0, Lcom/squareup/noho/NohoNumberPicker;->wrapSelectorWheel:Z

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorIndices:[I

    aget p1, p1, v0

    iget p2, p0, Lcom/squareup/noho/NohoNumberPicker;->maxValue:I

    if-lt p1, p2, :cond_2

    .line 595
    iget p1, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    iget p2, p0, Lcom/squareup/noho/NohoNumberPicker;->initialScrollOffset:I

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->currentScrollOffset:I

    goto :goto_1

    :cond_3
    return-void
.end method

.method public setFormatter(Lcom/squareup/noho/NohoNumberPicker$Formatter;)V
    .locals 1

    .line 627
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->formatter:Lcom/squareup/noho/NohoNumberPicker$Formatter;

    if-ne p1, v0, :cond_0

    return-void

    .line 630
    :cond_0
    iput-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->formatter:Lcom/squareup/noho/NohoNumberPicker$Formatter;

    .line 631
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->initializeSelectorWheelIndices()V

    .line 632
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->requestLayout()V

    .line 633
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->invalidate()V

    return-void
.end method

.method public setMaxValue(I)V
    .locals 1

    .line 763
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->maxValue:I

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    if-ltz p1, :cond_2

    .line 769
    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->maxValue:I

    .line 770
    iget p1, p0, Lcom/squareup/noho/NohoNumberPicker;->maxValue:I

    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->value:I

    if-ge p1, v0, :cond_1

    .line 771
    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->value:I

    .line 773
    :cond_1
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->updateWrapSelectorWheel()V

    .line 774
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->initializeSelectorWheelIndices()V

    .line 775
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->tryComputeMaxWidth()V

    .line 776
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->invalidate()V

    return-void

    .line 767
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "maxValue must be >= 0"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setMaxWidth(I)V
    .locals 1

    .line 693
    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->maxWidth:I

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 694
    :goto_0
    iput-boolean p1, p0, Lcom/squareup/noho/NohoNumberPicker;->computeMaxWidth:Z

    .line 695
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->tryComputeMaxWidth()V

    return-void
.end method

.method public setMinValue(I)V
    .locals 1

    .line 740
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->minValue:I

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    if-ltz p1, :cond_2

    .line 746
    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->minValue:I

    .line 747
    iget p1, p0, Lcom/squareup/noho/NohoNumberPicker;->minValue:I

    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->value:I

    if-le p1, v0, :cond_1

    .line 748
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoNumberPicker;->setValue(I)V

    .line 750
    :cond_1
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->updateWrapSelectorWheel()V

    .line 751
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->initializeSelectorWheelIndices()V

    .line 752
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->tryComputeMaxWidth()V

    .line 753
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->invalidate()V

    return-void

    .line 744
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "minValue must be >= 0"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setOnScrollListener(Lcom/squareup/noho/NohoNumberPicker$OnScrollListener;)V
    .locals 0

    .line 622
    iput-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->onScrollListener:Lcom/squareup/noho/NohoNumberPicker$OnScrollListener;

    return-void
.end method

.method public setOnValueChangedListener(Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;)V
    .locals 0

    .line 617
    iput-object p1, p0, Lcom/squareup/noho/NohoNumberPicker;->onValueChangeListener:Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;

    return-void
.end method

.method public setValue(I)V
    .locals 1

    .line 924
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->value:I

    if-ne v0, p1, :cond_0

    return-void

    .line 928
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/noho/NohoNumberPicker;->wrapSelectorWheel:Z

    if-eqz v0, :cond_1

    .line 929
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoNumberPicker;->getWrappedSelectorIndex(I)I

    move-result p1

    goto :goto_0

    .line 931
    :cond_1
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->minValue:I

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 932
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->maxValue:I

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 934
    :goto_0
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->value:I

    .line 935
    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->value:I

    .line 936
    invoke-direct {p0, v0, p1}, Lcom/squareup/noho/NohoNumberPicker;->notifyChange(II)V

    .line 937
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->initializeSelectorWheelIndices()V

    .line 938
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->invalidate()V

    return-void
.end method

.method public setWidestFormattedValue(Ljava/lang/String;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    .line 708
    iput-boolean p1, p0, Lcom/squareup/noho/NohoNumberPicker;->computeMaxWidth:Z

    .line 709
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->tryComputeMaxWidth()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 711
    iput-boolean v0, p0, Lcom/squareup/noho/NohoNumberPicker;->computeMaxWidth:Z

    .line 712
    iget-object v0, p0, Lcom/squareup/noho/NohoNumberPicker;->selectorWheelPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result p1

    float-to-int p1, p1

    .line 714
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getPaddingLeft()I

    move-result v0

    add-int/2addr p1, v0

    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getPaddingRight()I

    move-result v0

    add-int/2addr p1, v0

    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 715
    iget v0, p0, Lcom/squareup/noho/NohoNumberPicker;->maxWidth:I

    if-eq v0, p1, :cond_1

    .line 716
    iput p1, p0, Lcom/squareup/noho/NohoNumberPicker;->maxWidth:I

    .line 717
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->requestLayout()V

    .line 718
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->invalidate()V

    :cond_1
    :goto_0
    return-void
.end method

.method public setWrapSelectorWheel(Z)V
    .locals 0

    .line 670
    iput-boolean p1, p0, Lcom/squareup/noho/NohoNumberPicker;->canWrapSelectorWheel:Z

    .line 671
    invoke-direct {p0}, Lcom/squareup/noho/NohoNumberPicker;->updateWrapSelectorWheel()V

    .line 672
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->requestLayout()V

    .line 673
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNumberPicker;->invalidate()V

    return-void
.end method
