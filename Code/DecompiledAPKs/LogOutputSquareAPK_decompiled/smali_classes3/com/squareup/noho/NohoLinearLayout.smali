.class public Lcom/squareup/noho/NohoLinearLayout;
.super Landroid/widget/LinearLayout;
.source "NohoLinearLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;
    }
.end annotation


# instance fields
.field private final edgePainter:Lcom/squareup/noho/EdgePainter;

.field private selfEdges:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 31
    sget v0, Lcom/squareup/noho/R$attr;->nohoLinearLayoutStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/noho/NohoLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    .line 28
    iput v0, p0, Lcom/squareup/noho/NohoLinearLayout;->selfEdges:I

    .line 36
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoLinearLayout;->setWillNotDraw(Z)V

    .line 38
    sget-object v1, Lcom/squareup/noho/R$styleable;->NohoLinearLayout:[I

    sget v2, Lcom/squareup/noho/R$style;->Widget_Noho_LinearLayout:I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 40
    sget p2, Lcom/squareup/noho/R$styleable;->NohoLinearLayout_sqEdgeWidth:I

    const/4 p3, -0x1

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    .line 41
    sget v1, Lcom/squareup/noho/R$styleable;->NohoLinearLayout_sqEdgeColor:I

    invoke-virtual {p1, v1, p3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 42
    sget v2, Lcom/squareup/noho/R$styleable;->NohoLinearLayout_layout_edges:I

    invoke-virtual {p1, v2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/squareup/noho/NohoLinearLayout;->selfEdges:I

    .line 43
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 45
    invoke-virtual {p0}, Lcom/squareup/noho/NohoLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    if-ne p2, p3, :cond_0

    .line 46
    sget p2, Lcom/squareup/noho/R$dimen;->noho_divider_hairline:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    :cond_0
    if-ne v1, p3, :cond_1

    .line 49
    sget p3, Lcom/squareup/noho/R$color;->noho_divider_hairline:I

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 52
    :cond_1
    new-instance p1, Lcom/squareup/noho/EdgePainter;

    invoke-direct {p1, p0, p2, v1}, Lcom/squareup/noho/EdgePainter;-><init>(Landroid/view/View;II)V

    iput-object p1, p0, Lcom/squareup/noho/NohoLinearLayout;->edgePainter:Lcom/squareup/noho/EdgePainter;

    return-void
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 0

    .line 56
    instance-of p1, p1, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;

    return p1
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .line 72
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 73
    invoke-virtual {p0}, Lcom/squareup/noho/NohoLinearLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    .line 75
    invoke-virtual {p0, v2}, Lcom/squareup/noho/NohoLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 76
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_2

    .line 79
    :cond_0
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;

    .line 80
    iget-object v5, p0, Lcom/squareup/noho/NohoLinearLayout;->edgePainter:Lcom/squareup/noho/EdgePainter;

    invoke-static {v4}, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;->access$000(Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/squareup/noho/EdgePainter;->setEdges(I)V

    .line 82
    invoke-static {v4}, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;->access$100(Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 83
    iget-object v4, p0, Lcom/squareup/noho/NohoLinearLayout;->edgePainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/squareup/noho/EdgePainter;->setHorizontalInsets(II)V

    goto :goto_1

    .line 85
    :cond_1
    iget-object v4, p0, Lcom/squareup/noho/NohoLinearLayout;->edgePainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v4, v1, v1}, Lcom/squareup/noho/EdgePainter;->setHorizontalInsets(II)V

    .line 87
    :goto_1
    iget-object v4, p0, Lcom/squareup/noho/NohoLinearLayout;->edgePainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v4, p1, v3}, Lcom/squareup/noho/EdgePainter;->drawChildEdges(Landroid/graphics/Canvas;Landroid/view/View;)V

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/noho/NohoLinearLayout;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .locals 3

    .line 60
    new-instance v0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;-><init>(II)V

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 0

    .line 26
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoLinearLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 0

    .line 26
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoLinearLayout;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object p1

    return-object p1
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 2

    .line 64
    new-instance v0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 1

    .line 68
    new-instance v0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;

    invoke-direct {v0, p1}, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/noho/NohoLinearLayout;->edgePainter:Lcom/squareup/noho/EdgePainter;

    iget v1, p0, Lcom/squareup/noho/NohoLinearLayout;->selfEdges:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/EdgePainter;->setEdges(I)V

    .line 93
    iget-object v0, p0, Lcom/squareup/noho/NohoLinearLayout;->edgePainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/EdgePainter;->drawEdges(Landroid/graphics/Canvas;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/noho/NohoLinearLayout;->edgePainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v0}, Lcom/squareup/noho/EdgePainter;->clearEdges()V

    .line 95
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method
