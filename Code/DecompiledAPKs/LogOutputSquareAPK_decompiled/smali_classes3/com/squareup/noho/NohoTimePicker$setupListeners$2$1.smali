.class final Lcom/squareup/noho/NohoTimePicker$setupListeners$2$1;
.super Ljava/lang/Object;
.source "NohoTimePicker.kt"

# interfaces
.implements Lcom/squareup/noho/NohoNumberPicker$Formatter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/noho/NohoTimePicker;->setupListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "format"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_with:Lcom/squareup/noho/NohoNumberPicker;


# direct methods
.method constructor <init>(Lcom/squareup/noho/NohoNumberPicker;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/noho/NohoTimePicker$setupListeners$2$1;->$this_with:Lcom/squareup/noho/NohoNumberPicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final format(I)Ljava/lang/String;
    .locals 1

    .line 88
    iget-object p1, p0, Lcom/squareup/noho/NohoTimePicker$setupListeners$2$1;->$this_with:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoNumberPicker;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/noho/R$string;->noho_time_picker_hour_minute_separator:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
