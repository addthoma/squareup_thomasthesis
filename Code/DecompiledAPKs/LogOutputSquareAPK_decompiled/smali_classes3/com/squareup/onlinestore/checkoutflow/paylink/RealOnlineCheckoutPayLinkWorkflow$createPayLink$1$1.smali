.class final Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1$1;
.super Ljava/lang/Object;
.source "RealOnlineCheckoutPayLinkWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1;->apply(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult;",
        "result",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1$1;

    invoke-direct {v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1$1;-><init>()V

    sput-object v0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1$1;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;)Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult;
    .locals 1

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    instance-of v0, p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult$Success;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult$Success;

    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult$Success;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult$Success;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult$Success;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult;

    goto :goto_0

    .line 265
    :cond_0
    sget-object v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult$AlreadyExists;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult$AlreadyExists;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult$AlreadyExistsError;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult$AlreadyExistsError;

    move-object v0, p1

    check-cast v0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult;

    goto :goto_0

    .line 266
    :cond_1
    sget-object v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult$OtherError;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult$OtherError;

    move-object v0, p1

    check-cast v0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult;

    :goto_0
    return-object v0

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 72
    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1$1;->apply(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;)Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult;

    move-result-object p1

    return-object p1
.end method
