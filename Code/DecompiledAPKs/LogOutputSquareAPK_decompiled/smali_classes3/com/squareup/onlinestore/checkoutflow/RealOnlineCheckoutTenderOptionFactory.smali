.class public final Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;
.super Ljava/lang/Object;
.source "RealOnlineCheckoutTenderOptionFactory.kt"

# interfaces
.implements Lcom/squareup/onlinestore/checkoutflow/OnlineCheckoutTenderOptionFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOnlineCheckoutTenderOptionFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOnlineCheckoutTenderOptionFactory.kt\ncom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,185:1\n1577#2,4:186\n1550#2,2:190\n1550#2,3:192\n1552#2:195\n1550#2,3:196\n1550#2,3:199\n*E\n*S KotlinDebug\n*F\n+ 1 RealOnlineCheckoutTenderOptionFactory.kt\ncom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory\n*L\n84#1,4:186\n90#1,2:190\n90#1,3:192\n90#1:195\n98#1,3:196\n110#1,3:199\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000[\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008*\u0001\u0014\u0018\u00002\u00020\u0001:\u0001#B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0008\u0010\u0016\u001a\u00020\u0017H\u0002J\u0008\u0010\u0018\u001a\u00020\u0012H\u0002J\u0008\u0010\u0019\u001a\u00020\u001aH\u0016J\u0008\u0010\u001b\u001a\u00020\u001cH\u0016J\u0008\u0010\u001d\u001a\u00020\u0010H\u0002J\u0008\u0010\u001e\u001a\u00020\u0010H\u0002J\u0008\u0010\u001f\u001a\u00020\u0010H\u0002J\u0008\u0010 \u001a\u00020\u0010H\u0002J\u0008\u0010!\u001a\u00020\u0010H\u0002J\u0008\u0010\"\u001a\u00020\u0010H\u0002R\u001a\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00120\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0015\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;",
        "Lcom/squareup/onlinestore/checkoutflow/OnlineCheckoutTenderOptionFactory;",
        "res",
        "Lcom/squareup/util/Res;",
        "onlineCheckoutBuyLinkWorkflow",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;",
        "onlineCheckoutPayLinkWorkflow",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;",
        "onlineStoreRestrictions",
        "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "(Lcom/squareup/util/Res;Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;Lcom/squareup/payment/Transaction;)V",
        "enabledStrategy",
        "Lkotlin/Function1;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
        "",
        "titleStrategy",
        "",
        "workflow",
        "com/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1",
        "Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;",
        "getCatalogItemCountInCart",
        "",
        "getCheckoutLinkItemId",
        "getTenderOption",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;",
        "getTenderOptionKey",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;",
        "hasCustomItemInCart",
        "hasEcomAvailableItemInCart",
        "hasNonRegularItemInCart",
        "isBuyLinkEnabled",
        "isPayLinkEnabled",
        "isSilentAuthEnabled",
        "OnlineCheckoutTypeState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final enabledStrategy:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final onlineCheckoutBuyLinkWorkflow:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;

.field private final onlineCheckoutPayLinkWorkflow:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;

.field private final onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

.field private final res:Lcom/squareup/util/Res;

.field private final titleStrategy:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final workflow:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;Lcom/squareup/payment/Transaction;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onlineCheckoutBuyLinkWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onlineCheckoutPayLinkWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onlineStoreRestrictions"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->onlineCheckoutBuyLinkWorkflow:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;

    iput-object p3, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->onlineCheckoutPayLinkWorkflow:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;

    iput-object p4, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    iput-object p5, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->transaction:Lcom/squareup/payment/Transaction;

    .line 47
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$titleStrategy$1;

    invoke-direct {p1, p0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$titleStrategy$1;-><init>(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->titleStrategy:Lkotlin/jvm/functions/Function1;

    .line 72
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$enabledStrategy$1;

    invoke-direct {p1, p0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$enabledStrategy$1;-><init>(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->enabledStrategy:Lkotlin/jvm/functions/Function1;

    .line 147
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;

    invoke-direct {p1, p0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;-><init>(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)V

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->workflow:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;

    return-void
.end method

.method public static final synthetic access$getCatalogItemCountInCart(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)I
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->getCatalogItemCountInCart()I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getCheckoutLinkItemId(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Ljava/lang/String;
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->getCheckoutLinkItemId()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getOnlineCheckoutBuyLinkWorkflow$p(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->onlineCheckoutBuyLinkWorkflow:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;

    return-object p0
.end method

.method public static final synthetic access$getOnlineCheckoutPayLinkWorkflow$p(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->onlineCheckoutPayLinkWorkflow:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;

    return-object p0
.end method

.method public static final synthetic access$getOnlineStoreRestrictions$p(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Lcom/squareup/util/Res;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getTransaction$p(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method public static final synthetic access$hasCustomItemInCart(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Z
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->hasCustomItemInCart()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$hasEcomAvailableItemInCart(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Z
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->hasEcomAvailableItemInCart()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$isBuyLinkEnabled(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Z
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->isBuyLinkEnabled()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$isPayLinkEnabled(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Z
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->isPayLinkEnabled()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$isSilentAuthEnabled(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Z
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->isSilentAuthEnabled()Z

    move-result p0

    return p0
.end method

.method private final getCatalogItemCountInCart()I
    .locals 4

    .line 84
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object v0

    const-string/jumbo v1, "transaction.items"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 186
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 188
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    const-string v3, "it"

    .line 84
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->isCustomItem()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    add-int/lit8 v2, v2, 0x1

    if-gez v2, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_0

    :cond_2
    :goto_1
    return v2
.end method

.method private final getCheckoutLinkItemId()Ljava/lang/String;
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getEcomCheckoutLinkItemId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    const-string/jumbo v1, "transaction.ecomCheckoutLinkItemId ?: \"\""

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final hasCustomItemInCart()Z
    .locals 6

    .line 90
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object v0

    const-string/jumbo v1, "transaction.items"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getCartItemCount()I

    move-result v1

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 190
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_3

    .line 191
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    const-string v4, "it"

    .line 91
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->isCustomItem()Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->variations:Ljava/util/List;

    const-string v4, "it.variations"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    .line 192
    instance-of v4, v1, Ljava/util/Collection;

    if-eqz v4, :cond_3

    move-object v4, v1

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 193
    :cond_3
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/OrderVariation;

    const-string/jumbo v5, "v"

    .line 91
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_5

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    :cond_6
    :goto_1
    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_1

    const/4 v3, 0x1

    :cond_7
    :goto_3
    return v3
.end method

.method private final hasEcomAvailableItemInCart()Z
    .locals 5

    .line 108
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object v0

    const-string/jumbo v1, "transaction.items"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 109
    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getCartItemCount()I

    move-result v1

    .line 108
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 199
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 200
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 110
    iget-boolean v4, v1, Lcom/squareup/checkout/CartItem;->isEcomAvailable:Z

    if-eqz v4, :cond_2

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->merchantCatalogObjectToken:Ljava/lang/String;

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const/4 v3, 0x1

    :cond_3
    :goto_1
    return v3
.end method

.method private final hasNonRegularItemInCart()Z
    .locals 5

    .line 98
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object v0

    const-string/jumbo v1, "transaction.items"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getCartItemCount()I

    move-result v1

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 196
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 197
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    const-string v4, "it"

    .line 99
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v1

    sget-object v4, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    if-eq v1, v4, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const/4 v3, 0x1

    :cond_3
    :goto_1
    return v3
.end method

.method private final isBuyLinkEnabled()Z
    .locals 5

    .line 113
    invoke-direct {p0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->getCatalogItemCountInCart()I

    move-result v0

    .line 114
    invoke-direct {p0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->hasCustomItemInCart()Z

    move-result v1

    .line 115
    invoke-direct {p0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->hasEcomAvailableItemInCart()Z

    move-result v2

    .line 116
    invoke-direct {p0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->hasNonRegularItemInCart()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v0, v4, :cond_0

    if-nez v3, :cond_0

    if-nez v1, :cond_0

    if-nez v2, :cond_1

    .line 118
    invoke-direct {p0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->isSilentAuthEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :cond_1
    :goto_0
    return v4
.end method

.method private final isPayLinkEnabled()Z
    .locals 5

    .line 125
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    invoke-interface {v0}, Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;->isOnlineCheckoutPayLinksEnabled()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCartItemCount()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 126
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object v0

    const-string/jumbo v3, "transaction.items"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    .line 127
    iget-object v3, v0, Lcom/squareup/checkout/CartItem;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    sget-object v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_AMOUNT:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    if-ne v3, v4, :cond_0

    sget-object v3, Lcom/squareup/onlinestore/common/PayLinkUtil;->INSTANCE:Lcom/squareup/onlinestore/common/PayLinkUtil;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string/jumbo v4, "total()"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Lcom/squareup/onlinestore/common/PayLinkUtil;->isAmountInRange(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private final isSilentAuthEnabled()Z
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    invoke-interface {v0}, Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;->isBuyLinksWithSilentAuthEnabled()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getTenderOption()Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;
    .locals 5

    .line 179
    new-instance v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;

    .line 180
    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONLINE_CHECKOUT:Lcom/squareup/analytics/RegisterTapName;

    iget-object v2, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->titleStrategy:Lkotlin/jvm/functions/Function1;

    iget-object v3, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->enabledStrategy:Lkotlin/jvm/functions/Function1;

    iget-object v4, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->workflow:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;

    check-cast v4, Lcom/squareup/workflow/Workflow;

    .line 179
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;-><init>(Lcom/squareup/analytics/RegisterTapName;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/Workflow;)V

    return-object v0
.end method

.method public getTenderOptionKey()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;
    .locals 3

    .line 183
    new-instance v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "javaClass.name"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
