.class public final Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;
.super Ljava/lang/Object;
.source "RealCheckoutLinksRepository.kt"

# interfaces
.implements Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0096\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u000e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u0016J\u001e\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000e2\u0006\u0010\u0012\u001a\u00020\u000c2\u0006\u0010\u0013\u001a\u00020\u000cH\u0016J&\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000e2\u0006\u0010\u0012\u001a\u00020\u000c2\u0006\u0010\u0013\u001a\u00020\u000c2\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J(\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000e2\u0006\u0010\u0012\u001a\u00020\u000c2\u0006\u0010\u0013\u001a\u00020\u000c2\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0002J\u001e\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u000e2\u0006\u0010\u0012\u001a\u00020\u000c2\u0006\u0010\u001a\u001a\u00020\u000cH\u0016J\u001e\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u000e2\u0006\u0010\u0012\u001a\u00020\u000c2\u0006\u0010\u001c\u001a\u00020\u000cH\u0016J\u000e\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001e0\u000eH\u0016J2\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020 0\u000e2\u0006\u0010\u0012\u001a\u00020\u000c2\u0008\u0008\u0001\u0010!\u001a\u00020\"2\u0008\u0008\u0001\u0010#\u001a\u00020\"2\u0006\u0010$\u001a\u00020\u0019H\u0016J\u000e\u0010%\u001a\u0008\u0012\u0004\u0012\u00020&0\u000eH\u0016J\u0016\u0010\'\u001a\u00020\u000f2\u000c\u0010(\u001a\u0008\u0012\u0004\u0012\u00020*0)H\u0002J\u001e\u0010+\u001a\u0008\u0012\u0004\u0012\u00020,0\u000e2\u0006\u0010\u0012\u001a\u00020\u000c2\u0006\u0010-\u001a\u00020.H\u0016J\u0012\u0010/\u001a\u00020\u0011*\u0008\u0012\u0004\u0012\u00020100H\u0002J\u0012\u00102\u001a\u00020\u001e*\u0008\u0012\u0004\u0012\u00020300H\u0002J\u0012\u00104\u001a\u00020,*\u0008\u0012\u0004\u0012\u00020100H\u0002J\u000c\u00105\u001a\u00020\u000c*\u00020\u000cH\u0002J\u0014\u00106\u001a\u000207*\u00020.2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00068"
    }
    d2 = {
        "Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "weeblySquareSyncService",
        "Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;",
        "multiPassAuthService",
        "Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;Lcom/squareup/protos/common/CurrencyCode;)V",
        "merchantToken",
        "",
        "checkCheckoutLinksEnabled",
        "Lio/reactivex/Single;",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;",
        "createDonationLink",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;",
        "jwtToken",
        "name",
        "createPayLink",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "createPayLinkHelper",
        "deletePayLink",
        "",
        "payLinkId",
        "enableEcomAvailableForItem",
        "merchantCatalogObjectToken",
        "getJwtTokenForSquareSync",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;",
        "getPayLinks",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;",
        "page",
        "",
        "perPageCount",
        "includeDonations",
        "merchantExists",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult;",
        "processMerchantTokenResponse",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/onlinestore/api/squaresync/MerchantSquareSyncStatusResponse;",
        "updatePayLink",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;",
        "payLink",
        "Lcom/squareup/onlinestore/repository/PayLink;",
        "processCreatePayLinkError",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;",
        "processJwtTokenError",
        "Lcom/squareup/onlinestore/api/squaresync/MultiPassAuthResponse;",
        "processUpdatePayLinkError",
        "toBearerToken",
        "toPayLinkReqBody",
        "Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final merchantToken:Ljava/lang/String;

.field private final multiPassAuthService:Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;

.field private final weeblySquareSyncService:Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountStatusSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "weeblySquareSyncService"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "multiPassAuthService"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->weeblySquareSyncService:Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;

    iput-object p3, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->multiPassAuthService:Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;

    iput-object p4, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 48
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p1

    const-string p2, "accountStatusSettings.userSettings"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/settings/server/UserSettings;->getMerchantToken()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->merchantToken:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$getMerchantToken$p(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;)Ljava/lang/String;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->merchantToken:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getWeeblySquareSyncService$p(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;)Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->weeblySquareSyncService:Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;

    return-object p0
.end method

.method public static final synthetic access$processCreatePayLinkError(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->processCreatePayLinkError(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$processJwtTokenError(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->processJwtTokenError(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$processMerchantTokenResponse(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->processMerchantTokenResponse(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$processUpdatePayLinkError(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->processUpdatePayLinkError(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;

    move-result-object p0

    return-object p0
.end method

.method private final createPayLinkHelper(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lio/reactivex/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;",
            ">;"
        }
    .end annotation

    .line 145
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->merchantToken:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 146
    new-instance v0, Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;

    if-eqz p3, :cond_0

    .line 148
    iget-object v1, p3, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v1}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object v1

    :goto_0
    move-object v3, v1

    if-eqz p3, :cond_1

    .line 149
    iget-object v1, p3, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    move-object v4, v1

    const/4 v5, 0x1

    .line 151
    iget-object v6, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->merchantToken:Ljava/lang/String;

    if-nez p3, :cond_2

    const/4 p3, 0x1

    const/4 v7, 0x1

    goto :goto_2

    :cond_2
    const/4 p3, 0x0

    const/4 v7, 0x0

    :goto_2
    move-object v1, v0

    move-object v2, p2

    .line 146
    invoke-direct/range {v1 .. v7}, Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZLjava/lang/String;Z)V

    .line 154
    iget-object p2, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->weeblySquareSyncService:Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->toBearerToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p1, v0}, Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;->createPayLink(Ljava/lang/String;Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 155
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 156
    invoke-virtual {p1}, Lio/reactivex/Single;->toMaybe()Lio/reactivex/Maybe;

    move-result-object p1

    .line 157
    new-instance p2, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$createPayLinkHelper$1;

    invoke-direct {p2, p0}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$createPayLinkHelper$1;-><init>(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    .line 163
    sget-object p2, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult$OtherError;

    invoke-virtual {p1, p2}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string/jumbo p2, "weeblySquareSyncService.\u2026    .toSingle(OtherError)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 145
    :cond_3
    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult$OtherError;

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.just(OtherError)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final processCreatePayLinkError(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;",
            ">;)",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;"
        }
    .end annotation

    .line 168
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getStatusCode()I

    move-result p1

    const/16 v0, 0x1a6

    if-ne p1, v0, :cond_1

    .line 169
    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult$AlreadyExists;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult$AlreadyExists;

    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;

    goto :goto_0

    .line 171
    :cond_1
    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult$OtherError;

    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;

    :goto_0
    return-object p1
.end method

.method private final processJwtTokenError(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/onlinestore/api/squaresync/MultiPassAuthResponse;",
            ">;)",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;"
        }
    .end annotation

    .line 105
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getStatusCode()I

    move-result p1

    const/16 v0, 0x199

    if-ne p1, v0, :cond_1

    .line 106
    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$UnableToEnableSos;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$UnableToEnableSos;

    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;

    goto :goto_0

    .line 108
    :cond_1
    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$OtherError;

    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;

    :goto_0
    return-object p1
.end method

.method private final processMerchantTokenResponse(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/onlinestore/api/squaresync/MerchantSquareSyncStatusResponse;",
            ">;)",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;"
        }
    .end annotation

    .line 64
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 65
    new-instance v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult$EnabledStatus;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/onlinestore/api/squaresync/MerchantSquareSyncStatusResponse;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/MerchantSquareSyncStatusResponse;->getInitialSyncFinished()Z

    move-result p1

    invoke-direct {v0, p1}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult$EnabledStatus;-><init>(Z)V

    check-cast v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;

    goto :goto_1

    .line 67
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_3

    .line 68
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 69
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    instance-of p1, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz p1, :cond_2

    :goto_0
    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult$NetworkError;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult$NetworkError;

    move-object v0, p1

    check-cast v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;

    goto :goto_1

    .line 70
    :cond_2
    new-instance p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult$EnabledStatus;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult$EnabledStatus;-><init>(Z)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;

    :goto_1
    return-object v0

    .line 68
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final processUpdatePayLinkError(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;",
            ">;)",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;"
        }
    .end annotation

    .line 219
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getStatusCode()I

    move-result p1

    const/16 v0, 0x1a6

    if-ne p1, v0, :cond_1

    .line 220
    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$AlreadyExists;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$AlreadyExists;

    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;

    goto :goto_0

    .line 222
    :cond_1
    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$OtherError;

    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;

    :goto_0
    return-object p1
.end method

.method private final toBearerToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 329
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Bearer "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final toPayLinkReqBody(Lcom/squareup/onlinestore/repository/PayLink;Ljava/lang/String;)Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;
    .locals 8

    .line 332
    instance-of v0, p1, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;

    .line 333
    invoke-virtual {p1}, Lcom/squareup/onlinestore/repository/PayLink;->getName()Ljava/lang/String;

    move-result-object v2

    .line 334
    move-object v1, p1

    check-cast v1, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;

    invoke-virtual {v1}, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v3}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object v3

    .line 335
    invoke-virtual {v1}, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v4, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 336
    invoke-virtual {p1}, Lcom/squareup/onlinestore/repository/PayLink;->getEnabled()Z

    move-result v5

    const/4 v7, 0x0

    move-object v1, v0

    move-object v6, p2

    .line 332
    invoke-direct/range {v1 .. v7}, Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZLjava/lang/String;Z)V

    goto :goto_0

    .line 340
    :cond_0
    instance-of v0, p1, Lcom/squareup/onlinestore/repository/PayLink$Donation;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;

    .line 341
    invoke-virtual {p1}, Lcom/squareup/onlinestore/repository/PayLink;->getName()Ljava/lang/String;

    move-result-object v2

    .line 342
    iget-object v1, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v1}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 344
    invoke-virtual {p1}, Lcom/squareup/onlinestore/repository/PayLink;->getEnabled()Z

    move-result v5

    const/4 v7, 0x1

    move-object v1, v0

    move-object v6, p2

    .line 340
    invoke-direct/range {v1 .. v7}, Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZLjava/lang/String;Z)V

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public checkCheckoutLinksEnabled()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;",
            ">;"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->merchantToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 54
    iget-object v1, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->weeblySquareSyncService:Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;

    invoke-interface {v1, v0}, Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;->getMerchant(Ljava/lang/String;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 56
    new-instance v1, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$checkCheckoutLinksEnabled$1;

    invoke-direct {v1, p0}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$checkCheckoutLinksEnabled$1;-><init>(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string/jumbo v1, "weeblySquareSyncService.\u2026kenResponse(it)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 53
    :cond_0
    sget-object v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult$NetworkError;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult$NetworkError;

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "Single.just(NetworkError)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public createDonationLink(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;",
            ">;"
        }
    .end annotation

    const-string v0, "jwtToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 135
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->createPayLinkHelper(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public createPayLink(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;",
            ">;"
        }
    .end annotation

    const-string v0, "jwtToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->createPayLinkHelper(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public deletePayLink(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "jwtToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "payLinkId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->merchantToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 231
    new-instance v1, Lcom/squareup/onlinestore/api/squaresync/DeletePayLinkReqBody;

    invoke-direct {v1, p2, v0}, Lcom/squareup/onlinestore/api/squaresync/DeletePayLinkReqBody;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->weeblySquareSyncService:Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->toBearerToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2, v1}, Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;->deletePayLink(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/onlinestore/api/squaresync/DeletePayLinkReqBody;)Lio/reactivex/Single;

    move-result-object p1

    .line 233
    sget-object p2, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$deletePayLink$1;->INSTANCE:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$deletePayLink$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 234
    sget-object p2, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$deletePayLink$2;->INSTANCE:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$deletePayLink$2;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string/jumbo p2, "weeblySquareSyncService.\u2026 .onErrorReturn { false }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_0
    const/4 p1, 0x0

    .line 230
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.just(false)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public enableEcomAvailableForItem(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "jwtToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "merchantCatalogObjectToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->merchantToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->weeblySquareSyncService:Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;

    .line 119
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->toBearerToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 121
    new-instance v1, Lcom/squareup/onlinestore/api/squaresync/EnableEcomAvailableRequestBody;

    iget-object v2, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->merchantToken:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/squareup/onlinestore/api/squaresync/EnableEcomAvailableRequestBody;-><init>(Ljava/lang/String;)V

    .line 118
    invoke-interface {v0, p1, p2, v1}, Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;->syncItemWithWeebly(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/onlinestore/api/squaresync/EnableEcomAvailableRequestBody;)Lio/reactivex/Single;

    move-result-object p1

    .line 123
    sget-object p2, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$enableEcomAvailableForItem$1;->INSTANCE:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$enableEcomAvailableForItem$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string/jumbo p2, "weeblySquareSyncService\n\u2026.map { it.code() == 204 }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_0
    const/4 p1, 0x0

    .line 116
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.just(false)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public getJwtTokenForSquareSync()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;",
            ">;"
        }
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->merchantToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->multiPassAuthService:Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;

    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;->createIdentityTokenFromSession(Ljava/util/Map;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lio/reactivex/Single;->toMaybe()Lio/reactivex/Maybe;

    move-result-object v0

    .line 80
    new-instance v1, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getJwtTokenForSquareSync$1;

    invoke-direct {v1, p0}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getJwtTokenForSquareSync$1;-><init>(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 87
    new-instance v1, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getJwtTokenForSquareSync$2;

    invoke-direct {v1, p0}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getJwtTokenForSquareSync$2;-><init>(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 92
    new-instance v1, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getJwtTokenForSquareSync$3;

    invoke-direct {v1, p0}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getJwtTokenForSquareSync$3;-><init>(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 98
    sget-object v1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$OtherError;

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "multiPassAuthService.cre\u2026wtTokenResult.OtherError)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 76
    :cond_0
    sget-object v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$OtherError;

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "Single.just(SquareSyncJwtTokenResult.OtherError)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getPayLinks(Ljava/lang/String;IIZ)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IIZ)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;",
            ">;"
        }
    .end annotation

    const-string v0, "jwtToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->merchantToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->weeblySquareSyncService:Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;

    .line 246
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->toBearerToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 247
    iget-object v1, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->merchantToken:Ljava/lang/String;

    .line 245
    invoke-interface {v0, p1, v1, p2, p3}, Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;->getPayLinks(Ljava/lang/String;Ljava/lang/String;II)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 251
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 252
    new-instance p2, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getPayLinks$1;

    invoke-direct {p2, p4}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getPayLinks$1;-><init>(Z)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string/jumbo p2, "weeblySquareSyncService\n\u2026ror\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 243
    :cond_0
    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Error;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Error;

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.just(PayLinksResult.Error)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public merchantExists()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult;",
            ">;"
        }
    .end annotation

    .line 295
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->merchantToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->multiPassAuthService:Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;

    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;->createIdentityTokenFromSession(Ljava/util/Map;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 297
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 298
    invoke-virtual {v0}, Lio/reactivex/Single;->toMaybe()Lio/reactivex/Maybe;

    move-result-object v0

    .line 299
    sget-object v1, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$1;->INSTANCE:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 306
    new-instance v1, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$2;

    invoke-direct {v1, p0}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$2;-><init>(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 311
    sget-object v1, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$3;->INSTANCE:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$merchantExists$3;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 326
    sget-object v1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult$Error;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult$Error;

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "multiPassAuthService.cre\u2026rchantExistsResult.Error)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 295
    :cond_0
    sget-object v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult$Error;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult$Error;

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "Single.just(MerchantExistsResult.Error)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public updatePayLink(Ljava/lang/String;Lcom/squareup/onlinestore/repository/PayLink;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/onlinestore/repository/PayLink;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;",
            ">;"
        }
    .end annotation

    const-string v0, "jwtToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "payLink"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->merchantToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 180
    invoke-direct {p0, p2, v0}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->toPayLinkReqBody(Lcom/squareup/onlinestore/repository/PayLink;Ljava/lang/String;)Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;

    move-result-object v0

    .line 181
    iget-object v1, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->weeblySquareSyncService:Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->toBearerToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/onlinestore/repository/PayLink;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v2, v0}, Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;->updatePayLink(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/onlinestore/api/squaresync/PayLinkReqBody;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 182
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 183
    invoke-virtual {p1}, Lio/reactivex/Single;->toMaybe()Lio/reactivex/Maybe;

    move-result-object p1

    .line 184
    new-instance v0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$updatePayLink$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$updatePayLink$1;-><init>(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;Lcom/squareup/onlinestore/repository/PayLink;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    .line 214
    sget-object p2, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$OtherError;

    invoke-virtual {p1, p2}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string/jumbo p2, "weeblySquareSyncService.\u2026PayLinkResult.OtherError)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 179
    :cond_0
    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$OtherError;

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.just(UpdatePayLinkResult.OtherError)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
