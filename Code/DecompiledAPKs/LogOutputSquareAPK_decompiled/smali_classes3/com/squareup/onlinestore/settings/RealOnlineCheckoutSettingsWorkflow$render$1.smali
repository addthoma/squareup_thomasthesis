.class final Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOnlineCheckoutSettingsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow;->render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lcom/squareup/workflow/WorkflowAction;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "",
        "",
        "it",
        "invoke",
        "(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow$render$1;->this$0:Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow$render$1;->this$0:Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow;

    new-instance v1, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow$render$1$1;

    invoke-direct {v1, p1}, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow$render$1$1;-><init>(Lkotlin/Unit;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, p1, v1, v2, p1}, Lcom/squareup/workflow/StatelessWorkflowKt;->action$default(Lcom/squareup/workflow/StatelessWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow$render$1;->invoke(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
