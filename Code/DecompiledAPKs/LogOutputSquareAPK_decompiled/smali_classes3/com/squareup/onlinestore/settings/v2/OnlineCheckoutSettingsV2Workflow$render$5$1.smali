.class final Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OnlineCheckoutSettingsV2Workflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5;->invoke(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $it:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;

.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5$1;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5$1;->$it:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 94
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;",
            "-",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5$1;->$it:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;

    .line 207
    instance-of v0, v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult$Success;

    if-eqz v0, :cond_1

    .line 208
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5$1;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5;

    iget-object v0, v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5;->$state:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    move-object v1, v0

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;

    .line 209
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5$1;->$it:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult$Success;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult$Success;->getCheckoutLinks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5$1;->$it:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult$Success;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult$Success;->getHasMore()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 210
    :goto_0
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5$1;->$it:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult$Success;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult$Success;->getHasMore()Z

    move-result v4

    .line 211
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5$1;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5;

    iget-object v0, v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5;->$state:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;->getList()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5$1;->$it:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;

    check-cast v2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult$Success;

    invoke-virtual {v2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult$Success;->getCheckoutLinks()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v2

    .line 212
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5$1;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5;

    iget v3, v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5;->$nextPage:I

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    .line 208
    invoke-static/range {v1 .. v8}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;->copy$default(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;Ljava/util/List;IZZLcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo;ILjava/lang/Object;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;

    move-result-object v0

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    goto :goto_1

    .line 216
    :cond_1
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5$1;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5;

    iget-object v0, v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5;->$state:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    move-object v1, v0

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x13

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;->copy$default(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;Ljava/util/List;IZZLcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo;ILjava/lang/Object;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;

    move-result-object v0

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    .line 206
    :goto_1
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
