.class public final Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "OnlineCheckoutSettingsV2Workflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnlineCheckoutSettingsV2Workflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnlineCheckoutSettingsV2Workflow.kt\ncom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 6 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 7 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,715:1\n32#2,12:716\n85#3:728\n85#3:760\n85#3:763\n85#3:766\n85#3:769\n240#4:729\n240#4:761\n240#4:764\n240#4:767\n240#4:770\n276#5:730\n276#5:762\n276#5:765\n276#5:768\n276#5:771\n149#6,5:731\n149#6,5:736\n149#6,5:741\n149#6,5:746\n149#6,5:751\n1360#7:756\n1429#7,3:757\n*E\n*S KotlinDebug\n*F\n+ 1 OnlineCheckoutSettingsV2Workflow.kt\ncom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow\n*L\n150#1,12:716\n202#1:728\n660#1:760\n686#1:763\n119#1:766\n121#1:769\n202#1:729\n660#1:761\n686#1:764\n119#1:767\n121#1:770\n202#1:730\n660#1:762\n686#1:765\n119#1:768\n121#1:771\n286#1,5:731\n342#1,5:736\n463#1,5:741\n523#1,5:746\n526#1,5:751\n623#1:756\n623#1,3:757\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00e2\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 [2<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0002\u0012&\u0012$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00080\u0001:\u0002[\\B_\u0008\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u001c\u0012\u0006\u0010\u001d\u001a\u00020\u001e\u00a2\u0006\u0002\u0010\u001fJ\u0016\u0010-\u001a\u0008\u0012\u0004\u0012\u00020.0!2\u0006\u0010/\u001a\u000200H\u0002J \u00101\u001a\u0010\u0012\u000c\u0012\n 3*\u0004\u0018\u00010\"0\"022\u0008\u0008\u0002\u00104\u001a\u000205H\u0002J\u0010\u00106\u001a\u0002002\u0006\u0010/\u001a\u000200H\u0002J\u001f\u00107\u001a\u00020\u00032\u0006\u00108\u001a\u00020\u00022\u0008\u00109\u001a\u0004\u0018\u00010:H\u0016\u00a2\u0006\u0002\u0010;J\u0018\u0010<\u001a\u00020\u00022\u0006\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u00020.H\u0002J\u0010\u0010@\u001a\u00020\u00022\u0006\u0010=\u001a\u00020>H\u0002J\u0018\u0010A\u001a\u00020\u00022\u0006\u0010B\u001a\u00020C2\u0006\u0010D\u001a\u000200H\u0002J \u0010E\u001a\u00020\u00022\u0006\u0010B\u001a\u00020C2\u0006\u0010D\u001a\u0002002\u0006\u0010F\u001a\u00020.H\u0002JF\u0010G\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010H\u001a\u00020I2\u0012\u0010B\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00020JH\u0002J\u0010\u0010K\u001a\u00020\"2\u0006\u0010L\u001a\u00020MH\u0002JS\u0010N\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u00108\u001a\u00020\u00022\u0006\u0010H\u001a\u00020\u00032\u0012\u0010B\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00020JH\u0016\u00a2\u0006\u0002\u0010OJ\u0010\u0010P\u001a\u00020:2\u0006\u0010H\u001a\u00020\u0003H\u0016J\u001e\u0010Q\u001a\u0008\u0012\u0004\u0012\u00020.0!2\u0006\u0010=\u001a\u00020>2\u0006\u0010R\u001a\u00020.H\u0002JB\u0010S\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0008*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00020J2\u0006\u0010=\u001a\u00020>H\u0002J\u0014\u0010T\u001a\u00020U*\u00020>2\u0006\u0010V\u001a\u00020.H\u0002J.\u0010W\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0008*\u00020XH\u0002J.\u0010Y\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0008*\u00020ZH\u0002R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\"0!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00020$X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\"0!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\'0!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\'0!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010)\u001a\u0008\u0012\u0004\u0012\u00020\'0!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010*\u001a\u0008\u0012\u0004\u0012\u00020\'0!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010+\u001a\u0008\u0012\u0004\u0012\u00020\'0!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010,\u001a\u0008\u0012\u0004\u0012\u00020\'0!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006]"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "server",
        "Lcom/squareup/http/Server;",
        "res",
        "Lcom/squareup/util/Res;",
        "clipboard",
        "Lcom/squareup/util/Clipboard;",
        "toastFactory",
        "Lcom/squareup/util/ToastFactory;",
        "qrCodeGenerator",
        "Lcom/squareup/qrcodegenerator/QrCodeGenerator;",
        "checkoutLinkShareSheet",
        "Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;",
        "checkoutLinksRepository",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;",
        "createLinkWorkflow",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;",
        "editLinkWorkflow",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;",
        "analytics",
        "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
        "onlineStoreRestrictions",
        "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
        "(Lcom/squareup/http/Server;Lcom/squareup/util/Res;Lcom/squareup/util/Clipboard;Lcom/squareup/util/ToastFactory;Lcom/squareup/qrcodegenerator/QrCodeGenerator;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)V",
        "checkSosAndFetchLinksWorker",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;",
        "exitAction",
        "Lcom/squareup/workflow/WorkflowAction;",
        "fetchLinksWorker",
        "logAccountIneligibleError",
        "",
        "logActionsWorker",
        "logFetchingAccountStatusError",
        "logViewDonationLink",
        "logViewLinkList",
        "logViewSharePayLink",
        "deleteLink",
        "",
        "payLinkId",
        "",
        "fetchPayLinks",
        "Lio/reactivex/Single;",
        "kotlin.jvm.PlatformType",
        "page",
        "",
        "getCheckoutLinkUrl",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;",
        "logActivation",
        "checkoutLink",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLink;",
        "activate",
        "logDeleteEvent",
        "onClickCheckoutLinkUrl",
        "context",
        "Landroid/content/Context;",
        "checkoutLinkUrl",
        "onClickShareButton",
        "isDonationLink",
        "onErrorState",
        "state",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorState;",
        "Lcom/squareup/workflow/RenderContext;",
        "processPayLinksResult",
        "payLinksResult",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;",
        "render",
        "(Lkotlin/Unit;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "snapshotState",
        "updateLinkEnabled",
        "enabled",
        "renderCheckoutLinkScreen",
        "toPayLink",
        "Lcom/squareup/onlinestore/repository/PayLink;",
        "enabledStatus",
        "toPosBodyLayer",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;",
        "toPosDialogLayer",
        "Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData;",
        "Companion",
        "FetchCheckoutLinksResult",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CHECKOUT_LINK_LIST_WORKER_KEY:Ljava/lang/String; = "checkout-link-list-worker-key"

.field public static final Companion:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$Companion;

.field public static final INITIAL_LOADING_WORKER_KEY:Ljava/lang/String; = "initial-loading-worker-key"

.field public static final LOG_ACTIONS_WORKER_KEY:Ljava/lang/String; = "log-actions-worker-key"

.field public static final LOG_INELIGIBLE_KEY:Ljava/lang/String; = "log-ineligible-worker-key"

.field public static final LOG_LINK_LIST_ERROR_KEY:Ljava/lang/String; = "log-link-list-error-worker-key"

.field public static final LOG_VIEW_LINK_LIST_KEY:Ljava/lang/String; = "log-view-link-list-worker-key"

.field private static final PAY_LINKS_PER_PAGE_COUNT:I = 0xa

.field public static final UPDATE_LINK_STATUS_WORKER_KEY:Ljava/lang/String; = "update-link-status-worker-key"


# instance fields
.field private final analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

.field private final checkSosAndFetchLinksWorker:Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutLinkShareSheet:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;

.field private final checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

.field private final clipboard:Lcom/squareup/util/Clipboard;

.field private final createLinkWorkflow:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;

.field private final editLinkWorkflow:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;

.field private final exitAction:Lcom/squareup/workflow/WorkflowAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final fetchLinksWorker:Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;",
            ">;"
        }
    .end annotation
.end field

.field private final logAccountIneligibleError:Lcom/squareup/workflow/Worker;

.field private final logActionsWorker:Lcom/squareup/workflow/Worker;

.field private final logFetchingAccountStatusError:Lcom/squareup/workflow/Worker;

.field private final logViewDonationLink:Lcom/squareup/workflow/Worker;

.field private final logViewLinkList:Lcom/squareup/workflow/Worker;

.field private final logViewSharePayLink:Lcom/squareup/workflow/Worker;

.field private final onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

.field private final qrCodeGenerator:Lcom/squareup/qrcodegenerator/QrCodeGenerator;

.field private final res:Lcom/squareup/util/Res;

.field private final server:Lcom/squareup/http/Server;

.field private final toastFactory:Lcom/squareup/util/ToastFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->Companion:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/http/Server;Lcom/squareup/util/Res;Lcom/squareup/util/Clipboard;Lcom/squareup/util/ToastFactory;Lcom/squareup/qrcodegenerator/QrCodeGenerator;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "server"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clipboard"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "toastFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "qrCodeGenerator"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkoutLinkShareSheet"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkoutLinksRepository"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "createLinkWorkflow"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editLinkWorkflow"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onlineStoreRestrictions"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->server:Lcom/squareup/http/Server;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->clipboard:Lcom/squareup/util/Clipboard;

    iput-object p4, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->toastFactory:Lcom/squareup/util/ToastFactory;

    iput-object p5, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->qrCodeGenerator:Lcom/squareup/qrcodegenerator/QrCodeGenerator;

    iput-object p6, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->checkoutLinkShareSheet:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;

    iput-object p7, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    iput-object p8, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->createLinkWorkflow:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;

    iput-object p9, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->editLinkWorkflow:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;

    iput-object p10, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    iput-object p11, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    .line 108
    sget-object p1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$exitAction$1;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$exitAction$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    const/4 p2, 0x1

    const/4 p3, 0x0

    invoke-static {p0, p3, p1, p2, p3}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->exitAction:Lcom/squareup/workflow/WorkflowAction;

    .line 109
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    invoke-interface {p1}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;->merchantExists()Lio/reactivex/Single;

    move-result-object p1

    .line 110
    new-instance p4, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$checkSosAndFetchLinksWorker$1;

    invoke-direct {p4, p0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$checkSosAndFetchLinksWorker$1;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;)V

    check-cast p4, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p4}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p4, "checkoutLinksRepository.\u2026.Error)\n        }\n      }"

    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 766
    sget-object p4, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p4, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$$special$$inlined$asWorker$1;

    invoke-direct {p4, p1, p3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$$special$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p4, Lkotlin/jvm/functions/Function1;

    .line 767
    invoke-static {p4}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 768
    const-class p4, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;

    invoke-static {p4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p4

    new-instance p5, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p5, p4, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p5, Lcom/squareup/workflow/Worker;

    .line 766
    iput-object p5, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->checkSosAndFetchLinksWorker:Lcom/squareup/workflow/Worker;

    const/4 p1, 0x0

    .line 121
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->fetchPayLinks$default(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;IILjava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    .line 769
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$$special$$inlined$asWorker$2;

    invoke-direct {p2, p1, p3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$$special$$inlined$asWorker$2;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 770
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 771
    const-class p2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance p4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p4, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p4, Lcom/squareup/workflow/Worker;

    .line 769
    iput-object p4, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->fetchLinksWorker:Lcom/squareup/workflow/Worker;

    .line 123
    sget-object p1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$logActionsWorker$1;

    invoke-direct {p2, p0, p3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$logActionsWorker$1;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->logActionsWorker:Lcom/squareup/workflow/Worker;

    .line 127
    sget-object p1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$logViewSharePayLink$1;

    invoke-direct {p2, p0, p3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$logViewSharePayLink$1;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->logViewSharePayLink:Lcom/squareup/workflow/Worker;

    .line 131
    sget-object p1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$logViewDonationLink$1;

    invoke-direct {p2, p0, p3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$logViewDonationLink$1;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->logViewDonationLink:Lcom/squareup/workflow/Worker;

    .line 135
    sget-object p1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$logViewLinkList$1;

    invoke-direct {p2, p0, p3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$logViewLinkList$1;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->logViewLinkList:Lcom/squareup/workflow/Worker;

    .line 139
    sget-object p1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$logFetchingAccountStatusError$1;

    invoke-direct {p2, p0, p3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$logFetchingAccountStatusError$1;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->logFetchingAccountStatusError:Lcom/squareup/workflow/Worker;

    .line 143
    sget-object p1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$logAccountIneligibleError$1;

    invoke-direct {p2, p0, p3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$logAccountIneligibleError$1;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->logAccountIneligibleError:Lcom/squareup/workflow/Worker;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;)Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;
    .locals 0

    .line 94
    iget-object p0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getCheckoutLinksRepository$p(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;
    .locals 0

    .line 94
    iget-object p0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    return-object p0
.end method

.method public static final synthetic access$getExitAction$p(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 94
    iget-object p0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->exitAction:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method

.method public static final synthetic access$getOnlineStoreRestrictions$p(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;)Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;
    .locals 0

    .line 94
    iget-object p0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    return-object p0
.end method

.method public static final synthetic access$logActivation(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/onlinestore/settings/v2/CheckoutLink;Z)V
    .locals 0

    .line 94
    invoke-direct {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->logActivation(Lcom/squareup/onlinestore/settings/v2/CheckoutLink;Z)V

    return-void
.end method

.method public static final synthetic access$logDeleteEvent(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/onlinestore/settings/v2/CheckoutLink;)V
    .locals 0

    .line 94
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->logDeleteEvent(Lcom/squareup/onlinestore/settings/v2/CheckoutLink;)V

    return-void
.end method

.method public static final synthetic access$onClickCheckoutLinkUrl(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 94
    invoke-direct {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->onClickCheckoutLinkUrl(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$onClickShareButton(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 0

    .line 94
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->onClickShareButton(Landroid/content/Context;Ljava/lang/String;Z)V

    return-void
.end method

.method public static final synthetic access$processPayLinksResult(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;
    .locals 0

    .line 94
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->processPayLinksResult(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toPayLink(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/onlinestore/settings/v2/CheckoutLink;Z)Lcom/squareup/onlinestore/repository/PayLink;
    .locals 0

    .line 94
    invoke-direct {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->toPayLink(Lcom/squareup/onlinestore/settings/v2/CheckoutLink;Z)Lcom/squareup/onlinestore/repository/PayLink;

    move-result-object p0

    return-object p0
.end method

.method private final deleteLink(Ljava/lang/String;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 677
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    invoke-interface {v0}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;->getJwtTokenForSquareSync()Lio/reactivex/Single;

    move-result-object v0

    .line 678
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$deleteLink$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$deleteLink$1;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "checkoutLinksRepository.\u2026se)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 763
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$deleteLink$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$deleteLink$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 764
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 765
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1
.end method

.method private final fetchPayLinks(I)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;",
            ">;"
        }
    .end annotation

    .line 590
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    invoke-interface {v0}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;->getJwtTokenForSquareSync()Lio/reactivex/Single;

    move-result-object v0

    .line 591
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$fetchPayLinks$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$fetchPayLinks$1;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;I)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "checkoutLinksRepository.\u2026      }\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method static synthetic fetchPayLinks$default(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;IILjava/lang/Object;)Lio/reactivex/Single;
    .locals 0

    const/4 p3, 0x1

    and-int/2addr p2, p3

    if-eqz p2, :cond_0

    const/4 p1, 0x1

    .line 590
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->fetchPayLinks(I)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final getCheckoutLinkUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 568
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->server:Lcom/squareup/http/Server;

    invoke-virtual {v0}, Lcom/squareup/http/Server;->isProduction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569
    sget v0, Lcom/squareup/onlinestore/settings/impl/R$string;->paylink_url:I

    goto :goto_0

    .line 571
    :cond_0
    sget v0, Lcom/squareup/onlinestore/settings/impl/R$string;->paylink_url_staging:I

    .line 574
    :goto_0
    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 575
    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "id"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 576
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 577
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final logActivation(Lcom/squareup/onlinestore/settings/v2/CheckoutLink;Z)V
    .locals 1

    .line 416
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    .line 418
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    new-instance p2, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;

    sget-object v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->ACTIVATE:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    invoke-direct {p2, v0}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;)V

    check-cast p2, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {p1, p2}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    goto :goto_0

    .line 420
    :cond_0
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    new-instance p2, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;

    sget-object v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->DEACTIVATE:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    invoke-direct {p2, v0}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;)V

    check-cast p2, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {p1, p2}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    goto :goto_0

    .line 423
    :cond_1
    instance-of p1, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$DonationLink;

    if-eqz p1, :cond_3

    if-eqz p2, :cond_2

    .line 425
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    new-instance p2, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;

    sget-object v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->ACTIVATE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    invoke-direct {p2, v0}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;)V

    check-cast p2, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {p1, p2}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    goto :goto_0

    .line 427
    :cond_2
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    new-instance p2, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;

    sget-object v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->DEACTIVATE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    invoke-direct {p2, v0}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;)V

    check-cast p2, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {p1, p2}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private final logDeleteEvent(Lcom/squareup/onlinestore/settings/v2/CheckoutLink;)V
    .locals 2

    .line 582
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    new-instance v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;

    sget-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->DELETE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;)V

    check-cast v0, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {p1, v0}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    goto :goto_0

    .line 583
    :cond_0
    instance-of p1, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$DonationLink;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    new-instance v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;

    sget-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->DELETE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;)V

    check-cast v0, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {p1, v0}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final onClickCheckoutLinkUrl(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .line 558
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->clipboard:Lcom/squareup/util/Clipboard;

    .line 560
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->checkoutlink_clipboard_label:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 561
    check-cast p2, Ljava/lang/CharSequence;

    .line 558
    invoke-interface {v0, p1, v1, p2}, Lcom/squareup/util/Clipboard;->copyPlainText(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 564
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->toastFactory:Lcom/squareup/util/ToastFactory;

    sget p2, Lcom/squareup/onlinestore/settings/impl/R$string;->checkoutlink_copied_toast_message:I

    const/4 v0, 0x0

    invoke-interface {p1, p2, v0}, Lcom/squareup/util/ToastFactory;->showText(II)V

    return-void
.end method

.method private final onClickShareButton(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 9

    const-string v0, "context.getString(R.string.share_sheet_subject)"

    const-string v1, "context.getString(R.string.share_sheet_title)"

    if-eqz p3, :cond_0

    .line 534
    iget-object p3, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    new-instance v2, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;

    sget-object v3, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->SHARE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    invoke-direct {v2, v3}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;)V

    check-cast v2, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {p3, v2}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    .line 535
    iget-object v3, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->checkoutLinkShareSheet:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;

    .line 537
    sget p3, Lcom/squareup/onlinestore/settings/impl/R$string;->share_sheet_title:I

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 538
    sget p3, Lcom/squareup/onlinestore/settings/impl/R$string;->share_sheet_subject:I

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 540
    sget-object v8, Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;->ONLINE_CHECKOUT_SETTINGS_DONATION_LINK:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;

    move-object v4, p1

    move-object v7, p2

    .line 535
    invoke-interface/range {v3 .. v8}, Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;->openShareSheetForCheckoutLink(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;)V

    goto :goto_0

    .line 543
    :cond_0
    iget-object p3, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    new-instance v2, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;

    sget-object v3, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->SHARE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    invoke-direct {v2, v3}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;)V

    check-cast v2, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {p3, v2}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    .line 544
    iget-object v3, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->checkoutLinkShareSheet:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;

    .line 546
    sget p3, Lcom/squareup/onlinestore/settings/impl/R$string;->share_sheet_title:I

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 547
    sget p3, Lcom/squareup/onlinestore/settings/impl/R$string;->share_sheet_subject:I

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 549
    sget-object v8, Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;->ONLINE_CHECKOUT_SETTINGS_CUSTOM_AMOUNT_LINK:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;

    move-object v4, p1

    move-object v7, p2

    .line 544
    invoke-interface/range {v3 .. v8}, Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;->openShareSheetForCheckoutLink(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;)V

    :goto_0
    return-void
.end method

.method private final onErrorState(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 471
    invoke-interface {p2}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 472
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorState;->getErrorKind()Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind;

    move-result-object v1

    .line 473
    sget-object v2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind$InitialLoadError;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind$InitialLoadError;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "log-link-list-error-worker-key"

    if-eqz v2, :cond_0

    .line 474
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->logFetchingAccountStatusError:Lcom/squareup/workflow/Worker;

    invoke-static {p2, p1, v3}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 475
    new-instance p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$ErrorScreenData;

    .line 476
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$1;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 479
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$2;

    invoke-direct {v0, p0, p2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$2;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 475
    invoke-direct {p1, v1, v0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$ErrorScreenData;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;

    .line 480
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->toPosBodyLayer(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 482
    :cond_0
    sget-object v2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind$LoadCheckoutLinksError;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind$LoadCheckoutLinksError;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 483
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->logFetchingAccountStatusError:Lcom/squareup/workflow/Worker;

    invoke-static {p2, p1, v3}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 484
    new-instance p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$ErrorScreenData;

    .line 485
    new-instance p2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$3;

    invoke-direct {p2, p0, v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$3;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 490
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$4;

    invoke-direct {v1, p0, v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$4;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 484
    invoke-direct {p1, p2, v1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$ErrorScreenData;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;

    .line 491
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->toPosBodyLayer(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 493
    :cond_1
    instance-of p2, v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind$UpdateEnabledError;

    if-eqz p2, :cond_2

    new-instance p2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$ErrorScreenData;

    .line 494
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$5;

    invoke-direct {v1, p0, v0, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$5;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorState;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 502
    new-instance p1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$6;

    invoke-direct {p1, p0, v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$6;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    .line 493
    invoke-direct {p2, v1, p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$ErrorScreenData;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;

    .line 507
    invoke-direct {p0, p2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->toPosBodyLayer(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 508
    :cond_2
    instance-of p2, v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind$DeleteError;

    if-eqz p2, :cond_3

    new-instance p2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$ErrorScreenData;

    .line 509
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$7;

    invoke-direct {v1, p0, v0, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$7;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorState;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 514
    new-instance p1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$8;

    invoke-direct {p1, p0, v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$8;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    .line 508
    invoke-direct {p2, v1, p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$ErrorScreenData;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;

    .line 519
    invoke-direct {p0, p2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->toPosBodyLayer(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final processPayLinksResult(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;
    .locals 7

    .line 621
    instance-of v0, p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;

    if-eqz v0, :cond_3

    .line 622
    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->getPayLinks()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 756
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 757
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 758
    check-cast v2, Lcom/squareup/onlinestore/repository/PayLink;

    .line 625
    instance-of v3, v2, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;

    if-eqz v3, :cond_0

    .line 626
    new-instance v3, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;

    invoke-virtual {v2}, Lcom/squareup/onlinestore/repository/PayLink;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/squareup/onlinestore/repository/PayLink;->getName()Ljava/lang/String;

    move-result-object v5

    move-object v6, v2

    check-cast v6, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;

    invoke-virtual {v6}, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-virtual {v2}, Lcom/squareup/onlinestore/repository/PayLink;->getEnabled()Z

    move-result v2

    invoke-direct {v3, v4, v5, v6, v2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Z)V

    check-cast v3, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    goto :goto_1

    .line 628
    :cond_0
    instance-of v3, v2, Lcom/squareup/onlinestore/repository/PayLink$Donation;

    if-eqz v3, :cond_1

    .line 629
    new-instance v3, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$DonationLink;

    invoke-virtual {v2}, Lcom/squareup/onlinestore/repository/PayLink;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/squareup/onlinestore/repository/PayLink;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/squareup/onlinestore/repository/PayLink;->getEnabled()Z

    move-result v2

    invoke-direct {v3, v4, v5, v2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$DonationLink;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    check-cast v3, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    .line 631
    :goto_1
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 629
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 759
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 633
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 635
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult$Success;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->getCurrentPage()I

    move-result v2

    invoke-virtual {p1}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;->getHasMore()Z

    move-result p1

    invoke-direct {v1, v0, v2, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult$Success;-><init>(Ljava/util/List;IZ)V

    .line 634
    check-cast v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;

    goto :goto_2

    .line 638
    :cond_3
    instance-of p1, p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Error;

    if-eqz p1, :cond_4

    sget-object p1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult$Error;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult$Error;

    move-object v1, p1

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;

    :goto_2
    return-object v1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final renderCheckoutLinkScreen(Lcom/squareup/workflow/RenderContext;Lcom/squareup/onlinestore/settings/v2/CheckoutLink;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;",
            "-",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/onlinestore/settings/v2/CheckoutLink;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 436
    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->getCheckoutLinkUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 438
    instance-of v0, p2, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$CustomAmountLinkInfo;

    .line 439
    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getName()Ljava/lang/String;

    move-result-object v1

    move-object v2, p2

    check-cast v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;

    invoke-virtual {v2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getEnabled()Z

    move-result v3

    .line 438
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$CustomAmountLinkInfo;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Z)V

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo;

    :goto_0
    move-object v2, v0

    goto :goto_1

    .line 441
    :cond_0
    instance-of v0, p2, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$DonationLink;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$DonationLinkInfo;

    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getEnabled()Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo$DonationLinkInfo;-><init>(Ljava/lang/String;Z)V

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo;

    goto :goto_0

    .line 444
    :goto_1
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;

    .line 447
    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->qrCodeGenerator:Lcom/squareup/qrcodegenerator/QrCodeGenerator;

    .line 448
    iget-object v3, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/onlinestore/settings/impl/R$dimen;->qr_code_size:I

    invoke-interface {v3, v5}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v3

    .line 447
    invoke-interface {v1, v4, v3}, Lcom/squareup/qrcodegenerator/QrCodeGenerator;->generate(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 450
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$renderCheckoutLinkScreen$1;

    invoke-direct {v1, p0, v4, p2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$renderCheckoutLinkScreen$1;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Ljava/lang/String;Lcom/squareup/onlinestore/settings/v2/CheckoutLink;)V

    move-object v6, v1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 453
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$renderCheckoutLinkScreen$2;

    invoke-direct {v1, p0, v4}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$renderCheckoutLinkScreen$2;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Ljava/lang/String;)V

    move-object v5, v1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 454
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$renderCheckoutLinkScreen$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$renderCheckoutLinkScreen$3;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/onlinestore/settings/v2/CheckoutLink;)V

    move-object v7, v1

    check-cast v7, Lkotlin/jvm/functions/Function0;

    .line 458
    new-instance p2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$renderCheckoutLinkScreen$4;

    invoke-direct {p2, p0, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$renderCheckoutLinkScreen$4;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/RenderContext;)V

    move-object v8, p2

    check-cast v8, Lkotlin/jvm/functions/Function0;

    move-object v1, v0

    .line 444
    invoke-direct/range {v1 .. v8}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;-><init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkInfo;Landroid/graphics/Bitmap;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 742
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 743
    const-class p2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenData;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 744
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 742
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 464
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 441
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final toPayLink(Lcom/squareup/onlinestore/settings/v2/CheckoutLink;Z)Lcom/squareup/onlinestore/repository/PayLink;
    .locals 3

    .line 663
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;

    .line 664
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getId()Ljava/lang/String;

    move-result-object v1

    .line 665
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getName()Ljava/lang/String;

    move-result-object v2

    .line 666
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 663
    invoke-direct {v0, v1, v2, p1, p2}, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Z)V

    check-cast v0, Lcom/squareup/onlinestore/repository/PayLink;

    goto :goto_0

    .line 669
    :cond_0
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$DonationLink;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/onlinestore/repository/PayLink$Donation;

    .line 670
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getId()Ljava/lang/String;

    move-result-object v1

    .line 671
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getName()Ljava/lang/String;

    move-result-object p1

    .line 669
    invoke-direct {v0, v1, p1, p2}, Lcom/squareup/onlinestore/repository/PayLink$Donation;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    check-cast v0, Lcom/squareup/onlinestore/repository/PayLink;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final toPosBodyLayer(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 523
    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 747
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    .line 748
    const-class v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 749
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 747
    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 523
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v0, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final toPosDialogLayer(Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 526
    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 752
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    .line 753
    const-class v1, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 754
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 752
    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 526
    sget-object p1, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {v0, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final updateLinkEnabled(Lcom/squareup/onlinestore/settings/v2/CheckoutLink;Z)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/settings/v2/CheckoutLink;",
            "Z)",
            "Lcom/squareup/workflow/Worker<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 644
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    invoke-interface {v0}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;->getJwtTokenForSquareSync()Lio/reactivex/Single;

    move-result-object v0

    .line 645
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$updateLinkEnabled$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$updateLinkEnabled$1;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/onlinestore/settings/v2/CheckoutLink;Z)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 654
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$updateLinkEnabled$2;

    invoke-direct {v0, p2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$updateLinkEnabled$2;-><init>(Z)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "checkoutLinksRepository.\u2026> false\n        }\n      }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 760
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$updateLinkEnabled$$inlined$asWorker$1;

    const/4 v0, 0x0

    invoke-direct {p2, p1, v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$updateLinkEnabled$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 761
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 762
    sget-object p2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 716
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 721
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 722
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 723
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 724
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 725
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 726
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 727
    :cond_3
    check-cast v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 150
    :cond_4
    sget-object p1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$InitialLoadingState;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$InitialLoadingState;

    move-object v1, p1

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 94
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 94
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->render(Lkotlin/Unit;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "props"

    move-object/from16 v4, p1

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "state"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "context"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    invoke-interface/range {p3 .. p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v9

    .line 159
    sget-object v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$InitialLoadingState;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$InitialLoadingState;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x0

    if-eqz v3, :cond_0

    .line 160
    iget-object v1, v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->checkSosAndFetchLinksWorker:Lcom/squareup/workflow/Worker;

    new-instance v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$1;

    invoke-direct {v3, v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$1;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const-string v7, "initial-loading-worker-key"

    invoke-interface {v2, v1, v7, v3}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 177
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;

    new-instance v2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$2;

    invoke-direct {v2, v0, v9}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$2;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-direct {v1, v2, v5, v4, v6}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;-><init>(Lkotlin/jvm/functions/Function0;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->toPosBodyLayer(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_2

    .line 180
    :cond_0
    instance-of v3, v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$LoadCheckoutLinkListState;

    const-string v7, "checkout-link-list-worker-key"

    if-eqz v3, :cond_1

    .line 181
    iget-object v3, v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->fetchLinksWorker:Lcom/squareup/workflow/Worker;

    new-instance v8, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$3;

    invoke-direct {v8, v0, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$3;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)V

    check-cast v8, Lkotlin/jvm/functions/Function1;

    invoke-interface {v2, v3, v7, v8}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 195
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;

    new-instance v2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$4;

    invoke-direct {v2, v0, v9}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$4;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-direct {v1, v2, v5, v4, v6}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;-><init>(Lkotlin/jvm/functions/Function0;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->toPosBodyLayer(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_2

    .line 198
    :cond_1
    instance-of v3, v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;

    const/4 v10, 0x1

    if-eqz v3, :cond_3

    .line 199
    move-object v3, v1

    check-cast v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;

    invoke-virtual {v3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;->getLoadNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 200
    invoke-virtual {v3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;->getCurrentPage()I

    move-result v4

    add-int/2addr v4, v10

    .line 202
    invoke-direct {v0, v4}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->fetchPayLinks(I)Lio/reactivex/Single;

    move-result-object v5

    .line 728
    sget-object v8, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v8, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$$inlined$asWorker$1;

    invoke-direct {v8, v5, v6}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 729
    invoke-static {v8}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v5

    .line 730
    const-class v6, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$FetchCheckoutLinksResult;

    invoke-static {v6}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v6

    new-instance v8, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v8, v6, v5}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v8, Lcom/squareup/workflow/Worker;

    .line 203
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 204
    new-instance v6, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5;

    invoke-direct {v6, v0, v1, v4}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$5;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;I)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 201
    invoke-interface {v2, v8, v5, v6}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 223
    :cond_2
    iget-object v4, v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->logViewLinkList:Lcom/squareup/workflow/Worker;

    const-string v5, "log-view-link-list-worker-key"

    invoke-static {v2, v4, v5}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 226
    :goto_0
    new-instance v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;

    .line 227
    invoke-virtual {v3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;->getList()Ljava/util/List;

    move-result-object v11

    .line 228
    invoke-virtual {v3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;->getLinkUpdatedInfo()Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo;

    move-result-object v12

    .line 229
    invoke-virtual {v3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;->getHasNext()Z

    move-result v13

    .line 230
    invoke-virtual {v3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;->getLoadNext()Z

    move-result v14

    .line 231
    new-instance v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$6;

    invoke-direct {v3, v0, v1, v9}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$6;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;Lcom/squareup/workflow/Sink;)V

    move-object v15, v3

    check-cast v15, Lkotlin/jvm/functions/Function0;

    .line 238
    new-instance v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$7;

    invoke-direct {v3, v0, v9}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$7;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;)V

    move-object/from16 v16, v3

    check-cast v16, Lkotlin/jvm/functions/Function0;

    .line 241
    new-instance v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$8;

    invoke-direct {v3, v0, v9, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$8;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)V

    move-object/from16 v17, v3

    check-cast v17, Lkotlin/jvm/functions/Function1;

    .line 244
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$9;

    invoke-direct {v1, v0, v9}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$9;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;)V

    move-object/from16 v18, v1

    check-cast v18, Lkotlin/jvm/functions/Function0;

    move-object v10, v2

    .line 226
    invoke-direct/range {v10 .. v18}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;-><init>(Ljava/util/List;Lcom/squareup/onlinestore/settings/v2/LinkUpdatedInfo;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;

    .line 245
    invoke-direct {v0, v2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->toPosBodyLayer(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_2

    .line 248
    :cond_3
    sget-object v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$FeatureNotAvailableState;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$FeatureNotAvailableState;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 249
    iget-object v1, v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->logAccountIneligibleError:Lcom/squareup/workflow/Worker;

    const-string v3, "log-ineligible-worker-key"

    invoke-static {v2, v1, v3}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 250
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$FeatureNotAvailableScreenData;

    .line 251
    new-instance v2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$10;

    invoke-direct {v2, v0, v9}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$10;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 250
    invoke-direct {v1, v2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$FeatureNotAvailableScreenData;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;

    .line 252
    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->toPosBodyLayer(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_2

    .line 255
    :cond_4
    instance-of v3, v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;

    const-string v11, ""

    if-eqz v3, :cond_6

    .line 256
    iget-object v3, v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->createLinkWorkflow:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;

    check-cast v3, Lcom/squareup/workflow/Workflow;

    sget-object v4, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    const/4 v5, 0x0

    new-instance v6, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$createLinkScreen$1;

    invoke-direct {v6, v0, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$createLinkScreen$1;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object/from16 v2, p3

    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 272
    check-cast v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;

    invoke-virtual {v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;->isSosEnabled()Z

    move-result v1

    if-eqz v1, :cond_5

    move-object v1, v2

    goto/16 :goto_2

    .line 283
    :cond_5
    sget-object v1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    new-instance v3, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;

    .line 284
    new-instance v4, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$11;

    invoke-direct {v4, v0, v9}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$11;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 283
    invoke-direct {v3, v4, v10}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;-><init>(Lkotlin/jvm/functions/Function0;Z)V

    check-cast v3, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;

    check-cast v3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 732
    new-instance v4, Lcom/squareup/workflow/legacy/Screen;

    .line 733
    const-class v5, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v5

    invoke-static {v5, v11}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    .line 734
    sget-object v6, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v6}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v6

    .line 732
    invoke-direct {v4, v5, v3, v6}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 283
    invoke-static {v1, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    .line 282
    invoke-static {v1}, Lkotlin/collections/MapsKt;->mapOf(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v1

    invoke-static {v2, v1}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_2

    .line 291
    :cond_6
    instance-of v3, v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$EditLinkState;

    if-eqz v3, :cond_7

    .line 292
    iget-object v3, v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->editLinkWorkflow:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;

    check-cast v3, Lcom/squareup/workflow/Workflow;

    new-instance v4, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInput;

    move-object v5, v1

    check-cast v5, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$EditLinkState;

    invoke-virtual {v5}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$EditLinkState;->getCheckoutLink()Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInput;-><init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLink;)V

    const/4 v5, 0x0

    new-instance v6, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$12;

    invoke-direct {v6, v0, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$12;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object/from16 v1, p3

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move v6, v7

    move-object v7, v8

    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    goto/16 :goto_2

    .line 302
    :cond_7
    instance-of v3, v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$NewCheckoutLinkState;

    if-eqz v3, :cond_a

    .line 303
    check-cast v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$NewCheckoutLinkState;

    invoke-virtual {v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$NewCheckoutLinkState;->getCheckoutLink()Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    move-result-object v3

    .line 304
    instance-of v4, v3, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;

    if-eqz v4, :cond_8

    .line 305
    iget-object v3, v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->logViewSharePayLink:Lcom/squareup/workflow/Worker;

    invoke-virtual {v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$NewCheckoutLinkState;->getCheckoutLink()Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getId()Ljava/lang/String;

    move-result-object v4

    .line 304
    invoke-static {v2, v3, v4}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    goto :goto_1

    .line 307
    :cond_8
    instance-of v3, v3, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$DonationLink;

    if-eqz v3, :cond_9

    .line 308
    iget-object v3, v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->logViewDonationLink:Lcom/squareup/workflow/Worker;

    invoke-virtual {v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$NewCheckoutLinkState;->getCheckoutLink()Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getId()Ljava/lang/String;

    move-result-object v4

    .line 307
    invoke-static {v2, v3, v4}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 311
    :cond_9
    :goto_1
    invoke-virtual {v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$NewCheckoutLinkState;->getCheckoutLink()Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->renderCheckoutLinkScreen(Lcom/squareup/workflow/RenderContext;Lcom/squareup/onlinestore/settings/v2/CheckoutLink;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_2

    .line 314
    :cond_a
    instance-of v3, v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkState;

    if-eqz v3, :cond_b

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkState;

    invoke-virtual {v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkState;->getCheckoutLink()Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->renderCheckoutLinkScreen(Lcom/squareup/workflow/RenderContext;Lcom/squareup/onlinestore/settings/v2/CheckoutLink;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_2

    .line 316
    :cond_b
    instance-of v3, v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ActionBottomSheetDialogState;

    if-eqz v3, :cond_c

    .line 317
    iget-object v3, v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->logActionsWorker:Lcom/squareup/workflow/Worker;

    const-string v4, "log-actions-worker-key"

    invoke-static {v2, v3, v4}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 318
    new-instance v2, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;

    .line 319
    move-object v3, v1

    check-cast v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ActionBottomSheetDialogState;

    invoke-virtual {v3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ActionBottomSheetDialogState;->getCheckoutLink()Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getEnabled()Z

    move-result v13

    .line 320
    new-instance v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$13;

    invoke-direct {v3, v0, v9, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$13;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)V

    move-object v14, v3

    check-cast v14, Lkotlin/jvm/functions/Function0;

    .line 326
    new-instance v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$14;

    invoke-direct {v3, v0, v9, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$14;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)V

    move-object v15, v3

    check-cast v15, Lkotlin/jvm/functions/Function1;

    .line 334
    new-instance v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$15;

    invoke-direct {v3, v0, v9, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$15;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)V

    move-object/from16 v16, v3

    check-cast v16, Lkotlin/jvm/functions/Function0;

    .line 339
    new-instance v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$16;

    invoke-direct {v3, v0, v9, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$16;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)V

    move-object/from16 v17, v3

    check-cast v17, Lkotlin/jvm/functions/Function0;

    move-object v12, v2

    .line 318
    invoke-direct/range {v12 .. v17}, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;-><init>(ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 737
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 738
    const-class v3, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v11}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 739
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 737
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 343
    sget-object v2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_2

    .line 346
    :cond_c
    instance-of v3, v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledConfirmationState;

    if-eqz v3, :cond_d

    new-instance v2, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeactivateLinkConfirmationDialogScreenData;

    .line 347
    move-object v3, v1

    check-cast v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledConfirmationState;

    invoke-virtual {v3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledConfirmationState;->getCheckoutLink()Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getEnabled()Z

    move-result v3

    .line 348
    new-instance v4, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$17;

    invoke-direct {v4, v0, v9, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$17;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 353
    new-instance v5, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$18;

    invoke-direct {v5, v0, v9, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$18;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 346
    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeactivateLinkConfirmationDialogScreenData;-><init>(ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData;

    .line 356
    invoke-direct {v0, v2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->toPosDialogLayer(Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_2

    .line 358
    :cond_d
    instance-of v3, v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;

    if-eqz v3, :cond_e

    .line 360
    move-object v3, v1

    check-cast v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;

    invoke-virtual {v3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->getCheckoutLink()Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    move-result-object v7

    invoke-virtual {v3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;->getNewEnabledState()Z

    move-result v3

    invoke-direct {v0, v7, v3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->updateLinkEnabled(Lcom/squareup/onlinestore/settings/v2/CheckoutLink;Z)Lcom/squareup/workflow/Worker;

    move-result-object v3

    .line 362
    new-instance v7, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;

    invoke-direct {v7, v0, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$19;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)V

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const-string/jumbo v8, "update-link-status-worker-key"

    .line 359
    invoke-interface {v2, v3, v8, v7}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 375
    new-instance v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;

    new-instance v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$20;

    invoke-direct {v3, v0, v9, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$20;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-direct {v2, v3, v5, v4, v6}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;-><init>(Lkotlin/jvm/functions/Function0;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;

    .line 377
    invoke-direct {v0, v2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->toPosBodyLayer(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;)Ljava/util/Map;

    move-result-object v1

    goto :goto_2

    .line 380
    :cond_e
    instance-of v3, v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$DeleteCheckoutLinkConfirmationState;

    if-eqz v3, :cond_f

    new-instance v2, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeleteLinkConfirmationDialogScreenData;

    .line 381
    new-instance v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$21;

    invoke-direct {v3, v0, v9, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$21;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 384
    new-instance v4, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$22;

    invoke-direct {v4, v0, v9, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$22;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 380
    invoke-direct {v2, v3, v4}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeleteLinkConfirmationDialogScreenData;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData;

    .line 387
    invoke-direct {v0, v2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->toPosDialogLayer(Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData;)Ljava/util/Map;

    move-result-object v1

    goto :goto_2

    .line 389
    :cond_f
    instance-of v3, v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$DeleteCheckoutLinkState;

    if-eqz v3, :cond_10

    .line 390
    move-object v3, v1

    check-cast v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$DeleteCheckoutLinkState;

    invoke-virtual {v3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$DeleteCheckoutLinkState;->getCheckoutLink()Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getId()Ljava/lang/String;

    move-result-object v3

    .line 391
    invoke-direct {v0, v3}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->deleteLink(Ljava/lang/String;)Lcom/squareup/workflow/Worker;

    move-result-object v7

    new-instance v8, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$23;

    invoke-direct {v8, v0, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$23;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)V

    check-cast v8, Lkotlin/jvm/functions/Function1;

    invoke-interface {v2, v7, v3, v8}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 402
    new-instance v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;

    new-instance v3, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$24;

    invoke-direct {v3, v0, v9, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$render$24;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-direct {v2, v3, v5, v4, v6}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$LoadingScreenData;-><init>(Lkotlin/jvm/functions/Function0;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;

    .line 404
    invoke-direct {v0, v2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->toPosBodyLayer(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData;)Ljava/util/Map;

    move-result-object v1

    goto :goto_2

    .line 407
    :cond_10
    instance-of v3, v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorState;

    if-eqz v3, :cond_11

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorState;

    invoke-direct {v0, v1, v2}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->onErrorState(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object v1

    :goto_2
    return-object v1

    :cond_11
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public snapshotState(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 701
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 94
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->snapshotState(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
