.class public final Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "CreateLinkWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult;,
        Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCreateLinkWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CreateLinkWorkflow.kt\ncom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 4 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 5 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 6 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,193:1\n32#2,12:194\n149#3,5:206\n85#4:211\n240#5:212\n276#6:213\n*E\n*S KotlinDebug\n*F\n+ 1 CreateLinkWorkflow.kt\ncom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow\n*L\n57#1,12:194\n139#1,5:206\n179#1:211\n179#1:212\n179#1:213\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000o\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0005*\u0001\u0010\u0018\u0000 &2<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0002&\'B\u0017\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0016\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u00132\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u001e\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u00172\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u001f\u0010\u001b\u001a\u00020\u00032\u0006\u0010\u001c\u001a\u00020\u00022\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016\u00a2\u0006\u0002\u0010\u001fJS\u0010 \u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u001c\u001a\u00020\u00022\u0006\u0010!\u001a\u00020\u00032\u0012\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040#H\u0016\u00a2\u0006\u0002\u0010$J\u0010\u0010%\u001a\u00020\u001e2\u0006\u0010!\u001a\u00020\u0003H\u0016R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0011\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "checkoutLinksRepository",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;",
        "analytics",
        "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
        "(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;)V",
        "logCreatePayWorker",
        "com/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$logCreatePayWorker$1",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$logCreatePayWorker$1;",
        "createLink",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult;",
        "createLinkInfo",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;",
        "Lio/reactivex/Single;",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;",
        "jwt",
        "",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "snapshotState",
        "Companion",
        "CreatePayLinkWorkerResult",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATE_PAY_LINK_WORKER_KEY:Ljava/lang/String; = "create-pay-link-worker-key"

.field public static final Companion:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$Companion;

.field public static final LOG_CREATE_PAY_WORKER_KEY:Ljava/lang/String; = "log-create-pay-worker-key"


# instance fields
.field private final analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

.field private final checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

.field private final logCreatePayWorker:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$logCreatePayWorker$1;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->Companion:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "checkoutLinksRepository"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    .line 48
    new-instance p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$logCreatePayWorker$1;

    invoke-direct {p1, p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$logCreatePayWorker$1;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;)V

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->logCreatePayWorker:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$logCreatePayWorker$1;

    return-void
.end method

.method public static final synthetic access$createLink(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;Ljava/lang/String;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;)Lio/reactivex/Single;
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->createLink(Ljava/lang/String;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;)Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    return-object p0
.end method

.method private final createLink(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult;",
            ">;"
        }
    .end annotation

    .line 159
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    invoke-interface {v0}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;->getJwtTokenForSquareSync()Lio/reactivex/Single;

    move-result-object v0

    .line 160
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$createLink$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$createLink$1;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "checkoutLinksRepository.\u2026      }\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$createLink$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$createLink$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 212
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 213
    const-class v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$CreatePayLinkWorkerResult;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1
.end method

.method private final createLink(Ljava/lang/String;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;",
            ">;"
        }
    .end annotation

    .line 149
    instance-of v0, p2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateCustomAmountLinkInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    .line 150
    check-cast p2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateCustomAmountLinkInfo;

    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateCustomAmountLinkInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateCustomAmountLinkInfo;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 149
    invoke-interface {v0, p1, v1, p2}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;->createPayLink(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 152
    :cond_0
    instance-of v0, p2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateDonationLinkInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    .line 153
    check-cast p2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateDonationLinkInfo;

    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateDonationLinkInfo;->getName()Ljava/lang/String;

    move-result-object p2

    .line 152
    invoke-interface {v0, p1, p2}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;->createDonationLink(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    if-eqz p2, :cond_4

    .line 194
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, p1

    :goto_1
    if-eqz p2, :cond_3

    .line 199
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 201
    array-length v2, p2

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 202
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 203
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p2

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    goto :goto_2

    :cond_3
    move-object p2, p1

    .line 205
    :goto_2
    check-cast p2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;

    if-eqz p2, :cond_4

    goto :goto_3

    .line 57
    :cond_4
    new-instance p2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$CreateLinkState;

    invoke-direct {p2, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$CreateLinkState;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;)V

    check-cast p2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;

    :goto_3
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->render(Lkotlin/Unit;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;",
            "-",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    instance-of p1, p2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$CreateLinkState;

    if-eqz p1, :cond_0

    .line 66
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->logCreatePayWorker:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$logCreatePayWorker$1;

    check-cast p1, Lcom/squareup/workflow/Worker;

    const-string v0, "log-create-pay-worker-key"

    invoke-static {p3, p1, v0}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 67
    new-instance p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$CreateLinkScreenData;

    .line 68
    check-cast p2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$CreateLinkState;

    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$CreateLinkState;->getCreateLinkInfo()Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;

    move-result-object p2

    .line 69
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$1;

    invoke-direct {v0, p0, p3}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$1;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 72
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$2;

    invoke-direct {v1, p0, p3}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$2;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 67
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$CreateLinkScreenData;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData;

    goto :goto_0

    .line 77
    :cond_0
    instance-of p1, p2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$LinkAlreadyExistsErrorState;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;

    .line 78
    check-cast p2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$LinkAlreadyExistsErrorState;

    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$LinkAlreadyExistsErrorState;->getCreateLinkInfo()Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;

    move-result-object p2

    .line 79
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$3;

    invoke-direct {v0, p0, p3}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$3;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 82
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$4;

    invoke-direct {v1, p0, p3}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$4;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 77
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData;

    goto :goto_0

    .line 86
    :cond_1
    instance-of p1, p2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$LinkCreationFailedErrorState;

    if-eqz p1, :cond_2

    new-instance p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkCreationFailedErrorScreenData;

    .line 87
    check-cast p2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$LinkCreationFailedErrorState;

    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$LinkCreationFailedErrorState;->getCreateLinkInfo()Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;

    move-result-object p2

    .line 88
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$5;

    invoke-direct {v0, p0, p3}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$5;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 91
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$6;

    invoke-direct {v1, p0, p3}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$6;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 86
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkCreationFailedErrorScreenData;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData;

    goto :goto_0

    .line 95
    :cond_2
    instance-of p1, p2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$SaveLinkState;

    if-eqz p1, :cond_3

    .line 96
    move-object p1, p2

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$SaveLinkState;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState$SaveLinkState;->getCreateLinkInfo()Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->createLink(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;

    invoke-direct {v0, p0, p2}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$7;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const-string v1, "create-pay-link-worker-key"

    invoke-interface {p3, p1, v1, v0}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 129
    new-instance p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$SaveLinkScreenData;

    .line 130
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$8;

    invoke-direct {v0, p0, p3, p2}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$8;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 129
    invoke-direct {p1, v0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$SaveLinkScreenData;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData;

    .line 139
    :goto_0
    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 207
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 208
    const-class p3, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    const-string v0, ""

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 209
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 207
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 140
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 129
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->snapshotState(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
