.class public final Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory;
.super Lcom/squareup/workflow/CompoundWorkflowViewFactory;
.source "RealOnlineCheckoutSettingsWorkflowViewFactory.kt"

# interfaces
.implements Lcom/squareup/onlinestore/settings/OnlineCheckoutSettingsWorkflowViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u000f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory;",
        "Lcom/squareup/onlinestore/settings/OnlineCheckoutSettingsWorkflowViewFactory;",
        "Lcom/squareup/workflow/CompoundWorkflowViewFactory;",
        "onlineCheckoutSettingsV2WorkflowViewFactory",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory;",
        "(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "onlineCheckoutSettingsV2WorkflowViewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/workflow/WorkflowViewFactory;

    .line 12
    sget-object v1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflowViewFactory;->INSTANCE:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflowViewFactory;

    check-cast v1, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 13
    check-cast p1, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 11
    invoke-direct {p0, v0}, Lcom/squareup/workflow/CompoundWorkflowViewFactory;-><init>([Lcom/squareup/workflow/WorkflowViewFactory;)V

    return-void
.end method
