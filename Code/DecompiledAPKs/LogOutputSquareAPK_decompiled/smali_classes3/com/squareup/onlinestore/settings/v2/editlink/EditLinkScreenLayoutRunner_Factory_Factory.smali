.class public final Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner_Factory_Factory;
.super Ljava/lang/Object;
.source "EditLinkScreenLayoutRunner_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/SelectableTextScrubber;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/text/method/DigitsKeyListener;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/SelectableTextScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/text/method/DigitsKeyListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner_Factory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner_Factory_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p5, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner_Factory_Factory;->arg4Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner_Factory_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/SelectableTextScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/text/method/DigitsKeyListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ">;)",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner_Factory_Factory;"
        }
    .end annotation

    .line 47
    new-instance v6, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner_Factory_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/text/SelectableTextScrubber;Landroid/text/method/DigitsKeyListener;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/SelectableTextScrubber;",
            "Landroid/text/method/DigitsKeyListener;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ")",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;"
        }
    .end annotation

    .line 53
    new-instance v6, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;-><init>(Lcom/squareup/text/SelectableTextScrubber;Landroid/text/method/DigitsKeyListener;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;
    .locals 5

    .line 40
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/SelectableTextScrubber;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/method/DigitsKeyListener;

    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner_Factory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v3, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner_Factory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/text/Formatter;

    iget-object v4, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner_Factory_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner_Factory_Factory;->newInstance(Lcom/squareup/text/SelectableTextScrubber;Landroid/text/method/DigitsKeyListener;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner_Factory_Factory;->get()Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$Factory;

    move-result-object v0

    return-object v0
.end method
