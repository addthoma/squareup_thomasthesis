.class public abstract Lcom/squareup/onlinestore/settings/v2/CheckoutLink;
.super Ljava/lang/Object;
.source "CheckoutLink.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;,
        Lcom/squareup/onlinestore/settings/v2/CheckoutLink$DonationLink;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\r\u000eB\u001f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000b\u0082\u0001\u0002\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLink;",
        "Landroid/os/Parcelable;",
        "id",
        "",
        "name",
        "enabled",
        "",
        "(Ljava/lang/String;Ljava/lang/String;Z)V",
        "getEnabled",
        "()Z",
        "getId",
        "()Ljava/lang/String;",
        "getName",
        "CustomAmountLink",
        "DonationLink",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLink$DonationLink;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final enabled:Z

.field private final id:Ljava/lang/String;

.field private final name:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->name:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->enabled:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 7
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public getEnabled()Z
    .locals 1

    .line 10
    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->enabled:Z

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->name:Ljava/lang/String;

    return-object v0
.end method
