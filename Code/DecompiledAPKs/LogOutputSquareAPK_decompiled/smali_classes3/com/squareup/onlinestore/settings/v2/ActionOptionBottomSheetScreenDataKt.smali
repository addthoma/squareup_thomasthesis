.class public final Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenDataKt;
.super Ljava/lang/Object;
.source "ActionOptionBottomSheetScreenData.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\n\u0002\u0010\u000b\n\u0000*\"\u0010\u0000\"\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001*\n\u0010\u0004\"\u00020\u00052\u00020\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "ActionOptionBottomSheetDialogScreen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;",
        "",
        "NewEnabledStatus",
        "",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
