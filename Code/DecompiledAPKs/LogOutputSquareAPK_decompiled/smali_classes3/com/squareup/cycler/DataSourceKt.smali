.class public final Lcom/squareup/cycler/DataSourceKt;
.super Ljava/lang/Object;
.source "DataSource.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010 \n\u0000\u001a!\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u001a\u001c\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "toDataSource",
        "Lcom/squareup/cycler/DataSource;",
        "T",
        "",
        "([Ljava/lang/Object;)Lcom/squareup/cycler/DataSource;",
        "",
        "lib_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# direct methods
.method public static final toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List<",
            "+TT;>;)",
            "Lcom/squareup/cycler/DataSource<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$toDataSource"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    new-instance v0, Lcom/squareup/cycler/DataSourceKt$toDataSource$1;

    invoke-direct {v0, p0}, Lcom/squareup/cycler/DataSourceKt$toDataSource$1;-><init>(Ljava/util/List;)V

    check-cast v0, Lcom/squareup/cycler/DataSource;

    return-object v0
.end method

.method public static final toDataSource([Ljava/lang/Object;)Lcom/squareup/cycler/DataSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)",
            "Lcom/squareup/cycler/DataSource<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$toDataSource"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    new-instance v0, Lcom/squareup/cycler/DataSourceKt$toDataSource$2;

    invoke-direct {v0, p0}, Lcom/squareup/cycler/DataSourceKt$toDataSource$2;-><init>([Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/cycler/DataSource;

    return-object v0
.end method
