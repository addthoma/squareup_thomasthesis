.class public final Lcom/squareup/cycler/MutableDataSource;
.super Ljava/lang/Object;
.source "MutableDataSource.kt"

# interfaces
.implements Lcom/squareup/cycler/DataSource;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/cycler/DataSource<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008\u0000\u0018\u0000*\u0004\u0008\u0000\u0010\u00012\u0008\u0012\u0004\u0012\u0002H\u00010\u0002B\u0013\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0002\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u000b\u001a\u00020\u0006J\u0016\u0010\u000c\u001a\u00028\u00002\u0006\u0010\r\u001a\u00020\u0008H\u0096\u0002\u00a2\u0006\u0002\u0010\u000eJ\u0016\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00082\u0006\u0010\u0012\u001a\u00020\u0008J\u000e\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0014\u001a\u00020\u0008R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0002X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/cycler/MutableDataSource;",
        "T",
        "Lcom/squareup/cycler/DataSource;",
        "originalDataSource",
        "(Lcom/squareup/cycler/DataSource;)V",
        "mutationMap",
        "Lcom/squareup/cycler/MutationMap;",
        "size",
        "",
        "getSize",
        "()I",
        "copyMutationMap",
        "get",
        "i",
        "(I)Ljava/lang/Object;",
        "move",
        "",
        "from",
        "to",
        "remove",
        "index",
        "lib_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private final mutationMap:Lcom/squareup/cycler/MutationMap;

.field private final originalDataSource:Lcom/squareup/cycler/DataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/DataSource<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/DataSource;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/DataSource<",
            "+TT;>;)V"
        }
    .end annotation

    const-string v0, "originalDataSource"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cycler/MutableDataSource;->originalDataSource:Lcom/squareup/cycler/DataSource;

    .line 12
    new-instance p1, Lcom/squareup/cycler/MutationMap;

    iget-object v0, p0, Lcom/squareup/cycler/MutableDataSource;->originalDataSource:Lcom/squareup/cycler/DataSource;

    invoke-interface {v0}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v0

    invoke-direct {p1, v0}, Lcom/squareup/cycler/MutationMap;-><init>(I)V

    iput-object p1, p0, Lcom/squareup/cycler/MutableDataSource;->mutationMap:Lcom/squareup/cycler/MutationMap;

    return-void
.end method


# virtual methods
.method public final copyMutationMap()Lcom/squareup/cycler/MutationMap;
    .locals 2

    .line 17
    new-instance v0, Lcom/squareup/cycler/MutationMap;

    iget-object v1, p0, Lcom/squareup/cycler/MutableDataSource;->mutationMap:Lcom/squareup/cycler/MutationMap;

    invoke-direct {v0, v1}, Lcom/squareup/cycler/MutationMap;-><init>(Lcom/squareup/cycler/MutationMap;)V

    return-object v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/squareup/cycler/MutableDataSource;->originalDataSource:Lcom/squareup/cycler/DataSource;

    iget-object v1, p0, Lcom/squareup/cycler/MutableDataSource;->mutationMap:Lcom/squareup/cycler/MutationMap;

    invoke-virtual {v1, p1}, Lcom/squareup/cycler/MutationMap;->get(I)I

    move-result p1

    invoke-interface {v0, p1}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getSize()I
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/cycler/MutableDataSource;->mutationMap:Lcom/squareup/cycler/MutationMap;

    invoke-virtual {v0}, Lcom/squareup/cycler/MutationMap;->getSize()I

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .line 8
    invoke-static {p0}, Lcom/squareup/cycler/DataSource$DefaultImpls;->isEmpty(Lcom/squareup/cycler/DataSource;)Z

    move-result v0

    return v0
.end method

.method public final move(II)V
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/cycler/MutableDataSource;->mutationMap:Lcom/squareup/cycler/MutationMap;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/cycler/MutationMap;->move(II)V

    return-void
.end method

.method public final remove(I)V
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/cycler/MutableDataSource;->mutationMap:Lcom/squareup/cycler/MutationMap;

    invoke-virtual {v0, p1}, Lcom/squareup/cycler/MutationMap;->remove(I)V

    return-void
.end method
