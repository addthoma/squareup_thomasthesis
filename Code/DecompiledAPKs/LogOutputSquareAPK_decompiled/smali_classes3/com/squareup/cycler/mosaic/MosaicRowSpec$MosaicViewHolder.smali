.class final Lcom/squareup/cycler/mosaic/MosaicRowSpec$MosaicViewHolder;
.super Lcom/squareup/cycler/Recycler$ViewHolder;
.source "MosaicRowSpec.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cycler/mosaic/MosaicRowSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MosaicViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Lcom/squareup/mosaic/core/UiModel<",
        "*>;>",
        "Lcom/squareup/cycler/Recycler$ViewHolder<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0000\n\u0000\u0008\u0082\u0004\u0018\u0000*\u000c\u0008\u0002\u0010\u0001*\u0006\u0012\u0002\u0008\u00030\u00022\u0008\u0012\u0004\u0012\u00020\u00040\u0003B\u0017\u0012\u0010\u0010\u0005\u001a\u000c\u0012\u0004\u0012\u00028\u0002\u0012\u0002\u0008\u00030\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u001b\u0010\u0005\u001a\u000c\u0012\u0004\u0012\u00028\u0002\u0012\u0002\u0008\u00030\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/cycler/mosaic/MosaicRowSpec$MosaicViewHolder;",
        "M",
        "Lcom/squareup/mosaic/core/UiModel;",
        "Lcom/squareup/cycler/Recycler$ViewHolder;",
        "Landroid/view/View;",
        "viewRef",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "(Lcom/squareup/cycler/mosaic/MosaicRowSpec;Lcom/squareup/mosaic/core/ViewRef;)V",
        "getViewRef",
        "()Lcom/squareup/mosaic/core/ViewRef;",
        "bind",
        "",
        "index",
        "",
        "dataItem",
        "",
        "recycler-mosaic_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cycler/mosaic/MosaicRowSpec;

.field private final viewRef:Lcom/squareup/mosaic/core/ViewRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mosaic/core/ViewRef<",
            "TM;*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/mosaic/MosaicRowSpec;Lcom/squareup/mosaic/core/ViewRef;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "TM;*>;)V"
        }
    .end annotation

    const-string/jumbo v0, "viewRef"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    iput-object p1, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec$MosaicViewHolder;->this$0:Lcom/squareup/cycler/mosaic/MosaicRowSpec;

    .line 189
    invoke-virtual {p2}, Lcom/squareup/mosaic/core/ViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/cycler/Recycler$ViewHolder;-><init>(Landroid/view/View;)V

    iput-object p2, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec$MosaicViewHolder;->viewRef:Lcom/squareup/mosaic/core/ViewRef;

    return-void
.end method


# virtual methods
.method public bind(ILjava/lang/Object;)V
    .locals 1

    const-string v0, "dataItem"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec$MosaicViewHolder;->this$0:Lcom/squareup/cycler/mosaic/MosaicRowSpec;

    invoke-static {v0, p1, p2}, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->access$modelFor(Lcom/squareup/cycler/mosaic/MosaicRowSpec;ILjava/lang/Object;)Lcom/squareup/mosaic/core/UiModel;

    move-result-object p1

    .line 194
    iget-object p2, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec$MosaicViewHolder;->viewRef:Lcom/squareup/mosaic/core/ViewRef;

    if-eqz p1, :cond_0

    invoke-virtual {p2, p1}, Lcom/squareup/mosaic/core/ViewRef;->bind(Lcom/squareup/mosaic/core/UiModel;)V

    return-void

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type M"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getViewRef()Lcom/squareup/mosaic/core/ViewRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "TM;*>;"
        }
    .end annotation

    .line 188
    iget-object v0, p0, Lcom/squareup/cycler/mosaic/MosaicRowSpec$MosaicViewHolder;->viewRef:Lcom/squareup/mosaic/core/ViewRef;

    return-object v0
.end method
