.class public final Lcom/squareup/cycler/RecyclerData;
.super Ljava/lang/Object;
.source "RecyclerData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cycler/RecyclerData$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecyclerData.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecyclerData.kt\ncom/squareup/cycler/RecyclerData\n+ 2 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$RowSpec\n*L\n1#1,80:1\n57#1,3:81\n60#1,3:89\n63#1:97\n261#2:84\n262#2:87\n251#2:92\n252#2:95\n205#3,2:85\n205#3,2:93\n527#4:88\n527#4:96\n*E\n*S KotlinDebug\n*F\n+ 1 RecyclerData.kt\ncom/squareup/cycler/RecyclerData\n*L\n67#1,3:81\n67#1,3:89\n67#1:97\n67#1:84\n67#1:87\n67#1:92\n67#1:95\n67#1,2:85\n67#1,2:93\n67#1:88\n67#1:96\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u0000 7*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0002:\u00017B+\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0004\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0002\u00a2\u0006\u0002\u0010\u0008J\u0006\u0010#\u001a\u00020$J\u0008\u0010%\u001a\u00020&H\u0002J$\u0010\'\u001a\u0004\u0018\u0001H(\"\n\u0008\u0001\u0010(\u0018\u0001*\u00020\u00022\u0006\u0010)\u001a\u00020\u0013H\u0086\u0008\u00a2\u0006\u0002\u0010*JR\u0010+\u001a\u0002H(\"\u0004\u0008\u0001\u0010(2\u0006\u0010)\u001a\u00020\u00132\u0012\u0010,\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u0002H(0-2\u0012\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u0002H(0-2\u000c\u0010/\u001a\u0008\u0012\u0004\u0012\u0002H(00H\u0086\u0008\u00a2\u0006\u0002\u00101J\u0016\u00102\u001a\u00020&2\u0006\u00103\u001a\u00020\u00132\u0006\u00104\u001a\u00020\u0013J\u000e\u00105\u001a\u00020&2\u0006\u00106\u001a\u00020\u0013R\"\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00048\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\t\u0010\n\u001a\u0004\u0008\u000b\u0010\u000cR\u0017\u0010\r\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0002\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0012\u001a\u00020\u00138F\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0015R$\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0016\u001a\u00020\u0017@@X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\"\u0004\u0008\u001b\u0010\u001cR\u0011\u0010\u001d\u001a\u00020\u00178F\u00a2\u0006\u0006\u001a\u0004\u0008\u001e\u0010\u001aR\u0014\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00028\u00000 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010!\u001a\u00020\u00138F\u00a2\u0006\u0006\u001a\u0004\u0008\"\u0010\u0015\u00a8\u00068"
    }
    d2 = {
        "Lcom/squareup/cycler/RecyclerData;",
        "I",
        "",
        "config",
        "Lcom/squareup/cycler/Recycler$Config;",
        "originalData",
        "Lcom/squareup/cycler/DataSource;",
        "extraItem",
        "(Lcom/squareup/cycler/Recycler$Config;Lcom/squareup/cycler/DataSource;Ljava/lang/Object;)V",
        "config$annotations",
        "()V",
        "getConfig",
        "()Lcom/squareup/cycler/Recycler$Config;",
        "data",
        "getData",
        "()Lcom/squareup/cycler/DataSource;",
        "getExtraItem",
        "()Ljava/lang/Object;",
        "extraItemIndex",
        "",
        "getExtraItemIndex",
        "()I",
        "<set-?>",
        "",
        "frozen",
        "getFrozen",
        "()Z",
        "setFrozen$lib_release",
        "(Z)V",
        "hasExtraItem",
        "getHasExtraItem",
        "mutableData",
        "Lcom/squareup/cycler/MutableDataSource;",
        "totalCount",
        "getTotalCount",
        "copyMutationMap",
        "Lcom/squareup/cycler/MutationMap;",
        "ensureNotFrozen",
        "",
        "extension",
        "T",
        "position",
        "(I)Ljava/lang/Object;",
        "forPosition",
        "onDataItem",
        "Lkotlin/Function1;",
        "onExtraItem",
        "orElse",
        "Lkotlin/Function0;",
        "(ILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;",
        "move",
        "from",
        "to",
        "remove",
        "index",
        "Companion",
        "lib_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/cycler/RecyclerData$Companion;


# instance fields
.field private final config:Lcom/squareup/cycler/Recycler$Config;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;"
        }
    .end annotation
.end field

.field private final extraItem:Ljava/lang/Object;

.field private frozen:Z

.field private final mutableData:Lcom/squareup/cycler/MutableDataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/MutableDataSource<",
            "TI;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/cycler/RecyclerData$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/cycler/RecyclerData$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/cycler/RecyclerData;->Companion:Lcom/squareup/cycler/RecyclerData$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cycler/Recycler$Config;Lcom/squareup/cycler/DataSource;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lcom/squareup/cycler/DataSource<",
            "+TI;>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    const-string v0, "config"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "originalData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cycler/RecyclerData;->config:Lcom/squareup/cycler/Recycler$Config;

    iput-object p3, p0, Lcom/squareup/cycler/RecyclerData;->extraItem:Ljava/lang/Object;

    .line 44
    new-instance p1, Lcom/squareup/cycler/MutableDataSource;

    invoke-direct {p1, p2}, Lcom/squareup/cycler/MutableDataSource;-><init>(Lcom/squareup/cycler/DataSource;)V

    iput-object p1, p0, Lcom/squareup/cycler/RecyclerData;->mutableData:Lcom/squareup/cycler/MutableDataSource;

    return-void
.end method

.method public static synthetic config$annotations()V
    .locals 0

    return-void
.end method

.method private final ensureNotFrozen()V
    .locals 2

    .line 39
    iget-boolean v0, p0, Lcom/squareup/cycler/RecyclerData;->frozen:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot change items in a frozen MutableDataSource!"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public final copyMutationMap()Lcom/squareup/cycler/MutationMap;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/cycler/RecyclerData;->mutableData:Lcom/squareup/cycler/MutableDataSource;

    invoke-virtual {v0}, Lcom/squareup/cycler/MutableDataSource;->copyMutationMap()Lcom/squareup/cycler/MutationMap;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic extension(I)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I)TT;"
        }
    .end annotation

    .line 82
    invoke-virtual {p0}, Lcom/squareup/cycler/RecyclerData;->getExtraItemIndex()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x4

    const-string v3, "Collection contains no element matching the predicate."

    const/4 v4, 0x0

    const-string v5, "T?"

    if-ne p1, v0, :cond_3

    .line 83
    invoke-virtual {p0}, Lcom/squareup/cycler/RecyclerData;->getHasExtraItem()Z

    move-result p1

    if-eqz p1, :cond_7

    invoke-virtual {p0}, Lcom/squareup/cycler/RecyclerData;->getExtraItem()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 70
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/cycler/RecyclerData;->getConfig()Lcom/squareup/cycler/Recycler$Config;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler$Config;->getExtraItemSpecs()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 85
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 84
    invoke-virtual {v4, p1}, Lcom/squareup/cycler/Recycler$RowSpec;->matches(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 88
    invoke-virtual {v4}, Lcom/squareup/cycler/Recycler$RowSpec;->getExtensions()Ljava/util/Map;

    move-result-object p1

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Ljava/lang/Object;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    move-object v4, p1

    check-cast v4, Ljava/lang/Object;

    goto :goto_0

    .line 86
    :cond_2
    new-instance p1, Ljava/util/NoSuchElementException;

    invoke-direct {p1, v3}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 91
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v0

    if-gez p1, :cond_4

    goto :goto_0

    :cond_4
    if-le v0, p1, :cond_7

    invoke-virtual {p0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object p1

    .line 69
    invoke-virtual {p0}, Lcom/squareup/cycler/RecyclerData;->getConfig()Lcom/squareup/cycler/Recycler$Config;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler$Config;->getRowSpecs()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 93
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 92
    invoke-virtual {v4, p1}, Lcom/squareup/cycler/Recycler$RowSpec;->matches(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 96
    invoke-virtual {v4}, Lcom/squareup/cycler/Recycler$RowSpec;->getExtensions()Ljava/util/Map;

    move-result-object p1

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Ljava/lang/Object;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    move-object v4, p1

    check-cast v4, Ljava/lang/Object;

    goto :goto_0

    .line 94
    :cond_6
    new-instance p1, Ljava/util/NoSuchElementException;

    invoke-direct {p1, v3}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_7
    :goto_0
    return-object v4
.end method

.method public final forPosition(ILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Lkotlin/jvm/functions/Function1<",
            "-TI;+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Object;",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function0<",
            "+TT;>;)TT;"
        }
    .end annotation

    const-string v0, "onDataItem"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onExtraItem"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orElse"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-virtual {p0}, Lcom/squareup/cycler/RecyclerData;->getExtraItemIndex()I

    move-result v0

    if-ne p1, v0, :cond_2

    .line 59
    invoke-virtual {p0}, Lcom/squareup/cycler/RecyclerData;->getHasExtraItem()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/cycler/RecyclerData;->getExtraItem()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-interface {p3, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_1

    .line 60
    :cond_1
    invoke-interface {p4}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    goto :goto_1

    .line 62
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object p3

    invoke-interface {p3}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result p3

    if-gez p1, :cond_3

    goto :goto_0

    :cond_3
    if-le p3, p1, :cond_4

    invoke-virtual {p0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object p3

    invoke-interface {p3, p1}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_1

    .line 63
    :cond_4
    :goto_0
    invoke-interface {p4}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method public final getConfig()Lcom/squareup/cycler/Recycler$Config;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;"
        }
    .end annotation

    .line 10
    iget-object v0, p0, Lcom/squareup/cycler/RecyclerData;->config:Lcom/squareup/cycler/Recycler$Config;

    return-object v0
.end method

.method public final getData()Lcom/squareup/cycler/DataSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/DataSource<",
            "TI;>;"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/squareup/cycler/RecyclerData;->mutableData:Lcom/squareup/cycler/MutableDataSource;

    check-cast v0, Lcom/squareup/cycler/DataSource;

    return-object v0
.end method

.method public final getExtraItem()Ljava/lang/Object;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/cycler/RecyclerData;->extraItem:Ljava/lang/Object;

    return-object v0
.end method

.method public final getExtraItemIndex()I
    .locals 1

    .line 47
    invoke-virtual {p0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v0

    return v0
.end method

.method public final getFrozen()Z
    .locals 1

    .line 25
    iget-boolean v0, p0, Lcom/squareup/cycler/RecyclerData;->frozen:Z

    return v0
.end method

.method public final getHasExtraItem()Z
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/cycler/RecyclerData;->extraItem:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final getTotalCount()I
    .locals 2

    .line 48
    invoke-virtual {p0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/cycler/RecyclerData;->getHasExtraItem()Z

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final move(II)V
    .locals 1

    .line 29
    invoke-direct {p0}, Lcom/squareup/cycler/RecyclerData;->ensureNotFrozen()V

    .line 30
    iget-object v0, p0, Lcom/squareup/cycler/RecyclerData;->mutableData:Lcom/squareup/cycler/MutableDataSource;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/cycler/MutableDataSource;->move(II)V

    return-void
.end method

.method public final remove(I)V
    .locals 1

    .line 34
    invoke-direct {p0}, Lcom/squareup/cycler/RecyclerData;->ensureNotFrozen()V

    .line 35
    iget-object v0, p0, Lcom/squareup/cycler/RecyclerData;->mutableData:Lcom/squareup/cycler/MutableDataSource;

    invoke-virtual {v0, p1}, Lcom/squareup/cycler/MutableDataSource;->remove(I)V

    return-void
.end method

.method public final setFrozen$lib_release(Z)V
    .locals 0

    .line 25
    iput-boolean p1, p0, Lcom/squareup/cycler/RecyclerData;->frozen:Z

    return-void
.end method
