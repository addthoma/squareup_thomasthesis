.class final Lcom/squareup/cycler/Update$notifyChangesExtraItem$1;
.super Lkotlin/jvm/internal/Lambda;
.source "Update.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cycler/Update;->notifyChangesExtraItem()Lkotlin/jvm/functions/Function1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "*>;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00032\n\u0010\u0004\u001a\u0006\u0012\u0002\u0008\u00030\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "adapter",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cycler/Update;


# direct methods
.method constructor <init>(Lcom/squareup/cycler/Update;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cycler/Update$notifyChangesExtraItem$1;->this$0:Lcom/squareup/cycler/Update;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {p0, p1}, Lcom/squareup/cycler/Update$notifyChangesExtraItem$1;->invoke(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/squareup/cycler/Update$notifyChangesExtraItem$1;->this$0:Lcom/squareup/cycler/Update;

    invoke-static {v0}, Lcom/squareup/cycler/Update;->access$getOldRecyclerData$p(Lcom/squareup/cycler/Update;)Lcom/squareup/cycler/RecyclerData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getHasExtraItem()Z

    move-result v0

    .line 133
    iget-object v1, p0, Lcom/squareup/cycler/Update$notifyChangesExtraItem$1;->this$0:Lcom/squareup/cycler/Update;

    invoke-virtual {v1}, Lcom/squareup/cycler/Update;->getExtraItem()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 135
    iget-object v0, p0, Lcom/squareup/cycler/Update$notifyChangesExtraItem$1;->this$0:Lcom/squareup/cycler/Update;

    invoke-static {v0}, Lcom/squareup/cycler/Update;->access$getOldRecyclerData$p(Lcom/squareup/cycler/Update;)Lcom/squareup/cycler/RecyclerData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getExtraItemIndex()I

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    goto :goto_1

    :cond_1
    if-eqz v0, :cond_2

    if-nez v1, :cond_2

    .line 136
    iget-object v0, p0, Lcom/squareup/cycler/Update$notifyChangesExtraItem$1;->this$0:Lcom/squareup/cycler/Update;

    invoke-static {v0}, Lcom/squareup/cycler/Update;->access$getOldRecyclerData$p(Lcom/squareup/cycler/Update;)Lcom/squareup/cycler/RecyclerData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getExtraItemIndex()I

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRemoved(I)V

    goto :goto_1

    :cond_2
    if-nez v0, :cond_3

    if-eqz v1, :cond_3

    .line 137
    iget-object v0, p0, Lcom/squareup/cycler/Update$notifyChangesExtraItem$1;->this$0:Lcom/squareup/cycler/Update;

    invoke-static {v0}, Lcom/squareup/cycler/Update;->access$getOldRecyclerData$p(Lcom/squareup/cycler/Update;)Lcom/squareup/cycler/RecyclerData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getExtraItemIndex()I

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemInserted(I)V

    :cond_3
    :goto_1
    return-void
.end method
