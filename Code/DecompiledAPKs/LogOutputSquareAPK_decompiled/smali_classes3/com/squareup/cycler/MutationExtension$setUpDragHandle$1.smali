.class final Lcom/squareup/cycler/MutationExtension$setUpDragHandle$1;
.super Ljava/lang/Object;
.source "RecyclerMutations.kt"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cycler/MutationExtension;->setUpDragHandle$lib_release(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u00052\u000e\u0010\u0007\u001a\n \u0006*\u0004\u0018\u00010\u00080\u0008H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "T",
        "",
        "touchedView",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "event",
        "Landroid/view/MotionEvent;",
        "onTouch"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cycler/MutationExtension;


# direct methods
.method constructor <init>(Lcom/squareup/cycler/MutationExtension;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cycler/MutationExtension$setUpDragHandle$1;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    const-string v0, "event"

    .line 130
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result p2

    if-nez p2, :cond_1

    .line 131
    iget-object p2, p0, Lcom/squareup/cycler/MutationExtension$setUpDragHandle$1;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-static {p2}, Lcom/squareup/cycler/MutationExtension;->access$getRecycler$p(Lcom/squareup/cycler/MutationExtension;)Lcom/squareup/cycler/Recycler;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroidx/recyclerview/widget/RecyclerView;->findContainingViewHolder(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string p2, "recycler.view.findContai\u2026ViewHolder(touchedView)!!"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    iget-object p2, p0, Lcom/squareup/cycler/MutationExtension$setUpDragHandle$1;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-static {p2}, Lcom/squareup/cycler/MutationExtension;->access$getTouchHelper$p(Lcom/squareup/cycler/MutationExtension;)Landroidx/recyclerview/widget/ItemTouchHelper;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroidx/recyclerview/widget/ItemTouchHelper;->startDrag(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    :cond_1
    const/4 p1, 0x0

    return p1
.end method
