.class public final Lcom/squareup/loyalty/ui/CounterView;
.super Landroid/widget/LinearLayout;
.source "CounterView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyalty/ui/CounterView$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCounterView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CounterView.kt\ncom/squareup/loyalty/ui/CounterView\n*L\n1#1,109:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0006\u0018\u0000 \"2\u00020\u0001:\u0001\"B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J+\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\t2\n\u0008\u0002\u0010\u001f\u001a\u0004\u0018\u00010\t2\n\u0008\u0002\u0010 \u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010!R7\u0010\u0007\u001a\u001f\u0012\u0013\u0012\u00110\t\u00a2\u0006\u000c\u0008\n\u0012\u0008\u0008\u000b\u0012\u0004\u0008\u0008(\u000c\u0012\u0004\u0012\u00020\r\u0018\u00010\u0008X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011R\u000e\u0010\u0012\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/loyalty/ui/CounterView;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "countChangeListener",
        "Lkotlin/Function1;",
        "",
        "Lkotlin/ParameterName;",
        "name",
        "count",
        "",
        "getCountChangeListener",
        "()Lkotlin/jvm/functions/Function1;",
        "setCountChangeListener",
        "(Lkotlin/jvm/functions/Function1;)V",
        "currentCount",
        "disabledColor",
        "enabledColor",
        "maxCount",
        "minCount",
        "minusIcon",
        "Landroid/widget/ImageView;",
        "numberLabel",
        "Landroid/widget/TextView;",
        "plusIcon",
        "update",
        "",
        "newCount",
        "optionalNewMax",
        "optionalNewMin",
        "(ILjava/lang/Integer;Ljava/lang/Integer;)Z",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/loyalty/ui/CounterView$Companion;

.field private static final DEFAULT_MAX_COUNT:I = 0x7fffffff

.field private static final DEFAULT_MIN_COUNT:I


# instance fields
.field private countChangeListener:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private currentCount:I

.field private final disabledColor:I

.field private final enabledColor:I

.field private maxCount:I

.field private minCount:I

.field private final minusIcon:Landroid/widget/ImageView;

.field private final numberLabel:Landroid/widget/TextView;

.field private final plusIcon:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/loyalty/ui/CounterView$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/loyalty/ui/CounterView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/loyalty/ui/CounterView;->Companion:Lcom/squareup/loyalty/ui/CounterView$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const p2, 0x7fffffff

    .line 32
    iput p2, p0, Lcom/squareup/loyalty/ui/CounterView;->maxCount:I

    .line 36
    sget p2, Lcom/squareup/loyalty/R$color;->counter_view_enabled:I

    invoke-static {p1, p2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p2

    iput p2, p0, Lcom/squareup/loyalty/ui/CounterView;->enabledColor:I

    .line 37
    sget p2, Lcom/squareup/loyalty/R$color;->counter_view_disabled:I

    invoke-static {p1, p2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p2

    iput p2, p0, Lcom/squareup/loyalty/ui/CounterView;->disabledColor:I

    .line 40
    sget p2, Lcom/squareup/loyalty/R$layout;->counter_view:I

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p1, p2, v0}, Landroid/widget/LinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 42
    sget p2, Lcom/squareup/loyalty/R$id;->counter_view_minus_icon:I

    invoke-virtual {p0, p2}, Lcom/squareup/loyalty/ui/CounterView;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v0, "findViewById(R.id.counter_view_minus_icon)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/squareup/loyalty/ui/CounterView;->minusIcon:Landroid/widget/ImageView;

    .line 43
    sget p2, Lcom/squareup/loyalty/R$id;->counter_view_plus_icon:I

    invoke-virtual {p0, p2}, Lcom/squareup/loyalty/ui/CounterView;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v0, "findViewById(R.id.counter_view_plus_icon)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/squareup/loyalty/ui/CounterView;->plusIcon:Landroid/widget/ImageView;

    .line 44
    sget p2, Lcom/squareup/loyalty/R$id;->counter_view_number_label:I

    invoke-virtual {p0, p2}, Lcom/squareup/loyalty/ui/CounterView;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v0, "findViewById(R.id.counter_view_number_label)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/squareup/loyalty/ui/CounterView;->numberLabel:Landroid/widget/TextView;

    .line 48
    iget-object p2, p0, Lcom/squareup/loyalty/ui/CounterView;->minusIcon:Landroid/widget/ImageView;

    .line 49
    sget v0, Lcom/squareup/vectoricons/R$drawable;->circle_minus_24:I

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/noho/ContextExtensions;->getCustomDrawable$default(Landroid/content/Context;ILandroid/content/res/Resources$Theme;ILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 48
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 51
    iget-object p2, p0, Lcom/squareup/loyalty/ui/CounterView;->plusIcon:Landroid/widget/ImageView;

    .line 52
    sget v0, Lcom/squareup/vectoricons/R$drawable;->circle_plus_24:I

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/noho/ContextExtensions;->getCustomDrawable$default(Landroid/content/Context;ILandroid/content/res/Resources$Theme;ILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 51
    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 55
    iget-object p1, p0, Lcom/squareup/loyalty/ui/CounterView;->minusIcon:Landroid/widget/ImageView;

    new-instance p2, Lcom/squareup/loyalty/ui/CounterView$1;

    invoke-direct {p2, p0}, Lcom/squareup/loyalty/ui/CounterView$1;-><init>(Lcom/squareup/loyalty/ui/CounterView;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    iget-object p1, p0, Lcom/squareup/loyalty/ui/CounterView;->plusIcon:Landroid/widget/ImageView;

    new-instance p2, Lcom/squareup/loyalty/ui/CounterView$2;

    invoke-direct {p2, p0}, Lcom/squareup/loyalty/ui/CounterView$2;-><init>(Lcom/squareup/loyalty/ui/CounterView;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    iget v1, p0, Lcom/squareup/loyalty/ui/CounterView;->currentCount:I

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/squareup/loyalty/ui/CounterView;->update$default(Lcom/squareup/loyalty/ui/CounterView;ILjava/lang/Integer;Ljava/lang/Integer;ILjava/lang/Object;)Z

    return-void
.end method

.method public static final synthetic access$getCurrentCount$p(Lcom/squareup/loyalty/ui/CounterView;)I
    .locals 0

    .line 17
    iget p0, p0, Lcom/squareup/loyalty/ui/CounterView;->currentCount:I

    return p0
.end method

.method public static final synthetic access$setCurrentCount$p(Lcom/squareup/loyalty/ui/CounterView;I)V
    .locals 0

    .line 17
    iput p1, p0, Lcom/squareup/loyalty/ui/CounterView;->currentCount:I

    return-void
.end method

.method public static synthetic update$default(Lcom/squareup/loyalty/ui/CounterView;ILjava/lang/Integer;Ljava/lang/Integer;ILjava/lang/Object;)Z
    .locals 1

    and-int/lit8 p5, p4, 0x2

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    .line 78
    move-object p2, v0

    check-cast p2, Ljava/lang/Integer;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 79
    move-object p3, v0

    check-cast p3, Ljava/lang/Integer;

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/loyalty/ui/CounterView;->update(ILjava/lang/Integer;Ljava/lang/Integer;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public final getCountChangeListener()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/loyalty/ui/CounterView;->countChangeListener:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final setCountChangeListener(Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 31
    iput-object p1, p0, Lcom/squareup/loyalty/ui/CounterView;->countChangeListener:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final update(ILjava/lang/Integer;Ljava/lang/Integer;)Z
    .locals 1

    .line 81
    iget v0, p0, Lcom/squareup/loyalty/ui/CounterView;->currentCount:I

    .line 82
    iput p1, p0, Lcom/squareup/loyalty/ui/CounterView;->currentCount:I

    if-eqz p2, :cond_0

    .line 83
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p1

    iput p1, p0, Lcom/squareup/loyalty/ui/CounterView;->maxCount:I

    :cond_0
    if-eqz p3, :cond_1

    .line 84
    check-cast p3, Ljava/lang/Number;

    invoke-virtual {p3}, Ljava/lang/Number;->intValue()I

    move-result p1

    iput p1, p0, Lcom/squareup/loyalty/ui/CounterView;->minCount:I

    .line 86
    :cond_1
    iget p1, p0, Lcom/squareup/loyalty/ui/CounterView;->currentCount:I

    .line 87
    iget p2, p0, Lcom/squareup/loyalty/ui/CounterView;->minCount:I

    invoke-static {p1, p2}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result p1

    .line 88
    iget p2, p0, Lcom/squareup/loyalty/ui/CounterView;->maxCount:I

    invoke-static {p1, p2}, Lkotlin/ranges/RangesKt;->coerceAtMost(II)I

    move-result p1

    iput p1, p0, Lcom/squareup/loyalty/ui/CounterView;->currentCount:I

    .line 90
    iget-object p1, p0, Lcom/squareup/loyalty/ui/CounterView;->numberLabel:Landroid/widget/TextView;

    iget p2, p0, Lcom/squareup/loyalty/ui/CounterView;->currentCount:I

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/loyalty/ui/CounterView;->minusIcon:Landroid/widget/ImageView;

    .line 92
    iget p2, p0, Lcom/squareup/loyalty/ui/CounterView;->currentCount:I

    iget p3, p0, Lcom/squareup/loyalty/ui/CounterView;->minCount:I

    if-le p2, p3, :cond_2

    .line 93
    iget p2, p0, Lcom/squareup/loyalty/ui/CounterView;->enabledColor:I

    goto :goto_0

    .line 95
    :cond_2
    iget p2, p0, Lcom/squareup/loyalty/ui/CounterView;->disabledColor:I

    .line 91
    :goto_0
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 98
    iget-object p1, p0, Lcom/squareup/loyalty/ui/CounterView;->plusIcon:Landroid/widget/ImageView;

    .line 99
    iget p2, p0, Lcom/squareup/loyalty/ui/CounterView;->currentCount:I

    iget p3, p0, Lcom/squareup/loyalty/ui/CounterView;->maxCount:I

    if-ge p2, p3, :cond_3

    .line 100
    iget p2, p0, Lcom/squareup/loyalty/ui/CounterView;->enabledColor:I

    goto :goto_1

    .line 102
    :cond_3
    iget p2, p0, Lcom/squareup/loyalty/ui/CounterView;->disabledColor:I

    .line 98
    :goto_1
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 106
    iget p1, p0, Lcom/squareup/loyalty/ui/CounterView;->currentCount:I

    if-eq v0, p1, :cond_4

    const/4 p1, 0x1

    goto :goto_2

    :cond_4
    const/4 p1, 0x0

    :goto_2
    return p1
.end method
