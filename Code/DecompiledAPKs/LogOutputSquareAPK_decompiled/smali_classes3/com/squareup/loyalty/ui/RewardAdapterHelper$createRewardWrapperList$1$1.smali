.class final Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RewardAdapterHelper.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;->call(Ljava/lang/Integer;)Lcom/squareup/loyalty/ui/RewardWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;


# direct methods
.method constructor <init>(Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1$1;->this$0:Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 108
    iget-object v0, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1$1;->this$0:Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;

    iget-object v0, v0, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;->$applyEvent:Lkotlin/jvm/functions/Function1;

    iget-object v1, p0, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1$1;->this$0:Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;

    iget-object v1, v1, Lcom/squareup/loyalty/ui/RewardAdapterHelper$createRewardWrapperList$1;->$rewardTier:Lcom/squareup/server/account/protos/RewardTier;

    const-string v2, "rewardTier"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/loyalty/ui/RewardAdapterKt;->toRewardTier(Lcom/squareup/server/account/protos/RewardTier;)Lcom/squareup/protos/client/loyalty/RewardTier;

    move-result-object v1

    const-string v2, "rewardTier.toRewardTier()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
