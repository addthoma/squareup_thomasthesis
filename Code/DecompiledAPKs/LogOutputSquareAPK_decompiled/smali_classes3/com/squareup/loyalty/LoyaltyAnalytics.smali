.class public Lcom/squareup/loyalty/LoyaltyAnalytics;
.super Ljava/lang/Object;
.source "LoyaltyAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyalty/LoyaltyAnalytics$NoThanksAction;,
        Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitTimeoutAction;,
        Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitSuccessAction;,
        Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitAction;,
        Lcom/squareup/loyalty/LoyaltyAnalytics$StatusProgressRewardView;,
        Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollView;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Ljava/lang/String;
    .locals 0

    .line 28
    invoke-static {p0}, Lcom/squareup/loyalty/LoyaltyAnalytics;->buyerToken(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static buyerToken(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Ljava/lang/String;
    .locals 2

    .line 255
    iget-object p0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->phoneToken:Ljava/lang/String;

    .line 256
    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "p-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method private static newBuyerSubject(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Lcom/squareup/protos/eventstream/v1/Subject;
    .locals 2

    .line 248
    new-instance v0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;-><init>()V

    .line 249
    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->anonymized_user_token(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    move-result-object v0

    .line 250
    invoke-static {p0}, Lcom/squareup/loyalty/LoyaltyAnalytics;->buyerToken(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->user_token(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    move-result-object p0

    .line 251
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->build()Lcom/squareup/protos/eventstream/v1/Subject;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public logActionEnrollSubmit(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;D)V
    .locals 9

    .line 214
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v7, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitAction;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_MERCHANT:Lcom/squareup/analytics/RegisterActionName;

    const/4 v3, 0x1

    move-object v1, v7

    move-object v4, p1

    move-wide v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitAction;-><init>(Lcom/squareup/analytics/RegisterActionName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;D)V

    invoke-interface {v0, v7}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 216
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v8, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitAction;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_BUYER:Lcom/squareup/analytics/RegisterActionName;

    .line 218
    invoke-static {p1}, Lcom/squareup/loyalty/LoyaltyAnalytics;->newBuyerSubject(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Lcom/squareup/protos/eventstream/v1/Subject;

    move-result-object v7

    const/4 v3, 0x0

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitAction;-><init>(Lcom/squareup/analytics/RegisterActionName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;DLcom/squareup/protos/eventstream/v1/Subject;)V

    .line 216
    invoke-interface {v0, v8}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logActionEnrollSubmitSuccess(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V
    .locals 5

    .line 222
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitSuccessAction;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_SUCCESS_MERCHANT:Lcom/squareup/analytics/RegisterActionName;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, p1}, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitSuccessAction;-><init>(Lcom/squareup/analytics/RegisterActionName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 224
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitSuccessAction;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_SUCCESS_BUYER:Lcom/squareup/analytics/RegisterActionName;

    .line 226
    invoke-static {p1}, Lcom/squareup/loyalty/LoyaltyAnalytics;->newBuyerSubject(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Lcom/squareup/protos/eventstream/v1/Subject;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4, p1, v3}, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitSuccessAction;-><init>(Lcom/squareup/analytics/RegisterActionName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/protos/eventstream/v1/Subject;)V

    .line 224
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logActionEnrollSubmitTimeout(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V
    .locals 5

    .line 230
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitTimeoutAction;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_TIMEOUT_MERCHANT:Lcom/squareup/analytics/RegisterActionName;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, p1}, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitTimeoutAction;-><init>(Lcom/squareup/analytics/RegisterActionName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 232
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitTimeoutAction;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_ENROLL_SUBMIT_TIMEOUT_BUYER:Lcom/squareup/analytics/RegisterActionName;

    .line 234
    invoke-static {p1}, Lcom/squareup/loyalty/LoyaltyAnalytics;->newBuyerSubject(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Lcom/squareup/protos/eventstream/v1/Subject;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4, p1, v3}, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollSubmitTimeoutAction;-><init>(Lcom/squareup/analytics/RegisterActionName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/protos/eventstream/v1/Subject;)V

    .line 232
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logActionNoThanks(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;D)V
    .locals 9

    .line 238
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v7, Lcom/squareup/loyalty/LoyaltyAnalytics$NoThanksAction;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_NO_THANKS_MERCHANT:Lcom/squareup/analytics/RegisterActionName;

    const/4 v3, 0x1

    move-object v1, v7

    move-object v4, p1

    move-wide v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/loyalty/LoyaltyAnalytics$NoThanksAction;-><init>(Lcom/squareup/analytics/RegisterActionName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;D)V

    invoke-interface {v0, v7}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 240
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v8, Lcom/squareup/loyalty/LoyaltyAnalytics$NoThanksAction;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_NO_THANKS_BUYER:Lcom/squareup/analytics/RegisterActionName;

    .line 241
    invoke-static {p1}, Lcom/squareup/loyalty/LoyaltyAnalytics;->newBuyerSubject(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Lcom/squareup/protos/eventstream/v1/Subject;

    move-result-object v7

    const/4 v3, 0x0

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/squareup/loyalty/LoyaltyAnalytics$NoThanksAction;-><init>(Lcom/squareup/analytics/RegisterActionName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;DLcom/squareup/protos/eventstream/v1/Subject;)V

    .line 240
    invoke-interface {v0, v8}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logViewEnroll(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V
    .locals 5

    .line 201
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollView;

    sget-object v2, Lcom/squareup/analytics/RegisterViewName;->LOYALTY_ENROLL_MERCHANT:Lcom/squareup/analytics/RegisterViewName;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, p1}, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollView;-><init>(Lcom/squareup/analytics/RegisterViewName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 202
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollView;

    sget-object v2, Lcom/squareup/analytics/RegisterViewName;->LOYALTY_ENROLL_BUYER:Lcom/squareup/analytics/RegisterViewName;

    invoke-static {p1}, Lcom/squareup/loyalty/LoyaltyAnalytics;->newBuyerSubject(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Lcom/squareup/protos/eventstream/v1/Subject;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4, p1, v3}, Lcom/squareup/loyalty/LoyaltyAnalytics$EnrollView;-><init>(Lcom/squareup/analytics/RegisterViewName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/protos/eventstream/v1/Subject;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logViewStatusProgressReward(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V
    .locals 5

    .line 206
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/loyalty/LoyaltyAnalytics$StatusProgressRewardView;

    sget-object v2, Lcom/squareup/analytics/RegisterViewName;->LOYALTY_STATUS_PROGRESS_REWARD_MERCHANT:Lcom/squareup/analytics/RegisterViewName;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, p1}, Lcom/squareup/loyalty/LoyaltyAnalytics$StatusProgressRewardView;-><init>(Lcom/squareup/analytics/RegisterViewName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 208
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/loyalty/LoyaltyAnalytics$StatusProgressRewardView;

    sget-object v2, Lcom/squareup/analytics/RegisterViewName;->LOYALTY_STATUS_PROGRESS_REWARD_BUYER:Lcom/squareup/analytics/RegisterViewName;

    .line 210
    invoke-static {p1}, Lcom/squareup/loyalty/LoyaltyAnalytics;->newBuyerSubject(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Lcom/squareup/protos/eventstream/v1/Subject;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4, p1, v3}, Lcom/squareup/loyalty/LoyaltyAnalytics$StatusProgressRewardView;-><init>(Lcom/squareup/analytics/RegisterViewName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/protos/eventstream/v1/Subject;)V

    .line 208
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
