.class public final Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion;
.super Ljava/lang/Object;
.source "LoyaltyRewardProgress.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyalty/LoyaltyRewardProgress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J8\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0007\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion;",
        "",
        "()V",
        "fromLoyaltyEvent",
        "Lcom/squareup/loyalty/LoyaltyRewardProgress;",
        "event",
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
        "res",
        "Lcom/squareup/util/Res;",
        "locale",
        "Ljava/util/Locale;",
        "cashIntegration",
        "",
        "pointsTermFormatter",
        "Lcom/squareup/loyalty/PointsTermsFormatter;",
        "loyaltyRulesFormatter",
        "Lcom/squareup/loyalty/LoyaltyRulesFormatter;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromLoyaltyEvent(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/util/Res;Ljava/util/Locale;ZLcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/LoyaltyRulesFormatter;)Lcom/squareup/loyalty/LoyaltyRewardProgress;
    .locals 6
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pointsTermFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyRulesFormatter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget v0, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedInTransaction:I

    if-eqz v0, :cond_0

    .line 45
    new-instance v0, Lcom/squareup/loyalty/AnimateProgressRewardText;

    .line 47
    iget v1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedInTransaction:I

    sget v2, Lcom/squareup/loyalty/R$string;->loyalty_points_amount_new:I

    .line 46
    invoke-virtual {p5, v1, v2}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(II)Ljava/lang/String;

    move-result-object v1

    .line 49
    iget v2, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedInTransaction:I

    .line 50
    iget v3, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedTotal:I

    .line 51
    new-instance v4, Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion$fromLoyaltyEvent$rewardText$1;

    invoke-direct {v4, p5}, Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion$fromLoyaltyEvent$rewardText$1;-><init>(Lcom/squareup/loyalty/PointsTermsFormatter;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 45
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/loyalty/AnimateProgressRewardText;-><init>(Ljava/lang/String;IILkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/loyalty/LoyaltyRewardText;

    goto :goto_0

    .line 42
    :cond_0
    new-instance v0, Lcom/squareup/loyalty/StaticRewardText;

    .line 43
    iget v1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedTotal:I

    sget v2, Lcom/squareup/loyalty/R$string;->loyalty_points_amount:I

    invoke-virtual {p5, v1, v2}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(II)Ljava/lang/String;

    move-result-object v1

    .line 42
    invoke-direct {v0, v1}, Lcom/squareup/loyalty/StaticRewardText;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/loyalty/LoyaltyRewardText;

    .line 55
    :goto_0
    iget v1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->newEligibleRewardTiers:I

    .line 56
    iget v2, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->totalEligibleRewardTiers:I

    .line 57
    invoke-static {p3}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object p3

    const-string v3, "rewards"

    if-eqz v1, :cond_1

    .line 65
    new-instance v4, Lcom/squareup/loyalty/AnimateProgressRewardText;

    .line 66
    sget v5, Lcom/squareup/loyalty/R$string;->loyalty_rewards_amount_new:I

    invoke-interface {p2, v5}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 67
    invoke-virtual {v5, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 68
    invoke-virtual {v3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v3

    .line 69
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 72
    new-instance v5, Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion$fromLoyaltyEvent$rewardSubtext$1;

    invoke-direct {v5, p2}, Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion$fromLoyaltyEvent$rewardSubtext$1;-><init>(Lcom/squareup/util/Res;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 65
    invoke-direct {v4, v3, v1, v2, v5}, Lcom/squareup/loyalty/AnimateProgressRewardText;-><init>(Ljava/lang/String;IILkotlin/jvm/functions/Function1;)V

    check-cast v4, Lcom/squareup/loyalty/LoyaltyRewardText;

    goto :goto_1

    .line 59
    :cond_1
    new-instance v4, Lcom/squareup/loyalty/StaticRewardText;

    .line 60
    sget v5, Lcom/squareup/loyalty/R$string;->loyalty_rewards_amount:I

    invoke-interface {p2, v5}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 61
    invoke-virtual {v5, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 62
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    .line 63
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 59
    invoke-direct {v4, v2}, Lcom/squareup/loyalty/StaticRewardText;-><init>(Ljava/lang/String;)V

    check-cast v4, Lcom/squareup/loyalty/LoyaltyRewardText;

    .line 81
    :goto_1
    iget v2, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedInTransaction:I

    const/4 v3, 0x1

    if-lez v2, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    .line 84
    :goto_2
    iget-boolean v5, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->didLinkNewCardToExistingLoyalty:Z

    if-eqz v5, :cond_3

    iget-object v5, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->obfuscatedPhoneNumber:Ljava/lang/String;

    check-cast v5, Ljava/lang/CharSequence;

    invoke-static {v5}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 85
    sget p3, Lcom/squareup/loyalty/R$string;->loyalty_congratulations_linked_phone:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 86
    iget-object p1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->obfuscatedPhoneNumber:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    const-string p3, "phone_number"

    invoke-virtual {p2, p3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 87
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 88
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_4

    .line 90
    :cond_3
    iget-boolean v5, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->didLinkNewCardToExistingLoyalty:Z

    if-eqz v5, :cond_4

    .line 91
    sget p1, Lcom/squareup/loyalty/R$string;->loyalty_congratulations_linked_card:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    .line 93
    :cond_4
    iget-boolean v5, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isNewlyEnrolled:Z

    if-eqz v5, :cond_6

    if-eqz p4, :cond_5

    if-nez v2, :cond_5

    .line 96
    sget p1, Lcom/squareup/loyalty/R$string;->loyalty_cash_app_sms:I

    goto :goto_3

    .line 97
    :cond_5
    sget p1, Lcom/squareup/loyalty/R$string;->loyalty_congratulations_welcome_check_sms:I

    .line 94
    :goto_3
    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    :cond_6
    if-nez v2, :cond_8

    .line 104
    sget p1, Lcom/squareup/loyalty/R$string;->loyalty_did_not_qualify:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 105
    invoke-virtual {p6}, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->loyaltyProgramRequirements()Ljava/lang/String;

    move-result-object p2

    if-nez p2, :cond_7

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_7
    check-cast p2, Ljava/lang/CharSequence;

    const-string p3, "rule"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 106
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 107
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    :cond_8
    if-eqz v2, :cond_9

    if-ne v1, v3, :cond_9

    .line 110
    sget p1, Lcom/squareup/loyalty/R$string;->loyalty_congratulations_multiple_available_single_reward:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    :cond_9
    if-eqz v2, :cond_a

    if-le v1, v3, :cond_a

    .line 113
    sget p1, Lcom/squareup/loyalty/R$string;->loyalty_congratulations_multiple_available_rewards:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    int-to-long p4, v1

    .line 114
    invoke-virtual {p3, p4, p5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string p3, "num_rewards"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 115
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 116
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    .line 119
    :cond_a
    iget p1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedInTransaction:I

    sget p2, Lcom/squareup/loyalty/R$string;->loyalty_congratulations_title:I

    .line 118
    invoke-virtual {p5, p1, p2}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(II)Ljava/lang/String;

    move-result-object p1

    .line 125
    :goto_4
    new-instance p2, Lcom/squareup/loyalty/LoyaltyRewardProgress;

    const/4 p3, 0x0

    invoke-direct {p2, v0, v4, p1, p3}, Lcom/squareup/loyalty/LoyaltyRewardProgress;-><init>(Lcom/squareup/loyalty/LoyaltyRewardText;Lcom/squareup/loyalty/LoyaltyRewardText;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p2
.end method
