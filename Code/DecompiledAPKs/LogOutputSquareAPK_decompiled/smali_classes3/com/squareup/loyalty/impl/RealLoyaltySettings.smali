.class public final Lcom/squareup/loyalty/impl/RealLoyaltySettings;
.super Ljava/lang/Object;
.source "RealLoyaltySettings.kt"

# interfaces
.implements Lcom/squareup/loyalty/LoyaltySettings;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealLoyaltySettings.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealLoyaltySettings.kt\ncom/squareup/loyalty/impl/RealLoyaltySettings\n*L\n1#1,114:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001BM\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u000e\u0008\u0001\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u000e\u0008\u0001\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u000e\u0008\u0001\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0008\u00a2\u0006\u0002\u0010\rJ\n\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016J\u0008\u0010\u0010\u001a\u00020\tH\u0016J\n\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016J\u0008\u0010\u0013\u001a\u00020\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\u000cH\u0016J\n\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0016J\u0008\u0010\u0018\u001a\u00020\tH\u0016J\u0008\u0010\u0019\u001a\u00020\tH\u0016J\u0008\u0010\u001a\u001a\u00020\tH\u0016J\u000e\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\t0\u001cH\u0016J\u0008\u0010\u001d\u001a\u00020\tH\u0002J\u0008\u0010\u001e\u001a\u00020\u001fH\u0016J\n\u0010 \u001a\u0004\u0018\u00010\u0017H\u0016J\u0010\u0010!\u001a\n\u0012\u0004\u0012\u00020#\u0018\u00010\"H\u0016J\u0010\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\u000cH\u0016J\u0010\u0010\'\u001a\u00020%2\u0006\u0010&\u001a\u00020\tH\u0016J\u0010\u0010(\u001a\u00020%2\u0006\u0010&\u001a\u00020\tH\u0016J\u0008\u0010\n\u001a\u00020\tH\u0016J\u0010\u0010)\u001a\n **\u0004\u0018\u00010\u00040\u0004H\u0002R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/loyalty/impl/RealLoyaltySettings;",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "statusResponseProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "res",
        "Lcom/squareup/util/Res;",
        "loyaltyScreensEnabled",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "showNonQualifyingLoyaltyEvents",
        "loyaltyScreenTimeoutSeconds",
        "",
        "(Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;)V",
        "accrualRules",
        "Lcom/squareup/server/account/protos/AccrualRules;",
        "canRedeemPoints",
        "getExpirationPolicy",
        "Lcom/squareup/server/account/protos/ExpirationPolicy;",
        "getLoyaltyScreenTimeoutMillis",
        "",
        "getLoyaltyScreenTimeoutSeconds",
        "getSubunitName",
        "",
        "hasLoyaltyProgram",
        "isLoyaltyProgramActive",
        "isLoyaltyScreensEnabled",
        "isLoyaltyScreensEnabledObservable",
        "Lio/reactivex/Observable;",
        "isPointsProgram",
        "pointsTerms",
        "Lcom/squareup/loyalty/PointsTerms;",
        "qualifyingPurchaseDescription",
        "rewardTiers",
        "",
        "Lcom/squareup/server/account/protos/RewardTier;",
        "setLoyaltyScreenTimeoutSeconds",
        "",
        "value",
        "setLoyaltyScreensEnabled",
        "setShowNonQualifyingLoyaltyEvents",
        "statusResponse",
        "kotlin.jvm.PlatformType",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final loyaltyScreenTimeoutSeconds:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyScreensEnabled:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final showNonQualifyingLoyaltyEvents:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final statusResponseProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;)V
    .locals 1
    .param p3    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/loyalty/LoyaltyScreensEnabled;
        .end annotation
    .end param
    .param p4    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/loyalty/ShowNonQualifyingLoyaltyEvents;
        .end annotation
    .end param
    .param p5    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/loyalty/LoyaltyScreenTimeout;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "statusResponseProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyScreensEnabled"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "showNonQualifyingLoyaltyEvents"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyScreenTimeoutSeconds"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->statusResponseProvider:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->loyaltyScreensEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p4, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->showNonQualifyingLoyaltyEvents:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p5, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->loyaltyScreenTimeoutSeconds:Lcom/f2prateek/rx/preferences2/Preference;

    return-void
.end method

.method private final isPointsProgram()Z
    .locals 2

    .line 49
    invoke-direct {p0}, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->statusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->POINTS:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method private final statusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->statusResponseProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccountStatusResponse;

    return-object v0
.end method


# virtual methods
.method public accrualRules()Lcom/squareup/server/account/protos/AccrualRules;
    .locals 1

    .line 60
    invoke-direct {p0}, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->statusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/LoyaltyProgram;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public canRedeemPoints()Z
    .locals 2

    .line 42
    invoke-direct {p0}, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->statusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 43
    :goto_0
    sget-object v1, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->INACTIVE:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    if-eq v0, v1, :cond_1

    invoke-direct {p0}, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->isPointsProgram()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public getExpirationPolicy()Lcom/squareup/server/account/protos/ExpirationPolicy;
    .locals 1

    .line 64
    invoke-direct {p0}, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->statusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/LoyaltyProgram;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getLoyaltyScreenTimeoutMillis()I
    .locals 3

    .line 108
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0}, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->getLoyaltyScreenTimeoutSeconds()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v1, v0

    return v1
.end method

.method public getLoyaltyScreenTimeoutSeconds()J
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->loyaltyScreenTimeoutSeconds:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->isSet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->loyaltyScreenTimeoutSeconds:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "loyaltyScreenTimeoutSeconds.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x3c

    :goto_0
    return-wide v0
.end method

.method public getSubunitName()Ljava/lang/String;
    .locals 1

    .line 66
    invoke-direct {p0}, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->statusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->subunit_name:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public hasLoyaltyProgram()Z
    .locals 2

    .line 34
    invoke-direct {p0}, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->statusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 35
    :goto_0
    sget-object v1, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->ACTIVE:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->INACTIVE:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    if-ne v0, v1, :cond_2

    :cond_1
    invoke-direct {p0}, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->isPointsProgram()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public isLoyaltyProgramActive()Z
    .locals 2

    .line 30
    invoke-direct {p0}, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->statusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->ACTIVE:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->isPointsProgram()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public isLoyaltyScreensEnabled()Z
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->loyaltyScreensEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->isSet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->loyaltyScreensEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "loyaltyScreensEnabled.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public isLoyaltyScreensEnabledObservable()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->loyaltyScreensEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v0

    .line 80
    invoke-virtual {p0}, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->isLoyaltyScreensEnabled()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "loyaltyScreensEnabled.as\u2026(isLoyaltyScreensEnabled)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public pointsTerms()Lcom/squareup/loyalty/PointsTerms;
    .locals 4

    .line 52
    invoke-direct {p0}, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->statusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/squareup/server/account/protos/LoyaltyProgram;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    if-eqz v0, :cond_2

    .line 53
    new-instance v1, Lcom/squareup/loyalty/PointsTerms;

    iget-object v2, v0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->one:Ljava/lang/String;

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v3, "it.one!!"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, v0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->other:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    const-string v3, "it.other!!"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2, v0}, Lcom/squareup/loyalty/PointsTerms;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 54
    :cond_2
    new-instance v1, Lcom/squareup/loyalty/PointsTerms;

    .line 55
    iget-object v0, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/loyalty/R$string;->loyalty_points_default_singular:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 56
    iget-object v2, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/loyalty/R$string;->loyalty_points_default_plural:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 54
    invoke-direct {v1, v0, v2}, Lcom/squareup/loyalty/PointsTerms;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v1
.end method

.method public qualifyingPurchaseDescription()Ljava/lang/String;
    .locals 1

    .line 47
    invoke-direct {p0}, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->statusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/LoyaltyProgram;->qualifying_purchase_description:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public rewardTiers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/RewardTier;",
            ">;"
        }
    .end annotation

    .line 62
    invoke-direct {p0}, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->statusResponse()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/LoyaltyProgram;->reward_tiers:Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public setLoyaltyScreenTimeoutSeconds(J)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->loyaltyScreenTimeoutSeconds:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public setLoyaltyScreensEnabled(Z)V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->loyaltyScreensEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public setShowNonQualifyingLoyaltyEvents(Z)V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->showNonQualifyingLoyaltyEvents:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showNonQualifyingLoyaltyEvents()Z
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->showNonQualifyingLoyaltyEvents:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->isSet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/squareup/loyalty/impl/RealLoyaltySettings;->showNonQualifyingLoyaltyEvents:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "showNonQualifyingLoyaltyEvents.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method
