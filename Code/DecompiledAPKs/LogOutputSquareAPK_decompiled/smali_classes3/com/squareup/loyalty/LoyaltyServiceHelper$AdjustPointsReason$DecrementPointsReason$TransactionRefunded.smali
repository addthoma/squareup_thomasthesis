.class public final Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$TransactionRefunded;
.super Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason;
.source "LoyaltyServiceHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TransactionRefunded"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$TransactionRefunded;",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason;",
        "()V",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$TransactionRefunded;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 128
    new-instance v0, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$TransactionRefunded;

    invoke-direct {v0}, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$TransactionRefunded;-><init>()V

    sput-object v0, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$TransactionRefunded;->INSTANCE:Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$TransactionRefunded;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 128
    sget-object v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;->TRANSACTION_REFUNDED:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason;-><init>(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
