.class public final Lcom/squareup/loyalty/LoyaltyRewardProgress;
.super Ljava/lang/Object;
.source "LoyaltyRewardProgress.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u00082\u00020\u0001:\u0001\u0008B!\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007R\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/loyalty/LoyaltyRewardProgress;",
        "",
        "rewardText",
        "Lcom/squareup/loyalty/LoyaltyRewardText;",
        "rewardSubtext",
        "congratulationsText",
        "",
        "(Lcom/squareup/loyalty/LoyaltyRewardText;Lcom/squareup/loyalty/LoyaltyRewardText;Ljava/lang/String;)V",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion;


# instance fields
.field public final congratulationsText:Ljava/lang/String;

.field public final rewardSubtext:Lcom/squareup/loyalty/LoyaltyRewardText;

.field public final rewardText:Lcom/squareup/loyalty/LoyaltyRewardText;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/loyalty/LoyaltyRewardProgress;->Companion:Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/loyalty/LoyaltyRewardText;Lcom/squareup/loyalty/LoyaltyRewardText;Ljava/lang/String;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyRewardProgress;->rewardText:Lcom/squareup/loyalty/LoyaltyRewardText;

    iput-object p2, p0, Lcom/squareup/loyalty/LoyaltyRewardProgress;->rewardSubtext:Lcom/squareup/loyalty/LoyaltyRewardText;

    iput-object p3, p0, Lcom/squareup/loyalty/LoyaltyRewardProgress;->congratulationsText:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/loyalty/LoyaltyRewardText;Lcom/squareup/loyalty/LoyaltyRewardText;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/loyalty/LoyaltyRewardProgress;-><init>(Lcom/squareup/loyalty/LoyaltyRewardText;Lcom/squareup/loyalty/LoyaltyRewardText;Ljava/lang/String;)V

    return-void
.end method

.method public static final fromLoyaltyEvent(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/util/Res;Ljava/util/Locale;ZLcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/LoyaltyRulesFormatter;)Lcom/squareup/loyalty/LoyaltyRewardProgress;
    .locals 7
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/loyalty/LoyaltyRewardProgress;->Companion:Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion;->fromLoyaltyEvent(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/util/Res;Ljava/util/Locale;ZLcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/LoyaltyRulesFormatter;)Lcom/squareup/loyalty/LoyaltyRewardProgress;

    move-result-object p0

    return-object p0
.end method
