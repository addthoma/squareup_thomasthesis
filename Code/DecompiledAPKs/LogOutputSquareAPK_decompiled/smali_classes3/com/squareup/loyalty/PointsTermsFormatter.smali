.class public final Lcom/squareup/loyalty/PointsTermsFormatter;
.super Ljava/lang/Object;
.source "PointsTermsFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPointsTermsFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PointsTermsFormatter.kt\ncom/squareup/loyalty/PointsTermsFormatter\n*L\n1#1,83:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B%\u0008\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tB%\u0012\u0006\u0010\n\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\u000eJ\u000e\u0010\u000f\u001a\u00020\u000c2\u0006\u0010\u0010\u001a\u00020\u0011J\u0010\u0010\r\u001a\u00020\u000c2\u0008\u0008\u0001\u0010\u0012\u001a\u00020\u0011J\u0016\u0010\u0010\u001a\n \u0013*\u0004\u0018\u00010\u000c0\u000c2\u0006\u0010\u0010\u001a\u00020\u0011J\u0018\u0010\u0010\u001a\u00020\u000c2\u0006\u0010\u0010\u001a\u00020\u00112\u0008\u0008\u0003\u0010\u0012\u001a\u00020\u0011J\u0016\u0010\u0010\u001a\n \u0013*\u0004\u0018\u00010\u000c0\u000c2\u0006\u0010\u0010\u001a\u00020\u0014J\u0018\u0010\u0010\u001a\u00020\u000c2\u0006\u0010\u0010\u001a\u00020\u00142\u0008\u0008\u0003\u0010\u0012\u001a\u00020\u0011J\u000e\u0010\u0015\u001a\u00020\u000c2\u0006\u0010\u0010\u001a\u00020\u0011J\u000e\u0010\u0015\u001a\u00020\u000c2\u0006\u0010\u0010\u001a\u00020\u0014J\u0010\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0001\u0010\u0012\u001a\u00020\u0011R\u000e\u0010\n\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/loyalty/PointsTermsFormatter;",
        "",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/loyalty/LoyaltySettings;Ljavax/inject/Provider;Lcom/squareup/util/Res;)V",
        "locale",
        "singular",
        "",
        "plural",
        "(Ljava/util/Locale;Lcom/squareup/util/Res;Ljava/lang/String;Ljava/lang/String;)V",
        "getTerm",
        "points",
        "",
        "phrase",
        "kotlin.jvm.PlatformType",
        "",
        "pointsWithSuffix",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final locale:Ljava/util/Locale;

.field private final plural:Ljava/lang/String;

.field private final res:Lcom/squareup/util/Res;

.field private final singular:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/loyalty/LoyaltySettings;Ljavax/inject/Provider;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loyaltySettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-interface {p2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p2

    const-string v0, "localeProvider.get()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Ljava/util/Locale;

    .line 29
    invoke-interface {p1}, Lcom/squareup/loyalty/LoyaltySettings;->pointsTerms()Lcom/squareup/loyalty/PointsTerms;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/loyalty/PointsTerms;->getSingular()Ljava/lang/String;

    move-result-object v0

    .line 30
    invoke-interface {p1}, Lcom/squareup/loyalty/LoyaltySettings;->pointsTerms()Lcom/squareup/loyalty/PointsTerms;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/loyalty/PointsTerms;->getPlural()Ljava/lang/String;

    move-result-object p1

    .line 26
    invoke-direct {p0, p2, p3, v0, p1}, Lcom/squareup/loyalty/PointsTermsFormatter;-><init>(Ljava/util/Locale;Lcom/squareup/util/Res;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;Lcom/squareup/util/Res;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "locale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "singular"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "plural"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyalty/PointsTermsFormatter;->locale:Ljava/util/Locale;

    iput-object p2, p0, Lcom/squareup/loyalty/PointsTermsFormatter;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/loyalty/PointsTermsFormatter;->singular:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/loyalty/PointsTermsFormatter;->plural:Ljava/lang/String;

    return-void
.end method

.method public static synthetic points$default(Lcom/squareup/loyalty/PointsTermsFormatter;IIILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 58
    sget p2, Lcom/squareup/loyalty/R$string;->loyalty_points_with_suffix:I

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(II)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic points$default(Lcom/squareup/loyalty/PointsTermsFormatter;JIILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    .line 48
    sget p3, Lcom/squareup/loyalty/R$string;->loyalty_points_with_suffix:I

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(JI)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final getTerm(I)Ljava/lang/String;
    .locals 1

    .line 81
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lcom/squareup/loyalty/PointsTermsFormatter;->singular:Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/loyalty/PointsTermsFormatter;->plural:Ljava/lang/String;

    :goto_0
    return-object p1
.end method

.method public final plural(I)Ljava/lang/String;
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/loyalty/PointsTermsFormatter;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 67
    iget-object v0, p0, Lcom/squareup/loyalty/PointsTermsFormatter;->plural:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "points_terminology"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 68
    iget-object v0, p0, Lcom/squareup/loyalty/PointsTermsFormatter;->plural:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "plural_points_terminology"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 69
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 70
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final points(I)Ljava/lang/String;
    .locals 2

    int-to-long v0, p1

    .line 36
    invoke-virtual {p0, v0, v1}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(J)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final points(II)Ljava/lang/String;
    .locals 2

    int-to-long v0, p1

    .line 59
    invoke-virtual {p0, v0, v1, p2}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(JI)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final points(J)Ljava/lang/String;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/loyalty/PointsTermsFormatter;->locale:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final points(JI)Ljava/lang/String;
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/loyalty/PointsTermsFormatter;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 50
    invoke-virtual {p0, p1, p2}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(J)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "points"

    invoke-virtual {p3, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    long-to-int p2, p1

    .line 52
    invoke-virtual {p0, p2}, Lcom/squareup/loyalty/PointsTermsFormatter;->getTerm(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v0, "points_terminology"

    invoke-virtual {p3, v0, p1}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 53
    invoke-virtual {p0, p2}, Lcom/squareup/loyalty/PointsTermsFormatter;->getTerm(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string p3, "plural_points_terminology"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 54
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 55
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final pointsWithSuffix(I)Ljava/lang/String;
    .locals 1

    .line 42
    sget v0, Lcom/squareup/loyalty/R$string;->loyalty_points_with_suffix:I

    invoke-virtual {p0, p1, v0}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(II)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final pointsWithSuffix(J)Ljava/lang/String;
    .locals 1

    .line 39
    sget v0, Lcom/squareup/loyalty/R$string;->loyalty_points_with_suffix:I

    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(JI)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final singular(I)Ljava/lang/String;
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/squareup/loyalty/PointsTermsFormatter;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 76
    iget-object v0, p0, Lcom/squareup/loyalty/PointsTermsFormatter;->singular:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "singular_points_terminology"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 77
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 78
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
