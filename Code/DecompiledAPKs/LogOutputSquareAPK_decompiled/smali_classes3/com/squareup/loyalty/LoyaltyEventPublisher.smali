.class public Lcom/squareup/loyalty/LoyaltyEventPublisher;
.super Ljava/lang/Object;
.source "LoyaltyEventPublisher.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoyaltyEventPublisher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoyaltyEventPublisher.kt\ncom/squareup/loyalty/LoyaltyEventPublisher\n+ 2 Rx2.kt\ncom/squareup/util/rx2/Rx2Kt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,156:1\n19#2:157\n250#3,2:158\n*E\n*S KotlinDebug\n*F\n+ 1 LoyaltyEventPublisher.kt\ncom/squareup/loyalty/LoyaltyEventPublisher\n*L\n62#1:157\n98#1,2:158\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u0017\u0018\u00002\u00020\u0001B!\u0008\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008B9\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\n\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u000eJ\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00192\u0006\u0010\u000f\u001a\u00020\u0011H\u0002J\u0010\u0010\u001d\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0008\u0010\u001e\u001a\u00020\u001fH\u0002J\u000c\u0010 \u001a\u00020\u001f*\u00020\u001bH\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00108VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0013R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00108VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0013R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\r0\u00108VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\u0013R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/loyalty/LoyaltyEventPublisher;",
        "",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "unitToken",
        "",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/loyalty/LoyaltySettings;Ljava/lang/String;Lcom/squareup/settings/server/Features;)V",
        "maybeLoyaltyEventPublisher",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent;",
        "missedLoyaltyReasonPublisher",
        "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;",
        "(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/loyalty/LoyaltySettings;Ljava/lang/String;Lcom/squareup/settings/server/Features;)V",
        "loyaltyEvent",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
        "getLoyaltyEvent",
        "()Lio/reactivex/Observable;",
        "maybeLoyaltyEvent",
        "getMaybeLoyaltyEvent",
        "missedLoyaltyReason",
        "getMissedLoyaltyReason",
        "maybeTriggerLoyaltyEvent",
        "",
        "addedTenderEvent",
        "Lcom/squareup/payment/AddedTenderEvent;",
        "publish",
        "publishNonLoyaltyEvent",
        "shouldAlwaysShowLoyaltyScreen",
        "",
        "serverHandlesReturnItems",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final maybeLoyaltyEventPublisher:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/loyalty/MaybeLoyaltyEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final missedLoyaltyReasonPublisher:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;",
            ">;"
        }
    .end annotation
.end field

.field private final unitToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/loyalty/LoyaltySettings;Ljava/lang/String;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/loyalty/MaybeLoyaltyEvent;",
            ">;",
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;",
            ">;",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            "Ljava/lang/String;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    const-string v0, "maybeLoyaltyEventPublisher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "missedLoyaltyReasonPublisher"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltySettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unitToken"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->maybeLoyaltyEventPublisher:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iput-object p2, p0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->missedLoyaltyReasonPublisher:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iput-object p3, p0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    iput-object p4, p0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->unitToken:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/loyalty/LoyaltySettings;Ljava/lang/String;Lcom/squareup/settings/server/Features;)V
    .locals 7
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/user/UserToken;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loyaltySettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unitToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v2

    const-string v0, "BehaviorRelay.create<MaybeLoyaltyEvent>()"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v3

    const-string v0, "BehaviorRelay.create<ReasonForNoStars>()"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    .line 44
    invoke-direct/range {v1 .. v6}, Lcom/squareup/loyalty/LoyaltyEventPublisher;-><init>(Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/jakewharton/rxrelay2/BehaviorRelay;Lcom/squareup/loyalty/LoyaltySettings;Ljava/lang/String;Lcom/squareup/settings/server/Features;)V

    return-void
.end method

.method private final publish(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->maybeLoyaltyEventPublisher:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final publishNonLoyaltyEvent(Lcom/squareup/payment/AddedTenderEvent;)V
    .locals 3

    .line 152
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->maybeLoyaltyEventPublisher:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 153
    new-instance v1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;

    iget-object v2, p1, Lcom/squareup/payment/AddedTenderEvent;->tenderIdPair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/payment/AddedTenderEvent;->billIdPair:Lcom/squareup/protos/client/IdPair;

    invoke-direct {v1, v2, p1}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V

    .line 152
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final serverHandlesReturnItems(Lcom/squareup/payment/AddedTenderEvent;)Z
    .locals 3

    .line 143
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_HANDLE_RETURN_ITEMS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/payment/AddedTenderEvent;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    if-eqz p1, :cond_0

    check-cast p1, Ljava/util/Collection;

    .line 144
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method private final shouldAlwaysShowLoyaltyScreen()Z
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->showNonQualifyingLoyaltyEvents()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyProgramActive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public getLoyaltyEvent()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
            ">;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->maybeLoyaltyEventPublisher:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    .line 157
    const-class v1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "ofType(T::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getMaybeLoyaltyEvent()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/loyalty/MaybeLoyaltyEvent;",
            ">;"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->maybeLoyaltyEventPublisher:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public getMissedLoyaltyReason()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;",
            ">;"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->missedLoyaltyReasonPublisher:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public maybeTriggerLoyaltyEvent(Lcom/squareup/payment/AddedTenderEvent;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "addedTenderEvent"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/AddedTenderEvent;->getAddedTender()Lcom/squareup/protos/client/bills/AddedTender;

    move-result-object v2

    .line 97
    iget-object v3, v2, Lcom/squareup/protos/client/bills/AddedTender;->loyalty_status:Ljava/util/List;

    const-string v4, "addedTender.loyalty_status"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/Iterable;

    .line 158
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v8, v5

    check-cast v8, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;

    .line 98
    iget-object v8, v8, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->type:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    sget-object v9, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->PRIMARY:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    if-ne v8, v9, :cond_1

    const/4 v8, 0x1

    goto :goto_0

    :cond_1
    const/4 v8, 0x0

    :goto_0
    if-eqz v8, :cond_0

    goto :goto_1

    :cond_2
    move-object v5, v7

    .line 159
    :goto_1
    check-cast v5, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;

    if-eqz v5, :cond_3

    goto :goto_2

    .line 99
    :cond_3
    iget-object v2, v2, Lcom/squareup/protos/client/bills/AddedTender;->loyalty_status:Ljava/util/List;

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v6}, Lkotlin/collections/CollectionsKt;->getOrNull(Ljava/util/List;I)Ljava/lang/Object;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;

    :goto_2
    if-eqz v5, :cond_4

    .line 103
    iget-object v7, v5, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    :cond_4
    if-eqz v7, :cond_8

    iget-object v2, v0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v2}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyScreensEnabled()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-direct/range {p0 .. p1}, Lcom/squareup/loyalty/LoyaltyEventPublisher;->serverHandlesReturnItems(Lcom/squareup/payment/AddedTenderEvent;)Z

    move-result v2

    if-eqz v2, :cond_5

    goto :goto_5

    .line 117
    :cond_5
    sget-object v6, Lcom/squareup/loyalty/MaybeLoyaltyEvent;->Companion:Lcom/squareup/loyalty/MaybeLoyaltyEvent$Companion;

    .line 118
    iget-object v7, v1, Lcom/squareup/payment/AddedTenderEvent;->tenderIdPair:Lcom/squareup/protos/client/IdPair;

    .line 119
    iget-object v8, v1, Lcom/squareup/payment/AddedTenderEvent;->billIdPair:Lcom/squareup/protos/client/IdPair;

    .line 120
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/AddedTenderEvent;->getTenderType()Lcom/squareup/protos/client/bills/Tender$Type;

    move-result-object v9

    .line 121
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/AddedTenderEvent;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v10

    .line 122
    iget-object v12, v5, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->punch_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    const-string v2, "loyaltyStatus.punch_status"

    invoke-static {v12, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    iget-object v2, v5, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;->is_enrolled:Ljava/lang/Boolean;

    const-string v3, "loyaltyStatus.is_enrolled"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    .line 124
    sget-object v13, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$EventContext;->ADD_TENDERS_RESPONSE:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$EventContext;

    .line 125
    iget-object v14, v0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->unitToken:Ljava/lang/String;

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 117
    invoke-virtual/range {v6 .. v16}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$Companion;->createLoyaltyEventFromLoyaltyStatus(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/Cart;ZLcom/squareup/protos/client/loyalty/LoyaltyStatus;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$EventContext;Ljava/lang/String;ZZ)Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    move-result-object v2

    .line 130
    iget-object v3, v2, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->type:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    sget-object v4, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;->EARN_POINTS:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    if-eq v3, v4, :cond_7

    invoke-direct/range {p0 .. p0}, Lcom/squareup/loyalty/LoyaltyEventPublisher;->shouldAlwaysShowLoyaltyScreen()Z

    move-result v3

    if-eqz v3, :cond_6

    goto :goto_3

    .line 133
    :cond_6
    invoke-direct/range {p0 .. p1}, Lcom/squareup/loyalty/LoyaltyEventPublisher;->publishNonLoyaltyEvent(Lcom/squareup/payment/AddedTenderEvent;)V

    goto :goto_4

    .line 131
    :cond_7
    :goto_3
    invoke-direct {v0, v2}, Lcom/squareup/loyalty/LoyaltyEventPublisher;->publish(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    :goto_4
    return-void

    .line 105
    :cond_8
    :goto_5
    invoke-direct/range {p0 .. p1}, Lcom/squareup/loyalty/LoyaltyEventPublisher;->publishNonLoyaltyEvent(Lcom/squareup/payment/AddedTenderEvent;)V

    .line 107
    iget-object v1, v0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->missedLoyaltyReasonPublisher:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->hasValue()Z

    move-result v1

    if-nez v1, :cond_a

    .line 108
    iget-object v1, v0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v1}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyScreensEnabled()Z

    move-result v1

    if-nez v1, :cond_9

    .line 109
    iget-object v1, v0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->missedLoyaltyReasonPublisher:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v2, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->CLIENT_DISABLED_LOYALTY:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_6

    .line 110
    :cond_9
    iget-object v1, v0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v1}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyProgramActive()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 111
    iget-object v1, v0, Lcom/squareup/loyalty/LoyaltyEventPublisher;->missedLoyaltyReasonPublisher:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v2, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->MISSING_LOYALTY_INFO:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_a
    :goto_6
    return-void
.end method
