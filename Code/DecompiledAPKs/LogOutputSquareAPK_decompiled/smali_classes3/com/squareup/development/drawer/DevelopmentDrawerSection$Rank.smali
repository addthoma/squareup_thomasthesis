.class public final enum Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;
.super Ljava/lang/Enum;
.source "DevelopmentDrawerSection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/development/drawer/DevelopmentDrawerSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Rank"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\r\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;",
        "",
        "(Ljava/lang/String;I)V",
        "APP_INFO",
        "NETWORK",
        "CONNECTIVITY",
        "FEATURES",
        "LEAK_CANARY",
        "HAIRBALL_MISC",
        "HAIRBALL_MAIN",
        "T2_EXTRAS",
        "X2_EXTRAS",
        "SIGN_IN_APP_ENDPOINTS",
        "LAST",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

.field public static final enum APP_INFO:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

.field public static final enum CONNECTIVITY:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

.field public static final enum FEATURES:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

.field public static final enum HAIRBALL_MAIN:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

.field public static final enum HAIRBALL_MISC:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

.field public static final enum LAST:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

.field public static final enum LEAK_CANARY:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

.field public static final enum NETWORK:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

.field public static final enum SIGN_IN_APP_ENDPOINTS:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

.field public static final enum T2_EXTRAS:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

.field public static final enum X2_EXTRAS:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    new-instance v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    const/4 v2, 0x0

    const-string v3, "APP_INFO"

    invoke-direct {v1, v3, v2}, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;->APP_INFO:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    const/4 v2, 0x1

    const-string v3, "NETWORK"

    invoke-direct {v1, v3, v2}, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;->NETWORK:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    const/4 v2, 0x2

    const-string v3, "CONNECTIVITY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;->CONNECTIVITY:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    const/4 v2, 0x3

    const-string v3, "FEATURES"

    invoke-direct {v1, v3, v2}, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;->FEATURES:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    const/4 v2, 0x4

    const-string v3, "LEAK_CANARY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;->LEAK_CANARY:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    const/4 v2, 0x5

    const-string v3, "HAIRBALL_MISC"

    invoke-direct {v1, v3, v2}, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;->HAIRBALL_MISC:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    const/4 v2, 0x6

    const-string v3, "HAIRBALL_MAIN"

    invoke-direct {v1, v3, v2}, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;->HAIRBALL_MAIN:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    const/4 v2, 0x7

    const-string v3, "T2_EXTRAS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;->T2_EXTRAS:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    const/16 v2, 0x8

    const-string v3, "X2_EXTRAS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;->X2_EXTRAS:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    const/16 v2, 0x9

    const-string v3, "SIGN_IN_APP_ENDPOINTS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;->SIGN_IN_APP_ENDPOINTS:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    const/16 v2, 0xa

    const-string v3, "LAST"

    invoke-direct {v1, v3, v2}, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;->LAST:Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;->$VALUES:[Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;
    .locals 1

    const-class v0, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    return-object p0
.end method

.method public static values()[Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;
    .locals 1

    sget-object v0, Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;->$VALUES:[Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    invoke-virtual {v0}, [Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;

    return-object v0
.end method
