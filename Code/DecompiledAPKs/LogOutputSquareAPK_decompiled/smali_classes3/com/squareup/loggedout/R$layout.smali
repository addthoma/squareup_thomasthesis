.class public final Lcom/squareup/loggedout/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loggedout/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final create_account_view:I = 0x7f0d010c

.field public static final landing_view:I = 0x7f0d0320

.field public static final logged_out_container:I = 0x7f0d0339

.field public static final splash_page_accept_payments:I = 0x7f0d04dc

.field public static final splash_page_build_trust:I = 0x7f0d04dd

.field public static final splash_page_button_bar:I = 0x7f0d04de

.field public static final splash_page_container:I = 0x7f0d04df

.field public static final splash_page_reports:I = 0x7f0d04e0

.field public static final splash_page_sell_in_minutes:I = 0x7f0d04e1

.field public static final text_above_image_splash_page:I = 0x7f0d0520

.field public static final text_above_image_splash_page_button_bar:I = 0x7f0d0521

.field public static final text_above_image_splash_page_container:I = 0x7f0d0522

.field public static final tour_popup_view:I = 0x7f0d055e

.field public static final world_landing_view:I = 0x7f0d0590


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
