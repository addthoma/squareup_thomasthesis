.class public Lcom/squareup/loggedout/NoLoggedOutStarter;
.super Ljava/lang/Object;
.source "NoLoggedOutStarter.java"

# interfaces
.implements Lcom/squareup/loggedout/LoggedOutStarter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loggedout/NoLoggedOutStarter$Module;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public showLogin(Landroid/content/Context;)V
    .locals 1

    .line 28
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "The logged out flow should never be launched, the logged out feature is not included."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public startLoggedOutActivity(Landroid/app/Activity;)V
    .locals 1

    .line 23
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "The logged out flow should never be launched, the logged out feature is not included."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
