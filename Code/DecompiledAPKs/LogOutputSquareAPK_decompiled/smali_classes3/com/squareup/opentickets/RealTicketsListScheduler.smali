.class public Lcom/squareup/opentickets/RealTicketsListScheduler;
.super Lcom/squareup/opentickets/TicketsScheduler;
.source "RealTicketsListScheduler.java"

# interfaces
.implements Lcom/squareup/opentickets/TicketsListScheduler;
.implements Lcom/squareup/jailkeeper/JailKeeperService;


# static fields
.field private static final FAST_POLL_INTERVAL_MS:J = 0x1388L

.field private static final SLOW_POLL_INTERVAL_MS:J = 0x7530L

.field private static final SYNC_PUSH_MESSAGE_TIMEOUT_MS:J = 0x1388L


# instance fields
.field final lock:Lcom/squareup/util/LockWithTimeout;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final registrar:Lcom/squareup/pushmessages/PushServiceRegistrar;

.field private final ticketsSyncPushMessageObservable:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/pushmessages/PushMessage$TicketsSyncPushMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/Tickets$InternalTickets;Lcom/squareup/util/Clock;Lcom/squareup/pushmessages/PushServiceRegistrar;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/pushmessages/PushMessageDelegate;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 48
    invoke-direct {p0, p1, p2}, Lcom/squareup/opentickets/TicketsScheduler;-><init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/Tickets$InternalTickets;)V

    .line 49
    iput-object p4, p0, Lcom/squareup/opentickets/RealTicketsListScheduler;->registrar:Lcom/squareup/pushmessages/PushServiceRegistrar;

    .line 50
    iput-object p5, p0, Lcom/squareup/opentickets/RealTicketsListScheduler;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 51
    const-class p1, Lcom/squareup/pushmessages/PushMessage$TicketsSyncPushMessage;

    invoke-interface {p6, p1}, Lcom/squareup/pushmessages/PushMessageDelegate;->observe(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/opentickets/RealTicketsListScheduler;->ticketsSyncPushMessageObservable:Lio/reactivex/Observable;

    .line 52
    new-instance p1, Lcom/squareup/util/LockWithTimeout;

    const-wide/16 p4, 0x1388

    invoke-direct {p1, p3, p4, p5}, Lcom/squareup/util/LockWithTimeout;-><init>(Lcom/squareup/util/Clock;J)V

    iput-object p1, p0, Lcom/squareup/opentickets/RealTicketsListScheduler;->lock:Lcom/squareup/util/LockWithTimeout;

    return-void
.end method

.method public static synthetic lambda$c3r5KO2sdCCU5jS7rFjLKoltBE4(Lcom/squareup/opentickets/RealTicketsListScheduler;Lcom/squareup/pushmessages/PushMessage$TicketsSyncPushMessage;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/opentickets/RealTicketsListScheduler;->pushMessageSync(Lcom/squareup/pushmessages/PushMessage$TicketsSyncPushMessage;)V

    return-void
.end method

.method private pushMessageSync(Lcom/squareup/pushmessages/PushMessage$TicketsSyncPushMessage;)V
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsListScheduler;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsListScheduler;->lock:Lcom/squareup/util/LockWithTimeout;

    invoke-virtual {v0}, Lcom/squareup/util/LockWithTimeout;->tryLock()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsListScheduler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/opentickets/-$$Lambda$RealTicketsListScheduler$y-gkhc72vpoMOJk57t7W5VXcgPw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/opentickets/-$$Lambda$RealTicketsListScheduler$y-gkhc72vpoMOJk57t7W5VXcgPw;-><init>(Lcom/squareup/opentickets/RealTicketsListScheduler;Lcom/squareup/pushmessages/PushMessage$TicketsSyncPushMessage;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method protected doSync()V
    .locals 3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Hitting local store for ticket list ticket infos."

    .line 79
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsListScheduler;->localTickets:Lcom/squareup/tickets/Tickets$InternalTickets;

    invoke-interface {v0}, Lcom/squareup/tickets/Tickets$InternalTickets;->getBroker()Lcom/squareup/tickets/TicketsBroker;

    move-result-object v0

    new-instance v1, Lcom/squareup/opentickets/-$$Lambda$RealTicketsListScheduler$6j9QoOI7Vw3Y3Q9fvoX5i-qH6_A;

    invoke-direct {v1, p0}, Lcom/squareup/opentickets/-$$Lambda$RealTicketsListScheduler$6j9QoOI7Vw3Y3Q9fvoX5i-qH6_A;-><init>(Lcom/squareup/opentickets/RealTicketsListScheduler;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lcom/squareup/tickets/TicketsBroker;->listTickets(Lcom/squareup/tickets/TicketsCallback;Ljava/lang/String;Lcom/squareup/util/LockWithTimeout;)V

    return-void
.end method

.method protected getInterval()J
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsListScheduler;->registrar:Lcom/squareup/pushmessages/PushServiceRegistrar;

    invoke-interface {v0}, Lcom/squareup/pushmessages/PushServiceRegistrar;->getPushServiceEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x7530

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x1388

    :goto_0
    return-wide v0
.end method

.method public synthetic lambda$doSync$0$RealTicketsListScheduler(Lcom/squareup/tickets/TicketsResult;)V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Local store has been updated."

    .line 81
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    invoke-interface {p1}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/opentickets/RealTicketsListScheduler;->onSyncFinished(Z)V

    return-void
.end method

.method public synthetic lambda$null$1$RealTicketsListScheduler(Lcom/squareup/tickets/TicketsResult;)V
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsListScheduler;->callback:Lcom/squareup/ui/ticket/TicketsListSchedulerListener;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsListScheduler;->callback:Lcom/squareup/ui/ticket/TicketsListSchedulerListener;

    invoke-interface {p1}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/ticket/TicketsListSchedulerListener;->onUpdate(Z)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$pushMessageSync$2$RealTicketsListScheduler(Lcom/squareup/pushmessages/PushMessage$TicketsSyncPushMessage;)V
    .locals 3

    .line 96
    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsListScheduler;->localTickets:Lcom/squareup/tickets/Tickets$InternalTickets;

    invoke-interface {v0}, Lcom/squareup/tickets/Tickets$InternalTickets;->getBroker()Lcom/squareup/tickets/TicketsBroker;

    move-result-object v0

    new-instance v1, Lcom/squareup/opentickets/-$$Lambda$RealTicketsListScheduler$9buaEBkHRMUqF0gMkR9Dg_VqLWo;

    invoke-direct {v1, p0}, Lcom/squareup/opentickets/-$$Lambda$RealTicketsListScheduler$9buaEBkHRMUqF0gMkR9Dg_VqLWo;-><init>(Lcom/squareup/opentickets/RealTicketsListScheduler;)V

    .line 100
    invoke-virtual {p1}, Lcom/squareup/pushmessages/PushMessage$TicketsSyncPushMessage;->getDcHint()Ljava/lang/String;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/opentickets/RealTicketsListScheduler;->lock:Lcom/squareup/util/LockWithTimeout;

    .line 96
    invoke-virtual {v0, v1, p1, v2}, Lcom/squareup/tickets/TicketsBroker;->listTickets(Lcom/squareup/tickets/TicketsCallback;Ljava/lang/String;Lcom/squareup/util/LockWithTimeout;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsListScheduler;->ticketsSyncPushMessageObservable:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/opentickets/-$$Lambda$RealTicketsListScheduler$c3r5KO2sdCCU5jS7rFjLKoltBE4;

    invoke-direct {v1, p0}, Lcom/squareup/opentickets/-$$Lambda$RealTicketsListScheduler$c3r5KO2sdCCU5jS7rFjLKoltBE4;-><init>(Lcom/squareup/opentickets/RealTicketsListScheduler;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public preload()Lio/reactivex/Completable;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsListScheduler;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/opentickets/-$$Lambda$dBK2eyzFryskH2KXB_Wn26gCLNQ;

    invoke-direct {v0, p0}, Lcom/squareup/opentickets/-$$Lambda$dBK2eyzFryskH2KXB_Wn26gCLNQ;-><init>(Lcom/squareup/opentickets/RealTicketsListScheduler;)V

    .line 66
    invoke-static {v0}, Lio/reactivex/Completable;->fromAction(Lio/reactivex/functions/Action;)Lio/reactivex/Completable;

    move-result-object v0

    goto :goto_0

    .line 67
    :cond_0
    invoke-static {}, Lio/reactivex/Completable;->complete()Lio/reactivex/Completable;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public reload()Lio/reactivex/Completable;
    .locals 1

    .line 71
    invoke-static {}, Lio/reactivex/Completable;->complete()Lio/reactivex/Completable;

    move-result-object v0

    return-object v0
.end method
