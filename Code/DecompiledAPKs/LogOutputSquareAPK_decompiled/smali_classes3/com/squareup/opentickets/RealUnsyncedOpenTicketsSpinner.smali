.class public final Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;
.super Ljava/lang/Object;
.source "RealUnsyncedOpenTicketsSpinner.kt"

# interfaces
.implements Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0010\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u0007H\u0016R2\u0010\u0005\u001a&\u0012\u000c\u0012\n \u0008*\u0004\u0018\u00010\u00070\u0007 \u0008*\u0012\u0012\u000c\u0012\n \u0008*\u0004\u0018\u00010\u00070\u0007\u0018\u00010\u00060\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;",
        "Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;",
        "spinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "(Lcom/squareup/register/widgets/GlassSpinner;)V",
        "onCheckOpenTicketCount",
        "Lcom/jakewharton/rxrelay/PublishRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "setUpSpinner",
        "",
        "view",
        "Landroid/view/View;",
        "showSpinner",
        "show",
        "open-tickets_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final onCheckOpenTicketCount:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final spinner:Lcom/squareup/register/widgets/GlassSpinner;


# direct methods
.method public constructor <init>(Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "spinner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 15
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;->onCheckOpenTicketCount:Lcom/jakewharton/rxrelay/PublishRelay;

    return-void
.end method

.method public static final synthetic access$getOnCheckOpenTicketCount$p(Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;->onCheckOpenTicketCount:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$getSpinner$p(Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;)Lcom/squareup/register/widgets/GlassSpinner;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-object p0
.end method


# virtual methods
.method public setUpSpinner(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    new-instance v0, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$1;

    invoke-direct {v0, p0}, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$1;-><init>(Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 26
    new-instance v0, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$2;-><init>(Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;Landroid/view/View;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public showSpinner(Z)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;->onCheckOpenTicketCount:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
