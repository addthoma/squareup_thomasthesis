.class final Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$1$1;
.super Ljava/lang/Object;
.source "RealUnsyncedOpenTicketsSpinner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$1;->invoke()Lrx/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Ljava/lang/Boolean;",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "call",
        "(Ljava/lang/Boolean;)Lcom/squareup/register/widgets/GlassSpinnerState;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$1$1;

    invoke-direct {v0}, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$1$1;-><init>()V

    sput-object v0, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$1$1;->INSTANCE:Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Boolean;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 2

    .line 21
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    const-string v1, "it"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    sget v1, Lcom/squareup/opentickets/R$string;->open_tickets_checking:I

    invoke-virtual {v0, p1, v1}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showDebouncedSpinner(ZI)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$1$1;->call(Ljava/lang/Boolean;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method
