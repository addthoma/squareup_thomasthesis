.class public final Lcom/squareup/opentickets/NoOpUnsyncedOpenTicketsSpinner;
.super Ljava/lang/Object;
.source "NoOpUnsyncedOpenTicketsSpinner.kt"

# interfaces
.implements Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\tH\u0016\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/opentickets/NoOpUnsyncedOpenTicketsSpinner;",
        "Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;",
        "()V",
        "setUpSpinner",
        "",
        "view",
        "Landroid/view/View;",
        "showSpinner",
        "show",
        "",
        "impl-noop_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/opentickets/NoOpUnsyncedOpenTicketsSpinner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5
    new-instance v0, Lcom/squareup/opentickets/NoOpUnsyncedOpenTicketsSpinner;

    invoke-direct {v0}, Lcom/squareup/opentickets/NoOpUnsyncedOpenTicketsSpinner;-><init>()V

    sput-object v0, Lcom/squareup/opentickets/NoOpUnsyncedOpenTicketsSpinner;->INSTANCE:Lcom/squareup/opentickets/NoOpUnsyncedOpenTicketsSpinner;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setUpSpinner(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public showSpinner(Z)V
    .locals 1

    .line 11
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Should not be able to display glass spinner for checking open tickets"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
