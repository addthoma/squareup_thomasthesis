.class public Lcom/squareup/opentickets/RealTicketsSweeperManager;
.super Ljava/lang/Object;
.source "RealTicketsSweeperManager.java"

# interfaces
.implements Lcom/squareup/opentickets/TicketsSweeperManager;


# instance fields
.field private final sweepers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/opentickets/TicketsScheduler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/opentickets/TicketsSyncSweeper;Lcom/squareup/opentickets/TicketDeleteClosedSweeper;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/opentickets/TicketsScheduler;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    .line 17
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/opentickets/RealTicketsSweeperManager;->sweepers:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public schedulePeriodicSync()V
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsSweeperManager;->sweepers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/opentickets/TicketsScheduler;

    .line 25
    invoke-virtual {v1}, Lcom/squareup/opentickets/TicketsScheduler;->schedulePeriodicSync()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public stopSyncing()V
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsSweeperManager;->sweepers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/opentickets/TicketsScheduler;

    .line 34
    invoke-virtual {v1}, Lcom/squareup/opentickets/TicketsScheduler;->stopSyncing()V

    goto :goto_0

    :cond_0
    return-void
.end method
