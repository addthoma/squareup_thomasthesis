.class public final Lcom/squareup/disputes/AllDisputesAdapter$SummaryViewHolder;
.super Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;
.source "AllDisputesAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/AllDisputesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SummaryViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder<",
        "Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/disputes/AllDisputesAdapter$SummaryViewHolder;",
        "Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;",
        "Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "count",
        "Lcom/squareup/noho/NohoRow;",
        "disputed",
        "protection",
        "bind",
        "",
        "data",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final count:Lcom/squareup/noho/NohoRow;

.field private final disputed:Lcom/squareup/noho/NohoRow;

.field private final protection:Lcom/squareup/noho/NohoRow;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    invoke-direct {p0, p1}, Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 270
    sget v0, Lcom/squareup/disputes/R$id;->count_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$SummaryViewHolder;->count:Lcom/squareup/noho/NohoRow;

    .line 271
    sget v0, Lcom/squareup/disputes/R$id;->disputed_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$SummaryViewHolder;->disputed:Lcom/squareup/noho/NohoRow;

    .line 272
    sget v0, Lcom/squareup/disputes/R$id;->protection_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoRow;

    iput-object p1, p0, Lcom/squareup/disputes/AllDisputesAdapter$SummaryViewHolder;->protection:Lcom/squareup/noho/NohoRow;

    return-void
.end method


# virtual methods
.method public bind(Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;)V
    .locals 3

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 275
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$SummaryViewHolder;->count:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->getCount()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 276
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$SummaryViewHolder;->disputed:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->getDisputed()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 277
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$SummaryViewHolder;->protection:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;->getProtection()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic bind(Lcom/squareup/disputes/AllDisputesAdapter$Data;)V
    .locals 0

    .line 269
    check-cast p1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;

    invoke-virtual {p0, p1}, Lcom/squareup/disputes/AllDisputesAdapter$SummaryViewHolder;->bind(Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;)V

    return-void
.end method
