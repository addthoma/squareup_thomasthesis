.class public interface abstract Lcom/squareup/disputes/DisputesLoader;
.super Ljava/lang/Object;
.source "DisputesLoader.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0008`\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00040\u0003H&J\u0008\u0010\u0005\u001a\u00020\u0004H&J\u0018\u0010\u0006\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00040\u00032\u0006\u0010\u0007\u001a\u00020\u0008H&J\u0018\u0010\t\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00040\u00032\u0006\u0010\n\u001a\u00020\u000bH&\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesLoader;",
        "",
        "initialLoadDisputes",
        "Lio/reactivex/Single;",
        "Lcom/squareup/disputes/DisputesState;",
        "latestResponseState",
        "loadForm",
        "disputedPayment",
        "Lcom/squareup/protos/client/cbms/DisputedPayment;",
        "loadMore",
        "cursor",
        "",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract initialLoadDisputes()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/disputes/DisputesState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract latestResponseState()Lcom/squareup/disputes/DisputesState;
.end method

.method public abstract loadForm(Lcom/squareup/protos/client/cbms/DisputedPayment;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/disputes/DisputesState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract loadMore(I)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/disputes/DisputesState;",
            ">;"
        }
    .end annotation
.end method
