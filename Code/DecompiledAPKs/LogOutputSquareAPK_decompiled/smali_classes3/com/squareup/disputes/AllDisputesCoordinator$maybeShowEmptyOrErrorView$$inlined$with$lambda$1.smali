.class final Lcom/squareup/disputes/AllDisputesCoordinator$maybeShowEmptyOrErrorView$$inlined$with$lambda$1;
.super Ljava/lang/Object;
.source "AllDisputesCoordinator.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/disputes/AllDisputesCoordinator;->maybeShowEmptyOrErrorView(Lcom/squareup/disputes/AllDisputes$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "run",
        "com/squareup/disputes/AllDisputesCoordinator$maybeShowEmptyOrErrorView$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screenData$inlined:Lcom/squareup/disputes/AllDisputes$ScreenData;

.field final synthetic $workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;


# direct methods
.method constructor <init>(Lcom/squareup/disputes/AllDisputes$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/disputes/AllDisputesCoordinator$maybeShowEmptyOrErrorView$$inlined$with$lambda$1;->$screenData$inlined:Lcom/squareup/disputes/AllDisputes$ScreenData;

    iput-object p2, p0, Lcom/squareup/disputes/AllDisputesCoordinator$maybeShowEmptyOrErrorView$$inlined$with$lambda$1;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator$maybeShowEmptyOrErrorView$$inlined$with$lambda$1;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v1, Lcom/squareup/disputes/AllDisputesEvent$TryLoadDisputesAgain;->INSTANCE:Lcom/squareup/disputes/AllDisputesEvent$TryLoadDisputesAgain;

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
