.class public abstract Lcom/squareup/disputes/RealHandlesDisputesModule;
.super Ljava/lang/Object;
.source "RealHandlesDisputesModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/disputes/RealHandlesDisputesModule$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\'\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\n\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/disputes/RealHandlesDisputesModule;",
        "",
        "()V",
        "bindHandlesDisputes",
        "Lcom/squareup/disputes/api/HandlesDisputes;",
        "impl",
        "Lcom/squareup/disputes/RealHandlesDisputes;",
        "bindHandlesDisputes$disputes_release",
        "bindHandlesDisputesIntoLoginScope",
        "Lmortar/Scoped;",
        "bindHandlesDisputesIntoLoginScope$disputes_release",
        "Companion",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/disputes/RealHandlesDisputesModule$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/disputes/RealHandlesDisputesModule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/disputes/RealHandlesDisputesModule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/disputes/RealHandlesDisputesModule;->Companion:Lcom/squareup/disputes/RealHandlesDisputesModule$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideLastSeenDisputesPopup(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .param p0    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/disputes/LastSeenDisputesPopup;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/disputes/RealHandlesDisputesModule;->Companion:Lcom/squareup/disputes/RealHandlesDisputesModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/disputes/RealHandlesDisputesModule$Companion;->provideLastSeenDisputesPopup(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method public static final provideLastSeenDisputesReport(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .param p0    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/disputes/LastSeenDisputesReport;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/disputes/RealHandlesDisputesModule;->Companion:Lcom/squareup/disputes/RealHandlesDisputesModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/disputes/RealHandlesDisputesModule$Companion;->provideLastSeenDisputesReport(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public abstract bindHandlesDisputes$disputes_release(Lcom/squareup/disputes/RealHandlesDisputes;)Lcom/squareup/disputes/api/HandlesDisputes;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindHandlesDisputesIntoLoginScope$disputes_release(Lcom/squareup/disputes/RealHandlesDisputes;)Lmortar/Scoped;
    .annotation runtime Lcom/squareup/ForLoggedIn;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method
