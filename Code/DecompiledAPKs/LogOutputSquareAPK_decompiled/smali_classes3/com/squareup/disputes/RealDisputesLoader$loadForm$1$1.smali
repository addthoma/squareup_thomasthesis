.class final Lcom/squareup/disputes/RealDisputesLoader$loadForm$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DisputesLoader.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/disputes/DisputesState$ChallengeSummaryState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/irf/GetFormResponse;",
        "Lcom/squareup/receiving/FailureMessage$Parts;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisputesLoader.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisputesLoader.kt\ncom/squareup/disputes/RealDisputesLoader$loadForm$1$1\n*L\n1#1,131:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/receiving/FailureMessage$Parts;",
        "it",
        "Lcom/squareup/protos/client/irf/GetFormResponse;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;


# direct methods
.method constructor <init>(Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/disputes/RealDisputesLoader$loadForm$1$1;->this$0:Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/protos/client/irf/GetFormResponse;)Lcom/squareup/receiving/FailureMessage$Parts;
    .locals 3

    .line 98
    iget-object p1, p1, Lcom/squareup/protos/client/irf/GetFormResponse;->status:Lcom/squareup/protos/client/Status;

    new-instance v0, Lcom/squareup/receiving/FailureMessage$Parts;

    if-eqz p1, :cond_0

    invoke-direct {v0, p1}, Lcom/squareup/receiving/FailureMessage$Parts;-><init>(Lcom/squareup/protos/client/Status;)V

    goto :goto_0

    .line 99
    :cond_0
    iget-object p1, p0, Lcom/squareup/disputes/RealDisputesLoader$loadForm$1$1;->this$0:Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;

    iget-object p1, p1, Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;->this$0:Lcom/squareup/disputes/RealDisputesLoader;

    invoke-static {p1}, Lcom/squareup/disputes/RealDisputesLoader;->access$getRes$p(Lcom/squareup/disputes/RealDisputesLoader;)Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/squareup/disputes/R$string;->disputes_error_title:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 100
    iget-object v1, p0, Lcom/squareup/disputes/RealDisputesLoader$loadForm$1$1;->this$0:Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;

    iget-object v1, v1, Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;->this$0:Lcom/squareup/disputes/RealDisputesLoader;

    invoke-static {v1}, Lcom/squareup/disputes/RealDisputesLoader;->access$getRes$p(Lcom/squareup/disputes/RealDisputesLoader;)Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/disputes/R$string;->disputes_error_try_again:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 98
    invoke-direct {v0, p1, v1}, Lcom/squareup/receiving/FailureMessage$Parts;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/protos/client/irf/GetFormResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/disputes/RealDisputesLoader$loadForm$1$1;->invoke(Lcom/squareup/protos/client/irf/GetFormResponse;)Lcom/squareup/receiving/FailureMessage$Parts;

    move-result-object p1

    return-object p1
.end method
