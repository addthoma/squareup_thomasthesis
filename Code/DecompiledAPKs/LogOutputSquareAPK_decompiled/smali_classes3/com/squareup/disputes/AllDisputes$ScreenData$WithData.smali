.class public final Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;
.super Lcom/squareup/disputes/AllDisputes$ScreenData;
.source "AllDisputesScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/AllDisputes$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WithData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u000f\u0018\u00002\u00020\u0001BI\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u0007\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u0012\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u00a2\u0006\u0002\u0010\u000fR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\n\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0013R\u0017\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0017\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001a\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;",
        "Lcom/squareup/disputes/AllDisputes$ScreenData;",
        "moreType",
        "Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;",
        "nextCursor",
        "",
        "protection",
        "Lcom/squareup/protos/common/Money;",
        "count",
        "",
        "disputed",
        "unresolvedDisputes",
        "",
        "Lcom/squareup/protos/client/cbms/DisputedPayment;",
        "resolvedDisputes",
        "(Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;ILcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;)V",
        "getCount",
        "()J",
        "getDisputed",
        "()Lcom/squareup/protos/common/Money;",
        "getMoreType",
        "()Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;",
        "getNextCursor",
        "()I",
        "getProtection",
        "getResolvedDisputes",
        "()Ljava/util/List;",
        "getUnresolvedDisputes",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final count:J

.field private final disputed:Lcom/squareup/protos/common/Money;

.field private final moreType:Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;

.field private final nextCursor:I

.field private final protection:Lcom/squareup/protos/common/Money;

.field private final resolvedDisputes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;"
        }
    .end annotation
.end field

.field private final unresolvedDisputes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;ILcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;",
            "I",
            "Lcom/squareup/protos/common/Money;",
            "J",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;)V"
        }
    .end annotation

    const-string v0, "moreType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "protection"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "disputed"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unresolvedDisputes"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resolvedDisputes"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, v0}, Lcom/squareup/disputes/AllDisputes$ScreenData;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->moreType:Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;

    iput p2, p0, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->nextCursor:I

    iput-object p3, p0, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->protection:Lcom/squareup/protos/common/Money;

    iput-wide p4, p0, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->count:J

    iput-object p6, p0, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->disputed:Lcom/squareup/protos/common/Money;

    iput-object p7, p0, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->unresolvedDisputes:Ljava/util/List;

    iput-object p8, p0, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->resolvedDisputes:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final getCount()J
    .locals 2

    .line 23
    iget-wide v0, p0, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->count:J

    return-wide v0
.end method

.method public final getDisputed()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->disputed:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getMoreType()Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->moreType:Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;

    return-object v0
.end method

.method public final getNextCursor()I
    .locals 1

    .line 21
    iget v0, p0, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->nextCursor:I

    return v0
.end method

.method public final getProtection()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->protection:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getResolvedDisputes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->resolvedDisputes:Ljava/util/List;

    return-object v0
.end method

.method public final getUnresolvedDisputes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->unresolvedDisputes:Ljava/util/List;

    return-object v0
.end method
