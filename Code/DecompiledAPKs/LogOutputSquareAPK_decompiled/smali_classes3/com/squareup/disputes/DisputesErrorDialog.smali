.class public final Lcom/squareup/disputes/DisputesErrorDialog;
.super Ljava/lang/Object;
.source "DisputesErrorDialogScreen.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/disputes/DisputesErrorDialog$ScreenData;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u00c0\u0002\u0018\u00002\u00020\u0001:\u0001\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u001d\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesErrorDialog;",
        "",
        "()V",
        "KEY",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/disputes/DisputesErrorDialog$ScreenData;",
        "Lcom/squareup/disputes/ErrorDialogEvent;",
        "getKEY",
        "()Lcom/squareup/workflow/legacy/Screen$Key;",
        "ScreenData",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/disputes/DisputesErrorDialog;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/disputes/DisputesErrorDialog$ScreenData;",
            "Lcom/squareup/disputes/ErrorDialogEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 11
    new-instance v0, Lcom/squareup/disputes/DisputesErrorDialog;

    invoke-direct {v0}, Lcom/squareup/disputes/DisputesErrorDialog;-><init>()V

    sput-object v0, Lcom/squareup/disputes/DisputesErrorDialog;->INSTANCE:Lcom/squareup/disputes/DisputesErrorDialog;

    .line 12
    new-instance v1, Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-direct {v1, v0}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lcom/squareup/disputes/DisputesErrorDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getKEY()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/disputes/DisputesErrorDialog$ScreenData;",
            "Lcom/squareup/disputes/ErrorDialogEvent;",
            ">;"
        }
    .end annotation

    .line 12
    sget-object v0, Lcom/squareup/disputes/DisputesErrorDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method
