.class public final Lcom/squareup/disputes/SummaryAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SummaryAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/disputes/SummaryAdapter$ViewType;,
        Lcom/squareup/disputes/SummaryAdapter$FormData;,
        Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder;,
        Lcom/squareup/disputes/SummaryAdapter$TextAnswerViewHolder;,
        Lcom/squareup/disputes/SummaryAdapter$FileUploadViewHolder;,
        Lcom/squareup/disputes/SummaryAdapter$TitleViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder<",
        "+",
        "Lcom/squareup/disputes/SummaryAdapter$FormData;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSummaryAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SummaryAdapter.kt\ncom/squareup/disputes/SummaryAdapter\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 ArraysJVM.kt\nkotlin/collections/ArraysKt__ArraysJVMKt\n*L\n1#1,136:1\n1265#2,12:137\n1360#2:149\n1429#2,3:150\n37#3,2:153\n*E\n*S KotlinDebug\n*F\n+ 1 SummaryAdapter.kt\ncom/squareup/disputes/SummaryAdapter\n*L\n59#1,12:137\n131#1:149\n131#1,3:150\n131#1,2:153\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008\u0000\u0018\u00002\u0010\u0012\u000c\u0012\n\u0012\u0006\u0008\u0001\u0012\u00020\u00030\u00020\u0001:\u0006\u0018\u0019\u001a\u001b\u001c\u001dB\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0010\u0010\t\u001a\u00020\u00082\u0006\u0010\n\u001a\u00020\u0008H\u0016J\u000e\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eJ \u0010\u000f\u001a\u00020\u000c2\u000e\u0010\u0010\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00030\u00022\u0006\u0010\n\u001a\u00020\u0008H\u0016J \u0010\u0011\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00030\u00022\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0008H\u0016J\u0016\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00062\u0006\u0010\u0016\u001a\u00020\u0017H\u0002R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0006X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/disputes/SummaryAdapter;",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder;",
        "Lcom/squareup/disputes/SummaryAdapter$FormData;",
        "()V",
        "rows",
        "",
        "getItemCount",
        "",
        "getItemViewType",
        "position",
        "init",
        "",
        "challengeEngine",
        "Lcom/squareup/disputes/ChallengeEngine;",
        "onBindViewHolder",
        "holder",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "viewType",
        "toData",
        "field",
        "Lcom/squareup/disputes/Field;",
        "BaseViewHolder",
        "FileUploadViewHolder",
        "FormData",
        "TextAnswerViewHolder",
        "TitleViewHolder",
        "ViewType",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private rows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/disputes/SummaryAdapter$FormData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    return-void
.end method

.method private final toData(Lcom/squareup/disputes/Field;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/Field;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/disputes/SummaryAdapter$FormData;",
            ">;"
        }
    .end annotation

    .line 111
    instance-of v0, p1, Lcom/squareup/disputes/Field$SelectField;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz v0, :cond_0

    new-array v0, v3, [Lcom/squareup/disputes/SummaryAdapter$FormData;

    .line 112
    new-instance v3, Lcom/squareup/disputes/SummaryAdapter$FormData$Title;

    invoke-virtual {p1}, Lcom/squareup/disputes/Field;->getSectionHeader()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/squareup/disputes/SummaryAdapter$FormData$Title;-><init>(Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/disputes/SummaryAdapter$FormData;

    aput-object v3, v0, v2

    .line 113
    new-instance v2, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;

    new-instance v3, Lcom/squareup/util/ViewString$TextString;

    check-cast p1, Lcom/squareup/disputes/Field$SelectField;

    invoke-virtual {p1}, Lcom/squareup/disputes/Field$SelectField;->formattedAnswer()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-direct {v3, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/squareup/util/ViewString;

    invoke-direct {v2, v3}, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;-><init>(Lcom/squareup/util/ViewString;)V

    check-cast v2, Lcom/squareup/disputes/SummaryAdapter$FormData;

    aput-object v2, v0, v1

    .line 111
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto/16 :goto_1

    .line 115
    :cond_0
    instance-of v0, p1, Lcom/squareup/disputes/Field$TextField;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/squareup/disputes/Field;->isSkipped()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    goto/16 :goto_1

    :cond_1
    new-array v0, v3, [Lcom/squareup/disputes/SummaryAdapter$FormData;

    .line 119
    new-instance v3, Lcom/squareup/disputes/SummaryAdapter$FormData$Title;

    invoke-virtual {p1}, Lcom/squareup/disputes/Field;->getSectionHeader()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/squareup/disputes/SummaryAdapter$FormData$Title;-><init>(Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/disputes/SummaryAdapter$FormData;

    aput-object v3, v0, v2

    .line 120
    new-instance v2, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;

    new-instance v3, Lcom/squareup/util/ViewString$TextString;

    check-cast p1, Lcom/squareup/disputes/Field$TextField;

    invoke-virtual {p1}, Lcom/squareup/disputes/Field$TextField;->getAnswer()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-direct {v3, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/squareup/util/ViewString;

    invoke-direct {v2, v3}, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;-><init>(Lcom/squareup/util/ViewString;)V

    check-cast v2, Lcom/squareup/disputes/SummaryAdapter$FormData;

    aput-object v2, v0, v1

    .line 118
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto/16 :goto_1

    .line 123
    :cond_2
    instance-of v0, p1, Lcom/squareup/disputes/Field$FileField;

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/squareup/disputes/Field;->isSkipped()Z

    move-result v0

    if-eqz v0, :cond_3

    new-array v0, v3, [Lcom/squareup/disputes/SummaryAdapter$FormData;

    .line 125
    new-instance v3, Lcom/squareup/disputes/SummaryAdapter$FormData$Title;

    invoke-virtual {p1}, Lcom/squareup/disputes/Field;->getSectionHeader()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v3, p1}, Lcom/squareup/disputes/SummaryAdapter$FormData$Title;-><init>(Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/disputes/SummaryAdapter$FormData;

    aput-object v3, v0, v2

    .line 126
    new-instance p1, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;

    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/disputes/R$string;->dispute_skipped_file_upload:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/util/ViewString;

    invoke-direct {p1, v2}, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;-><init>(Lcom/squareup/util/ViewString;)V

    check-cast p1, Lcom/squareup/disputes/SummaryAdapter$FormData;

    aput-object p1, v0, v1

    .line 124
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_1

    .line 129
    :cond_3
    new-instance v0, Lkotlin/jvm/internal/SpreadBuilder;

    invoke-direct {v0, v3}, Lkotlin/jvm/internal/SpreadBuilder;-><init>(I)V

    .line 130
    new-instance v1, Lcom/squareup/disputes/SummaryAdapter$FormData$Title;

    invoke-virtual {p1}, Lcom/squareup/disputes/Field;->getSectionHeader()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/squareup/disputes/SummaryAdapter$FormData$Title;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/disputes/SummaryAdapter$FormData;

    invoke-virtual {v0, v1}, Lkotlin/jvm/internal/SpreadBuilder;->add(Ljava/lang/Object;)V

    .line 131
    check-cast p1, Lcom/squareup/disputes/Field$FileField;

    invoke-virtual {p1}, Lcom/squareup/disputes/Field$FileField;->getFiles()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 149
    new-instance v1, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {p1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 150
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 151
    check-cast v3, Lcom/squareup/disputes/FileUpload;

    .line 131
    new-instance v4, Lcom/squareup/disputes/SummaryAdapter$FormData$FileUpload;

    invoke-virtual {v3}, Lcom/squareup/disputes/FileUpload;->getFormattedCategory()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Lcom/squareup/disputes/SummaryAdapter$FormData$FileUpload;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 152
    :cond_4
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    new-array p1, v2, [Lcom/squareup/disputes/SummaryAdapter$FormData$FileUpload;

    .line 154
    invoke-interface {v1, p1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_5

    invoke-virtual {v0, p1}, Lkotlin/jvm/internal/SpreadBuilder;->addSpread(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lkotlin/jvm/internal/SpreadBuilder;->size()I

    move-result p1

    new-array p1, p1, [Lcom/squareup/disputes/SummaryAdapter$FormData;

    invoke-virtual {v0, p1}, Lkotlin/jvm/internal/SpreadBuilder;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/squareup/disputes/SummaryAdapter$FormData;

    .line 129
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    :goto_1
    return-object p1

    .line 154
    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 123
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public getItemCount()I
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/disputes/SummaryAdapter;->rows:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v1, "rows"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/squareup/disputes/SummaryAdapter;->rows:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v1, "rows"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/disputes/SummaryAdapter$FormData;

    invoke-virtual {p1}, Lcom/squareup/disputes/SummaryAdapter$FormData;->getViewType()Lcom/squareup/disputes/SummaryAdapter$ViewType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/disputes/SummaryAdapter$ViewType;->ordinal()I

    move-result p1

    return p1
.end method

.method public final init(Lcom/squareup/disputes/ChallengeEngine;)V
    .locals 2

    const-string v0, "challengeEngine"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-virtual {p1}, Lcom/squareup/disputes/ChallengeEngine;->getSummary()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 144
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 145
    check-cast v1, Lcom/squareup/disputes/Field;

    .line 59
    invoke-direct {p0, v1}, Lcom/squareup/disputes/SummaryAdapter;->toData(Lcom/squareup/disputes/Field;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 146
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 148
    :cond_0
    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/disputes/SummaryAdapter;->rows:Ljava/util/List;

    return-void
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/disputes/SummaryAdapter;->onBindViewHolder(Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder<",
            "+",
            "Lcom/squareup/disputes/SummaryAdapter$FormData;",
            ">;I)V"
        }
    .end annotation

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/disputes/SummaryAdapter;->rows:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v1, "rows"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/disputes/SummaryAdapter$FormData;

    invoke-virtual {p1, p2}, Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder;->uncheckedBind(Lcom/squareup/disputes/SummaryAdapter$FormData;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 27
    invoke-virtual {p0, p1, p2}, Lcom/squareup/disputes/SummaryAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder<",
            "+",
            "Lcom/squareup/disputes/SummaryAdapter$FormData;",
            ">;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {}, Lcom/squareup/disputes/SummaryAdapter$ViewType;->values()[Lcom/squareup/disputes/SummaryAdapter$ViewType;

    move-result-object v0

    aget-object p2, v0, p2

    sget-object v0, Lcom/squareup/disputes/SummaryAdapter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/disputes/SummaryAdapter$ViewType;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_2

    const/4 v0, 0x2

    if-eq p2, v0, :cond_1

    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 39
    new-instance p2, Lcom/squareup/disputes/SummaryAdapter$FileUploadViewHolder;

    .line 40
    sget v0, Lcom/squareup/disputes/R$layout;->row_file_upload:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 39
    invoke-direct {p2, p1}, Lcom/squareup/disputes/SummaryAdapter$FileUploadViewHolder;-><init>(Landroid/view/View;)V

    check-cast p2, Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 36
    :cond_1
    new-instance p2, Lcom/squareup/disputes/SummaryAdapter$TextAnswerViewHolder;

    .line 37
    sget v0, Lcom/squareup/disputes/R$layout;->row_text_answer:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 36
    invoke-direct {p2, p1}, Lcom/squareup/disputes/SummaryAdapter$TextAnswerViewHolder;-><init>(Landroid/view/View;)V

    check-cast p2, Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder;

    goto :goto_0

    .line 35
    :cond_2
    new-instance p2, Lcom/squareup/disputes/SummaryAdapter$TitleViewHolder;

    sget v0, Lcom/squareup/disputes/R$layout;->row_title:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/disputes/SummaryAdapter$TitleViewHolder;-><init>(Landroid/view/View;)V

    check-cast p2, Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder;

    :goto_0
    return-object p2
.end method
