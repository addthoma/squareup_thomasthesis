.class public final Lcom/squareup/disputes/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final dispute_accept:I = 0x7f120894

.field public static final dispute_action_dialog_button:I = 0x7f120895

.field public static final dispute_action_dialog_message:I = 0x7f120896

.field public static final dispute_action_dialog_title:I = 0x7f120897

.field public static final dispute_card_type:I = 0x7f120898

.field public static final dispute_challenge:I = 0x7f120899

.field public static final dispute_challenge_submission:I = 0x7f12089a

.field public static final dispute_date:I = 0x7f12089b

.field public static final dispute_deadline:I = 0x7f12089c

.field public static final dispute_detail_description_accepted:I = 0x7f12089d

.field public static final dispute_detail_description_lost:I = 0x7f12089e

.field public static final dispute_detail_description_no_action_low:I = 0x7f12089f

.field public static final dispute_detail_description_no_action_refunded:I = 0x7f1208a0

.field public static final dispute_detail_description_pending:I = 0x7f1208a1

.field public static final dispute_detail_description_reopened:I = 0x7f1208a2

.field public static final dispute_detail_description_under_review:I = 0x7f1208a3

.field public static final dispute_detail_description_won:I = 0x7f1208a4

.field public static final dispute_detail_held_description:I = 0x7f1208a5

.field public static final dispute_detail_title_closed:I = 0x7f1208a6

.field public static final dispute_detail_title_open:I = 0x7f1208a7

.field public static final dispute_detail_title_pending:I = 0x7f1208a8

.field public static final dispute_detail_title_under_review:I = 0x7f1208a9

.field public static final dispute_held_amount:I = 0x7f1208aa

.field public static final dispute_list_action_required:I = 0x7f1208ab

.field public static final dispute_list_pending:I = 0x7f1208ac

.field public static final dispute_list_resolved_accepted:I = 0x7f1208ad

.field public static final dispute_list_resolved_cardholder:I = 0x7f1208ae

.field public static final dispute_list_resolved_merchant:I = 0x7f1208af

.field public static final dispute_list_summary_funds:I = 0x7f1208b0

.field public static final dispute_list_summary_protection:I = 0x7f1208b1

.field public static final dispute_list_summary_total:I = 0x7f1208b2

.field public static final dispute_load_more_error_body:I = 0x7f1208b3

.field public static final dispute_load_more_error_title:I = 0x7f1208b4

.field public static final dispute_reason:I = 0x7f1208b5

.field public static final dispute_reason_amount_differs:I = 0x7f1208b6

.field public static final dispute_reason_cancelled:I = 0x7f1208b7

.field public static final dispute_reason_compliance:I = 0x7f1208b8

.field public static final dispute_reason_customer_requests_credit:I = 0x7f1208b9

.field public static final dispute_reason_dissatisfied:I = 0x7f1208ba

.field public static final dispute_reason_duplicate:I = 0x7f1208bb

.field public static final dispute_reason_emv_liability_shift:I = 0x7f1208bc

.field public static final dispute_reason_fraud:I = 0x7f1208bd

.field public static final dispute_reason_no_knowledge:I = 0x7f1208be

.field public static final dispute_reason_not_as_described:I = 0x7f1208bf

.field public static final dispute_reason_not_received:I = 0x7f1208c0

.field public static final dispute_reason_paid_by_other_means:I = 0x7f1208c1

.field public static final dispute_reason_unauthorized:I = 0x7f1208c2

.field public static final dispute_reason_unknown:I = 0x7f1208c3

.field public static final dispute_receipt:I = 0x7f1208c4

.field public static final dispute_receipt_token:I = 0x7f1208c5

.field public static final dispute_skipped_file_upload:I = 0x7f1208c6

.field public static final dispute_view_submission:I = 0x7f1208c7

.field public static final disputed_amount:I = 0x7f1208c8

.field public static final disputes_empty_message:I = 0x7f1208c9

.field public static final disputes_empty_title:I = 0x7f1208ca

.field public static final disputes_error_title:I = 0x7f1208cb

.field public static final disputes_error_try_again:I = 0x7f1208cc

.field public static final disputes_learn_more:I = 0x7f1208cd

.field public static final disputes_learn_more_url:I = 0x7f1208ce

.field public static final disputes_notification_dialog_content:I = 0x7f1208cf

.field public static final disputes_notification_dialog_primary:I = 0x7f1208d0

.field public static final disputes_notification_dialog_title:I = 0x7f1208d1

.field public static final empty_disputes_title:I = 0x7f120a3d

.field public static final reports_disputes:I = 0x7f12168d

.field public static final support_center:I = 0x7f1218d9

.field public static final uppercase_closed_disputes:I = 0x7f121b21

.field public static final uppercase_open_disputes:I = 0x7f121b56

.field public static final uppercase_overview:I = 0x7f121b59

.field public static final uppercase_transaction:I = 0x7f121b8c


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
