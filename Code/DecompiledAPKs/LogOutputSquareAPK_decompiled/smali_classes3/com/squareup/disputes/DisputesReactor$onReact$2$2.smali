.class final Lcom/squareup/disputes/DisputesReactor$onReact$2$2;
.super Lkotlin/jvm/internal/Lambda;
.source "DisputesReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/disputes/DisputesReactor$onReact$2;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/disputes/DisputesState;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/disputes/DisputesState;",
        "+",
        "Lkotlin/Unit;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/disputes/DisputesState;",
        "",
        "it",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/disputes/DisputesReactor$onReact$2;


# direct methods
.method constructor <init>(Lcom/squareup/disputes/DisputesReactor$onReact$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/disputes/DisputesReactor$onReact$2$2;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$2;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/disputes/DisputesState;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/DisputesState;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/disputes/DisputesState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/disputes/DisputesReactor$onReact$2$2;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$2;

    iget-object v0, v0, Lcom/squareup/disputes/DisputesReactor$onReact$2;->this$0:Lcom/squareup/disputes/DisputesReactor;

    invoke-static {v0}, Lcom/squareup/disputes/DisputesReactor;->access$getHandlesDisputes$p(Lcom/squareup/disputes/DisputesReactor;)Lcom/squareup/disputes/api/HandlesDisputes;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/disputes/api/HandlesDisputes;->markReportSeen()V

    .line 85
    iget-object v0, p0, Lcom/squareup/disputes/DisputesReactor$onReact$2$2;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$2;

    iget-object v0, v0, Lcom/squareup/disputes/DisputesReactor$onReact$2;->this$0:Lcom/squareup/disputes/DisputesReactor;

    .line 87
    iget-object v1, p0, Lcom/squareup/disputes/DisputesReactor$onReact$2$2;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$2;

    iget-object v1, v1, Lcom/squareup/disputes/DisputesReactor$onReact$2;->$state:Lcom/squareup/disputes/DisputesState;

    check-cast v1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputes;

    invoke-virtual {v1}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputes;->getStartingPaymentToken()Ljava/lang/String;

    move-result-object v1

    .line 88
    iget-object v2, p0, Lcom/squareup/disputes/DisputesReactor$onReact$2$2;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$2;

    iget-object v2, v2, Lcom/squareup/disputes/DisputesReactor$onReact$2;->this$0:Lcom/squareup/disputes/DisputesReactor;

    invoke-static {v2}, Lcom/squareup/disputes/DisputesReactor;->access$getAnalytics$p(Lcom/squareup/disputes/DisputesReactor;)Lcom/squareup/analytics/Analytics;

    move-result-object v2

    .line 85
    invoke-static {v0, p1, v1, v2}, Lcom/squareup/disputes/DisputesReactor;->access$changeStateForPaymentTokenAfterLoadingDisputes(Lcom/squareup/disputes/DisputesReactor;Lcom/squareup/disputes/DisputesState;Ljava/lang/String;Lcom/squareup/analytics/Analytics;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/disputes/DisputesState;

    invoke-virtual {p0, p1}, Lcom/squareup/disputes/DisputesReactor$onReact$2$2;->invoke(Lcom/squareup/disputes/DisputesState;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method
