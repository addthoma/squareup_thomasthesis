.class final Lcom/squareup/disputes/DisputesWorkflowRunner$onEnterScope$2;
.super Ljava/lang/Object;
.source "DisputesWorkflowRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/disputes/DisputesWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisputesWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisputesWorkflowRunner.kt\ncom/squareup/disputes/DisputesWorkflowRunner$onEnterScope$2\n+ 2 PosContainer.kt\ncom/squareup/ui/main/PosContainers\n*L\n1#1,100:1\n152#2:101\n*E\n*S KotlinDebug\n*F\n+ 1 DisputesWorkflowRunner.kt\ncom/squareup/disputes/DisputesWorkflowRunner$onEnterScope$2\n*L\n55#1:101\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/disputes/DisputesWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/disputes/DisputesWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/disputes/DisputesWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/disputes/DisputesWorkflowRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/disputes/DisputesWorkflowRunner$onEnterScope$2;->accept(Lkotlin/Unit;)V

    return-void
.end method

.method public final accept(Lkotlin/Unit;)V
    .locals 3

    .line 55
    iget-object p1, p0, Lcom/squareup/disputes/DisputesWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/disputes/DisputesWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/disputes/DisputesWorkflowRunner;->access$getContainer$p(Lcom/squareup/disputes/DisputesWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    .line 101
    const-class v1, Lcom/squareup/container/WorkflowTreeKey;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/PosContainer;->goBackPast([Ljava/lang/Class;)V

    return-void
.end method
