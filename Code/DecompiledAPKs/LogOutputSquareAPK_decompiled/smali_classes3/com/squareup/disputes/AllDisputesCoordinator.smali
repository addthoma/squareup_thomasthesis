.class public final Lcom/squareup/disputes/AllDisputesCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AllDisputesCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/disputes/AllDisputesCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAllDisputesCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AllDisputesCoordinator.kt\ncom/squareup/disputes/AllDisputesCoordinator\n*L\n1#1,170:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001:\u0001(B+\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0018H\u0016J\u0010\u0010\u001c\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0018H\u0002J\u001e\u0010\u001d\u001a\u00020\u001a2\u0006\u0010\u001e\u001a\u00020\u00052\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00060 H\u0002J\u001e\u0010!\u001a\u00020\u001a2\u0006\u0010\u001e\u001a\u00020\u00052\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00060 H\u0002J\u0010\u0010\"\u001a\u00020\u001a2\u0006\u0010\u001e\u001a\u00020\u0005H\u0002J \u0010#\u001a\u00020\u001a2\u0016\u0010$\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J\u001e\u0010%\u001a\u00020\u001a2\u0006\u0010\u001e\u001a\u00020\u00052\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00060 H\u0002J\u000c\u0010&\u001a\u00020\'*\u00020\u0005H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/disputes/AllDisputesCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/disputes/AllDisputes$ScreenData;",
        "Lcom/squareup/disputes/AllDisputesEvent;",
        "Lcom/squareup/disputes/AllDisputesScreen;",
        "adapter",
        "Lcom/squareup/disputes/AllDisputesAdapter;",
        "(Lio/reactivex/Observable;Lcom/squareup/disputes/AllDisputesAdapter;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "context",
        "Landroid/content/Context;",
        "emptyViewMessage",
        "",
        "errorContainer",
        "Landroid/view/ViewGroup;",
        "errorView",
        "Lcom/squareup/noho/NohoMessageView;",
        "list",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "spinner",
        "Landroid/view/View;",
        "attach",
        "",
        "view",
        "bindViews",
        "maybeShowDisputesList",
        "screenData",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "maybeShowEmptyOrErrorView",
        "maybeShowLoadingSpinner",
        "onScreen",
        "screen",
        "setUpActionBar",
        "hasDisputes",
        "",
        "Factory",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final adapter:Lcom/squareup/disputes/AllDisputesAdapter;

.field private context:Landroid/content/Context;

.field private emptyViewMessage:Ljava/lang/CharSequence;

.field private errorContainer:Landroid/view/ViewGroup;

.field private errorView:Lcom/squareup/noho/NohoMessageView;

.field private list:Landroidx/recyclerview/widget/RecyclerView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/disputes/AllDisputes$ScreenData;",
            "Lcom/squareup/disputes/AllDisputesEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private spinner:Landroid/view/View;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/disputes/AllDisputesAdapter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/disputes/AllDisputes$ScreenData;",
            "Lcom/squareup/disputes/AllDisputesEvent;",
            ">;>;",
            "Lcom/squareup/disputes/AllDisputesAdapter;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "adapter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->adapter:Lcom/squareup/disputes/AllDisputesAdapter;

    return-void
.end method

.method public static final synthetic access$onScreen(Lcom/squareup/disputes/AllDisputesCoordinator;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/disputes/AllDisputesCoordinator;->onScreen(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 158
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "view.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->context:Landroid/content/Context;

    .line 159
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 160
    sget v0, Lcom/squareup/disputes/R$id;->disputes_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->list:Landroidx/recyclerview/widget/RecyclerView;

    .line 161
    sget v0, Lcom/squareup/disputes/R$id;->disputes_error_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->errorContainer:Landroid/view/ViewGroup;

    .line 162
    sget v0, Lcom/squareup/disputes/R$id;->disputes_error:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    iput-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->errorView:Lcom/squareup/noho/NohoMessageView;

    .line 163
    sget v0, Lcom/squareup/disputes/R$id;->spinner:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->spinner:Landroid/view/View;

    return-void
.end method

.method private final hasDisputes(Lcom/squareup/disputes/AllDisputes$ScreenData;)Z
    .locals 2

    .line 167
    instance-of v0, p1, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->getResolvedDisputes()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v1

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->getUnresolvedDisputes()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method private final maybeShowDisputesList(Lcom/squareup/disputes/AllDisputes$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/AllDisputes$ScreenData;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/disputes/AllDisputesEvent;",
            ">;)V"
        }
    .end annotation

    .line 123
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->list:Landroidx/recyclerview/widget/RecyclerView;

    const-string v1, "list"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/squareup/disputes/AllDisputesCoordinator;->hasDisputes(Lcom/squareup/disputes/AllDisputes$ScreenData;)Z

    move-result v2

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 125
    instance-of v0, p1, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;

    if-eqz v0, :cond_5

    .line 126
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->list:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    move-result-object v0

    if-nez v0, :cond_4

    .line 127
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->adapter:Lcom/squareup/disputes/AllDisputesAdapter;

    check-cast p1, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;

    iget-object v2, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->context:Landroid/content/Context;

    if-nez v2, :cond_2

    const-string v3, "context"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, p1, p2, v2}, Lcom/squareup/disputes/AllDisputesAdapter;->init(Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/Context;)V

    .line 128
    iget-object p1, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->list:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object p2, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->adapter:Lcom/squareup/disputes/AllDisputesAdapter;

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    goto :goto_0

    .line 130
    :cond_4
    iget-object p2, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->adapter:Lcom/squareup/disputes/AllDisputesAdapter;

    .line 131
    check-cast p1, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->getResolvedDisputes()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->getNextCursor()I

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->getMoreType()Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;

    move-result-object p1

    .line 130
    invoke-virtual {p2, v0, v1, p1}, Lcom/squareup/disputes/AllDisputesAdapter;->updateResolved(Ljava/util/List;ILcom/squareup/disputes/AllDisputes$ScreenData$MoreType;)V

    :cond_5
    :goto_0
    return-void
.end method

.method private final maybeShowEmptyOrErrorView(Lcom/squareup/disputes/AllDisputes$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/AllDisputes$ScreenData;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/disputes/AllDisputesEvent;",
            ">;)V"
        }
    .end annotation

    .line 92
    instance-of v0, p1, Lcom/squareup/disputes/AllDisputes$ScreenData$DisputesError;

    const-string v1, "errorView"

    const/4 v2, 0x0

    const-string v3, "errorContainer"

    if-eqz v0, :cond_2

    .line 93
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->errorContainer:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->errorView:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 96
    :cond_1
    sget v1, Lcom/squareup/vectoricons/R$drawable;->circle_alert_96:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setDrawable(I)V

    .line 97
    move-object v1, p1

    check-cast v1, Lcom/squareup/disputes/AllDisputes$ScreenData$DisputesError;

    invoke-virtual {v1}, Lcom/squareup/disputes/AllDisputes$ScreenData$DisputesError;->getTitle()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 98
    invoke-virtual {v1}, Lcom/squareup/disputes/AllDisputes$ScreenData$DisputesError;->getMessage()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setMessage(Ljava/lang/CharSequence;)V

    .line 99
    invoke-virtual {v0}, Lcom/squareup/noho/NohoMessageView;->showPrimaryButton()V

    .line 100
    sget v1, Lcom/squareup/disputes/R$string;->disputes_error_try_again:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonText(I)V

    .line 101
    new-instance v1, Lcom/squareup/disputes/AllDisputesCoordinator$maybeShowEmptyOrErrorView$$inlined$with$lambda$1;

    invoke-direct {v1, p1, p2}, Lcom/squareup/disputes/AllDisputesCoordinator$maybeShowEmptyOrErrorView$$inlined$with$lambda$1;-><init>(Lcom/squareup/disputes/AllDisputes$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    const-string p2, "debounceRunnable {\n     \u2026dDisputesAgain)\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    goto :goto_0

    .line 105
    :cond_2
    instance-of p2, p1, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;

    if-eqz p2, :cond_6

    invoke-direct {p0, p1}, Lcom/squareup/disputes/AllDisputesCoordinator;->hasDisputes(Lcom/squareup/disputes/AllDisputes$ScreenData;)Z

    move-result p1

    if-nez p1, :cond_6

    .line 106
    iget-object p1, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->errorContainer:Landroid/view/ViewGroup;

    if-nez p1, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 108
    iget-object p1, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->errorView:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 109
    :cond_4
    sget p2, Lcom/squareup/disputes/R$drawable;->icon_document_128:I

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoMessageView;->setDrawable(I)V

    .line 110
    sget p2, Lcom/squareup/disputes/R$string;->disputes_empty_title:I

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 111
    iget-object p2, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->emptyViewMessage:Ljava/lang/CharSequence;

    if-nez p2, :cond_5

    const-string v0, "emptyViewMessage"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoMessageView;->setMessage(Ljava/lang/CharSequence;)V

    .line 112
    invoke-virtual {p1}, Lcom/squareup/noho/NohoMessageView;->hidePrimaryButton()V

    goto :goto_0

    .line 115
    :cond_6
    iget-object p1, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->errorContainer:Landroid/view/ViewGroup;

    if-nez p1, :cond_7

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private final maybeShowLoadingSpinner(Lcom/squareup/disputes/AllDisputes$ScreenData;)V
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->spinner:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "spinner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    instance-of p1, p1, Lcom/squareup/disputes/AllDisputes$ScreenData$Loading;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final onScreen(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/disputes/AllDisputes$ScreenData;",
            "Lcom/squareup/disputes/AllDisputesEvent;",
            ">;)V"
        }
    .end annotation

    .line 75
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/disputes/AllDisputes$ScreenData;

    .line 76
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 78
    invoke-direct {p0, v0, p1}, Lcom/squareup/disputes/AllDisputesCoordinator;->setUpActionBar(Lcom/squareup/disputes/AllDisputes$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 79
    invoke-direct {p0, v0}, Lcom/squareup/disputes/AllDisputesCoordinator;->maybeShowLoadingSpinner(Lcom/squareup/disputes/AllDisputes$ScreenData;)V

    .line 80
    invoke-direct {p0, v0, p1}, Lcom/squareup/disputes/AllDisputesCoordinator;->maybeShowDisputesList(Lcom/squareup/disputes/AllDisputes$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 81
    invoke-direct {p0, v0, p1}, Lcom/squareup/disputes/AllDisputesCoordinator;->maybeShowEmptyOrErrorView(Lcom/squareup/disputes/AllDisputes$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method private final setUpActionBar(Lcom/squareup/disputes/AllDisputes$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/AllDisputes$ScreenData;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/disputes/AllDisputesEvent;",
            ">;)V"
        }
    .end annotation

    .line 154
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 141
    :cond_0
    new-instance v9, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v9}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 144
    iget-object v2, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v2, Landroid/view/View;

    invoke-static {v2}, Lcom/squareup/container/ContainerKt;->getScreenSize(Landroid/view/View;)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isMasterDetail()Z

    move-result v1

    if-nez v1, :cond_2

    .line 145
    sget-object v1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/disputes/AllDisputesCoordinator$setUpActionBar$$inlined$apply$lambda$1;

    invoke-direct {v2, p0, p2, p1}, Lcom/squareup/disputes/AllDisputesCoordinator$setUpActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/disputes/AllDisputesCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/disputes/AllDisputes$ScreenData;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v9, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 147
    :cond_2
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/disputes/R$string;->reports_disputes:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v9, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 148
    invoke-direct {p0, p1}, Lcom/squareup/disputes/AllDisputesCoordinator;->hasDisputes(Lcom/squareup/disputes/AllDisputes$ScreenData;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 149
    sget-object v3, Lcom/squareup/noho/NohoActionButtonStyle;->SECONDARY:Lcom/squareup/noho/NohoActionButtonStyle;

    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/disputes/R$string;->disputes_learn_more:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v4, v1

    check-cast v4, Lcom/squareup/resources/TextModel;

    const/4 v5, 0x0

    new-instance v1, Lcom/squareup/disputes/AllDisputesCoordinator$setUpActionBar$$inlined$apply$lambda$2;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/disputes/AllDisputesCoordinator$setUpActionBar$$inlined$apply$lambda$2;-><init>(Lcom/squareup/disputes/AllDisputesCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/disputes/AllDisputes$ScreenData;)V

    move-object v6, v1

    check-cast v6, Lkotlin/jvm/functions/Function0;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, v9

    invoke-static/range {v2 .. v8}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 154
    :cond_3
    invoke-virtual {v9}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/disputes/AllDisputesCoordinator;->bindViews(Landroid/view/View;)V

    .line 60
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 61
    sget v1, Lcom/squareup/disputes/R$string;->disputes_empty_message:I

    const-string v2, "support_center"

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 62
    sget v1, Lcom/squareup/disputes/R$string;->support_center:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 63
    sget v1, Lcom/squareup/disputes/R$string;->disputes_learn_more_url:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 64
    sget v1, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->linkColor(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "LinkSpan.Builder(view.co\u2026        .asCharSequence()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->emptyViewMessage:Ljava/lang/CharSequence;

    .line 67
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->list:Landroidx/recyclerview/widget/RecyclerView;

    const-string v1, "list"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->list:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v2, Lcom/squareup/noho/NohoEdgeDecoration;

    iget-object v3, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->list:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v3, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v3}, Landroidx/recyclerview/widget/RecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v3, "list.resources"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v1}, Lcom/squareup/noho/NohoEdgeDecoration;-><init>(Landroid/content/res/Resources;)V

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/disputes/AllDisputesCoordinator$attach$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/disputes/AllDisputesCoordinator;

    invoke-direct {v1, v2}, Lcom/squareup/disputes/AllDisputesCoordinator$attach$1;-><init>(Lcom/squareup/disputes/AllDisputesCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/disputes/AllDisputesCoordinator$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/disputes/AllDisputesCoordinator$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe(::onScreen)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
