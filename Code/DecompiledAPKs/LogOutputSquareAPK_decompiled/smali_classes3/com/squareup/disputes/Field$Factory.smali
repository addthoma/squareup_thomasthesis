.class public final Lcom/squareup/disputes/Field$Factory;
.super Ljava/lang/Object;
.source "Field.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/Field;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/disputes/Field$Factory;",
        "",
        "gson",
        "Lcom/google/gson/Gson;",
        "(Lcom/google/gson/Gson;)V",
        "getGson",
        "()Lcom/google/gson/Gson;",
        "create",
        "Lcom/squareup/disputes/Field;",
        "section",
        "Lcom/squareup/protos/client/irf/Section;",
        "fieldProto",
        "Lcom/squareup/protos/client/irf/Field;",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final gson:Lcom/google/gson/Gson;


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1

    const-string v0, "gson"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/Field$Factory;->gson:Lcom/google/gson/Gson;

    return-void
.end method


# virtual methods
.method public final create(Lcom/squareup/protos/client/irf/Section;Lcom/squareup/protos/client/irf/Field;)Lcom/squareup/disputes/Field;
    .locals 2

    const-string v0, "section"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fieldProto"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p2, Lcom/squareup/protos/client/irf/Field;->field_type:Lcom/squareup/protos/client/irf/Field$FieldType;

    if-eqz v0, :cond_0

    sget-object v1, Lcom/squareup/disputes/Field$Factory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/irf/Field$FieldType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 45
    :pswitch_0
    new-instance v0, Lcom/squareup/disputes/Field$FileField;

    iget-object v1, p0, Lcom/squareup/disputes/Field$Factory;->gson:Lcom/google/gson/Gson;

    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/disputes/Field$FileField;-><init>(Lcom/squareup/protos/client/irf/Section;Lcom/squareup/protos/client/irf/Field;Lcom/google/gson/Gson;)V

    check-cast v0, Lcom/squareup/disputes/Field;

    goto :goto_0

    .line 43
    :pswitch_1
    new-instance v0, Lcom/squareup/disputes/Field$SelectField;

    invoke-direct {v0, p1, p2}, Lcom/squareup/disputes/Field$SelectField;-><init>(Lcom/squareup/protos/client/irf/Section;Lcom/squareup/protos/client/irf/Field;)V

    check-cast v0, Lcom/squareup/disputes/Field;

    goto :goto_0

    .line 40
    :pswitch_2
    new-instance v0, Lcom/squareup/disputes/Field$TextField;

    invoke-direct {v0, p1, p2}, Lcom/squareup/disputes/Field$TextField;-><init>(Lcom/squareup/protos/client/irf/Section;Lcom/squareup/protos/client/irf/Field;)V

    check-cast v0, Lcom/squareup/disputes/Field;

    :goto_0
    return-object v0

    .line 47
    :cond_0
    :goto_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Unexpected field type"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final getGson()Lcom/google/gson/Gson;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/disputes/Field$Factory;->gson:Lcom/google/gson/Gson;

    return-object v0
.end method
