.class public final enum Lcom/squareup/disputes/AllDisputesAdapter$ViewType;
.super Ljava/lang/Enum;
.source "AllDisputesAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/AllDisputesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ViewType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/disputes/AllDisputesAdapter$ViewType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0006\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/disputes/AllDisputesAdapter$ViewType;",
        "",
        "(Ljava/lang/String;I)V",
        "TYPE_HEADER",
        "TYPE_SUMMARY",
        "TYPE_DISPUTE",
        "TYPE_LOAD_MORE",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

.field public static final enum TYPE_DISPUTE:Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

.field public static final enum TYPE_HEADER:Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

.field public static final enum TYPE_LOAD_MORE:Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

.field public static final enum TYPE_SUMMARY:Lcom/squareup/disputes/AllDisputesAdapter$ViewType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    new-instance v1, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    const/4 v2, 0x0

    const-string v3, "TYPE_HEADER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->TYPE_HEADER:Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    const/4 v2, 0x1

    const-string v3, "TYPE_SUMMARY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->TYPE_SUMMARY:Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    const/4 v2, 0x2

    const-string v3, "TYPE_DISPUTE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->TYPE_DISPUTE:Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    const/4 v2, 0x3

    const-string v3, "TYPE_LOAD_MORE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->TYPE_LOAD_MORE:Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->$VALUES:[Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 221
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/disputes/AllDisputesAdapter$ViewType;
    .locals 1

    const-class v0, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/disputes/AllDisputesAdapter$ViewType;
    .locals 1

    sget-object v0, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->$VALUES:[Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    invoke-virtual {v0}, [Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    return-object v0
.end method
