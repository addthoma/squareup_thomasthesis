.class public Lcom/squareup/flowlegacy/ConfirmationPopup;
.super Lcom/squareup/flowlegacy/DialogPopup;
.source "ConfirmationPopup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/DialogPopup<",
        "Lcom/squareup/register/widgets/Confirmation;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/flowlegacy/DialogPopup;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic lambda$createDialog$0(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p1, 0x1

    .line 29
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$createDialog$1(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p1, 0x0

    .line 30
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$createDialog$2(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x0

    .line 32
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected createDialog(Lcom/squareup/register/widgets/Confirmation;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/AlertDialog;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/register/widgets/Confirmation;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/register/widgets/Confirmation;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Landroid/app/AlertDialog;"
        }
    .end annotation

    .line 23
    invoke-virtual {p0}, Lcom/squareup/flowlegacy/ConfirmationPopup;->getContext()Landroid/content/Context;

    move-result-object p2

    .line 25
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/register/widgets/Confirmation;->getStrings(Landroid/content/res/Resources;)Lcom/squareup/register/widgets/Confirmation$Strings;

    move-result-object p1

    .line 27
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object p2, p1, Lcom/squareup/register/widgets/Confirmation$Strings;->title:Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p2

    iget-object v0, p1, Lcom/squareup/register/widgets/Confirmation$Strings;->body:Ljava/lang/String;

    .line 28
    invoke-virtual {p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p2

    iget-object v0, p1, Lcom/squareup/register/widgets/Confirmation$Strings;->confirm:Ljava/lang/String;

    new-instance v1, Lcom/squareup/flowlegacy/-$$Lambda$ConfirmationPopup$0cWblYul0lNeWKnal16GxkLGjwg;

    invoke-direct {v1, p3}, Lcom/squareup/flowlegacy/-$$Lambda$ConfirmationPopup$0cWblYul0lNeWKnal16GxkLGjwg;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 29
    invoke-virtual {p2, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p2

    iget-object p1, p1, Lcom/squareup/register/widgets/Confirmation$Strings;->cancel:Ljava/lang/String;

    new-instance v0, Lcom/squareup/flowlegacy/-$$Lambda$ConfirmationPopup$IRhG6wsgiBmgjYc8SocC5ghX7eE;

    invoke-direct {v0, p3}, Lcom/squareup/flowlegacy/-$$Lambda$ConfirmationPopup$IRhG6wsgiBmgjYc8SocC5ghX7eE;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 30
    invoke-virtual {p2, p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 p2, 0x1

    .line 31
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/flowlegacy/-$$Lambda$ConfirmationPopup$FgAkurWd3_fuw8l977hpquzkzCg;

    invoke-direct {p2, p3}, Lcom/squareup/flowlegacy/-$$Lambda$ConfirmationPopup$FgAkurWd3_fuw8l977hpquzkzCg;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 32
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 33
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/register/widgets/Confirmation;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/flowlegacy/ConfirmationPopup;->createDialog(Lcom/squareup/register/widgets/Confirmation;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
