.class public Lcom/squareup/flowlegacy/WarningPopup;
.super Lcom/squareup/flowlegacy/DialogPopup;
.source "WarningPopup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/DialogPopup<",
        "Lcom/squareup/widgets/warning/Warning;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1}, Lcom/squareup/flowlegacy/DialogPopup;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic lambda$createDialog$0(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p1, 0x0

    .line 27
    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$createDialog$1(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x0

    .line 29
    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected createDialog(Lcom/squareup/widgets/warning/Warning;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/AlertDialog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/widgets/warning/Warning;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/widgets/warning/Warning;",
            "Ljava/lang/Void;",
            ">;)",
            "Landroid/app/AlertDialog;"
        }
    .end annotation

    .line 20
    invoke-virtual {p0}, Lcom/squareup/flowlegacy/WarningPopup;->getContext()Landroid/content/Context;

    move-result-object p2

    .line 22
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/widgets/warning/Warning;->getStrings(Landroid/content/res/Resources;)Lcom/squareup/widgets/warning/Warning$Strings;

    move-result-object p1

    .line 24
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object p2, p1, Lcom/squareup/widgets/warning/Warning$Strings;->title:Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p2

    iget-object p1, p1, Lcom/squareup/widgets/warning/Warning$Strings;->body:Ljava/lang/String;

    .line 25
    invoke-virtual {p2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget p2, Lcom/squareup/common/strings/R$string;->ok:I

    new-instance v0, Lcom/squareup/flowlegacy/-$$Lambda$WarningPopup$1TxB9Ld_YkHuX7Nae2AFFFv1DBU;

    invoke-direct {v0, p3}, Lcom/squareup/flowlegacy/-$$Lambda$WarningPopup$1TxB9Ld_YkHuX7Nae2AFFFv1DBU;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 26
    invoke-virtual {p1, p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 p2, 0x1

    .line 28
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/flowlegacy/-$$Lambda$WarningPopup$OZ6vIheOtfEOYYyQ0nVtUfwERxM;

    invoke-direct {p2, p3}, Lcom/squareup/flowlegacy/-$$Lambda$WarningPopup$OZ6vIheOtfEOYYyQ0nVtUfwERxM;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 29
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 30
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/widgets/warning/Warning;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/flowlegacy/WarningPopup;->createDialog(Lcom/squareup/widgets/warning/Warning;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
