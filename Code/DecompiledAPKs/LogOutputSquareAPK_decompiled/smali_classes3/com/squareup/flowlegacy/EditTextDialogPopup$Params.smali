.class public Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;
.super Ljava/lang/Object;
.source "EditTextDialogPopup.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/flowlegacy/EditTextDialogPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Params"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final hint:Ljava/lang/String;

.field private final inputType:I

.field private final titleId:I

.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 118
    new-instance v0, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params$1;

    invoke-direct {v0}, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params$1;-><init>()V

    sput-object v0, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    const/4 v0, -0x1

    .line 97
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;->value:Ljava/lang/String;

    .line 102
    iput-object p2, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;->hint:Ljava/lang/String;

    .line 103
    iput p3, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;->titleId:I

    .line 104
    iput p4, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;->inputType:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;)Ljava/lang/String;
    .locals 0

    .line 90
    iget-object p0, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;->value:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;)Ljava/lang/String;
    .locals 0

    .line 90
    iget-object p0, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;->hint:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;)I
    .locals 0

    .line 90
    iget p0, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;->titleId:I

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;)I
    .locals 0

    .line 90
    iget p0, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;->inputType:I

    return p0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 112
    iget-object p2, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;->value:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 113
    iget-object p2, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;->hint:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 114
    iget p2, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;->titleId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 115
    iget p2, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;->inputType:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
