.class public final enum Lcom/squareup/flowlegacy/YesNo;
.super Ljava/lang/Enum;
.source "YesNo.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/flowlegacy/YesNo;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/flowlegacy/YesNo;

.field public static final enum NO:Lcom/squareup/flowlegacy/YesNo;

.field public static final enum YES:Lcom/squareup/flowlegacy/YesNo;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 12
    new-instance v0, Lcom/squareup/flowlegacy/YesNo;

    const/4 v1, 0x0

    const-string v2, "YES"

    invoke-direct {v0, v2, v1}, Lcom/squareup/flowlegacy/YesNo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/flowlegacy/YesNo;->YES:Lcom/squareup/flowlegacy/YesNo;

    .line 13
    new-instance v0, Lcom/squareup/flowlegacy/YesNo;

    const/4 v2, 0x1

    const-string v3, "NO"

    invoke-direct {v0, v3, v2}, Lcom/squareup/flowlegacy/YesNo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/flowlegacy/YesNo;->NO:Lcom/squareup/flowlegacy/YesNo;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/flowlegacy/YesNo;

    .line 11
    sget-object v3, Lcom/squareup/flowlegacy/YesNo;->YES:Lcom/squareup/flowlegacy/YesNo;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/flowlegacy/YesNo;->NO:Lcom/squareup/flowlegacy/YesNo;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/flowlegacy/YesNo;->$VALUES:[Lcom/squareup/flowlegacy/YesNo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/flowlegacy/YesNo;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/flowlegacy/YesNo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/flowlegacy/YesNo;

    return-object p0
.end method

.method public static values()[Lcom/squareup/flowlegacy/YesNo;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/flowlegacy/YesNo;->$VALUES:[Lcom/squareup/flowlegacy/YesNo;

    invoke-virtual {v0}, [Lcom/squareup/flowlegacy/YesNo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/flowlegacy/YesNo;

    return-object v0
.end method
