.class public final Lcom/squareup/notificationcenterlauncher/NotificationCenterResultRunner;
.super Ljava/lang/Object;
.source "NotificationCenterResultRunner.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/notificationcenterlauncher/NotificationCenterResultRunner;",
        "",
        "notificationHandler",
        "Lcom/squareup/notificationcenter/NotificationHandler;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "(Lcom/squareup/notificationcenter/NotificationHandler;Lcom/squareup/ui/main/PosContainer;)V",
        "onResult",
        "",
        "result",
        "Lcom/squareup/notificationcenter/NotificationCenterOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final notificationHandler:Lcom/squareup/notificationcenter/NotificationHandler;


# direct methods
.method public constructor <init>(Lcom/squareup/notificationcenter/NotificationHandler;Lcom/squareup/ui/main/PosContainer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "notificationHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/notificationcenterlauncher/NotificationCenterResultRunner;->notificationHandler:Lcom/squareup/notificationcenter/NotificationHandler;

    iput-object p2, p0, Lcom/squareup/notificationcenterlauncher/NotificationCenterResultRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method


# virtual methods
.method public final onResult(Lcom/squareup/notificationcenter/NotificationCenterOutput;)V
    .locals 1

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    instance-of v0, p1, Lcom/squareup/notificationcenter/NotificationCenterOutput$RequestedNotificationHandling;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenterlauncher/NotificationCenterResultRunner;->notificationHandler:Lcom/squareup/notificationcenter/NotificationHandler;

    .line 18
    check-cast p1, Lcom/squareup/notificationcenter/NotificationCenterOutput$RequestedNotificationHandling;

    invoke-virtual {p1}, Lcom/squareup/notificationcenter/NotificationCenterOutput$RequestedNotificationHandling;->getNotification()Lcom/squareup/notificationcenterdata/Notification;

    move-result-object p1

    .line 17
    invoke-interface {v0, p1}, Lcom/squareup/notificationcenter/NotificationHandler;->handleNotification(Lcom/squareup/notificationcenterdata/Notification;)V

    goto :goto_0

    .line 20
    :cond_0
    instance-of p1, p1, Lcom/squareup/notificationcenter/NotificationCenterOutput$Exited;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/notificationcenterlauncher/NotificationCenterResultRunner;->container:Lcom/squareup/ui/main/PosContainer;

    sget-object v0, Lcom/squareup/notificationcenterlauncher/NotificationCenterWorkflowRunner;->Companion:Lcom/squareup/notificationcenterlauncher/NotificationCenterWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/notificationcenterlauncher/NotificationCenterWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/PosContainer;->goBackPastWorkflow(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method
