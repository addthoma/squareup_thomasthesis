.class public interface abstract Lcom/squareup/core/location/settings/LocationRelationSettings;
.super Ljava/lang/Object;
.source "LocationRelationSettings.java"


# virtual methods
.method public abstract getFenceRadius()F
.end method

.method public abstract getInnerRadius()F
.end method

.method public abstract getMaxAge()J
.end method

.method public abstract getMinAccuracy()F
.end method

.method public abstract getOuterRadius()F
.end method

.method public abstract getServiceAlarmRepeatDelay()J
.end method
