.class Lcom/squareup/core/location/providers/GpsProvider$WrongLocationTimestampEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "GpsProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/core/location/providers/GpsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WrongLocationTimestampEvent"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 136
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ERROR:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "Wrong location timestamp"

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/core/location/providers/GpsProvider$1;)V
    .locals 0

    .line 134
    invoke-direct {p0}, Lcom/squareup/core/location/providers/GpsProvider$WrongLocationTimestampEvent;-><init>()V

    return-void
.end method
