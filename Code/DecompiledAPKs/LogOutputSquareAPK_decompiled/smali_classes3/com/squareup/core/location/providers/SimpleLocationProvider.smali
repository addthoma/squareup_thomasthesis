.class public Lcom/squareup/core/location/providers/SimpleLocationProvider;
.super Ljava/lang/Object;
.source "SimpleLocationProvider.java"

# interfaces
.implements Lcom/squareup/core/location/providers/LocationProvider;


# instance fields
.field private final available:Z

.field private final context:Landroid/content/Context;

.field private final locationManager:Landroid/location/LocationManager;

.field private final type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/location/LocationManager;Ljava/lang/String;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/core/location/providers/SimpleLocationProvider;->context:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/squareup/core/location/providers/SimpleLocationProvider;->locationManager:Landroid/location/LocationManager;

    .line 29
    iput-object p3, p0, Lcom/squareup/core/location/providers/SimpleLocationProvider;->type:Ljava/lang/String;

    .line 30
    invoke-virtual {p2}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/core/location/providers/SimpleLocationProvider;->available:Z

    return-void
.end method


# virtual methods
.method public getLastKnownLocation()Landroid/location/Location;
    .locals 2

    .line 34
    iget-boolean v0, p0, Lcom/squareup/core/location/providers/SimpleLocationProvider;->available:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->LOCATION:Lcom/squareup/systempermissions/SystemPermission;

    iget-object v1, p0, Lcom/squareup/core/location/providers/SimpleLocationProvider;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/squareup/systempermissions/SystemPermission;->hasPermission(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/core/location/providers/SimpleLocationProvider;->locationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/squareup/core/location/providers/SimpleLocationProvider;->type:Ljava/lang/String;

    .line 35
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public removeUpdates()V
    .locals 0

    return-void
.end method

.method public requestLocationUpdate(Landroid/location/LocationListener;J)V
    .locals 1

    const/4 v0, 0x0

    .line 40
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/squareup/core/location/providers/SimpleLocationProvider;->requestLocationUpdate(Landroid/location/LocationListener;JF)V

    return-void
.end method

.method public requestLocationUpdate(Landroid/location/LocationListener;JF)V
    .locals 7

    .line 45
    iget-boolean v0, p0, Lcom/squareup/core/location/providers/SimpleLocationProvider;->available:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->LOCATION:Lcom/squareup/systempermissions/SystemPermission;

    iget-object v1, p0, Lcom/squareup/core/location/providers/SimpleLocationProvider;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/squareup/systempermissions/SystemPermission;->hasPermission(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 49
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/squareup/core/location/providers/SimpleLocationProvider;->locationManager:Landroid/location/LocationManager;

    iget-object v2, p0, Lcom/squareup/core/location/providers/SimpleLocationProvider;->type:Ljava/lang/String;

    move-wide v3, p2

    move v5, p4

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string p2, "Caught and ignored bogus SecurityException. See https://code.google.com/p/android/issues/detail?id=22036"

    .line 58
    invoke-static {p1, p2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method
