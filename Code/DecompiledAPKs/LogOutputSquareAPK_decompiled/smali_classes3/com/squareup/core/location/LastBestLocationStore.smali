.class public Lcom/squareup/core/location/LastBestLocationStore;
.super Ljava/lang/Object;
.source "LastBestLocationStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/core/location/LastBestLocationStore$LocationStruct;
    }
.end annotation


# instance fields
.field private final delegate:Lcom/squareup/persistent/Persistent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/persistent/Persistent<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final fileThreadExecutor:Ljava/util/concurrent/Executor;

.field private final gson:Lcom/google/gson/Gson;

.field private location:Landroid/location/Location;

.field private locationSet:Z


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;Lcom/squareup/persistent/Persistent;Ljava/util/concurrent/Executor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            "Lcom/squareup/persistent/Persistent<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 19
    iput-boolean v0, p0, Lcom/squareup/core/location/LastBestLocationStore;->locationSet:Z

    .line 24
    iput-object p1, p0, Lcom/squareup/core/location/LastBestLocationStore;->gson:Lcom/google/gson/Gson;

    .line 25
    iput-object p2, p0, Lcom/squareup/core/location/LastBestLocationStore;->delegate:Lcom/squareup/persistent/Persistent;

    .line 26
    iput-object p3, p0, Lcom/squareup/core/location/LastBestLocationStore;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method private locationFromString(Ljava/lang/String;)Landroid/location/Location;
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 55
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/squareup/core/location/LastBestLocationStore;->gson:Lcom/google/gson/Gson;

    const-class v2, Lcom/squareup/core/location/LastBestLocationStore$LocationStruct;

    invoke-virtual {v1, p1, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/core/location/LastBestLocationStore$LocationStruct;

    invoke-virtual {v1}, Lcom/squareup/core/location/LastBestLocationStore$LocationStruct;->toLocation()Landroid/location/Location;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object p1

    :catchall_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string p1, "Ignoring incompatible JSON: %s"

    .line 57
    invoke-static {p1, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method private locationToString(Landroid/location/Location;)Ljava/lang/String;
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/squareup/core/location/LastBestLocationStore;->gson:Lcom/google/gson/Gson;

    new-instance v1, Lcom/squareup/core/location/LastBestLocationStore$LocationStruct;

    invoke-direct {v1, p1}, Lcom/squareup/core/location/LastBestLocationStore$LocationStruct;-><init>(Landroid/location/Location;)V

    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public declared-synchronized get()Landroid/location/Location;
    .locals 1

    monitor-enter p0

    .line 31
    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/core/location/LastBestLocationStore;->locationSet:Z

    if-nez v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/squareup/core/location/LastBestLocationStore;->delegate:Lcom/squareup/persistent/Persistent;

    invoke-interface {v0}, Lcom/squareup/persistent/Persistent;->getSynchronous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/squareup/core/location/LastBestLocationStore;->locationFromString(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/core/location/LastBestLocationStore;->location:Landroid/location/Location;

    const/4 v0, 0x1

    .line 34
    iput-boolean v0, p0, Lcom/squareup/core/location/LastBestLocationStore;->locationSet:Z

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/squareup/core/location/LastBestLocationStore;->location:Landroid/location/Location;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public synthetic lambda$set$0$LastBestLocationStore(Landroid/location/Location;)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/core/location/LastBestLocationStore;->delegate:Lcom/squareup/persistent/Persistent;

    invoke-direct {p0, p1}, Lcom/squareup/core/location/LastBestLocationStore;->locationToString(Landroid/location/Location;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/persistent/Persistent;->setSynchronous(Ljava/lang/Object;)V

    return-void
.end method

.method public declared-synchronized set(Landroid/location/Location;)V
    .locals 2

    monitor-enter p0

    .line 40
    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/core/location/LastBestLocationStore;->locationSet:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/core/location/LastBestLocationStore;->location:Landroid/location/Location;

    if-eq v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 41
    iput-boolean v0, p0, Lcom/squareup/core/location/LastBestLocationStore;->locationSet:Z

    .line 42
    iput-object p1, p0, Lcom/squareup/core/location/LastBestLocationStore;->location:Landroid/location/Location;

    .line 43
    iget-object v0, p0, Lcom/squareup/core/location/LastBestLocationStore;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/core/location/-$$Lambda$LastBestLocationStore$HpS8_R8KPS2cS6OdBgG9RitC1ag;

    invoke-direct {v1, p0, p1}, Lcom/squareup/core/location/-$$Lambda$LastBestLocationStore$HpS8_R8KPS2cS6OdBgG9RitC1ag;-><init>(Lcom/squareup/core/location/LastBestLocationStore;Landroid/location/Location;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
