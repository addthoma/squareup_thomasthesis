.class public final Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler_Factory;
.super Ljava/lang/Object;
.source "CreateItemTutorialDeepLinkHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryAppletGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler_Factory;->tutorialCreatorProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;)",
            "Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/orderentry/OrderEntryAppletGateway;)Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler_Factory;->tutorialCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;

    iget-object v2, p0, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-static {v0, v1, v2}, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/orderentry/OrderEntryAppletGateway;)Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler_Factory;->get()Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;

    move-result-object v0

    return-object v0
.end method
