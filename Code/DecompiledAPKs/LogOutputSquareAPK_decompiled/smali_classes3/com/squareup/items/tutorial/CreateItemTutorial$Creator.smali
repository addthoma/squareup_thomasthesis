.class public final Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;
.super Lcom/squareup/tutorialv2/TutorialCreator;
.source "CreateItemTutorial.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/tutorial/CreateItemTutorial;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Creator"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$Seed;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCreateItemTutorial.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CreateItemTutorial.kt\ncom/squareup/items/tutorial/CreateItemTutorial$Creator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,244:1\n1550#2,3:245\n704#2:248\n777#2,2:249\n*E\n*S KotlinDebug\n*F\n+ 1 CreateItemTutorial.kt\ncom/squareup/items/tutorial/CreateItemTutorial$Creator\n*L\n215#1,3:245\n229#1:248\n229#1,2:249\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0008\u0007\u0018\u00002\u00020\u0001:\u00012Bc\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0003\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0015J\u0016\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001a2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J,\u0010\u001e\u001a&\u0012\u000c\u0012\n !*\u0004\u0018\u00010 0  !*\u0012\u0012\u000c\u0012\n !*\u0004\u0018\u00010 0 \u0018\u00010\u001f0\u001fH\u0002J,\u0010\"\u001a&\u0012\u000c\u0012\n !*\u0004\u0018\u00010 0  !*\u0012\u0012\u000c\u0012\n !*\u0004\u0018\u00010 0 \u0018\u00010\u001f0\u001fH\u0002J\u0010\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&H\u0016J \u0010\'\u001a\u0012\u0012\u000c\u0012\n !*\u0004\u0018\u00010)0)\u0018\u00010(2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0018\u0010*\u001a\n !*\u0004\u0018\u00010+0+2\u0006\u0010,\u001a\u00020-H\u0002J\u000e\u0010.\u001a\u00020$2\u0006\u0010/\u001a\u00020 J\u0008\u00100\u001a\u00020 H\u0002J\u000e\u00101\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u001fH\u0016R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00063"
    }
    d2 = {
        "Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;",
        "Lcom/squareup/tutorialv2/TutorialCreator;",
        "tutorialProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/items/tutorial/CreateItemTutorial;",
        "tutorialPresenter",
        "Lcom/squareup/register/tutorial/TutorialPresenter;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "precogService",
        "Lcom/squareup/server/precog/PrecogService;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "bus",
        "Lcom/squareup/badbus/BadBus;",
        "orderEntryAppletGateway",
        "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
        "prefs",
        "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
        "(Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/precog/PrecogService;Ljavax/inject/Provider;Lcom/squareup/badbus/BadBus;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)V",
        "seeds",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/tutorialv2/TutorialSeed;",
        "handleSuccessfulPrecogResponse",
        "Lio/reactivex/Single;",
        "Lcom/squareup/protos/precog/ProductIntent;",
        "response",
        "Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse;",
        "hasItems",
        "Lio/reactivex/Observable;",
        "",
        "kotlin.jvm.PlatformType",
        "isPosBapi",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "precogBapiSignals",
        "",
        "Lcom/squareup/protos/precog/Signal;",
        "precogRequest",
        "Lcom/squareup/protos/precog/GetSignalsByMerchantTokenRequest;",
        "merchantToken",
        "",
        "ready",
        "startedFromSupportApplet",
        "shouldStart",
        "triggeredTutorial",
        "Seed",
        "items-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final cogs:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private final precogService:Lcom/squareup/server/precog/PrecogService;

.field private final prefs:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

.field private final seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;"
        }
    .end annotation
.end field

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tutorialPresenter:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorial;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/precog/PrecogService;Ljavax/inject/Provider;Lcom/squareup/badbus/BadBus;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)V
    .locals 1
    .param p9    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/server/precog/PrecogService;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "tutorialProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tutorialPresenter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "precogService"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cogs"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bus"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEntryAppletGateway"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "prefs"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    invoke-direct {p0}, Lcom/squareup/tutorialv2/TutorialCreator;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->tutorialProvider:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->tutorialPresenter:Ljavax/inject/Provider;

    iput-object p3, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->features:Lcom/squareup/settings/server/Features;

    iput-object p4, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p5, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->precogService:Lcom/squareup/server/precog/PrecogService;

    iput-object p6, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->cogs:Ljavax/inject/Provider;

    iput-object p7, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->bus:Lcom/squareup/badbus/BadBus;

    iput-object p8, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iput-object p9, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->prefs:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    .line 142
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getCogs$p(Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;)Ljavax/inject/Provider;
    .locals 0

    .line 130
    iget-object p0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->cogs:Ljavax/inject/Provider;

    return-object p0
.end method

.method public static final synthetic access$getTutorialProvider$p(Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;)Ljavax/inject/Provider;
    .locals 0

    .line 130
    iget-object p0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->tutorialProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method public static final synthetic access$handleSuccessfulPrecogResponse(Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse;)Lio/reactivex/Single;
    .locals 0

    .line 130
    invoke-direct {p0, p1}, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->handleSuccessfulPrecogResponse(Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final handleSuccessfulPrecogResponse(Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/protos/precog/ProductIntent;",
            ">;"
        }
    .end annotation

    .line 214
    invoke-direct {p0, p1}, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->precogBapiSignals(Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse;)Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_4

    .line 215
    check-cast p1, Ljava/lang/Iterable;

    .line 245
    instance-of v2, p1, Ljava/util/Collection;

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    .line 246
    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/precog/Signal;

    .line 216
    iget-object v2, v2, Lcom/squareup/protos/precog/Signal;->product_intent_value:Lcom/squareup/protos/precog/ProductIntent;

    sget-object v3, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_POS:Lcom/squareup/protos/precog/ProductIntent;

    if-ne v2, v3, :cond_3

    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_2

    const/4 p1, 0x1

    :goto_1
    if-eqz p1, :cond_4

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_5

    .line 221
    sget-object p1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_POS:Lcom/squareup/protos/precog/ProductIntent;

    goto :goto_3

    .line 223
    :cond_5
    sget-object p1, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_UNKNOWN:Lcom/squareup/protos/precog/ProductIntent;

    .line 219
    :goto_3
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(\n          i\u2026KNOWN\n          }\n      )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final hasItems()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 181
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/jailkeeper/JailKeeper$State;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    .line 182
    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$hasItems$1;->INSTANCE:Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$hasItems$1;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 183
    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$hasItems$2;

    invoke-direct {v1, p0}, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$hasItems$2;-><init>(Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 189
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 190
    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$hasItems$3;->INSTANCE:Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$hasItems$3;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method private final isPosBapi()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 196
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->precogService:Lcom/squareup/server/precog/PrecogService;

    .line 197
    iget-object v1, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    const-string v2, "settings.userSettings"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getMerchantToken()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v2, "settings.userSettings.merchantToken!!"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->precogRequest(Ljava/lang/String;)Lcom/squareup/protos/precog/GetSignalsByMerchantTokenRequest;

    move-result-object v1

    const-string v2, "precogRequest(settings.u\u2026Settings.merchantToken!!)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/server/precog/PrecogService;->getSignalsByMerchantToken(Lcom/squareup/protos/precog/GetSignalsByMerchantTokenRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 198
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 199
    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$isPosBapi$1;

    invoke-direct {v1, p0}, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$isPosBapi$1;-><init>(Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 205
    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$isPosBapi$2;->INSTANCE:Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$isPosBapi$2;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 206
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method private final precogBapiSignals(Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/precog/Signal;",
            ">;"
        }
    .end annotation

    .line 229
    iget-object p1, p1, Lcom/squareup/protos/precog/GetSignalsByMerchantTokenResponse;->merchant_signals:Lcom/squareup/protos/precog/MerchantSignals;

    if-eqz p1, :cond_3

    iget-object p1, p1, Lcom/squareup/protos/precog/MerchantSignals;->signals:Ljava/util/List;

    if-eqz p1, :cond_3

    check-cast p1, Ljava/lang/Iterable;

    .line 248
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 249
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/precog/Signal;

    .line 230
    iget-object v2, v2, Lcom/squareup/protos/precog/Signal;->signal_name:Lcom/squareup/protos/precog/SignalName;

    sget-object v3, Lcom/squareup/protos/precog/SignalName;->SIGNAL_BEST_AVAILABLE_PRODUCT_INTENT_PRIMARY:Lcom/squareup/protos/precog/SignalName;

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 250
    :cond_2
    check-cast v0, Ljava/util/List;

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    :goto_2
    return-object v0
.end method

.method private final precogRequest(Ljava/lang/String;)Lcom/squareup/protos/precog/GetSignalsByMerchantTokenRequest;
    .locals 1

    .line 208
    new-instance v0, Lcom/squareup/protos/precog/GetSignalsByMerchantTokenRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/precog/GetSignalsByMerchantTokenRequest$Builder;-><init>()V

    .line 209
    invoke-virtual {v0, p1}, Lcom/squareup/protos/precog/GetSignalsByMerchantTokenRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/precog/GetSignalsByMerchantTokenRequest$Builder;

    move-result-object p1

    .line 210
    invoke-virtual {p1}, Lcom/squareup/protos/precog/GetSignalsByMerchantTokenRequest$Builder;->build()Lcom/squareup/protos/precog/GetSignalsByMerchantTokenRequest;

    move-result-object p1

    return-object p1
.end method

.method private final shouldStart()Z
    .locals 4

    .line 174
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_ITEMS_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->prefs:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    const-string v3, "createItemTutorialCompleted"

    invoke-virtual {v0, v3, v2}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {v0}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->hasOrderEntryApplet()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    invoke-direct {p0}, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->shouldStart()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Lio/reactivex/Observable;

    const/4 v1, 0x0

    .line 165
    invoke-direct {p0}, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->hasItems()Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->isPosBapi()Lio/reactivex/Observable;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$onEnterScope$1;->INSTANCE:Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$onEnterScope$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-static {v0, v1}, Lio/reactivex/Observable;->combineLatest(Ljava/lang/Iterable;Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 167
    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$onEnterScope$2;-><init>(Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "combineLatest(listOf(has\u2026 false)\n        }\n      }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public final ready(Z)V
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->tutorialPresenter:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/tutorial/TutorialPresenter;

    .line 147
    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->endTutorial()V

    if-eqz p1, :cond_0

    .line 150
    sget-object p1, Lcom/squareup/tutorialv2/TutorialSeed$Priority;->MERCHANT_STARTED:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    goto :goto_0

    .line 152
    :cond_0
    sget-object p1, Lcom/squareup/tutorialv2/TutorialSeed$Priority;->AUTO_STARTED:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    .line 155
    :goto_0
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$Seed;

    invoke-direct {v1, p0, p1}, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$Seed;-><init>(Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public triggeredTutorial()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;"
        }
    .end annotation

    .line 158
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
