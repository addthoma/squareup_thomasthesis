.class final Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner$onEnterScope$2;
.super Ljava/lang/Object;
.source "RealAddSingleVariationWithOptionsWorkflowRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;)V
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;->access$getOutputHandler$p(Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner;)Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v1, "it"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutputHandler;->handle(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflowRunner$onEnterScope$2;->accept(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;)V

    return-void
.end method
