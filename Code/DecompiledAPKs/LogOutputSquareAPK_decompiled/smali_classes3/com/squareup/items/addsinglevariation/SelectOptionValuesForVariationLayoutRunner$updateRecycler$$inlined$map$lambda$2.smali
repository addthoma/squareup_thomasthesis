.class final Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$updateRecycler$$inlined$map$lambda$2;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectOptionValuesForVariationLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner;->updateRecycler(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke",
        "com/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$updateRecycler$availableVariationRows$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $it:Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;

.field final synthetic $rendering$inlined:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;


# direct methods
.method constructor <init>(Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$updateRecycler$$inlined$map$lambda$2;->$it:Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;

    iput-object p2, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$updateRecycler$$inlined$map$lambda$2;->$rendering$inlined:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$updateRecycler$$inlined$map$lambda$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    .line 161
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$updateRecycler$$inlined$map$lambda$2;->$rendering$inlined:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;

    invoke-virtual {v0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;->getAddVariation()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$updateRecycler$$inlined$map$lambda$2;->$it:Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
