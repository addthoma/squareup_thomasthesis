.class public final Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps$CREATOR;
.super Ljava/lang/Object;
.source "AddSingleVariationWithOptionsProps.kt"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CREATOR"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAddSingleVariationWithOptionsProps.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AddSingleVariationWithOptionsProps.kt\ncom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps$CREATOR\n*L\n1#1,85:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u001d\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016\u00a2\u0006\u0002\u0010\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps$CREATOR;",
        "Landroid/os/Parcelable$Creator;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;",
        "()V",
        "createFromParcel",
        "parcel",
        "Landroid/os/Parcel;",
        "newArray",
        "",
        "size",
        "",
        "(I)[Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps$CREATOR;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;
    .locals 10

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v0, "parcel.readString()!!"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 49
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/List;

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v1, :cond_2

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 54
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    .line 55
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 57
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    check-cast v6, Ljava/util/List;

    const/4 v7, 0x0

    :goto_2
    if-ge v7, v1, :cond_4

    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v8

    .line 60
    sget-object v9, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;

    invoke-virtual {v9, v8}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->parse([B)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    move-result-object v8

    const-string v9, "CatalogModelObjectRegist\u2026NSTANCE.parse(optionData)"

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 63
    :cond_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 64
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    check-cast v7, Ljava/util/List;

    :goto_3
    if-ge v5, v1, :cond_5

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v8

    .line 67
    invoke-static {v8}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->objectFromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v8

    const-string v9, "CatalogObjectType.Adapte\u2026mByteArray(variationData)"

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 70
    :cond_5
    new-instance p1, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;

    move-object v1, p1

    move v5, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLjava/util/List;Ljava/util/List;)V

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps$CREATOR;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;
    .locals 0

    .line 81
    new-array p1, p1, [Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps$CREATOR;->newArray(I)[Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;

    move-result-object p1

    return-object p1
.end method
