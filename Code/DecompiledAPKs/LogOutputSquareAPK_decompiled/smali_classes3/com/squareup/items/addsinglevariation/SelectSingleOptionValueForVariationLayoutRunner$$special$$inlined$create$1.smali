.class public final Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/cycler/StandardRowSpec$Creator<",
        "TI;TS;TV;>;",
        "Landroid/content/Context;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$create$1\n+ 2 SelectSingleOptionValueForVariationLayoutRunner.kt\ncom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner\n+ 3 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator\n*L\n1#1,87:1\n83#2,11:88\n114#2:101\n64#3,2:99\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00072\u0006\u0010\u0008\u001a\u00020\tH\n\u00a2\u0006\u0002\u0008\n\u00a8\u0006\u000b"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "Lcom/squareup/cycler/StandardRowSpec$Creator;",
        "context",
        "Landroid/content/Context;",
        "invoke",
        "com/squareup/cycler/StandardRowSpec$create$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $layoutId:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    iput p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1;->$layoutId:I

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/cycler/StandardRowSpec$Creator;

    check-cast p2, Landroid/content/Context;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec$Creator<",
            "TI;TS;TV;>;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1;->$layoutId:I

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p1, p2}, Lcom/squareup/cycler/StandardRowSpec$Creator;->setView(Landroid/view/View;)V

    .line 88
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/LinearLayout;

    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$id;->select_option_values_add_value:I

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/noho/NohoEditText;

    .line 90
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/items/editoption/R$integer;->maximum_length_of_option_value_names:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    const/16 v1, 0x8

    .line 91
    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoEditText;->setBorderEdges(I)V

    const-string v1, "editView"

    .line 92
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 93
    sget v2, Lcom/squareup/items/assignitemoptions/impl/R$string;->select_option_values_add_new_option_value_row_hint:I

    .line 92
    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoEditText;->setHint(Ljava/lang/CharSequence;)V

    const/4 v1, 0x5

    .line 95
    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoEditText;->setImeOptions(I)V

    const/16 v1, 0x2001

    .line 96
    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoEditText;->setInputType(I)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter$LengthFilter;

    const/4 v2, 0x0

    .line 97
    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v3, v0}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v1, v2

    check-cast v1, [Landroid/text/InputFilter;

    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 99
    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1$lambda$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationLayoutRunner$$special$$inlined$create$1$lambda$1;-><init>(Lcom/squareup/cycler/StandardRowSpec$Creator;Lcom/squareup/noho/NohoEditText;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->bind(Lkotlin/jvm/functions/Function2;)V

    return-void

    .line 37
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type V"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
