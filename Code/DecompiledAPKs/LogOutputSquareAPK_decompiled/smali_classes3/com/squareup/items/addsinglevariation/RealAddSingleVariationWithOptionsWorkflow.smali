.class public final Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealAddSingleVariationWithOptionsWorkflow.kt"

# interfaces
.implements Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAddSingleVariationWithOptionsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAddSingleVariationWithOptionsWorkflow.kt\ncom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,206:1\n32#2,12:207\n1360#3:219\n1429#3,3:220\n1360#3:223\n1429#3,3:224\n1360#3:227\n1429#3,3:228\n*E\n*S KotlinDebug\n*F\n+ 1 RealAddSingleVariationWithOptionsWorkflow.kt\ncom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow\n*L\n45#1,12:207\n72#1:219\n72#1,3:220\n74#1:223\n74#1,3:224\n200#1:227\n200#1,3:228\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0001\u001fB\u0017\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u001a\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00032\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016JN\u0010\u0014\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u00042\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u0004H\u0016J\u001a\u0010\u0019\u001a\u00020\u001a*\u00020\u001b2\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001e0\u001dH\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "fetchAllVariationsWorkflow",
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;",
        "selectOptionValuesForVariationWorkflow",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;",
        "(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "toItemOptionValueCombination",
        "Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        "options",
        "",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final fetchAllVariationsWorkflow:Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;

.field private final selectOptionValuesForVariationWorkflow:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "fetchAllVariationsWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectOptionValuesForVariationWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;->fetchAllVariationsWorkflow:Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;

    iput-object p2, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;->selectOptionValuesForVariationWorkflow:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;

    return-void
.end method

.method private final toItemOptionValueCombination(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;)Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            ">;)",
            "Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;"
        }
    .end annotation

    .line 190
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 191
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cogs/itemoptions/ItemOption;

    .line 192
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v2, Ljava/util/Map;

    .line 193
    invoke-virtual {v1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 194
    invoke-virtual {v4}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getValueId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 196
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 198
    :cond_1
    new-instance p2, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-direct {p2, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V

    .line 199
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->copyOptionValueIdPairs()Ljava/util/List;

    move-result-object p1

    const-string p2, "CatalogItemVariation.Bui\u2026.copyOptionValueIdPairs()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 227
    new-instance p2, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p2, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 228
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 229
    check-cast v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 201
    iget-object v2, v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionId:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    check-cast v2, Ljava/util/Map;

    iget-object v1, v1, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;->optionValueId:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    check-cast v1, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-interface {p2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 230
    :cond_4
    check-cast p2, Ljava/util/List;

    .line 203
    new-instance p1, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;

    invoke-direct {p1, p2}, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;-><init>(Ljava/util/List;)V

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 207
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v2

    :goto_1
    if-eqz p2, :cond_3

    .line 212
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 214
    array-length v2, p2

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 215
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 216
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 218
    :cond_3
    check-cast v2, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;

    if-eqz v2, :cond_4

    goto :goto_3

    .line 46
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;->getCanSkipFetchingVariations()Z

    move-result p2

    if-nez p2, :cond_6

    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;->getItemToken()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_5

    goto :goto_2

    .line 47
    :cond_5
    sget-object p1, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState$FetchAllVariations;->INSTANCE:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState$FetchAllVariations;

    move-object v2, p1

    check-cast v2, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;

    goto :goto_3

    .line 46
    :cond_6
    :goto_2
    sget-object p1, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState$AddSingleVariation;->INSTANCE:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState$AddSingleVariation;

    move-object v2, p1

    check-cast v2, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;

    :goto_3
    return-object v2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;->initialState(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;

    check-cast p2, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;->render(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;",
            "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;",
            "-",
            "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    instance-of v0, p2, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState$FetchAllVariations;

    if-eqz v0, :cond_1

    .line 56
    iget-object p2, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;->fetchAllVariationsWorkflow:Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 57
    new-instance v2, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsProps;

    .line 58
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;->getItemToken()Ljava/lang/String;

    move-result-object p2

    if-nez p2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const/4 v0, 0x1

    .line 57
    invoke-direct {v2, p2, v0}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsProps;-><init>(Ljava/lang/String;Z)V

    const/4 v3, 0x0

    .line 61
    new-instance p2, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$render$1;

    invoke-direct {p2, p1}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$render$1;-><init>(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;)V

    move-object v4, p2

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 55
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_2

    .line 71
    :cond_1
    instance-of p2, p2, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState$AddSingleVariation;

    if-eqz p2, :cond_4

    .line 72
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;->getAssignedOptions()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 219
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 220
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 221
    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 72
    invoke-static {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionKt;->toItemOption(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 222
    :cond_2
    check-cast v0, Ljava/util/List;

    .line 74
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;->getExistingVariations()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 223
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {p2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 224
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 225
    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 74
    invoke-direct {p0, v1, v0}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;->toItemOptionValueCombination(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;)Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 226
    :cond_3
    check-cast v2, Ljava/util/List;

    .line 76
    iget-object p2, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;->selectOptionValuesForVariationWorkflow:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;

    move-object v4, p2

    check-cast v4, Lcom/squareup/workflow/Workflow;

    .line 77
    new-instance v5, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;

    .line 78
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;->getItemCogsId()Ljava/lang/String;

    move-result-object p2

    .line 77
    invoke-direct {v5, p2, v2, v0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationProps;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    const/4 v6, 0x0

    .line 82
    new-instance p2, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$render$2;

    invoke-direct {p2, p1}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$render$2;-><init>(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsProps;)V

    move-object v7, p2

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v3, p3

    .line 75
    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_2
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;

    invoke-virtual {p0, p1}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow;->snapshotState(Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
