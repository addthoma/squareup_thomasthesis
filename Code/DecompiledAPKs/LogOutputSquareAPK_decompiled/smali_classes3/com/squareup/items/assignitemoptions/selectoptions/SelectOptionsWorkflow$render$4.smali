.class final Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$4;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectOptionsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->render(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "from",
        "",
        "to",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $assignedItemOption:Ljava/util/List;

.field final synthetic $props:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic this$0:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;Lcom/squareup/workflow/Sink;Ljava/util/List;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$4;->this$0:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$4;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p3, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$4;->$assignedItemOption:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$4;->$props:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 58
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$4;->invoke(II)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(II)V
    .locals 8

    .line 125
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$4;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v7, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;

    .line 126
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$4;->$assignedItemOption:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/items/assignitemoptions/models/AssignedItemOption;

    invoke-virtual {v1}, Lcom/squareup/items/assignitemoptions/models/AssignedItemOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v2

    .line 129
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$4;->$props:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

    invoke-virtual {v1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v5

    .line 130
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$4;->this$0:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;

    invoke-static {v1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->access$getAssignItemOptionEventLogger$p(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;)Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;

    move-result-object v6

    move-object v1, v7

    move v3, p1

    move v4, p2

    .line 125
    invoke-direct/range {v1 .. v6}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;IILcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;)V

    invoke-interface {v0, v7}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
