.class public abstract Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;
.super Ljava/lang/Object;
.source "SelectOptionValuesState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;,
        Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$InvalidNewValue;,
        Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;,
        Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnReachingVariationNumberLimit;,
        Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectOptionValuesState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectOptionValuesState.kt\ncom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,160:1\n1360#2:161\n1429#2,3:162\n1866#2,7:165\n*E\n*S KotlinDebug\n*F\n+ 1 SelectOptionValuesState.kt\ncom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState\n*L\n12#1:161\n12#1,3:162\n15#1,7:165\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u000f2\u00020\u0001:\u0005\u000f\u0010\u0011\u0012\u0013B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u000c\u001a\u00020\u0000J\u0006\u0010\r\u001a\u00020\u000eR\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000b\u0082\u0001\u0004\u0014\u0015\u0016\u0017\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;",
        "Landroid/os/Parcelable;",
        "()V",
        "newValueInEditing",
        "",
        "getNewValueInEditing",
        "()Ljava/lang/String;",
        "optionValueSelections",
        "",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;",
        "getOptionValueSelections",
        "()Ljava/util/List;",
        "backToSelectValues",
        "newValueIsDuplicate",
        "",
        "Companion",
        "InvalidNewValue",
        "SelectValues",
        "WarnChanges",
        "WarnReachingVariationNumberLimit",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$InvalidNewValue;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnReachingVariationNumberLimit;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;->Companion:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 7
    invoke-direct {p0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;-><init>()V

    return-void
.end method


# virtual methods
.method public final backToSelectValues()Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;
    .locals 10

    .line 19
    instance-of v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    if-eqz v0, :cond_0

    move-object v1, p0

    check-cast v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3f

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;->copy$default(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ZZZILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;

    goto :goto_0

    .line 20
    :cond_0
    instance-of v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$InvalidNewValue;

    if-eqz v0, :cond_1

    .line 21
    move-object v0, p0

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$InvalidNewValue;

    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$InvalidNewValue;->getSelectValues()Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/16 v8, 0xf

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;->copy$default(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ZZZILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;

    goto :goto_0

    .line 25
    :cond_1
    instance-of v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;

    if-eqz v0, :cond_2

    move-object v0, p0

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;

    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->getSelectValues()Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;

    goto :goto_0

    .line 26
    :cond_2
    instance-of v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnReachingVariationNumberLimit;

    if-eqz v0, :cond_3

    move-object v0, p0

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnReachingVariationNumberLimit;

    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnReachingVariationNumberLimit;->getSelectValues()Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;

    :goto_0
    return-object v0

    :cond_3
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public abstract getNewValueInEditing()Ljava/lang/String;
.end method

.method public abstract getOptionValueSelections()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;",
            ">;"
        }
    .end annotation
.end method

.method public final newValueIsDuplicate()Z
    .locals 7

    .line 11
    invoke-virtual {p0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;->getNewValueInEditing()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const/4 v2, 0x0

    if-eqz v0, :cond_5

    .line 12
    invoke-virtual {p0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;->getOptionValueSelections()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 161
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v0, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 162
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 163
    check-cast v4, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;

    .line 13
    sget-object v5, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;->Companion:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$Companion;

    invoke-virtual {v4}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$Companion;->normalizeOptionValueNameForComparison(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 14
    sget-object v5, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;->Companion:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$Companion;

    invoke-virtual {p0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;->getNewValueInEditing()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$Companion;->normalizeOptionValueNameForComparison(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 164
    :cond_0
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 165
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 166
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 167
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 168
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 169
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_2

    if-eqz v4, :cond_1

    goto :goto_2

    :cond_1
    const/4 v3, 0x0

    goto :goto_3

    :cond_2
    :goto_2
    const/4 v3, 0x1

    .line 15
    :goto_3
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    goto :goto_1

    .line 171
    :cond_3
    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_4

    .line 166
    :cond_4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Empty collection can\'t be reduced."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_5
    const/4 v1, 0x0

    :goto_4
    return v1
.end method
