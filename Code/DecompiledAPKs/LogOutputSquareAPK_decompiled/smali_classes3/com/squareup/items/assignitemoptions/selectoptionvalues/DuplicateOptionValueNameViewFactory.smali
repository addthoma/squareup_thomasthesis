.class public final Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "DuplicateOptionValueNameViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameViewFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameViewFactory;

    invoke-direct {v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameViewFactory;-><init>()V

    sput-object v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameViewFactory;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameViewFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 6
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 7
    const-class v2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    .line 8
    sget-object v3, Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameViewFactory$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameViewFactory$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 6
    invoke-virtual {v1, v2, v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 5
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
