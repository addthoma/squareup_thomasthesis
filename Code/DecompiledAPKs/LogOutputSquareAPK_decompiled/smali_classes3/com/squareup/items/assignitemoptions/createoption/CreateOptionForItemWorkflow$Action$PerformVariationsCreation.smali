.class public final Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;
.super Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action;
.source "CreateOptionForItemWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PerformVariationsCreation"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B#\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u000f\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0008H\u00c6\u0003J-\u0010\u0013\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\u0018\u0010\u001c\u001a\u00020\u001d*\u000e\u0012\u0004\u0012\u00020\u001f\u0012\u0004\u0012\u00020 0\u001eH\u0016R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action;",
        "combinations",
        "",
        "Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;",
        "optionValueToExtendExistingVariations",
        "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
        "assignmentEngine",
        "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "(Ljava/util/List;Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V",
        "getAssignmentEngine",
        "()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "getCombinations",
        "()Ljava/util/List;",
        "getOptionValueToExtendExistingVariations",
        "()Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

.field private final combinations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;",
            ">;"
        }
    .end annotation
.end field

.field private final optionValueToExtendExistingVariations:Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;",
            ">;",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
            ")V"
        }
    .end annotation

    const-string v0, "combinations"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "optionValueToExtendExistingVariations"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignmentEngine"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 194
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->combinations:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->optionValueToExtendExistingVariations:Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    iput-object p3, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;Ljava/util/List;Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;ILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->combinations:Ljava/util/List;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->optionValueToExtendExistingVariations:Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->copy(Ljava/util/List;Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    .line 197
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->optionValueToExtendExistingVariations:Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    .line 198
    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->combinations:Ljava/util/List;

    .line 196
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->updateExistingVariationsAndCreateNewVariations(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;Ljava/util/List;)V

    .line 200
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->cleanUpIntendedOptionsAndValues()V

    .line 201
    sget-object v0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput$Created;->INSTANCE:Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput$Created;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->combinations:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->optionValueToExtendExistingVariations:Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    return-object v0
.end method

.method public final component3()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final copy(Ljava/util/List;Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;",
            ">;",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
            ")",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;"
        }
    .end annotation

    const-string v0, "combinations"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "optionValueToExtendExistingVariations"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignmentEngine"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;-><init>(Ljava/util/List;Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->combinations:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->combinations:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->optionValueToExtendExistingVariations:Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->optionValueToExtendExistingVariations:Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iget-object p1, p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final getCombinations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;",
            ">;"
        }
    .end annotation

    .line 191
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->combinations:Ljava/util/List;

    return-object v0
.end method

.method public final getOptionValueToExtendExistingVariations()Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->optionValueToExtendExistingVariations:Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->combinations:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->optionValueToExtendExistingVariations:Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PerformVariationsCreation(combinations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->combinations:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", optionValueToExtendExistingVariations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->optionValueToExtendExistingVariations:Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", assignmentEngine="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
