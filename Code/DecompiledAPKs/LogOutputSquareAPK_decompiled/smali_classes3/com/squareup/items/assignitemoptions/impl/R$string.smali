.class public final Lcom/squareup/items/assignitemoptions/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final assign_option_to_item_fetch_all_variations_failed_dialog_message:I = 0x7f1200e2

.field public static final assign_option_to_item_fetch_all_variations_failed_dialog_title:I = 0x7f1200e3

.field public static final assign_option_to_item_fetch_all_variations_spinner_label:I = 0x7f1200e4

.field public static final assign_option_to_item_select_options_action_bar_primary_button_text:I = 0x7f1200e5

.field public static final assign_option_to_item_select_options_action_bar_title:I = 0x7f1200e6

.field public static final assign_option_to_item_select_options_available_options_section_header:I = 0x7f1200e7

.field public static final assign_option_to_item_select_options_create_option_button_text:I = 0x7f1200e8

.field public static final assign_option_to_item_select_options_search_bar_hint:I = 0x7f1200e9

.field public static final combined_name_and_display_name:I = 0x7f120440

.field public static final review_variations_to_delete_header:I = 0x7f1216a6

.field public static final review_variations_to_delete_title:I = 0x7f1216a7

.field public static final select_option_values_action_bar_primary_button_text:I = 0x7f1217a3

.field public static final select_option_values_add_new_option_value_row_hint:I = 0x7f1217a4

.field public static final select_option_values_all_options:I = 0x7f1217a5

.field public static final select_option_values_capital_section_header:I = 0x7f1217a6

.field public static final select_option_values_create_from_search:I = 0x7f1217a7

.field public static final select_option_values_duplicate_option_value_name_message:I = 0x7f1217a8

.field public static final select_option_values_duplicate_option_value_name_title:I = 0x7f1217a9

.field public static final select_option_values_for_variation_add_custom_item_variation_upper_case:I = 0x7f1217aa

.field public static final select_option_values_for_variation_add_variation:I = 0x7f1217ab

.field public static final select_option_values_for_variation_choose_from_option_values_upper_case:I = 0x7f1217ac

.field public static final select_option_values_for_variation_create_variation_button_text:I = 0x7f1217ad

.field public static final select_option_values_for_variation_item_option_value_placeholder_text:I = 0x7f1217ae

.field public static final select_option_values_remove_option_set:I = 0x7f1217af

.field public static final select_option_values_search_bar_hint_format:I = 0x7f1217b0

.field public static final select_options_create_from_search:I = 0x7f1217b1

.field public static final select_options_null_state_content:I = 0x7f1217b2

.field public static final select_options_null_state_create_button_hint:I = 0x7f1217b3

.field public static final select_options_too_many_option_sets_message:I = 0x7f1217b4

.field public static final select_options_too_many_option_sets_title:I = 0x7f1217b5

.field public static final select_variations_to_create_screen_all_variations_label:I = 0x7f1217b7

.field public static final select_variations_to_create_screen_title:I = 0x7f1217b8

.field public static final select_variations_to_create_screen_update_existing_variations_row_help_text:I = 0x7f1217b9

.field public static final select_variations_to_create_screen_variations_label:I = 0x7f1217ba

.field public static final select_variations_to_create_workflow_update_existing_variations_text:I = 0x7f1217bb

.field public static final warn_duplicate_variation_with_same_option_values_message:I = 0x7f121bbf

.field public static final warn_duplicate_variation_with_same_option_values_title:I = 0x7f121bc0

.field public static final warn_reaching_variation_number_limit_message:I = 0x7f121bc1

.field public static final warn_variation_number_limit_message_plural:I = 0x7f121bc2

.field public static final warn_variation_number_limit_message_singular:I = 0x7f121bc3

.field public static final warn_variation_number_limit_title:I = 0x7f121bc4

.field public static final warn_variations_to_delete_confirm_delete_and_create:I = 0x7f121bc5

.field public static final warn_variations_to_delete_confirm_delete_plural:I = 0x7f121bc6

.field public static final warn_variations_to_delete_confirm_delete_singular:I = 0x7f121bc7

.field public static final warn_variations_to_delete_message_plural_delete_plural_add:I = 0x7f121bc8

.field public static final warn_variations_to_delete_message_plural_delete_singular_add:I = 0x7f121bc9

.field public static final warn_variations_to_delete_message_remove_multiple_values_plural_variations:I = 0x7f121bca

.field public static final warn_variations_to_delete_message_remove_option_set_no_display_plural:I = 0x7f121bcb

.field public static final warn_variations_to_delete_message_remove_option_set_no_display_singular:I = 0x7f121bcc

.field public static final warn_variations_to_delete_message_remove_option_set_plural:I = 0x7f121bcd

.field public static final warn_variations_to_delete_message_remove_option_set_singular:I = 0x7f121bce

.field public static final warn_variations_to_delete_message_remove_single_value_plural_variations:I = 0x7f121bcf

.field public static final warn_variations_to_delete_message_remove_single_value_single_variation:I = 0x7f121bd0

.field public static final warn_variations_to_delete_message_singular_delete_plural_add:I = 0x7f121bd1

.field public static final warn_variations_to_delete_message_singular_delete_singular_add:I = 0x7f121bd2

.field public static final warn_variations_to_delete_title_delete:I = 0x7f121bd3

.field public static final warn_variations_to_delete_title_delete_and_create:I = 0x7f121bd4


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
