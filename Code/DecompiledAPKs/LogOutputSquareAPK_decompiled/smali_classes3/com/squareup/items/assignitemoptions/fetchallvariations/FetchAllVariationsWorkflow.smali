.class public final Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "FetchAllVariationsWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsProps;",
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState;",
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFetchAllVariationsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FetchAllVariationsWorkflow.kt\ncom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 4 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 5 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 6 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,133:1\n32#2,12:134\n149#3,5:146\n149#3,5:154\n85#4:151\n240#5:152\n276#6:153\n*E\n*S KotlinDebug\n*F\n+ 1 FetchAllVariationsWorkflow.kt\ncom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow\n*L\n63#1,12:134\n73#1,5:146\n90#1,5:154\n78#1:151\n78#1:152\n78#1:153\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0001\u001cB\u000f\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u001c\u0010\r\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00100\u000f0\u000e2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u001a\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00022\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016JN\u0010\u0017\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00032\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u0003H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsProps;",
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState;",
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "(Lcom/squareup/cogs/Cogs;)V",
        "fetchItem",
        "Lio/reactivex/Single;",
        "Lcom/squareup/shared/catalog/sync/SyncResult;",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
        "itemToken",
        "",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cogs:Lcom/squareup/cogs/Cogs;


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/Cogs;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cogs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;->cogs:Lcom/squareup/cogs/Cogs;

    return-void
.end method

.method private final fetchItem(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;>;"
        }
    .end annotation

    .line 129
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$fetchItem$1;

    invoke-direct {v1, p1}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$fetchItem$1;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/shared/catalog/CatalogOnlineTask;

    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->asSingleOnline(Lcom/squareup/shared/catalog/CatalogOnlineTask;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "cogs.asSingleOnline { on\u2026V2Object(itemToken)\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 134
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 139
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 141
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 142
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 143
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 145
    :cond_3
    check-cast v1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 63
    :cond_4
    sget-object p1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState$FetchInProgress;->INSTANCE:Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState$FetchInProgress;

    move-object v1, p1

    check-cast v1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;->initialState(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsProps;

    check-cast p2, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;->render(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsProps;Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsProps;Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsProps;",
            "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 72
    new-instance v1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchInProgressScreen;

    invoke-direct {v1}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchInProgressScreen;-><init>()V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 147
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 148
    const-class v3, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchInProgressScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, ""

    invoke-static {v3, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 149
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 147
    invoke-direct {v2, v3, v1, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 74
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsProps;->getShownInCard()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    .line 76
    :goto_0
    instance-of v3, p2, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState$FetchInProgress;

    if-eqz v3, :cond_1

    .line 78
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsProps;->getItemToken()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;->fetchItem(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 151
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$render$$inlined$asWorker$1;

    const/4 v0, 0x0

    invoke-direct {p2, p1, v0}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 152
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 153
    const-class p2, Lcom/squareup/shared/catalog/sync/SyncResult;

    sget-object v0, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    invoke-virtual {v0, v3}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v0

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v0

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 79
    sget-object p1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$render$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$render$1;

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p3

    .line 77
    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 84
    invoke-static {v2, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    .line 86
    :cond_1
    instance-of p1, p2, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState$FetchFailed;

    if-eqz p1, :cond_2

    .line 88
    new-instance p1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedScreen;

    .line 89
    new-instance p2, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$render$failureDialogScreen$1;

    invoke-direct {p2, v0}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$render$failureDialogScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    new-instance p3, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$render$failureDialogScreen$2;

    invoke-direct {p3, v0}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow$render$failureDialogScreen$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    .line 88
    invoke-direct {p1, p2, p3}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedScreen;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 155
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 156
    const-class p3, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 157
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 155
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 91
    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 p3, 0x2

    new-array p3, p3, [Lkotlin/Pair;

    const/4 v0, 0x0

    .line 92
    new-instance v3, Lkotlin/Pair;

    invoke-direct {v3, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, p3, v0

    const/4 v0, 0x1

    .line 93
    new-instance v1, Lkotlin/Pair;

    sget-object v2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-direct {v1, v2, p2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, p3, v0

    .line 91
    invoke-virtual {p1, p3}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState;

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsWorkflow;->snapshotState(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
