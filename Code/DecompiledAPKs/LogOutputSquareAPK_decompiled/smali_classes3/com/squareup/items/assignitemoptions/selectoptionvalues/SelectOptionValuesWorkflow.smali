.class public final Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "SelectOptionValuesWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action;,
        Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$DialogAction;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectOptionValuesWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectOptionValuesWorkflow.kt\ncom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 4 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,468:1\n32#2,12:469\n149#3,5:481\n149#3,5:486\n149#3,5:491\n149#3,5:496\n149#3,5:501\n149#3,5:506\n704#4:511\n777#4,2:512\n1360#4:514\n1429#4,3:515\n1550#4,3:518\n*E\n*S KotlinDebug\n*F\n+ 1 SelectOptionValuesWorkflow.kt\ncom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow\n*L\n65#1,12:469\n78#1,5:481\n91#1,5:486\n92#1,5:491\n116#1,5:496\n128#1,5:501\n129#1,5:506\n386#1:511\n386#1,2:512\n389#1:514\n389#1,3:515\n405#1,3:518\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010%\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0002#$B\u000f\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u001a\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u000f\u001a\u00020\u00022\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016JN\u0010\u0016\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00032\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u00152\u0006\u0010\u0010\u001a\u00020\u0003H\u0016JX\u0010\u001a\u001a\u001a\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u001b*\u001a\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u001b2\u0006\u0010\u001c\u001a\u00020\u00062\u0012\u0010\u001d\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u0008H\u0002JD\u0010\u001e\u001a\u00020\u001f*\u00020\u00132\u0006\u0010\u000f\u001a\u00020\u00022\u001a\u0010 \u001a\u0016\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\"\u0018\u00010!2\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0018H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "warnVariationsToDeleteWorkflow",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteWorkflow;",
        "(Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteWorkflow;)V",
        "getWarningDescription",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription;",
        "props",
        "state",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;",
        "initialState",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "renderScreenInBackground",
        "",
        "backgroundLayer",
        "backgroundScreen",
        "toSelectOptionValuesScreen",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;",
        "actionSink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Action",
        "DialogAction",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final warnVariationsToDeleteWorkflow:Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "warnVariationsToDeleteWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;->warnVariationsToDeleteWorkflow:Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteWorkflow;

    return-void
.end method

.method private final getWarningDescription(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;)Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription;
    .locals 6

    .line 139
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->needsToDeleteVariations()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 143
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->findAllVariationsToDelete()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 144
    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->isOptionSetRemoved()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v1, :cond_2

    .line 146
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/cogs/itemoptions/ItemOption;->getName()Ljava/lang/String;

    move-result-object p2

    .line 147
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    if-eqz v2, :cond_1

    goto :goto_0

    .line 150
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    .line 145
    :goto_0
    new-instance p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$RemovedOptionSetDescription;

    invoke-direct {p1, p2, v4, v0}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$RemovedOptionSetDescription;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    check-cast p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription;

    goto/16 :goto_3

    .line 154
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->getValueToExtend()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v5}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getIdPair()Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    move-result-object v5

    goto :goto_1

    :cond_3
    move-object v5, v4

    :goto_1
    invoke-virtual {v1, v5}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->canGenerateCombinations(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 155
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object p1

    .line 156
    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->getValueToExtend()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object p2

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getIdPair()Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    move-result-object v4

    .line 155
    :cond_4
    invoke-virtual {p1, v4}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->generateCombinations(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    .line 158
    new-instance p2, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$AddedAndDeletedValuesDescription;

    invoke-direct {p2, v0, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$AddedAndDeletedValuesDescription;-><init>(II)V

    move-object p1, p2

    check-cast p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription;

    goto :goto_3

    .line 163
    :cond_5
    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->getRemovedValues()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v3

    if-eqz p1, :cond_7

    .line 167
    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->getRemovedValues()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-le p1, v3, :cond_6

    .line 168
    sget-object p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;->Companion:Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription$Companion;

    .line 169
    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->getRemovedValues()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    .line 168
    invoke-virtual {p1, p2, v0}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription$Companion;->multipleValues(II)Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;

    move-result-object p1

    goto :goto_2

    .line 173
    :cond_6
    sget-object p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;->Companion:Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription$Companion;

    .line 174
    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->getRemovedValues()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {p2}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getName()Ljava/lang/String;

    move-result-object p2

    .line 173
    invoke-virtual {p1, p2, v0}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription$Companion;->singleValue(Ljava/lang/String;I)Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;

    move-result-object p1

    .line 167
    :goto_2
    check-cast p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription;

    :goto_3
    return-object p1

    .line 163
    :cond_7
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "If there are deletes there should either be a removed option set or removed values"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 139
    :cond_8
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Should only go to WarnChanges if there are deletes to be made"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final renderScreenInBackground(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 186
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 187
    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object p1
.end method

.method private final toSelectOptionValuesScreen(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;Lcom/squareup/workflow/Sink;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;",
            "+",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesOutput;",
            ">;>;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesOutput;",
            ">;)",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;"
        }
    .end annotation

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    .line 386
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;->getOptionValueSelections()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 511
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/Collection;

    .line 512
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v6, 0x0

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v7, v5

    check-cast v7, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;

    .line 387
    sget-object v8, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;->Companion:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$Companion;

    invoke-virtual {v7}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$Companion;->normalizeOptionValueNameForComparison(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    .line 388
    sget-object v8, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;->Companion:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$Companion;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;->getSearchText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$Companion;->normalizeOptionValueNameForComparison(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    const/4 v9, 0x2

    const/4 v10, 0x0

    invoke-static {v7, v8, v6, v9, v10}, Lkotlin/text/StringsKt;->contains$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 513
    :cond_1
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 514
    new-instance v3, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {v4, v5}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 515
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 516
    move-object v7, v5

    check-cast v7, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;

    .line 390
    sget-object v5, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;->Companion:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$Companion;

    invoke-virtual {v7}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$Companion;->normalizeOptionValueNameForComparison(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 391
    sget-object v8, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;->Companion:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$Companion;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;->getNewValueInEditing()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$Companion;->normalizeOptionValueNameForComparison(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x3

    const/4 v12, 0x0

    .line 392
    invoke-static/range {v7 .. v12}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->copy$default(Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 517
    :cond_2
    move-object v14, v3

    check-cast v14, Ljava/util/List;

    .line 394
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;->getOptionValueSelections()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v3, v4, :cond_3

    const/4 v9, 0x1

    goto :goto_2

    :cond_3
    const/4 v9, 0x0

    .line 395
    :goto_2
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->getAlreadyAssigned()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 396
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;->getRemoveButtonWarning()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 397
    sget-object v3, Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;->SHOW_CONFIRM_DELETE_BUTTON:Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;

    goto :goto_3

    .line 399
    :cond_4
    sget-object v3, Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;->SHOW_DELETE_BUTTON:Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;

    goto :goto_3

    .line 401
    :cond_5
    sget-object v3, Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;->HIDE_DELETE_BUTTON:Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;

    :goto_3
    move-object v10, v3

    .line 405
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;->getOptionValueSelections()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 518
    instance-of v4, v3, Ljava/util/Collection;

    if-eqz v4, :cond_7

    move-object v4, v3

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_6
    const/4 v8, 0x0

    goto :goto_4

    .line 519
    :cond_7
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;

    .line 405
    invoke-virtual {v4}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isSelected()Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v8, 0x1

    .line 409
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;->getSearchText()Ljava/lang/String;

    move-result-object v3

    .line 411
    sget-object v4, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$2;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$2;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const-string v5, "SelectOptionValues-Search"

    .line 408
    invoke-static {v2, v3, v5, v4}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v11

    .line 412
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreenKt;->itemOptionName(Lcom/squareup/cogs/itemoptions/ItemOption;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;

    move-result-object v12

    .line 414
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;->getNewValueInEditing()Ljava/lang/String;

    move-result-object v3

    .line 415
    sget-object v4, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$3;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$3;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const-string v5, "OptionValueNameField"

    .line 413
    invoke-static {v2, v3, v5, v4}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v13

    .line 419
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;->getOptionValueSelections()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v15

    .line 420
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;->getShouldHighlightDuplicateName()Z

    move-result v16

    .line 421
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;->getShouldFocusOnNewValueRowAndShowKeyboard()Z

    move-result v17

    .line 423
    new-instance v2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$4;

    invoke-direct {v2, v1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$4;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object/from16 v18, v2

    check-cast v18, Lkotlin/jvm/functions/Function1;

    .line 426
    new-instance v2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$5;

    invoke-direct {v2, v1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$5;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object/from16 v19, v2

    check-cast v19, Lkotlin/jvm/functions/Function2;

    .line 429
    new-instance v2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$6;

    invoke-direct {v2, v1, v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$6;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;)V

    move-object/from16 v20, v2

    check-cast v20, Lkotlin/jvm/functions/Function0;

    .line 432
    new-instance v2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$7;

    invoke-direct {v2, v1, v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$7;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;)V

    move-object/from16 v21, v2

    check-cast v21, Lkotlin/jvm/functions/Function0;

    .line 435
    new-instance v2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$8;

    invoke-direct {v2, v1, v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$8;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;)V

    move-object/from16 v22, v2

    check-cast v22, Lkotlin/jvm/functions/Function0;

    .line 442
    new-instance v2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$9;

    invoke-direct {v2, v1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$9;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object/from16 v23, v2

    check-cast v23, Lkotlin/jvm/functions/Function0;

    .line 445
    new-instance v2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$10;

    invoke-direct {v2, v1, v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$10;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;)V

    move-object/from16 v24, v2

    check-cast v24, Lkotlin/jvm/functions/Function0;

    .line 404
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;

    move-object v7, v0

    invoke-direct/range {v7 .. v24}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;-><init>(ZZLcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;IZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method


# virtual methods
.method public initialState(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;
    .locals 12

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 469
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v2

    :goto_1
    if-eqz p2, :cond_3

    .line 474
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 475
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 476
    array-length v2, p2

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 477
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 478
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 479
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 480
    :cond_3
    check-cast v2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    if-eqz v2, :cond_4

    move-object p2, v2

    goto :goto_2

    .line 65
    :cond_4
    new-instance p2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    .line 66
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->getOptionValueSelections()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x3e

    const/4 v11, 0x0

    move-object v3, p2

    .line 65
    invoke-direct/range {v3 .. v11}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_2
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;->initialState(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;

    check-cast p2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;->render(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 77
    instance-of v1, p2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    const-string v2, ""

    if-eqz v1, :cond_0

    .line 78
    check-cast p2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    invoke-direct {p0, p2, p1, v0, p3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;->toSelectOptionValuesScreen(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;Lcom/squareup/workflow/Sink;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 482
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 483
    const-class p3, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 484
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 482
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 79
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 81
    :cond_0
    instance-of v1, p2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$InvalidNewValue;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    if-eqz v1, :cond_1

    .line 83
    check-cast p2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$InvalidNewValue;

    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$InvalidNewValue;->getSelectValues()Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    move-result-object p2

    invoke-direct {p0, p2, p1, v6, p3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;->toSelectOptionValuesScreen(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;Lcom/squareup/workflow/Sink;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;

    move-result-object p1

    .line 85
    new-instance p2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameScreen;

    .line 86
    new-instance p3, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$render$duplicateOptionDialogScreen$1;

    invoke-direct {p3, v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$render$duplicateOptionDialogScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    .line 85
    invoke-direct {p2, p3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 90
    sget-object p3, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    new-array v0, v5, [Lkotlin/Pair;

    .line 91
    sget-object v1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 487
    new-instance v5, Lcom/squareup/workflow/legacy/Screen;

    .line 488
    const-class v6, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;

    invoke-static {v6}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v6

    invoke-static {v6, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v6

    .line 489
    sget-object v7, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v7}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v7

    .line 487
    invoke-direct {v5, v6, p1, v7}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 91
    new-instance p1, Lkotlin/Pair;

    invoke-direct {p1, v1, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object p1, v0, v4

    .line 92
    sget-object p1, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 492
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 493
    const-class v4, Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameScreen;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 494
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 492
    invoke-direct {v1, v2, p2, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 92
    new-instance p2, Lkotlin/Pair;

    invoke-direct {p2, p1, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object p2, v0, v3

    .line 90
    invoke-virtual {p3, v0}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 95
    :cond_1
    instance-of v1, p2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;

    if-eqz v1, :cond_2

    .line 97
    move-object v0, p2

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;

    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->getSelectValues()Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    move-result-object v1

    invoke-direct {p0, v1, p1, v6, p3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;->toSelectOptionValuesScreen(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;Lcom/squareup/workflow/Sink;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;

    move-result-object v1

    .line 101
    iget-object v3, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;->warnVariationsToDeleteWorkflow:Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteWorkflow;

    move-object v5, v3

    check-cast v5, Lcom/squareup/workflow/Workflow;

    .line 102
    new-instance v6, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteProps;

    .line 103
    invoke-direct {p0, p1, v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;->getWarningDescription(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;)Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription;

    move-result-object v0

    .line 102
    invoke-direct {v6, v0}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteProps;-><init>(Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription;)V

    const/4 v7, 0x0

    .line 105
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$render$1;

    invoke-direct {v0, p2, p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$render$1;-><init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;)V

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v9, 0x4

    const/4 v10, 0x0

    move-object v4, p3

    .line 100
    invoke-static/range {v4 .. v10}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    .line 114
    invoke-static {p1}, Lkotlin/collections/MapsKt;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    .line 115
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    .line 116
    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 497
    new-instance p3, Lcom/squareup/workflow/legacy/Screen;

    .line 498
    const-class v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 499
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 497
    invoke-direct {p3, v0, v1, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 114
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;->renderScreenInBackground(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    .line 117
    invoke-static {p1}, Lkotlin/collections/MapsKt;->toMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 120
    :cond_2
    instance-of v1, p2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnReachingVariationNumberLimit;

    if-eqz v1, :cond_3

    .line 122
    check-cast p2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnReachingVariationNumberLimit;

    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnReachingVariationNumberLimit;->getSelectValues()Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;

    move-result-object p2

    invoke-direct {p0, p2, p1, v6, p3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;->toSelectOptionValuesScreen(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;Lcom/squareup/workflow/Sink;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;

    move-result-object p1

    .line 124
    new-instance p2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitScreen;

    .line 125
    new-instance p3, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$render$warnReachingVariationNumberLimitScreen$1;

    invoke-direct {p3, v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$render$warnReachingVariationNumberLimitScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    .line 124
    invoke-direct {p2, p3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 127
    sget-object p3, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    new-array v0, v5, [Lkotlin/Pair;

    .line 128
    sget-object v1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 502
    new-instance v5, Lcom/squareup/workflow/legacy/Screen;

    .line 503
    const-class v6, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;

    invoke-static {v6}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v6

    invoke-static {v6, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v6

    .line 504
    sget-object v7, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v7}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v7

    .line 502
    invoke-direct {v5, v6, p1, v7}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 128
    new-instance p1, Lkotlin/Pair;

    invoke-direct {p1, v1, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object p1, v0, v4

    .line 129
    sget-object p1, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 507
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 508
    const-class v4, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitScreen;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 509
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 507
    invoke-direct {v1, v2, p2, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 129
    new-instance p2, Lkotlin/Pair;

    invoke-direct {p2, p1, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object p2, v0, v3

    .line 127
    invoke-virtual {p3, v0}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;->snapshotState(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
