.class final Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$2$1;
.super Ljava/lang/Object;
.source "SaveUnitWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$2;->apply(Lcom/squareup/shared/catalog/sync/SyncResult;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0014\u0010\u0004\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/shared/catalog/sync/SyncResult;",
        "Ljava/lang/Void;",
        "kotlin.jvm.PlatformType",
        "syncResult",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$2$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$2$1;

    invoke-direct {v0}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$2$1;-><init>()V

    sput-object v0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$2$1;->INSTANCE:Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$2$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/shared/catalog/sync/SyncResult;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const-string v0, "syncResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 207
    check-cast p1, Lcom/squareup/shared/catalog/sync/SyncResult;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$2$1;->apply(Lcom/squareup/shared/catalog/sync/SyncResult;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1
.end method
