.class final Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupDeleteButton$1;
.super Ljava/lang/Object;
.source "EditUnitCoordinator.kt"

# interfaces
.implements Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/ui/EditUnitCoordinator;->setupDeleteButton(Lcom/squareup/items/unit/EditUnitScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "onConfirm"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/items/unit/EditUnitScreen;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/EditUnitScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupDeleteButton$1;->$screen:Lcom/squareup/items/unit/EditUnitScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onConfirm()V
    .locals 2

    .line 182
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupDeleteButton$1;->$screen:Lcom/squareup/items/unit/EditUnitScreen;

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    sget-object v1, Lcom/squareup/items/unit/EditUnitScreen$Event$DeleteUnit;->INSTANCE:Lcom/squareup/items/unit/EditUnitScreen$Event$DeleteUnit;

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
