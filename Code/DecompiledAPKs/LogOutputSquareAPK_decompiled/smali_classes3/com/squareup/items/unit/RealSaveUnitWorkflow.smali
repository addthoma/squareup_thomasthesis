.class public final Lcom/squareup/items/unit/RealSaveUnitWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "SaveUnitWorkflow.kt"

# interfaces
.implements Lcom/squareup/items/unit/SaveUnitWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;,
        Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;,
        Lcom/squareup/items/unit/RealSaveUnitWorkflow$FetchCountOfAffectedVariations;,
        Lcom/squareup/items/unit/RealSaveUnitWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/items/unit/SaveUnitInput;",
        "Lcom/squareup/items/unit/SaveUnitState;",
        "Lcom/squareup/items/unit/SaveUnitResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/workflow/MainAndModal;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/items/unit/SaveUnitWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSaveUnitWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SaveUnitWorkflow.kt\ncom/squareup/items/unit/RealSaveUnitWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,283:1\n149#2,5:284\n149#2,5:289\n149#2,5:294\n149#2,5:299\n149#2,5:304\n*E\n*S KotlinDebug\n*F\n+ 1 SaveUnitWorkflow.kt\ncom/squareup/items/unit/RealSaveUnitWorkflow\n*L\n137#1,5:284\n150#1,5:289\n176#1,5:294\n181#1,5:299\n187#1,5:304\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u0000 \u001f2\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0004\u001f !\"B\u0019\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0008\u0008\u0001\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u001a\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00032\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0016JN\u0010\u001a\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u00042\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001dH\u0016J\u0010\u0010\u001e\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\u0004H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/items/unit/RealSaveUnitWorkflow;",
        "Lcom/squareup/items/unit/SaveUnitWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/items/unit/SaveUnitInput;",
        "Lcom/squareup/items/unit/SaveUnitState;",
        "Lcom/squareup/items/unit/SaveUnitResult;",
        "",
        "Lcom/squareup/workflow/MainAndModal;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Lcom/squareup/cogs/Cogs;Lio/reactivex/Scheduler;)V",
        "getUnitSaveFailedAlertScreenConfiguration",
        "Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;",
        "error",
        "Lcom/squareup/shared/catalog/sync/SyncError;",
        "saving",
        "Lcom/squareup/items/unit/SaveUnitState$Saving;",
        "initialState",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Companion",
        "DeleteUnitAndSync",
        "FetchCountOfAffectedVariations",
        "UpsertUnit",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/unit/RealSaveUnitWorkflow$Companion;

.field public static final MAX_DISPLAYED_COUNT_OF_AFFECTED_VARIATIONS:I = 0x64


# instance fields
.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final mainScheduler:Lio/reactivex/Scheduler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/unit/RealSaveUnitWorkflow;->Companion:Lcom/squareup/items/unit/RealSaveUnitWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cogs/Cogs;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cogs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow;->cogs:Lcom/squareup/cogs/Cogs;

    iput-object p2, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow;->mainScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method public static final synthetic access$getCogs$p(Lcom/squareup/items/unit/RealSaveUnitWorkflow;)Lcom/squareup/cogs/Cogs;
    .locals 0

    .line 74
    iget-object p0, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow;->cogs:Lcom/squareup/cogs/Cogs;

    return-object p0
.end method

.method public static final synthetic access$getMainScheduler$p(Lcom/squareup/items/unit/RealSaveUnitWorkflow;)Lio/reactivex/Scheduler;
    .locals 0

    .line 74
    iget-object p0, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow;->mainScheduler:Lio/reactivex/Scheduler;

    return-object p0
.end method

.method public static final synthetic access$getUnitSaveFailedAlertScreenConfiguration(Lcom/squareup/items/unit/RealSaveUnitWorkflow;Lcom/squareup/shared/catalog/sync/SyncError;Lcom/squareup/items/unit/SaveUnitState$Saving;)Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;
    .locals 0

    .line 74
    invoke-direct {p0, p1, p2}, Lcom/squareup/items/unit/RealSaveUnitWorkflow;->getUnitSaveFailedAlertScreenConfiguration(Lcom/squareup/shared/catalog/sync/SyncError;Lcom/squareup/items/unit/SaveUnitState$Saving;)Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    move-result-object p0

    return-object p0
.end method

.method private final getUnitSaveFailedAlertScreenConfiguration(Lcom/squareup/shared/catalog/sync/SyncError;Lcom/squareup/items/unit/SaveUnitState$Saving;)Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;
    .locals 5

    .line 246
    iget-object p1, p1, Lcom/squareup/shared/catalog/sync/SyncError;->errorType:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    sget-object v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_NETWORK:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 248
    :goto_0
    invoke-virtual {p2}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getSaveUnitAction()Lcom/squareup/items/unit/SaveUnitAction;

    move-result-object v0

    sget-object v2, Lcom/squareup/items/unit/RealSaveUnitWorkflow$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {v0}, Lcom/squareup/items/unit/SaveUnitAction;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x2

    if-eq v0, v1, :cond_2

    if-ne v0, v2, :cond_1

    .line 253
    sget v0, Lcom/squareup/editunit/R$string;->delete_unit_failed_alert_title:I

    goto :goto_1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 250
    :cond_2
    invoke-virtual {p2}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getUnitToSave()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/items/unit/SaveUnitWorkflowKt;->isNew(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lcom/squareup/editunit/R$string;->create_unit_failed_alert_title:I

    goto :goto_1

    .line 251
    :cond_3
    sget v0, Lcom/squareup/editunit/R$string;->edit_unit_failed_alert_title:I

    .line 256
    :goto_1
    invoke-virtual {p2}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getSaveUnitAction()Lcom/squareup/items/unit/SaveUnitAction;

    move-result-object v3

    sget-object v4, Lcom/squareup/items/unit/RealSaveUnitWorkflow$WhenMappings;->$EnumSwitchMapping$4:[I

    invoke-virtual {v3}, Lcom/squareup/items/unit/SaveUnitAction;->ordinal()I

    move-result v3

    aget v3, v4, v3

    if-eq v3, v1, :cond_6

    if-ne v3, v2, :cond_5

    if-eqz p1, :cond_4

    .line 267
    sget p2, Lcom/squareup/editunit/R$string;->delete_unit_failed_alert_message_due_to_network:I

    goto :goto_2

    .line 268
    :cond_4
    sget p2, Lcom/squareup/editunit/R$string;->delete_unit_failed_alert_message_not_due_to_network:I

    goto :goto_2

    .line 267
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 258
    :cond_6
    invoke-virtual {p2}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getUnitToSave()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/items/unit/SaveUnitWorkflowKt;->isNew(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Z

    move-result p2

    if-eqz p2, :cond_8

    if-eqz p1, :cond_7

    .line 259
    sget p2, Lcom/squareup/editunit/R$string;->create_unit_failed_alert_message_due_to_network:I

    goto :goto_2

    .line 260
    :cond_7
    sget p2, Lcom/squareup/editunit/R$string;->create_unit_failed_alert_message_not_due_to_network:I

    goto :goto_2

    :cond_8
    if-eqz p1, :cond_9

    .line 262
    sget p2, Lcom/squareup/editunit/R$string;->edit_unit_failed_alert_message_due_to_network:I

    goto :goto_2

    .line 263
    :cond_9
    sget p2, Lcom/squareup/editunit/R$string;->edit_unit_failed_alert_message_not_due_to_network:I

    .line 272
    :goto_2
    new-instance v1, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    invoke-direct {v1, v0, p2, p1}, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;-><init>(IIZ)V

    return-object v1
.end method


# virtual methods
.method public initialState(Lcom/squareup/items/unit/SaveUnitInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/unit/SaveUnitState;
    .locals 4

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p1}, Lcom/squareup/items/unit/SaveUnitInput;->getSaveUnitAction()Lcom/squareup/items/unit/SaveUnitAction;

    move-result-object v0

    sget-object v1, Lcom/squareup/items/unit/RealSaveUnitWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/items/unit/SaveUnitAction;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    goto :goto_0

    .line 89
    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 88
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/items/unit/SaveUnitInput;->getUnitToSave()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/items/unit/SaveUnitWorkflowKt;->isNew(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 92
    :goto_0
    new-instance v0, Lcom/squareup/items/unit/SaveUnitState$Saving;

    invoke-virtual {p1}, Lcom/squareup/items/unit/SaveUnitInput;->getIdempotencyKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/items/unit/SaveUnitInput;->getUnitToSave()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/items/unit/SaveUnitInput;->getSaveUnitAction()Lcom/squareup/items/unit/SaveUnitAction;

    move-result-object p1

    invoke-direct {v0, v2, v3, p1}, Lcom/squareup/items/unit/SaveUnitState$Saving;-><init>(Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/items/unit/SaveUnitAction;)V

    if-eqz v1, :cond_3

    .line 94
    new-instance p1, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    invoke-direct {p1, v0}, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;-><init>(Lcom/squareup/items/unit/SaveUnitState$Saving;)V

    check-cast p1, Lcom/squareup/items/unit/SaveUnitState;

    goto :goto_1

    .line 97
    :cond_3
    move-object p1, v0

    check-cast p1, Lcom/squareup/items/unit/SaveUnitState;

    :goto_1
    if-eqz p2, :cond_4

    .line 100
    sget-object v0, Lcom/squareup/items/unit/SaveUnitState;->Companion:Lcom/squareup/items/unit/SaveUnitState$Companion;

    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/items/unit/SaveUnitState$Companion;->fromSnapshot(Lokio/ByteString;)Lcom/squareup/items/unit/SaveUnitState;

    move-result-object p2

    if-eqz p2, :cond_4

    move-object p1, p2

    :cond_4
    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 74
    check-cast p1, Lcom/squareup/items/unit/SaveUnitInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/unit/RealSaveUnitWorkflow;->initialState(Lcom/squareup/items/unit/SaveUnitInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/unit/SaveUnitState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 74
    check-cast p1, Lcom/squareup/items/unit/SaveUnitInput;

    check-cast p2, Lcom/squareup/items/unit/SaveUnitState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/unit/RealSaveUnitWorkflow;->render(Lcom/squareup/items/unit/SaveUnitInput;Lcom/squareup/items/unit/SaveUnitState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/items/unit/SaveUnitInput;Lcom/squareup/items/unit/SaveUnitState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/unit/SaveUnitInput;",
            "Lcom/squareup/items/unit/SaveUnitState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/unit/SaveUnitState;",
            "-",
            "Lcom/squareup/items/unit/SaveUnitResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    instance-of p1, p2, Lcom/squareup/items/unit/SaveUnitState$Saving;

    const-string v0, ""

    if-eqz p1, :cond_2

    .line 110
    move-object p1, p2

    check-cast p1, Lcom/squareup/items/unit/SaveUnitState$Saving;

    invoke-virtual {p1}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getSaveUnitAction()Lcom/squareup/items/unit/SaveUnitAction;

    move-result-object v1

    sget-object v2, Lcom/squareup/items/unit/RealSaveUnitWorkflow$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v1}, Lcom/squareup/items/unit/SaveUnitAction;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 112
    new-instance v1, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;

    invoke-virtual {p1}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getUnitToSave()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getId()Ljava/lang/String;

    move-result-object v2

    const-string v3, "state.unitToSave.id"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p0, v2}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;-><init>(Lcom/squareup/items/unit/RealSaveUnitWorkflow;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/workflow/rx2/PublisherWorker;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 111
    :cond_1
    new-instance v1, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;

    invoke-virtual {p1}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getIdempotencyKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getUnitToSave()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$UpsertUnit;-><init>(Lcom/squareup/items/unit/RealSaveUnitWorkflow;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    check-cast v1, Lcom/squareup/workflow/rx2/PublisherWorker;

    .line 115
    :goto_0
    check-cast v1, Lcom/squareup/workflow/Worker;

    .line 116
    invoke-virtual {p1}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getIdempotencyKey()Ljava/lang/String;

    move-result-object p1

    .line 117
    new-instance v2, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$1;

    invoke-direct {v2, p0, p2}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$1;-><init>(Lcom/squareup/items/unit/RealSaveUnitWorkflow;Lcom/squareup/items/unit/SaveUnitState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 114
    invoke-interface {p3, v1, p1, v2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 137
    new-instance p1, Lcom/squareup/items/unit/SaveInProgressScreen;

    invoke-direct {p1}, Lcom/squareup/items/unit/SaveInProgressScreen;-><init>()V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 285
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 286
    const-class p3, Lcom/squareup/items/unit/SaveInProgressScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 287
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 285
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 138
    sget-object p1, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 141
    :cond_2
    instance-of p1, p2, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 142
    new-instance v1, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen;

    move-object v2, p2

    check-cast v2, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;

    new-instance v3, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$3;

    invoke-direct {v3, p2}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$3;-><init>(Lcom/squareup/items/unit/SaveUnitState;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {v1, v2, p2}, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen;-><init>(Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 290
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 291
    const-class p3, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 292
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 290
    invoke-direct {p2, p3, v1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 141
    invoke-virtual {p1, p2}, Lcom/squareup/workflow/MainAndModal$Companion;->partial(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 153
    :cond_3
    instance-of p1, p2, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    if-eqz p1, :cond_4

    .line 155
    new-instance p1, Lcom/squareup/items/unit/RealSaveUnitWorkflow$FetchCountOfAffectedVariations;

    move-object v1, p2

    check-cast v1, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    invoke-virtual {v1}, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;->getIncomingSaving()Lcom/squareup/items/unit/SaveUnitState$Saving;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getUnitToSave()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getId()Ljava/lang/String;

    move-result-object v2

    const-string v3, "state.incomingSaving.unitToSave.id"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, p0, v2}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$FetchCountOfAffectedVariations;-><init>(Lcom/squareup/items/unit/RealSaveUnitWorkflow;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/workflow/Worker;

    .line 156
    invoke-virtual {v1}, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;->getIncomingSaving()Lcom/squareup/items/unit/SaveUnitState$Saving;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/items/unit/SaveUnitState$Saving;->getIdempotencyKey()Ljava/lang/String;

    move-result-object v1

    .line 157
    new-instance v2, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$4;

    invoke-direct {v2, p0, p2}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$4;-><init>(Lcom/squareup/items/unit/RealSaveUnitWorkflow;Lcom/squareup/items/unit/SaveUnitState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 154
    invoke-interface {p3, p1, v1, v2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 176
    new-instance p1, Lcom/squareup/items/unit/SaveInProgressScreen;

    invoke-direct {p1}, Lcom/squareup/items/unit/SaveInProgressScreen;-><init>()V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 295
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 296
    const-class p3, Lcom/squareup/items/unit/SaveInProgressScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 297
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 295
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 177
    sget-object p1, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    .line 180
    :cond_4
    instance-of p1, p2, Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;

    if-eqz p1, :cond_5

    sget-object p1, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 181
    new-instance v1, Lcom/squareup/items/unit/SaveInProgressScreen;

    invoke-direct {v1}, Lcom/squareup/items/unit/SaveInProgressScreen;-><init>()V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 300
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 301
    const-class v3, Lcom/squareup/items/unit/SaveInProgressScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 302
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 300
    invoke-direct {v2, v3, v1, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 182
    new-instance v1, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;

    move-object v3, p2

    check-cast v3, Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;

    new-instance v4, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$6;

    invoke-direct {v4, p2}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$6;-><init>(Lcom/squareup/items/unit/SaveUnitState;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {v1, v3, p2}, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;-><init>(Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 305
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 306
    const-class p3, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 307
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 305
    invoke-direct {p2, p3, v1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 180
    invoke-virtual {p1, v2, p2}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/items/unit/SaveUnitState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    invoke-virtual {p1}, Lcom/squareup/items/unit/SaveUnitState;->toSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 74
    check-cast p1, Lcom/squareup/items/unit/SaveUnitState;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/RealSaveUnitWorkflow;->snapshotState(Lcom/squareup/items/unit/SaveUnitState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
