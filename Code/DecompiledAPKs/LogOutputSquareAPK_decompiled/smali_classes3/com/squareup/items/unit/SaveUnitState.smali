.class public abstract Lcom/squareup/items/unit/SaveUnitState;
.super Ljava/lang/Object;
.source "SaveUnitState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/unit/SaveUnitState$Saving;,
        Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;,
        Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;,
        Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;,
        Lcom/squareup/items/unit/SaveUnitState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00052\u00020\u0001:\u0005\u0005\u0006\u0007\u0008\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u0082\u0001\u0004\n\u000b\u000c\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/items/unit/SaveUnitState;",
        "",
        "()V",
        "toSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "Companion",
        "DisplayCountOfAffectedVariations",
        "FetchingCountOfAffectedVariations",
        "Saving",
        "UnitSaveFailed",
        "Lcom/squareup/items/unit/SaveUnitState$Saving;",
        "Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;",
        "Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;",
        "Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/unit/SaveUnitState$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/items/unit/SaveUnitState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/unit/SaveUnitState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/unit/SaveUnitState;->Companion:Lcom/squareup/items/unit/SaveUnitState$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/items/unit/SaveUnitState;-><init>()V

    return-void
.end method


# virtual methods
.method public final toSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 62
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/items/unit/SaveUnitState$toSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/items/unit/SaveUnitState$toSnapshot$1;-><init>(Lcom/squareup/items/unit/SaveUnitState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
