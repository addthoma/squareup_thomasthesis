.class final Lcom/squareup/items/unit/ui/UnitUnsavedChangesAlertDialogFactory$create$1$2;
.super Ljava/lang/Object;
.source "UnitUnsavedChangesAlertDialogFactory.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/ui/UnitUnsavedChangesAlertDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/workflow/legacy/Screen;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/ui/UnitUnsavedChangesAlertDialogFactory$create$1$2;->$screen:Lcom/squareup/workflow/legacy/Screen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/items/unit/ui/UnitUnsavedChangesAlertDialogFactory$create$1$2;->$screen:Lcom/squareup/workflow/legacy/Screen;

    const-string v1, "screen"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/workflow/legacy/ScreenKt;->getUnwrapV2Screen(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/unit/UnitUnsavedChangesAlertScreen;

    invoke-virtual {v0}, Lcom/squareup/items/unit/UnitUnsavedChangesAlertScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    sget-object v1, Lcom/squareup/items/unit/UnitUnsavedChangesAlertScreen$Event$Resume;->INSTANCE:Lcom/squareup/items/unit/UnitUnsavedChangesAlertScreen$Event$Resume;

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
