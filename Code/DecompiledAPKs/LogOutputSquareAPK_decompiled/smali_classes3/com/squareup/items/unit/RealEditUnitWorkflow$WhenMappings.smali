.class public final synthetic Lcom/squareup/items/unit/RealEditUnitWorkflow$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 4

    invoke-static {}, Lcom/squareup/connectivity/InternetState;->values()[Lcom/squareup/connectivity/InternetState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/items/unit/RealEditUnitWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/items/unit/RealEditUnitWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    invoke-virtual {v1}, Lcom/squareup/connectivity/InternetState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/items/unit/RealEditUnitWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/connectivity/InternetState;->NOT_CONNECTED:Lcom/squareup/connectivity/InternetState;

    invoke-virtual {v1}, Lcom/squareup/connectivity/InternetState;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/items/unit/SaveUnitAction;->values()[Lcom/squareup/items/unit/SaveUnitAction;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/items/unit/RealEditUnitWorkflow$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/items/unit/RealEditUnitWorkflow$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/items/unit/SaveUnitAction;->UPDATE:Lcom/squareup/items/unit/SaveUnitAction;

    invoke-virtual {v1}, Lcom/squareup/items/unit/SaveUnitAction;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/items/unit/RealEditUnitWorkflow$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/items/unit/SaveUnitAction;->DELETE:Lcom/squareup/items/unit/SaveUnitAction;

    invoke-virtual {v1}, Lcom/squareup/items/unit/SaveUnitAction;->ordinal()I

    move-result v1

    aput v3, v0, v1

    return-void
.end method
