.class public abstract Lcom/squareup/items/unit/EditUnitState;
.super Ljava/lang/Object;
.source "EditUnitState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/unit/EditUnitState$Saving;,
        Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;,
        Lcom/squareup/items/unit/EditUnitState$EditUnit;,
        Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;,
        Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;,
        Lcom/squareup/items/unit/EditUnitState$WarnInternetUnavailable;,
        Lcom/squareup/items/unit/EditUnitState$CheckInternetAvailability;,
        Lcom/squareup/items/unit/EditUnitState$HasOriginalUnit;,
        Lcom/squareup/items/unit/EditUnitState$RequiresInternet;,
        Lcom/squareup/items/unit/EditUnitState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00062\u00020\u0001:\n\u0005\u0006\u0007\u0008\t\n\u000b\u000c\r\u000eB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u0082\u0001\u0007\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/items/unit/EditUnitState;",
        "",
        "()V",
        "toSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "CheckInternetAvailability",
        "Companion",
        "DuplicateCustomUnitName",
        "EditUnit",
        "EditsDiscardAttempted",
        "HasOriginalUnit",
        "RequiresInternet",
        "Saving",
        "StandardUnitsList",
        "WarnInternetUnavailable",
        "Lcom/squareup/items/unit/EditUnitState$Saving;",
        "Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;",
        "Lcom/squareup/items/unit/EditUnitState$EditUnit;",
        "Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;",
        "Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;",
        "Lcom/squareup/items/unit/EditUnitState$WarnInternetUnavailable;",
        "Lcom/squareup/items/unit/EditUnitState$CheckInternetAvailability;",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/unit/EditUnitState$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/items/unit/EditUnitState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/unit/EditUnitState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/unit/EditUnitState;->Companion:Lcom/squareup/items/unit/EditUnitState$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/items/unit/EditUnitState;-><init>()V

    return-void
.end method


# virtual methods
.method public final toSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 86
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/items/unit/EditUnitState$toSnapshot$1;-><init>(Lcom/squareup/items/unit/EditUnitState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
