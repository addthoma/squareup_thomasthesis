.class public final Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;
.super Ljava/lang/Object;
.source "EditOptionValueLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;,
        Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditOptionValueLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditOptionValueLayoutRunner.kt\ncom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner\n*L\n1#1,129:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0018\u0000 \u001b2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u001a\u001bB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018*\u0004\u0018\u00010\u0018H\u0002J\u000c\u0010\u0019\u001a\u00020\u0018*\u00020\u0018H\u0002R\u0016\u0010\u0006\u001a\n \u0008*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\n \u0008*\u0004\u0018\u00010\n0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\n \u0008*\u0004\u0018\u00010\u000c0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\n \u0008*\u0004\u0018\u00010\u000e0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n \u0008*\u0004\u0018\u00010\n0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\n \u0008*\u0004\u0018\u00010\u00110\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "kotlin.jvm.PlatformType",
        "colorField",
        "Lcom/squareup/noho/NohoEditRow;",
        "colorPicker",
        "Lcom/squareup/register/widgets/PickColorGrid;",
        "deleteButton",
        "Lcom/squareup/ui/ConfirmButton;",
        "nameField",
        "nameLabel",
        "Lcom/squareup/noho/NohoLabel;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "toArgb",
        "",
        "toRgbWithHash",
        "ColorIdAndNameId",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$Companion;

.field private static final EditOptionValueLayoutBinding:Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

.field private static final standardColorIdAndNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final colorField:Lcom/squareup/noho/NohoEditRow;

.field private final colorPicker:Lcom/squareup/register/widgets/PickColorGrid;

.field private final deleteButton:Lcom/squareup/ui/ConfirmButton;

.field private final nameField:Lcom/squareup/noho/NohoEditRow;

.field private final nameLabel:Lcom/squareup/noho/NohoLabel;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    new-instance v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->Companion:Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$Companion;

    .line 77
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 78
    const-class v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    .line 79
    sget v4, Lcom/squareup/items/editoption/impl/R$layout;->edit_option_value:I

    .line 80
    sget-object v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$Companion$EditOptionValueLayoutBinding$1;->INSTANCE:Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$Companion$EditOptionValueLayoutBinding$1;

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    .line 77
    invoke-static/range {v2 .. v9}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object v0

    sput-object v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->EditOptionValueLayoutBinding:Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    const/16 v0, 0xe

    new-array v0, v0, [Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    .line 84
    new-instance v1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    sget v2, Lcom/squareup/items/editoption/impl/R$color;->edit_option_value_color_picker_white:I

    .line 85
    sget v3, Lcom/squareup/noho/R$string;->noho_standard_white:I

    .line 84
    invoke-direct {v1, v2, v3}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;-><init>(II)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 86
    new-instance v1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    sget v2, Lcom/squareup/items/editoption/impl/R$color;->edit_option_value_color_picker_black:I

    .line 87
    sget v3, Lcom/squareup/noho/R$string;->noho_standard_black:I

    .line 86
    invoke-direct {v1, v2, v3}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;-><init>(II)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 88
    new-instance v1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    sget v2, Lcom/squareup/items/editoption/impl/R$color;->edit_option_value_color_picker_brown:I

    .line 89
    sget v3, Lcom/squareup/noho/R$string;->noho_standard_brown:I

    .line 88
    invoke-direct {v1, v2, v3}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;-><init>(II)V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 90
    new-instance v1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    sget v2, Lcom/squareup/items/editoption/impl/R$color;->edit_option_value_color_picker_maroon:I

    .line 91
    sget v3, Lcom/squareup/noho/R$string;->noho_standard_maroon:I

    .line 90
    invoke-direct {v1, v2, v3}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;-><init>(II)V

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 92
    new-instance v1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    sget v2, Lcom/squareup/items/editoption/impl/R$color;->edit_option_value_color_picker_orange:I

    .line 93
    sget v3, Lcom/squareup/noho/R$string;->noho_standard_orange:I

    .line 92
    invoke-direct {v1, v2, v3}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;-><init>(II)V

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 94
    new-instance v1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    sget v2, Lcom/squareup/items/editoption/impl/R$color;->edit_option_value_color_picker_gold:I

    .line 95
    sget v3, Lcom/squareup/noho/R$string;->noho_standard_gold:I

    .line 94
    invoke-direct {v1, v2, v3}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;-><init>(II)V

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 96
    new-instance v1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    sget v2, Lcom/squareup/items/editoption/impl/R$color;->edit_option_value_color_picker_lime:I

    .line 97
    sget v3, Lcom/squareup/noho/R$string;->noho_standard_lime:I

    .line 96
    invoke-direct {v1, v2, v3}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;-><init>(II)V

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 98
    new-instance v1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    sget v2, Lcom/squareup/items/editoption/impl/R$color;->edit_option_value_color_picker_green:I

    .line 99
    sget v3, Lcom/squareup/noho/R$string;->noho_standard_green:I

    .line 98
    invoke-direct {v1, v2, v3}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;-><init>(II)V

    const/4 v2, 0x7

    aput-object v1, v0, v2

    .line 100
    new-instance v1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    sget v2, Lcom/squareup/items/editoption/impl/R$color;->edit_option_value_color_picker_teal:I

    .line 101
    sget v3, Lcom/squareup/noho/R$string;->noho_standard_teal:I

    .line 100
    invoke-direct {v1, v2, v3}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;-><init>(II)V

    const/16 v2, 0x8

    aput-object v1, v0, v2

    .line 102
    new-instance v1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    sget v2, Lcom/squareup/items/editoption/impl/R$color;->edit_option_value_color_picker_blue:I

    .line 103
    sget v3, Lcom/squareup/noho/R$string;->noho_standard_blue:I

    .line 102
    invoke-direct {v1, v2, v3}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;-><init>(II)V

    const/16 v2, 0x9

    aput-object v1, v0, v2

    .line 104
    new-instance v1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    sget v2, Lcom/squareup/items/editoption/impl/R$color;->edit_option_value_color_picker_indigo:I

    .line 105
    sget v3, Lcom/squareup/noho/R$string;->noho_standard_indigo:I

    .line 104
    invoke-direct {v1, v2, v3}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;-><init>(II)V

    const/16 v2, 0xa

    aput-object v1, v0, v2

    .line 106
    new-instance v1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    sget v2, Lcom/squareup/items/editoption/impl/R$color;->edit_option_value_color_picker_purple:I

    .line 107
    sget v3, Lcom/squareup/noho/R$string;->noho_standard_purple:I

    .line 106
    invoke-direct {v1, v2, v3}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;-><init>(II)V

    const/16 v2, 0xb

    aput-object v1, v0, v2

    .line 108
    new-instance v1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    sget v2, Lcom/squareup/items/editoption/impl/R$color;->edit_option_value_color_picker_plum:I

    .line 109
    sget v3, Lcom/squareup/noho/R$string;->noho_standard_plum:I

    .line 108
    invoke-direct {v1, v2, v3}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;-><init>(II)V

    const/16 v2, 0xc

    aput-object v1, v0, v2

    .line 110
    new-instance v1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    sget v2, Lcom/squareup/items/editoption/impl/R$color;->edit_option_value_color_picker_pink:I

    .line 111
    sget v3, Lcom/squareup/noho/R$string;->noho_standard_pink:I

    .line 110
    invoke-direct {v1, v2, v3}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;-><init>(II)V

    const/16 v2, 0xd

    aput-object v1, v0, v2

    .line 83
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->standardColorIdAndNames:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->view:Landroid/view/View;

    .line 28
    iget-object p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    iput-object p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 29
    iget-object p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/items/editoption/impl/R$id;->edit_option_value_name_label:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->nameLabel:Lcom/squareup/noho/NohoLabel;

    .line 30
    iget-object p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/items/editoption/impl/R$id;->edit_option_value_name_field:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow;

    iput-object p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->nameField:Lcom/squareup/noho/NohoEditRow;

    .line 31
    iget-object p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/items/editoption/impl/R$id;->edit_option_value_color_field:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow;

    iput-object p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->colorField:Lcom/squareup/noho/NohoEditRow;

    .line 32
    iget-object p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/items/editoption/impl/R$id;->edit_option_value_color_grid:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/PickColorGrid;

    iput-object p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->colorPicker:Lcom/squareup/register/widgets/PickColorGrid;

    .line 33
    iget-object p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/items/editoption/impl/R$id;->edit_option_value_delete_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ConfirmButton;

    iput-object p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->deleteButton:Lcom/squareup/ui/ConfirmButton;

    return-void
.end method

.method public static final synthetic access$getEditOptionValueLayoutBinding$cp()Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;
    .locals 1

    .line 26
    sget-object v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->EditOptionValueLayoutBinding:Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    return-object v0
.end method

.method public static final synthetic access$toRgbWithHash(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->toRgbWithHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private final toArgb(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v1, "#"

    .line 116
    move-object v2, v1

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {p1, v2}, Lkotlin/text/StringsKt;->removePrefix(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/util/Colors;->isColor(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ff"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {p1, v1}, Lkotlin/text/StringsKt;->removePrefix(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    move-object v0, p1

    :cond_0
    return-object v0
.end method

.method private final toRgbWithHash(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "ff"

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {p1, v1}, Lkotlin/text/StringsKt;->removePrefix(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public showRendering(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 8

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;->getColorText()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/workflow/text/WorkflowEditableText;->getText()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->toArgb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 43
    iget-object v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 40
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 41
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/items/editoption/impl/R$string;->edit_option_value_screen_title:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 42
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$showRendering$1;

    invoke-direct {v3, p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$showRendering$1;-><init>(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 43
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->nameLabel:Lcom/squareup/noho/NohoLabel;

    const-string v1, "nameLabel"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;->getNameFieldTitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->nameField:Lcom/squareup/noho/NohoEditRow;

    const-string v1, "nameField"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;->getNameText()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->colorField:Lcom/squareup/noho/NohoEditRow;

    const-string v1, "colorField"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;->getColorText()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->colorPicker:Lcom/squareup/register/widgets/PickColorGrid;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/PickColorGrid;->setOnColorChangeListener(Lcom/squareup/register/widgets/PickColorGrid$OnColorChangeListener;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->colorPicker:Lcom/squareup/register/widgets/PickColorGrid;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/PickColorGrid;->clearColors()V

    if-eqz p2, :cond_0

    .line 54
    invoke-static {p2}, Lcom/squareup/util/Colors;->parseHex(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 55
    :cond_0
    sget-object v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->standardColorIdAndNames:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    .line 56
    iget-object v5, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v4}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->getColorResId()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 57
    iget-object v6, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->colorPicker:Lcom/squareup/register/widgets/PickColorGrid;

    iget-object v7, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v4}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->getColorNameStringId()I

    move-result v4

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v5, v4}, Lcom/squareup/register/widgets/PickColorGrid;->addColor(ILjava/lang/String;)V

    if-nez v1, :cond_2

    goto :goto_0

    .line 58
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v5, v4, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    const-string v0, "colorPicker"

    if-eqz v3, :cond_4

    .line 63
    iget-object v1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->colorPicker:Lcom/squareup/register/widgets/PickColorGrid;

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Lcom/squareup/register/widgets/PickColorGrid;->setSelectedColor(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    if-eqz v1, :cond_5

    .line 65
    iget-object v3, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->colorPicker:Lcom/squareup/register/widgets/PickColorGrid;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const-string v4, ""

    invoke-virtual {v3, v1, v4}, Lcom/squareup/register/widgets/PickColorGrid;->addColor(ILjava/lang/String;)V

    .line 66
    iget-object v1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->colorPicker:Lcom/squareup/register/widgets/PickColorGrid;

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Lcom/squareup/register/widgets/PickColorGrid;->setSelectedColor(Ljava/lang/String;)V

    .line 68
    :cond_5
    :goto_1
    iget-object p2, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->colorPicker:Lcom/squareup/register/widgets/PickColorGrid;

    new-instance v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$showRendering$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$showRendering$2;-><init>(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;)V

    check-cast v0, Lcom/squareup/register/widgets/PickColorGrid$OnColorChangeListener;

    invoke-virtual {p2, v0}, Lcom/squareup/register/widgets/PickColorGrid;->setOnColorChangeListener(Lcom/squareup/register/widgets/PickColorGrid$OnColorChangeListener;)V

    .line 70
    iget-object p2, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->deleteButton:Lcom/squareup/ui/ConfirmButton;

    new-instance v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$showRendering$3;

    invoke-direct {v0, p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$showRendering$3;-><init>(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;)V

    check-cast v0, Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    .line 71
    iget-object p2, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->deleteButton:Lcom/squareup/ui/ConfirmButton;

    const-string v0, "deleteButton"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;->getShouldShowDeleteButton()Z

    move-result v0

    if-eqz v0, :cond_6

    goto :goto_2

    :cond_6
    const/16 v2, 0x8

    :goto_2
    invoke-virtual {p2, v2}, Lcom/squareup/ui/ConfirmButton;->setVisibility(I)V

    .line 73
    iget-object p2, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$showRendering$4;

    invoke-direct {v0, p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$showRendering$4;-><init>(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->showRendering(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
