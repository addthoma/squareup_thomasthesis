.class public final Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;
.super Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;
.source "RealEditOptionWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UpdateName"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0003H\u00d6\u0001J\u0018\u0010\u0010\u001a\u00020\u0011*\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u0012H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;",
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;",
        "name",
        "",
        "(Ljava/lang/String;)V",
        "getName",
        "()Ljava/lang/String;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/editoption/EditOptionState;",
        "Lcom/squareup/items/editoption/EditOptionOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 253
    invoke-direct {p0, v0}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;->name:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;->name:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;->copy(Ljava/lang/String;)Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/editoption/EditOptionState;",
            "-",
            "Lcom/squareup/items/editoption/EditOptionOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 255
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/editoption/EditOptionState;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/items/editoption/EditOptionState;

    invoke-virtual {v1}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v2

    .line 256
    iget-object v4, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;->name:Ljava/lang/String;

    .line 257
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/items/editoption/EditOptionState;

    invoke-virtual {v1}, Lcom/squareup/items/editoption/EditOptionState;->isDisplayNameTrackingName()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 258
    iget-object v1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;->name:Ljava/lang/String;

    goto :goto_0

    .line 260
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/items/editoption/EditOptionState;

    invoke-virtual {v1}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    :goto_0
    move-object v5, v1

    const/4 v6, 0x0

    const/16 v7, 0x9

    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 255
    invoke-static/range {v2 .. v8}, Lcom/squareup/cogs/itemoptions/ItemOption;->copy$default(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/items/editoption/EditOptionState;->updateOption(Lcom/squareup/cogs/itemoptions/ItemOption;)Lcom/squareup/items/editoption/EditOptionState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;)Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;

    invoke-direct {v0, p1}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;->name:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .line 252
    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;->name:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UpdateName(name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
