.class abstract Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;
.super Ljava/lang/Object;
.source "EditOptionLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/editoption/EditOptionLayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "EditOptionRow"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$OptionNameInputRow;,
        Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$OptionDisplayNameInputRow;,
        Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$OptionValueSectionHeaderRow;,
        Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;,
        Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$OptionValueNumberLimitHelpTextRow;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u00020\u0001:\u0005\u0007\u0008\t\n\u000bB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0005\u000c\r\u000e\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;",
        "",
        "stableId",
        "",
        "(Ljava/lang/String;)V",
        "getStableId",
        "()Ljava/lang/String;",
        "AddedOptionValueRow",
        "OptionDisplayNameInputRow",
        "OptionNameInputRow",
        "OptionValueNumberLimitHelpTextRow",
        "OptionValueSectionHeaderRow",
        "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$OptionNameInputRow;",
        "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$OptionDisplayNameInputRow;",
        "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$OptionValueSectionHeaderRow;",
        "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;",
        "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$OptionValueNumberLimitHelpTextRow;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final stableId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;->stableId:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 277
    invoke-direct {p0, p1}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getStableId()Ljava/lang/String;
    .locals 1

    .line 277
    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;->stableId:Ljava/lang/String;

    return-object v0
.end method
