.class public final Lcom/squareup/items/editoption/EditOptionEventLogger;
.super Ljava/lang/Object;
.source "EditOptionEventLogger.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/items/editoption/EditOptionEventLogger;",
        "",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/analytics/Analytics;)V",
        "logClickOnLearnMoreAboutItemOptionsLink",
        "",
        "merchantToken",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/editoption/EditOptionEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public final logClickOnLearnMoreAboutItemOptionsLink(Ljava/lang/String;)V
    .locals 2

    const-string v0, "merchantToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    sget-object v0, Lcom/squareup/catalog/event/CatalogFeature;->OPTION_SET_EMPTY_PAGE_LEARN_MORE_LINK_CLICKED:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v1, p0, Lcom/squareup/items/editoption/EditOptionEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method
