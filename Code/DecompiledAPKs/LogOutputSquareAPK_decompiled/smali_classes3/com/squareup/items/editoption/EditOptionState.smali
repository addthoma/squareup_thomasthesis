.class public abstract Lcom/squareup/items/editoption/EditOptionState;
.super Ljava/lang/Object;
.source "EditOptionState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/editoption/EditOptionState$EditOption;,
        Lcom/squareup/items/editoption/EditOptionState$InvalidOption;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u00086\u0018\u00002\u00020\u0001:\u0002\u001a\u001bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u000e\u001a\u00020\u000fJ&\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008H\u0002J\u000e\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014J\u0006\u0010\u0015\u001a\u00020\u0000J\u000e\u0010\u0016\u001a\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u000bJ\u0016\u0010\u0018\u001a\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u0004R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\tR\u0012\u0010\n\u001a\u00020\u000bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\r\u0082\u0001\u0002\u000f\u0012\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/items/editoption/EditOptionState;",
        "Landroid/os/Parcelable;",
        "()V",
        "addOptionRowId",
        "",
        "getAddOptionRowId",
        "()Ljava/lang/String;",
        "isDisplayNameTrackingName",
        "",
        "()Z",
        "option",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "getOption",
        "()Lcom/squareup/cogs/itemoptions/ItemOption;",
        "backToEditOption",
        "Lcom/squareup/items/editoption/EditOptionState$EditOption;",
        "copy",
        "invalidFor",
        "Lcom/squareup/items/editoption/EditOptionState$InvalidOption;",
        "reason",
        "Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;",
        "stopTrackingDisplayName",
        "updateOption",
        "updatedOption",
        "updateOptionWithNewValue",
        "newValueId",
        "EditOption",
        "InvalidOption",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/squareup/items/editoption/EditOptionState;-><init>()V

    return-void
.end method

.method private final copy(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Z)Lcom/squareup/items/editoption/EditOptionState;
    .locals 2

    .line 50
    instance-of v0, p0, Lcom/squareup/items/editoption/EditOptionState$EditOption;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/items/editoption/EditOptionState$EditOption;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/items/editoption/EditOptionState$EditOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Z)V

    check-cast v0, Lcom/squareup/items/editoption/EditOptionState;

    goto :goto_0

    .line 51
    :cond_0
    instance-of v0, p0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;

    move-object v1, p0

    check-cast v1, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;

    invoke-virtual {v1}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;->getReason()Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;

    move-result-object v1

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;ZLcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;)V

    check-cast v0, Lcom/squareup/items/editoption/EditOptionState;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method static synthetic copy$default(Lcom/squareup/items/editoption/EditOptionState;Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/items/editoption/EditOptionState;
    .locals 0

    if-nez p5, :cond_3

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    .line 45
    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    .line 46
    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState;->getAddOptionRowId()Ljava/lang/String;

    move-result-object p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    .line 47
    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState;->isDisplayNameTrackingName()Z

    move-result p3

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/items/editoption/EditOptionState;->copy(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Z)Lcom/squareup/items/editoption/EditOptionState;

    move-result-object p0

    return-object p0

    .line 0
    :cond_3
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: copy"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public final backToEditOption()Lcom/squareup/items/editoption/EditOptionState$EditOption;
    .locals 4

    .line 38
    new-instance v0, Lcom/squareup/items/editoption/EditOptionState$EditOption;

    .line 39
    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v1

    .line 40
    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState;->getAddOptionRowId()Ljava/lang/String;

    move-result-object v2

    .line 41
    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState;->isDisplayNameTrackingName()Z

    move-result v3

    .line 38
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/items/editoption/EditOptionState$EditOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public abstract getAddOptionRowId()Ljava/lang/String;
.end method

.method public abstract getOption()Lcom/squareup/cogs/itemoptions/ItemOption;
.end method

.method public final invalidFor(Lcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;)Lcom/squareup/items/editoption/EditOptionState$InvalidOption;
    .locals 4

    const-string v0, "reason"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    new-instance v0, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;

    .line 32
    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState;->getAddOptionRowId()Ljava/lang/String;

    move-result-object v2

    .line 34
    invoke-virtual {p0}, Lcom/squareup/items/editoption/EditOptionState;->isDisplayNameTrackingName()Z

    move-result v3

    .line 31
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/items/editoption/EditOptionState$InvalidOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;ZLcom/squareup/items/editoption/EditOptionState$InvalidOption$InvalidOptionReason;)V

    return-object v0
.end method

.method public abstract isDisplayNameTrackingName()Z
.end method

.method public final stopTrackingDisplayName()Lcom/squareup/items/editoption/EditOptionState;
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x0

    move-object v0, p0

    .line 18
    invoke-static/range {v0 .. v5}, Lcom/squareup/items/editoption/EditOptionState;->copy$default(Lcom/squareup/items/editoption/EditOptionState;Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/items/editoption/EditOptionState;

    move-result-object v0

    return-object v0
.end method

.method public final updateOption(Lcom/squareup/cogs/itemoptions/ItemOption;)Lcom/squareup/items/editoption/EditOptionState;
    .locals 7

    const-string/jumbo v0, "updatedOption"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    .line 14
    invoke-static/range {v1 .. v6}, Lcom/squareup/items/editoption/EditOptionState;->copy$default(Lcom/squareup/items/editoption/EditOptionState;Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/items/editoption/EditOptionState;

    move-result-object p1

    return-object p1
.end method

.method public final updateOptionWithNewValue(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;)Lcom/squareup/items/editoption/EditOptionState;
    .locals 7

    const-string/jumbo v0, "updatedOption"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newValueId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    .line 25
    invoke-static/range {v1 .. v6}, Lcom/squareup/items/editoption/EditOptionState;->copy$default(Lcom/squareup/items/editoption/EditOptionState;Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/items/editoption/EditOptionState;

    move-result-object p1

    return-object p1
.end method
