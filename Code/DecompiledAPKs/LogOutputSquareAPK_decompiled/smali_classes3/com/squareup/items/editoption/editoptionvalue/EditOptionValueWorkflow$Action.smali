.class public abstract Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action;
.super Ljava/lang/Object;
.source "EditOptionValueWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$SaveOptionValue;,
        Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$DeleteOptionValue;,
        Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;,
        Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueColor;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00080\u0018\u00002\u0012\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0004\u0006\u0007\u0008\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0005\u0082\u0001\u0004\n\u000b\u000c\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueState;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput;",
        "()V",
        "ChangeOptionValueColor",
        "ChangeOptionValueName",
        "DeleteOptionValue",
        "SaveOptionValue",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$SaveOptionValue;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$DeleteOptionValue;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueColor;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;",
            ">;)",
            "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput;

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 61
    invoke-virtual {p0, p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;",
            "-",
            "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
