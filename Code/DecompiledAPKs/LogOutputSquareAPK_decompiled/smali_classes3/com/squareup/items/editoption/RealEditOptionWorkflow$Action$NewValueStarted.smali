.class public final Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;
.super Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;
.source "RealEditOptionWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NewValueStarted"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEditOptionWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEditOptionWorkflow.kt\ncom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,355:1\n1360#2:356\n1429#2,3:357\n*E\n*S KotlinDebug\n*F\n+ 1 RealEditOptionWorkflow.kt\ncom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted\n*L\n291#1:356\n291#1,3:357\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\'\u0010\u000e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0003H\u00d6\u0001J\u0018\u0010\u0016\u001a\u00020\u0017*\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a0\u0018H\u0016R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0008R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0008\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;",
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;",
        "newValueName",
        "",
        "newValueId",
        "nextId",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
        "getNewValueId",
        "()Ljava/lang/String;",
        "getNewValueName",
        "getNextId",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/editoption/EditOptionState;",
        "Lcom/squareup/items/editoption/EditOptionOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final newValueId:Ljava/lang/String;

.field private final newValueName:Ljava/lang/String;

.field private final nextId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "newValueName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newValueId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextId"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 289
    invoke-direct {p0, v0}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->nextId:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueName:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueId:Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->nextId:Ljava/lang/String;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/editoption/EditOptionState;",
            "-",
            "Lcom/squareup/items/editoption/EditOptionOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 291
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/editoption/EditOptionState;

    invoke-virtual {v0}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 356
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 357
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 358
    check-cast v2, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 291
    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getValueId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 359
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 291
    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueId:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 293
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/editoption/EditOptionState;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/items/editoption/EditOptionState;

    invoke-virtual {v1}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueId:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueName:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/squareup/items/editoption/RealEditOptionWorkflowKt;->access$updateValueName(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/items/editoption/EditOptionState;->updateOption(Lcom/squareup/cogs/itemoptions/ItemOption;)Lcom/squareup/items/editoption/EditOptionState;

    move-result-object v0

    goto :goto_1

    .line 295
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/editoption/EditOptionState;

    .line 296
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/items/editoption/EditOptionState;

    invoke-virtual {v1}, Lcom/squareup/items/editoption/EditOptionState;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueId:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueName:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/squareup/items/editoption/RealEditOptionWorkflowKt;->access$addNewValue(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v1

    .line 297
    iget-object v2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->nextId:Ljava/lang/String;

    .line 295
    invoke-virtual {v0, v1, v2}, Lcom/squareup/items/editoption/EditOptionState;->updateOptionWithNewValue(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;)Lcom/squareup/items/editoption/EditOptionState;

    move-result-object v0

    .line 292
    :goto_1
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueName:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueId:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->nextId:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;
    .locals 1

    const-string v0, "newValueName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newValueId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextId"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->nextId:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->nextId:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getNewValueId()Ljava/lang/String;
    .locals 1

    .line 287
    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueId:Ljava/lang/String;

    return-object v0
.end method

.method public final getNewValueName()Ljava/lang/String;
    .locals 1

    .line 286
    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueName:Ljava/lang/String;

    return-object v0
.end method

.method public final getNextId()Ljava/lang/String;
    .locals 1

    .line 288
    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->nextId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueId:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->nextId:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NewValueStarted(newValueName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", newValueId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->newValueId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", nextId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;->nextId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
