.class public interface abstract Lcom/squareup/items/editoption/EditOptionViewFactory;
.super Ljava/lang/Object;
.source "EditOptionViewFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001\u00a8\u0006\u0002"
    }
    d2 = {
        "Lcom/squareup/items/editoption/EditOptionViewFactory;",
        "Lcom/squareup/workflow/WorkflowViewFactory;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
