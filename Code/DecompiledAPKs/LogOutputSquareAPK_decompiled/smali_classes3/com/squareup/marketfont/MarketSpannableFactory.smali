.class public final Lcom/squareup/marketfont/MarketSpannableFactory;
.super Landroid/text/Spannable$Factory;
.source "MarketSpannableFactory.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private final weight:Lcom/squareup/marketfont/MarketFont$Weight;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Landroid/text/Spannable$Factory;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/marketfont/MarketSpannableFactory;->context:Landroid/content/Context;

    .line 16
    iput-object p2, p0, Lcom/squareup/marketfont/MarketSpannableFactory;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    return-void
.end method


# virtual methods
.method public newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;
    .locals 10

    .line 20
    instance-of v0, p1, Landroid/text/Spannable;

    if-eqz v0, :cond_1

    .line 21
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 23
    check-cast p1, Landroid/text/Spannable;

    .line 25
    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v1

    const-class v2, Landroid/text/style/StyleSpan;

    const/4 v3, 0x0

    invoke-interface {p1, v3, v1, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/StyleSpan;

    .line 26
    array-length v2, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_0

    aget-object v5, v1, v4

    .line 27
    invoke-interface {p1, v5}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    .line 28
    invoke-interface {p1, v5}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    .line 31
    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 32
    iget-object v8, p0, Lcom/squareup/marketfont/MarketSpannableFactory;->context:Landroid/content/Context;

    iget-object v9, p0, Lcom/squareup/marketfont/MarketSpannableFactory;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v5}, Landroid/text/style/StyleSpan;->getStyle()I

    move-result v5

    invoke-static {v8, v9, v5}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;I)Lcom/squareup/fonts/FontSpan;

    move-result-object v5

    invoke-virtual {v0, v5, v6, v7, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    return-object v0

    .line 36
    :cond_1
    invoke-super {p0, p1}, Landroid/text/Spannable$Factory;->newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object p1

    return-object p1
.end method
