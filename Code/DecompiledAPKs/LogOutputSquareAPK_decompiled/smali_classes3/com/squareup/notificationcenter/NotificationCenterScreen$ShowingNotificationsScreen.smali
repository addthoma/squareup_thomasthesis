.class public final Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;
.super Lcom/squareup/notificationcenter/NotificationCenterScreen;
.source "NotificationCenterScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenter/NotificationCenterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowingNotificationsScreen"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0019\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 ,2\u00020\u0001:\u0001,Bg\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000b0\n\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\r\u0012\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\r\u0012\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0002\u0010\u0010J\u000f\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0008H\u00c6\u0003J\u0015\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000b0\nH\u00c6\u0003J\u000f\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\rH\u00c6\u0003J\u000f\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\rH\u00c6\u0003J\u0015\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000b0\nH\u00c6\u0003Jy\u0010$\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0014\u0008\u0002\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000b0\n2\u000e\u0008\u0002\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\r2\u000e\u0008\u0002\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\r2\u0014\u0008\u0002\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000b0\nH\u00c6\u0001J\u0013\u0010%\u001a\u00020\u00082\u0008\u0010&\u001a\u0004\u0018\u00010\'H\u00d6\u0003J\t\u0010(\u001a\u00020)H\u00d6\u0001J\t\u0010*\u001a\u00020+H\u00d6\u0001R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0017\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0017\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0014R\u001d\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u001d\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0017R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001c\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;",
        "Lcom/squareup/notificationcenter/NotificationCenterScreen;",
        "notifications",
        "",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "selectedTab",
        "Lcom/squareup/notificationcenter/NotificationCenterTab;",
        "showError",
        "",
        "onNotificationClicked",
        "Lkotlin/Function1;",
        "",
        "onClosePressed",
        "Lkotlin/Function0;",
        "onBackPressed",
        "onTabSelected",
        "(Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V",
        "getNotifications",
        "()Ljava/util/List;",
        "getOnBackPressed",
        "()Lkotlin/jvm/functions/Function0;",
        "getOnClosePressed",
        "getOnNotificationClicked",
        "()Lkotlin/jvm/functions/Function1;",
        "getOnTabSelected",
        "getSelectedTab",
        "()Lcom/squareup/notificationcenter/NotificationCenterTab;",
        "getShowError",
        "()Z",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# instance fields
.field private final notifications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation
.end field

.field private final onBackPressed:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onClosePressed:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onNotificationClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onTabSelected:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

.field private final showError:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->Companion:Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen$Companion;

    .line 31
    const-class v0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            "Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/notificationcenterdata/Notification;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "notifications"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedTab"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onNotificationClicked"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClosePressed"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBackPressed"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onTabSelected"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, v0}, Lcom/squareup/notificationcenter/NotificationCenterScreen;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->notifications:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    iput-boolean p3, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->showError:Z

    iput-object p4, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onNotificationClicked:Lkotlin/jvm/functions/Function1;

    iput-object p5, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onClosePressed:Lkotlin/jvm/functions/Function0;

    iput-object p6, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onBackPressed:Lkotlin/jvm/functions/Function0;

    iput-object p7, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onTabSelected:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->notifications:Ljava/util/List;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->showError:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onNotificationClicked:Lkotlin/jvm/functions/Function1;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onClosePressed:Lkotlin/jvm/functions/Function0;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onBackPressed:Lkotlin/jvm/functions/Function0;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onTabSelected:Lkotlin/jvm/functions/Function1;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->copy(Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->notifications:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Lcom/squareup/notificationcenter/NotificationCenterTab;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->showError:Z

    return v0
.end method

.method public final component4()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onNotificationClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component5()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onClosePressed:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component6()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onBackPressed:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component7()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onTabSelected:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            "Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/notificationcenterdata/Notification;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;"
        }
    .end annotation

    const-string v0, "notifications"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedTab"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onNotificationClicked"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClosePressed"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBackPressed"

    move-object v7, p6

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onTabSelected"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;

    move-object v1, v0

    move v4, p3

    invoke-direct/range {v1 .. v8}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;-><init>(Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->notifications:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->notifications:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    iget-object v1, p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->showError:Z

    iget-boolean v1, p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->showError:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onNotificationClicked:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onNotificationClicked:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onClosePressed:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onClosePressed:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onBackPressed:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onBackPressed:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onTabSelected:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onTabSelected:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getNotifications()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->notifications:Ljava/util/List;

    return-object v0
.end method

.method public final getOnBackPressed()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onBackPressed:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnClosePressed()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onClosePressed:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnNotificationClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onNotificationClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnTabSelected()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onTabSelected:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getSelectedTab()Lcom/squareup/notificationcenter/NotificationCenterTab;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    return-object v0
.end method

.method public final getShowError()Z
    .locals 1

    .line 24
    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->showError:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->notifications:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->showError:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onNotificationClicked:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onClosePressed:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onBackPressed:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onTabSelected:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShowingNotificationsScreen(notifications="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->notifications:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedTab="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->showError:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", onNotificationClicked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onNotificationClicked:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onClosePressed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onClosePressed:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onBackPressed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onBackPressed:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onTabSelected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->onTabSelected:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
