.class public final Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;
.super Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;
.source "RealNotificationCenterApplet.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B7\u0008\u0001\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u000e\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0015H\u0016J\u0018\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u001c2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0014J\u0008\u0010 \u001a\u00020\u000fH\u0016J\u0010\u0010!\u001a\u00020\u000f2\u0006\u0010\"\u001a\u00020#H\u0016J\u000e\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0015H\u0016R\u0014\u0010\u000e\u001a\u00020\u000fX\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u00020\u0016X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;",
        "Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;",
        "container",
        "Ldagger/Lazy;",
        "Lcom/squareup/ui/main/PosContainer;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "notificationCenterBadge",
        "Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;",
        "notificationCenterScreens",
        "Lcom/squareup/notificationcenter/NotificationCenterScreens;",
        "(Ldagger/Lazy;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;Lcom/squareup/notificationcenter/NotificationCenterScreens;)V",
        "analyticsName",
        "",
        "getAnalyticsName",
        "()Ljava/lang/String;",
        "getFeatures",
        "()Lcom/squareup/settings/server/Features;",
        "isEnabled",
        "Lio/reactivex/Observable;",
        "",
        "isNotificationCenterApplet",
        "()Z",
        "badge",
        "Lcom/squareup/applet/Applet$Badge;",
        "getHomeScreens",
        "",
        "Lcom/squareup/container/ContainerTreeKey;",
        "currentHistory",
        "Lflow/History;",
        "getIntentScreenExtra",
        "getText",
        "resources",
        "Landroid/content/res/Resources;",
        "visibility",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analyticsName:Ljava/lang/String;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final isEnabled:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final isNotificationCenterApplet:Z

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final notificationCenterBadge:Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;

.field private final notificationCenterScreens:Lcom/squareup/notificationcenter/NotificationCenterScreens;


# direct methods
.method public constructor <init>(Ldagger/Lazy;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;Lcom/squareup/notificationcenter/NotificationCenterScreens;)V
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;",
            "Lcom/squareup/notificationcenter/NotificationCenterScreens;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationCenterBadge"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationCenterScreens"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/notificationcenter/applet/NotificationCenterApplet;-><init>(Ldagger/Lazy;)V

    iput-object p2, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;->features:Lcom/squareup/settings/server/Features;

    iput-object p3, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p4, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;->notificationCenterBadge:Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;

    iput-object p5, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;->notificationCenterScreens:Lcom/squareup/notificationcenter/NotificationCenterScreens;

    .line 39
    iget-object p1, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;->features:Lcom/squareup/settings/server/Features;

    sget-object p2, Lcom/squareup/settings/server/Features$Feature;->NOTIFICATION_CENTER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, p2}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "features.featureEnabled(NOTIFICATION_CENTER)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;->isEnabled:Lio/reactivex/Observable;

    const-string p1, "notification-center"

    .line 41
    iput-object p1, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;->analyticsName:Ljava/lang/String;

    const/4 p1, 0x1

    .line 43
    iput-boolean p1, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;->isNotificationCenterApplet:Z

    return-void
.end method

.method public static final synthetic access$getNotificationCenterBadge$p(Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;)Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;->notificationCenterBadge:Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;

    return-object p0
.end method


# virtual methods
.method public badge()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/applet/Applet$Badge;",
            ">;"
        }
    .end annotation

    .line 77
    iget-object v0, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;->isEnabled:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet$badge$1;

    invoke-direct {v1, p0}, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet$badge$1;-><init>(Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "isEnabled.switchMap { no\u2026      }\n          }\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getAnalyticsName()Ljava/lang/String;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;->analyticsName:Ljava/lang/String;

    return-object v0
.end method

.method public final getFeatures()Lcom/squareup/settings/server/Features;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;->features:Lcom/squareup/settings/server/Features;

    return-object v0
.end method

.method protected getHomeScreens(Lflow/History;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 56
    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lflow/History;->emptyBuilder()Lflow/History$Builder;

    move-result-object p1

    :goto_0
    const-string v0, "builder"

    .line 58
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lflow/History$Builder;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 59
    invoke-virtual {p1}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object v0

    .line 60
    instance-of v1, v0, Lcom/squareup/container/WorkflowTreeKey;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;->notificationCenterScreens:Lcom/squareup/notificationcenter/NotificationCenterScreens;

    .line 61
    check-cast v0, Lcom/squareup/container/WorkflowTreeKey;

    iget-object v0, v0, Lcom/squareup/container/WorkflowTreeKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 60
    invoke-interface {v1, v0}, Lcom/squareup/notificationcenter/NotificationCenterScreens;->screenInListOfScreens(Lcom/squareup/workflow/legacy/Screen$Key;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    .line 66
    :cond_1
    invoke-virtual {p1}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    goto :goto_0

    .line 69
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lflow/History$Builder;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 70
    new-instance p1, Lcom/squareup/notificationcenterlauncher/NotificationCenterBootstrapScreen;

    sget-object v0, Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope;->INSTANCE:Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope;

    check-cast v0, Lcom/squareup/ui/main/InMainActivityScope;

    invoke-direct {p1, v0}, Lcom/squareup/notificationcenterlauncher/NotificationCenterBootstrapScreen;-><init>(Lcom/squareup/ui/main/InMainActivityScope;)V

    .line 69
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_2

    .line 71
    :cond_3
    invoke-static {p1}, Lcom/squareup/container/Histories;->toList(Lflow/History$Builder;)Ljava/util/List;

    move-result-object p1

    :goto_2
    return-object p1
.end method

.method public getIntentScreenExtra()Ljava/lang/String;
    .locals 1

    const-string v0, "NOTIFICATION_CENTER"

    return-object v0
.end method

.method public getText(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    sget v0, Lcom/squareup/notificationcenter/applet/R$string;->notification_center_applet_name:I

    .line 46
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(\n   \u2026_center_applet_name\n    )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public isNotificationCenterApplet()Z
    .locals 1

    .line 43
    iget-boolean v0, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;->isNotificationCenterApplet:Z

    return v0
.end method

.method public visibility()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;->isEnabled:Lio/reactivex/Observable;

    iget-object v1, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "isEnabled.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
