.class public final Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;
.super Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action;
.source "RealNotificationCenterWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoadNotifications"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B9\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u000f\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u000f\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\tH\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u000bH\u00c6\u0003JG\u0010\u001b\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u000e\u0008\u0002\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000bH\u00c6\u0001J\u0013\u0010\u001c\u001a\u00020\t2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u00d6\u0003J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001J\t\u0010!\u001a\u00020\"H\u00d6\u0001J\u0014\u0010#\u001a\u0004\u0018\u00010$*\u0008\u0012\u0004\u0012\u00020&0%H\u0016R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;",
        "Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action;",
        "importantNotifications",
        "",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "generalNotifications",
        "selectedTab",
        "Lcom/squareup/notificationcenter/NotificationCenterTab;",
        "hasError",
        "",
        "analytics",
        "Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
        "(Ljava/util/List;Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;ZLcom/squareup/notificationcenter/NotificationCenterAnalytics;)V",
        "getAnalytics",
        "()Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
        "getGeneralNotifications",
        "()Ljava/util/List;",
        "getHasError",
        "()Z",
        "getImportantNotifications",
        "getSelectedTab",
        "()Lcom/squareup/notificationcenter/NotificationCenterTab;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "Lcom/squareup/notificationcenter/NotificationCenterOutput;",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/notificationcenter/NotificationCenterState;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

.field private final generalNotifications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation
.end field

.field private final hasError:Z

.field private final importantNotifications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;ZLcom/squareup/notificationcenter/NotificationCenterAnalytics;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            "Z",
            "Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
            ")V"
        }
    .end annotation

    const-string v0, "importantNotifications"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "generalNotifications"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedTab"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 267
    invoke-direct {p0, v0}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->importantNotifications:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->generalNotifications:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    iput-boolean p4, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->hasError:Z

    iput-object p5, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;Ljava/util/List;Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;ZLcom/squareup/notificationcenter/NotificationCenterAnalytics;ILjava/lang/Object;)Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->importantNotifications:Ljava/util/List;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->generalNotifications:Ljava/util/List;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->hasError:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->copy(Ljava/util/List;Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;ZLcom/squareup/notificationcenter/NotificationCenterAnalytics;)Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/notificationcenter/NotificationCenterOutput;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/notificationcenter/NotificationCenterState;",
            ">;)",
            "Lcom/squareup/notificationcenter/NotificationCenterOutput;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/notificationcenter/NotificationCenterState;

    .line 271
    instance-of v1, v0, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    if-eqz v1, :cond_0

    .line 272
    new-instance v1, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    .line 273
    iget-object v3, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->importantNotifications:Ljava/util/List;

    .line 274
    iget-object v4, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->generalNotifications:Ljava/util/List;

    .line 275
    iget-object v5, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    .line 276
    iget-boolean v6, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->hasError:Z

    .line 277
    check-cast v0, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    invoke-virtual {v0}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;->getSelectedExternalNotification()Lcom/squareup/notificationcenterdata/Notification;

    move-result-object v7

    move-object v2, v1

    .line 272
    invoke-direct/range {v2 .. v7}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;-><init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;ZLcom/squareup/notificationcenterdata/Notification;)V

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 280
    :cond_0
    new-instance v0, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    .line 281
    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->importantNotifications:Ljava/util/List;

    .line 282
    iget-object v2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->generalNotifications:Ljava/util/List;

    .line 283
    iget-object v3, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    .line 284
    iget-boolean v4, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->hasError:Z

    .line 280
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;-><init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 261
    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/notificationcenter/NotificationCenterOutput;

    move-result-object p1

    return-object p1
.end method

.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->importantNotifications:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->generalNotifications:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Lcom/squareup/notificationcenter/NotificationCenterTab;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->hasError:Z

    return v0
.end method

.method public final component5()Lcom/squareup/notificationcenter/NotificationCenterAnalytics;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    return-object v0
.end method

.method public final copy(Ljava/util/List;Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;ZLcom/squareup/notificationcenter/NotificationCenterAnalytics;)Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            "Z",
            "Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
            ")",
            "Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;"
        }
    .end annotation

    const-string v0, "importantNotifications"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "generalNotifications"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedTab"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;-><init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;ZLcom/squareup/notificationcenter/NotificationCenterAnalytics;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->importantNotifications:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->importantNotifications:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->generalNotifications:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->generalNotifications:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    iget-object v1, p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->hasError:Z

    iget-boolean v1, p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->hasError:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    iget-object p1, p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAnalytics()Lcom/squareup/notificationcenter/NotificationCenterAnalytics;
    .locals 1

    .line 266
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    return-object v0
.end method

.method public final getGeneralNotifications()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation

    .line 263
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->generalNotifications:Ljava/util/List;

    return-object v0
.end method

.method public final getHasError()Z
    .locals 1

    .line 265
    iget-boolean v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->hasError:Z

    return v0
.end method

.method public final getImportantNotifications()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation

    .line 262
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->importantNotifications:Ljava/util/List;

    return-object v0
.end method

.method public final getSelectedTab()Lcom/squareup/notificationcenter/NotificationCenterTab;
    .locals 1

    .line 264
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->importantNotifications:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->generalNotifications:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->hasError:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LoadNotifications(importantNotifications="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->importantNotifications:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", generalNotifications="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->generalNotifications:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedTab="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->selectedTab:Lcom/squareup/notificationcenter/NotificationCenterTab;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", hasError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->hasError:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", analytics="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$LoadNotifications;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
