.class public final Lcom/squareup/notificationcenter/impl/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenter/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final message_view_margin:I = 0x7f0702c2

.field public static final notification_row_bottom_gap:I = 0x7f0703e1

.field public static final notification_row_corner:I = 0x7f0703e2

.field public static final notification_row_elevation:I = 0x7f0703e3

.field public static final notification_row_horizontal_margin:I = 0x7f0703e4

.field public static final notification_row_max_width:I = 0x7f0703e5

.field public static final notification_row_top_gap:I = 0x7f0703e6

.field public static final notification_title_content_gap:I = 0x7f0703ea

.field public static final responsive_16_32:I = 0x7f070461

.field public static final responsive_36_40:I = 0x7f07046f

.field public static final selected_tab_corner:I = 0x7f0704b4

.field public static final tab_bar_corner:I = 0x7f07051d

.field public static final tab_bar_height:I = 0x7f07051e

.field public static final tab_bar_padding:I = 0x7f07051f

.field public static final tab_bar_selected_elevation:I = 0x7f070520

.field public static final tab_bar_unselected_elevation:I = 0x7f070521


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
