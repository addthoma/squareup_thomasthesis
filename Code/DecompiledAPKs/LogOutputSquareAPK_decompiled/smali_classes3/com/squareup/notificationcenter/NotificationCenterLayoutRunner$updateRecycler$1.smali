.class final Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$updateRecycler$1;
.super Lkotlin/jvm/internal/Lambda;
.source "NotificationCenterLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->updateRecycler(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cycler/Update<",
        "Lcom/squareup/notificationcenter/ui/NotificationCenterRow;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/Update;",
        "Lcom/squareup/notificationcenter/ui/NotificationCenterRow;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $notifications:Ljava/util/List;

.field final synthetic $onNotificationClicked:Lkotlin/jvm/functions/Function1;

.field final synthetic this$0:Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;Ljava/util/List;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$updateRecycler$1;->this$0:Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;

    iput-object p2, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$updateRecycler$1;->$notifications:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$updateRecycler$1;->$onNotificationClicked:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 44
    check-cast p1, Lcom/squareup/cycler/Update;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$updateRecycler$1;->invoke(Lcom/squareup/cycler/Update;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/Update;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Update<",
            "Lcom/squareup/notificationcenter/ui/NotificationCenterRow;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$updateRecycler$1;->this$0:Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;

    .line 140
    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$updateRecycler$1;->$notifications:Ljava/util/List;

    .line 141
    iget-object v2, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$updateRecycler$1;->$onNotificationClicked:Lkotlin/jvm/functions/Function1;

    .line 139
    invoke-static {v0, v1, v2}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->access$createNotificationRowsFromNotifications(Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;Ljava/util/List;Lkotlin/jvm/functions/Function1;)Ljava/util/List;

    move-result-object v0

    .line 143
    invoke-static {v0}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Update;->setData(Lcom/squareup/cycler/DataSource;)V

    return-void
.end method
