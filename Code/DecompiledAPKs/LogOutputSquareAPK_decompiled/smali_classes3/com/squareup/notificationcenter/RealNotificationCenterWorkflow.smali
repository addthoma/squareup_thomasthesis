.class public final Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealNotificationCenterWorkflow.kt"

# interfaces
.implements Lcom/squareup/notificationcenter/NotificationCenterWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/notificationcenter/NotificationCenterState;",
        "Lcom/squareup/notificationcenter/NotificationCenterOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/notificationcenter/NotificationCenterWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealNotificationCenterWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealNotificationCenterWorkflow.kt\ncom/squareup/notificationcenter/RealNotificationCenterWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 4 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,366:1\n32#2,12:367\n149#3,5:379\n149#3,5:384\n149#3,5:389\n149#3,5:394\n149#3,5:399\n41#4:404\n56#4,2:405\n276#5:407\n*E\n*S KotlinDebug\n*F\n+ 1 RealNotificationCenterWorkflow.kt\ncom/squareup/notificationcenter/RealNotificationCenterWorkflow\n*L\n63#1,12:367\n81#1,5:379\n88#1,5:384\n101#1,5:389\n109#1,5:394\n125#1,5:399\n215#1:404\n215#1,2:405\n215#1:407\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0001.B-\u0008\u0007\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J&\u0010\u0019\u001a\u00020\u00032\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001b2\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0002J\u001f\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u00032\u0008\u0010 \u001a\u0004\u0018\u00010!H\u0016\u00a2\u0006\u0002\u0010\"JS\u0010#\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u001f\u001a\u00020\u00032\u0006\u0010$\u001a\u00020\u00042\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001bH\u0016\u00a2\u0006\u0002\u0010%J\u001e\u0010&\u001a\u00020\'2\u0006\u0010$\u001a\u00020(2\u000c\u0010)\u001a\u0008\u0012\u0004\u0012\u00020+0*H\u0002J\u0010\u0010&\u001a\u00020\'2\u0006\u0010$\u001a\u00020,H\u0002J\u0010\u0010-\u001a\u00020!2\u0006\u0010$\u001a\u00020\u0004H\u0016R \u0010\u0015\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00180\u00170\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;",
        "Lcom/squareup/notificationcenter/NotificationCenterWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/notificationcenter/NotificationCenterState;",
        "Lcom/squareup/notificationcenter/NotificationCenterOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "openInBrowserDialogWorkflowProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/notificationcenter/OpenInBrowserDialogWorkflow;",
        "analytics",
        "Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
        "notificationsRepository",
        "Lcom/squareup/notificationcenterdata/NotificationsRepository;",
        "notificationResolver",
        "Lcom/squareup/notificationcenterdata/NotificationResolver;",
        "(Ljavax/inject/Provider;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;Lcom/squareup/notificationcenterdata/NotificationsRepository;Lcom/squareup/notificationcenterdata/NotificationResolver;)V",
        "allNotificationsWorker",
        "Lcom/squareup/workflow/Worker;",
        "Lkotlin/Pair;",
        "Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;",
        "handleNotifications",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "selectedTab",
        "Lcom/squareup/notificationcenter/NotificationCenterTab;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/notificationcenter/NotificationCenterState;",
        "render",
        "state",
        "(Lkotlin/Unit;Lcom/squareup/notificationcenter/NotificationCenterState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "showingOpenInBrowserDialogScreenFromState",
        "Lcom/squareup/notificationcenter/NotificationCenterScreen;",
        "Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action;",
        "Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final allNotificationsWorker:Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Worker<",
            "Lkotlin/Pair<",
            "Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;",
            "Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;",
            ">;>;"
        }
    .end annotation
.end field

.field private final analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

.field private final notificationResolver:Lcom/squareup/notificationcenterdata/NotificationResolver;

.field private final openInBrowserDialogWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/OpenInBrowserDialogWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;Lcom/squareup/notificationcenterdata/NotificationsRepository;Lcom/squareup/notificationcenterdata/NotificationResolver;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/OpenInBrowserDialogWorkflow;",
            ">;",
            "Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
            "Lcom/squareup/notificationcenterdata/NotificationsRepository;",
            "Lcom/squareup/notificationcenterdata/NotificationResolver;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "openInBrowserDialogWorkflowProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationsRepository"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationResolver"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->openInBrowserDialogWorkflowProvider:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    iput-object p4, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->notificationResolver:Lcom/squareup/notificationcenterdata/NotificationResolver;

    .line 210
    sget-object p1, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 212
    sget-object p2, Lcom/squareup/notificationcenterdata/Notification$Priority$Important;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$Priority$Important;

    check-cast p2, Lcom/squareup/notificationcenterdata/Notification$Priority;

    invoke-interface {p3, p2}, Lcom/squareup/notificationcenterdata/NotificationsRepository;->notifications(Lcom/squareup/notificationcenterdata/Notification$Priority;)Lio/reactivex/Observable;

    move-result-object p2

    .line 213
    sget-object p4, Lcom/squareup/notificationcenterdata/Notification$Priority$General;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$Priority$General;

    check-cast p4, Lcom/squareup/notificationcenterdata/Notification$Priority;

    invoke-interface {p3, p4}, Lcom/squareup/notificationcenterdata/NotificationsRepository;->notifications(Lcom/squareup/notificationcenterdata/Notification$Priority;)Lio/reactivex/Observable;

    move-result-object p3

    .line 211
    invoke-virtual {p1, p2, p3}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    .line 404
    sget-object p2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string/jumbo p2, "this.toFlowable(BUFFER)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_0

    .line 406
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 407
    const-class p2, Lkotlin/Pair;

    sget-object p3, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class p4, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;

    invoke-static {p4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p4

    invoke-virtual {p3, p4}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object p3

    sget-object p4, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v0, Lcom/squareup/notificationcenterdata/NotificationsRepository$Result;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    invoke-virtual {p4, v0}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object p4

    invoke-static {p2, p3, p4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance p3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p3, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p3, Lcom/squareup/workflow/Worker;

    .line 404
    iput-object p3, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->allNotificationsWorker:Lcom/squareup/workflow/Worker;

    return-void

    .line 406
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;)Lcom/squareup/notificationcenter/NotificationCenterAnalytics;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getNotificationResolver$p(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;)Lcom/squareup/notificationcenterdata/NotificationResolver;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->notificationResolver:Lcom/squareup/notificationcenterdata/NotificationResolver;

    return-object p0
.end method

.method private final handleNotifications(Lcom/squareup/workflow/RenderContext;Lcom/squareup/notificationcenter/NotificationCenterTab;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/notificationcenter/NotificationCenterState;",
            "-",
            "Lcom/squareup/notificationcenter/NotificationCenterOutput;",
            ">;",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            ")V"
        }
    .end annotation

    .line 186
    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->allNotificationsWorker:Lcom/squareup/workflow/Worker;

    new-instance v0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$handleNotifications$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$handleNotifications$1;-><init>(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;Lcom/squareup/notificationcenter/NotificationCenterTab;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final showingOpenInBrowserDialogScreenFromState(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;Lcom/squareup/workflow/Sink;)Lcom/squareup/notificationcenter/NotificationCenterScreen;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action;",
            ">;)",
            "Lcom/squareup/notificationcenter/NotificationCenterScreen;"
        }
    .end annotation

    .line 134
    new-instance v8, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;

    .line 135
    invoke-static {p1}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflowKt;->access$getNotifications$p(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)Ljava/util/List;

    move-result-object v1

    .line 136
    invoke-virtual {p1}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;->getSelectedTab()Lcom/squareup/notificationcenter/NotificationCenterTab;

    move-result-object v2

    .line 137
    invoke-virtual {p1}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;->getShowError()Z

    move-result v3

    .line 138
    new-instance v0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$1;

    invoke-direct {v0, p2}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$1;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 139
    new-instance v0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$2;

    invoke-direct {v0, p2}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$2;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 140
    new-instance v0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$3;-><init>(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;Lcom/squareup/workflow/Sink;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 145
    new-instance v0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$4;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$showingOpenInBrowserDialogScreenFromState$4;-><init>(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)V

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function1;

    move-object v0, v8

    .line 134
    invoke-direct/range {v0 .. v7}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;-><init>(Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    check-cast v8, Lcom/squareup/notificationcenter/NotificationCenterScreen;

    return-object v8
.end method

.method private final showingOpenInBrowserDialogScreenFromState(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;)Lcom/squareup/notificationcenter/NotificationCenterScreen;
    .locals 3

    .line 170
    new-instance v0, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;

    .line 171
    invoke-static {p1}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflowKt;->access$getNotifications$p(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;)Ljava/util/List;

    move-result-object v1

    .line 172
    invoke-virtual {p1}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;->getSelectedTab()Lcom/squareup/notificationcenter/NotificationCenterTab;

    move-result-object v2

    .line 173
    invoke-virtual {p1}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;->getShowError()Z

    move-result p1

    .line 170
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;-><init>(Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;Z)V

    check-cast v0, Lcom/squareup/notificationcenter/NotificationCenterScreen;

    return-object v0
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/notificationcenter/NotificationCenterState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 367
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 372
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 373
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 374
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 375
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 376
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 377
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 378
    :cond_3
    check-cast v1, Lcom/squareup/notificationcenter/NotificationCenterState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 64
    :cond_4
    sget-object p1, Lcom/squareup/notificationcenter/NotificationCenterState$LoadingNotificationsState;->INSTANCE:Lcom/squareup/notificationcenter/NotificationCenterState$LoadingNotificationsState;

    move-object v1, p1

    check-cast v1, Lcom/squareup/notificationcenter/NotificationCenterState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/notificationcenter/NotificationCenterState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/notificationcenter/NotificationCenterState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->render(Lkotlin/Unit;Lcom/squareup/notificationcenter/NotificationCenterState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/notificationcenter/NotificationCenterState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/notificationcenter/NotificationCenterState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/notificationcenter/NotificationCenterState;",
            "-",
            "Lcom/squareup/notificationcenter/NotificationCenterOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "props"

    move-object/from16 v4, p1

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "state"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "context"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-interface/range {p3 .. p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v3

    .line 76
    instance-of v4, v1, Lcom/squareup/notificationcenter/NotificationCenterState$LoadingNotificationsState;

    const/4 v8, 0x0

    const-string v9, ""

    if-eqz v4, :cond_0

    const/4 v1, 0x0

    .line 77
    invoke-direct {v0, v2, v1}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->handleNotifications(Lcom/squareup/workflow/RenderContext;Lcom/squareup/notificationcenter/NotificationCenterTab;)V

    .line 78
    sget-object v1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    new-instance v2, Lcom/squareup/notificationcenter/NotificationCenterScreen$LoadingScreen;

    .line 79
    new-instance v4, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$1;

    invoke-direct {v4, v3}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 80
    new-instance v5, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$2;

    invoke-direct {v5, v3}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 78
    invoke-direct {v2, v4, v5}, Lcom/squareup/notificationcenter/NotificationCenterScreen$LoadingScreen;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 380
    new-instance v3, Lcom/squareup/workflow/legacy/Screen;

    .line 381
    const-class v4, Lcom/squareup/notificationcenter/NotificationCenterScreen;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-static {v4, v9}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 382
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 380
    invoke-direct {v3, v4, v2, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 78
    invoke-virtual {v1, v3}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v1

    .line 82
    invoke-static {v1, v8}, Lcom/squareup/workflow/LayeredScreenKt;->withPersistence(Ljava/util/Map;Z)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 84
    :cond_0
    instance-of v4, v1, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    if-eqz v4, :cond_2

    .line 85
    move-object v4, v1

    check-cast v4, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    invoke-virtual {v4}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;->getSelectedTab()Lcom/squareup/notificationcenter/NotificationCenterTab;

    move-result-object v5

    invoke-direct {v0, v2, v5}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->handleNotifications(Lcom/squareup/workflow/RenderContext;Lcom/squareup/notificationcenter/NotificationCenterTab;)V

    .line 86
    invoke-static {v4}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflowKt;->access$getHasNotifications$p(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 87
    invoke-direct {v0, v4, v3}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->showingOpenInBrowserDialogScreenFromState(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;Lcom/squareup/workflow/Sink;)Lcom/squareup/notificationcenter/NotificationCenterScreen;

    move-result-object v1

    .line 88
    sget-object v2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 385
    new-instance v3, Lcom/squareup/workflow/legacy/Screen;

    .line 386
    const-class v4, Lcom/squareup/notificationcenter/NotificationCenterScreen;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-static {v4, v9}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 387
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 385
    invoke-direct {v3, v4, v1, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 88
    invoke-virtual {v2, v3}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v1

    .line 89
    invoke-static {v1, v8}, Lcom/squareup/workflow/LayeredScreenKt;->withPersistence(Ljava/util/Map;Z)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 91
    :cond_1
    sget-object v2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    new-instance v5, Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen;

    .line 92
    invoke-virtual {v4}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;->getSelectedTab()Lcom/squareup/notificationcenter/NotificationCenterTab;

    move-result-object v11

    .line 93
    invoke-virtual {v4}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;->getShowError()Z

    move-result v12

    .line 94
    new-instance v4, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$3;

    invoke-direct {v4, v3}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$3;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v13, v4

    check-cast v13, Lkotlin/jvm/functions/Function0;

    .line 95
    new-instance v4, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$4;

    invoke-direct {v4, v3}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$4;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v14, v4

    check-cast v14, Lkotlin/jvm/functions/Function0;

    .line 96
    new-instance v4, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$5;

    invoke-direct {v4, v0, v1, v3}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$5;-><init>(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;Lcom/squareup/notificationcenter/NotificationCenterState;Lcom/squareup/workflow/Sink;)V

    move-object v15, v4

    check-cast v15, Lkotlin/jvm/functions/Function1;

    move-object v10, v5

    .line 91
    invoke-direct/range {v10 .. v15}, Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen;-><init>(Lcom/squareup/notificationcenter/NotificationCenterTab;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    check-cast v5, Lcom/squareup/workflow/legacy/V2Screen;

    .line 390
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 391
    const-class v3, Lcom/squareup/notificationcenter/NotificationCenterScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v9}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 392
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 390
    invoke-direct {v1, v3, v5, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 91
    invoke-virtual {v2, v1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v1

    .line 102
    invoke-static {v1, v8}, Lcom/squareup/workflow/LayeredScreenKt;->withPersistence(Ljava/util/Map;Z)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 105
    :cond_2
    instance-of v3, v1, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    if-eqz v3, :cond_3

    .line 106
    move-object v3, v1

    check-cast v3, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    invoke-virtual {v3}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;->getSelectedTab()Lcom/squareup/notificationcenter/NotificationCenterTab;

    move-result-object v4

    invoke-direct {v0, v2, v4}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->handleNotifications(Lcom/squareup/workflow/RenderContext;Lcom/squareup/notificationcenter/NotificationCenterTab;)V

    .line 107
    invoke-direct {v0, v3}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->showingOpenInBrowserDialogScreenFromState(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;)Lcom/squareup/notificationcenter/NotificationCenterScreen;

    move-result-object v4

    .line 108
    sget-object v10, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 109
    check-cast v4, Lcom/squareup/workflow/legacy/V2Screen;

    .line 395
    new-instance v11, Lcom/squareup/workflow/legacy/Screen;

    .line 396
    const-class v5, Lcom/squareup/notificationcenter/NotificationCenterScreen;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v5

    invoke-static {v5, v9}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    .line 397
    sget-object v6, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v6}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v6

    .line 395
    invoke-direct {v11, v5, v4, v6}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v12, 0x0

    const/4 v13, 0x0

    .line 111
    iget-object v4, v0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->openInBrowserDialogWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    const-string v5, "openInBrowserDialogWorkflowProvider.get()"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/squareup/workflow/Workflow;

    .line 112
    invoke-virtual {v3}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;->getSelectedExternalNotification()Lcom/squareup/notificationcenterdata/Notification;

    move-result-object v3

    const/4 v5, 0x0

    .line 113
    new-instance v6, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$6;

    invoke-direct {v6, v0, v1}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$6;-><init>(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;Lcom/squareup/notificationcenter/NotificationCenterState;)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v14, 0x0

    move-object/from16 v1, p3

    move-object v2, v4

    move-object v4, v5

    move-object v5, v6

    move v6, v7

    move-object v7, v14

    .line 110
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 400
    new-instance v14, Lcom/squareup/workflow/legacy/Screen;

    .line 401
    const-class v2, Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v9}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 402
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 400
    invoke-direct {v14, v2, v1, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v15, 0x6

    const/16 v16, 0x0

    .line 108
    invoke-static/range {v10 .. v16}, Lcom/squareup/container/PosLayering$Companion;->dialogStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    .line 126
    invoke-static {v1, v8}, Lcom/squareup/workflow/LayeredScreenKt;->withPersistence(Ljava/util/Map;Z)Ljava/util/Map;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_3
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public snapshotState(Lcom/squareup/notificationcenter/NotificationCenterState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/notificationcenter/NotificationCenterState;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->snapshotState(Lcom/squareup/notificationcenter/NotificationCenterState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
