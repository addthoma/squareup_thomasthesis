.class public final Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;
.super Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action;
.source "RealNotificationCenterWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OpenNotification"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\u0014\u0010\u0016\u001a\u0004\u0018\u00010\u0017*\u0008\u0012\u0004\u0012\u00020\u00190\u0018H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;",
        "Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action;",
        "notification",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "showingNotificationState",
        "Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;",
        "(Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)V",
        "getNotification",
        "()Lcom/squareup/notificationcenterdata/Notification;",
        "getShowingNotificationState",
        "()Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "Lcom/squareup/notificationcenter/NotificationCenterOutput;",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/notificationcenter/NotificationCenterState;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final notification:Lcom/squareup/notificationcenterdata/Notification;

.field private final showingNotificationState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;


# direct methods
.method public constructor <init>(Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)V
    .locals 1

    const-string v0, "notification"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "showingNotificationState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 314
    invoke-direct {p0, v0}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->notification:Lcom/squareup/notificationcenterdata/Notification;

    iput-object p2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->showingNotificationState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;ILjava/lang/Object;)Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->notification:Lcom/squareup/notificationcenterdata/Notification;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->showingNotificationState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->copy(Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/notificationcenter/NotificationCenterOutput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/notificationcenter/NotificationCenterState;",
            ">;)",
            "Lcom/squareup/notificationcenter/NotificationCenterOutput;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 316
    new-instance p1, Lcom/squareup/notificationcenter/NotificationCenterOutput$RequestedNotificationHandling;

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->notification:Lcom/squareup/notificationcenterdata/Notification;

    invoke-direct {p1, v0}, Lcom/squareup/notificationcenter/NotificationCenterOutput$RequestedNotificationHandling;-><init>(Lcom/squareup/notificationcenterdata/Notification;)V

    check-cast p1, Lcom/squareup/notificationcenter/NotificationCenterOutput;

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 311
    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/notificationcenter/NotificationCenterOutput;

    move-result-object p1

    return-object p1
.end method

.method public final component1()Lcom/squareup/notificationcenterdata/Notification;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->notification:Lcom/squareup/notificationcenterdata/Notification;

    return-object v0
.end method

.method public final component2()Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->showingNotificationState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    return-object v0
.end method

.method public final copy(Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;
    .locals 1

    const-string v0, "notification"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "showingNotificationState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;

    invoke-direct {v0, p1, p2}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;-><init>(Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->notification:Lcom/squareup/notificationcenterdata/Notification;

    iget-object v1, p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->notification:Lcom/squareup/notificationcenterdata/Notification;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->showingNotificationState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    iget-object p1, p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->showingNotificationState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getNotification()Lcom/squareup/notificationcenterdata/Notification;
    .locals 1

    .line 312
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->notification:Lcom/squareup/notificationcenterdata/Notification;

    return-object v0
.end method

.method public final getShowingNotificationState()Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->showingNotificationState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->notification:Lcom/squareup/notificationcenterdata/Notification;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->showingNotificationState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OpenNotification(notification="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->notification:Lcom/squareup/notificationcenterdata/Notification;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showingNotificationState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenNotification;->showingNotificationState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
