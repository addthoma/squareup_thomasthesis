.class Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PaymentPadTabletLandscapeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->animateToEditMode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)V
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$2;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .line 114
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$2;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->access$002(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    .line 115
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$2;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    iget-object p1, p1, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->saleFrame:Landroid/view/View;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 116
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$2;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    iget-object p1, p1, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->presenter:Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;->endAnimation()V

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "End animate to Edit Mode"

    .line 118
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    .line 106
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$2;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    iget-object p1, p1, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->saleFrame:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 107
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$2;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    iget-object p1, p1, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->editFrame:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 108
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$2;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    iget-object p1, p1, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->presenter:Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;->startAnimation()V

    new-array p1, v0, [Ljava/lang/Object;

    const-string v0, "Start animate to Edit Mode"

    .line 110
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
