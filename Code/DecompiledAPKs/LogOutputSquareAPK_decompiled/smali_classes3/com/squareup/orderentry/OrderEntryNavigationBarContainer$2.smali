.class Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "OrderEntryNavigationBarContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->fadeToSaleActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;)V
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$2;->this$0:Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .line 128
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$2;->this$0:Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->access$402(Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    .line 129
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$2;->this$0:Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;

    invoke-static {p1}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->access$300(Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;)Lcom/squareup/orderentry/NavigationBarEditView;

    move-result-object p1

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/NavigationBarEditView;->setVisibility(I)V

    .line 130
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$2;->this$0:Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;

    invoke-static {p1}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->access$200(Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;)Lcom/squareup/orderentry/NavigationBarSaleView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/NavigationBarSaleView;->setVisibility(I)V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    .line 123
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$2;->this$0:Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;

    invoke-static {p1}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->access$200(Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;)Lcom/squareup/orderentry/NavigationBarSaleView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/NavigationBarSaleView;->setVisibility(I)V

    .line 124
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer$2;->this$0:Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;

    invoke-static {p1}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->access$300(Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;)Lcom/squareup/orderentry/NavigationBarEditView;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/NavigationBarEditView;->setVisibility(I)V

    return-void
.end method
