.class public final Lcom/squareup/orderentry/RealOrderEntryAppletGateway_AsHomeApplet_Factory;
.super Ljava/lang/Object;
.source "RealOrderEntryAppletGateway_AsHomeApplet_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsHomeApplet;",
        ">;"
    }
.end annotation


# instance fields
.field private final orderEntryAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryApplet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryApplet;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/orderentry/RealOrderEntryAppletGateway_AsHomeApplet_Factory;->orderEntryAppletProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/orderentry/RealOrderEntryAppletGateway_AsHomeApplet_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryApplet;",
            ">;)",
            "Lcom/squareup/orderentry/RealOrderEntryAppletGateway_AsHomeApplet_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/orderentry/RealOrderEntryAppletGateway_AsHomeApplet_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/RealOrderEntryAppletGateway_AsHomeApplet_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/orderentry/OrderEntryApplet;)Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsHomeApplet;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsHomeApplet;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsHomeApplet;-><init>(Lcom/squareup/orderentry/OrderEntryApplet;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsHomeApplet;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/orderentry/RealOrderEntryAppletGateway_AsHomeApplet_Factory;->orderEntryAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryApplet;

    invoke-static {v0}, Lcom/squareup/orderentry/RealOrderEntryAppletGateway_AsHomeApplet_Factory;->newInstance(Lcom/squareup/orderentry/OrderEntryApplet;)Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsHomeApplet;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/orderentry/RealOrderEntryAppletGateway_AsHomeApplet_Factory;->get()Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsHomeApplet;

    move-result-object v0

    return-object v0
.end method
