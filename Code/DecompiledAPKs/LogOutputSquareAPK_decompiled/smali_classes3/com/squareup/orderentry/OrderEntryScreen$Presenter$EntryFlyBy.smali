.class public Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;
.super Ljava/lang/Object;
.source "OrderEntryScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/OrderEntryScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EntryFlyBy"
.end annotation


# instance fields
.field private final listener:Lcom/squareup/orderentry/FlyByListener;

.field private final source:Landroid/view/View;

.field private final useDiscountDrawable:Z


# direct methods
.method public constructor <init>(Landroid/view/View;ZLcom/squareup/orderentry/FlyByListener;)V
    .locals 0

    .line 425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 426
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;->source:Landroid/view/View;

    .line 427
    iput-boolean p2, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;->useDiscountDrawable:Z

    .line 428
    iput-object p3, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;->listener:Lcom/squareup/orderentry/FlyByListener;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;)Landroid/view/View;
    .locals 0

    .line 420
    iget-object p0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;->source:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;)Z
    .locals 0

    .line 420
    iget-boolean p0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;->useDiscountDrawable:Z

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;)Lcom/squareup/orderentry/FlyByListener;
    .locals 0

    .line 420
    iget-object p0, p0, Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;->listener:Lcom/squareup/orderentry/FlyByListener;

    return-object p0
.end method
