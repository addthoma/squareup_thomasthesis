.class Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;
.super Landroid/widget/BaseAdapter;
.source "CategoryDropDownView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/CategoryDropDownView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CategoryAdapter"
.end annotation


# instance fields
.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

.field placeholders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 1

    .line 92
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 89
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->placeholders:Ljava/util/List;

    .line 93
    iput-object p1, p0, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz v0, :cond_0

    .line 115
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getCount()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 119
    :goto_0
    iget-object v1, p0, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->placeholders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v1, v0

    return v1
.end method

.method public getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->placeholders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    iget-object v1, p0, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->placeholders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr p1, v1

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToPosition(I)Z

    .line 128
    iget-object p1, p0, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 86
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getTitleForPlaceholder(Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;)Ljava/lang/Integer;
    .locals 2

    .line 147
    sget-object v0, Lcom/squareup/orderentry/CategoryDropDownView$1;->$SwitchMap$com$squareup$librarylist$LibraryListConfiguration$Placeholder:[I

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 165
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "unknown placeholder: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 163
    :pswitch_0
    sget p1, Lcom/squareup/librarylist/R$string;->item_library_custom_amount:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 161
    :pswitch_1
    sget p1, Lcom/squareup/librarylist/R$string;->item_library_redeem_rewards:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 159
    :pswitch_2
    sget p1, Lcom/squareup/librarylist/R$string;->item_library_all_gift_cards:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 157
    :pswitch_3
    sget p1, Lcom/squareup/librarylist/R$string;->item_library_all_discounts:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 155
    :pswitch_4
    sget p1, Lcom/squareup/librarylist/R$string;->item_library_all_services:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 149
    :pswitch_5
    iget-object p1, p0, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {p1}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowPartiallyRolledOutFeature()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 150
    sget p1, Lcom/squareup/librarylist/R$string;->item_library_all_items_and_services:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 152
    :cond_0
    sget p1, Lcom/squareup/librarylist/R$string;->item_library_all_items:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    if-nez p2, :cond_0

    .line 97
    sget p2, Lcom/squareup/orderentry/R$layout;->category_drop_down_row:I

    .line 98
    invoke-static {p2, p3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    check-cast p2, Landroid/widget/TextView;

    .line 101
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->getItem(I)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p3

    if-eqz p3, :cond_1

    .line 103
    invoke-virtual {p3}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 105
    :cond_1
    iget-object p3, p0, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->placeholders:Ljava/util/List;

    invoke-interface {p3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    .line 106
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->getTitleForPlaceholder(Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-object p2
.end method

.method public update(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;)V"
        }
    .end annotation

    .line 136
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz v0, :cond_0

    if-eq v0, p1, :cond_0

    .line 137
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 138
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    .line 140
    :cond_0
    iput-object p1, p0, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 141
    iput-object p2, p0, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->placeholders:Ljava/util/List;

    .line 142
    invoke-virtual {p0}, Lcom/squareup/orderentry/CategoryDropDownView$CategoryAdapter;->notifyDataSetChanged()V

    return-void
.end method
