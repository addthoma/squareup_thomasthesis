.class public final Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;
.super Ljava/lang/Object;
.source "PosPaymentFlowHistoryFactory.kt"

# interfaces
.implements Lcom/squareup/ui/main/PaymentFlowHistoryFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;",
        "Lcom/squareup/ui/main/PaymentFlowHistoryFactory;",
        "apiRequestController",
        "Lcom/squareup/api/ApiRequestController;",
        "home",
        "Lcom/squareup/ui/main/Home;",
        "invoicesAppletRunner",
        "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
        "(Lcom/squareup/api/ApiRequestController;Lcom/squareup/ui/main/Home;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;)V",
        "historyToPaymentFlowBackground",
        "Lflow/History;",
        "history",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final apiRequestController:Lcom/squareup/api/ApiRequestController;

.field private final home:Lcom/squareup/ui/main/Home;

.field private final invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/api/ApiRequestController;Lcom/squareup/ui/main/Home;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "apiRequestController"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "home"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoicesAppletRunner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    iput-object p2, p0, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;->home:Lcom/squareup/ui/main/Home;

    iput-object p3, p0, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;->invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    return-void
.end method


# virtual methods
.method public historyToPaymentFlowBackground(Lflow/History;)Lflow/History;
    .locals 2

    const-string v0, "history"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->isApiRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    sget-object p1, Lcom/squareup/ui/main/BlankScreen;->INSTANCE:Lcom/squareup/ui/main/BlankScreen;

    invoke-static {p1}, Lflow/History;->single(Ljava/lang/Object;)Lflow/History;

    move-result-object p1

    const-string v0, "History.single(BlankScreen.INSTANCE)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;->invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    invoke-interface {v0}, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;->isTakingInvoicePayment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 28
    iget-object v0, p0, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;->invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    iget-object v1, p0, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;->home:Lcom/squareup/ui/main/Home;

    invoke-interface {v0, v1, p1}, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;->historyBehindInvoiceTenderScreen(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;

    move-result-object p1

    const-string v0, "invoicesAppletRunner.his\u2026nderScreen(home, history)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 31
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;->home:Lcom/squareup/ui/main/Home;

    invoke-static {v0, p1}, Lcom/squareup/ui/main/HomeKt;->getHomeHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;

    move-result-object p1

    :goto_0
    return-object p1
.end method
