.class public final Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "ItemListScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/category/ItemListScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final barcodeScannerTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final editItemGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final entryHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final favoritesTileItemSelectionEventsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final itemsAppletGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)V"
        }
    .end annotation

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p2, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p3, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->entryHandlerProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p4, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p5, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p6, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p7, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p8, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p9, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->favoritesTileItemSelectionEventsProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p10, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->itemsAppletGatewayProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p11, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->editItemGatewayProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p12, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)",
            "Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;"
        }
    .end annotation

    .line 91
    new-instance v13, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/ui/main/CheckoutEntryHandler;Ljavax/inject/Provider;Lcom/squareup/badbus/BadBus;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;Lcom/squareup/ui/items/ItemsAppletGateway;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/orderentry/category/ItemListScreen$Presenter;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lflow/Flow;",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            "Lcom/squareup/ui/items/ItemsAppletGateway;",
            "Lcom/squareup/ui/items/EditItemGateway;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ")",
            "Lcom/squareup/orderentry/category/ItemListScreen$Presenter;"
        }
    .end annotation

    .line 101
    new-instance v13, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;-><init>(Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/ui/main/CheckoutEntryHandler;Ljavax/inject/Provider;Lcom/squareup/badbus/BadBus;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;Lcom/squareup/ui/items/ItemsAppletGateway;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/tutorialv2/TutorialCore;)V

    return-object v13
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/category/ItemListScreen$Presenter;
    .locals 13

    .line 78
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->entryHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/main/CheckoutEntryHandler;

    iget-object v4, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/badbus/BadBus;

    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->favoritesTileItemSelectionEventsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->itemsAppletGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/ui/items/ItemsAppletGateway;

    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->editItemGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/ui/items/EditItemGateway;

    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/tutorialv2/TutorialCore;

    invoke-static/range {v1 .. v12}, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->newInstance(Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/ui/main/CheckoutEntryHandler;Ljavax/inject/Provider;Lcom/squareup/badbus/BadBus;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;Lcom/squareup/ui/items/ItemsAppletGateway;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/orderentry/category/ItemListScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/orderentry/category/ItemListScreen_Presenter_Factory;->get()Lcom/squareup/orderentry/category/ItemListScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
