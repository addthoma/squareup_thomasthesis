.class Lcom/squareup/orderentry/KeypadPanelPresenter$1;
.super Lcom/squareup/padlock/MoneyKeypadListener;
.source "KeypadPanelPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/KeypadPanelPresenter;->initializeKeypad(Lcom/squareup/padlock/Padlock;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/KeypadPanelPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/KeypadPanelPresenter;Lcom/squareup/padlock/Padlock;Landroid/os/Vibrator;J)V
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadPanelPresenter$1;->this$0:Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/squareup/padlock/MoneyKeypadListener;-><init>(Lcom/squareup/padlock/Padlock;Landroid/os/Vibrator;J)V

    return-void
.end method


# virtual methods
.method public getAmount()J
    .locals 2

    .line 216
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter$1;->this$0:Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-static {v0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->access$000(Lcom/squareup/orderentry/KeypadPanelPresenter;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getKeypadPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public onClearClicked()V
    .locals 5

    .line 207
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter$1;->this$0:Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-static {v0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->access$000(Lcom/squareup/orderentry/KeypadPanelPresenter;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getKeypadPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    .line 208
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter$1;->this$0:Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->showAppropriateClearDialog()V

    goto :goto_0

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter$1;->this$0:Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-static {v0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->access$000(Lcom/squareup/orderentry/KeypadPanelPresenter;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->clearKeypadItem()V

    .line 211
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter$1;->updateKeyStates()V

    :goto_0
    return-void
.end method

.method public onClearLongpressed()V
    .locals 1

    .line 202
    invoke-super {p0}, Lcom/squareup/padlock/MoneyKeypadListener;->onClearLongpressed()V

    .line 203
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter$1;->this$0:Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->showAppropriateClearDialog()V

    return-void
.end method

.method public onSubmitClicked()V
    .locals 2

    .line 224
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter$1;->this$0:Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-static {v0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->access$100(Lcom/squareup/orderentry/KeypadPanelPresenter;)Lcom/squareup/log/cart/TransactionInteractionsLogger;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_KEYPAD_PANEL:Lcom/squareup/analytics/RegisterViewName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter$1;->this$0:Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-static {v0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->access$200(Lcom/squareup/orderentry/KeypadPanelPresenter;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/orderentry/KeypadPanel;

    invoke-virtual {v1}, Lcom/squareup/orderentry/KeypadPanel;->getPriceView()Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/orderentry/KeypadPanelPresenter;->access$300(Lcom/squareup/orderentry/KeypadPanelPresenter;Landroid/view/View;)V

    return-void
.end method

.method public updateAmount(J)V
    .locals 1

    .line 220
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter$1;->this$0:Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/orderentry/KeypadPanelPresenter;->updateKeypadAmount(J)V

    return-void
.end method
