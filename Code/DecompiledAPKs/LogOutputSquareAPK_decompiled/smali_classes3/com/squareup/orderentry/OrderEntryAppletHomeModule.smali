.class public abstract Lcom/squareup/orderentry/OrderEntryAppletHomeModule;
.super Ljava/lang/Object;
.source "OrderEntryAppletHomeModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/orderentry/OrderEntryCommonModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideHome(Lcom/squareup/orderentry/OrderEntryApplet;)Lcom/squareup/ui/main/Home;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideHomeScreenSelector(Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;)Lcom/squareup/ui/main/HomeScreenSelector;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideOrderEntryAppletGateway(Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsHomeApplet;)Lcom/squareup/orderentry/OrderEntryAppletGateway;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePaymentFlowHistoryFactory(Lcom/squareup/orderentry/PosPaymentFlowHistoryFactory;)Lcom/squareup/ui/main/PaymentFlowHistoryFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
