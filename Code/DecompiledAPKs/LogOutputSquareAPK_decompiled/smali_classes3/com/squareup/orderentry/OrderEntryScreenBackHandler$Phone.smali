.class public Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;
.super Ljava/lang/Object;
.source "OrderEntryScreenBackHandler.java"

# interfaces
.implements Lcom/squareup/orderentry/OrderEntryScreenBackHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/OrderEntryScreenBackHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Phone"
.end annotation


# instance fields
.field private final categoryDropDownPresenter:Lcom/squareup/orderentry/CategoryDropDownPresenter;

.field private final libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/orderentry/CategoryDropDownPresenter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 26
    iput-object p2, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    .line 27
    iput-object p3, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;->categoryDropDownPresenter:Lcom/squareup/orderentry/CategoryDropDownPresenter;

    return-void
.end method


# virtual methods
.method public onBackPressed()Z
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListStateManager;->getLibraryFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v0

    .line 33
    sget-object v1, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_CATEGORIES:Lcom/squareup/librarylist/LibraryListState$Filter;

    if-eq v0, v1, :cond_3

    .line 37
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;->categoryDropDownPresenter:Lcom/squareup/orderentry/CategoryDropDownPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->isDropDownOpen()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;->categoryDropDownPresenter:Lcom/squareup/orderentry/CategoryDropDownPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->toggle()V

    return v1

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    sget-object v2, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne v0, v2, :cond_2

    .line 43
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->isInteractionModeAnimating()Z

    move-result v0

    if-nez v0, :cond_1

    .line 44
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Phone;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->endEditing()V

    :cond_1
    return v1

    :cond_2
    const/4 v0, 0x0

    return v0

    .line 34
    :cond_3
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "All categories should never be visible on phone"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
