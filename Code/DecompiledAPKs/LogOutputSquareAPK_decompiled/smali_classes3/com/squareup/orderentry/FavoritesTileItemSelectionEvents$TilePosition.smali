.class public Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;
.super Ljava/lang/Object;
.source "FavoritesTileItemSelectionEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TilePosition"
.end annotation


# instance fields
.field public final columnCount:I

.field public final pageId:Ljava/lang/String;

.field public final position:Landroid/graphics/Point;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/graphics/Point;I)V
    .locals 0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;->pageId:Ljava/lang/String;

    .line 102
    iput-object p2, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;->position:Landroid/graphics/Point;

    .line 103
    iput p3, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;->columnCount:I

    return-void
.end method
