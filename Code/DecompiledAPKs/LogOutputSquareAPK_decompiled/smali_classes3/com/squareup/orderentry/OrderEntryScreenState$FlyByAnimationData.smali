.class public Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;
.super Ljava/lang/Object;
.source "OrderEntryScreenState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/OrderEntryScreenState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FlyByAnimationData"
.end annotation


# static fields
.field private static final ABBREVIATION_KEY:Ljava/lang/String; = "ABBREVIATION_KEY"

.field private static final COLOR_KEY:Ljava/lang/String; = "COLOR_KEY"

.field protected static final NO_ANIMATION_QUANTITY:I = 0x0

.field protected static final NULL_FOR_DISCOUNT:Ljava/lang/String; = null

.field private static final QUANTITY_KEY:Ljava/lang/String; = "QUANTITY_KEY"

.field private static final SOURCE_POSITION_KEY:Ljava/lang/String; = "SOURCE_POSITION_KEY"

.field private static final SOURCE_SIZE_KEY:Ljava/lang/String; = "SOURCE_SIZE_KEY"


# instance fields
.field private abbreviation:Ljava/lang/String;

.field private color:Ljava/lang/String;

.field private drawable:Landroid/graphics/drawable/Drawable;

.field private quantity:I

.field private sourcePosition:[I

.field private sourceSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 504
    iput v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->quantity:I

    return-void
.end method

.method public static buildEmptyAnimationData()Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;
    .locals 1

    .line 507
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;

    invoke-direct {v0}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;-><init>()V

    return-object v0
.end method

.method public static supportsFlyByAnimations(Lcom/squareup/util/Device;)Z
    .locals 1

    .line 515
    invoke-interface {p0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p0

    .line 517
    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPhone()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPortrait()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method


# virtual methods
.method public clearData()V
    .locals 2

    const/4 v0, 0x0

    .line 551
    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->sourcePosition:[I

    const/4 v1, -0x1

    .line 552
    iput v1, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->sourceSize:I

    .line 553
    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->color:Ljava/lang/String;

    .line 554
    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->abbreviation:Ljava/lang/String;

    .line 555
    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->drawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public clearQuantity()V
    .locals 1

    const/4 v0, 0x0

    .line 591
    iput v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->quantity:I

    return-void
.end method

.method public getDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .line 525
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->drawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    return-object v0

    .line 529
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->abbreviation:Ljava/lang/String;

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->NULL_FOR_DISCOUNT:Ljava/lang/String;

    if-ne v0, v1, :cond_1

    invoke-static {p1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forDiscount(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1

    .line 530
    :cond_1
    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->color:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forItem(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    move-result-object p1

    return-object p1
.end method

.method public getQuantity()I
    .locals 1

    .line 583
    iget v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->quantity:I

    return v0
.end method

.method public getSourcePosition()[I
    .locals 1

    .line 534
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->sourcePosition:[I

    return-object v0
.end method

.method public getSourceSize()I
    .locals 1

    .line 538
    iget v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->sourceSize:I

    return v0
.end method

.method public hasQuantityToAnimate()Z
    .locals 1

    .line 579
    iget v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->quantity:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasUsableData()Z
    .locals 1

    .line 521
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->sourcePosition:[I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public load(Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "SOURCE_POSITION_KEY"

    .line 559
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->sourcePosition:[I

    const-string v0, "SOURCE_SIZE_KEY"

    .line 560
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->sourceSize:I

    const-string v0, "QUANTITY_KEY"

    .line 561
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->quantity:I

    const-string v0, "COLOR_KEY"

    .line 562
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->color:Ljava/lang/String;

    const-string v0, "ABBREVIATION_KEY"

    .line 563
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->abbreviation:Ljava/lang/String;

    return-void
.end method

.method public save(Landroid/os/Bundle;)V
    .locals 2

    .line 567
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->sourcePosition:[I

    const-string v1, "SOURCE_POSITION_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 568
    iget v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->sourceSize:I

    const-string v1, "SOURCE_SIZE_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 569
    iget v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->quantity:I

    const-string v1, "QUANTITY_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 570
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->color:Ljava/lang/String;

    const-string v1, "COLOR_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->abbreviation:Ljava/lang/String;

    const-string v1, "ABBREVIATION_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 575
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->drawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public setData(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;[II)V
    .locals 0

    .line 543
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->abbreviation:Ljava/lang/String;

    .line 544
    iput-object p2, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->color:Ljava/lang/String;

    .line 545
    iput-object p3, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->drawable:Landroid/graphics/drawable/Drawable;

    .line 546
    iput-object p4, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->sourcePosition:[I

    .line 547
    iput p5, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->sourceSize:I

    return-void
.end method

.method public setQuantity(I)V
    .locals 0

    .line 587
    iput p1, p0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->quantity:I

    return-void
.end method
