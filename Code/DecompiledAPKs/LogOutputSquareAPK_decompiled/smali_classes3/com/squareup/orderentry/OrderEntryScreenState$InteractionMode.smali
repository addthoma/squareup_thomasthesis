.class public final enum Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;
.super Ljava/lang/Enum;
.source "OrderEntryScreenState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/OrderEntryScreenState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InteractionMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

.field public static final enum EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

.field public static final enum SALE:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 426
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    const/4 v1, 0x0

    const-string v2, "EDIT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    .line 430
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    const/4 v2, 0x1

    const-string v3, "SALE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->SALE:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    .line 422
    sget-object v3, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->SALE:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->$VALUES:[Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 422
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;
    .locals 1

    .line 422
    const-class v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;
    .locals 1

    .line 422
    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->$VALUES:[Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    invoke-virtual {v0}, [Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    return-object v0
.end method
