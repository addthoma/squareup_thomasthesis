.class public Lcom/squareup/orderentry/CartDropDownPresenter;
.super Lmortar/ViewPresenter;
.source "CartDropDownPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/orderentry/CartDropDownView;",
        ">;"
    }
.end annotation


# instance fields
.field private dropDownListener:Lcom/squareup/ui/DropDownContainer$DropDownListener;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 15
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method public closeCart()V
    .locals 1

    .line 49
    invoke-virtual {p0}, Lcom/squareup/orderentry/CartDropDownPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 51
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/CartDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/CartDropDownView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CartDropDownView;->isDropDownVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    invoke-virtual {p0}, Lcom/squareup/orderentry/CartDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/CartDropDownView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CartDropDownView;->closeDropDown()V

    :cond_1
    return-void
.end method

.method public hideCart(Z)V
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/orderentry/CartDropDownPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/orderentry/CartDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/CartDropDownView;

    invoke-virtual {p1}, Lcom/squareup/orderentry/CartDropDownView;->closeDropDown()V

    goto :goto_0

    .line 38
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/orderentry/CartDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/CartDropDownView;

    invoke-virtual {p1}, Lcom/squareup/orderentry/CartDropDownView;->setDropDownClosed()V

    :goto_0
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .line 61
    invoke-virtual {p0}, Lcom/squareup/orderentry/CartDropDownPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 62
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/CartDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/CartDropDownView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CartDropDownView;->isDropDownVisible()Z

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .line 57
    invoke-virtual {p0}, Lcom/squareup/orderentry/CartDropDownPresenter;->hasView()Z

    move-result v0

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 19
    iget-object p1, p0, Lcom/squareup/orderentry/CartDropDownPresenter;->dropDownListener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    if-eqz p1, :cond_0

    .line 20
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/CartDropDownPresenter;->setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V

    :cond_0
    return-void
.end method

.method public setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/orderentry/CartDropDownPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    invoke-virtual {p0}, Lcom/squareup/orderentry/CartDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/CartDropDownView;

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/CartDropDownView;->setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V

    goto :goto_0

    .line 28
    :cond_0
    iput-object p1, p0, Lcom/squareup/orderentry/CartDropDownPresenter;->dropDownListener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    :goto_0
    return-void
.end method

.method public toggleCart()V
    .locals 1

    .line 43
    invoke-virtual {p0}, Lcom/squareup/orderentry/CartDropDownPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/CartDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/CartDropDownView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CartDropDownView;->toggleDropDown()V

    return-void
.end method
