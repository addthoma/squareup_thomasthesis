.class Lcom/squareup/orderentry/FlyByCoordinator$PulseViewRunnable;
.super Ljava/lang/Object;
.source "FlyByCoordinator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/FlyByCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PulseViewRunnable"
.end annotation


# instance fields
.field private final destinationView:Landroid/widget/TextView;

.field private final newQuantity:I

.field final synthetic this$0:Lcom/squareup/orderentry/FlyByCoordinator;


# direct methods
.method private constructor <init>(Lcom/squareup/orderentry/FlyByCoordinator;Landroid/widget/TextView;I)V
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/orderentry/FlyByCoordinator$PulseViewRunnable;->this$0:Lcom/squareup/orderentry/FlyByCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object p2, p0, Lcom/squareup/orderentry/FlyByCoordinator$PulseViewRunnable;->destinationView:Landroid/widget/TextView;

    .line 84
    iput p3, p0, Lcom/squareup/orderentry/FlyByCoordinator$PulseViewRunnable;->newQuantity:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/orderentry/FlyByCoordinator;Landroid/widget/TextView;ILcom/squareup/orderentry/FlyByCoordinator$1;)V
    .locals 0

    .line 78
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/orderentry/FlyByCoordinator$PulseViewRunnable;-><init>(Lcom/squareup/orderentry/FlyByCoordinator;Landroid/widget/TextView;I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator$PulseViewRunnable;->destinationView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 89
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator$PulseViewRunnable;->destinationView:Landroid/widget/TextView;

    iget v1, p0, Lcom/squareup/orderentry/FlyByCoordinator$PulseViewRunnable;->newQuantity:I

    invoke-static {v1, v0}, Lcom/squareup/orderentry/FlyByCoordinator;->access$300(ILandroid/widget/TextView;)Landroid/view/animation/AnimationSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/orderentry/FlyByCoordinator$PulseViewRunnable;->this$0:Lcom/squareup/orderentry/FlyByCoordinator;

    invoke-static {v0}, Lcom/squareup/orderentry/FlyByCoordinator;->access$400(Lcom/squareup/orderentry/FlyByCoordinator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method
