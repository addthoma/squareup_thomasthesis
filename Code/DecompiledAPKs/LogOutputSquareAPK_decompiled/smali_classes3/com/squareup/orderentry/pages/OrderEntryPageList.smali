.class public Lcom/squareup/orderentry/pages/OrderEntryPageList;
.super Ljava/lang/Object;
.source "OrderEntryPageList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final favoritesPages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orderentry/pages/OrderEntryPage;",
            ">;"
        }
    .end annotation
.end field

.field private final keypadPage:Lcom/squareup/orderentry/pages/OrderEntryPage;

.field private final libraryPage:Lcom/squareup/orderentry/pages/OrderEntryPage;

.field private final widePages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orderentry/pages/OrderEntryPage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/util/Device;Ljava/util/List;Ljava/util/List;Lcom/squareup/orderentry/pages/OrderEntryPage;Lcom/squareup/orderentry/pages/OrderEntryPage;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Device;",
            "Ljava/util/List<",
            "Lcom/squareup/orderentry/pages/OrderEntryPage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orderentry/pages/OrderEntryPage;",
            ">;",
            "Lcom/squareup/orderentry/pages/OrderEntryPage;",
            "Lcom/squareup/orderentry/pages/OrderEntryPage;",
            ")V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->device:Lcom/squareup/util/Device;

    .line 44
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->favoritesPages:Ljava/util/List;

    .line 45
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->widePages:Ljava/util/List;

    .line 46
    iput-object p4, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->keypadPage:Lcom/squareup/orderentry/pages/OrderEntryPage;

    .line 47
    iput-object p5, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->libraryPage:Lcom/squareup/orderentry/pages/OrderEntryPage;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/util/Device;Ljava/util/List;Ljava/util/List;Lcom/squareup/orderentry/pages/OrderEntryPage;Lcom/squareup/orderentry/pages/OrderEntryPage;Lcom/squareup/orderentry/pages/OrderEntryPageList$1;)V
    .locals 0

    .line 34
    invoke-direct/range {p0 .. p5}, Lcom/squareup/orderentry/pages/OrderEntryPageList;-><init>(Lcom/squareup/util/Device;Ljava/util/List;Ljava/util/List;Lcom/squareup/orderentry/pages/OrderEntryPage;Lcom/squareup/orderentry/pages/OrderEntryPage;)V

    return-void
.end method

.method private simpleIndexing()Z
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method


# virtual methods
.method public favoritesPageIdIndex(Ljava/lang/String;)I
    .locals 2

    const/4 v0, 0x0

    .line 81
    :goto_0
    iget-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->favoritesPages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 82
    iget-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->favoritesPages:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/orderentry/pages/OrderEntryPage;

    .line 83
    invoke-virtual {v1}, Lcom/squareup/orderentry/pages/OrderEntryPage;->getFavoritesPageId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x2

    return p1
.end method

.method public getPanel(I)Lcom/squareup/orderentry/pages/OrderEntryPage;
    .locals 1

    .line 52
    invoke-virtual {p0}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->keypadPanelIndex()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 53
    iget-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->keypadPage:Lcom/squareup/orderentry/pages/OrderEntryPage;

    return-object p1

    .line 56
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->libraryPanelIndex()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 57
    iget-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->libraryPage:Lcom/squareup/orderentry/pages/OrderEntryPage;

    return-object p1

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->favoritesPages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/pages/OrderEntryPage;

    return-object p1
.end method

.method public getTab(I)Lcom/squareup/orderentry/pages/OrderEntryPage;
    .locals 2

    .line 65
    invoke-direct {p0}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->simpleIndexing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->getPanel(I)Lcom/squareup/orderentry/pages/OrderEntryPage;

    move-result-object p1

    return-object p1

    .line 69
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->keypadTabIndex()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 70
    iget-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->keypadPage:Lcom/squareup/orderentry/pages/OrderEntryPage;

    return-object p1

    .line 73
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->favoritesPages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 74
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->favoritesPages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/pages/OrderEntryPage;

    return-object p1

    .line 77
    :cond_2
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->widePages:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->favoritesPages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr p1, v1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/pages/OrderEntryPage;

    return-object p1
.end method

.method public keypadPanelIndex()I
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->favoritesPages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public keypadTabIndex()I
    .locals 1

    .line 112
    invoke-direct {p0}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->simpleIndexing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->keypadPanelIndex()I

    move-result v0

    return v0

    .line 116
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->tabCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public libraryPanelIndex()I
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->favoritesPages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    return v0
.end method

.method public panelAtIndex(I)Lcom/squareup/orderentry/pages/OrderEntryPageKey;
    .locals 2

    .line 153
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    .line 154
    sget-object p1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    :goto_0
    return-object p1

    .line 157
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->favoritesPages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 158
    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ALL:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    return-object p1

    .line 161
    :cond_2
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->favoritesPages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr p1, v0

    .line 162
    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ALL:Ljava/util/List;

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {v1}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ordinal()I

    move-result v1

    add-int/2addr v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    return-object p1
.end method

.method public panelCount()I
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->favoritesPages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public panelIndex(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)I
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    return p1

    .line 125
    :cond_1
    iget-boolean v0, p1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->isFavoritesGrid:Z

    if-eqz v0, :cond_2

    .line 126
    invoke-virtual {p1}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ordinal()I

    move-result p1

    return p1

    .line 130
    :cond_2
    iget-boolean v0, p1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->isInLibrary:Z

    if-eqz v0, :cond_3

    .line 131
    sget-object p1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 136
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ordinal()I

    move-result p1

    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {v0}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ordinal()I

    move-result v0

    sub-int/2addr p1, v0

    .line 137
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->favoritesPages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, p1

    return v0
.end method

.method public tabCount()I
    .locals 2

    .line 96
    invoke-direct {p0}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->simpleIndexing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->panelCount()I

    move-result v0

    return v0

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->favoritesPages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->widePages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public tabForPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;Lcom/squareup/librarylist/LibraryListState$Filter;)Lcom/squareup/orderentry/pages/OrderEntryPageKey;
    .locals 1

    .line 145
    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPageList;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 149
    :cond_0
    invoke-static {p2}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->libraryFilterToTab(Lcom/squareup/librarylist/LibraryListState$Filter;)Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    move-result-object p1

    :cond_1
    :goto_0
    return-object p1
.end method
