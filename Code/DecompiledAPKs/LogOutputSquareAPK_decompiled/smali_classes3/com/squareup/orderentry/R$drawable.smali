.class public final Lcom/squareup/orderentry/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final flyby_shadow:I = 0x7f0801e9

.field public static final order_entry_badge_background:I = 0x7f080416

.field public static final order_entry_badge_background_fatal:I = 0x7f080417

.field public static final order_entry_badge_background_red:I = 0x7f080418

.field public static final order_entry_wide_badge_background:I = 0x7f080419

.field public static final order_entry_wide_badge_background_fatal:I = 0x7f08041a

.field public static final order_entry_wide_badge_background_red:I = 0x7f08041b

.field public static final panel_background_clear_bottom_border:I = 0x7f080428

.field public static final payment_pad_landscape_background_edit:I = 0x7f08043a

.field public static final payment_pad_landscape_background_sale:I = 0x7f08043b

.field public static final payment_pad_portrait_background_edit:I = 0x7f08043c

.field public static final payment_pad_portrait_background_sale:I = 0x7f08043d

.field public static final payment_pad_portrait_background_sale_no_border:I = 0x7f08043e

.field public static final selector_order_entry_tab_keypad:I = 0x7f080488

.field public static final selector_order_entry_tab_library:I = 0x7f080489

.field public static final selector_payment_pad_button:I = 0x7f08048e

.field public static final selector_payment_pad_button_charge:I = 0x7f08048f

.field public static final selector_payment_pad_button_ticket:I = 0x7f080490

.field public static final selector_payment_pad_button_ticket_save:I = 0x7f080491

.field public static final selector_payment_pad_button_ticket_tickets:I = 0x7f080492

.field public static final tooltip_center_down_arrow:I = 0x7f0804c4

.field public static final tooltip_center_down_arrow_dark:I = 0x7f0804c5


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
