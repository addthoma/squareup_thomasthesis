.class public Lcom/squareup/orderentry/CategoryDropDownPresenter;
.super Lmortar/ViewPresenter;
.source "CategoryDropDownPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/orderentry/CategoryDropDownView;",
        ">;"
    }
.end annotation


# instance fields
.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final dropDownIsOpen:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;

.field private final libraryIsEmpty:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

.field private queryResult:Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;

.field private scopeExited:Z

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/badbus/BadBus;Ljavax/inject/Provider;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            "Lcom/squareup/badbus/BadBus;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lflow/Flow;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 47
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const/4 v0, 0x0

    .line 35
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->dropDownIsOpen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 36
    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->libraryIsEmpty:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 48
    iput-object p1, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    .line 49
    iput-object p2, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->bus:Lcom/squareup/badbus/BadBus;

    .line 50
    iput-object p3, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->cogsProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p4, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->flow:Lflow/Flow;

    .line 52
    iput-object p5, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/CategoryDropDownPresenter;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->dropDownIsOpen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method private loadCursor()V
    .locals 3

    .line 61
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$CategoryDropDownPresenter$uX_IlvVCjPFdau2cSqjkv5J5ff8;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$CategoryDropDownPresenter$uX_IlvVCjPFdau2cSqjkv5J5ff8;-><init>(Lcom/squareup/orderentry/CategoryDropDownPresenter;)V

    new-instance v2, Lcom/squareup/orderentry/-$$Lambda$CategoryDropDownPresenter$zRxVCn9vS8MfoX-eSosoW_RrzDk;

    invoke-direct {v2, p0}, Lcom/squareup/orderentry/-$$Lambda$CategoryDropDownPresenter$zRxVCn9vS8MfoX-eSosoW_RrzDk;-><init>(Lcom/squareup/orderentry/CategoryDropDownPresenter;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method private updateView(Lcom/squareup/orderentry/CategoryDropDownView;)V
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->queryResult:Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, v0, Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;->categoryCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    iget-object v1, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    .line 139
    invoke-interface {v1}, Lcom/squareup/librarylist/LibraryListStateManager;->buildCategoryPlaceholders()Ljava/util/List;

    move-result-object v1

    .line 138
    invoke-virtual {p1, v0, v1}, Lcom/squareup/orderentry/CategoryDropDownView;->updateRows(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/List;)V

    :cond_0
    return-void
.end method


# virtual methods
.method categoryClicked(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V
    .locals 1

    .line 127
    invoke-virtual {p0}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/CategoryDropDownView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CategoryDropDownView;->closeDropDown()V

    .line 128
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {v0, p1}, Lcom/squareup/librarylist/LibraryListStateManager;->setModeToSingleCategory(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    return-void
.end method

.method public dropDownIsOpen()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 110
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->dropDownIsOpen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public isDropDownOpen()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 123
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->dropDownIsOpen:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$loadCursor$1$CategoryDropDownPresenter(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;
    .locals 2

    .line 62
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 63
    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 64
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/squareup/api/items/Item$Type;

    .line 65
    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v0

    .line 64
    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllUsedCategoriesAndEmpty(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$loadCursor$2$CategoryDropDownPresenter(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 2

    .line 67
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;

    .line 68
    iget-boolean v0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->scopeExited:Z

    if-eqz v0, :cond_0

    .line 69
    iget-object p1, p1, Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;->categoryCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    return-void

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->queryResult:Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;

    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;->isLibraryEmpty:Z

    iget-boolean v1, p1, Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;->isLibraryEmpty:Z

    if-eq v0, v1, :cond_2

    .line 73
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->libraryIsEmpty:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-boolean v1, p1, Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;->isLibraryEmpty:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 75
    :cond_2
    iput-object p1, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->queryResult:Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;

    .line 77
    invoke-virtual {p0}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 78
    invoke-virtual {p0}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/CategoryDropDownView;

    invoke-direct {p0, p1}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->updateView(Lcom/squareup/orderentry/CategoryDropDownView;)V

    :cond_3
    return-void
.end method

.method public synthetic lambda$null$3$CategoryDropDownPresenter(Lcom/squareup/orderentry/CategoryDropDownView;Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 88
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->updateView(Lcom/squareup/orderentry/CategoryDropDownView;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$CategoryDropDownPresenter(Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->loadCursor()V

    return-void
.end method

.method public synthetic lambda$onLoad$4$CategoryDropDownPresenter(Lcom/squareup/orderentry/CategoryDropDownView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$CategoryDropDownPresenter$7Gbgpj6St_cugE6-MBt-5DAdFiQ;

    invoke-direct {v1, p0, p1}, Lcom/squareup/orderentry/-$$Lambda$CategoryDropDownPresenter$7Gbgpj6St_cugE6-MBt-5DAdFiQ;-><init>(Lcom/squareup/orderentry/CategoryDropDownPresenter;Lcom/squareup/orderentry/CategoryDropDownView;)V

    .line 88
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public libraryIsEmpty()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 114
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->libraryIsEmpty:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/cogs/CatalogUpdateEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$CategoryDropDownPresenter$1i_nnFpslrRAy8EHV6Rd9ZFAtuM;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$CategoryDropDownPresenter$1i_nnFpslrRAy8EHV6Rd9ZFAtuM;-><init>(Lcom/squareup/orderentry/CategoryDropDownPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 57
    invoke-direct {p0}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->loadCursor()V

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    const/4 v0, 0x1

    .line 103
    iput-boolean v0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->scopeExited:Z

    .line 104
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->queryResult:Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;->categoryCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->queryResult:Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;

    iget-object v0, v0, Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;->categoryCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    :cond_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 84
    invoke-virtual {p0}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/CategoryDropDownView;

    .line 86
    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$CategoryDropDownPresenter$ry07PyXHaHs2567-0I05b-1VwKc;

    invoke-direct {v0, p0, p1}, Lcom/squareup/orderentry/-$$Lambda$CategoryDropDownPresenter$ry07PyXHaHs2567-0I05b-1VwKc;-><init>(Lcom/squareup/orderentry/CategoryDropDownPresenter;Lcom/squareup/orderentry/CategoryDropDownView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 90
    new-instance v0, Lcom/squareup/orderentry/CategoryDropDownPresenter$1;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/CategoryDropDownPresenter$1;-><init>(Lcom/squareup/orderentry/CategoryDropDownPresenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/CategoryDropDownView;->setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V

    return-void
.end method

.method placeholderClicked(Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;)V
    .locals 2

    .line 132
    invoke-virtual {p0}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/CategoryDropDownView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CategoryDropDownView;->closeDropDown()V

    .line 133
    iget-object v0, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->libraryState:Lcom/squareup/librarylist/LibraryListStateManager;

    iget-object v1, p0, Lcom/squareup/orderentry/CategoryDropDownPresenter;->flow:Lflow/Flow;

    invoke-interface {v0, p1, v1}, Lcom/squareup/librarylist/LibraryListStateManager;->placeholderClicked(Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;Lflow/Flow;)V

    return-void
.end method

.method public toggle()V
    .locals 1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/orderentry/CategoryDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/CategoryDropDownView;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CategoryDropDownView;->toggleDropDown()V

    return-void
.end method
