.class Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$4;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PaymentPadTabletLandscapeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->animateCurrentTicketContentsAway()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)V
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$4;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .line 175
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$4;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    invoke-static {p1}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->access$100(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)Landroid/view/View;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 176
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$4;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    invoke-static {p1}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->access$200(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)Landroid/widget/ImageView;

    move-result-object p1

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 177
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$4;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    invoke-static {p1}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->access$200(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    .line 169
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$4;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    invoke-static {p1}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->access$100(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)Landroid/view/View;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 170
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView$4;->this$0:Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;

    invoke-static {p1}, Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;->access$200(Lcom/squareup/orderentry/PaymentPadTabletLandscapeView;)Landroid/widget/ImageView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method
