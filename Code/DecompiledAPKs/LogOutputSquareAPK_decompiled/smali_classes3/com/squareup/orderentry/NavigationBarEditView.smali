.class public Lcom/squareup/orderentry/NavigationBarEditView;
.super Lcom/squareup/orderentry/NavigationBarView;
.source "NavigationBarEditView.java"


# instance fields
.field presenter:Lcom/squareup/orderentry/NavigationBarEditPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2}, Lcom/squareup/orderentry/NavigationBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    const-class p2, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-interface {p1, p0}, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;->inject(Lcom/squareup/orderentry/NavigationBarEditView;)V

    return-void
.end method


# virtual methods
.method protected getNarrowTabGlyphLayout()I
    .locals 1

    .line 31
    sget v0, Lcom/squareup/orderentry/R$layout;->order_entry_actionbar_edit_tab_glyph:I

    return v0
.end method

.method protected getNarrowTabTextLayout()I
    .locals 1

    .line 35
    sget v0, Lcom/squareup/orderentry/R$layout;->order_entry_actionbar_edit_tab_text:I

    return v0
.end method

.method protected bridge synthetic getPresenter()Lcom/squareup/orderentry/NavigationBarAbstractPresenter;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarEditView;->getPresenter()Lcom/squareup/orderentry/NavigationBarEditPresenter;

    move-result-object v0

    return-object v0
.end method

.method protected getPresenter()Lcom/squareup/orderentry/NavigationBarEditPresenter;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarEditView;->presenter:Lcom/squareup/orderentry/NavigationBarEditPresenter;

    return-object v0
.end method

.method protected getWideTabLayout()I
    .locals 1

    .line 27
    sget v0, Lcom/squareup/orderentry/R$layout;->order_entry_actionbar_tab_text_wide:I

    return v0
.end method

.method protected isEditor()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
