.class public abstract Lcom/squareup/orderentry/RealOrderEntryAppletGateway;
.super Ljava/lang/Object;
.source "RealOrderEntryAppletGateway.kt"

# interfaces
.implements Lcom/squareup/orderentry/OrderEntryAppletGateway;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsHomeApplet;,
        Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsNonHomeApplet;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0014\u0015B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\nH\u0016J\u0008\u0010\u000b\u001a\u00020\nH\u0016J\u0012\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0016J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0004J\u0008\u0010\u0013\u001a\u00020\u0008H\u0016R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0002\u0016\u0017\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/orderentry/RealOrderEntryAppletGateway;",
        "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
        "()V",
        "orderEntryApplet",
        "Lcom/squareup/orderentry/OrderEntryApplet;",
        "getOrderEntryApplet",
        "()Lcom/squareup/orderentry/OrderEntryApplet;",
        "activateApplet",
        "",
        "hasFavoritesEditor",
        "",
        "hasOrderEntryApplet",
        "historyForFavoritesEditor",
        "Lflow/History;",
        "currentHistory",
        "orderEntryScreenForMode",
        "Lcom/squareup/container/ContainerTreeKey;",
        "mode",
        "Lcom/squareup/orderentry/OrderEntryMode;",
        "selectApplet",
        "AsHomeApplet",
        "AsNonHomeApplet",
        "Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsHomeApplet;",
        "Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsNonHomeApplet;",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/squareup/orderentry/RealOrderEntryAppletGateway;-><init>()V

    return-void
.end method


# virtual methods
.method public activateApplet()V
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/orderentry/RealOrderEntryAppletGateway;->getOrderEntryApplet()Lcom/squareup/orderentry/OrderEntryApplet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryApplet;->activate()V

    return-void
.end method

.method public abstract getOrderEntryApplet()Lcom/squareup/orderentry/OrderEntryApplet;
.end method

.method public hasFavoritesEditor()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public hasOrderEntryApplet()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public historyForFavoritesEditor(Lflow/History;)Lflow/History;
    .locals 1

    if-eqz p1, :cond_0

    .line 33
    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lflow/History;->emptyBuilder()Lflow/History$Builder;

    move-result-object p1

    .line 34
    :goto_0
    sget-object v0, Lcom/squareup/orderentry/OrderEntryMode;->EDIT_FAVORITES_FROM_ITEMS_APPLET:Lcom/squareup/orderentry/OrderEntryMode;

    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/RealOrderEntryAppletGateway;->orderEntryScreenForMode(Lcom/squareup/orderentry/OrderEntryMode;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-virtual {p1, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p1

    .line 35
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    const-string v0, "(currentHistory?.buildUp\u2026APPLET))\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected final orderEntryScreenForMode(Lcom/squareup/orderentry/OrderEntryMode;)Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    const-string v0, "mode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    sget-object v0, Lcom/squareup/orderentry/RealOrderEntryAppletGateway$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/orderentry/OrderEntryMode;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 42
    sget-object p1, Lcom/squareup/orderentry/OrderEntryScreen;->EDIT_FAVORITES_FROM_ITEMS_APPLET:Lcom/squareup/orderentry/OrderEntryScreen;

    const-string v0, "OrderEntryScreen.EDIT_FAVORITES_FROM_ITEMS_APPLET"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 41
    :cond_1
    sget-object p1, Lcom/squareup/orderentry/OrderEntryScreen;->KEYPAD:Lcom/squareup/orderentry/OrderEntryScreen;

    const-string v0, "OrderEntryScreen.KEYPAD"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    goto :goto_0

    .line 40
    :cond_2
    sget-object p1, Lcom/squareup/orderentry/OrderEntryScreen;->FAVORITES:Lcom/squareup/orderentry/OrderEntryScreen;

    const-string v0, "OrderEntryScreen.FAVORITES"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    goto :goto_0

    .line 39
    :cond_3
    sget-object p1, Lcom/squareup/orderentry/OrderEntryScreen;->LAST_SELECTED:Lcom/squareup/orderentry/OrderEntryScreen;

    const-string v0, "OrderEntryScreen.LAST_SELECTED"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    :goto_0
    return-object p1
.end method

.method public selectApplet()V
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/orderentry/RealOrderEntryAppletGateway;->getOrderEntryApplet()Lcom/squareup/orderentry/OrderEntryApplet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryApplet;->select()V

    return-void
.end method
