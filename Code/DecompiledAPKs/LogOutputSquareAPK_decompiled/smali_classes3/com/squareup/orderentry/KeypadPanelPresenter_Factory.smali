.class public final Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;
.super Ljava/lang/Object;
.source "KeypadPanelPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/KeypadPanelPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final addNoteScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AddNoteScreenRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final flyBySourceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FlyBySource;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final quickAmountsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionInteractionsLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field

.field private final vibratorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/os/Vibrator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AddNoteScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FlyBySource;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/os/Vibrator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p2, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->addNoteScreenRunnerProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p3, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->busProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p4, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p5, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p6, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p7, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->flyBySourceProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p8, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->vibratorProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p9, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p10, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p11, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p12, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->quickAmountsSettingsProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p13, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p14, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AddNoteScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FlyBySource;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/os/Vibrator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;"
        }
    .end annotation

    .line 97
    new-instance v15, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v15
.end method

.method public static newInstance(Lcom/squareup/ui/seller/SellerScopeRunner;Lcom/squareup/ui/main/AddNoteScreenRunner;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/orderentry/FlyBySource;Landroid/os/Vibrator;Lcom/squareup/util/Device;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/analytics/Analytics;)Lcom/squareup/orderentry/KeypadPanelPresenter;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            "Lcom/squareup/ui/main/AddNoteScreenRunner;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/orderentry/FlyBySource;",
            "Landroid/os/Vibrator;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/analytics/Analytics;",
            ")",
            "Lcom/squareup/orderentry/KeypadPanelPresenter;"
        }
    .end annotation

    .line 106
    new-instance v15, Lcom/squareup/orderentry/KeypadPanelPresenter;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/orderentry/KeypadPanelPresenter;-><init>(Lcom/squareup/ui/seller/SellerScopeRunner;Lcom/squareup/ui/main/AddNoteScreenRunner;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/orderentry/FlyBySource;Landroid/os/Vibrator;Lcom/squareup/util/Device;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/analytics/Analytics;)V

    return-object v15
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/KeypadPanelPresenter;
    .locals 15

    .line 85
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/seller/SellerScopeRunner;

    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->addNoteScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/main/AddNoteScreenRunner;

    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/badbus/BadBus;

    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->flyBySourceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/orderentry/FlyBySource;

    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->vibratorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/os/Vibrator;

    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/log/cart/TransactionInteractionsLogger;

    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->quickAmountsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/quickamounts/QuickAmountsSettings;

    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Lcom/squareup/analytics/Analytics;

    invoke-static/range {v1 .. v14}, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->newInstance(Lcom/squareup/ui/seller/SellerScopeRunner;Lcom/squareup/ui/main/AddNoteScreenRunner;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/orderentry/FlyBySource;Landroid/os/Vibrator;Lcom/squareup/util/Device;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/analytics/Analytics;)Lcom/squareup/orderentry/KeypadPanelPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter_Factory;->get()Lcom/squareup/orderentry/KeypadPanelPresenter;

    move-result-object v0

    return-object v0
.end method
