.class public final Lcom/squareup/orderentry/OrderEntryTutorialValues;
.super Ljava/lang/Object;
.source "OrderEntryTutorialEvents.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/orderentry/OrderEntryTutorialValues;",
        "",
        "()V",
        "HOME_OBSCURED",
        "",
        "HOME_VISIBLE",
        "order-entry-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final HOME_OBSCURED:Ljava/lang/String; = "Home not visible"

.field public static final HOME_VISIBLE:Ljava/lang/String; = "Home visible"

.field public static final INSTANCE:Lcom/squareup/orderentry/OrderEntryTutorialValues;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/orderentry/OrderEntryTutorialValues;

    invoke-direct {v0}, Lcom/squareup/orderentry/OrderEntryTutorialValues;-><init>()V

    sput-object v0, Lcom/squareup/orderentry/OrderEntryTutorialValues;->INSTANCE:Lcom/squareup/orderentry/OrderEntryTutorialValues;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
