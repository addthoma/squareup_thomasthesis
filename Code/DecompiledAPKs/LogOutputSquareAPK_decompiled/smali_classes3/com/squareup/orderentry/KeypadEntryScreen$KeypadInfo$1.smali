.class final Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$1;
.super Ljava/lang/Object;
.source "KeypadEntryScreen.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;
    .locals 6

    .line 187
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->valueOf(Ljava/lang/String;)Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    move-result-object v0

    .line 188
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 189
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 190
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 191
    sget-object v4, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->MONEY:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    if-ne v0, v4, :cond_0

    .line 192
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    .line 193
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/protos/common/CurrencyCode;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    .line 194
    new-instance v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    invoke-static {v4, v5, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 197
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v3

    .line 198
    new-instance p1, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    invoke-static {v3, v4}, Lcom/squareup/util/Percentage;->fromDouble(D)Lcom/squareup/util/Percentage;

    move-result-object v0

    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;-><init>(Lcom/squareup/util/Percentage;Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 185
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;
    .locals 0

    .line 203
    new-array p1, p1, [Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 185
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$1;->newArray(I)[Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    move-result-object p1

    return-object p1
.end method
