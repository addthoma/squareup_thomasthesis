.class Lcom/squareup/orderentry/FavoritePageView$5;
.super Lcom/squareup/orderentry/FavoritePageView$EnabledClickListener;
.source "FavoritePageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/FavoritePageView;->createMenuCategoryView(Lcom/squareup/shared/catalog/models/CatalogItemCategory;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/FavoritePageView;

.field final synthetic val$menuCategory:Lcom/squareup/shared/catalog/models/CatalogItemCategory;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/FavoritePageView;Lcom/squareup/shared/catalog/models/CatalogItemCategory;)V
    .locals 0

    .line 417
    iput-object p1, p0, Lcom/squareup/orderentry/FavoritePageView$5;->this$0:Lcom/squareup/orderentry/FavoritePageView;

    iput-object p2, p0, Lcom/squareup/orderentry/FavoritePageView$5;->val$menuCategory:Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/squareup/orderentry/FavoritePageView$EnabledClickListener;-><init>(Lcom/squareup/orderentry/FavoritePageView$1;)V

    return-void
.end method


# virtual methods
.method public click(Landroid/view/View;)V
    .locals 1

    .line 419
    iget-object p1, p0, Lcom/squareup/orderentry/FavoritePageView$5;->this$0:Lcom/squareup/orderentry/FavoritePageView;

    iget-object p1, p1, Lcom/squareup/orderentry/FavoritePageView;->presenter:Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView$5;->val$menuCategory:Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->onCategoryClicked(Lcom/squareup/shared/catalog/models/CatalogItemCategory;)V

    return-void
.end method
