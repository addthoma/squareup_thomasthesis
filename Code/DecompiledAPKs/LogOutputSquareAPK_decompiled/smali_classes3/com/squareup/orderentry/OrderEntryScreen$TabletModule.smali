.class abstract Lcom/squareup/orderentry/OrderEntryScreen$TabletModule;
.super Ljava/lang/Object;
.source "OrderEntryScreen.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/orderentry/OrderEntryScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/OrderEntryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "TabletModule"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 544
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindCartScreenFinisher(Lcom/squareup/ui/cart/CartScreenFinisher$Tablet;)Lcom/squareup/ui/cart/CartScreenFinisher;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideHomeScreenBackHandler(Lcom/squareup/orderentry/OrderEntryScreenBackHandler$Tablet;)Lcom/squareup/orderentry/OrderEntryScreenBackHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
