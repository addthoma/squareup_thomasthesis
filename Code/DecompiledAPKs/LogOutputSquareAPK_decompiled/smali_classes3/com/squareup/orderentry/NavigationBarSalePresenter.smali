.class public Lcom/squareup/orderentry/NavigationBarSalePresenter;
.super Lcom/squareup/orderentry/NavigationBarAbstractPresenter;
.source "NavigationBarSalePresenter.java"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

.field private final flow:Lflow/Flow;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;


# direct methods
.method public constructor <init>(Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/orderentry/CartDropDownPresenter;Lcom/squareup/permissions/PermissionGatekeeper;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/librarylist/LibraryListStateManager;)V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v7, p0

    move-object v0, p0

    move-object/from16 v1, p8

    move-object v2, p1

    move-object v3, p6

    move-object/from16 v4, p10

    move-object v5, p7

    move-object/from16 v6, p9

    .line 38
    invoke-direct/range {v0 .. v6}, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;-><init>(Lcom/squareup/util/Device;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/analytics/Analytics;Lcom/squareup/tutorialv2/TutorialCore;)V

    move-object v0, p7

    .line 39
    iput-object v0, v7, Lcom/squareup/orderentry/NavigationBarSalePresenter;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v0, p2

    .line 40
    iput-object v0, v7, Lcom/squareup/orderentry/NavigationBarSalePresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    move-object v0, p3

    .line 41
    iput-object v0, v7, Lcom/squareup/orderentry/NavigationBarSalePresenter;->cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

    move-object v0, p4

    .line 42
    iput-object v0, v7, Lcom/squareup/orderentry/NavigationBarSalePresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    move-object v0, p5

    .line 43
    iput-object v0, v7, Lcom/squareup/orderentry/NavigationBarSalePresenter;->flow:Lflow/Flow;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/NavigationBarSalePresenter;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/orderentry/NavigationBarSalePresenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/NavigationBarSalePresenter;)Lcom/squareup/orderentry/OrderEntryScreenState;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/orderentry/NavigationBarSalePresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/orderentry/NavigationBarSalePresenter;)Lflow/Flow;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/orderentry/NavigationBarSalePresenter;->flow:Lflow/Flow;

    return-object p0
.end method


# virtual methods
.method public tabLongClicked(Lcom/squareup/orderentry/pages/OrderEntryPage;)V
    .locals 3

    .line 54
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarSalePresenter;->getCurrentPage()Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->isFavoritesGrid:Z

    if-nez v0, :cond_0

    .line 55
    invoke-virtual {p1}, Lcom/squareup/orderentry/pages/OrderEntryPage;->getKey()Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/NavigationBarSalePresenter;->tabSelected(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarSalePresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->EDIT_ITEMS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/orderentry/NavigationBarSalePresenter$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/orderentry/NavigationBarSalePresenter$1;-><init>(Lcom/squareup/orderentry/NavigationBarSalePresenter;Lcom/squareup/orderentry/pages/OrderEntryPage;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method tabSelected(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarSalePresenter;->cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/CartDropDownPresenter;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarSalePresenter;->cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/CartDropDownPresenter;->hideCart(Z)V

    .line 50
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->tabSelected(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    return-void
.end method
