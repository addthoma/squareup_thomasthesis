.class final enum Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;
.super Ljava/lang/Enum;
.source "ChargeAndTicketButtonsPresenter.java"

# interfaces
.implements Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ButtonState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "ChargeState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;",
        ">;",
        "Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ButtonState;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

.field public static final enum CHARGE:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

.field public static final enum CONFIRMABLE:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

.field public static final enum CONFIRMING:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

.field public static final enum DISABLED:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;


# instance fields
.field final phoneBackgroundSelectorId:I

.field final phoneTextColorId:I

.field final tabletBackgroundSelectorId:I

.field final tabletTextColorId:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 120
    new-instance v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    sget v1, Lcom/squareup/marin/R$color;->marin_white:I

    sget v2, Lcom/squareup/marin/R$drawable;->marin_selector_blue:I

    const/4 v3, 0x0

    const-string v4, "CHARGE"

    invoke-direct {v0, v4, v3, v1, v2}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->CHARGE:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    .line 123
    new-instance v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget v2, Lcom/squareup/orderentry/R$drawable;->selector_payment_pad_button:I

    const/4 v4, 0x1

    const-string v5, "CONFIRMABLE"

    invoke-direct {v0, v5, v4, v1, v2}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->CONFIRMABLE:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    .line 125
    new-instance v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    sget v1, Lcom/squareup/marin/R$color;->marin_white:I

    sget v2, Lcom/squareup/marin/R$drawable;->marin_selector_blue:I

    const/4 v5, 0x2

    const-string v6, "CONFIRMING"

    invoke-direct {v0, v6, v5, v1, v2}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->CONFIRMING:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    .line 128
    new-instance v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    sget v1, Lcom/squareup/marin/R$color;->marin_light_gray:I

    sget v2, Lcom/squareup/orderentry/R$drawable;->selector_payment_pad_button:I

    const/4 v6, 0x3

    const-string v7, "DISABLED"

    invoke-direct {v0, v7, v6, v1, v2}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->DISABLED:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    .line 118
    sget-object v1, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->CHARGE:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->CONFIRMABLE:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->CONFIRMING:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->DISABLED:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->$VALUES:[Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 135
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 136
    iput p3, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->tabletTextColorId:I

    .line 137
    iput p4, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->tabletBackgroundSelectorId:I

    .line 139
    sget p1, Lcom/squareup/marin/R$color;->marin_text_selector_white_disabled_white_translucent:I

    iput p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->phoneTextColorId:I

    .line 141
    sget p1, Lcom/squareup/orderentry/R$drawable;->selector_payment_pad_button_charge:I

    iput p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->phoneBackgroundSelectorId:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;
    .locals 1

    .line 118
    const-class v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;
    .locals 1

    .line 118
    sget-object v0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->$VALUES:[Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    invoke-virtual {v0}, [Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    return-object v0
.end method


# virtual methods
.method public getBackgroundId(Z)I
    .locals 0

    if-eqz p1, :cond_0

    .line 151
    iget p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->tabletBackgroundSelectorId:I

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->phoneBackgroundSelectorId:I

    :goto_0
    return p1
.end method

.method public getTextColorId(Z)I
    .locals 0

    if-eqz p1, :cond_0

    .line 145
    iget p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->tabletTextColorId:I

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->phoneTextColorId:I

    :goto_0
    return p1
.end method
