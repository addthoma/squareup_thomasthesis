.class Lcom/squareup/configure/item/ConfigureItemDetailView$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "ConfigureItemDetailView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/configure/item/ConfigureItemDetailView;->buildTitleForCustomAmount(Ljava/lang/CharSequence;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/configure/item/ConfigureItemDetailView;


# direct methods
.method constructor <init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailView;

    iget-object v0, v0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onEditingItemTitle(Ljava/lang/String;)V

    return-void
.end method
