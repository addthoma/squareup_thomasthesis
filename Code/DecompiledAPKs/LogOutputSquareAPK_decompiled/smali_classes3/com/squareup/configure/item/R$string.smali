.class public final Lcom/squareup/configure/item/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final add:I = 0x7f12006c

.field public static final add_item_note:I = 0x7f120081

.field public static final add_item_price:I = 0x7f120082

.field public static final add_processing_time:I = 0x7f120093

.field public static final cart_item_price:I = 0x7f12034a

.field public static final cart_remove_comp_item:I = 0x7f120351

.field public static final cart_remove_comp_item_confirm:I = 0x7f120352

.field public static final cart_remove_item:I = 0x7f120353

.field public static final cart_remove_item_confirm:I = 0x7f120354

.field public static final cart_remove_service:I = 0x7f120355

.field public static final cart_remove_service_confirm:I = 0x7f120356

.field public static final comp_initial:I = 0x7f12045c

.field public static final comp_item:I = 0x7f12045d

.field public static final comp_uppercase_reason:I = 0x7f120462

.field public static final conditional_taxes_help_text_for_cart:I = 0x7f120473

.field public static final configure_item_detail_override_price_title:I = 0x7f120475

.field public static final configure_item_option_section_header:I = 0x7f120476

.field public static final configure_item_price_required_dialog_message:I = 0x7f120477

.field public static final configure_item_price_required_dialog_title:I = 0x7f120478

.field public static final configure_item_summary:I = 0x7f120479

.field public static final configure_item_summary_quantity:I = 0x7f12047a

.field public static final dashboard:I = 0x7f1207c0

.field public static final dashboard_taxes_url:I = 0x7f1207c6

.field public static final duration:I = 0x7f1208f5

.field public static final final_time:I = 0x7f120abd

.field public static final gift_card_activation_failed:I = 0x7f120afd

.field public static final gift_card_purchase_limit_exceeded_message:I = 0x7f120b23

.field public static final initial_time:I = 0x7f120bf6

.field public static final item_library_variable_price:I = 0x7f120e37

.field public static final item_variation_price_hint_unfocused:I = 0x7f120e43

.field public static final percent:I = 0x7f12142e

.field public static final price:I = 0x7f121498

.field public static final processing_time:I = 0x7f1214f8

.field public static final quantity_footer_adjust_weight_on_scale:I = 0x7f1214fc

.field public static final quantity_footer_error:I = 0x7f1214fd

.field public static final quantity_footer_incompatible_unit:I = 0x7f1214fe

.field public static final quantity_footer_manual_weight_entry:I = 0x7f1214ff

.field public static final quantity_footer_no_weight_reading:I = 0x7f121500

.field public static final search_library_hint_all:I = 0x7f12178d

.field public static final search_library_hint_all_items:I = 0x7f12178e

.field public static final set_price:I = 0x7f1217cf

.field public static final title_hint:I = 0x7f1219d3

.field public static final uppercase_cart_dining_option_header:I = 0x7f121b0e

.field public static final uppercase_cart_discount_header:I = 0x7f121b0f

.field public static final uppercase_cart_item_description_header:I = 0x7f121b10

.field public static final uppercase_cart_notes_header:I = 0x7f121b11

.field public static final uppercase_cart_quantity_header:I = 0x7f121b12

.field public static final uppercase_cart_quantity_header_area:I = 0x7f121b13

.field public static final uppercase_cart_quantity_header_length:I = 0x7f121b14

.field public static final uppercase_cart_quantity_header_volume:I = 0x7f121b15

.field public static final uppercase_cart_quantity_header_weight:I = 0x7f121b16

.field public static final uppercase_cart_taxes_header:I = 0x7f121b17

.field public static final uppercase_cart_variable_price_header:I = 0x7f121b18

.field public static final uppercase_choose_between_counts:I = 0x7f121b1a

.field public static final uppercase_choose_count:I = 0x7f121b1b

.field public static final uppercase_choose_many:I = 0x7f121b1d

.field public static final uppercase_choose_one:I = 0x7f121b1e

.field public static final uppercase_choose_up_to_count:I = 0x7f121b1f

.field public static final uppercase_choose_up_to_one:I = 0x7f121b20

.field public static final void_confirm:I = 0x7f121bb2

.field public static final void_initial:I = 0x7f121bb3

.field public static final void_item:I = 0x7f121bb4

.field public static final void_ticket_help_text:I = 0x7f121bb8

.field public static final void_uppercase_reason:I = 0x7f121bbb


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
