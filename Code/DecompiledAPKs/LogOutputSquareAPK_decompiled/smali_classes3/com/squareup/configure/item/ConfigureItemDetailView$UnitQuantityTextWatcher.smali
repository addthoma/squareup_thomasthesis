.class Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "ConfigureItemDetailView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/ConfigureItemDetailView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UnitQuantityTextWatcher"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher$OnUnitQuantityChangedListener;
    }
.end annotation


# instance fields
.field private final invalidCharacters:Ljava/util/regex/Pattern;

.field private final quantityListener:Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher$OnUnitQuantityChangedListener;

.field private final quantityPrecision:I


# direct methods
.method constructor <init>(CILcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher$OnUnitQuantityChangedListener;)V
    .locals 2

    .line 799
    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    .line 800
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[^0-9^"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher;->invalidCharacters:Ljava/util/regex/Pattern;

    .line 801
    iput p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher;->quantityPrecision:I

    .line 802
    iput-object p3, p0, Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher;->quantityListener:Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher$OnUnitQuantityChangedListener;

    return-void
.end method

.method private toBigDecimal(Landroid/text/Editable;)Ljava/math/BigDecimal;
    .locals 1

    .line 811
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher;->invalidCharacters:Ljava/util/regex/Pattern;

    invoke-static {p1, v0}, Lcom/squareup/util/Strings;->removePattern(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Numbers;->parseDecimal(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Optional;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 812
    invoke-virtual {p1, v0}, Lcom/squareup/util/Optional;->orElse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/math/BigDecimal;

    return-object p1
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 806
    invoke-direct {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher;->toBigDecimal(Landroid/text/Editable;)Ljava/math/BigDecimal;

    move-result-object p1

    .line 807
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher;->quantityListener:Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher$OnUnitQuantityChangedListener;

    iget v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher;->quantityPrecision:I

    invoke-static {p1, v1}, Lcom/squareup/quantity/ItemQuantities;->applyQuantityPrecision(Ljava/math/BigDecimal;I)Ljava/math/BigDecimal;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher$OnUnitQuantityChangedListener;->onUnitQuantityChanged(Ljava/math/BigDecimal;)V

    return-void
.end method
