.class public Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;
.super Lcom/squareup/padlock/MoneyKeypadListener;
.source "ConfigureItemPriceScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "PriceEntryKeypadListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;Landroid/os/Vibrator;J)V
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->this$0:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    .line 188
    invoke-static {p1}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->access$000(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/ConfigureItemPriceView;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemPriceView;->getKeypad()Lcom/squareup/padlock/Padlock;

    move-result-object p1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/padlock/MoneyKeypadListener;-><init>(Lcom/squareup/padlock/Padlock;Landroid/os/Vibrator;J)V

    return-void
.end method

.method private resetToInitialAmount()V
    .locals 4

    .line 239
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->this$0:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->access$600(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->displayPrice(Lcom/squareup/protos/common/Money;Z)V

    .line 241
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->this$0:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->access$200(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-static {v2, v3, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->access$102(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    .line 242
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->this$0:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->userHasInteracted:Z

    .line 244
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->updateKeyStates()V

    return-void
.end method


# virtual methods
.method protected backspaceEnabled()Z
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->this$0:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    iget-boolean v0, v0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->userHasInteracted:Z

    return v0
.end method

.method public getAmount()J
    .locals 2

    .line 192
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->this$0:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->access$100(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public onBackspaceClicked()V
    .locals 5

    .line 218
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->getAmount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 219
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->resetToInitialAmount()V

    return-void

    .line 223
    :cond_0
    invoke-super {p0}, Lcom/squareup/padlock/MoneyKeypadListener;->onBackspaceClicked()V

    return-void
.end method

.method public onClearClicked()V
    .locals 0

    .line 209
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->resetToInitialAmount()V

    return-void
.end method

.method public onClearLongpressed()V
    .locals 2

    const-wide/16 v0, 0x32

    .line 213
    invoke-virtual {p0, v0, v1}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->vibrate(J)V

    .line 214
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->resetToInitialAmount()V

    return-void
.end method

.method public onSubmitClicked()V
    .locals 1

    .line 235
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->this$0:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->onCommitSelected()V

    return-void
.end method

.method protected submitEnabled()Z
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->this$0:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->access$500(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonEnabled:Z

    return v0
.end method

.method public updateAmount(J)V
    .locals 6

    .line 196
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->this$0:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->access$200(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v1

    invoke-static {p1, p2, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->access$102(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    .line 197
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->this$0:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->access$100(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->displayPrice(Lcom/squareup/protos/common/Money;Z)V

    .line 198
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->this$0:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->userHasInteracted:Z

    .line 200
    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->access$300(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->isGiftCard()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->this$0:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->access$400(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardActivationMinimum()J

    move-result-wide v3

    .line 202
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->this$0:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->access$500(Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    cmp-long v5, p1, v3

    if-ltz v5, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    .line 205
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter$PriceEntryKeypadListener;->updateKeyStates()V

    return-void
.end method
