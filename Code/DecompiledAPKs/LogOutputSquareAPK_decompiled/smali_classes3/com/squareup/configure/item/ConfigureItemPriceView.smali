.class public Lcom/squareup/configure/item/ConfigureItemPriceView;
.super Lcom/squareup/register/widgets/KeypadEntryView;
.source "ConfigureItemPriceView.java"

# interfaces
.implements Lcom/squareup/container/VisualTransitionListener;


# instance fields
.field presenter:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Lcom/squareup/register/widgets/KeypadEntryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    const-class p2, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Component;->inject(Lcom/squareup/configure/item/ConfigureItemPriceView;)V

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 40
    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getDevice(Landroid/content/Context;)Lcom/squareup/util/Device;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    :goto_0
    return-object p1
.end method

.method public onBackPressed()Z
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceView;->presenter:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->onCommitSelected()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceView;->presenter:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 31
    invoke-super {p0}, Lcom/squareup/register/widgets/KeypadEntryView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 25
    invoke-super {p0}, Lcom/squareup/register/widgets/KeypadEntryView;->onFinishInflate()V

    .line 26
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceView;->presenter:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onStartVisualTransition()V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemPriceView;->presenter:Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen$Presenter;->onStartVisualTransition()V

    return-void
.end method
