.class Lcom/squareup/configure/item/ConfigureItemDetailView$4;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "ConfigureItemDetailView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/configure/item/ConfigureItemDetailView;->buildFixedPriceOverridePriceButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/configure/item/ConfigureItemDetailView;


# direct methods
.method constructor <init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V
    .locals 0

    .line 255
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView$4;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 257
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView$4;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailView;

    iget-object p1, p1, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onFixedPriceOverrideButtonClicked()V

    return-void
.end method
