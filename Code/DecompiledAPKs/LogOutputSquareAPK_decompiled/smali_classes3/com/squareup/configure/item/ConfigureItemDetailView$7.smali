.class Lcom/squareup/configure/item/ConfigureItemDetailView$7;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "ConfigureItemDetailView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/configure/item/ConfigureItemDetailView;->configureVoidButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/configure/item/ConfigureItemDetailView;


# direct methods
.method constructor <init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V
    .locals 0

    .line 575
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView$7;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 577
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView$7;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailView;

    iget-object p1, p1, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onVoidButtonClicked()V

    return-void
.end method
