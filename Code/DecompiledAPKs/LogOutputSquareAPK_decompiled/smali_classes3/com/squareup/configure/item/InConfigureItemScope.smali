.class public abstract Lcom/squareup/configure/item/InConfigureItemScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InConfigureItemScope.java"


# instance fields
.field final configureItemPath:Lcom/squareup/configure/item/ConfigureItemScope;


# direct methods
.method constructor <init>(Lcom/squareup/configure/item/ConfigureItemScope;)V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/squareup/configure/item/InConfigureItemScope;->configureItemPath:Lcom/squareup/configure/item/ConfigureItemScope;

    return-void
.end method


# virtual methods
.method public final getParentKey()Lcom/squareup/configure/item/ConfigureItemScope;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/configure/item/InConfigureItemScope;->configureItemPath:Lcom/squareup/configure/item/ConfigureItemScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 5
    invoke-virtual {p0}, Lcom/squareup/configure/item/InConfigureItemScope;->getParentKey()Lcom/squareup/configure/item/ConfigureItemScope;

    move-result-object v0

    return-object v0
.end method
