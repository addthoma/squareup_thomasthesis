.class public final Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;
.super Ljava/lang/Object;
.source "ConfigureItemScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/configure/item/ConfigureItemScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final availableDiscountsStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AvailableDiscountsStore;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final hostProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            ">;"
        }
    .end annotation
.end field

.field private final navigatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemNavigator;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final orderItemBundleKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/checkout/CartItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private final posContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final workingItemBundleKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/checkout/CartItem;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemNavigator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AvailableDiscountsStore;",
            ">;)V"
        }
    .end annotation

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->currencyProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->orderItemBundleKeyProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p3, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p4, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->hostProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p5, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p6, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->workingItemBundleKeyProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p7, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->navigatorProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p8, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->posContainerProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p9, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p10, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->availableDiscountsStoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/checkout/CartItem;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemNavigator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AvailableDiscountsStore;",
            ">;)",
            "Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;"
        }
    .end annotation

    .line 79
    new-instance v11, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/BundleKey;Lcom/squareup/configure/item/ConfigureItemNavigator;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/payment/AvailableDiscountsStore;)Lcom/squareup/configure/item/ConfigureItemScopeRunner;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/checkout/CartItem;",
            ">;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;",
            "Lcom/squareup/configure/item/ConfigureItemNavigator;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/payment/AvailableDiscountsStore;",
            ")",
            "Lcom/squareup/configure/item/ConfigureItemScopeRunner;"
        }
    .end annotation

    .line 88
    new-instance v11, Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/BundleKey;Lcom/squareup/configure/item/ConfigureItemNavigator;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/payment/AvailableDiscountsStore;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/configure/item/ConfigureItemScopeRunner;
    .locals 11

    .line 67
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->currencyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->orderItemBundleKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/BundleKey;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->hostProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/configure/item/ConfigureItemHost;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->workingItemBundleKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/BundleKey;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->navigatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/configure/item/ConfigureItemNavigator;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->posContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/main/PosContainer;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->availableDiscountsStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/payment/AvailableDiscountsStore;

    invoke-static/range {v1 .. v10}, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->newInstance(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/BundleKey;Lcom/squareup/configure/item/ConfigureItemNavigator;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/payment/AvailableDiscountsStore;)Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner_Factory;->get()Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    move-result-object v0

    return-object v0
.end method
