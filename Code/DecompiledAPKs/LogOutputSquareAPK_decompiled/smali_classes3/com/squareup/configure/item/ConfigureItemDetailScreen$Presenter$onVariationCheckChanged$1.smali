.class final Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVariationCheckChanged$1;
.super Ljava/lang/Object;
.source "ConfigureItemDetailScreen.kt"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onVariationCheckChanged(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogTask<",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000e\u0010\u0003\u001a\n \u0002*\u0004\u0018\u00010\u00040\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        "kotlin.jvm.PlatformType",
        "catalogLocal",
        "Lcom/squareup/shared/catalog/Catalog$Local;",
        "perform"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $selectedVariation:Lcom/squareup/checkout/OrderVariation;


# direct methods
.method constructor <init>(Lcom/squareup/checkout/OrderVariation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVariationCheckChanged$1;->$selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/models/CatalogItemVariation;
    .locals 2

    .line 1105
    const-class v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVariationCheckChanged$1;->$selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v1}, Lcom/squareup/checkout/OrderVariation;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    return-object p1
.end method

.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 0

    .line 181
    invoke-virtual {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onVariationCheckChanged$1;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object p1

    return-object p1
.end method
