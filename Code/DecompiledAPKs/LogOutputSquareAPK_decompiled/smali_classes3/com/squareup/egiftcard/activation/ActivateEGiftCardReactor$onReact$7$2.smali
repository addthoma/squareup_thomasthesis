.class final Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$7$2;
.super Lkotlin/jvm/internal/Lambda;
.source "ActivateEGiftCardReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$7;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$ExitEntireFlow;",
        "Lcom/squareup/workflow/legacy/FinishWith<",
        "+",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult$Registered;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/FinishWith;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult$Registered;",
        "it",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$ExitEntireFlow;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$7;


# direct methods
.method constructor <init>(Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$7;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$7$2;->this$0:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$7;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$ExitEntireFlow;)Lcom/squareup/workflow/legacy/FinishWith;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$ExitEntireFlow;",
            ")",
            "Lcom/squareup/workflow/legacy/FinishWith<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult$Registered;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$7$2;->this$0:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$7;

    iget-object p1, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$7;->$finishSuccess$6:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$6;

    invoke-virtual {p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$6;->invoke()Lcom/squareup/workflow/legacy/FinishWith;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$ExitEntireFlow;

    invoke-virtual {p0, p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$7$2;->invoke(Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$ExitEntireFlow;)Lcom/squareup/workflow/legacy/FinishWith;

    move-result-object p1

    return-object p1
.end method
