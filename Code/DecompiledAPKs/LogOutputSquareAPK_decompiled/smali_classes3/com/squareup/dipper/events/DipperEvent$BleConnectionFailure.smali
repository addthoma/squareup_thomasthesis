.class public final Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;
.super Lcom/squareup/dipper/events/DipperEvent;
.source "DipperEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/dipper/events/DipperEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BleConnectionFailure"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B1\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\tH\u00c6\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003J=\u0010\u001b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000bH\u00c6\u0001J\u0013\u0010\u001c\u001a\u00020\t2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u00d6\u0003J\t\u0010\u001f\u001a\u00020\u0007H\u00d6\u0001J\t\u0010 \u001a\u00020!H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0015\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;",
        "Lcom/squareup/dipper/events/DipperEvent;",
        "device",
        "Lcom/squareup/dipper/events/BleDevice;",
        "errorType",
        "Lcom/squareup/dipper/events/BleErrorType;",
        "attemptNumber",
        "",
        "isTerminal",
        "",
        "disconnectionEvent",
        "Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;",
        "(Lcom/squareup/dipper/events/BleDevice;Lcom/squareup/dipper/events/BleErrorType;IZLcom/squareup/cardreader/ble/ConnectionState$Disconnected;)V",
        "getAttemptNumber",
        "()I",
        "getDevice",
        "()Lcom/squareup/dipper/events/BleDevice;",
        "getDisconnectionEvent",
        "()Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;",
        "getErrorType",
        "()Lcom/squareup/dipper/events/BleErrorType;",
        "()Z",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final attemptNumber:I

.field private final device:Lcom/squareup/dipper/events/BleDevice;

.field private final disconnectionEvent:Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

.field private final errorType:Lcom/squareup/dipper/events/BleErrorType;

.field private final isTerminal:Z


# direct methods
.method public constructor <init>(Lcom/squareup/dipper/events/BleDevice;Lcom/squareup/dipper/events/BleErrorType;IZLcom/squareup/cardreader/ble/ConnectionState$Disconnected;)V
    .locals 1

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 19
    invoke-direct {p0, v0}, Lcom/squareup/dipper/events/DipperEvent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->device:Lcom/squareup/dipper/events/BleDevice;

    iput-object p2, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->errorType:Lcom/squareup/dipper/events/BleErrorType;

    iput p3, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->attemptNumber:I

    iput-boolean p4, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->isTerminal:Z

    iput-object p5, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->disconnectionEvent:Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/dipper/events/BleDevice;Lcom/squareup/dipper/events/BleErrorType;IZLcom/squareup/cardreader/ble/ConnectionState$Disconnected;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    const/4 p5, 0x0

    .line 18
    check-cast p5, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

    :cond_0
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;-><init>(Lcom/squareup/dipper/events/BleDevice;Lcom/squareup/dipper/events/BleErrorType;IZLcom/squareup/cardreader/ble/ConnectionState$Disconnected;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;Lcom/squareup/dipper/events/BleDevice;Lcom/squareup/dipper/events/BleErrorType;IZLcom/squareup/cardreader/ble/ConnectionState$Disconnected;ILjava/lang/Object;)Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    invoke-virtual {p0}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object p1

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->errorType:Lcom/squareup/dipper/events/BleErrorType;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->attemptNumber:I

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->isTerminal:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->disconnectionEvent:Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v0

    move p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->copy(Lcom/squareup/dipper/events/BleDevice;Lcom/squareup/dipper/events/BleErrorType;IZLcom/squareup/cardreader/ble/ConnectionState$Disconnected;)Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/dipper/events/BleDevice;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/dipper/events/BleErrorType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->errorType:Lcom/squareup/dipper/events/BleErrorType;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->attemptNumber:I

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->isTerminal:Z

    return v0
.end method

.method public final component5()Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;
    .locals 1

    iget-object v0, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->disconnectionEvent:Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

    return-object v0
.end method

.method public final copy(Lcom/squareup/dipper/events/BleDevice;Lcom/squareup/dipper/events/BleErrorType;IZLcom/squareup/cardreader/ble/ConnectionState$Disconnected;)Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;
    .locals 7

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;-><init>(Lcom/squareup/dipper/events/BleDevice;Lcom/squareup/dipper/events/BleErrorType;IZLcom/squareup/cardreader/ble/ConnectionState$Disconnected;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;

    invoke-virtual {p0}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->errorType:Lcom/squareup/dipper/events/BleErrorType;

    iget-object v1, p1, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->errorType:Lcom/squareup/dipper/events/BleErrorType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->attemptNumber:I

    iget v1, p1, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->attemptNumber:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->isTerminal:Z

    iget-boolean v1, p1, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->isTerminal:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->disconnectionEvent:Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

    iget-object p1, p1, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->disconnectionEvent:Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAttemptNumber()I
    .locals 1

    .line 16
    iget v0, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->attemptNumber:I

    return v0
.end method

.method public getDevice()Lcom/squareup/dipper/events/BleDevice;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->device:Lcom/squareup/dipper/events/BleDevice;

    return-object v0
.end method

.method public final getDisconnectionEvent()Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->disconnectionEvent:Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

    return-object v0
.end method

.method public final getErrorType()Lcom/squareup/dipper/events/BleErrorType;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->errorType:Lcom/squareup/dipper/events/BleErrorType;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->errorType:Lcom/squareup/dipper/events/BleErrorType;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->attemptNumber:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->isTerminal:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->disconnectionEvent:Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public final isTerminal()Z
    .locals 1

    .line 17
    iget-boolean v0, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->isTerminal:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BleConnectionFailure(device="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->getDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", errorType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->errorType:Lcom/squareup/dipper/events/BleErrorType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", attemptNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->attemptNumber:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isTerminal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->isTerminal:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", disconnectionEvent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;->disconnectionEvent:Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
