.class public Lcom/felhr/deviceids/CH34xIds;
.super Ljava/lang/Object;
.source "CH34xIds.java"


# static fields
.field private static final ch34xDevices:[J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x4

    new-array v0, v0, [J

    const/16 v1, 0x5523

    const/16 v2, 0x4348

    .line 14
    invoke-static {v2, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/4 v4, 0x0

    aput-wide v2, v0, v4

    const/16 v2, 0x1a86

    const/16 v3, 0x7523

    .line 15
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/4 v5, 0x1

    aput-wide v3, v0, v5

    .line 16
    invoke-static {v2, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/4 v1, 0x2

    aput-wide v3, v0, v1

    const/16 v1, 0x445

    .line 17
    invoke-static {v2, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/4 v3, 0x3

    aput-wide v1, v0, v3

    .line 13
    invoke-static {v0}, Lcom/felhr/deviceids/Helpers;->createTable([J)[J

    move-result-object v0

    sput-object v0, Lcom/felhr/deviceids/CH34xIds;->ch34xDevices:[J

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isDeviceSupported(II)Z
    .locals 1

    .line 22
    sget-object v0, Lcom/felhr/deviceids/CH34xIds;->ch34xDevices:[J

    invoke-static {v0, p0, p1}, Lcom/felhr/deviceids/Helpers;->exists([JII)Z

    move-result p0

    return p0
.end method
