.class abstract Lcom/felhr/usbserial/AbstractWorkerThread;
.super Ljava/lang/Thread;
.source "AbstractWorkerThread.java"


# instance fields
.field firstTime:Z

.field private volatile keep:Z

.field private volatile workingThread:Ljava/lang/Thread;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 3
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x1

    .line 4
    iput-boolean v0, p0, Lcom/felhr/usbserial/AbstractWorkerThread;->firstTime:Z

    .line 5
    iput-boolean v0, p0, Lcom/felhr/usbserial/AbstractWorkerThread;->keep:Z

    return-void
.end method


# virtual methods
.method abstract doRun()V
.end method

.method public final run()V
    .locals 1

    .line 16
    iget-boolean v0, p0, Lcom/felhr/usbserial/AbstractWorkerThread;->keep:Z

    if-nez v0, :cond_0

    return-void

    .line 19
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/felhr/usbserial/AbstractWorkerThread;->workingThread:Ljava/lang/Thread;

    .line 20
    :goto_0
    iget-boolean v0, p0, Lcom/felhr/usbserial/AbstractWorkerThread;->keep:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/felhr/usbserial/AbstractWorkerThread;->workingThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 21
    invoke-virtual {p0}, Lcom/felhr/usbserial/AbstractWorkerThread;->doRun()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method stopThread()V
    .locals 1

    const/4 v0, 0x0

    .line 9
    iput-boolean v0, p0, Lcom/felhr/usbserial/AbstractWorkerThread;->keep:Z

    .line 10
    iget-object v0, p0, Lcom/felhr/usbserial/AbstractWorkerThread;->workingThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 11
    iget-object v0, p0, Lcom/felhr/usbserial/AbstractWorkerThread;->workingThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_0
    return-void
.end method
