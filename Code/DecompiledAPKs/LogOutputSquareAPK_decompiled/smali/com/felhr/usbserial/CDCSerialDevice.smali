.class public Lcom/felhr/usbserial/CDCSerialDevice;
.super Lcom/felhr/usbserial/UsbSerialDevice;
.source "CDCSerialDevice.java"


# static fields
.field private static final CDC_CONTROL_LINE_OFF:I = 0x0

.field private static final CDC_CONTROL_LINE_ON:I = 0x3

.field private static final CDC_DEFAULT_LINE_CODING:[B

.field private static final CDC_GET_LINE_CODING:I = 0x21

.field private static final CDC_REQTYPE_DEVICE2HOST:I = 0xa1

.field private static final CDC_REQTYPE_HOST2DEVICE:I = 0x21

.field private static final CDC_SET_CONTROL_LINE_STATE:I = 0x22

.field private static final CDC_SET_CONTROL_LINE_STATE_DTR:I = 0x1

.field private static final CDC_SET_CONTROL_LINE_STATE_RTS:I = 0x2

.field private static final CDC_SET_LINE_CODING:I = 0x20

.field private static final CLASS_ID:Ljava/lang/String;


# instance fields
.field private controlLineState:I

.field private inEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private initialBaudRate:I

.field private final mInterface:Landroid/hardware/usb/UsbInterface;

.field private outEndpoint:Landroid/hardware/usb/UsbEndpoint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    const-class v0, Lcom/felhr/usbserial/CDCSerialDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/felhr/usbserial/CDCSerialDevice;->CLASS_ID:Ljava/lang/String;

    const/4 v0, 0x7

    new-array v0, v0, [B

    .line 36
    fill-array-data v0, :array_0

    sput-object v0, Lcom/felhr/usbserial/CDCSerialDevice;->CDC_DEFAULT_LINE_CODING:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        -0x3et
        0x1t
        0x0t
        0x0t
        0x0t
        0x8t
    .end array-data
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V
    .locals 1

    const/4 v0, -0x1

    .line 59
    invoke-direct {p0, p1, p2, v0}, Lcom/felhr/usbserial/CDCSerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V

    return-void
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V
    .locals 0

    .line 64
    invoke-direct {p0, p1, p2}, Lcom/felhr/usbserial/UsbSerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V

    const/4 p2, 0x0

    .line 53
    iput p2, p0, Lcom/felhr/usbserial/CDCSerialDevice;->initialBaudRate:I

    const/4 p2, 0x3

    .line 55
    iput p2, p0, Lcom/felhr/usbserial/CDCSerialDevice;->controlLineState:I

    if-ltz p3, :cond_0

    goto :goto_0

    .line 65
    :cond_0
    invoke-static {p1}, Lcom/felhr/usbserial/CDCSerialDevice;->findFirstCDC(Landroid/hardware/usb/UsbDevice;)I

    move-result p3

    :goto_0
    invoke-virtual {p1, p3}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object p1

    iput-object p1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    return-void
.end method

.method private static findFirstCDC(Landroid/hardware/usb/UsbDevice;)I
    .locals 4

    .line 390
    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 394
    invoke-virtual {p0, v1}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/usb/UsbInterface;->getInterfaceClass()I

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 400
    :cond_1
    sget-object p0, Lcom/felhr/usbserial/CDCSerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v0, "There is no CDC class interface"

    invoke-static {p0, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p0, -0x1

    return p0
.end method

.method private getLineCoding()[B
    .locals 9

    const/4 v0, 0x7

    new-array v0, v0, [B

    .line 383
    iget-object v1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    array-length v7, v0

    const/16 v2, 0xa1

    const/16 v3, 0x21

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object v6, v0

    invoke-virtual/range {v1 .. v8}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result v1

    .line 384
    sget-object v2, Lcom/felhr/usbserial/CDCSerialDevice;->CLASS_ID:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Control Transfer Response: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method private openCDC()Z
    .locals 8

    .line 313
    iget-object v0, p0, Lcom/felhr/usbserial/CDCSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    .line 315
    sget-object v0, Lcom/felhr/usbserial/CDCSerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v3, "Interface succesfully claimed"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    iget-object v0, p0, Lcom/felhr/usbserial/CDCSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result v0

    const/4 v3, 0x0

    :goto_0
    add-int/lit8 v4, v0, -0x1

    if-gt v3, v4, :cond_2

    .line 326
    iget-object v4, p0, Lcom/felhr/usbserial/CDCSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v4, v3}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v4

    .line 327
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getType()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 328
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v5

    const/16 v7, 0x80

    if-ne v5, v7, :cond_0

    .line 330
    iput-object v4, p0, Lcom/felhr/usbserial/CDCSerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    goto :goto_1

    .line 331
    :cond_0
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getType()I

    move-result v5

    if-ne v5, v6, :cond_1

    .line 332
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v5

    if-nez v5, :cond_1

    .line 334
    iput-object v4, p0, Lcom/felhr/usbserial/CDCSerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 338
    :cond_2
    iget-object v0, p0, Lcom/felhr/usbserial/CDCSerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/felhr/usbserial/CDCSerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    if-nez v0, :cond_3

    goto :goto_2

    :cond_3
    const/16 v0, 0x20

    .line 345
    invoke-virtual {p0}, Lcom/felhr/usbserial/CDCSerialDevice;->getInitialLineCoding()[B

    move-result-object v3

    invoke-direct {p0, v0, v1, v3}, Lcom/felhr/usbserial/CDCSerialDevice;->setControlCommand(II[B)I

    const/16 v0, 0x22

    const/4 v1, 0x3

    const/4 v3, 0x0

    .line 346
    invoke-direct {p0, v0, v1, v3}, Lcom/felhr/usbserial/CDCSerialDevice;->setControlCommand(II[B)I

    return v2

    .line 340
    :cond_4
    :goto_2
    sget-object v0, Lcom/felhr/usbserial/CDCSerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v2, "Interface does not have an IN or OUT interface"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    .line 318
    :cond_5
    sget-object v0, Lcom/felhr/usbserial/CDCSerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v2, "Interface could not be claimed"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v1
.end method

.method private setControlCommand(II[B)I
    .locals 9

    if-eqz p3, :cond_0

    .line 373
    array-length v0, p3

    move v7, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 375
    :goto_0
    iget-object v1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    const/16 v2, 0x21

    const/4 v5, 0x0

    const/4 v8, 0x0

    move v3, p1

    move v4, p2

    move-object v6, p3

    invoke-virtual/range {v1 .. v8}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result p1

    .line 376
    sget-object p2, Lcom/felhr/usbserial/CDCSerialDevice;->CLASS_ID:Ljava/lang/String;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Control Transfer Response: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return p1
.end method


# virtual methods
.method public close()V
    .locals 3

    const/4 v0, 0x0

    const/16 v1, 0x22

    const/4 v2, 0x0

    .line 110
    invoke-direct {p0, v1, v0, v2}, Lcom/felhr/usbserial/CDCSerialDevice;->setControlCommand(II[B)I

    .line 111
    invoke-virtual {p0}, Lcom/felhr/usbserial/CDCSerialDevice;->killWorkingThread()V

    .line 112
    invoke-virtual {p0}, Lcom/felhr/usbserial/CDCSerialDevice;->killWriteThread()V

    .line 113
    iget-object v1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/CDCSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    .line 114
    iget-object v1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDeviceConnection;->close()V

    .line 115
    iput-boolean v0, p0, Lcom/felhr/usbserial/CDCSerialDevice;->isOpen:Z

    return-void
.end method

.method public getBreak(Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;)V
    .locals 0

    return-void
.end method

.method public getCTS(Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;)V
    .locals 0

    return-void
.end method

.method public getDSR(Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;)V
    .locals 0

    return-void
.end method

.method public getFrame(Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;)V
    .locals 0

    return-void
.end method

.method public getInitialBaudRate()I
    .locals 1

    .line 75
    iget v0, p0, Lcom/felhr/usbserial/CDCSerialDevice;->initialBaudRate:I

    return v0
.end method

.method protected getInitialLineCoding()[B
    .locals 4

    .line 354
    invoke-virtual {p0}, Lcom/felhr/usbserial/CDCSerialDevice;->getInitialBaudRate()I

    move-result v0

    if-lez v0, :cond_0

    .line 357
    sget-object v1, Lcom/felhr/usbserial/CDCSerialDevice;->CDC_DEFAULT_LINE_CODING:[B

    invoke-virtual {v1}, [B->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x4

    if-ge v2, v3, :cond_1

    mul-int/lit8 v3, v2, 0x8

    shr-int v3, v0, v3

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    .line 359
    aput-byte v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 362
    :cond_0
    sget-object v1, Lcom/felhr/usbserial/CDCSerialDevice;->CDC_DEFAULT_LINE_CODING:[B

    :cond_1
    return-object v1
.end method

.method public getOverrun(Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;)V
    .locals 0

    return-void
.end method

.method public getParity(Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;)V
    .locals 0

    return-void
.end method

.method public open()Z
    .locals 3

    .line 81
    invoke-direct {p0}, Lcom/felhr/usbserial/CDCSerialDevice;->openCDC()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    new-instance v0, Lcom/felhr/utils/SafeUsbRequest;

    invoke-direct {v0}, Lcom/felhr/utils/SafeUsbRequest;-><init>()V

    .line 87
    iget-object v1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/CDCSerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbRequest;->initialize(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;)Z

    .line 90
    invoke-virtual {p0}, Lcom/felhr/usbserial/CDCSerialDevice;->restartWorkingThread()V

    .line 91
    invoke-virtual {p0}, Lcom/felhr/usbserial/CDCSerialDevice;->restartWriteThread()V

    .line 94
    iget-object v1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {p0, v0, v1}, Lcom/felhr/usbserial/CDCSerialDevice;->setThreadsParams(Landroid/hardware/usb/UsbRequest;Landroid/hardware/usb/UsbEndpoint;)V

    const/4 v0, 0x1

    .line 96
    iput-boolean v0, p0, Lcom/felhr/usbserial/CDCSerialDevice;->asyncMode:Z

    .line 97
    iput-boolean v0, p0, Lcom/felhr/usbserial/CDCSerialDevice;->isOpen:Z

    return v0

    :cond_0
    const/4 v0, 0x0

    .line 102
    iput-boolean v0, p0, Lcom/felhr/usbserial/CDCSerialDevice;->isOpen:Z

    return v0
.end method

.method public setBaudRate(I)V
    .locals 4

    .line 152
    invoke-direct {p0}, Lcom/felhr/usbserial/CDCSerialDevice;->getLineCoding()[B

    move-result-object v0

    and-int/lit16 v1, p1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x0

    .line 154
    aput-byte v1, v0, v2

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v3, 0x1

    .line 155
    aput-byte v1, v0, v3

    shr-int/lit8 v1, p1, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v3, 0x2

    .line 156
    aput-byte v1, v0, v3

    shr-int/lit8 p1, p1, 0x18

    and-int/lit16 p1, p1, 0xff

    int-to-byte p1, p1

    const/4 v1, 0x3

    .line 157
    aput-byte p1, v0, v1

    const/16 p1, 0x20

    .line 159
    invoke-direct {p0, p1, v2, v0}, Lcom/felhr/usbserial/CDCSerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public setBreak(Z)V
    .locals 0

    return-void
.end method

.method public setDTR(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 269
    iget p1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->controlLineState:I

    or-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->controlLineState:I

    goto :goto_0

    .line 271
    :cond_0
    iget p1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->controlLineState:I

    and-int/lit8 p1, p1, -0x2

    iput p1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->controlLineState:I

    :goto_0
    const/16 p1, 0x22

    .line 272
    iget v0, p0, Lcom/felhr/usbserial/CDCSerialDevice;->controlLineState:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/felhr/usbserial/CDCSerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public setDataBits(I)V
    .locals 3

    .line 165
    invoke-direct {p0}, Lcom/felhr/usbserial/CDCSerialDevice;->getLineCoding()[B

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x6

    if-eq p1, v1, :cond_3

    if-eq p1, v2, :cond_2

    const/4 v1, 0x7

    if-eq p1, v1, :cond_1

    const/16 v1, 0x8

    if-eq p1, v1, :cond_0

    return-void

    .line 178
    :cond_0
    aput-byte v1, v0, v2

    goto :goto_0

    .line 175
    :cond_1
    aput-byte v1, v0, v2

    goto :goto_0

    .line 172
    :cond_2
    aput-byte v2, v0, v2

    goto :goto_0

    .line 169
    :cond_3
    aput-byte v1, v0, v2

    :goto_0
    const/16 p1, 0x20

    const/4 v1, 0x0

    .line 184
    invoke-direct {p0, p1, v1, v0}, Lcom/felhr/usbserial/CDCSerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public setFlowControl(I)V
    .locals 0

    return-void
.end method

.method public setInitialBaudRate(I)V
    .locals 0

    .line 70
    iput p1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->initialBaudRate:I

    return-void
.end method

.method public setParity(I)V
    .locals 4

    .line 215
    invoke-direct {p0}, Lcom/felhr/usbserial/CDCSerialDevice;->getLineCoding()[B

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x5

    if-eqz p1, :cond_4

    const/4 v3, 0x1

    if-eq p1, v3, :cond_3

    const/4 v3, 0x2

    if-eq p1, v3, :cond_2

    const/4 v3, 0x3

    if-eq p1, v3, :cond_1

    const/4 v3, 0x4

    if-eq p1, v3, :cond_0

    return-void

    .line 231
    :cond_0
    aput-byte v3, v0, v2

    goto :goto_0

    .line 228
    :cond_1
    aput-byte v3, v0, v2

    goto :goto_0

    .line 225
    :cond_2
    aput-byte v3, v0, v2

    goto :goto_0

    .line 222
    :cond_3
    aput-byte v3, v0, v2

    goto :goto_0

    .line 219
    :cond_4
    aput-byte v1, v0, v2

    :goto_0
    const/16 p1, 0x20

    .line 237
    invoke-direct {p0, p1, v1, v0}, Lcom/felhr/usbserial/CDCSerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public setRTS(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 258
    iget p1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->controlLineState:I

    or-int/lit8 p1, p1, 0x2

    iput p1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->controlLineState:I

    goto :goto_0

    .line 260
    :cond_0
    iget p1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->controlLineState:I

    and-int/lit8 p1, p1, -0x3

    iput p1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->controlLineState:I

    :goto_0
    const/16 p1, 0x22

    .line 261
    iget v0, p0, Lcom/felhr/usbserial/CDCSerialDevice;->controlLineState:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/felhr/usbserial/CDCSerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public setStopBits(I)V
    .locals 5

    .line 191
    invoke-direct {p0}, Lcom/felhr/usbserial/CDCSerialDevice;->getLineCoding()[B

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x4

    if-eq p1, v2, :cond_2

    const/4 v4, 0x2

    if-eq p1, v4, :cond_1

    const/4 v4, 0x3

    if-eq p1, v4, :cond_0

    return-void

    .line 198
    :cond_0
    aput-byte v2, v0, v3

    goto :goto_0

    .line 201
    :cond_1
    aput-byte v4, v0, v3

    goto :goto_0

    .line 195
    :cond_2
    aput-byte v1, v0, v3

    :goto_0
    const/16 p1, 0x20

    .line 207
    invoke-direct {p0, p1, v1, v0}, Lcom/felhr/usbserial/CDCSerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public syncClose()V
    .locals 3

    const/4 v0, 0x0

    const/16 v1, 0x22

    const/4 v2, 0x0

    .line 143
    invoke-direct {p0, v1, v0, v2}, Lcom/felhr/usbserial/CDCSerialDevice;->setControlCommand(II[B)I

    .line 144
    iget-object v1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/CDCSerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    .line 145
    iget-object v1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDeviceConnection;->close()V

    .line 146
    iput-boolean v0, p0, Lcom/felhr/usbserial/CDCSerialDevice;->isOpen:Z

    return-void
.end method

.method public syncOpen()Z
    .locals 3

    .line 121
    invoke-direct {p0}, Lcom/felhr/usbserial/CDCSerialDevice;->openCDC()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/felhr/usbserial/CDCSerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v2, p0, Lcom/felhr/usbserial/CDCSerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {p0, v0, v2}, Lcom/felhr/usbserial/CDCSerialDevice;->setSyncParams(Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)V

    .line 125
    iput-boolean v1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->asyncMode:Z

    const/4 v0, 0x1

    .line 126
    iput-boolean v0, p0, Lcom/felhr/usbserial/CDCSerialDevice;->isOpen:Z

    .line 129
    new-instance v1, Lcom/felhr/usbserial/SerialInputStream;

    invoke-direct {v1, p0}, Lcom/felhr/usbserial/SerialInputStream;-><init>(Lcom/felhr/usbserial/UsbSerialInterface;)V

    iput-object v1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->inputStream:Lcom/felhr/usbserial/SerialInputStream;

    .line 130
    new-instance v1, Lcom/felhr/usbserial/SerialOutputStream;

    invoke-direct {v1, p0}, Lcom/felhr/usbserial/SerialOutputStream;-><init>(Lcom/felhr/usbserial/UsbSerialInterface;)V

    iput-object v1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->outputStream:Lcom/felhr/usbserial/SerialOutputStream;

    return v0

    .line 135
    :cond_0
    iput-boolean v1, p0, Lcom/felhr/usbserial/CDCSerialDevice;->isOpen:Z

    return v1
.end method
