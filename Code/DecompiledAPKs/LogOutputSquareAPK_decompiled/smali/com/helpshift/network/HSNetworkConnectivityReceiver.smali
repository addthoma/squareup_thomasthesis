.class public Lcom/helpshift/network/HSNetworkConnectivityReceiver;
.super Landroid/content/BroadcastReceiver;
.source "HSNetworkConnectivityReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/helpshift/network/HSNetworkConnectivityReceiver$HSNetworkConnectivityListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Helpshift_NetStateRcv"


# instance fields
.field private context:Landroid/content/Context;

.field protected listeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/helpshift/network/HSNetworkConnectivityReceiver$HSNetworkConnectivityListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 22
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 23
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/helpshift/network/HSNetworkConnectivityReceiver;->listeners:Ljava/util/Set;

    .line 24
    iput-object p1, p0, Lcom/helpshift/network/HSNetworkConnectivityReceiver;->context:Landroid/content/Context;

    return-void
.end method

.method private isConnected()Ljava/lang/Boolean;
    .locals 4

    const/4 v0, 0x0

    .line 67
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/network/HSNetworkConnectivityReceiver;->context:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    if-eqz v1, :cond_1

    .line 69
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 70
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 71
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 74
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "Helpshift_NetStateRcv"

    const-string v3, "Error in network state receiver."

    .line 79
    invoke-static {v2, v3, v1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return-object v0
.end method

.method private notifyState(Lcom/helpshift/network/HSNetworkConnectivityReceiver$HSNetworkConnectivityListener;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 45
    invoke-interface {p1}, Lcom/helpshift/network/HSNetworkConnectivityReceiver$HSNetworkConnectivityListener;->networkAvailable()V

    goto :goto_0

    .line 48
    :cond_0
    invoke-interface {p1}, Lcom/helpshift/network/HSNetworkConnectivityReceiver$HSNetworkConnectivityListener;->networkUnavailable()V

    :goto_0
    return-void
.end method

.method private notifyStateToAll(Z)V
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/helpshift/network/HSNetworkConnectivityReceiver;->listeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/network/HSNetworkConnectivityReceiver$HSNetworkConnectivityListener;

    .line 39
    invoke-direct {p0, v1, p1}, Lcom/helpshift/network/HSNetworkConnectivityReceiver;->notifyState(Lcom/helpshift/network/HSNetworkConnectivityReceiver$HSNetworkConnectivityListener;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public addListener(Lcom/helpshift/network/HSNetworkConnectivityReceiver$HSNetworkConnectivityListener;)V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/helpshift/network/HSNetworkConnectivityReceiver;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 54
    invoke-direct {p0}, Lcom/helpshift/network/HSNetworkConnectivityReceiver;->isConnected()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/helpshift/network/HSNetworkConnectivityReceiver;->notifyState(Lcom/helpshift/network/HSNetworkConnectivityReceiver$HSNetworkConnectivityListener;Z)V

    :cond_0
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    if-eqz p2, :cond_1

    .line 28
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 31
    :cond_0
    invoke-direct {p0}, Lcom/helpshift/network/HSNetworkConnectivityReceiver;->isConnected()Ljava/lang/Boolean;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 33
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/helpshift/network/HSNetworkConnectivityReceiver;->notifyStateToAll(Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method public removeListener(Lcom/helpshift/network/HSNetworkConnectivityReceiver$HSNetworkConnectivityListener;)V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/helpshift/network/HSNetworkConnectivityReceiver;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method
