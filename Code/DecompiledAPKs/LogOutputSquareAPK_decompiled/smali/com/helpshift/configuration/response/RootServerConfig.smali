.class public Lcom/helpshift/configuration/response/RootServerConfig;
.super Ljava/lang/Object;
.source "RootServerConfig.java"


# instance fields
.field public final allowUserAttachments:Z

.field public final breadcrumbLimit:I

.field public final conversationGreetingMessage:Ljava/lang/String;

.field public final conversationalIssueFiling:Z

.field public final customerSatisfactionSurvey:Z

.field public final debugLogLimit:I

.field public final disableHelpshiftBranding:Z

.field public final disableInAppConversation:Z

.field public final enableTypingIndicator:Z

.field public final issueExists:Z

.field public final lastRedactionAt:Ljava/lang/Long;

.field public final periodicFetchInterval:Ljava/lang/Long;

.field public final periodicReview:Lcom/helpshift/configuration/response/PeriodicReview;

.field public final profileCreatedAt:Ljava/lang/Long;

.field public final profileFormEnable:Z

.field public final requireNameAndEmail:Z

.field public final reviewUrl:Ljava/lang/String;

.field public final shouldShowConversationHistory:Z

.field public final showAgentName:Z

.field public final showConversationResolutionQuestion:Z


# direct methods
.method public constructor <init>(ZZZZZZZIILjava/lang/String;Lcom/helpshift/configuration/response/PeriodicReview;ZLjava/lang/String;ZZZLjava/lang/Long;Ljava/lang/Long;ZLjava/lang/Long;)V
    .locals 2

    move-object v0, p0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move v1, p1

    .line 55
    iput-boolean v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->requireNameAndEmail:Z

    move v1, p2

    .line 56
    iput-boolean v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->profileFormEnable:Z

    move v1, p3

    .line 57
    iput-boolean v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->showAgentName:Z

    move v1, p4

    .line 58
    iput-boolean v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->customerSatisfactionSurvey:Z

    move v1, p5

    .line 59
    iput-boolean v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->disableInAppConversation:Z

    move v1, p6

    .line 60
    iput-boolean v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->disableHelpshiftBranding:Z

    move v1, p7

    .line 61
    iput-boolean v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->issueExists:Z

    move v1, p8

    .line 62
    iput v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->debugLogLimit:I

    move v1, p9

    .line 63
    iput v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->breadcrumbLimit:I

    move-object v1, p10

    .line 64
    iput-object v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->reviewUrl:Ljava/lang/String;

    move-object v1, p11

    .line 65
    iput-object v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->periodicReview:Lcom/helpshift/configuration/response/PeriodicReview;

    move v1, p12

    .line 66
    iput-boolean v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->conversationalIssueFiling:Z

    move-object v1, p13

    .line 67
    iput-object v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->conversationGreetingMessage:Ljava/lang/String;

    move/from16 v1, p14

    .line 68
    iput-boolean v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->enableTypingIndicator:Z

    move/from16 v1, p15

    .line 69
    iput-boolean v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->showConversationResolutionQuestion:Z

    move/from16 v1, p16

    .line 70
    iput-boolean v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->shouldShowConversationHistory:Z

    move-object/from16 v1, p17

    .line 71
    iput-object v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->lastRedactionAt:Ljava/lang/Long;

    move-object/from16 v1, p18

    .line 72
    iput-object v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->profileCreatedAt:Ljava/lang/Long;

    move/from16 v1, p19

    .line 73
    iput-boolean v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->allowUserAttachments:Z

    move-object/from16 v1, p20

    .line 74
    iput-object v1, v0, Lcom/helpshift/configuration/response/RootServerConfig;->periodicFetchInterval:Ljava/lang/Long;

    return-void
.end method
