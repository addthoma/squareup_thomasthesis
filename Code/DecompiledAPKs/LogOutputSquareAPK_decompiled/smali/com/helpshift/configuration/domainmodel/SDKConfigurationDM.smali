.class public Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;
.super Ljava/util/Observable;
.source "SDKConfigurationDM.java"


# static fields
.field public static final ALLOW_USER_ATTACHMENTS:Ljava/lang/String; = "allowUserAttachments"

.field public static final API_KEY:Ljava/lang/String; = "apiKey"

.field public static final APP_REVIEWED:Ljava/lang/String; = "app_reviewed"

.field public static final BREADCRUMB_LIMIT:Ljava/lang/String; = "breadcrumbLimit"

.field public static final CONVERSATIONAL_ISSUE_FILING:Ljava/lang/String; = "conversationalIssueFiling"

.field public static final CONVERSATION_GREETING_MESSAGE:Ljava/lang/String; = "conversationGreetingMessage"

.field public static final CONVERSATION_PRE_FILL_TEXT:Ljava/lang/String; = "conversationPrefillText"

.field public static final CUSTOMER_SATISFACTION_SURVEY:Ljava/lang/String; = "customerSatisfactionSurvey"

.field public static final DEBUG_LOG_LIMIT:Ljava/lang/String; = "debugLogLimit"

.field public static final DEFAULT_FALLBACK_LANGUAGE_ENABLE:Ljava/lang/String; = "defaultFallbackLanguageEnable"

.field public static final DISABLE_ANIMATION:Ljava/lang/String; = "disableAnimations"

.field public static final DISABLE_APP_LAUNCH_EVENT:Ljava/lang/String; = "disableAppLaunchEvent"

.field public static final DISABLE_ERROR_LOGGING:Ljava/lang/String; = "disableErrorLogging"

.field public static final DISABLE_IN_APP_CONVERSATION:Ljava/lang/String; = "disableInAppConversation"

.field public static final DOMAIN_NAME:Ljava/lang/String; = "domainName"

.field public static final ENABLE_CONTACT_US:Ljava/lang/String; = "enableContactUs"

.field public static final ENABLE_DEFAULT_CONVERSATIONAL_FILING:Ljava/lang/String; = "enableDefaultConversationalFiling"

.field public static final ENABLE_FULL_PRIVACY:Ljava/lang/String; = "fullPrivacy"

.field public static final ENABLE_IN_APP_NOTIFICATION:Ljava/lang/String; = "enableInAppNotification"

.field public static final ENABLE_TYPING_INDICATOR:Ljava/lang/String; = "enableTypingIndicator"

.field public static final ENABLE_TYPING_INDICATOR_AGENT:Ljava/lang/String; = "enableTypingIndicatorAgent"

.field public static final FONT_PATH:Ljava/lang/String; = "fontPath"

.field public static final GOTO_CONVERSATION_AFTER_CONTACT_US:Ljava/lang/String; = "gotoConversationAfterContactUs"

.field public static final HELPSHIFT_BRANDING_DISABLE_AGENT:Ljava/lang/String; = "disableHelpshiftBrandingAgent"

.field public static final HELPSHIFT_BRANDING_DISABLE_INSTALL:Ljava/lang/String; = "disableHelpshiftBranding"

.field public static final HIDE_NAME_AND_EMAIL:Ljava/lang/String; = "hideNameAndEmail"

.field public static final INBOX_POLLING_ENABLE:Ljava/lang/String; = "inboxPollingEnable"

.field public static final LAST_SUCCESSFUL_CONFIG_FETCH_TIME:Ljava/lang/String; = "lastSuccessfulConfigFetchTime"

.field public static final MINIMUM_PERIODIC_FETCH_INTERVAL:Ljava/lang/Long;

.field public static final NOTIFICATION_ICON_ID:Ljava/lang/String; = "notificationIconId"

.field public static final NOTIFICATION_LARGE_ICON_ID:Ljava/lang/String; = "notificationLargeIconId"

.field public static final NOTIFICATION_MUTE_ENABLE:Ljava/lang/String; = "notificationMute"

.field public static final NOTIFICATION_SOUND_ID:Ljava/lang/String; = "notificationSoundId"

.field public static final PERIODIC_FETCH_INTERVAL:Ljava/lang/String; = "periodicFetchInterval"

.field public static final PERIODIC_REVIEW_ENABLED:Ljava/lang/String; = "periodicReviewEnabled"

.field public static final PERIODIC_REVIEW_INTERVAL:Ljava/lang/String; = "periodicReviewInterval"

.field public static final PERIODIC_REVIEW_TYPE:Ljava/lang/String; = "periodicReviewType"

.field public static final PLATFORM_ID:Ljava/lang/String; = "platformId"

.field public static final PLUGIN_VERSION:Ljava/lang/String; = "pluginVersion"

.field public static final PROFILE_FORM_ENABLE:Ljava/lang/String; = "profileFormEnable"

.field public static final REQUIRE_EMAIL:Ljava/lang/String; = "requireEmail"

.field public static final REQUIRE_NAME_AND_EMAIL:Ljava/lang/String; = "requireNameAndEmail"

.field public static final REVIEW_URL:Ljava/lang/String; = "reviewUrl"

.field public static final RUNTIME_VERSION:Ljava/lang/String; = "runtimeVersion"

.field public static final SDK_LANGUAGE:Ljava/lang/String; = "sdkLanguage"

.field public static final SDK_TYPE:Ljava/lang/String; = "sdkType"

.field public static final SHOULD_SHOW_CONVERSATION_HISTORY_AGENT:Ljava/lang/String; = "showConversationHistoryAgent"

.field public static final SHOW_AGENT_NAME:Ljava/lang/String; = "showAgentName"

.field public static final SHOW_CONVERSATION_INFO_SCREEN:Ljava/lang/String; = "showConversationInfoScreen"

.field public static final SHOW_CONVERSATION_RESOLUTION_QUESTION_AGENT:Ljava/lang/String; = "showConversationResolutionQuestionAgent"

.field public static final SHOW_CONVERSATION_RESOLUTION_QUESTION_API:Ljava/lang/String; = "showConversationResolutionQuestion"

.field public static final SHOW_SEARCH_ON_NEW_CONVERSATION:Ljava/lang/String; = "showSearchOnNewConversation"

.field public static final SUPPORT_NOTIFICATION_CHANNEL_ID:Ljava/lang/String; = "supportNotificationChannelId"

.field private static final TAG:Ljava/lang/String; = "Helpshift_SDKConfigDM"


# instance fields
.field private final domain:Lcom/helpshift/common/domain/Domain;

.field private final kvStore:Lcom/helpshift/common/platform/KVStore;

.field private final platform:Lcom/helpshift/common/platform/Platform;

.field private final responseParser:Lcom/helpshift/common/platform/network/ResponseParser;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, 0x3c

    .line 93
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->MINIMUM_PERIODIC_FETCH_INTERVAL:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V
    .locals 0

    .line 101
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 102
    iput-object p1, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->domain:Lcom/helpshift/common/domain/Domain;

    .line 103
    iput-object p2, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->platform:Lcom/helpshift/common/platform/Platform;

    .line 104
    invoke-interface {p2}, Lcom/helpshift/common/platform/Platform;->getResponseParser()Lcom/helpshift/common/platform/network/ResponseParser;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->responseParser:Lcom/helpshift/common/platform/network/ResponseParser;

    .line 105
    invoke-interface {p2}, Lcom/helpshift/common/platform/Platform;->getKVStore()Lcom/helpshift/common/platform/KVStore;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    return-void
.end method

.method private removeNullValues(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/io/Serializable;",
            ">;)V"
        }
    .end annotation

    .line 392
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 393
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 394
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 395
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 396
    invoke-interface {p1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private updateLastSuccessfulConfigFetchTime()V
    .locals 5

    .line 200
    iget-object v0, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "lastSuccessfulConfigFetchTime"

    invoke-interface {v0, v2, v1}, Lcom/helpshift/common/platform/KVStore;->setLong(Ljava/lang/String;Ljava/lang/Long;)V

    return-void
.end method

.method private updateUserConfig(Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/configuration/response/RootServerConfig;Lcom/helpshift/account/domainmodel/UserManagerDM;)V
    .locals 0

    .line 162
    iget-boolean p2, p2, Lcom/helpshift/configuration/response/RootServerConfig;->issueExists:Z

    invoke-virtual {p3, p1, p2}, Lcom/helpshift/account/domainmodel/UserManagerDM;->updateIssueExists(Lcom/helpshift/account/domainmodel/UserDM;Z)V

    return-void
.end method


# virtual methods
.method public fetchServerConfig(Lcom/helpshift/account/domainmodel/UserManagerDM;)Lcom/helpshift/configuration/response/RootServerConfig;
    .locals 5

    .line 119
    invoke-virtual {p1}, Lcom/helpshift/account/domainmodel/UserManagerDM;->getActiveUser()Lcom/helpshift/account/domainmodel/UserDM;

    move-result-object v0

    .line 120
    sget-object v1, Lcom/helpshift/common/domain/network/NetworkConstants;->SUPPORT_CONFIG_ROUTE:Ljava/lang/String;

    .line 121
    new-instance v2, Lcom/helpshift/common/domain/network/GETNetwork;

    iget-object v3, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v4, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v2, v1, v3, v4}, Lcom/helpshift/common/domain/network/GETNetwork;-><init>(Ljava/lang/String;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 122
    new-instance v3, Lcom/helpshift/common/domain/network/TSCorrectedNetwork;

    iget-object v4, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v3, v2, v4}, Lcom/helpshift/common/domain/network/TSCorrectedNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;Lcom/helpshift/common/platform/Platform;)V

    .line 123
    new-instance v2, Lcom/helpshift/common/domain/network/ETagNetwork;

    iget-object v4, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v2, v3, v4, v1}, Lcom/helpshift/common/domain/network/ETagNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;Lcom/helpshift/common/platform/Platform;Ljava/lang/String;)V

    .line 124
    new-instance v1, Lcom/helpshift/common/domain/network/GuardOKNetwork;

    invoke-direct {v1, v2}, Lcom/helpshift/common/domain/network/GuardOKNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;)V

    .line 125
    new-instance v2, Lcom/helpshift/common/platform/network/RequestData;

    invoke-static {p1}, Lcom/helpshift/common/domain/network/NetworkDataRequestUtil;->getUserRequestData(Lcom/helpshift/account/domainmodel/UserManagerDM;)Ljava/util/HashMap;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/helpshift/common/platform/network/RequestData;-><init>(Ljava/util/Map;)V

    .line 127
    :try_start_0
    invoke-interface {v1, v2}, Lcom/helpshift/common/domain/network/Network;->makeRequest(Lcom/helpshift/common/platform/network/RequestData;)Lcom/helpshift/common/platform/network/Response;

    move-result-object v1

    .line 129
    iget-object v2, v1, Lcom/helpshift/common/platform/network/Response;->responseString:Ljava/lang/String;
    :try_end_0
    .catch Lcom/helpshift/common/exception/RootAPIException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "Helpshift_SDKConfigDM"

    if-nez v2, :cond_0

    :try_start_1
    const-string p1, "SDK config data fetched but nothing to update."

    .line 130
    invoke-static {v3, p1}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    invoke-direct {p0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->updateLastSuccessfulConfigFetchTime()V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const-string v2, "SDK config data updated successfully"

    .line 135
    invoke-static {v3, v2}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v2, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->responseParser:Lcom/helpshift/common/platform/network/ResponseParser;

    iget-object v1, v1, Lcom/helpshift/common/platform/network/Response;->responseString:Ljava/lang/String;

    invoke-interface {v2, v1}, Lcom/helpshift/common/platform/network/ResponseParser;->parseConfigResponse(Ljava/lang/String;)Lcom/helpshift/configuration/response/RootServerConfig;

    move-result-object v1

    .line 138
    invoke-virtual {p0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->updateServerConfig(Lcom/helpshift/configuration/response/RootServerConfig;)V

    .line 139
    invoke-direct {p0, v0, v1, p1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->updateUserConfig(Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/configuration/response/RootServerConfig;Lcom/helpshift/account/domainmodel/UserManagerDM;)V

    .line 140
    invoke-direct {p0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->updateLastSuccessfulConfigFetchTime()V
    :try_end_1
    .catch Lcom/helpshift/common/exception/RootAPIException; {:try_start_1 .. :try_end_1} :catch_0

    return-object v1

    :catch_0
    move-exception p1

    .line 146
    iget-object v0, p1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    instance-of v0, v0, Lcom/helpshift/common/exception/NetworkException;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    check-cast v0, Lcom/helpshift/common/exception/NetworkException;

    iget v0, v0, Lcom/helpshift/common/exception/NetworkException;->serverStatusCode:I

    sget-object v1, Lcom/helpshift/common/domain/network/NetworkErrorCodes;->CONTENT_UNCHANGED:Ljava/lang/Integer;

    .line 147
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 148
    invoke-direct {p0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->updateLastSuccessfulConfigFetchTime()V

    .line 150
    :cond_1
    throw p1
.end method

.method public getBoolean(Ljava/lang/String;)Z
    .locals 3

    .line 209
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v0, "showAgentName"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_1
    const-string v0, "allowUserAttachments"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_2
    const-string v0, "defaultFallbackLanguageEnable"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_3
    const-string v0, "enableInAppNotification"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_4
    const-string v0, "enableTypingIndicatorAgent"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_5
    const-string v0, "showConversationResolutionQuestionAgent"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_6
    const-string v0, "profileFormEnable"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_7
    const-string v0, "conversationalIssueFiling"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    goto :goto_1

    :cond_0
    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    const/4 v1, 0x0

    goto :goto_2

    .line 220
    :pswitch_0
    iget-object v0, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "enableDefaultConversationalFiling"

    invoke-interface {v0, v2, v1}, Lcom/helpshift/common/platform/KVStore;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 226
    :goto_2
    :pswitch_1
    iget-object v0, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/helpshift/common/platform/KVStore;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6583db5c -> :sswitch_7
        -0x23465e10 -> :sswitch_6
        -0x193ffecd -> :sswitch_5
        -0x15d84a50 -> :sswitch_4
        -0x142b457c -> :sswitch_3
        0x4b466e1e -> :sswitch_2
        0x54dac45c -> :sswitch_1
        0x745f78b3 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getEnableContactUs()Lcom/helpshift/configuration/dto/RootApiConfig$EnableContactUs;
    .locals 3

    .line 365
    iget-object v0, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "enableContactUs"

    invoke-interface {v0, v2, v1}, Lcom/helpshift/common/platform/KVStore;->getInt(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/helpshift/configuration/dto/RootApiConfig$EnableContactUs;->fromInt(I)Lcom/helpshift/configuration/dto/RootApiConfig$EnableContactUs;

    move-result-object v0

    return-object v0
.end method

.method public getInt(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3

    .line 231
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x444e5b6

    const/4 v2, 0x1

    if-eq v0, v1, :cond_1

    const v1, 0x5285b578

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "breadcrumbLimit"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const-string v0, "debugLogLimit"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-eqz v0, :cond_3

    if-eq v0, v2, :cond_3

    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    const/16 v0, 0x64

    .line 234
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 240
    :goto_2
    iget-object v1, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    invoke-interface {v1, p1, v0}, Lcom/helpshift/common/platform/KVStore;->getInt(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public getLastSuccessfulConfigFetchTime()Ljava/lang/Long;
    .locals 3

    .line 204
    iget-object v0, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "lastSuccessfulConfigFetchTime"

    invoke-interface {v0, v2, v1}, Lcom/helpshift/common/platform/KVStore;->getLong(Ljava/lang/String;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getMinimumConversationDescriptionLength()I
    .locals 1

    .line 388
    iget-object v0, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v0}, Lcom/helpshift/common/platform/Platform;->getMinimumConversationDescriptionLength()I

    move-result v0

    return v0
.end method

.method public getPeriodicFetchInterval()Ljava/lang/Long;
    .locals 3

    const-wide/16 v0, 0x0

    .line 421
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 422
    iget-object v1, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    const-string v2, "periodicFetchInterval"

    invoke-interface {v1, v2, v0}, Lcom/helpshift/common/platform/KVStore;->getLong(Ljava/lang/String;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getPeriodicReview()Lcom/helpshift/configuration/response/PeriodicReview;
    .locals 5

    .line 261
    iget-object v0, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "periodicReviewEnabled"

    invoke-interface {v0, v3, v2}, Lcom/helpshift/common/platform/KVStore;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 262
    iget-object v2, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v3, "periodicReviewInterval"

    invoke-interface {v2, v3, v1}, Lcom/helpshift/common/platform/KVStore;->getInt(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 263
    iget-object v2, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    const-string v3, "periodicReviewType"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Lcom/helpshift/common/platform/KVStore;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 264
    new-instance v3, Lcom/helpshift/configuration/response/PeriodicReview;

    invoke-direct {v3, v0, v1, v2}, Lcom/helpshift/configuration/response/PeriodicReview;-><init>(ZILjava/lang/String;)V

    return-object v3
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 245
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x144c264e

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eq v0, v1, :cond_2

    const v1, 0x1d62f6f7

    if-eq v0, v1, :cond_1

    const v1, 0x741d1294

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "sdkType"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    goto :goto_1

    :cond_1
    const-string v0, "reviewUrl"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const-string v0, "sdkLanguage"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-eqz v0, :cond_5

    if-eq v0, v3, :cond_5

    if-eq v0, v2, :cond_4

    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    const-string v0, "android"

    goto :goto_2

    :cond_5
    const-string v0, ""

    .line 257
    :goto_2
    iget-object v1, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    invoke-interface {v1, p1, v0}, Lcom/helpshift/common/platform/KVStore;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public isHelpshiftBrandingDisabled()Z
    .locals 4

    .line 315
    iget-object v0, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "disableHelpshiftBranding"

    invoke-interface {v0, v3, v2}, Lcom/helpshift/common/platform/KVStore;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    const-string v3, "disableHelpshiftBrandingAgent"

    .line 316
    invoke-interface {v0, v3, v2}, Lcom/helpshift/common/platform/KVStore;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public setAppReviewed(Z)V
    .locals 2

    .line 369
    iget-object v0, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const-string v1, "app_reviewed"

    invoke-interface {v0, v1, p1}, Lcom/helpshift/common/platform/KVStore;->setBoolean(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-void
.end method

.method public setInstallCredentials(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 309
    iget-object v0, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    const-string v1, "apiKey"

    invoke-interface {v0, v1, p1}, Lcom/helpshift/common/platform/KVStore;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    iget-object p1, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    const-string v0, "domainName"

    invoke-interface {p1, v0, p2}, Lcom/helpshift/common/platform/KVStore;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    iget-object p1, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    const-string p2, "platformId"

    invoke-interface {p1, p2, p3}, Lcom/helpshift/common/platform/KVStore;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setSdkLanguage(Ljava/lang/String;)V
    .locals 2

    .line 373
    iget-object v0, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    const-string v1, "sdkLanguage"

    invoke-interface {v0, v1, p1}, Lcom/helpshift/common/platform/KVStore;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public shouldCreateConversationAnonymously()Z
    .locals 1

    const-string v0, "fullPrivacy"

    .line 381
    invoke-virtual {p0, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "requireNameAndEmail"

    .line 382
    invoke-virtual {p0, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "hideNameAndEmail"

    .line 383
    invoke-virtual {p0, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "profileFormEnable"

    .line 384
    invoke-virtual {p0, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public shouldEnableTypingIndicator()Z
    .locals 1

    const-string v0, "enableTypingIndicatorAgent"

    .line 377
    invoke-virtual {p0, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "enableTypingIndicator"

    invoke-virtual {p0, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public shouldShowConversationHistory()Z
    .locals 1

    const-string v0, "showConversationHistoryAgent"

    .line 410
    invoke-virtual {p0, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "conversationalIssueFiling"

    .line 412
    invoke-virtual {p0, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "fullPrivacy"

    .line 414
    invoke-virtual {p0, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public shouldShowConversationResolutionQuestion()Z
    .locals 1

    const-string v0, "showConversationResolutionQuestionAgent"

    .line 320
    invoke-virtual {p0, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "showConversationResolutionQuestion"

    .line 321
    invoke-virtual {p0, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public updateApiConfig(Lcom/helpshift/configuration/dto/RootApiConfig;)V
    .locals 4

    .line 335
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 336
    iget-object v1, p1, Lcom/helpshift/configuration/dto/RootApiConfig;->conversationPrefillText:Ljava/lang/String;

    const-string v2, "conversationPrefillText"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 342
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootApiConfig;->enableFullPrivacy:Ljava/lang/Boolean;

    const-string v3, "fullPrivacy"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootApiConfig;->hideNameAndEmail:Ljava/lang/Boolean;

    const-string v3, "hideNameAndEmail"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootApiConfig;->requireEmail:Ljava/lang/Boolean;

    const-string v3, "requireEmail"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootApiConfig;->showSearchOnNewConversation:Ljava/lang/Boolean;

    const-string v3, "showSearchOnNewConversation"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootApiConfig;->gotoConversationAfterContactUs:Ljava/lang/Boolean;

    const-string v3, "gotoConversationAfterContactUs"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootApiConfig;->showConversationResolutionQuestion:Ljava/lang/Boolean;

    const-string v3, "showConversationResolutionQuestion"

    .line 348
    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootApiConfig;->showConversationInfoScreen:Ljava/lang/Boolean;

    const-string v3, "showConversationInfoScreen"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootApiConfig;->enableTypingIndicator:Ljava/lang/Boolean;

    const-string v3, "enableTypingIndicator"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootApiConfig;->enableContactUs:Lcom/helpshift/configuration/dto/RootApiConfig$EnableContactUs;

    if-eqz v2, :cond_0

    .line 353
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootApiConfig;->enableContactUs:Lcom/helpshift/configuration/dto/RootApiConfig$EnableContactUs;

    invoke-virtual {v2}, Lcom/helpshift/configuration/dto/RootApiConfig$EnableContactUs;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "enableContactUs"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    :cond_0
    iget-object p1, p1, Lcom/helpshift/configuration/dto/RootApiConfig;->enableDefaultConversationalFiling:Ljava/lang/Boolean;

    const-string v2, "enableDefaultConversationalFiling"

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    invoke-direct {p0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->removeNullValues(Ljava/util/Map;)V

    .line 360
    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 361
    iget-object p1, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    invoke-interface {p1, v1}, Lcom/helpshift/common/platform/KVStore;->setKeyValues(Ljava/util/Map;)V

    return-void
.end method

.method public updateApiConfig(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 326
    new-instance v0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;

    invoke-direct {v0}, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;-><init>()V

    invoke-virtual {v0, p1}, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->applyMap(Ljava/util/Map;)Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->build()Lcom/helpshift/configuration/dto/RootApiConfig;

    move-result-object p1

    .line 327
    invoke-virtual {p0, p1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->updateApiConfig(Lcom/helpshift/configuration/dto/RootApiConfig;)V

    return-void
.end method

.method public updateInstallConfig(Lcom/helpshift/configuration/dto/RootInstallConfig;)V
    .locals 4

    .line 279
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 280
    iget-object v1, p1, Lcom/helpshift/configuration/dto/RootInstallConfig;->supportNotificationChannelId:Ljava/lang/String;

    const-string v2, "supportNotificationChannelId"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    iget-object v1, p1, Lcom/helpshift/configuration/dto/RootInstallConfig;->fontPath:Ljava/lang/String;

    const-string v2, "fontPath"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 287
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootInstallConfig;->enableInAppNotification:Ljava/lang/Boolean;

    const-string v3, "enableInAppNotification"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootInstallConfig;->enableDefaultFallbackLanguage:Ljava/lang/Boolean;

    const-string v3, "defaultFallbackLanguageEnable"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootInstallConfig;->enableInboxPolling:Ljava/lang/Boolean;

    const-string v3, "inboxPollingEnable"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootInstallConfig;->enableNotificationMute:Ljava/lang/Boolean;

    const-string v3, "notificationMute"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootInstallConfig;->disableAnimations:Ljava/lang/Boolean;

    const-string v3, "disableAnimations"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootInstallConfig;->disableHelpshiftBranding:Ljava/lang/Boolean;

    const-string v3, "disableHelpshiftBranding"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootInstallConfig;->disableErrorLogging:Ljava/lang/Boolean;

    const-string v3, "disableErrorLogging"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootInstallConfig;->disableAppLaunchEvent:Ljava/lang/Boolean;

    const-string v3, "disableAppLaunchEvent"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootInstallConfig;->notificationSound:Ljava/lang/Integer;

    const-string v3, "notificationSoundId"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootInstallConfig;->notificationIcon:Ljava/lang/Integer;

    const-string v3, "notificationIconId"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootInstallConfig;->largeNotificationIcon:Ljava/lang/Integer;

    const-string v3, "notificationLargeIconId"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootInstallConfig;->sdkType:Ljava/lang/String;

    const-string v3, "sdkType"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    iget-object v2, p1, Lcom/helpshift/configuration/dto/RootInstallConfig;->pluginVersion:Ljava/lang/String;

    const-string v3, "pluginVersion"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    iget-object p1, p1, Lcom/helpshift/configuration/dto/RootInstallConfig;->runtimeVersion:Ljava/lang/String;

    const-string v2, "runtimeVersion"

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    invoke-direct {p0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->removeNullValues(Ljava/util/Map;)V

    .line 304
    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 305
    iget-object p1, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    invoke-interface {p1, v1}, Lcom/helpshift/common/platform/KVStore;->setKeyValues(Ljava/util/Map;)V

    return-void
.end method

.method public updateInstallConfig(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 269
    new-instance v0, Lcom/helpshift/configuration/dto/RootInstallConfig$RootInstallConfigBuilder;

    invoke-direct {v0}, Lcom/helpshift/configuration/dto/RootInstallConfig$RootInstallConfigBuilder;-><init>()V

    .line 270
    invoke-virtual {v0, p1}, Lcom/helpshift/configuration/dto/RootInstallConfig$RootInstallConfigBuilder;->applyMap(Ljava/util/Map;)Lcom/helpshift/configuration/dto/RootInstallConfig$RootInstallConfigBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/helpshift/configuration/dto/RootInstallConfig$RootInstallConfigBuilder;->build()Lcom/helpshift/configuration/dto/RootInstallConfig;

    move-result-object p1

    .line 271
    invoke-virtual {p0, p1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->updateInstallConfig(Lcom/helpshift/configuration/dto/RootInstallConfig;)V

    return-void
.end method

.method public updateServerConfig(Lcom/helpshift/configuration/response/RootServerConfig;)V
    .locals 4

    .line 166
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 167
    iget-boolean v1, p1, Lcom/helpshift/configuration/response/RootServerConfig;->requireNameAndEmail:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "requireNameAndEmail"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    iget-boolean v1, p1, Lcom/helpshift/configuration/response/RootServerConfig;->profileFormEnable:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "profileFormEnable"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    iget-boolean v1, p1, Lcom/helpshift/configuration/response/RootServerConfig;->showAgentName:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "showAgentName"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    iget-boolean v1, p1, Lcom/helpshift/configuration/response/RootServerConfig;->customerSatisfactionSurvey:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "customerSatisfactionSurvey"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    iget-boolean v1, p1, Lcom/helpshift/configuration/response/RootServerConfig;->disableInAppConversation:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "disableInAppConversation"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    iget-boolean v1, p1, Lcom/helpshift/configuration/response/RootServerConfig;->disableHelpshiftBranding:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "disableHelpshiftBrandingAgent"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    iget v1, p1, Lcom/helpshift/configuration/response/RootServerConfig;->debugLogLimit:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "debugLogLimit"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    iget v1, p1, Lcom/helpshift/configuration/response/RootServerConfig;->breadcrumbLimit:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "breadcrumbLimit"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    iget-object v1, p1, Lcom/helpshift/configuration/response/RootServerConfig;->reviewUrl:Ljava/lang/String;

    const-string v2, "reviewUrl"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    iget-object v1, p1, Lcom/helpshift/configuration/response/RootServerConfig;->periodicReview:Lcom/helpshift/configuration/response/PeriodicReview;

    if-nez v1, :cond_0

    .line 179
    new-instance v1, Lcom/helpshift/configuration/response/PeriodicReview;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v3, v3, v2}, Lcom/helpshift/configuration/response/PeriodicReview;-><init>(ZILjava/lang/String;)V

    .line 182
    :cond_0
    iget-boolean v2, v1, Lcom/helpshift/configuration/response/PeriodicReview;->isEnabled:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "periodicReviewEnabled"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    iget v2, v1, Lcom/helpshift/configuration/response/PeriodicReview;->interval:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "periodicReviewInterval"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    iget-object v1, v1, Lcom/helpshift/configuration/response/PeriodicReview;->type:Ljava/lang/String;

    const-string v2, "periodicReviewType"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    iget-object v1, p1, Lcom/helpshift/configuration/response/RootServerConfig;->conversationGreetingMessage:Ljava/lang/String;

    const-string v2, "conversationGreetingMessage"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    iget-boolean v1, p1, Lcom/helpshift/configuration/response/RootServerConfig;->conversationalIssueFiling:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "conversationalIssueFiling"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    iget-boolean v1, p1, Lcom/helpshift/configuration/response/RootServerConfig;->enableTypingIndicator:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "enableTypingIndicatorAgent"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    iget-boolean v1, p1, Lcom/helpshift/configuration/response/RootServerConfig;->showConversationResolutionQuestion:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "showConversationResolutionQuestionAgent"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    iget-boolean v1, p1, Lcom/helpshift/configuration/response/RootServerConfig;->shouldShowConversationHistory:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "showConversationHistoryAgent"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    iget-boolean v1, p1, Lcom/helpshift/configuration/response/RootServerConfig;->allowUserAttachments:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "allowUserAttachments"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    iget-object p1, p1, Lcom/helpshift/configuration/response/RootServerConfig;->periodicFetchInterval:Ljava/lang/Long;

    const-string v1, "periodicFetchInterval"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    iget-object p1, p0, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->kvStore:Lcom/helpshift/common/platform/KVStore;

    invoke-interface {p1, v0}, Lcom/helpshift/common/platform/KVStore;->setKeyValues(Ljava/util/Map;)V

    .line 195
    invoke-virtual {p0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->setChanged()V

    .line 196
    invoke-virtual {p0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->notifyObservers()V

    return-void
.end method
