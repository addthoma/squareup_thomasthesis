.class public Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;
.super Ljava/lang/Object;
.source "RootApiConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/helpshift/configuration/dto/RootApiConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RootApiConfigBuilder"
.end annotation


# instance fields
.field private conversationPrefillText:Ljava/lang/String;

.field private enableContactUs:Lcom/helpshift/configuration/dto/RootApiConfig$EnableContactUs;

.field private enableDefaultConversationalFiling:Ljava/lang/Boolean;

.field private enableFullPrivacy:Ljava/lang/Boolean;

.field private enableTypingIndicator:Ljava/lang/Boolean;

.field private gotoConversationAfterContactUs:Ljava/lang/Boolean;

.field private hideNameAndEmail:Ljava/lang/Boolean;

.field private requireEmail:Ljava/lang/Boolean;

.field private showConversationInfoScreen:Ljava/lang/Boolean;

.field private showConversationResolutionQuestion:Ljava/lang/Boolean;

.field private showSearchOnNewConversation:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public applyMap(Ljava/util/Map;)Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;"
        }
    .end annotation

    const-string v0, "enableContactUs"

    .line 84
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 85
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/helpshift/configuration/dto/RootApiConfig$EnableContactUs;->fromInt(I)Lcom/helpshift/configuration/dto/RootApiConfig$EnableContactUs;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->enableContactUs:Lcom/helpshift/configuration/dto/RootApiConfig$EnableContactUs;

    :cond_0
    const-string v0, "gotoConversationAfterContactUs"

    .line 87
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 88
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->gotoConversationAfterContactUs:Ljava/lang/Boolean;

    goto :goto_0

    :cond_1
    const-string v0, "gotoCoversationAfterContactUs"

    .line 90
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 91
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->gotoConversationAfterContactUs:Ljava/lang/Boolean;

    :cond_2
    :goto_0
    const-string v0, "requireEmail"

    .line 93
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 94
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->requireEmail:Ljava/lang/Boolean;

    :cond_3
    const-string v0, "hideNameAndEmail"

    .line 96
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 97
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->hideNameAndEmail:Ljava/lang/Boolean;

    :cond_4
    const-string v0, "enableFullPrivacy"

    .line 99
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 100
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->enableFullPrivacy:Ljava/lang/Boolean;

    :cond_5
    const-string v0, "showSearchOnNewConversation"

    .line 102
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 103
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->showSearchOnNewConversation:Ljava/lang/Boolean;

    :cond_6
    const-string v0, "showConversationResolutionQuestion"

    .line 105
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 106
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->showConversationResolutionQuestion:Ljava/lang/Boolean;

    :cond_7
    const-string v0, "conversationPrefillText"

    .line 108
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 109
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 110
    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 111
    iput-object v0, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->conversationPrefillText:Ljava/lang/String;

    :cond_8
    const-string v0, "showConversationInfoScreen"

    .line 114
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    .line 115
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->showConversationInfoScreen:Ljava/lang/Boolean;

    :cond_9
    const-string v0, "enableTypingIndicator"

    .line 117
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 118
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->enableTypingIndicator:Ljava/lang/Boolean;

    :cond_a
    const-string v0, "enableDefaultConversationalFiling"

    .line 120
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 121
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->enableDefaultConversationalFiling:Ljava/lang/Boolean;

    :cond_b
    return-object p0
.end method

.method public build()Lcom/helpshift/configuration/dto/RootApiConfig;
    .locals 13

    .line 127
    new-instance v12, Lcom/helpshift/configuration/dto/RootApiConfig;

    iget-object v1, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->gotoConversationAfterContactUs:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->requireEmail:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->hideNameAndEmail:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->enableFullPrivacy:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->showSearchOnNewConversation:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->showConversationResolutionQuestion:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->enableContactUs:Lcom/helpshift/configuration/dto/RootApiConfig$EnableContactUs;

    iget-object v8, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->conversationPrefillText:Ljava/lang/String;

    iget-object v9, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->showConversationInfoScreen:Ljava/lang/Boolean;

    iget-object v10, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->enableTypingIndicator:Ljava/lang/Boolean;

    iget-object v11, p0, Lcom/helpshift/configuration/dto/RootApiConfig$RootApiConfigBuilder;->enableDefaultConversationalFiling:Ljava/lang/Boolean;

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/helpshift/configuration/dto/RootApiConfig;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/helpshift/configuration/dto/RootApiConfig$EnableContactUs;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    return-object v12
.end method
