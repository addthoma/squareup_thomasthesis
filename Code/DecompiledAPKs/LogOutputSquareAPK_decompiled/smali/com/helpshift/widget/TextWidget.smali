.class public abstract Lcom/helpshift/widget/TextWidget;
.super Lcom/helpshift/widget/Widget;
.source "TextWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/helpshift/widget/TextWidget$TextWidgetError;
    }
.end annotation


# static fields
.field public static final specialCharactersPattern:Ljava/util/regex/Pattern;


# instance fields
.field private error:Lcom/helpshift/widget/TextWidget$TextWidgetError;

.field private text:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "\\W+"

    .line 11
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/helpshift/widget/TextWidget;->specialCharactersPattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/helpshift/widget/Widget;-><init>()V

    return-void
.end method


# virtual methods
.method public getError()Lcom/helpshift/widget/TextWidget$TextWidgetError;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/helpshift/widget/TextWidget;->error:Lcom/helpshift/widget/TextWidget$TextWidgetError;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/helpshift/widget/TextWidget;->text:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method protected setError(Lcom/helpshift/widget/TextWidget$TextWidgetError;)V
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/helpshift/widget/TextWidget;->error:Lcom/helpshift/widget/TextWidget$TextWidgetError;

    .line 22
    invoke-virtual {p0}, Lcom/helpshift/widget/TextWidget;->notifyChanged()V

    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/helpshift/widget/TextWidget;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 31
    iput-object p1, p0, Lcom/helpshift/widget/TextWidget;->text:Ljava/lang/String;

    .line 32
    invoke-virtual {p0}, Lcom/helpshift/widget/TextWidget;->getError()Lcom/helpshift/widget/TextWidget$TextWidgetError;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 33
    invoke-virtual {p0, p1}, Lcom/helpshift/widget/TextWidget;->setError(Lcom/helpshift/widget/TextWidget$TextWidgetError;)V

    :cond_0
    return-void
.end method

.method public abstract validateText()V
.end method
