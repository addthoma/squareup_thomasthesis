.class public Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;
.super Lcom/helpshift/support/fragments/MainFragment;
.source "ScreenshotPreviewFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/helpshift/common/domain/AttachmentFileManagerDM$Listener;
.implements Lcom/helpshift/conversation/activeconversation/ScreenshotPreviewRenderer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$Modes;,
        Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$LaunchSource;,
        Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$ScreenshotAction;
    }
.end annotation


# static fields
.field public static final FRAGMENT_TAG:Ljava/lang/String; = "ScreenshotPreviewFragment"

.field public static final KEY_MESSAGE_REFERS_ID:Ljava/lang/String; = "key_refers_id"

.field public static final KEY_SCREENSHOT_MODE:Ljava/lang/String; = "key_screenshot_mode"

.field private static final screenType:Lcom/helpshift/support/util/AppSessionConstants$Screen;


# instance fields
.field private attachmentMessageRefersId:Ljava/lang/String;

.field private buttonsContainer:Landroid/view/View;

.field private buttonsSeparator:Landroid/view/View;

.field imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

.field launchSource:Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$LaunchSource;

.field private mode:I

.field progressBar:Landroid/widget/ProgressBar;

.field private screenshotPreview:Landroid/widget/ImageView;

.field private screenshotPreviewListener:Lcom/helpshift/support/contracts/ScreenshotPreviewListener;

.field private screenshotPreviewVM:Lcom/helpshift/conversation/viewmodel/ScreenshotPreviewVM;

.field private secondaryButton:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    sget-object v0, Lcom/helpshift/support/util/AppSessionConstants$Screen;->SCREENSHOT_PREVIEW:Lcom/helpshift/support/util/AppSessionConstants$Screen;

    sput-object v0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->screenType:Lcom/helpshift/support/util/AppSessionConstants$Screen;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/helpshift/support/fragments/MainFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Lcom/helpshift/support/contracts/ScreenshotPreviewListener;)Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;
    .locals 1

    .line 52
    new-instance v0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;

    invoke-direct {v0}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;-><init>()V

    .line 53
    iput-object p0, v0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->screenshotPreviewListener:Lcom/helpshift/support/contracts/ScreenshotPreviewListener;

    return-object v0
.end method

.method private setScreenshotPreview()V
    .locals 3

    .line 165
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 166
    iget-object v0, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    if-nez v0, :cond_1

    .line 167
    iget-object v0, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->screenshotPreviewListener:Lcom/helpshift/support/contracts/ScreenshotPreviewListener;

    if-eqz v0, :cond_0

    .line 168
    invoke-interface {v0}, Lcom/helpshift/support/contracts/ScreenshotPreviewListener;->removeScreenshotPreviewFragment()V

    :cond_0
    return-void

    .line 172
    :cond_1
    iget-object v0, v0, Lcom/helpshift/conversation/dto/ImagePickerFile;->filePath:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 174
    iget-object v0, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    iget-object v0, v0, Lcom/helpshift/conversation/dto/ImagePickerFile;->filePath:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->renderScreenshotPreview(Ljava/lang/String;)V

    goto :goto_0

    .line 176
    :cond_2
    iget-object v0, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    iget-object v0, v0, Lcom/helpshift/conversation/dto/ImagePickerFile;->transientUri:Ljava/lang/Object;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 179
    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->toggleProgressBarViewsVisibility(Z)V

    .line 180
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getAttachmentFileManagerDM()Lcom/helpshift/common/domain/AttachmentFileManagerDM;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    iget-object v2, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->attachmentMessageRefersId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p0}, Lcom/helpshift/common/domain/AttachmentFileManagerDM;->compressAndCopyScreenshot(Lcom/helpshift/conversation/dto/ImagePickerFile;Ljava/lang/String;Lcom/helpshift/common/domain/AttachmentFileManagerDM$Listener;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private static setSecondaryButtonText(Landroid/widget/Button;I)V
    .locals 2

    .line 58
    invoke-virtual {p0}, Landroid/widget/Button;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    const-string p1, ""

    goto :goto_0

    .line 68
    :cond_0
    sget p1, Lcom/helpshift/R$string;->hs__send_msg_btn:I

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 65
    :cond_1
    sget p1, Lcom/helpshift/R$string;->hs__screenshot_remove:I

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 62
    :cond_2
    sget p1, Lcom/helpshift/R$string;->hs__screenshot_add:I

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 74
    :goto_0
    invoke-virtual {p0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public deleteAttachmentLocalCopy()V
    .locals 2

    .line 159
    iget-object v0, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->launchSource:Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$LaunchSource;

    sget-object v1, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$LaunchSource;->GALLERY_APP:Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$LaunchSource;

    if-ne v0, v1, :cond_0

    .line 160
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getAttachmentFileManagerDM()Lcom/helpshift/common/domain/AttachmentFileManagerDM;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/AttachmentFileManagerDM;->deleteAttachmentLocalCopy(Lcom/helpshift/conversation/dto/ImagePickerFile;)V

    :cond_0
    return-void
.end method

.method public onAuthenticationFailure()V
    .locals 1

    .line 272
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/helpshift/support/fragments/SupportFragment;

    invoke-virtual {v0}, Lcom/helpshift/support/fragments/SupportFragment;->getSupportController()Lcom/helpshift/support/controllers/SupportController;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 274
    invoke-virtual {v0}, Lcom/helpshift/support/controllers/SupportController;->onAuthenticationFailure()V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 199
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    .line 200
    sget v0, Lcom/helpshift/R$id;->secondary_button:I

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    if-eqz v0, :cond_3

    .line 201
    iget p1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->mode:I

    if-eq p1, v1, :cond_2

    if-eq p1, v2, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    goto :goto_0

    .line 206
    :cond_0
    iget-object p1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->screenshotPreviewListener:Lcom/helpshift/support/contracts/ScreenshotPreviewListener;

    iget-object v1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->attachmentMessageRefersId:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/helpshift/support/contracts/ScreenshotPreviewListener;->sendScreenshot(Lcom/helpshift/conversation/dto/ImagePickerFile;Ljava/lang/String;)V

    goto :goto_0

    .line 210
    :cond_1
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object p1

    invoke-interface {p1}, Lcom/helpshift/CoreApi;->getAttachmentFileManagerDM()Lcom/helpshift/common/domain/AttachmentFileManagerDM;

    move-result-object p1

    iget-object v0, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    invoke-virtual {p1, v0}, Lcom/helpshift/common/domain/AttachmentFileManagerDM;->deleteAttachmentLocalCopy(Lcom/helpshift/conversation/dto/ImagePickerFile;)V

    .line 211
    iget-object p1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->screenshotPreviewListener:Lcom/helpshift/support/contracts/ScreenshotPreviewListener;

    invoke-interface {p1}, Lcom/helpshift/support/contracts/ScreenshotPreviewListener;->removeScreenshot()V

    goto :goto_0

    .line 203
    :cond_2
    iget-object p1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->screenshotPreviewListener:Lcom/helpshift/support/contracts/ScreenshotPreviewListener;

    invoke-interface {p1, v0}, Lcom/helpshift/support/contracts/ScreenshotPreviewListener;->addScreenshot(Lcom/helpshift/conversation/dto/ImagePickerFile;)V

    goto :goto_0

    .line 215
    :cond_3
    sget v0, Lcom/helpshift/R$id;->change:I

    if-ne p1, v0, :cond_5

    .line 216
    iget p1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->mode:I

    if-ne p1, v2, :cond_4

    .line 217
    iput v1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->mode:I

    .line 219
    :cond_4
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object p1

    invoke-interface {p1}, Lcom/helpshift/CoreApi;->getAttachmentFileManagerDM()Lcom/helpshift/common/domain/AttachmentFileManagerDM;

    move-result-object p1

    iget-object v0, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    invoke-virtual {p1, v0}, Lcom/helpshift/common/domain/AttachmentFileManagerDM;->deleteAttachmentLocalCopy(Lcom/helpshift/conversation/dto/ImagePickerFile;)V

    .line 220
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 221
    iget v0, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->mode:I

    const-string v1, "key_screenshot_mode"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 222
    iget-object v0, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->attachmentMessageRefersId:Ljava/lang/String;

    const-string v1, "key_refers_id"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->screenshotPreviewListener:Lcom/helpshift/support/contracts/ScreenshotPreviewListener;

    invoke-interface {v0, p1}, Lcom/helpshift/support/contracts/ScreenshotPreviewListener;->changeScreenshot(Landroid/os/Bundle;)V

    :cond_5
    :goto_0
    return-void
.end method

.method public onCompressAndCopyFailure(Lcom/helpshift/common/exception/RootAPIException;)V
    .locals 1

    .line 229
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 230
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    new-instance v0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$1;

    invoke-direct {v0, p0}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$1;-><init>(Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;)V

    invoke-virtual {p1, v0}, Landroidx/fragment/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public onCompressAndCopySuccess(Lcom/helpshift/conversation/dto/ImagePickerFile;)V
    .locals 2

    .line 244
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 245
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$2;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$2;-><init>(Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;Lcom/helpshift/conversation/dto/ImagePickerFile;)V

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .line 92
    sget p3, Lcom/helpshift/R$layout;->hs__screenshot_preview_fragment:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onDestroyView()V
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->screenshotPreviewVM:Lcom/helpshift/conversation/viewmodel/ScreenshotPreviewVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ScreenshotPreviewVM;->unregisterRenderer()V

    .line 125
    invoke-super {p0}, Lcom/helpshift/support/fragments/MainFragment;->onDestroyView()V

    return-void
.end method

.method public onPause()V
    .locals 1

    .line 137
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/helpshift/support/util/SnackbarUtil;->hideSnackbar(Landroid/view/View;)V

    .line 138
    invoke-super {p0}, Lcom/helpshift/support/fragments/MainFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 2

    .line 115
    invoke-super {p0}, Lcom/helpshift/support/fragments/MainFragment;->onResume()V

    .line 116
    iget-object v0, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->secondaryButton:Landroid/widget/Button;

    iget v1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->mode:I

    invoke-static {v0, v1}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->setSecondaryButtonText(Landroid/widget/Button;I)V

    .line 117
    invoke-direct {p0}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->setScreenshotPreview()V

    .line 118
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->getView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 119
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    return-void
.end method

.method public onStart()V
    .locals 3

    .line 130
    invoke-super {p0}, Lcom/helpshift/support/fragments/MainFragment;->onStart()V

    .line 131
    invoke-static {}, Lcom/helpshift/support/storage/IMAppSessionStorage;->getInstance()Lcom/helpshift/support/storage/IMAppSessionStorage;

    move-result-object v0

    sget-object v1, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->screenType:Lcom/helpshift/support/util/AppSessionConstants$Screen;

    const-string v2, "current_open_screen"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/support/storage/IMAppSessionStorage;->set(Ljava/lang/String;Ljava/io/Serializable;)Z

    return-void
.end method

.method public onStop()V
    .locals 3

    .line 143
    invoke-super {p0}, Lcom/helpshift/support/fragments/MainFragment;->onStop()V

    .line 144
    invoke-static {}, Lcom/helpshift/support/storage/IMAppSessionStorage;->getInstance()Lcom/helpshift/support/storage/IMAppSessionStorage;

    move-result-object v0

    const-string v1, "current_open_screen"

    .line 145
    invoke-virtual {v0, v1}, Lcom/helpshift/support/storage/IMAppSessionStorage;->get(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/helpshift/support/util/AppSessionConstants$Screen;

    if-eqz v0, :cond_0

    .line 146
    sget-object v2, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->screenType:Lcom/helpshift/support/util/AppSessionConstants$Screen;

    invoke-virtual {v0, v2}, Lcom/helpshift/support/util/AppSessionConstants$Screen;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    invoke-static {}, Lcom/helpshift/support/storage/IMAppSessionStorage;->getInstance()Lcom/helpshift/support/storage/IMAppSessionStorage;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/helpshift/support/storage/IMAppSessionStorage;->removeKey(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .line 97
    invoke-super {p0, p1, p2}, Lcom/helpshift/support/fragments/MainFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 98
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object p2

    invoke-interface {p2, p0}, Lcom/helpshift/CoreApi;->getScreenshotPreviewModel(Lcom/helpshift/conversation/activeconversation/ScreenshotPreviewRenderer;)Lcom/helpshift/conversation/viewmodel/ScreenshotPreviewVM;

    move-result-object p2

    iput-object p2, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->screenshotPreviewVM:Lcom/helpshift/conversation/viewmodel/ScreenshotPreviewVM;

    .line 99
    sget p2, Lcom/helpshift/R$id;->screenshot_preview:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->screenshotPreview:Landroid/widget/ImageView;

    .line 101
    sget p2, Lcom/helpshift/R$id;->change:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    .line 102
    invoke-virtual {p2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    sget p2, Lcom/helpshift/R$id;->secondary_button:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    iput-object p2, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->secondaryButton:Landroid/widget/Button;

    .line 105
    iget-object p2, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->secondaryButton:Landroid/widget/Button;

    invoke-virtual {p2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    sget p2, Lcom/helpshift/R$id;->screenshot_loading_indicator:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ProgressBar;

    iput-object p2, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 109
    sget p2, Lcom/helpshift/R$id;->button_containers:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->buttonsContainer:Landroid/view/View;

    .line 110
    sget p2, Lcom/helpshift/R$id;->buttons_separator:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->buttonsSeparator:Landroid/view/View;

    return-void
.end method

.method renderScreenshotPreview(Ljava/lang/String;)V
    .locals 1

    const/4 v0, -0x1

    .line 188
    invoke-static {p1, v0}, Lcom/helpshift/support/util/AttachmentUtil;->getBitmap(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 190
    iget-object v0, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->screenshotPreview:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 192
    :cond_0
    iget-object p1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->screenshotPreviewListener:Lcom/helpshift/support/contracts/ScreenshotPreviewListener;

    if-eqz p1, :cond_1

    .line 193
    invoke-interface {p1}, Lcom/helpshift/support/contracts/ScreenshotPreviewListener;->removeScreenshotPreviewFragment()V

    :cond_1
    :goto_0
    return-void
.end method

.method public setParams(Landroid/os/Bundle;Lcom/helpshift/conversation/dto/ImagePickerFile;Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$LaunchSource;)V
    .locals 1

    const-string v0, "key_screenshot_mode"

    .line 78
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->mode:I

    const-string v0, "key_refers_id"

    .line 79
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->attachmentMessageRefersId:Ljava/lang/String;

    .line 80
    iput-object p2, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    .line 81
    iput-object p3, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->launchSource:Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$LaunchSource;

    .line 82
    invoke-direct {p0}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->setScreenshotPreview()V

    return-void
.end method

.method public setScreenshotPreviewListener(Lcom/helpshift/support/contracts/ScreenshotPreviewListener;)V
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->screenshotPreviewListener:Lcom/helpshift/support/contracts/ScreenshotPreviewListener;

    return-void
.end method

.method public shouldRefreshMenu()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method toggleProgressBarViewsVisibility(Z)V
    .locals 2

    const/4 v0, 0x0

    const/16 v1, 0x8

    if-eqz p1, :cond_0

    .line 257
    iget-object p1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 258
    iget-object p1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->buttonsContainer:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 259
    iget-object p1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->buttonsSeparator:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 260
    iget-object p1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->screenshotPreview:Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 263
    :cond_0
    iget-object p1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 264
    iget-object p1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->buttonsContainer:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 265
    iget-object p1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->buttonsSeparator:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 266
    iget-object p1, p0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->screenshotPreview:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void
.end method
