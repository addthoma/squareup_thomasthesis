.class public Lcom/helpshift/support/fragments/SingleQuestionFragment;
.super Lcom/helpshift/support/fragments/MainFragment;
.source "SingleQuestionFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/helpshift/support/webkit/CustomWebViewClient$CustomWebViewClientListeners;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/helpshift/support/fragments/SingleQuestionFragment$Failure;,
        Lcom/helpshift/support/fragments/SingleQuestionFragment$Success;,
        Lcom/helpshift/support/fragments/SingleQuestionFragment$SingleQuestionModes;,
        Lcom/helpshift/support/fragments/SingleQuestionFragment$QuestionReadListener;
    }
.end annotation


# static fields
.field public static final BUNDLE_ARG_QUESTION_LANGUAGE:Ljava/lang/String; = "questionLanguage"

.field public static final BUNDLE_ARG_QUESTION_PUBLISH_ID:Ljava/lang/String; = "questionPublishId"

.field private static final TAG:Ljava/lang/String; = "Helpshift_SingleQstn"


# instance fields
.field private contactUsButton:Landroid/widget/Button;

.field private data:Lcom/helpshift/support/HSApiData;

.field private decomp:Z

.field eventSent:Z

.field private isHelpful:I

.field private isHighlighted:Z

.field private noButton:Landroid/widget/Button;

.field private progressBar:Landroid/view/View;

.field private question:Lcom/helpshift/support/Faq;

.field private questionFooter:Landroid/view/View;

.field private questionFooterMessage:Landroid/widget/TextView;

.field private questionReadListener:Lcom/helpshift/support/fragments/SingleQuestionFragment$QuestionReadListener;

.field private showRootLayoutInsideCardView:Z

.field private singleQuestionMode:I

.field private supportController:Lcom/helpshift/support/controllers/SupportController;

.field private textColor:Ljava/lang/String;

.field private textColorLink:Ljava/lang/String;

.field private webView:Lcom/helpshift/support/webkit/CustomWebView;

.field private yesButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 46
    invoke-direct {p0}, Lcom/helpshift/support/fragments/MainFragment;-><init>()V

    const/4 v0, 0x1

    .line 54
    iput v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->singleQuestionMode:I

    const/4 v0, 0x0

    .line 69
    iput v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->isHelpful:I

    .line 70
    iput-boolean v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->showRootLayoutInsideCardView:Z

    return-void
.end method

.method static synthetic access$000(Lcom/helpshift/support/fragments/SingleQuestionFragment;)Lcom/helpshift/support/Faq;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->question:Lcom/helpshift/support/Faq;

    return-object p0
.end method

.method private getColorsFromTheme(Landroid/content/Context;)V
    .locals 2

    .line 127
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const v0, 0x1010435

    goto :goto_0

    :cond_0
    const v0, 0x101009b

    :goto_0
    const v1, 0x1010036

    .line 130
    invoke-static {p1, v1}, Lcom/helpshift/util/Styles;->getHexColor(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->textColor:Ljava/lang/String;

    .line 131
    invoke-static {p1, v0}, Lcom/helpshift/util/Styles;->getHexColor(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->textColorLink:Ljava/lang/String;

    return-void
.end method

.method private getStyledBody(Lcom/helpshift/support/Faq;)Ljava/lang/String;
    .locals 10

    .line 261
    invoke-static {}, Lcom/helpshift/views/FontApplier;->getFontPath()Ljava/lang/String;

    move-result-object v0

    .line 264
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, ""

    if-nez v1, :cond_0

    .line 265
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file:///android_asset/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 266
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "@font-face {    font-family: custom;    src: url(\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\');}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v0, "font-family: custom, sans-serif;"

    goto :goto_0

    :cond_0
    move-object v0, v2

    .line 274
    :goto_0
    iget-object v1, p1, Lcom/helpshift/support/Faq;->body:Ljava/lang/String;

    .line 275
    iget-object v3, p1, Lcom/helpshift/support/Faq;->title:Ljava/lang/String;

    const-string v4, "<iframe"

    .line 277
    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    :try_start_0
    const-string v4, "https"

    const-string v5, "http"

    .line 279
    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v4

    const-string v5, "Helpshift_SingleQstn"

    const-string v6, "Error replacing https in bodyText"

    .line 282
    invoke-static {v5, v6, v4}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 286
    :cond_1
    :goto_1
    iget-object p1, p1, Lcom/helpshift/support/Faq;->is_rtl:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 287
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v4, "<html dir=\"rtl\">"

    invoke-direct {p1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 290
    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    const-string v4, "<html>"

    invoke-direct {p1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 292
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v5, 0x10

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "px "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v6, 0x10

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v6, 0x60

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v5, 0x10

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "px;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "<head>"

    .line 293
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "    <style type=\'text/css\'>"

    .line 294
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "        img,"

    .line 296
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "        object,"

    .line 297
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "        embed {"

    .line 298
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "            max-width: 100%;"

    .line 299
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "        }"

    .line 300
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "        a,"

    .line 301
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "        a:visited,"

    .line 302
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "        a:active,"

    .line 303
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "        a:hover {"

    .line 304
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "            color: "

    .line 305
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->textColorLink:Ljava/lang/String;

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ";"

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "        body {"

    .line 307
    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "            background-color: transparent;"

    .line 308
    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "            margin: 0;"

    .line 309
    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "            padding: "

    .line 310
    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "            font-size: "

    .line 311
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "16px"

    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "            line-height: "

    .line 313
    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "1.5"

    invoke-virtual {p1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "            white-space: normal;"

    .line 314
    invoke-virtual {p1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "            word-wrap: break-word;"

    .line 315
    invoke-virtual {p1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->textColor:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "        .title {"

    .line 318
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "            display: block;"

    .line 319
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "            padding: 0 0 "

    .line 321
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v5, 0x10

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, " 0;"

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 322
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "24px"

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "32px"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "        h1, h2, h3 { "

    .line 326
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "            line-height: 1.4; "

    .line 327
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "    </style>"

    .line 329
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "    <script language=\'javascript\'>"

    .line 330
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "     window.onload = function () {"

    .line 331
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "        var w = window,"

    .line 332
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "            d = document,"

    .line 333
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "            e = d.documentElement,"

    .line 334
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "            g = d.getElementsByTagName(\'body\')[0],"

    .line 335
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "            sWidth = Math.min (w.innerWidth || Infinity, e.clientWidth || Infinity, g.clientWidth || Infinity),"

    .line 338
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "            sHeight = Math.min (w.innerHeight || Infinity, e.clientHeight || Infinity, g.clientHeight || Infinity);"

    .line 340
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "        var frame, fw, fh;"

    .line 342
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "        var iframes = document.getElementsByTagName(\'iframe\');"

    .line 343
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "        var padding = "

    .line 344
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "        for (var i=0; i < iframes.length; i++) {"

    .line 345
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "            frame = iframes[i];"

    .line 346
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "            fw = frame.offsetWidth;"

    .line 347
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "            fh = frame.offsetHeight;"

    .line 348
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "            if (fw >= fh && fw > (sWidth - padding)) {"

    .line 349
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "                frame.style.width = sWidth - padding;"

    .line 350
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "                frame.style.height = ((sWidth - padding) * fh/fw).toString();"

    .line 351
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "            }"

    .line 352
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 353
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "        document.addEventListener(\'click\', function (event) {"

    .line 354
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "            if (event.target instanceof HTMLImageElement) {"

    .line 355
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "                event.preventDefault();"

    .line 356
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "                event.stopPropagation();"

    .line 357
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 358
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "        }, false);"

    .line 359
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "    };"

    .line 360
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "    </script>"

    .line 361
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "</head>"

    .line 362
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "<body>"

    .line 363
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "    <strong class=\'title\'> "

    .line 364
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " </strong> "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "</body>"

    .line 365
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "</html>"

    .line 366
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private hideQuestionFooter()V
    .locals 2

    .line 495
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->questionFooter:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private highlightAndReloadQuestion()V
    .locals 3

    const/4 v0, 0x1

    .line 455
    iput-boolean v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->isHighlighted:Z

    .line 456
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "searchTerms"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 458
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->question:Lcom/helpshift/support/Faq;

    invoke-static {v1, v2, v0}, Lcom/helpshift/support/util/Styles;->getQuestionWithHighlightedSearchTerms(Landroid/content/Context;Lcom/helpshift/support/Faq;Ljava/util/ArrayList;)Lcom/helpshift/support/Faq;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 460
    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->setQuestion(Lcom/helpshift/support/Faq;)V

    :cond_0
    return-void
.end method

.method private markQuestion(Z)V
    .locals 2

    .line 371
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->question:Lcom/helpshift/support/Faq;

    if-nez v0, :cond_0

    return-void

    .line 374
    :cond_0
    invoke-virtual {v0}, Lcom/helpshift/support/Faq;->getId()Ljava/lang/String;

    move-result-object v0

    .line 376
    iget-object v1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->data:Lcom/helpshift/support/HSApiData;

    invoke-virtual {v1, v0, p1}, Lcom/helpshift/support/HSApiData;->markFaqInDB(Ljava/lang/String;Z)V

    .line 379
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v1

    invoke-interface {v1}, Lcom/helpshift/CoreApi;->getFaqDM()Lcom/helpshift/faq/FaqsDM;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lcom/helpshift/faq/FaqsDM;->markHelpful(Ljava/lang/String;Z)V

    return-void
.end method

.method public static newInstance(Landroid/os/Bundle;IZLcom/helpshift/support/fragments/SingleQuestionFragment$QuestionReadListener;)Lcom/helpshift/support/fragments/SingleQuestionFragment;
    .locals 1

    .line 77
    new-instance v0, Lcom/helpshift/support/fragments/SingleQuestionFragment;

    invoke-direct {v0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;-><init>()V

    .line 78
    invoke-virtual {v0, p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->setArguments(Landroid/os/Bundle;)V

    .line 79
    iput p1, v0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->singleQuestionMode:I

    .line 80
    iput-boolean p2, v0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->showRootLayoutInsideCardView:Z

    .line 81
    iput-object p3, v0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->questionReadListener:Lcom/helpshift/support/fragments/SingleQuestionFragment$QuestionReadListener;

    return-object v0
.end method

.method private setIsHelpful(I)V
    .locals 0

    if-eqz p1, :cond_0

    .line 428
    iput p1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->isHelpful:I

    .line 430
    :cond_0
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->updateFooter()V

    return-void
.end method

.method private showHelpfulFooter()V
    .locals 3

    .line 507
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->questionFooter:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 508
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->questionFooterMessage:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/helpshift/R$string;->hs__question_helpful_message:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 509
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->questionFooterMessage:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 510
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->contactUsButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 511
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->yesButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 512
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->noButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method private showProgress(Z)V
    .locals 1

    .line 465
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->progressBar:Landroid/view/View;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 467
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    .line 470
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method private showQuestionFooter()V
    .locals 4

    .line 499
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->questionFooter:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 500
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->questionFooterMessage:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/helpshift/R$string;->hs__mark_yes_no_question:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 501
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->contactUsButton:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 502
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->yesButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 503
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->noButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method private showQuestionFooterContactUs()V
    .locals 2

    .line 524
    sget-object v0, Lcom/helpshift/support/ContactUsFilter$LOCATION;->QUESTION_FOOTER:Lcom/helpshift/support/ContactUsFilter$LOCATION;

    invoke-static {v0}, Lcom/helpshift/support/ContactUsFilter;->showContactUs(Lcom/helpshift/support/ContactUsFilter$LOCATION;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 525
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->contactUsButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 528
    :cond_0
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->contactUsButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private showUnhelpfulFooter()V
    .locals 3

    .line 516
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->questionFooter:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 517
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->questionFooterMessage:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/helpshift/R$string;->hs__question_unhelpful_message:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 518
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->showQuestionFooterContactUs()V

    .line 519
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->yesButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 520
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->noButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method private updateFooter()V
    .locals 2

    .line 476
    iget v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->singleQuestionMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 477
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->hideQuestionFooter()V

    return-void

    .line 481
    :cond_0
    iget v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->isHelpful:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 486
    :cond_1
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->showHelpfulFooter()V

    goto :goto_0

    .line 483
    :cond_2
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->showQuestionFooter()V

    goto :goto_0

    .line 489
    :cond_3
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->showUnhelpfulFooter()V

    :goto_0
    return-void
.end method


# virtual methods
.method public getFaqFlowListener()Lcom/helpshift/support/contracts/FaqFragmentListener;
    .locals 1

    .line 419
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/helpshift/support/contracts/FaqFlowViewParent;

    if-eqz v0, :cond_0

    .line 421
    invoke-interface {v0}, Lcom/helpshift/support/contracts/FaqFlowViewParent;->getFaqFlowListener()Lcom/helpshift/support/contracts/FaqFragmentListener;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getQuestionId()Ljava/lang/String;
    .locals 1

    .line 534
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->question:Lcom/helpshift/support/Faq;

    if-eqz v0, :cond_0

    .line 535
    invoke-virtual {v0}, Lcom/helpshift/support/Faq;->getId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1

    .line 87
    invoke-super {p0, p1}, Lcom/helpshift/support/fragments/MainFragment;->onAttach(Landroid/content/Context;)V

    .line 88
    new-instance v0, Lcom/helpshift/support/HSApiData;

    invoke-direct {v0, p1}, Lcom/helpshift/support/HSApiData;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->data:Lcom/helpshift/support/HSApiData;

    .line 89
    invoke-direct {p0, p1}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getColorsFromTheme(Landroid/content/Context;)V

    .line 90
    invoke-static {p0}, Lcom/helpshift/support/util/FragmentUtil;->getSupportFragment(Landroidx/fragment/app/Fragment;)Lcom/helpshift/support/fragments/SupportFragment;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 92
    invoke-virtual {p1}, Lcom/helpshift/support/fragments/SupportFragment;->getSupportController()Lcom/helpshift/support/controllers/SupportController;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->supportController:Lcom/helpshift/support/controllers/SupportController;

    .line 94
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->singleQuestionMode:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->fragmentName:Ljava/lang/String;

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 384
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/helpshift/R$id;->helpful_button:I

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    .line 385
    invoke-direct {p0, v2}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->markQuestion(Z)V

    .line 386
    invoke-direct {p0, v2}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->setIsHelpful(I)V

    .line 387
    iget p1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->singleQuestionMode:I

    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 388
    invoke-static {p0}, Lcom/helpshift/support/util/FragmentUtil;->getSupportFragment(Landroidx/fragment/app/Fragment;)Lcom/helpshift/support/fragments/SupportFragment;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 390
    invoke-virtual {p1}, Lcom/helpshift/support/fragments/SupportFragment;->getSupportController()Lcom/helpshift/support/controllers/SupportController;

    move-result-object p1

    .line 391
    invoke-virtual {p1}, Lcom/helpshift/support/controllers/SupportController;->actionDone()V

    goto :goto_0

    .line 395
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/helpshift/R$id;->unhelpful_button:I

    if-ne v0, v1, :cond_1

    const/4 p1, 0x0

    .line 396
    invoke-direct {p0, p1}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->markQuestion(Z)V

    const/4 p1, -0x1

    .line 397
    invoke-direct {p0, p1}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->setIsHelpful(I)V

    goto :goto_0

    .line 399
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    sget v0, Lcom/helpshift/R$id;->contact_us_button:I

    if-ne p1, v0, :cond_3

    .line 400
    iget-object p1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->supportController:Lcom/helpshift/support/controllers/SupportController;

    if-eqz p1, :cond_3

    .line 401
    iget p1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->singleQuestionMode:I

    if-ne p1, v2, :cond_2

    .line 402
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getFaqFlowListener()Lcom/helpshift/support/contracts/FaqFragmentListener;

    move-result-object p1

    if-eqz p1, :cond_3

    const/4 v0, 0x0

    .line 404
    invoke-interface {p1, v0}, Lcom/helpshift/support/contracts/FaqFragmentListener;->onContactUsClicked(Ljava/lang/String;)V

    goto :goto_0

    .line 408
    :cond_2
    invoke-static {p0}, Lcom/helpshift/support/util/FragmentUtil;->getSupportFragment(Landroidx/fragment/app/Fragment;)Lcom/helpshift/support/fragments/SupportFragment;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 410
    invoke-virtual {p1}, Lcom/helpshift/support/fragments/SupportFragment;->getSupportController()Lcom/helpshift/support/controllers/SupportController;

    move-result-object p1

    .line 411
    invoke-virtual {p1}, Lcom/helpshift/support/controllers/SupportController;->sendAnyway()V

    :cond_3
    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 136
    invoke-super {p0, p1}, Lcom/helpshift/support/fragments/MainFragment;->onCreate(Landroid/os/Bundle;)V

    .line 137
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    const-string v1, "decomp"

    .line 139
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->decomp:Z

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .line 146
    sget p3, Lcom/helpshift/R$layout;->hs__single_question_fragment:I

    .line 147
    iget-boolean v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->showRootLayoutInsideCardView:Z

    if-eqz v0, :cond_0

    .line 148
    sget p3, Lcom/helpshift/R$layout;->hs__single_question_layout_with_cardview:I

    :cond_0
    const/4 v0, 0x0

    .line 150
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onDestroyView()V
    .locals 2

    .line 235
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/helpshift/support/util/SnackbarUtil;->hideSnackbar(Landroid/view/View;)V

    const/4 v0, 0x0

    .line 238
    iput-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->questionFooter:Landroid/view/View;

    .line 239
    iget-object v1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->webView:Lcom/helpshift/support/webkit/CustomWebView;

    invoke-virtual {v1, v0}, Lcom/helpshift/support/webkit/CustomWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 240
    iput-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->webView:Lcom/helpshift/support/webkit/CustomWebView;

    .line 241
    iput-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->noButton:Landroid/widget/Button;

    .line 242
    iput-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->yesButton:Landroid/widget/Button;

    .line 243
    iput-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->contactUsButton:Landroid/widget/Button;

    .line 244
    invoke-super {p0}, Lcom/helpshift/support/fragments/MainFragment;->onDestroyView()V

    return-void
.end method

.method public onPageFinished()V
    .locals 2

    .line 441
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 442
    invoke-direct {p0, v0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->showProgress(Z)V

    .line 443
    iget-object v1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->question:Lcom/helpshift/support/Faq;

    iget v1, v1, Lcom/helpshift/support/Faq;->is_helpful:I

    invoke-direct {p0, v1}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->setIsHelpful(I)V

    .line 444
    iget-boolean v1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->isHighlighted:Z

    if-eqz v1, :cond_0

    .line 445
    iput-boolean v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->isHighlighted:Z

    goto :goto_0

    .line 448
    :cond_0
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->highlightAndReloadQuestion()V

    .line 450
    :goto_0
    iget-object v1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->webView:Lcom/helpshift/support/webkit/CustomWebView;

    invoke-virtual {v1, v0}, Lcom/helpshift/support/webkit/CustomWebView;->setBackgroundColor(I)V

    :cond_1
    return-void
.end method

.method public onPageStarted()V
    .locals 2

    const/4 v0, 0x1

    .line 435
    invoke-direct {p0, v0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->showProgress(Z)V

    .line 436
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->webView:Lcom/helpshift/support/webkit/CustomWebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/support/webkit/CustomWebView;->setBackgroundColor(I)V

    return-void
.end method

.method public onPause()V
    .locals 1

    .line 107
    invoke-super {p0}, Lcom/helpshift/support/fragments/MainFragment;->onPause()V

    .line 108
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->webView:Lcom/helpshift/support/webkit/CustomWebView;

    invoke-virtual {v0}, Lcom/helpshift/support/webkit/CustomWebView;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 2

    .line 210
    invoke-super {p0}, Lcom/helpshift/support/fragments/MainFragment;->onResume()V

    .line 212
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->isScreenLarge()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object v0

    .line 215
    instance-of v1, v0, Lcom/helpshift/support/fragments/FaqFlowFragment;

    if-eqz v1, :cond_0

    .line 216
    check-cast v0, Lcom/helpshift/support/fragments/FaqFlowFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/support/fragments/FaqFlowFragment;->updateSelectQuestionUI(Z)V

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->webView:Lcom/helpshift/support/webkit/CustomWebView;

    invoke-virtual {v0}, Lcom/helpshift/support/webkit/CustomWebView;->onResume()V

    .line 221
    iget-boolean v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->decomp:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->isScreenLarge()Z

    move-result v0

    if-nez v0, :cond_2

    .line 222
    :cond_1
    sget v0, Lcom/helpshift/R$string;->hs__question_header:I

    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->setToolbarTitle(Ljava/lang/String;)V

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->question:Lcom/helpshift/support/Faq;

    if-eqz v0, :cond_3

    .line 226
    invoke-virtual {v0}, Lcom/helpshift/support/Faq;->getId()Ljava/lang/String;

    move-result-object v0

    .line 227
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->eventSent:Z

    if-nez v0, :cond_3

    .line 228
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->reportReadFaqEvent()V

    :cond_3
    return-void
.end method

.method public onStart()V
    .locals 1

    .line 99
    invoke-super {p0}, Lcom/helpshift/support/fragments/MainFragment;->onStart()V

    .line 100
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 101
    iput-boolean v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->eventSent:Z

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    .line 113
    invoke-super {p0}, Lcom/helpshift/support/fragments/MainFragment;->onStop()V

    .line 114
    iget-boolean v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->decomp:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->isScreenLarge()Z

    move-result v0

    if-nez v0, :cond_1

    .line 115
    :cond_0
    sget v0, Lcom/helpshift/R$string;->hs__help_header:I

    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->setToolbarTitle(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8

    .line 157
    invoke-super {p0, p1, p2}, Lcom/helpshift/support/fragments/MainFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 159
    sget p2, Lcom/helpshift/R$id;->web_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/helpshift/support/webkit/CustomWebView;

    iput-object p2, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->webView:Lcom/helpshift/support/webkit/CustomWebView;

    .line 160
    iget-object p2, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->webView:Lcom/helpshift/support/webkit/CustomWebView;

    new-instance v0, Lcom/helpshift/support/webkit/CustomWebViewClient;

    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/helpshift/support/webkit/CustomWebViewClient;-><init>(Landroid/content/Context;Lcom/helpshift/support/webkit/CustomWebViewClient$CustomWebViewClientListeners;)V

    invoke-virtual {p2, v0}, Lcom/helpshift/support/webkit/CustomWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 162
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    invoke-virtual {p2}, Landroidx/fragment/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object p2

    invoke-virtual {p2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p2

    .line 163
    sget v0, Lcom/helpshift/R$id;->faq_content_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 164
    iget-object v1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->webView:Lcom/helpshift/support/webkit/CustomWebView;

    new-instance v2, Lcom/helpshift/support/webkit/CustomWebChromeClient;

    invoke-direct {v2, p2, v0}, Lcom/helpshift/support/webkit/CustomWebChromeClient;-><init>(Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Lcom/helpshift/support/webkit/CustomWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 166
    sget p2, Lcom/helpshift/R$id;->helpful_button:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    iput-object p2, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->yesButton:Landroid/widget/Button;

    .line 167
    iget-object p2, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->yesButton:Landroid/widget/Button;

    invoke-virtual {p2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    sget p2, Lcom/helpshift/R$id;->unhelpful_button:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    iput-object p2, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->noButton:Landroid/widget/Button;

    .line 170
    iget-object p2, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->noButton:Landroid/widget/Button;

    invoke-virtual {p2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    sget p2, Lcom/helpshift/R$id;->question_footer:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->questionFooter:Landroid/view/View;

    .line 173
    sget p2, Lcom/helpshift/R$id;->question_footer_message:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->questionFooterMessage:Landroid/widget/TextView;

    .line 175
    sget p2, Lcom/helpshift/R$id;->contact_us_button:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    iput-object p2, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->contactUsButton:Landroid/widget/Button;

    .line 176
    iget-object p2, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->contactUsButton:Landroid/widget/Button;

    invoke-virtual {p2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x18

    if-lt p2, v0, :cond_0

    .line 181
    iget-object p2, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->yesButton:Landroid/widget/Button;

    sget v0, Lcom/helpshift/R$string;->hs__mark_yes:I

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setText(I)V

    .line 182
    iget-object p2, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->noButton:Landroid/widget/Button;

    sget v0, Lcom/helpshift/R$string;->hs__mark_no:I

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setText(I)V

    .line 183
    iget-object p2, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->contactUsButton:Landroid/widget/Button;

    sget v0, Lcom/helpshift/R$string;->hs__contact_us_btn:I

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setText(I)V

    .line 186
    :cond_0
    iget p2, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->singleQuestionMode:I

    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 187
    iget-object p2, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->contactUsButton:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/helpshift/R$string;->hs__send_anyway:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 190
    :cond_1
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p2

    const-string v0, "questionPublishId"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 191
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p2

    const-string v0, "support_mode"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p2

    .line 195
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "questionLanguage"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 197
    iget v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->singleQuestionMode:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    const/4 v5, 0x1

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    :goto_0
    if-nez v5, :cond_4

    if-ne p2, v3, :cond_3

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    goto :goto_2

    :cond_4
    :goto_1
    const/4 v4, 0x1

    .line 201
    :goto_2
    iget-object v1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->data:Lcom/helpshift/support/HSApiData;

    new-instance v2, Lcom/helpshift/support/fragments/SingleQuestionFragment$Success;

    invoke-direct {v2, p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment$Success;-><init>(Lcom/helpshift/support/fragments/SingleQuestionFragment;)V

    new-instance v3, Lcom/helpshift/support/fragments/SingleQuestionFragment$Failure;

    invoke-direct {v3, p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment$Failure;-><init>(Lcom/helpshift/support/fragments/SingleQuestionFragment;)V

    invoke-virtual/range {v1 .. v7}, Lcom/helpshift/support/HSApiData;->getQuestion(Landroid/os/Handler;Landroid/os/Handler;ZZLjava/lang/String;Ljava/lang/String;)V

    .line 205
    sget p2, Lcom/helpshift/R$id;->progress_bar:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->progressBar:Landroid/view/View;

    return-void
.end method

.method reportReadFaqEvent()V
    .locals 3

    .line 541
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 542
    iget-object v1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->question:Lcom/helpshift/support/Faq;

    invoke-virtual {v1}, Lcom/helpshift/support/Faq;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "id"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/helpshift/util/HelpshiftConnectionUtil;->isOnline(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "nt"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v1

    invoke-interface {v1}, Lcom/helpshift/CoreApi;->getAnalyticsEventDM()Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    move-result-object v1

    sget-object v2, Lcom/helpshift/analytics/AnalyticsEventType;->READ_FAQ:Lcom/helpshift/analytics/AnalyticsEventType;

    invoke-virtual {v1, v2, v0}, Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;->pushEvent(Lcom/helpshift/analytics/AnalyticsEventType;Ljava/util/Map;)V

    .line 545
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->questionReadListener:Lcom/helpshift/support/fragments/SingleQuestionFragment$QuestionReadListener;

    if-eqz v0, :cond_0

    .line 546
    iget-object v1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->question:Lcom/helpshift/support/Faq;

    invoke-virtual {v1}, Lcom/helpshift/support/Faq;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/helpshift/support/fragments/SingleQuestionFragment$QuestionReadListener;->onQuestionRead(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    .line 548
    iput-boolean v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->eventSent:Z

    return-void
.end method

.method setQuestion(Lcom/helpshift/support/Faq;)V
    .locals 6

    .line 248
    iput-object p1, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->question:Lcom/helpshift/support/Faq;

    .line 249
    iget-object v0, p0, Lcom/helpshift/support/fragments/SingleQuestionFragment;->webView:Lcom/helpshift/support/webkit/CustomWebView;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 250
    invoke-direct {p0, p1}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getStyledBody(Lcom/helpshift/support/Faq;)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    const-string v3, "text/html"

    const-string v4, "utf-8"

    invoke-virtual/range {v0 .. v5}, Lcom/helpshift/support/webkit/CustomWebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public shouldRefreshMenu()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
