.class Lcom/helpshift/support/conversations/ConversationFragmentRenderer;
.super Ljava/lang/Object;
.source "ConversationFragmentRenderer.java"

# interfaces
.implements Lcom/helpshift/conversation/activeconversation/ConversationRenderer;


# instance fields
.field private confirmationBoxView:Landroid/view/View;

.field context:Landroid/content/Context;

.field protected conversationFragmentRouter:Lcom/helpshift/support/conversations/ConversationFragmentRouter;

.field private menuItemRenderer:Lcom/helpshift/support/fragments/IToolbarMenuItemRenderer;

.field messagesAdapter:Lcom/helpshift/support/conversations/MessagesAdapter;

.field messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field protected parentView:Landroid/view/View;

.field protected replyBoxView:Landroid/view/View;

.field private replyButton:Landroid/widget/ImageButton;

.field protected replyField:Landroid/widget/EditText;

.field private scrollIndicator:Landroid/view/View;

.field private scrollJumpButton:Landroid/view/View;

.field private unreadMessagesIndicatorDot:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;Landroid/view/View;Lcom/helpshift/support/conversations/ConversationFragmentRouter;Landroid/view/View;Landroid/view/View;Lcom/helpshift/support/fragments/IToolbarMenuItemRenderer;)V
    .locals 0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->context:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 62
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getItemAnimator()Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    move-result-object p1

    .line 63
    instance-of p2, p1, Landroidx/recyclerview/widget/SimpleItemAnimator;

    if-eqz p2, :cond_0

    .line 65
    check-cast p1, Landroidx/recyclerview/widget/SimpleItemAnimator;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/SimpleItemAnimator;->setSupportsChangeAnimations(Z)V

    .line 67
    :cond_0
    iput-object p3, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->parentView:Landroid/view/View;

    .line 68
    sget p1, Lcom/helpshift/R$id;->relativeLayout1:I

    invoke-virtual {p3, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyBoxView:Landroid/view/View;

    .line 69
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyBoxView:Landroid/view/View;

    sget p2, Lcom/helpshift/R$id;->hs__messageText:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyField:Landroid/widget/EditText;

    .line 70
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyBoxView:Landroid/view/View;

    sget p2, Lcom/helpshift/R$id;->hs__sendMessageBtn:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageButton;

    iput-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyButton:Landroid/widget/ImageButton;

    .line 71
    sget p1, Lcom/helpshift/R$id;->scroll_jump_button:I

    invoke-virtual {p3, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->scrollJumpButton:Landroid/view/View;

    .line 73
    iput-object p4, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->confirmationBoxView:Landroid/view/View;

    .line 74
    iput-object p5, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->conversationFragmentRouter:Lcom/helpshift/support/conversations/ConversationFragmentRouter;

    .line 75
    iput-object p8, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->menuItemRenderer:Lcom/helpshift/support/fragments/IToolbarMenuItemRenderer;

    .line 76
    iput-object p6, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->scrollIndicator:Landroid/view/View;

    .line 77
    iput-object p7, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->unreadMessagesIndicatorDot:Landroid/view/View;

    return-void
.end method

.method static synthetic access$000(Lcom/helpshift/support/conversations/ConversationFragmentRenderer;)Landroid/widget/ImageButton;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyButton:Landroid/widget/ImageButton;

    return-object p0
.end method

.method private changeMenuItemVisibility(Lcom/helpshift/support/fragments/HSMenuItemType;Z)V
    .locals 1

    .line 401
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->menuItemRenderer:Lcom/helpshift/support/fragments/IToolbarMenuItemRenderer;

    if-eqz v0, :cond_0

    .line 402
    invoke-interface {v0, p1, p2}, Lcom/helpshift/support/fragments/IToolbarMenuItemRenderer;->updateMenuItemVisibility(Lcom/helpshift/support/fragments/HSMenuItemType;Z)V

    :cond_0
    return-void
.end method

.method private launchAttachmentIntentInternal(Landroid/content/Intent;Ljava/io/File;)V
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 188
    iget-object p2, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->context:Landroid/content/Context;

    invoke-virtual {p2, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 190
    :cond_0
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object p1

    invoke-interface {p1}, Lcom/helpshift/CoreApi;->getDelegate()Lcom/helpshift/delegate/UIThreadDelegateDecorator;

    move-result-object p1

    invoke-virtual {p1}, Lcom/helpshift/delegate/UIThreadDelegateDecorator;->isDelegateRegistered()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 191
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object p1

    invoke-interface {p1}, Lcom/helpshift/CoreApi;->getDelegate()Lcom/helpshift/delegate/UIThreadDelegateDecorator;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/helpshift/delegate/UIThreadDelegateDecorator;->displayAttachmentFile(Ljava/io/File;)V

    goto :goto_0

    .line 194
    :cond_1
    sget-object p1, Lcom/helpshift/common/exception/PlatformException;->NO_APPS_FOR_OPENING_ATTACHMENT:Lcom/helpshift/common/exception/PlatformException;

    invoke-virtual {p0, p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->showErrorView(Lcom/helpshift/common/exception/ExceptionType;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public appendMessages(II)V
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesAdapter:Lcom/helpshift/support/conversations/MessagesAdapter;

    if-nez v0, :cond_0

    return-void

    .line 98
    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/helpshift/support/conversations/MessagesAdapter;->onItemRangeInserted(II)V

    return-void
.end method

.method public destroy()V
    .locals 1

    const/4 v0, 0x0

    .line 358
    iput-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->conversationFragmentRouter:Lcom/helpshift/support/conversations/ConversationFragmentRouter;

    return-void
.end method

.method public disableSendReplyButton()V
    .locals 3

    .line 145
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 146
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyButton:Landroid/widget/ImageButton;

    const/16 v2, 0x40

    invoke-static {v0, v2}, Lcom/helpshift/support/util/Styles;->setImageAlpha(Landroid/widget/ImageButton;I)V

    .line 147
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/helpshift/support/util/Styles;->setSendMessageButtonIconColor(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Z)V

    return-void
.end method

.method public enableSendReplyButton()V
    .locals 3

    .line 138
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyButton:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 139
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyButton:Landroid/widget/ImageButton;

    const/16 v2, 0xff

    invoke-static {v0, v2}, Lcom/helpshift/support/util/Styles;->setImageAlpha(Landroid/widget/ImageButton;I)V

    .line 140
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/helpshift/support/util/Styles;->setSendMessageButtonIconColor(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Z)V

    return-void
.end method

.method public getReply()Ljava/lang/String;
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hideAgentTypingIndicator()V
    .locals 2

    .line 286
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesAdapter:Lcom/helpshift/support/conversations/MessagesAdapter;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 287
    invoke-virtual {v0, v1}, Lcom/helpshift/support/conversations/MessagesAdapter;->setAgentTypingIndicatorVisibility(Z)V

    :cond_0
    return-void
.end method

.method public hideConversationResolutionQuestionUI()V
    .locals 2

    .line 168
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->confirmationBoxView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public hideImageAttachmentButton()V
    .locals 2

    .line 133
    sget-object v0, Lcom/helpshift/support/fragments/HSMenuItemType;->SCREENSHOT_ATTACHMENT:Lcom/helpshift/support/fragments/HSMenuItemType;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->changeMenuItemVisibility(Lcom/helpshift/support/fragments/HSMenuItemType;Z)V

    return-void
.end method

.method public hideKeyboard()V
    .locals 2

    .line 346
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyField:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/helpshift/support/util/KeyboardUtil;->hideKeyboard(Landroid/content/Context;Landroid/view/View;)V

    return-void
.end method

.method public hideScrollJumperView()V
    .locals 2

    .line 378
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->scrollIndicator:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 379
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->unreadMessagesIndicatorDot:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public hideSendReplyUI()V
    .locals 2

    .line 179
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1, v1, v1}, Landroidx/recyclerview/widget/RecyclerView;->setPadding(IIII)V

    .line 180
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyBoxView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public initializeMessages(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;)V"
        }
    .end annotation

    .line 82
    new-instance v0, Lcom/helpshift/support/conversations/MessagesAdapter;

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->conversationFragmentRouter:Lcom/helpshift/support/conversations/ConversationFragmentRouter;

    invoke-direct {v0, v1, p1, v2}, Lcom/helpshift/support/conversations/MessagesAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/helpshift/support/conversations/messages/MessagesAdapterClickListener;)V

    iput-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesAdapter:Lcom/helpshift/support/conversations/MessagesAdapter;

    .line 85
    new-instance p1, Landroidx/recyclerview/widget/LinearLayoutManager;

    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->context:Landroid/content/Context;

    invoke-direct {p1, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    .line 86
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->setStackFromEnd(Z)V

    .line 87
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 88
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesAdapter:Lcom/helpshift/support/conversations/MessagesAdapter;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void
.end method

.method public isReplyBoxVisible()Z
    .locals 1

    .line 300
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyBoxView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public launchAttachment(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 211
    invoke-static {p1}, Lcom/helpshift/util/FileUtil;->validateAndCreateFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 215
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 216
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->context:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/helpshift/util/IntentUtil;->createFileProviderIntent(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p2

    goto :goto_0

    .line 219
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 220
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 221
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-object p2, v0

    .line 223
    :goto_0
    invoke-direct {p0, p2, p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->launchAttachmentIntentInternal(Landroid/content/Intent;Ljava/io/File;)V

    goto :goto_1

    .line 226
    :cond_1
    sget-object p1, Lcom/helpshift/common/exception/PlatformException;->FILE_NOT_FOUND:Lcom/helpshift/common/exception/PlatformException;

    invoke-virtual {p0, p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->showErrorView(Lcom/helpshift/common/exception/ExceptionType;)V

    :goto_1
    return-void
.end method

.method public launchScreenshotAttachment(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 200
    invoke-static {p1}, Lcom/helpshift/util/FileUtil;->validateAndCreateFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 202
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->context:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/helpshift/util/IntentUtil;->createFileProviderIntent(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p2

    invoke-direct {p0, p2, p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->launchAttachmentIntentInternal(Landroid/content/Intent;Ljava/io/File;)V

    goto :goto_0

    .line 205
    :cond_0
    sget-object p1, Lcom/helpshift/common/exception/PlatformException;->FILE_NOT_FOUND:Lcom/helpshift/common/exception/PlatformException;

    invoke-virtual {p0, p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->showErrorView(Lcom/helpshift/common/exception/ExceptionType;)V

    :goto_0
    return-void
.end method

.method public notifyRefreshList()V
    .locals 1

    .line 350
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesAdapter:Lcom/helpshift/support/conversations/MessagesAdapter;

    if-eqz v0, :cond_0

    .line 351
    invoke-virtual {v0}, Lcom/helpshift/support/conversations/MessagesAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public onAuthenticationFailure()V
    .locals 1

    .line 293
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->conversationFragmentRouter:Lcom/helpshift/support/conversations/ConversationFragmentRouter;

    if-eqz v0, :cond_0

    .line 294
    invoke-interface {v0}, Lcom/helpshift/support/conversations/ConversationFragmentRouter;->onAuthenticationFailure()V

    :cond_0
    return-void
.end method

.method public openAppReviewStore(Ljava/lang/String;)V
    .locals 2

    .line 237
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 238
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 239
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->context:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 240
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->context:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 243
    :cond_0
    sget-object p1, Lcom/helpshift/common/exception/PlatformException;->NO_APPS_FOR_OPENING_ATTACHMENT:Lcom/helpshift/common/exception/PlatformException;

    invoke-virtual {p0, p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->showErrorView(Lcom/helpshift/common/exception/ExceptionType;)V

    :goto_0
    return-void
.end method

.method public openFreshConversationScreen()V
    .locals 1

    .line 274
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->conversationFragmentRouter:Lcom/helpshift/support/conversations/ConversationFragmentRouter;

    invoke-interface {v0}, Lcom/helpshift/support/conversations/ConversationFragmentRouter;->openFreshConversationScreen()V

    return-void
.end method

.method public removeMessages(II)V
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesAdapter:Lcom/helpshift/support/conversations/MessagesAdapter;

    if-nez v0, :cond_0

    return-void

    .line 123
    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/helpshift/support/conversations/MessagesAdapter;->onItemRangeRemoved(II)V

    return-void
.end method

.method public requestReplyFieldFocus()V
    .locals 1

    .line 336
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void
.end method

.method public scrollToBottom()V
    .locals 2

    .line 385
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesAdapter:Lcom/helpshift/support/conversations/MessagesAdapter;

    if-nez v0, :cond_0

    return-void

    .line 389
    :cond_0
    invoke-virtual {v0}, Lcom/helpshift/support/conversations/MessagesAdapter;->getItemCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 391
    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    :cond_1
    return-void
.end method

.method protected setMessagesViewBottomPadding()V
    .locals 3

    .line 396
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->context:Landroid/content/Context;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-static {v0, v1}, Lcom/helpshift/util/Styles;->dpToPx(Landroid/content/Context;F)F

    move-result v0

    float-to-int v0, v0

    .line 397
    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v2, v2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setPadding(IIII)V

    return-void
.end method

.method public setReply(Ljava/lang/String;)V
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyField:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setReplyboxListeners()V
    .locals 2

    .line 305
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyField:Landroid/widget/EditText;

    new-instance v1, Lcom/helpshift/support/conversations/ConversationFragmentRenderer$1;

    invoke-direct {v1, p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer$1;-><init>(Lcom/helpshift/support/conversations/ConversationFragmentRenderer;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 314
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyField:Landroid/widget/EditText;

    new-instance v1, Lcom/helpshift/support/conversations/ConversationFragmentRenderer$2;

    invoke-direct {v1, p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer$2;-><init>(Lcom/helpshift/support/conversations/ConversationFragmentRenderer;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 323
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/helpshift/support/conversations/ConversationFragmentRenderer$3;

    invoke-direct {v1, p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer$3;-><init>(Lcom/helpshift/support/conversations/ConversationFragmentRenderer;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public showAgentTypingIndicator()V
    .locals 2

    .line 279
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesAdapter:Lcom/helpshift/support/conversations/MessagesAdapter;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 280
    invoke-virtual {v0, v1}, Lcom/helpshift/support/conversations/MessagesAdapter;->setAgentTypingIndicatorVisibility(Z)V

    :cond_0
    return-void
.end method

.method public showCSATSubmittedView()V
    .locals 3

    .line 267
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->parentView:Landroid/view/View;

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/helpshift/R$string;->hs__csat_submit_toast:I

    .line 268
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 267
    invoke-static {v0, v1, v2}, Lcom/helpshift/support/util/SnackbarUtil;->showSnackbar(Landroid/view/View;Ljava/lang/String;I)V

    return-void
.end method

.method public showConversationResolutionQuestionUI()V
    .locals 2

    .line 162
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->hideKeyboard()V

    .line 163
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->confirmationBoxView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public showErrorView(Lcom/helpshift/common/exception/ExceptionType;)V
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->parentView:Landroid/view/View;

    invoke-static {p1, v0}, Lcom/helpshift/support/util/SnackbarUtil;->showSnackbar(Lcom/helpshift/common/exception/ExceptionType;Landroid/view/View;)V

    return-void
.end method

.method public showImageAttachmentButton()V
    .locals 2

    .line 128
    sget-object v0, Lcom/helpshift/support/fragments/HSMenuItemType;->SCREENSHOT_ATTACHMENT:Lcom/helpshift/support/fragments/HSMenuItemType;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->changeMenuItemVisibility(Lcom/helpshift/support/fragments/HSMenuItemType;Z)V

    return-void
.end method

.method public showKeyboard()V
    .locals 2

    .line 341
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyField:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/helpshift/support/util/KeyboardUtil;->showKeyboard(Landroid/content/Context;Landroid/view/View;)V

    return-void
.end method

.method public showScrollJumperView(Z)V
    .locals 2

    .line 364
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->scrollIndicator:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_0

    .line 366
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->unreadMessagesIndicatorDot:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 367
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->context:Landroid/content/Context;

    sget v0, Lcom/helpshift/R$string;->hs__jump_button_with_new_message_voice_over:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 370
    :cond_0
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->unreadMessagesIndicatorDot:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 371
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->context:Landroid/content/Context;

    sget v0, Lcom/helpshift/R$string;->hs__jump_button_voice_over:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 373
    :goto_0
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->scrollJumpButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showSendReplyUI()V
    .locals 2

    .line 173
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->setMessagesViewBottomPadding()V

    .line 174
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->replyBoxView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public unregisterFragmentRenderer()V
    .locals 1

    .line 408
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesAdapter:Lcom/helpshift/support/conversations/MessagesAdapter;

    if-eqz v0, :cond_0

    .line 409
    invoke-virtual {v0}, Lcom/helpshift/support/conversations/MessagesAdapter;->unregisterAdapterClickListener()V

    :cond_0
    return-void
.end method

.method public updateConversationFooterState(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V
    .locals 1

    .line 249
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesAdapter:Lcom/helpshift/support/conversations/MessagesAdapter;

    if-eqz v0, :cond_1

    .line 250
    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->NONE:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    if-eq p1, v0, :cond_0

    .line 251
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->hideKeyboard()V

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesAdapter:Lcom/helpshift/support/conversations/MessagesAdapter;

    invoke-virtual {v0, p1}, Lcom/helpshift/support/conversations/MessagesAdapter;->setConversationFooterState(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V

    :cond_1
    return-void
.end method

.method public updateHistoryLoadingState(Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;)V
    .locals 1

    .line 259
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesAdapter:Lcom/helpshift/support/conversations/MessagesAdapter;

    if-eqz v0, :cond_0

    .line 261
    invoke-virtual {v0, p1}, Lcom/helpshift/support/conversations/MessagesAdapter;->setHistoryLoadingState(Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;)V

    :cond_0
    return-void
.end method

.method public updateMessages(II)V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesAdapter:Lcom/helpshift/support/conversations/MessagesAdapter;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    .line 108
    invoke-virtual {v0}, Lcom/helpshift/support/conversations/MessagesAdapter;->getMessageCount()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 109
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesAdapter:Lcom/helpshift/support/conversations/MessagesAdapter;

    invoke-virtual {p1}, Lcom/helpshift/support/conversations/MessagesAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->messagesAdapter:Lcom/helpshift/support/conversations/MessagesAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/helpshift/support/conversations/MessagesAdapter;->onItemRangeChanged(II)V

    :goto_0
    return-void
.end method
