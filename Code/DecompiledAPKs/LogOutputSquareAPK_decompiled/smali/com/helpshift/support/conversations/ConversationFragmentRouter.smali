.class public interface abstract Lcom/helpshift/support/conversations/ConversationFragmentRouter;
.super Ljava/lang/Object;
.source "ConversationFragmentRouter.java"

# interfaces
.implements Lcom/helpshift/support/conversations/messages/MessagesAdapterClickListener;


# virtual methods
.method public abstract onAuthenticationFailure()V
.end method

.method public abstract onSendButtonClick()V
.end method

.method public abstract onSkipClick()V
.end method

.method public abstract onTextChanged(Ljava/lang/CharSequence;III)V
.end method

.method public abstract openFreshConversationScreen()V
.end method

.method public abstract retryPreIssueCreation()V
.end method
