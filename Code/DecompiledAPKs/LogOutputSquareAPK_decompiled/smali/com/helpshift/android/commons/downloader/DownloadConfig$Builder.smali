.class public Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;
.super Ljava/lang/Object;
.source "DownloadConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/helpshift/android/commons/downloader/DownloadConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private externalStorageDirectoryPath:Ljava/lang/String;

.field private isNoMedia:Z

.field private useCache:Z

.field private writeToFile:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 33
    iput-boolean v0, p0, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;->useCache:Z

    const/4 v1, 0x0

    .line 34
    iput-boolean v1, p0, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;->isNoMedia:Z

    .line 35
    iput-boolean v0, p0, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;->writeToFile:Z

    const-string v0, ""

    .line 36
    iput-object v0, p0, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;->externalStorageDirectoryPath:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public create()Lcom/helpshift/android/commons/downloader/DownloadConfig;
    .locals 2

    .line 91
    new-instance v0, Lcom/helpshift/android/commons/downloader/DownloadConfig;

    invoke-direct {v0}, Lcom/helpshift/android/commons/downloader/DownloadConfig;-><init>()V

    .line 92
    iget-boolean v1, p0, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;->useCache:Z

    iput-boolean v1, v0, Lcom/helpshift/android/commons/downloader/DownloadConfig;->useCache:Z

    .line 93
    iget-boolean v1, p0, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;->isNoMedia:Z

    iput-boolean v1, v0, Lcom/helpshift/android/commons/downloader/DownloadConfig;->isNoMedia:Z

    .line 94
    iget-boolean v1, p0, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;->writeToFile:Z

    iput-boolean v1, v0, Lcom/helpshift/android/commons/downloader/DownloadConfig;->writeToFile:Z

    .line 95
    iget-object v1, p0, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;->externalStorageDirectoryPath:Ljava/lang/String;

    iput-object v1, v0, Lcom/helpshift/android/commons/downloader/DownloadConfig;->externalStorageDirectoryPath:Ljava/lang/String;

    return-object v0
.end method

.method public setExternalStorageDirectoryPath(Ljava/lang/String;)Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;->externalStorageDirectoryPath:Ljava/lang/String;

    return-object p0
.end method

.method public setIsNoMedia(Z)Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;
    .locals 0

    .line 57
    iput-boolean p1, p0, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;->isNoMedia:Z

    return-object p0
.end method

.method public setUseCache(Z)Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;
    .locals 0

    .line 46
    iput-boolean p1, p0, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;->useCache:Z

    return-object p0
.end method

.method public setWriteToFile(Z)Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;
    .locals 0

    .line 69
    iput-boolean p1, p0, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;->writeToFile:Z

    return-object p0
.end method
