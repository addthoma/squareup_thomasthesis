.class public Lcom/helpshift/conversation/usersetup/UserSetupMediator;
.super Ljava/lang/Object;
.source "UserSetupMediator.java"

# interfaces
.implements Lcom/helpshift/widget/WidgetMediator;


# instance fields
.field private domain:Lcom/helpshift/common/domain/Domain;

.field private progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

.field private progressDescriptionWidget:Lcom/helpshift/widget/ProgressDescriptionWidget;

.field private userSetupRenderer:Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;


# direct methods
.method constructor <init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/widget/ProgressBarWidget;Lcom/helpshift/widget/ProgressDescriptionWidget;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->domain:Lcom/helpshift/common/domain/Domain;

    .line 24
    iput-object p2, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    .line 25
    iput-object p3, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->progressDescriptionWidget:Lcom/helpshift/widget/ProgressDescriptionWidget;

    .line 27
    iget-object p1, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    invoke-virtual {p1, p0}, Lcom/helpshift/widget/ProgressBarWidget;->setMediator(Lcom/helpshift/widget/WidgetMediator;)V

    .line 28
    iget-object p1, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->progressDescriptionWidget:Lcom/helpshift/widget/ProgressDescriptionWidget;

    invoke-virtual {p1, p0}, Lcom/helpshift/widget/ProgressDescriptionWidget;->setMediator(Lcom/helpshift/widget/WidgetMediator;)V

    return-void
.end method

.method static synthetic access$000(Lcom/helpshift/conversation/usersetup/UserSetupMediator;)Lcom/helpshift/widget/ProgressBarWidget;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    return-object p0
.end method

.method static synthetic access$100(Lcom/helpshift/conversation/usersetup/UserSetupMediator;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->renderProgressBar()V

    return-void
.end method

.method static synthetic access$200(Lcom/helpshift/conversation/usersetup/UserSetupMediator;)Lcom/helpshift/widget/ProgressDescriptionWidget;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->progressDescriptionWidget:Lcom/helpshift/widget/ProgressDescriptionWidget;

    return-object p0
.end method

.method static synthetic access$300(Lcom/helpshift/conversation/usersetup/UserSetupMediator;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->renderProgressDescriptionWidget()V

    return-void
.end method

.method static synthetic access$400(Lcom/helpshift/conversation/usersetup/UserSetupMediator;)Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->userSetupRenderer:Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;

    return-object p0
.end method

.method private renderProgressBar()V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->userSetupRenderer:Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;

    if-nez v0, :cond_0

    return-void

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ProgressBarWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->userSetupRenderer:Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;->showProgressBar()V

    .line 64
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->userSetupRenderer:Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;->hideNoInternetView()V

    goto :goto_0

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->userSetupRenderer:Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;->hideProgressBar()V

    :goto_0
    return-void
.end method

.method private renderProgressDescriptionWidget()V
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->userSetupRenderer:Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;

    if-nez v0, :cond_0

    return-void

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->progressDescriptionWidget:Lcom/helpshift/widget/ProgressDescriptionWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ProgressDescriptionWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ProgressBarWidget;->setVisible(Z)V

    .line 79
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->userSetupRenderer:Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;->showProgressDescription()V

    goto :goto_0

    .line 82
    :cond_1
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->userSetupRenderer:Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;->hideProgressDescription()V

    :goto_0
    return-void
.end method


# virtual methods
.method onAuthenticationFailure()V
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/usersetup/UserSetupMediator$4;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/usersetup/UserSetupMediator$4;-><init>(Lcom/helpshift/conversation/usersetup/UserSetupMediator;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public onChanged(Lcom/helpshift/widget/Widget;)V
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/usersetup/UserSetupMediator$1;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/usersetup/UserSetupMediator$1;-><init>(Lcom/helpshift/conversation/usersetup/UserSetupMediator;Lcom/helpshift/widget/Widget;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method onSetupCompleted()V
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/usersetup/UserSetupMediator$2;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/usersetup/UserSetupMediator$2;-><init>(Lcom/helpshift/conversation/usersetup/UserSetupMediator;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method declared-synchronized registerUserSetupRenderer(Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;)V
    .locals 0

    monitor-enter p0

    .line 33
    :try_start_0
    iput-object p1, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->userSetupRenderer:Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method renderAll()V
    .locals 0

    .line 87
    invoke-direct {p0}, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->renderProgressBar()V

    .line 88
    invoke-direct {p0}, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->renderProgressDescriptionWidget()V

    return-void
.end method

.method showOfflineError()V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/usersetup/UserSetupMediator$3;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/usersetup/UserSetupMediator$3;-><init>(Lcom/helpshift/conversation/usersetup/UserSetupMediator;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method declared-synchronized unregisterUserSetupRenderer()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    .line 38
    :try_start_0
    iput-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->userSetupRenderer:Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
