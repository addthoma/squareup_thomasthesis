.class public Lcom/helpshift/conversation/usersetup/UserSetupVM;
.super Ljava/lang/Object;
.source "UserSetupVM.java"

# interfaces
.implements Lcom/helpshift/account/domainmodel/UserSetupDM$UserSetupListener;
.implements Lcom/helpshift/account/AuthenticationFailureDM$AuthenticationFailureObserver;


# instance fields
.field private platform:Lcom/helpshift/common/platform/Platform;

.field private final progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

.field private final progressDescriptionWidget:Lcom/helpshift/widget/ProgressDescriptionWidget;

.field private userSetupDM:Lcom/helpshift/account/domainmodel/UserSetupDM;

.field private userSetupMediator:Lcom/helpshift/conversation/usersetup/UserSetupMediator;


# direct methods
.method public constructor <init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserSetupDM;Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;)V
    .locals 1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->platform:Lcom/helpshift/common/platform/Platform;

    .line 33
    iput-object p3, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->userSetupDM:Lcom/helpshift/account/domainmodel/UserSetupDM;

    .line 34
    invoke-direct {p0}, Lcom/helpshift/conversation/usersetup/UserSetupVM;->buildProgressBarWidget()Lcom/helpshift/widget/ProgressBarWidget;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    .line 35
    new-instance p1, Lcom/helpshift/widget/ProgressDescriptionWidget;

    invoke-direct {p1}, Lcom/helpshift/widget/ProgressDescriptionWidget;-><init>()V

    iput-object p1, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->progressDescriptionWidget:Lcom/helpshift/widget/ProgressDescriptionWidget;

    .line 36
    new-instance p1, Lcom/helpshift/conversation/usersetup/UserSetupMediator;

    iget-object p3, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->progressDescriptionWidget:Lcom/helpshift/widget/ProgressDescriptionWidget;

    invoke-direct {p1, p2, p3, v0}, Lcom/helpshift/conversation/usersetup/UserSetupMediator;-><init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/widget/ProgressBarWidget;Lcom/helpshift/widget/ProgressDescriptionWidget;)V

    iput-object p1, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->userSetupMediator:Lcom/helpshift/conversation/usersetup/UserSetupMediator;

    .line 37
    iget-object p1, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->userSetupMediator:Lcom/helpshift/conversation/usersetup/UserSetupMediator;

    invoke-virtual {p1, p4}, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->registerUserSetupRenderer(Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;)V

    .line 38
    iget-object p1, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->userSetupDM:Lcom/helpshift/account/domainmodel/UserSetupDM;

    invoke-virtual {p1, p0}, Lcom/helpshift/account/domainmodel/UserSetupDM;->registerUserSetupListener(Lcom/helpshift/account/domainmodel/UserSetupDM$UserSetupListener;)V

    .line 40
    invoke-virtual {p2}, Lcom/helpshift/common/domain/Domain;->getAuthenticationFailureDM()Lcom/helpshift/account/AuthenticationFailureDM;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/helpshift/account/AuthenticationFailureDM;->registerListener(Lcom/helpshift/account/AuthenticationFailureDM$AuthenticationFailureObserver;)V

    return-void
.end method

.method private buildProgressBarWidget()Lcom/helpshift/widget/ProgressBarWidget;
    .locals 3

    .line 44
    new-instance v0, Lcom/helpshift/widget/ProgressBarWidget;

    invoke-direct {v0}, Lcom/helpshift/widget/ProgressBarWidget;-><init>()V

    .line 45
    iget-object v1, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->userSetupDM:Lcom/helpshift/account/domainmodel/UserSetupDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserSetupDM;->getState()Lcom/helpshift/account/domainmodel/UserSetupState;

    move-result-object v1

    .line 47
    sget-object v2, Lcom/helpshift/account/domainmodel/UserSetupState;->IN_PROGRESS:Lcom/helpshift/account/domainmodel/UserSetupState;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ProgressBarWidget;->setVisible(Z)V

    return-object v0
.end method

.method private handleUserSetupState(Lcom/helpshift/account/domainmodel/UserSetupState;)V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v0}, Lcom/helpshift/common/platform/Platform;->isOnline()Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/helpshift/conversation/usersetup/UserSetupVM;->onNetworkUnavailable()V

    return-void

    .line 72
    :cond_0
    sget-object v0, Lcom/helpshift/conversation/usersetup/UserSetupVM$1;->$SwitchMap$com$helpshift$account$domainmodel$UserSetupState:[I

    invoke-virtual {p1}, Lcom/helpshift/account/domainmodel/UserSetupState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v1, 0x2

    if-eq p1, v1, :cond_3

    const/4 v1, 0x3

    if-eq p1, v1, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    goto :goto_0

    .line 82
    :cond_1
    iget-object p1, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->userSetupMediator:Lcom/helpshift/conversation/usersetup/UserSetupMediator;

    invoke-virtual {p1}, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->onSetupCompleted()V

    goto :goto_0

    .line 78
    :cond_2
    iget-object p1, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    invoke-virtual {p1, v0}, Lcom/helpshift/widget/ProgressBarWidget;->setVisible(Z)V

    goto :goto_0

    .line 75
    :cond_3
    iget-object p1, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->progressDescriptionWidget:Lcom/helpshift/widget/ProgressDescriptionWidget;

    invoke-virtual {p1, v0}, Lcom/helpshift/widget/ProgressDescriptionWidget;->setVisible(Z)V

    :goto_0
    return-void
.end method


# virtual methods
.method public onAuthenticationFailure()V
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->userSetupMediator:Lcom/helpshift/conversation/usersetup/UserSetupMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->onAuthenticationFailure()V

    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->userSetupMediator:Lcom/helpshift/conversation/usersetup/UserSetupMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->unregisterUserSetupRenderer()V

    return-void
.end method

.method public onNetworkAvailable()V
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ProgressBarWidget;->setVisible(Z)V

    return-void
.end method

.method public onNetworkUnavailable()V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->userSetupMediator:Lcom/helpshift/conversation/usersetup/UserSetupMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->showOfflineError()V

    return-void
.end method

.method public onResume()V
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->userSetupMediator:Lcom/helpshift/conversation/usersetup/UserSetupMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->renderAll()V

    .line 53
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->userSetupDM:Lcom/helpshift/account/domainmodel/UserSetupDM;

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserSetupDM;->getState()Lcom/helpshift/account/domainmodel/UserSetupState;

    move-result-object v0

    .line 54
    sget-object v1, Lcom/helpshift/account/domainmodel/UserSetupState;->COMPLETED:Lcom/helpshift/account/domainmodel/UserSetupState;

    if-ne v0, v1, :cond_0

    .line 55
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->userSetupMediator:Lcom/helpshift/conversation/usersetup/UserSetupMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/usersetup/UserSetupMediator;->onSetupCompleted()V

    goto :goto_0

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/usersetup/UserSetupVM;->userSetupDM:Lcom/helpshift/account/domainmodel/UserSetupDM;

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserSetupDM;->startSetup()V

    :goto_0
    return-void
.end method

.method public userSetupStateChanged(Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/account/domainmodel/UserSetupState;)V
    .locals 0

    .line 89
    invoke-direct {p0, p2}, Lcom/helpshift/conversation/usersetup/UserSetupVM;->handleUserSetupState(Lcom/helpshift/account/domainmodel/UserSetupState;)V

    return-void
.end method
