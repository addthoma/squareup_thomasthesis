.class public interface abstract Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;
.super Ljava/lang/Object;
.source "UserSetupRenderer.java"


# virtual methods
.method public abstract hideNoInternetView()V
.end method

.method public abstract hideProgressBar()V
.end method

.method public abstract hideProgressDescription()V
.end method

.method public abstract onAuthenticationFailure()V
.end method

.method public abstract onUserSetupComplete()V
.end method

.method public abstract showNoInternetView()V
.end method

.method public abstract showProgressBar()V
.end method

.method public abstract showProgressDescription()V
.end method
