.class Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;
.super Lcom/helpshift/common/domain/F;
.source "NewConversationMediator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->onChanged(Lcom/helpshift/widget/Widget;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

.field final synthetic val$widget:Lcom/helpshift/widget/Widget;


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/viewmodel/NewConversationMediator;Lcom/helpshift/widget/Widget;)V
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    iput-object p2, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->val$widget:Lcom/helpshift/widget/Widget;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->val$widget:Lcom/helpshift/widget/Widget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->descriptionWidget:Lcom/helpshift/widget/TextWidget;

    if-ne v0, v1, :cond_0

    .line 94
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderDescription()V

    goto :goto_0

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->val$widget:Lcom/helpshift/widget/Widget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->nameWidget:Lcom/helpshift/widget/TextWidget;

    if-ne v0, v1, :cond_1

    .line 97
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderName()V

    goto :goto_0

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->val$widget:Lcom/helpshift/widget/Widget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->emailWidget:Lcom/helpshift/widget/EmailWidget;

    if-ne v0, v1, :cond_2

    .line 100
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderEmail()V

    goto :goto_0

    .line 102
    :cond_2
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->val$widget:Lcom/helpshift/widget/Widget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->startConversationButton:Lcom/helpshift/widget/ButtonWidget;

    if-ne v0, v1, :cond_3

    .line 103
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderStartNewConversationButton()V

    goto :goto_0

    .line 105
    :cond_3
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->val$widget:Lcom/helpshift/widget/Widget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->attachImageButton:Lcom/helpshift/widget/ButtonWidget;

    if-ne v0, v1, :cond_4

    .line 106
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderAttachImageButton()V

    goto :goto_0

    .line 108
    :cond_4
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->val$widget:Lcom/helpshift/widget/Widget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    if-ne v0, v1, :cond_5

    .line 109
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderProgressBarWidget()V

    .line 110
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->updateAttachImageButtonWidget()V

    .line 111
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->updateStartNewConversationButtonWidget()V

    .line 112
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->updateImageAttachmentWidget()V

    .line 115
    :cond_5
    :goto_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->val$widget:Lcom/helpshift/widget/Widget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->imageAttachmentWidget:Lcom/helpshift/widget/ImageAttachmentWidget;

    if-ne v0, v1, :cond_6

    .line 116
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderImageAttachment()V

    .line 117
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->updateAttachImageButtonWidget()V

    :cond_6
    return-void
.end method
