.class public Lcom/helpshift/conversation/viewmodel/NewConversationVM;
.super Ljava/lang/Object;
.source "NewConversationVM.java"

# interfaces
.implements Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;
.implements Lcom/helpshift/account/AuthenticationFailureDM$AuthenticationFailureObserver;


# static fields
.field private static final TAG:Ljava/lang/String; = "Helpshift_NewConvVM"


# instance fields
.field final conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

.field final descriptionWidget:Lcom/helpshift/widget/DescriptionWidget;

.field final domain:Lcom/helpshift/common/domain/Domain;

.field final emailWidget:Lcom/helpshift/widget/EmailWidget;

.field final imageAttachmentWidget:Lcom/helpshift/widget/ImageAttachmentWidget;

.field final mediator:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

.field final nameWidget:Lcom/helpshift/widget/NameWidget;

.field final platform:Lcom/helpshift/common/platform/Platform;

.field final progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

.field rendererWeakRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;",
            ">;"
        }
    .end annotation
.end field

.field final sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

.field wasSearchPerformed:Z

.field final widgetGateway:Lcom/helpshift/widget/WidgetGateway;


# direct methods
.method public constructor <init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;)V
    .locals 4

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 53
    iput-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->wasSearchPerformed:Z

    .line 58
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->platform:Lcom/helpshift/common/platform/Platform;

    .line 59
    iput-object p2, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    .line 60
    invoke-virtual {p2}, Lcom/helpshift/common/domain/Domain;->getSDKConfigurationDM()Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    .line 61
    iput-object p3, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    .line 64
    new-instance p1, Lcom/helpshift/widget/WidgetGateway;

    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-direct {p1, v0, p3}, Lcom/helpshift/widget/WidgetGateway;-><init>(Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;)V

    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    .line 65
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    invoke-virtual {p1}, Lcom/helpshift/widget/WidgetGateway;->makeDescriptionWidget()Lcom/helpshift/widget/DescriptionWidget;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->descriptionWidget:Lcom/helpshift/widget/DescriptionWidget;

    .line 66
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    invoke-virtual {p1}, Lcom/helpshift/widget/WidgetGateway;->makeNameWidget()Lcom/helpshift/widget/NameWidget;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->nameWidget:Lcom/helpshift/widget/NameWidget;

    .line 67
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    invoke-virtual {p1}, Lcom/helpshift/widget/WidgetGateway;->makeEmailWidget()Lcom/helpshift/widget/EmailWidget;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->emailWidget:Lcom/helpshift/widget/EmailWidget;

    .line 68
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    invoke-virtual {p1}, Lcom/helpshift/widget/WidgetGateway;->makeImageAttachmentWidget()Lcom/helpshift/widget/ImageAttachmentWidget;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->imageAttachmentWidget:Lcom/helpshift/widget/ImageAttachmentWidget;

    .line 69
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    invoke-virtual {p1}, Lcom/helpshift/widget/WidgetGateway;->makeProgressBarWidget()Lcom/helpshift/widget/ProgressBarWidget;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    .line 70
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->nameWidget:Lcom/helpshift/widget/NameWidget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->emailWidget:Lcom/helpshift/widget/EmailWidget;

    invoke-virtual {p1, v0, v1}, Lcom/helpshift/widget/WidgetGateway;->makeProfileFormWidget(Lcom/helpshift/widget/TextWidget;Lcom/helpshift/widget/TextWidget;)Lcom/helpshift/widget/ProfileFormWidget;

    move-result-object p1

    .line 71
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->imageAttachmentWidget:Lcom/helpshift/widget/ImageAttachmentWidget;

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/WidgetGateway;->makeNewConversationAttachImageButtonWidget(Lcom/helpshift/widget/ImageAttachmentWidget;)Lcom/helpshift/widget/ButtonWidget;

    move-result-object v0

    .line 72
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    invoke-virtual {v1}, Lcom/helpshift/widget/WidgetGateway;->makeStartConversationButtonWidget()Lcom/helpshift/widget/ButtonWidget;

    move-result-object v1

    .line 75
    new-instance v2, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    invoke-direct {v2, p4, p2}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;-><init>(Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;Lcom/helpshift/common/domain/Domain;)V

    iput-object v2, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->mediator:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    .line 76
    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->mediator:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    iget-object v3, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->descriptionWidget:Lcom/helpshift/widget/DescriptionWidget;

    invoke-virtual {v2, v3}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->setDescriptionWidget(Lcom/helpshift/widget/TextWidget;)V

    .line 77
    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->mediator:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    iget-object v3, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->nameWidget:Lcom/helpshift/widget/NameWidget;

    invoke-virtual {v2, v3}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->setNameWidget(Lcom/helpshift/widget/TextWidget;)V

    .line 78
    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->mediator:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    iget-object v3, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->emailWidget:Lcom/helpshift/widget/EmailWidget;

    invoke-virtual {v2, v3}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->setEmailWidget(Lcom/helpshift/widget/EmailWidget;)V

    .line 79
    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->mediator:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    iget-object v3, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->imageAttachmentWidget:Lcom/helpshift/widget/ImageAttachmentWidget;

    invoke-virtual {v2, v3}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->setImageAttachmentWidget(Lcom/helpshift/widget/ImageAttachmentWidget;)V

    .line 80
    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->mediator:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    invoke-virtual {v2, v1}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->setStartConversationButton(Lcom/helpshift/widget/ButtonWidget;)V

    .line 81
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->mediator:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    invoke-virtual {v1, v0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->setAttachImageButton(Lcom/helpshift/widget/ButtonWidget;)V

    .line 82
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->mediator:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->setProgressBarWidget(Lcom/helpshift/widget/ProgressBarWidget;)V

    .line 83
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->mediator:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->setProfileFormWidget(Lcom/helpshift/widget/ProfileFormWidget;)V

    .line 85
    invoke-virtual {p3, p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->registerStartNewConversationListener(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;)V

    .line 86
    invoke-virtual {p2}, Lcom/helpshift/common/domain/Domain;->getAuthenticationFailureDM()Lcom/helpshift/account/AuthenticationFailureDM;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/helpshift/account/AuthenticationFailureDM;->registerListener(Lcom/helpshift/account/AuthenticationFailureDM$AuthenticationFailureObserver;)V

    .line 88
    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->rendererWeakRef:Ljava/lang/ref/WeakReference;

    .line 89
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->mediator:Lcom/helpshift/conversation/viewmodel/NewConversationMediator;

    invoke-virtual {p1, p4}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->setRenderer(Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;)V

    return-void
.end method

.method private handleException(Ljava/lang/Exception;)V
    .locals 2

    .line 217
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/NewConversationVM$6;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/NewConversationVM$6;-><init>(Lcom/helpshift/conversation/viewmodel/NewConversationVM;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method private showSearchOrStartNewConversationInternal(Z)V
    .locals 2

    .line 135
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/NewConversationVM$4;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/NewConversationVM$4;-><init>(Lcom/helpshift/conversation/viewmodel/NewConversationVM;Z)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method


# virtual methods
.method public handleImageAttachmentClearButtonClick()V
    .locals 2

    .line 265
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ProgressBarWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/NewConversationVM$9;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/viewmodel/NewConversationVM$9;-><init>(Lcom/helpshift/conversation/viewmodel/NewConversationVM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    const/4 v0, 0x0

    .line 278
    invoke-virtual {p0, v0}, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->setImageAttachment(Lcom/helpshift/conversation/dto/ImagePickerFile;)V

    return-void
.end method

.method public handleImageAttachmentClick()V
    .locals 2

    .line 293
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ProgressBarWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/NewConversationVM$11;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/viewmodel/NewConversationVM$11;-><init>(Lcom/helpshift/conversation/viewmodel/NewConversationVM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public initialize()V
    .locals 2

    .line 316
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/NewConversationVM$12;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/viewmodel/NewConversationVM$12;-><init>(Lcom/helpshift/conversation/viewmodel/NewConversationVM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method isFormValid()Z
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->descriptionWidget:Lcom/helpshift/widget/DescriptionWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/DescriptionWidget;->validateText()V

    .line 233
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->nameWidget:Lcom/helpshift/widget/NameWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/NameWidget;->validateText()V

    .line 234
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->emailWidget:Lcom/helpshift/widget/EmailWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/EmailWidget;->validateText()V

    .line 235
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->descriptionWidget:Lcom/helpshift/widget/DescriptionWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/DescriptionWidget;->getError()Lcom/helpshift/widget/TextWidget$TextWidgetError;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->nameWidget:Lcom/helpshift/widget/NameWidget;

    .line 236
    invoke-virtual {v0}, Lcom/helpshift/widget/NameWidget;->getError()Lcom/helpshift/widget/TextWidget$TextWidgetError;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->emailWidget:Lcom/helpshift/widget/EmailWidget;

    .line 237
    invoke-virtual {v0}, Lcom/helpshift/widget/EmailWidget;->getError()Lcom/helpshift/widget/TextWidget$TextWidgetError;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onAuthenticationFailure()V
    .locals 2

    .line 349
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/NewConversationVM$15;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/viewmodel/NewConversationVM$15;-><init>(Lcom/helpshift/conversation/viewmodel/NewConversationVM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public onCreateConversationFailure(Ljava/lang/Exception;)V
    .locals 2

    .line 204
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ProgressBarWidget;->setVisible(Z)V

    .line 205
    invoke-direct {p0, p1}, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->handleException(Ljava/lang/Exception;)V

    return-void
.end method

.method public onCreateConversationSuccess(J)V
    .locals 2

    .line 181
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ProgressBarWidget;->setVisible(Z)V

    .line 182
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->descriptionWidget:Lcom/helpshift/widget/DescriptionWidget;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/DescriptionWidget;->setText(Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->imageAttachmentWidget:Lcom/helpshift/widget/ImageAttachmentWidget;

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ImageAttachmentWidget;->setImagePickerFile(Lcom/helpshift/conversation/dto/ImagePickerFile;)V

    .line 184
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/NewConversationVM$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/helpshift/conversation/viewmodel/NewConversationVM$5;-><init>(Lcom/helpshift/conversation/viewmodel/NewConversationVM;J)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public setConversationViewState(I)V
    .locals 2

    .line 339
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/NewConversationVM$14;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/NewConversationVM$14;-><init>(Lcom/helpshift/conversation/viewmodel/NewConversationVM;I)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/NewConversationVM$1;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/NewConversationVM$1;-><init>(Lcom/helpshift/conversation/viewmodel/NewConversationVM;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 2

    .line 241
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/NewConversationVM$7;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/NewConversationVM$7;-><init>(Lcom/helpshift/conversation/viewmodel/NewConversationVM;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public setImageAttachment(Lcom/helpshift/conversation/dto/ImagePickerFile;)V
    .locals 2

    .line 282
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/NewConversationVM$10;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/NewConversationVM$10;-><init>(Lcom/helpshift/conversation/viewmodel/NewConversationVM;Lcom/helpshift/conversation/dto/ImagePickerFile;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/NewConversationVM$2;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/NewConversationVM$2;-><init>(Lcom/helpshift/conversation/viewmodel/NewConversationVM;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public setSearchQuery(Ljava/lang/String;)V
    .locals 2

    .line 250
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/NewConversationVM$8;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/NewConversationVM$8;-><init>(Lcom/helpshift/conversation/viewmodel/NewConversationVM;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public setShouldDropCustomMetadata(Z)V
    .locals 2

    .line 330
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/NewConversationVM$13;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/NewConversationVM$13;-><init>(Lcom/helpshift/conversation/viewmodel/NewConversationVM;Z)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public setWasSearchPerformed(Z)V
    .locals 2

    .line 115
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/NewConversationVM$3;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/NewConversationVM$3;-><init>(Lcom/helpshift/conversation/viewmodel/NewConversationVM;Z)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method shouldShowSearchOnNewConversation()Z
    .locals 2

    .line 175
    iget-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->wasSearchPerformed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v1, "showSearchOnNewConversation"

    .line 176
    invoke-virtual {v0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public showSearchOrStartNewConversation()V
    .locals 1

    const/4 v0, 0x1

    .line 127
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->showSearchOrStartNewConversationInternal(Z)V

    return-void
.end method

.method public startNewConversation()V
    .locals 1

    const/4 v0, 0x0

    .line 131
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->showSearchOrStartNewConversationInternal(Z)V

    return-void
.end method

.method public unregisterRenderer(Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;)V
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->rendererWeakRef:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 210
    new-instance p1, Ljava/lang/ref/WeakReference;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->rendererWeakRef:Ljava/lang/ref/WeakReference;

    .line 212
    :cond_0
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getAuthenticationFailureDM()Lcom/helpshift/account/AuthenticationFailureDM;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/helpshift/account/AuthenticationFailureDM;->unregisterListener(Lcom/helpshift/account/AuthenticationFailureDM$AuthenticationFailureObserver;)V

    .line 213
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {p1, p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->unregisterStartNewConversationListener(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;)V

    return-void
.end method
