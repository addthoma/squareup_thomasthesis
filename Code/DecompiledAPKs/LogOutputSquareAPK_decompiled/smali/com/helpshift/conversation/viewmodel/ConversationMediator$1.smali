.class Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;
.super Lcom/helpshift/common/domain/F;
.source "ConversationMediator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/viewmodel/ConversationMediator;->onChanged(Lcom/helpshift/widget/Widget;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

.field final synthetic val$widget:Lcom/helpshift/widget/Widget;


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/viewmodel/ConversationMediator;Lcom/helpshift/widget/Widget;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    iput-object p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->val$widget:Lcom/helpshift/widget/Widget;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->val$widget:Lcom/helpshift/widget/Widget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->attachImageButton:Lcom/helpshift/widget/ButtonWidget;

    if-ne v0, v1, :cond_0

    .line 45
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderAttachImageButton()V

    goto :goto_0

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->val$widget:Lcom/helpshift/widget/Widget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->replyButtonWidget:Lcom/helpshift/widget/ButtonWidget;

    if-ne v0, v1, :cond_1

    .line 48
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderReplyButton()V

    goto :goto_0

    .line 50
    :cond_1
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->val$widget:Lcom/helpshift/widget/Widget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->replyFieldWidget:Lcom/helpshift/widget/ReplyFieldWidget;

    if-ne v0, v1, :cond_2

    .line 51
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderReplyButton()V

    goto :goto_0

    .line 53
    :cond_2
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->val$widget:Lcom/helpshift/widget/Widget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    if-ne v0, v1, :cond_3

    .line 54
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderReplyBoxWidget()V

    .line 55
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderAttachImageButton()V

    goto :goto_0

    .line 57
    :cond_3
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->val$widget:Lcom/helpshift/widget/Widget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->scrollJumperWidget:Lcom/helpshift/widget/ScrollJumperWidget;

    if-ne v0, v1, :cond_4

    .line 58
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-static {v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->access$000(Lcom/helpshift/conversation/viewmodel/ConversationMediator;)V

    goto :goto_0

    .line 60
    :cond_4
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->val$widget:Lcom/helpshift/widget/Widget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->confirmationBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    if-ne v0, v1, :cond_5

    .line 61
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderConfirmationBoxWidget()V

    goto :goto_0

    .line 63
    :cond_5
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->val$widget:Lcom/helpshift/widget/Widget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->conversationFooterWidget:Lcom/helpshift/widget/ConversationFooterWidget;

    if-ne v0, v1, :cond_6

    .line 64
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderConversationFooterWidget()V

    goto :goto_0

    .line 66
    :cond_6
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->val$widget:Lcom/helpshift/widget/Widget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->historyLoadingWidget:Lcom/helpshift/widget/HistoryLoadingWidget;

    if-ne v0, v1, :cond_7

    .line 67
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-static {v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->access$100(Lcom/helpshift/conversation/viewmodel/ConversationMediator;)V

    :cond_7
    :goto_0
    return-void
.end method
