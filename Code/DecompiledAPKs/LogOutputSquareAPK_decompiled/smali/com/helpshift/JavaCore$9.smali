.class Lcom/helpshift/JavaCore$9;
.super Lcom/helpshift/common/domain/F;
.source "JavaCore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/JavaCore;->updateApiConfig(Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/JavaCore;

.field final synthetic val$apiConfig:Ljava/util/Map;

.field final synthetic val$coreApi:Lcom/helpshift/CoreApi;


# direct methods
.method constructor <init>(Lcom/helpshift/JavaCore;Ljava/util/Map;Lcom/helpshift/CoreApi;)V
    .locals 0

    .line 281
    iput-object p1, p0, Lcom/helpshift/JavaCore$9;->this$0:Lcom/helpshift/JavaCore;

    iput-object p2, p0, Lcom/helpshift/JavaCore$9;->val$apiConfig:Ljava/util/Map;

    iput-object p3, p0, Lcom/helpshift/JavaCore$9;->val$coreApi:Lcom/helpshift/CoreApi;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 4

    .line 284
    iget-object v0, p0, Lcom/helpshift/JavaCore$9;->this$0:Lcom/helpshift/JavaCore;

    iget-object v0, v0, Lcom/helpshift/JavaCore;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    iget-object v1, p0, Lcom/helpshift/JavaCore$9;->val$apiConfig:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->updateApiConfig(Ljava/util/Map;)V

    .line 285
    iget-object v0, p0, Lcom/helpshift/JavaCore$9;->val$apiConfig:Ljava/util/Map;

    const-string v1, "enableFullPrivacy"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/helpshift/JavaCore$9;->val$apiConfig:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    new-instance v0, Lcom/helpshift/account/domainmodel/UserLoginManager;

    iget-object v1, p0, Lcom/helpshift/JavaCore$9;->val$coreApi:Lcom/helpshift/CoreApi;

    iget-object v2, p0, Lcom/helpshift/JavaCore$9;->this$0:Lcom/helpshift/JavaCore;

    invoke-static {v2}, Lcom/helpshift/JavaCore;->access$000(Lcom/helpshift/JavaCore;)Lcom/helpshift/common/domain/Domain;

    move-result-object v2

    iget-object v3, p0, Lcom/helpshift/JavaCore$9;->this$0:Lcom/helpshift/JavaCore;

    iget-object v3, v3, Lcom/helpshift/JavaCore;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v0, v1, v2, v3}, Lcom/helpshift/account/domainmodel/UserLoginManager;-><init>(Lcom/helpshift/CoreApi;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserLoginManager;->clearPersonallyIdentifiableInformation()V

    :cond_0
    return-void
.end method
