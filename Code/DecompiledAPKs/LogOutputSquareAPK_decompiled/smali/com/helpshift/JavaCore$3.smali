.class Lcom/helpshift/JavaCore$3;
.super Lcom/helpshift/common/domain/F;
.source "JavaCore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/JavaCore;->setPushToken(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/JavaCore;

.field final synthetic val$pushToken:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/helpshift/JavaCore;Ljava/lang/String;)V
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/helpshift/JavaCore$3;->this$0:Lcom/helpshift/JavaCore;

    iput-object p2, p0, Lcom/helpshift/JavaCore$3;->val$pushToken:Ljava/lang/String;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 2

    .line 194
    iget-object v0, p0, Lcom/helpshift/JavaCore$3;->val$pushToken:Ljava/lang/String;

    if-nez v0, :cond_0

    return-void

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/helpshift/JavaCore$3;->this$0:Lcom/helpshift/JavaCore;

    iget-object v0, v0, Lcom/helpshift/JavaCore;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v0}, Lcom/helpshift/common/platform/Platform;->getDevice()Lcom/helpshift/common/platform/Device;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/common/platform/Device;->getPushToken()Ljava/lang/String;

    move-result-object v0

    .line 199
    iget-object v1, p0, Lcom/helpshift/JavaCore$3;->val$pushToken:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 203
    :cond_1
    iget-object v0, p0, Lcom/helpshift/JavaCore$3;->this$0:Lcom/helpshift/JavaCore;

    iget-object v0, v0, Lcom/helpshift/JavaCore;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v0}, Lcom/helpshift/common/platform/Platform;->getDevice()Lcom/helpshift/common/platform/Device;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/JavaCore$3;->val$pushToken:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/helpshift/common/platform/Device;->setPushToken(Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/helpshift/JavaCore$3;->this$0:Lcom/helpshift/JavaCore;

    invoke-virtual {v0}, Lcom/helpshift/JavaCore;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserManagerDM;->resetPushTokenSyncStatusForUsers()V

    .line 208
    iget-object v0, p0, Lcom/helpshift/JavaCore$3;->this$0:Lcom/helpshift/JavaCore;

    invoke-virtual {v0}, Lcom/helpshift/JavaCore;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserManagerDM;->sendPushToken()V

    return-void
.end method
