.class public final Lcom/evernote/android/job/Job$Params;
.super Ljava/lang/Object;
.source "Job.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/evernote/android/job/Job;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Params"
.end annotation


# instance fields
.field private mExtras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

.field private final mRequest:Lcom/evernote/android/job/JobRequest;

.field private mTransientExtras:Landroid/os/Bundle;


# direct methods
.method private constructor <init>(Lcom/evernote/android/job/JobRequest;Landroid/os/Bundle;)V
    .locals 0

    .line 373
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374
    iput-object p1, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    .line 375
    iput-object p2, p0, Lcom/evernote/android/job/Job$Params;->mTransientExtras:Landroid/os/Bundle;

    return-void
.end method

.method synthetic constructor <init>(Lcom/evernote/android/job/JobRequest;Landroid/os/Bundle;Lcom/evernote/android/job/Job$1;)V
    .locals 0

    .line 367
    invoke-direct {p0, p1, p2}, Lcom/evernote/android/job/Job$Params;-><init>(Lcom/evernote/android/job/JobRequest;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-eqz p1, :cond_2

    .line 612
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 614
    :cond_1
    check-cast p1, Lcom/evernote/android/job/Job$Params;

    .line 616
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    iget-object p1, p1, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0, p1}, Lcom/evernote/android/job/JobRequest;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public getBackoffMs()J
    .locals 2

    .line 476
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->getBackoffMs()J

    move-result-wide v0

    return-wide v0
.end method

.method public getBackoffPolicy()Lcom/evernote/android/job/JobRequest$BackoffPolicy;
    .locals 1

    .line 486
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->getBackoffPolicy()Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    move-result-object v0

    return-object v0
.end method

.method public getEndMs()J
    .locals 2

    .line 428
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->getEndMs()J

    move-result-wide v0

    return-wide v0
.end method

.method public getExtras()Lcom/evernote/android/job/util/support/PersistableBundleCompat;
    .locals 1

    .line 596
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mExtras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    if-nez v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->getExtras()Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/android/job/Job$Params;->mExtras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    .line 598
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mExtras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    if-nez v0, :cond_0

    .line 599
    new-instance v0, Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    invoke-direct {v0}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;-><init>()V

    iput-object v0, p0, Lcom/evernote/android/job/Job$Params;->mExtras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    .line 602
    :cond_0
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mExtras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    return-object v0
.end method

.method public getFailureCount()I
    .locals 1

    .line 549
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->getFailureCount()I

    move-result v0

    return v0
.end method

.method public getFlexMs()J
    .locals 2

    .line 449
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->getFlexMs()J

    move-result-wide v0

    return-wide v0
.end method

.method public getId()I
    .locals 1

    .line 383
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->getJobId()I

    move-result v0

    return v0
.end method

.method public getIntervalMs()J
    .locals 2

    .line 438
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->getIntervalMs()J

    move-result-wide v0

    return-wide v0
.end method

.method public getLastRun()J
    .locals 2

    .line 559
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->getLastRun()J

    move-result-wide v0

    return-wide v0
.end method

.method getRequest()Lcom/evernote/android/job/JobRequest;
    .locals 1

    .line 606
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    return-object v0
.end method

.method public getScheduledAt()J
    .locals 2

    .line 465
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->getScheduledAt()J

    move-result-wide v0

    return-wide v0
.end method

.method public getStartMs()J
    .locals 2

    .line 418
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->getStartMs()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .line 391
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->getTag()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTransientExtras()Landroid/os/Bundle;
    .locals 1

    .line 588
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mTransientExtras:Landroid/os/Bundle;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .line 621
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->hashCode()I

    move-result v0

    return v0
.end method

.method public isExact()Z
    .locals 1

    .line 408
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->isExact()Z

    move-result v0

    return v0
.end method

.method public isPeriodic()Z
    .locals 1

    .line 400
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->isPeriodic()Z

    move-result v0

    return v0
.end method

.method public isTransient()Z
    .locals 1

    .line 570
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->isTransient()Z

    move-result v0

    return v0
.end method

.method public requiredNetworkType()Lcom/evernote/android/job/JobRequest$NetworkType;
    .locals 1

    .line 516
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->requiredNetworkType()Lcom/evernote/android/job/JobRequest$NetworkType;

    move-result-object v0

    return-object v0
.end method

.method public requirementsEnforced()Z
    .locals 1

    .line 539
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->requirementsEnforced()Z

    move-result v0

    return v0
.end method

.method public requiresBatteryNotLow()Z
    .locals 1

    .line 523
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->requiresBatteryNotLow()Z

    move-result v0

    return v0
.end method

.method public requiresCharging()Z
    .locals 1

    .line 496
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->requiresCharging()Z

    move-result v0

    return v0
.end method

.method public requiresDeviceIdle()Z
    .locals 1

    .line 506
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->requiresDeviceIdle()Z

    move-result v0

    return v0
.end method

.method public requiresStorageNotLow()Z
    .locals 1

    .line 530
    iget-object v0, p0, Lcom/evernote/android/job/Job$Params;->mRequest:Lcom/evernote/android/job/JobRequest;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->requiresStorageNotLow()Z

    move-result v0

    return v0
.end method
