.class public final Lcom/evernote/android/job/JobConfig;
.super Ljava/lang/Object;
.source "JobConfig.java"


# static fields
.field private static final CAT:Lcom/evernote/android/job/util/JobCat;

.field private static final DEFAULT_EXECUTOR_SERVICE:Ljava/util/concurrent/ExecutorService;

.field private static final DEFAULT_JOB_RESCHEDULE_PAUSE:J = 0xbb8L

.field private static final ENABLED_APIS:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap<",
            "Lcom/evernote/android/job/JobApi;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile allowSmallerIntervals:Z

.field private static volatile clock:Lcom/evernote/android/job/util/Clock;

.field private static volatile closeDatabase:Z

.field private static volatile executorService:Ljava/util/concurrent/ExecutorService;

.field private static volatile forceAllowApi14:Z

.field private static volatile forceRtc:Z

.field private static volatile jobIdOffset:I

.field private static volatile jobReschedulePause:J

.field private static volatile skipJobReschedule:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 52
    new-instance v0, Lcom/evernote/android/job/util/JobCat;

    const-string v1, "JobConfig"

    invoke-direct {v0, v1}, Lcom/evernote/android/job/util/JobCat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/android/job/JobConfig;->CAT:Lcom/evernote/android/job/util/JobCat;

    .line 54
    new-instance v0, Lcom/evernote/android/job/JobConfig$1;

    invoke-direct {v0}, Lcom/evernote/android/job/JobConfig$1;-><init>()V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/evernote/android/job/JobConfig;->DEFAULT_EXECUTOR_SERVICE:Ljava/util/concurrent/ExecutorService;

    const/4 v0, 0x0

    .line 72
    sput-boolean v0, Lcom/evernote/android/job/JobConfig;->forceAllowApi14:Z

    const-wide/16 v1, 0xbb8

    .line 74
    sput-wide v1, Lcom/evernote/android/job/JobConfig;->jobReschedulePause:J

    .line 75
    sput-boolean v0, Lcom/evernote/android/job/JobConfig;->skipJobReschedule:Z

    .line 77
    sput v0, Lcom/evernote/android/job/JobConfig;->jobIdOffset:I

    .line 79
    sput-boolean v0, Lcom/evernote/android/job/JobConfig;->forceRtc:Z

    .line 81
    sget-object v1, Lcom/evernote/android/job/util/Clock;->DEFAULT:Lcom/evernote/android/job/util/Clock;

    sput-object v1, Lcom/evernote/android/job/JobConfig;->clock:Lcom/evernote/android/job/util/Clock;

    .line 82
    sget-object v1, Lcom/evernote/android/job/JobConfig;->DEFAULT_EXECUTOR_SERVICE:Ljava/util/concurrent/ExecutorService;

    sput-object v1, Lcom/evernote/android/job/JobConfig;->executorService:Ljava/util/concurrent/ExecutorService;

    .line 83
    sput-boolean v0, Lcom/evernote/android/job/JobConfig;->closeDatabase:Z

    .line 86
    new-instance v1, Ljava/util/EnumMap;

    const-class v2, Lcom/evernote/android/job/JobApi;

    invoke-direct {v1, v2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sput-object v1, Lcom/evernote/android/job/JobConfig;->ENABLED_APIS:Ljava/util/EnumMap;

    .line 87
    invoke-static {}, Lcom/evernote/android/job/JobApi;->values()[Lcom/evernote/android/job/JobApi;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 88
    sget-object v4, Lcom/evernote/android/job/JobConfig;->ENABLED_APIS:Ljava/util/EnumMap;

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4, v3, v5}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static declared-synchronized addLogger(Lcom/evernote/android/job/util/JobLogger;)Z
    .locals 1

    const-class v0, Lcom/evernote/android/job/JobConfig;

    monitor-enter v0

    .line 186
    :try_start_0
    invoke-static {p0}, Lcom/evernote/android/job/util/JobCat;->addLogger(Lcom/evernote/android/job/util/JobLogger;)Z

    move-result p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static forceApi(Lcom/evernote/android/job/JobApi;)V
    .locals 7

    .line 122
    invoke-static {}, Lcom/evernote/android/job/JobApi;->values()[Lcom/evernote/android/job/JobApi;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x1

    if-ge v3, v1, :cond_1

    aget-object v5, v0, v3

    .line 123
    sget-object v6, Lcom/evernote/android/job/JobConfig;->ENABLED_APIS:Ljava/util/EnumMap;

    if-ne v5, p0, :cond_0

    goto :goto_1

    :cond_0
    const/4 v4, 0x0

    :goto_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v6, v5, v4}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 125
    :cond_1
    sget-object v0, Lcom/evernote/android/job/JobConfig;->CAT:Lcom/evernote/android/job/util/JobCat;

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p0, v1, v2

    const-string p0, "forceApi - %s"

    invoke-virtual {v0, p0, v1}, Lcom/evernote/android/job/util/JobCat;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getClock()Lcom/evernote/android/job/util/Clock;
    .locals 1

    .line 284
    sget-object v0, Lcom/evernote/android/job/JobConfig;->clock:Lcom/evernote/android/job/util/Clock;

    return-object v0
.end method

.method public static getExecutorService()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .line 296
    sget-object v0, Lcom/evernote/android/job/JobConfig;->executorService:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public static getJobIdOffset()I
    .locals 1

    .line 244
    sget v0, Lcom/evernote/android/job/JobConfig;->jobIdOffset:I

    return v0
.end method

.method public static getJobReschedulePause()J
    .locals 2

    .line 219
    sget-wide v0, Lcom/evernote/android/job/JobConfig;->jobReschedulePause:J

    return-wide v0
.end method

.method public static isAllowSmallerIntervalsForMarshmallow()Z
    .locals 2

    .line 136
    sget-boolean v0, Lcom/evernote/android/job/JobConfig;->allowSmallerIntervals:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static isApiEnabled(Lcom/evernote/android/job/JobApi;)Z
    .locals 1

    .line 97
    sget-object v0, Lcom/evernote/android/job/JobConfig;->ENABLED_APIS:Ljava/util/EnumMap;

    invoke-virtual {v0, p0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method public static isCloseDatabase()Z
    .locals 1

    .line 313
    sget-boolean v0, Lcom/evernote/android/job/JobConfig;->closeDatabase:Z

    return v0
.end method

.method public static isForceAllowApi14()Z
    .locals 1

    .line 175
    sget-boolean v0, Lcom/evernote/android/job/JobConfig;->forceAllowApi14:Z

    return v0
.end method

.method public static isForceRtc()Z
    .locals 1

    .line 268
    sget-boolean v0, Lcom/evernote/android/job/JobConfig;->forceRtc:Z

    return v0
.end method

.method public static isLogcatEnabled()Z
    .locals 1

    .line 212
    invoke-static {}, Lcom/evernote/android/job/util/JobCat;->isLogcatEnabled()Z

    move-result v0

    return v0
.end method

.method static isSkipJobReschedule()Z
    .locals 1

    .line 233
    sget-boolean v0, Lcom/evernote/android/job/JobConfig;->skipJobReschedule:Z

    return v0
.end method

.method public static declared-synchronized removeLogger(Lcom/evernote/android/job/util/JobLogger;)V
    .locals 1

    const-class v0, Lcom/evernote/android/job/JobConfig;

    monitor-enter v0

    .line 196
    :try_start_0
    invoke-static {p0}, Lcom/evernote/android/job/util/JobCat;->removeLogger(Lcom/evernote/android/job/util/JobLogger;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static reset()V
    .locals 7

    .line 330
    invoke-static {}, Lcom/evernote/android/job/JobApi;->values()[Lcom/evernote/android/job/JobApi;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, v0, v3

    .line 331
    sget-object v5, Lcom/evernote/android/job/JobConfig;->ENABLED_APIS:Ljava/util/EnumMap;

    sget-object v6, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v5, v4, v6}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 333
    :cond_0
    sput-boolean v2, Lcom/evernote/android/job/JobConfig;->allowSmallerIntervals:Z

    .line 334
    sput-boolean v2, Lcom/evernote/android/job/JobConfig;->forceAllowApi14:Z

    const-wide/16 v0, 0xbb8

    .line 335
    sput-wide v0, Lcom/evernote/android/job/JobConfig;->jobReschedulePause:J

    .line 336
    sput-boolean v2, Lcom/evernote/android/job/JobConfig;->skipJobReschedule:Z

    .line 337
    sput v2, Lcom/evernote/android/job/JobConfig;->jobIdOffset:I

    .line 338
    sput-boolean v2, Lcom/evernote/android/job/JobConfig;->forceRtc:Z

    .line 339
    sget-object v0, Lcom/evernote/android/job/util/Clock;->DEFAULT:Lcom/evernote/android/job/util/Clock;

    sput-object v0, Lcom/evernote/android/job/JobConfig;->clock:Lcom/evernote/android/job/util/Clock;

    .line 340
    sget-object v0, Lcom/evernote/android/job/JobConfig;->DEFAULT_EXECUTOR_SERVICE:Ljava/util/concurrent/ExecutorService;

    sput-object v0, Lcom/evernote/android/job/JobConfig;->executorService:Ljava/util/concurrent/ExecutorService;

    .line 341
    sput-boolean v2, Lcom/evernote/android/job/JobConfig;->closeDatabase:Z

    const/4 v0, 0x1

    .line 342
    invoke-static {v0}, Lcom/evernote/android/job/util/JobCat;->setLogcatEnabled(Z)V

    .line 343
    invoke-static {}, Lcom/evernote/android/job/util/JobCat;->clearLogger()V

    return-void
.end method

.method public static setAllowSmallerIntervalsForMarshmallow(Z)V
    .locals 2

    if-eqz p0, :cond_1

    .line 147
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-ge v0, v1, :cond_0

    goto :goto_0

    .line 148
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "This method is only allowed to call on Android M or earlier"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 150
    :cond_1
    :goto_0
    sput-boolean p0, Lcom/evernote/android/job/JobConfig;->allowSmallerIntervals:Z

    return-void
.end method

.method public static setApiEnabled(Lcom/evernote/android/job/JobApi;Z)V
    .locals 3

    .line 111
    sget-object v0, Lcom/evernote/android/job/JobConfig;->ENABLED_APIS:Ljava/util/EnumMap;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/evernote/android/job/JobConfig;->CAT:Lcom/evernote/android/job/util/JobCat;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    const/4 p1, 0x1

    aput-object p0, v1, p1

    const-string p0, "setApiEnabled - %s, %b"

    invoke-virtual {v0, p0, v1}, Lcom/evernote/android/job/util/JobCat;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static setClock(Lcom/evernote/android/job/util/Clock;)V
    .locals 0

    .line 289
    sput-object p0, Lcom/evernote/android/job/JobConfig;->clock:Lcom/evernote/android/job/util/Clock;

    return-void
.end method

.method public static setCloseDatabase(Z)V
    .locals 0

    .line 323
    sput-boolean p0, Lcom/evernote/android/job/JobConfig;->closeDatabase:Z

    return-void
.end method

.method public static setExecutorService(Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .line 306
    invoke-static {p0}, Lcom/evernote/android/job/util/JobPreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/ExecutorService;

    sput-object p0, Lcom/evernote/android/job/JobConfig;->executorService:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public static setForceAllowApi14(Z)V
    .locals 0

    .line 168
    sput-boolean p0, Lcom/evernote/android/job/JobConfig;->forceAllowApi14:Z

    return-void
.end method

.method public static setForceRtc(Z)V
    .locals 0

    .line 277
    sput-boolean p0, Lcom/evernote/android/job/JobConfig;->forceRtc:Z

    return-void
.end method

.method public static setJobIdOffset(I)V
    .locals 1

    const-string v0, "offset can\'t be negative"

    .line 254
    invoke-static {p0, v0}, Lcom/evernote/android/job/util/JobPreconditions;->checkArgumentNonnegative(ILjava/lang/String;)I

    const v0, 0x7fffefcc

    if-gt p0, v0, :cond_0

    .line 259
    sput p0, Lcom/evernote/android/job/JobConfig;->jobIdOffset:I

    return-void

    .line 256
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "offset is too close to Integer.MAX_VALUE"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static setJobReschedulePause(JLjava/util/concurrent/TimeUnit;)V
    .locals 0

    .line 229
    invoke-virtual {p2, p0, p1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide p0

    sput-wide p0, Lcom/evernote/android/job/JobConfig;->jobReschedulePause:J

    return-void
.end method

.method public static setLogcatEnabled(Z)V
    .locals 0

    .line 205
    invoke-static {p0}, Lcom/evernote/android/job/util/JobCat;->setLogcatEnabled(Z)V

    return-void
.end method

.method static setSkipJobReschedule(Z)V
    .locals 0

    .line 237
    sput-boolean p0, Lcom/evernote/android/job/JobConfig;->skipJobReschedule:Z

    return-void
.end method
