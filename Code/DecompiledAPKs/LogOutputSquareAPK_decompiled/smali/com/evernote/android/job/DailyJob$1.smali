.class final Lcom/evernote/android/job/DailyJob$1;
.super Ljava/lang/Object;
.source "DailyJob.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/android/job/DailyJob;->scheduleAsync(Lcom/evernote/android/job/JobRequest$Builder;JJLcom/evernote/android/job/JobRequest$JobScheduledCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$baseBuilder:Lcom/evernote/android/job/JobRequest$Builder;

.field final synthetic val$callback:Lcom/evernote/android/job/JobRequest$JobScheduledCallback;

.field final synthetic val$endMs:J

.field final synthetic val$startMs:J


# direct methods
.method constructor <init>(Lcom/evernote/android/job/JobRequest$Builder;JJLcom/evernote/android/job/JobRequest$JobScheduledCallback;)V
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/evernote/android/job/DailyJob$1;->val$baseBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    iput-wide p2, p0, Lcom/evernote/android/job/DailyJob$1;->val$startMs:J

    iput-wide p4, p0, Lcom/evernote/android/job/DailyJob$1;->val$endMs:J

    iput-object p6, p0, Lcom/evernote/android/job/DailyJob$1;->val$callback:Lcom/evernote/android/job/JobRequest$JobScheduledCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 112
    :try_start_0
    iget-object v0, p0, Lcom/evernote/android/job/DailyJob$1;->val$baseBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    iget-wide v1, p0, Lcom/evernote/android/job/DailyJob$1;->val$startMs:J

    iget-wide v3, p0, Lcom/evernote/android/job/DailyJob$1;->val$endMs:J

    invoke-static {v0, v1, v2, v3, v4}, Lcom/evernote/android/job/DailyJob;->schedule(Lcom/evernote/android/job/JobRequest$Builder;JJ)I

    move-result v0

    .line 113
    iget-object v1, p0, Lcom/evernote/android/job/DailyJob$1;->val$callback:Lcom/evernote/android/job/JobRequest$JobScheduledCallback;

    iget-object v2, p0, Lcom/evernote/android/job/DailyJob$1;->val$baseBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    iget-object v2, v2, Lcom/evernote/android/job/JobRequest$Builder;->mTag:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v0, v2, v3}, Lcom/evernote/android/job/JobRequest$JobScheduledCallback;->onJobScheduled(ILjava/lang/String;Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 115
    iget-object v1, p0, Lcom/evernote/android/job/DailyJob$1;->val$callback:Lcom/evernote/android/job/JobRequest$JobScheduledCallback;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/evernote/android/job/DailyJob$1;->val$baseBuilder:Lcom/evernote/android/job/JobRequest$Builder;

    iget-object v3, v3, Lcom/evernote/android/job/JobRequest$Builder;->mTag:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/evernote/android/job/JobRequest$JobScheduledCallback;->onJobScheduled(ILjava/lang/String;Ljava/lang/Exception;)V

    :goto_0
    return-void
.end method
