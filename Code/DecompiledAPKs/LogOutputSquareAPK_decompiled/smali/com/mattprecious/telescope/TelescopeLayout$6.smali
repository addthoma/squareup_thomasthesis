.class Lcom/mattprecious/telescope/TelescopeLayout$6;
.super Ljava/lang/Object;
.source "TelescopeLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mattprecious/telescope/TelescopeLayout;->captureNativeScreenshot(Landroid/media/projection/MediaProjection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mattprecious/telescope/TelescopeLayout;

.field final synthetic val$projection:Landroid/media/projection/MediaProjection;


# direct methods
.method constructor <init>(Lcom/mattprecious/telescope/TelescopeLayout;Landroid/media/projection/MediaProjection;)V
    .locals 0

    .line 609
    iput-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout$6;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    iput-object p2, p0, Lcom/mattprecious/telescope/TelescopeLayout$6;->val$projection:Landroid/media/projection/MediaProjection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .line 611
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 612
    iget-object v1, p0, Lcom/mattprecious/telescope/TelescopeLayout$6;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    invoke-static {v1}, Lcom/mattprecious/telescope/TelescopeLayout;->access$1400(Lcom/mattprecious/telescope/TelescopeLayout;)Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 613
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 614
    iget v11, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    const/4 v2, 0x1

    const/4 v3, 0x2

    .line 616
    invoke-static {v1, v11, v2, v3}, Landroid/media/ImageReader;->newInstance(IIII)Landroid/media/ImageReader;

    move-result-object v12

    .line 617
    invoke-virtual {v12}, Landroid/media/ImageReader;->getSurface()Landroid/view/Surface;

    move-result-object v8

    .line 619
    iget-object v2, p0, Lcom/mattprecious/telescope/TelescopeLayout$6;->val$projection:Landroid/media/projection/MediaProjection;

    iget v6, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const-string v3, "telescope"

    const/4 v7, 0x2

    const/4 v9, 0x0

    const/4 v10, 0x0

    move v4, v1

    move v5, v11

    .line 620
    invoke-virtual/range {v2 .. v10}, Landroid/media/projection/MediaProjection;->createVirtualDisplay(Ljava/lang/String;IIIILandroid/view/Surface;Landroid/hardware/display/VirtualDisplay$Callback;Landroid/os/Handler;)Landroid/hardware/display/VirtualDisplay;

    move-result-object v0

    .line 623
    new-instance v2, Lcom/mattprecious/telescope/TelescopeLayout$6$1;

    invoke-direct {v2, p0, v1, v11, v0}, Lcom/mattprecious/telescope/TelescopeLayout$6$1;-><init>(Lcom/mattprecious/telescope/TelescopeLayout$6;IILandroid/hardware/display/VirtualDisplay;)V

    .line 685
    invoke-static {}, Lcom/mattprecious/telescope/TelescopeLayout;->access$1500()Landroid/os/Handler;

    move-result-object v0

    .line 623
    invoke-virtual {v12, v2, v0}, Landroid/media/ImageReader;->setOnImageAvailableListener(Landroid/media/ImageReader$OnImageAvailableListener;Landroid/os/Handler;)V

    return-void
.end method
