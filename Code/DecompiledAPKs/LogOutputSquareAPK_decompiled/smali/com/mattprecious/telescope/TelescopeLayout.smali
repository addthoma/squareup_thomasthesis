.class public Lcom/mattprecious/telescope/TelescopeLayout;
.super Landroid/widget/FrameLayout;
.source "TelescopeLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mattprecious/telescope/TelescopeLayout$SaveScreenshotTask;
    }
.end annotation


# static fields
.field private static final CANCEL_DURATION_MS:J = 0xfaL

.field private static final DEFAULT_POINTER_COUNT:I = 0x2

.field private static final DEFAULT_PROGRESS_COLOR:I = -0xde690d

.field private static final DONE_DURATION_MS:J = 0x3e8L

.field private static final PROGRESS_STROKE_DP:I = 0x4

.field private static final SCREENSHOT_FILE_FORMAT:Ljava/text/SimpleDateFormat;

.field private static final TAG:Ljava/lang/String; = "Telescope"

.field private static final TRIGGER_DURATION_MS:J = 0x3e8L

.field private static final VIBRATION_DURATION_MS:J = 0x32L

.field private static backgroundHandler:Landroid/os/Handler;


# instance fields
.field private capturing:Z

.field private final doneAnimator:Landroid/animation/ValueAnimator;

.field private doneFraction:F

.field private final halfStrokeWidth:F

.field private final handler:Landroid/os/Handler;

.field private lens:Lcom/mattprecious/telescope/Lens;

.field private pointerCount:I

.field private pressing:Z

.field private final progressAnimator:Landroid/animation/ValueAnimator;

.field private final progressCancelAnimator:Landroid/animation/ValueAnimator;

.field private progressFraction:F

.field private final progressPaint:Landroid/graphics/Paint;

.field private final projectionManager:Landroid/media/projection/MediaProjectionManager;

.field private final requestCaptureFilter:Landroid/content/IntentFilter;

.field private final requestCaptureReceiver:Landroid/content/BroadcastReceiver;

.field private saving:Z

.field private screenshotChildrenOnly:Z

.field private screenshotMode:Lcom/mattprecious/telescope/ScreenshotMode;

.field private screenshotTarget:Landroid/view/View;

.field private final trigger:Ljava/lang/Runnable;

.field private vibrate:Z

.field private final vibrator:Landroid/os/Vibrator;

.field private final windowManager:Landroid/view/WindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 61
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "\'telescope\'-yyyy-MM-dd-HHmmss.\'png\'"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/mattprecious/telescope/TelescopeLayout;->SCREENSHOT_FILE_FORMAT:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 107
    invoke-direct {p0, p1, v0}, Lcom/mattprecious/telescope/TelescopeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 111
    invoke-direct {p0, p1, p2, v0}, Lcom/mattprecious/telescope/TelescopeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .line 115
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 77
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->handler:Landroid/os/Handler;

    .line 78
    new-instance v0, Lcom/mattprecious/telescope/TelescopeLayout$1;

    invoke-direct {v0, p0}, Lcom/mattprecious/telescope/TelescopeLayout$1;-><init>(Lcom/mattprecious/telescope/TelescopeLayout;)V

    iput-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->trigger:Ljava/lang/Runnable;

    const/4 v0, 0x0

    .line 116
    invoke-virtual {p0, v0}, Lcom/mattprecious/telescope/TelescopeLayout;->setWillNotDraw(Z)V

    .line 117
    iput-object p0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->screenshotTarget:Landroid/view/View;

    .line 119
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x40800000    # 4.0f

    mul-float v1, v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v1, v2

    .line 120
    iput v2, p0, Lcom/mattprecious/telescope/TelescopeLayout;->halfStrokeWidth:F

    .line 122
    sget-object v2, Lcom/mattprecious/telescope/R$styleable;->telescope_TelescopeLayout:[I

    .line 123
    invoke-virtual {p1, p2, v2, p3, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 124
    sget p3, Lcom/mattprecious/telescope/R$styleable;->telescope_TelescopeLayout_telescope_pointerCount:I

    const/4 v2, 0x2

    invoke-virtual {p2, p3, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p3

    iput p3, p0, Lcom/mattprecious/telescope/TelescopeLayout;->pointerCount:I

    .line 126
    sget p3, Lcom/mattprecious/telescope/R$styleable;->telescope_TelescopeLayout_telescope_progressColor:I

    const v3, -0xde690d

    invoke-virtual {p2, p3, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p3

    .line 128
    invoke-static {}, Lcom/mattprecious/telescope/ScreenshotMode;->values()[Lcom/mattprecious/telescope/ScreenshotMode;

    move-result-object v3

    sget v4, Lcom/mattprecious/telescope/R$styleable;->telescope_TelescopeLayout_telescope_screenshotMode:I

    sget-object v5, Lcom/mattprecious/telescope/ScreenshotMode;->SYSTEM:Lcom/mattprecious/telescope/ScreenshotMode;

    .line 130
    invoke-virtual {v5}, Lcom/mattprecious/telescope/ScreenshotMode;->ordinal()I

    move-result v5

    .line 128
    invoke-virtual {p2, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, p0, Lcom/mattprecious/telescope/TelescopeLayout;->screenshotMode:Lcom/mattprecious/telescope/ScreenshotMode;

    .line 131
    sget v3, Lcom/mattprecious/telescope/R$styleable;->telescope_TelescopeLayout_telescope_screenshotChildrenOnly:I

    .line 132
    invoke-virtual {p2, v3, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->screenshotChildrenOnly:Z

    .line 133
    sget v0, Lcom/mattprecious/telescope/R$styleable;->telescope_TelescopeLayout_telescope_vibrate:I

    const/4 v3, 0x1

    invoke-virtual {p2, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->vibrate:Z

    .line 134
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 136
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p2, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressPaint:Landroid/graphics/Paint;

    .line 137
    iget-object p2, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressPaint:Landroid/graphics/Paint;

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 138
    iget-object p2, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressPaint:Landroid/graphics/Paint;

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 139
    iget-object p2, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressPaint:Landroid/graphics/Paint;

    sget-object p3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 141
    new-instance p2, Lcom/mattprecious/telescope/TelescopeLayout$2;

    invoke-direct {p2, p0}, Lcom/mattprecious/telescope/TelescopeLayout$2;-><init>(Lcom/mattprecious/telescope/TelescopeLayout;)V

    .line 148
    new-instance p3, Landroid/animation/ValueAnimator;

    invoke-direct {p3}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object p3, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressAnimator:Landroid/animation/ValueAnimator;

    .line 149
    iget-object p3, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v0, 0x3e8

    invoke-virtual {p3, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 150
    iget-object p3, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {p3, p2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 152
    new-instance p3, Landroid/animation/ValueAnimator;

    invoke-direct {p3}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object p3, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressCancelAnimator:Landroid/animation/ValueAnimator;

    .line 153
    iget-object p3, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressCancelAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v3, 0xfa

    invoke-virtual {p3, v3, v4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 154
    iget-object p3, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressCancelAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {p3, p2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    const/high16 p2, 0x3f800000    # 1.0f

    .line 156
    iput p2, p0, Lcom/mattprecious/telescope/TelescopeLayout;->doneFraction:F

    new-array p2, v2, [F

    .line 157
    fill-array-data p2, :array_0

    invoke-static {p2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p2

    iput-object p2, p0, Lcom/mattprecious/telescope/TelescopeLayout;->doneAnimator:Landroid/animation/ValueAnimator;

    .line 158
    iget-object p2, p0, Lcom/mattprecious/telescope/TelescopeLayout;->doneAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {p2, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 159
    iget-object p2, p0, Lcom/mattprecious/telescope/TelescopeLayout;->doneAnimator:Landroid/animation/ValueAnimator;

    new-instance p3, Lcom/mattprecious/telescope/TelescopeLayout$3;

    invoke-direct {p3, p0}, Lcom/mattprecious/telescope/TelescopeLayout$3;-><init>(Lcom/mattprecious/telescope/TelescopeLayout;)V

    invoke-virtual {p2, p3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 166
    invoke-virtual {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->isInEditMode()Z

    move-result p2

    const/4 p3, 0x0

    if-eqz p2, :cond_0

    .line 167
    iput-object p3, p0, Lcom/mattprecious/telescope/TelescopeLayout;->projectionManager:Landroid/media/projection/MediaProjectionManager;

    .line 168
    iput-object p3, p0, Lcom/mattprecious/telescope/TelescopeLayout;->windowManager:Landroid/view/WindowManager;

    .line 169
    iput-object p3, p0, Lcom/mattprecious/telescope/TelescopeLayout;->vibrator:Landroid/os/Vibrator;

    .line 170
    iput-object p3, p0, Lcom/mattprecious/telescope/TelescopeLayout;->requestCaptureFilter:Landroid/content/IntentFilter;

    .line 171
    iput-object p3, p0, Lcom/mattprecious/telescope/TelescopeLayout;->requestCaptureReceiver:Landroid/content/BroadcastReceiver;

    return-void

    :cond_0
    const-string p2, "window"

    .line 175
    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/WindowManager;

    iput-object p2, p0, Lcom/mattprecious/telescope/TelescopeLayout;->windowManager:Landroid/view/WindowManager;

    const-string p2, "vibrator"

    .line 176
    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/os/Vibrator;

    iput-object p2, p0, Lcom/mattprecious/telescope/TelescopeLayout;->vibrator:Landroid/os/Vibrator;

    .line 178
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x15

    if-ge p2, v0, :cond_1

    .line 179
    iput-object p3, p0, Lcom/mattprecious/telescope/TelescopeLayout;->projectionManager:Landroid/media/projection/MediaProjectionManager;

    .line 180
    iput-object p3, p0, Lcom/mattprecious/telescope/TelescopeLayout;->requestCaptureFilter:Landroid/content/IntentFilter;

    .line 181
    iput-object p3, p0, Lcom/mattprecious/telescope/TelescopeLayout;->requestCaptureReceiver:Landroid/content/BroadcastReceiver;

    goto :goto_0

    :cond_1
    const-string p2, "media_projection"

    .line 184
    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/media/projection/MediaProjectionManager;

    iput-object p2, p0, Lcom/mattprecious/telescope/TelescopeLayout;->projectionManager:Landroid/media/projection/MediaProjectionManager;

    .line 186
    new-instance p2, Landroid/content/IntentFilter;

    .line 187
    invoke-static {p1}, Lcom/mattprecious/telescope/RequestCaptureActivity;->getResultBroadcastAction(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/mattprecious/telescope/TelescopeLayout;->requestCaptureFilter:Landroid/content/IntentFilter;

    .line 188
    new-instance p1, Lcom/mattprecious/telescope/TelescopeLayout$4;

    invoke-direct {p1, p0}, Lcom/mattprecious/telescope/TelescopeLayout$4;-><init>(Lcom/mattprecious/telescope/TelescopeLayout;)V

    iput-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->requestCaptureReceiver:Landroid/content/BroadcastReceiver;

    :goto_0
    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic access$000(Lcom/mattprecious/telescope/TelescopeLayout;)V
    .locals 0

    .line 59
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->trigger()V

    return-void
.end method

.method static synthetic access$1000(Lcom/mattprecious/telescope/TelescopeLayout;)Lcom/mattprecious/telescope/Lens;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->lens:Lcom/mattprecious/telescope/Lens;

    return-object p0
.end method

.method static synthetic access$102(Lcom/mattprecious/telescope/TelescopeLayout;F)F
    .locals 0

    .line 59
    iput p1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressFraction:F

    return p1
.end method

.method static synthetic access$1102(Lcom/mattprecious/telescope/TelescopeLayout;Z)Z
    .locals 0

    .line 59
    iput-boolean p1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->saving:Z

    return p1
.end method

.method static synthetic access$1200(Landroid/content/Context;)Ljava/io/File;
    .locals 0

    .line 59
    invoke-static {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->getScreenshotFolder(Landroid/content/Context;)Ljava/io/File;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$1300()Ljava/text/SimpleDateFormat;
    .locals 1

    .line 59
    sget-object v0, Lcom/mattprecious/telescope/TelescopeLayout;->SCREENSHOT_FILE_FORMAT:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mattprecious/telescope/TelescopeLayout;)Landroid/view/WindowManager;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->windowManager:Landroid/view/WindowManager;

    return-object p0
.end method

.method static synthetic access$1500()Landroid/os/Handler;
    .locals 1

    .line 59
    invoke-static {}, Lcom/mattprecious/telescope/TelescopeLayout;->getBackgroundHandler()Landroid/os/Handler;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$202(Lcom/mattprecious/telescope/TelescopeLayout;F)F
    .locals 0

    .line 59
    iput p1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->doneFraction:F

    return p1
.end method

.method static synthetic access$300(Lcom/mattprecious/telescope/TelescopeLayout;)V
    .locals 0

    .line 59
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->unregisterRequestCaptureReceiver()V

    return-void
.end method

.method static synthetic access$400(Lcom/mattprecious/telescope/TelescopeLayout;)Landroid/media/projection/MediaProjectionManager;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->projectionManager:Landroid/media/projection/MediaProjectionManager;

    return-object p0
.end method

.method static synthetic access$500(Lcom/mattprecious/telescope/TelescopeLayout;)V
    .locals 0

    .line 59
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->captureCanvasScreenshot()V

    return-void
.end method

.method static synthetic access$600(Lcom/mattprecious/telescope/TelescopeLayout;Landroid/media/projection/MediaProjection;)V
    .locals 0

    .line 59
    invoke-direct {p0, p1}, Lcom/mattprecious/telescope/TelescopeLayout;->captureNativeScreenshot(Landroid/media/projection/MediaProjection;)V

    return-void
.end method

.method static synthetic access$700(Lcom/mattprecious/telescope/TelescopeLayout;)Landroid/view/View;
    .locals 0

    .line 59
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->getTargetView()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$800(Lcom/mattprecious/telescope/TelescopeLayout;)V
    .locals 0

    .line 59
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->capturingEnd()V

    return-void
.end method

.method static synthetic access$900(Lcom/mattprecious/telescope/TelescopeLayout;)V
    .locals 0

    .line 59
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->checkLens()V

    return-void
.end method

.method private cancel()V
    .locals 4

    .line 402
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->stop()V

    .line 403
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 404
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressCancelAnimator:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    iget v2, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressFraction:F

    const/4 v3, 0x0

    aput v2, v1, v3

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 405
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressCancelAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 406
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->trigger:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method private captureCanvasScreenshot()V
    .locals 1

    .line 464
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->capturingStart()V

    .line 467
    new-instance v0, Lcom/mattprecious/telescope/TelescopeLayout$5;

    invoke-direct {v0, p0}, Lcom/mattprecious/telescope/TelescopeLayout$5;-><init>(Lcom/mattprecious/telescope/TelescopeLayout;)V

    invoke-virtual {p0, v0}, Lcom/mattprecious/telescope/TelescopeLayout;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private captureNativeScreenshot(Landroid/media/projection/MediaProjection;)V
    .locals 1

    .line 606
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->capturingStart()V

    .line 609
    new-instance v0, Lcom/mattprecious/telescope/TelescopeLayout$6;

    invoke-direct {v0, p0, p1}, Lcom/mattprecious/telescope/TelescopeLayout$6;-><init>(Lcom/mattprecious/telescope/TelescopeLayout;Landroid/media/projection/MediaProjection;)V

    invoke-virtual {p0, v0}, Lcom/mattprecious/telescope/TelescopeLayout;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private capturingEnd()V
    .locals 1

    const/4 v0, 0x0

    .line 495
    iput-boolean v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->capturing:Z

    .line 496
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->doneAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method private capturingStart()V
    .locals 1

    .line 487
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    const/4 v0, 0x0

    .line 488
    iput v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressFraction:F

    const/4 v0, 0x1

    .line 490
    iput-boolean v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->capturing:Z

    .line 491
    invoke-virtual {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->invalidate()V

    return-void
.end method

.method private checkLens()V
    .locals 2

    .line 458
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->lens:Lcom/mattprecious/telescope/Lens;

    if-eqz v0, :cond_0

    return-void

    .line 459
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call setLens() before capturing a screenshot."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static cleanUp(Landroid/content/Context;)V
    .locals 1

    .line 224
    invoke-static {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->getScreenshotFolder(Landroid/content/Context;)Ljava/io/File;

    move-result-object p0

    .line 225
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 229
    :cond_0
    invoke-static {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->delete(Ljava/io/File;)V

    return-void
.end method

.method private static delete(Ljava/io/File;)V
    .locals 4

    .line 516
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 517
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 519
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 520
    invoke-static {v3}, Lcom/mattprecious/telescope/TelescopeLayout;->delete(Ljava/io/File;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 525
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    return-void
.end method

.method private static getBackgroundHandler()Landroid/os/Handler;
    .locals 3

    .line 595
    sget-object v0, Lcom/mattprecious/telescope/TelescopeLayout;->backgroundHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 596
    new-instance v0, Landroid/os/HandlerThread;

    const/16 v1, 0xa

    const-string v2, "telescope"

    invoke-direct {v0, v2, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 598
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 599
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/mattprecious/telescope/TelescopeLayout;->backgroundHandler:Landroid/os/Handler;

    .line 602
    :cond_0
    sget-object v0, Lcom/mattprecious/telescope/TelescopeLayout;->backgroundHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private static getScreenshotFolder(Landroid/content/Context;)Ljava/io/File;
    .locals 2

    .line 529
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object p0

    const-string v1, "telescope"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private getTargetView()Landroid/view/View;
    .locals 2

    .line 504
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->screenshotTarget:Landroid/view/View;

    .line 505
    iget-boolean v1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->screenshotChildrenOnly:Z

    if-nez v1, :cond_0

    .line 506
    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    if-eq v1, v0, :cond_0

    .line 507
    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private static hasVibratePermission(Landroid/content/Context;)Z
    .locals 3

    .line 533
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    const-string v2, "android.permission.VIBRATE"

    invoke-virtual {p0, v2, v0, v1}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private registerRequestCaptureReceiver()V
    .locals 3

    .line 587
    invoke-virtual {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->requestCaptureReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/mattprecious/telescope/TelescopeLayout;->requestCaptureFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private start()V
    .locals 5

    const/4 v0, 0x1

    .line 391
    iput-boolean v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->pressing:Z

    .line 392
    iget-object v1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressAnimator:Landroid/animation/ValueAnimator;

    const/4 v2, 0x2

    new-array v2, v2, [F

    iget v3, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressFraction:F

    const/4 v4, 0x0

    aput v3, v2, v4

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v2, v0

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 393
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 394
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->trigger:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private stop()V
    .locals 1

    const/4 v0, 0x0

    .line 398
    iput-boolean v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->pressing:Z

    return-void
.end method

.method private trigger()V
    .locals 4

    .line 410
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->stop()V

    .line 412
    iget-boolean v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->vibrate:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mattprecious/telescope/TelescopeLayout;->hasVibratePermission(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->vibrator:Landroid/os/Vibrator;

    const-wide/16 v1, 0x32

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    .line 416
    :cond_0
    sget-object v0, Lcom/mattprecious/telescope/TelescopeLayout$7;->$SwitchMap$com$mattprecious$telescope$ScreenshotMode:[I

    iget-object v1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->screenshotMode:Lcom/mattprecious/telescope/ScreenshotMode;

    invoke-virtual {v1}, Lcom/mattprecious/telescope/ScreenshotMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 433
    new-instance v0, Lcom/mattprecious/telescope/TelescopeLayout$SaveScreenshotTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mattprecious/telescope/TelescopeLayout$SaveScreenshotTask;-><init>(Lcom/mattprecious/telescope/TelescopeLayout;Landroid/graphics/Bitmap;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/mattprecious/telescope/TelescopeLayout$SaveScreenshotTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 436
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown screenshot mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/mattprecious/telescope/TelescopeLayout;->screenshotMode:Lcom/mattprecious/telescope/ScreenshotMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 418
    :cond_2
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->projectionManager:Landroid/media/projection/MediaProjectionManager;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->screenshotChildrenOnly:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->screenshotTarget:Landroid/view/View;

    if-ne v0, p0, :cond_3

    .line 421
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->windowHasSecureFlag()Z

    move-result v0

    if-nez v0, :cond_3

    .line 423
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->registerRequestCaptureReceiver()V

    .line 424
    invoke-virtual {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/mattprecious/telescope/RequestCaptureActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 430
    :cond_3
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->captureCanvasScreenshot()V

    :goto_0
    return-void
.end method

.method private unregisterRequestCaptureReceiver()V
    .locals 2

    .line 591
    invoke-virtual {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->requestCaptureReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method private windowHasSecureFlag()Z
    .locals 3

    .line 442
    invoke-virtual {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 443
    :goto_0
    instance-of v1, v0, Landroid/app/Activity;

    if-nez v1, :cond_0

    instance-of v2, v0, Landroid/content/ContextWrapper;

    if-eqz v2, :cond_0

    .line 444
    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    if-eqz v1, :cond_2

    .line 449
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :cond_2
    :goto_1
    return v2
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 18

    move-object/from16 v0, p0

    .line 351
    invoke-super/range {p0 .. p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 354
    iget-boolean v1, v0, Lcom/mattprecious/telescope/TelescopeLayout;->capturing:Z

    if-eqz v1, :cond_0

    return-void

    .line 358
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/mattprecious/telescope/TelescopeLayout;->getMeasuredWidth()I

    move-result v1

    .line 359
    invoke-virtual/range {p0 .. p0}, Lcom/mattprecious/telescope/TelescopeLayout;->getMeasuredHeight()I

    move-result v2

    .line 361
    iget v3, v0, Lcom/mattprecious/telescope/TelescopeLayout;->progressFraction:F

    const/4 v4, 0x0

    cmpl-float v4, v3, v4

    if-lez v4, :cond_1

    const/4 v6, 0x0

    .line 363
    iget v9, v0, Lcom/mattprecious/telescope/TelescopeLayout;->halfStrokeWidth:F

    int-to-float v11, v1

    mul-float v8, v11, v3

    iget-object v10, v0, Lcom/mattprecious/telescope/TelescopeLayout;->progressPaint:Landroid/graphics/Paint;

    move-object/from16 v5, p1

    move v7, v9

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 365
    iget v3, v0, Lcom/mattprecious/telescope/TelescopeLayout;->halfStrokeWidth:F

    sub-float v5, v11, v3

    sub-float v7, v11, v3

    int-to-float v3, v2

    iget v4, v0, Lcom/mattprecious/telescope/TelescopeLayout;->progressFraction:F

    mul-float v8, v3, v4

    iget-object v9, v0, Lcom/mattprecious/telescope/TelescopeLayout;->progressPaint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 368
    iget v4, v0, Lcom/mattprecious/telescope/TelescopeLayout;->halfStrokeWidth:F

    sub-float v12, v3, v4

    iget v5, v0, Lcom/mattprecious/telescope/TelescopeLayout;->progressFraction:F

    mul-float v5, v5, v11

    sub-float v13, v11, v5

    sub-float v14, v3, v4

    iget-object v15, v0, Lcom/mattprecious/telescope/TelescopeLayout;->progressPaint:Landroid/graphics/Paint;

    move-object/from16 v10, p1

    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 371
    iget v15, v0, Lcom/mattprecious/telescope/TelescopeLayout;->halfStrokeWidth:F

    iget v4, v0, Lcom/mattprecious/telescope/TelescopeLayout;->progressFraction:F

    mul-float v4, v4, v3

    sub-float v16, v3, v4

    iget-object v4, v0, Lcom/mattprecious/telescope/TelescopeLayout;->progressPaint:Landroid/graphics/Paint;

    move-object/from16 v12, p1

    move v13, v15

    move v14, v3

    move-object/from16 v17, v4

    invoke-virtual/range {v12 .. v17}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 375
    :cond_1
    iget v3, v0, Lcom/mattprecious/telescope/TelescopeLayout;->doneFraction:F

    const/high16 v4, 0x3f800000    # 1.0f

    cmpg-float v4, v3, v4

    if-gez v4, :cond_2

    int-to-float v1, v1

    mul-float v6, v1, v3

    .line 377
    iget v9, v0, Lcom/mattprecious/telescope/TelescopeLayout;->halfStrokeWidth:F

    iget-object v10, v0, Lcom/mattprecious/telescope/TelescopeLayout;->progressPaint:Landroid/graphics/Paint;

    move-object/from16 v5, p1

    move v7, v9

    move v8, v1

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 379
    iget v3, v0, Lcom/mattprecious/telescope/TelescopeLayout;->halfStrokeWidth:F

    sub-float v5, v1, v3

    int-to-float v2, v2

    iget v4, v0, Lcom/mattprecious/telescope/TelescopeLayout;->doneFraction:F

    mul-float v6, v2, v4

    sub-float v7, v1, v3

    iget-object v9, v0, Lcom/mattprecious/telescope/TelescopeLayout;->progressPaint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    move v8, v2

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 382
    iget v3, v0, Lcom/mattprecious/telescope/TelescopeLayout;->doneFraction:F

    mul-float v3, v3, v1

    sub-float v5, v1, v3

    iget v1, v0, Lcom/mattprecious/telescope/TelescopeLayout;->halfStrokeWidth:F

    sub-float v6, v2, v1

    const/4 v7, 0x0

    sub-float v8, v2, v1

    iget-object v9, v0, Lcom/mattprecious/telescope/TelescopeLayout;->progressPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 385
    iget v13, v0, Lcom/mattprecious/telescope/TelescopeLayout;->halfStrokeWidth:F

    iget v1, v0, Lcom/mattprecious/telescope/TelescopeLayout;->doneFraction:F

    mul-float v1, v1, v2

    sub-float v12, v2, v1

    const/4 v14, 0x0

    iget-object v15, v0, Lcom/mattprecious/telescope/TelescopeLayout;->progressPaint:Landroid/graphics/Paint;

    move-object/from16 v10, p1

    move v11, v13

    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_2
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 282
    invoke-virtual {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 287
    :cond_0
    iget-boolean v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->capturing:Z

    const/4 v1, 0x1

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->saving:Z

    if-eqz v0, :cond_1

    goto :goto_0

    .line 291
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_2

    .line 292
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    iget v2, p0, Lcom/mattprecious/telescope/TelescopeLayout;->pointerCount:I

    if-ne v0, v2, :cond_2

    .line 294
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->start()V

    return v1

    .line 300
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1

    :cond_3
    :goto_0
    return v1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 304
    invoke-virtual {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 309
    :cond_0
    iget-boolean v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->capturing:Z

    const/4 v2, 0x1

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->saving:Z

    if-eqz v0, :cond_1

    goto :goto_1

    .line 313
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-eqz v0, :cond_9

    if-eq v0, v2, :cond_7

    const/4 v3, 0x2

    if-eq v0, v3, :cond_5

    const/4 v3, 0x3

    if-eq v0, v3, :cond_7

    const/4 v3, 0x5

    if-eq v0, v3, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_7

    goto :goto_0

    .line 328
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    iget v1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->pointerCount:I

    if-ne v0, v1, :cond_4

    .line 331
    iget-boolean p1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->pressing:Z

    if-nez p1, :cond_3

    .line 332
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->start()V

    :cond_3
    return v2

    .line 336
    :cond_4
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->cancel()V

    goto :goto_0

    .line 340
    :cond_5
    iget-boolean v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->pressing:Z

    if-eqz v0, :cond_6

    .line 341
    invoke-virtual {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->invalidate()V

    return v2

    .line 347
    :cond_6
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1

    .line 317
    :cond_7
    iget-boolean p1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->pressing:Z

    if-eqz p1, :cond_8

    .line 318
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->cancel()V

    :cond_8
    return v1

    .line 323
    :cond_9
    iget-boolean v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->pressing:Z

    if-nez v0, :cond_a

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result p1

    iget v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->pointerCount:I

    if-ne p1, v0, :cond_a

    .line 324
    invoke-direct {p0}, Lcom/mattprecious/telescope/TelescopeLayout;->start()V

    :cond_a
    :goto_1
    return v2
.end method

.method public setLens(Lcom/mattprecious/telescope/Lens;)V
    .locals 1

    const-string v0, "lens == null"

    .line 234
    invoke-static {p1, v0}, Lcom/mattprecious/telescope/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 235
    iput-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->lens:Lcom/mattprecious/telescope/Lens;

    return-void
.end method

.method public setPointerCount(I)V
    .locals 1

    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    .line 244
    iput p1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->pointerCount:I

    return-void

    .line 241
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "pointerCount < 1"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setProgressColor(I)V
    .locals 1

    .line 249
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout;->progressPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public setScreenshotChildrenOnly(Z)V
    .locals 0

    .line 263
    iput-boolean p1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->screenshotChildrenOnly:Z

    return-void
.end method

.method public setScreenshotMode(Lcom/mattprecious/telescope/ScreenshotMode;)V
    .locals 1

    const-string v0, "screenshotMode == null"

    .line 254
    invoke-static {p1, v0}, Lcom/mattprecious/telescope/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 255
    iput-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->screenshotMode:Lcom/mattprecious/telescope/ScreenshotMode;

    return-void
.end method

.method public setScreenshotTarget(Landroid/view/View;)V
    .locals 1

    const-string v0, "screenshotTarget == null"

    .line 268
    invoke-static {p1, v0}, Lcom/mattprecious/telescope/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 269
    iput-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->screenshotTarget:Landroid/view/View;

    return-void
.end method

.method public setVibrate(Z)V
    .locals 0

    .line 278
    iput-boolean p1, p0, Lcom/mattprecious/telescope/TelescopeLayout;->vibrate:Z

    return-void
.end method
