.class Lcom/squareup/comms/net/Channel$2;
.super Ljava/lang/Object;
.source "Channel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/comms/net/Channel;->close()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/comms/net/Channel;


# direct methods
.method constructor <init>(Lcom/squareup/comms/net/Channel;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/squareup/comms/net/Channel$2;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 62
    iget-object v2, p0, Lcom/squareup/comms/net/Channel$2;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v2}, Lcom/squareup/comms/net/Channel;->access$400(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/Connection;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "Closing channel: %s"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    iget-object v1, p0, Lcom/squareup/comms/net/Channel$2;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v1, v0}, Lcom/squareup/comms/net/Channel;->access$502(Lcom/squareup/comms/net/Channel;Z)Z

    .line 64
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$2;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$300(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/ConnectionFactory;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/comms/net/ConnectionFactory;->close()V

    .line 65
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$2;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$400(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/Connection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$2;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$400(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/Connection;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/comms/net/Connection;->close()V

    .line 67
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$2;->this$0:Lcom/squareup/comms/net/Channel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/comms/net/Channel;->access$402(Lcom/squareup/comms/net/Channel;Lcom/squareup/comms/net/Connection;)Lcom/squareup/comms/net/Connection;

    .line 68
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$2;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$000(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/ChannelCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/comms/net/ChannelCallback;->onDisconnected()V

    :cond_0
    return-void
.end method
