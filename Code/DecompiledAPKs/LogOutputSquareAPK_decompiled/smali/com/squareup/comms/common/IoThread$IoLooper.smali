.class Lcom/squareup/comms/common/IoThread$IoLooper;
.super Ljava/lang/Object;
.source "IoThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/common/IoThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IoLooper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/comms/common/IoThread;


# direct methods
.method private constructor <init>(Lcom/squareup/comms/common/IoThread;)V
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/comms/common/IoThread$IoLooper;->this$0:Lcom/squareup/comms/common/IoThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/common/IoThread$1;)V
    .locals 0

    .line 197
    invoke-direct {p0, p1}, Lcom/squareup/comms/common/IoThread$IoLooper;-><init>(Lcom/squareup/comms/common/IoThread;)V

    return-void
.end method

.method private handleIoCompletions()V
    .locals 8

    .line 213
    iget-object v0, p0, Lcom/squareup/comms/common/IoThread$IoLooper;->this$0:Lcom/squareup/comms/common/IoThread;

    invoke-static {v0}, Lcom/squareup/comms/common/IoThread;->access$100(Lcom/squareup/comms/common/IoThread;)Ljava/nio/channels/Selector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/nio/channels/SelectionKey;

    .line 215
    invoke-virtual {v1}, Ljava/nio/channels/SelectionKey;->attachment()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/comms/common/IoOperation;

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 219
    :try_start_0
    invoke-virtual {v2}, Lcom/squareup/comms/common/IoOperation;->ioCompletion()Lcom/squareup/comms/common/IoCompletion;

    move-result-object v5

    invoke-interface {v5, v1}, Lcom/squareup/comms/common/IoCompletion;->onReady(Ljava/nio/channels/SelectionKey;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/nio/channels/CancelledKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    goto :goto_3

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v5

    .line 221
    :try_start_1
    invoke-virtual {v1}, Ljava/nio/channels/SelectionKey;->cancel()V

    const-string v1, "Error %s. Message: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v3

    .line 222
    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {v1, v6}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 223
    invoke-virtual {v2}, Lcom/squareup/comms/common/IoOperation;->ioCompletion()Lcom/squareup/comms/common/IoCompletion;

    move-result-object v1

    invoke-interface {v1, v5}, Lcom/squareup/comms/common/IoCompletion;->onError(Ljava/io/IOException;)V
    :try_end_1
    .catch Ljava/nio/channels/CancelledKeyException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 228
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :goto_2
    :try_start_2
    const-string v5, "Key has been canceled: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v3

    .line 226
    invoke-static {v1, v5, v4}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 228
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 229
    throw v1

    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 199
    :goto_0
    iget-object v0, p0, Lcom/squareup/comms/common/IoThread$IoLooper;->this$0:Lcom/squareup/comms/common/IoThread;

    invoke-static {v0}, Lcom/squareup/comms/common/IoThread;->access$200(Lcom/squareup/comms/common/IoThread;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 201
    :try_start_0
    iget-object v1, p0, Lcom/squareup/comms/common/IoThread$IoLooper;->this$0:Lcom/squareup/comms/common/IoThread;

    invoke-static {v1}, Lcom/squareup/comms/common/IoThread;->access$100(Lcom/squareup/comms/common/IoThread;)Ljava/nio/channels/Selector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/channels/Selector;->select()I

    .line 202
    invoke-direct {p0}, Lcom/squareup/comms/common/IoThread$IoLooper;->handleIoCompletions()V

    .line 203
    iget-object v1, p0, Lcom/squareup/comms/common/IoThread$IoLooper;->this$0:Lcom/squareup/comms/common/IoThread;

    invoke-static {v1}, Lcom/squareup/comms/common/IoThread;->access$300(Lcom/squareup/comms/common/IoThread;)Lcom/squareup/comms/common/RunnableQueue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/comms/common/RunnableQueue;->drain()V
    :try_end_0
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "IoLooper select operation failed"

    .line 207
    invoke-static {v1, v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "IoLooper has been closed"

    .line 205
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method
