.class public final Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "OnLoyaltyPhoneNumberEntered.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered;",
        "Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\n\u001a\u00020\u0002H\u0016J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u0006\u001a\u00020\u00002\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0005J\u000e\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\u0008R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\t\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered;",
        "()V",
        "phone_id",
        "",
        "phone_number",
        "phone_source",
        "",
        "Ljava/lang/Integer;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public phone_id:Ljava/lang/String;

.field public phone_number:Ljava/lang/String;

.field public phone_source:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered;
    .locals 5

    .line 127
    new-instance v0, Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered;

    .line 128
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered$Builder;->phone_number:Ljava/lang/String;

    .line 129
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered$Builder;->phone_id:Ljava/lang/String;

    .line 130
    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered$Builder;->phone_source:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 131
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    .line 127
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered;-><init>(Ljava/lang/String;Ljava/lang/String;ILokio/ByteString;)V

    return-object v0

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v3, v0, v1

    const/4 v1, 0x1

    const-string v2, "phone_source"

    aput-object v2, v0, v1

    .line 130
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered$Builder;->build()Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final phone_id(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered$Builder;->phone_id:Ljava/lang/String;

    return-object p0
.end method

.method public final phone_number(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered$Builder;
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered$Builder;->phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public final phone_source(I)Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered$Builder;
    .locals 0

    .line 123
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered$Builder;->phone_source:Ljava/lang/Integer;

    return-object p0
.end method
