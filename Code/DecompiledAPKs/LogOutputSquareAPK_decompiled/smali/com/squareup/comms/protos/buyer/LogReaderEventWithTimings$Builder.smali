.class public final Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LogReaderEventWithTimings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;",
        "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u000b\u001a\u00020\u0002H\u0016J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u000e\u0010\u0006\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\u0008\u001a\u00020\u00002\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tR\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;",
        "()V",
        "card_reader_info",
        "Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;",
        "event_name",
        "Lcom/squareup/comms/protos/buyer/BuyerEventName;",
        "payment_timings",
        "",
        "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$BuyerPaymentTiming;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

.field public event_name:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public payment_timings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$BuyerPaymentTiming;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 105
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;->payment_timings:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;
    .locals 6

    .line 123
    new-instance v0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;

    .line 124
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;->event_name:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-eqz v1, :cond_1

    .line 125
    iget-object v5, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    if-eqz v5, :cond_0

    .line 127
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;->payment_timings:Ljava/util/List;

    .line 128
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    .line 123
    invoke-direct {v0, v1, v5, v2, v3}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;-><init>(Lcom/squareup/comms/protos/buyer/BuyerEventName;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Ljava/util/List;Lokio/ByteString;)V

    return-object v0

    :cond_0
    new-array v0, v4, [Ljava/lang/Object;

    aput-object v5, v0, v3

    const-string v1, "card_reader_info"

    aput-object v1, v0, v2

    .line 125
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v0, v4, [Ljava/lang/Object;

    aput-object v1, v0, v3

    const-string v1, "event_name"

    aput-object v1, v0, v2

    .line 124
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;->build()Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final card_reader_info(Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;)Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;
    .locals 1

    const-string v0, "card_reader_info"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    return-object p0
.end method

.method public final event_name(Lcom/squareup/comms/protos/buyer/BuyerEventName;)Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;
    .locals 1

    const-string v0, "event_name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;->event_name:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    return-object p0
.end method

.method public final payment_timings(Ljava/util/List;)Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$BuyerPaymentTiming;",
            ">;)",
            "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;"
        }
    .end annotation

    const-string v0, "payment_timings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 119
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Builder;->payment_timings:Ljava/util/List;

    return-object p0
.end method
