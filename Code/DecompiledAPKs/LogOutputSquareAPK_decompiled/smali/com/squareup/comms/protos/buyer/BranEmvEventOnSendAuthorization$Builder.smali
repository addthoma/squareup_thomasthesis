.class public final Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BranEmvEventOnSendAuthorization.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization;",
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u000b\u001a\u00020\u0002H\u0016J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u000e\u0010\u0006\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007J\u000e\u0010\t\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\nR\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0008R\u0014\u0010\t\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization;",
        "()V",
        "card_info",
        "Lcom/squareup/comms/protos/buyer/CardInfo;",
        "card_presence_required",
        "",
        "Ljava/lang/Boolean;",
        "encrypted_card_data",
        "Lokio/ByteString;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public card_info:Lcom/squareup/comms/protos/buyer/CardInfo;

.field public card_presence_required:Ljava/lang/Boolean;

.field public encrypted_card_data:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 93
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization;
    .locals 7

    .line 118
    new-instance v0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization;

    .line 119
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization$Builder;->encrypted_card_data:Lokio/ByteString;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-eqz v1, :cond_2

    .line 121
    iget-object v5, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization$Builder;->card_presence_required:Ljava/lang/Boolean;

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 123
    iget-object v6, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization$Builder;->card_info:Lcom/squareup/comms/protos/buyer/CardInfo;

    if-eqz v6, :cond_0

    .line 124
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    .line 118
    invoke-direct {v0, v1, v5, v6, v2}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization;-><init>(Lokio/ByteString;ZLcom/squareup/comms/protos/buyer/CardInfo;Lokio/ByteString;)V

    return-object v0

    :cond_0
    new-array v0, v4, [Ljava/lang/Object;

    aput-object v6, v0, v3

    const-string v1, "card_info"

    aput-object v1, v0, v2

    .line 123
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v0, v4, [Ljava/lang/Object;

    aput-object v5, v0, v3

    const-string v1, "card_presence_required"

    aput-object v1, v0, v2

    .line 122
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_2
    new-array v0, v4, [Ljava/lang/Object;

    aput-object v1, v0, v3

    const-string v1, "encrypted_card_data"

    aput-object v1, v0, v2

    .line 119
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization$Builder;->build()Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final card_info(Lcom/squareup/comms/protos/buyer/CardInfo;)Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization$Builder;
    .locals 1

    const-string v0, "card_info"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization$Builder;->card_info:Lcom/squareup/comms/protos/buyer/CardInfo;

    return-object p0
.end method

.method public final card_presence_required(Z)Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization$Builder;
    .locals 0

    .line 109
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization$Builder;->card_presence_required:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final encrypted_card_data(Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization$Builder;
    .locals 1

    const-string v0, "encrypted_card_data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization$Builder;->encrypted_card_data:Lokio/ByteString;

    return-object p0
.end method
