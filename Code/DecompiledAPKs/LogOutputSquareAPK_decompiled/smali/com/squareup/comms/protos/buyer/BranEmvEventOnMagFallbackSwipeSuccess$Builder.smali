.class public final Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BranEmvEventOnMagFallbackSwipeSuccess.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;",
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u000c\u001a\u00020\u0002H\u0016J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u000e\u0010\u0006\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007J\u000e\u0010\t\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0007J\u000e\u0010\n\u001a\u00020\u00002\u0006\u0010\n\u001a\u00020\u000bR\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0008R\u0016\u0010\t\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0008R\u0014\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;",
        "()V",
        "fallback_type",
        "Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;",
        "has_track_1_data",
        "",
        "Ljava/lang/Boolean;",
        "has_track_2_data",
        "swipe",
        "Lcom/squareup/comms/protos/buyer/MagSwipe;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public fallback_type:Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;

.field public has_track_1_data:Ljava/lang/Boolean;

.field public has_track_2_data:Ljava/lang/Boolean;

.field public swipe:Lcom/squareup/comms/protos/buyer/MagSwipe;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 106
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;
    .locals 9

    .line 140
    new-instance v6, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;

    .line 141
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;->fallback_type:Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;

    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz v1, :cond_3

    .line 142
    iget-object v4, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;->swipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    if-eqz v4, :cond_2

    .line 143
    iget-object v5, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;->has_track_1_data:Ljava/lang/Boolean;

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 145
    iget-object v7, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;->has_track_2_data:Ljava/lang/Boolean;

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 147
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v6

    move-object v2, v4

    move v3, v5

    move v4, v7

    move-object v5, v8

    .line 140
    invoke-direct/range {v0 .. v5}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;-><init>(Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;Lcom/squareup/comms/protos/buyer/MagSwipe;ZZLokio/ByteString;)V

    return-object v6

    :cond_0
    new-array v1, v3, [Ljava/lang/Object;

    aput-object v7, v1, v2

    const-string v2, "has_track_2_data"

    aput-object v2, v1, v0

    .line 145
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v1, v3, [Ljava/lang/Object;

    aput-object v5, v1, v2

    const-string v2, "has_track_1_data"

    aput-object v2, v1, v0

    .line 143
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_2
    new-array v1, v3, [Ljava/lang/Object;

    aput-object v4, v1, v2

    const-string v2, "swipe"

    aput-object v2, v1, v0

    .line 142
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_3
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v2

    const-string v1, "fallback_type"

    aput-object v1, v3, v0

    .line 141
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;->build()Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final fallback_type(Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;)Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;
    .locals 1

    const-string v0, "fallback_type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;->fallback_type:Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;

    return-object p0
.end method

.method public final has_track_1_data(Z)Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;
    .locals 0

    .line 130
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;->has_track_1_data:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final has_track_2_data(Z)Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;
    .locals 0

    .line 135
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;->has_track_2_data:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final swipe(Lcom/squareup/comms/protos/buyer/MagSwipe;)Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;
    .locals 1

    const-string v0, "swipe"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess$Builder;->swipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    return-object p0
.end method
