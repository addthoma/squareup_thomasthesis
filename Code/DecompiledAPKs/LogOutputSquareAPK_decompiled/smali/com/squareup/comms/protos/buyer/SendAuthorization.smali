.class public final Lcom/squareup/comms/protos/buyer/SendAuthorization;
.super Lcom/squareup/wire/AndroidMessage;
.source "SendAuthorization.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;,
        Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;,
        Lcom/squareup/comms/protos/buyer/SendAuthorization$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/buyer/SendAuthorization;",
        "Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSendAuthorization.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SendAuthorization.kt\ncom/squareup/comms/protos/buyer/SendAuthorization\n*L\n1#1,370:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0018\u0000 \u00192\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u0017\u0018\u0019B?\u0012\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u0012\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u000cJ@\u0010\r\u001a\u00020\u00002\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00082\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u0004J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0096\u0002J\u0008\u0010\u0012\u001a\u00020\u0013H\u0016J\u0008\u0010\u0014\u001a\u00020\u0002H\u0016J\u0008\u0010\u0015\u001a\u00020\u0016H\u0016R\u0012\u0010\t\u001a\u0004\u0018\u00010\n8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/SendAuthorization;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;",
        "encrypted_reader_data",
        "Lokio/ByteString;",
        "method",
        "Lcom/squareup/comms/protos/buyer/EntryMethod;",
        "card_reader_info",
        "Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;",
        "buyer_card_info",
        "Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;",
        "unknownFields",
        "(Lokio/ByteString;Lcom/squareup/comms/protos/buyer/EntryMethod;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;Lokio/ByteString;)V",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "newBuilder",
        "toString",
        "",
        "Builder",
        "BuyerCardInfo",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/buyer/SendAuthorization;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/buyer/SendAuthorization;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/buyer/SendAuthorization$Companion;


# instance fields
.field public final buyer_card_info:Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.buyer.SendAuthorization$BuyerCardInfo#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.buyer.BuyerCardReaderInfo#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final encrypted_reader_data:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x1
    .end annotation
.end field

.field public final method:Lcom/squareup/comms/protos/buyer/EntryMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.buyer.EntryMethod#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/buyer/SendAuthorization$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/SendAuthorization$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->Companion:Lcom/squareup/comms/protos/buyer/SendAuthorization$Companion;

    .line 161
    new-instance v0, Lcom/squareup/comms/protos/buyer/SendAuthorization$Companion$ADAPTER$1;

    .line 162
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 163
    const-class v2, Lcom/squareup/comms/protos/buyer/SendAuthorization;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/buyer/SendAuthorization$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 211
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/buyer/SendAuthorization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1f

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/squareup/comms/protos/buyer/SendAuthorization;-><init>(Lokio/ByteString;Lcom/squareup/comms/protos/buyer/EntryMethod;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lcom/squareup/comms/protos/buyer/EntryMethod;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;Lokio/ByteString;)V
    .locals 1

    const-string v0, "unknownFields"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    sget-object v0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->encrypted_reader_data:Lokio/ByteString;

    iput-object p2, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->method:Lcom/squareup/comms/protos/buyer/EntryMethod;

    iput-object p3, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    iput-object p4, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->buyer_card_info:Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    return-void
.end method

.method public synthetic constructor <init>(Lokio/ByteString;Lcom/squareup/comms/protos/buyer/EntryMethod;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 3

    and-int/lit8 p7, p6, 0x1

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    .line 31
    move-object p1, v0

    check-cast p1, Lokio/ByteString;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    .line 41
    move-object p2, v0

    check-cast p2, Lcom/squareup/comms/protos/buyer/EntryMethod;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    .line 50
    move-object p3, v0

    check-cast p3, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    :cond_2
    move-object v1, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    .line 56
    move-object p4, v0

    check-cast p4, Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    :cond_3
    move-object v0, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    .line 57
    sget-object p5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v1

    move-object p6, v0

    move-object p7, v2

    invoke-direct/range {p2 .. p7}, Lcom/squareup/comms/protos/buyer/SendAuthorization;-><init>(Lokio/ByteString;Lcom/squareup/comms/protos/buyer/EntryMethod;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;Lokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/buyer/SendAuthorization;Lokio/ByteString;Lcom/squareup/comms/protos/buyer/EntryMethod;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;Lokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/buyer/SendAuthorization;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    .line 102
    iget-object p1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->encrypted_reader_data:Lokio/ByteString;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    .line 103
    iget-object p2, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->method:Lcom/squareup/comms/protos/buyer/EntryMethod;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    .line 104
    iget-object p3, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    .line 105
    iget-object p4, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->buyer_card_info:Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    .line 106
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/SendAuthorization;->unknownFields()Lokio/ByteString;

    move-result-object p5

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/comms/protos/buyer/SendAuthorization;->copy(Lokio/ByteString;Lcom/squareup/comms/protos/buyer/EntryMethod;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/SendAuthorization;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(Lokio/ByteString;Lcom/squareup/comms/protos/buyer/EntryMethod;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/SendAuthorization;
    .locals 7

    const-string v0, "unknownFields"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    new-instance v0, Lcom/squareup/comms/protos/buyer/SendAuthorization;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/comms/protos/buyer/SendAuthorization;-><init>(Lokio/ByteString;Lcom/squareup/comms/protos/buyer/EntryMethod;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;Lokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 70
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/buyer/SendAuthorization;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 71
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/buyer/SendAuthorization;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 76
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/SendAuthorization;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/buyer/SendAuthorization;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/buyer/SendAuthorization;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->encrypted_reader_data:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/comms/protos/buyer/SendAuthorization;->encrypted_reader_data:Lokio/ByteString;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->method:Lcom/squareup/comms/protos/buyer/EntryMethod;

    iget-object v3, p1, Lcom/squareup/comms/protos/buyer/SendAuthorization;->method:Lcom/squareup/comms/protos/buyer/EntryMethod;

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    iget-object v3, p1, Lcom/squareup/comms/protos/buyer/SendAuthorization;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->buyer_card_info:Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    iget-object p1, p1, Lcom/squareup/comms/protos/buyer/SendAuthorization;->buyer_card_info:Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 80
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_4

    .line 82
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/SendAuthorization;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 83
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->encrypted_reader_data:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 84
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->method:Lcom/squareup/comms/protos/buyer/EntryMethod;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 86
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->buyer_card_info:Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 87
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;
    .locals 2

    .line 60
    new-instance v0, Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;-><init>()V

    .line 61
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->encrypted_reader_data:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;->encrypted_reader_data:Lokio/ByteString;

    .line 62
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->method:Lcom/squareup/comms/protos/buyer/EntryMethod;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;->method:Lcom/squareup/comms/protos/buyer/EntryMethod;

    .line 63
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    .line 64
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->buyer_card_info:Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;->buyer_card_info:Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    .line 65
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/SendAuthorization;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/SendAuthorization;->newBuilder()Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 94
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->encrypted_reader_data:Lokio/ByteString;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "encrypted_reader_data="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->encrypted_reader_data:Lokio/ByteString;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 95
    :cond_0
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->method:Lcom/squareup/comms/protos/buyer/EntryMethod;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "method="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->method:Lcom/squareup/comms/protos/buyer/EntryMethod;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 96
    :cond_1
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    if-eqz v1, :cond_2

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "card_reader_info="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 97
    :cond_2
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->buyer_card_info:Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    if-eqz v1, :cond_3

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "buyer_card_info="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization;->buyer_card_info:Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 98
    :cond_3
    move-object v3, v0

    check-cast v3, Ljava/lang/Iterable;

    const-string v0, "SendAuthorization{"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const-string v0, ", "

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string v0, "}"

    move-object v6, v0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
