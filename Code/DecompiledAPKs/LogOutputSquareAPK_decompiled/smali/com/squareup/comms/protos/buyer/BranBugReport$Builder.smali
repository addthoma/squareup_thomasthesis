.class public final Lcom/squareup/comms/protos/buyer/BranBugReport$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BranBugReport.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/BranBugReport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/BranBugReport;",
        "Lcom/squareup/comms/protos/buyer/BranBugReport$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0008\u0010\u000c\u001a\u00020\u0002H\u0016J\u000e\u0010\u0006\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007J\u000e\u0010\t\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\nJ\u0010\u0010\u000b\u001a\u00020\u00002\u0008\u0010\u000b\u001a\u0004\u0018\u00010\nR\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0008R\u0014\u0010\t\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/BranBugReport$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/BranBugReport;",
        "()V",
        "attachment",
        "Lcom/squareup/comms/protos/buyer/BranBugReport$Attachment;",
        "done",
        "",
        "Ljava/lang/Boolean;",
        "id",
        "",
        "serial",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public attachment:Lcom/squareup/comms/protos/buyer/BranBugReport$Attachment;

.field public done:Ljava/lang/Boolean;

.field public id:Ljava/lang/String;

.field public serial:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 105
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public final attachment(Lcom/squareup/comms/protos/buyer/BranBugReport$Attachment;)Lcom/squareup/comms/protos/buyer/BranBugReport$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranBugReport$Builder;->attachment:Lcom/squareup/comms/protos/buyer/BranBugReport$Attachment;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/buyer/BranBugReport;
    .locals 9

    .line 141
    new-instance v6, Lcom/squareup/comms/protos/buyer/BranBugReport;

    .line 142
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/BranBugReport$Builder;->id:Ljava/lang/String;

    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz v1, :cond_1

    .line 143
    iget-object v4, p0, Lcom/squareup/comms/protos/buyer/BranBugReport$Builder;->serial:Ljava/lang/String;

    .line 144
    iget-object v5, p0, Lcom/squareup/comms/protos/buyer/BranBugReport$Builder;->attachment:Lcom/squareup/comms/protos/buyer/BranBugReport$Attachment;

    .line 145
    iget-object v7, p0, Lcom/squareup/comms/protos/buyer/BranBugReport$Builder;->done:Ljava/lang/Boolean;

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 146
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranBugReport$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v6

    move-object v2, v4

    move-object v3, v5

    move v4, v7

    move-object v5, v8

    .line 141
    invoke-direct/range {v0 .. v5}, Lcom/squareup/comms/protos/buyer/BranBugReport;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/comms/protos/buyer/BranBugReport$Attachment;ZLokio/ByteString;)V

    return-object v6

    :cond_0
    new-array v1, v3, [Ljava/lang/Object;

    aput-object v7, v1, v2

    const-string v2, "done"

    aput-object v2, v1, v0

    .line 145
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v2

    const-string v1, "id"

    aput-object v1, v3, v0

    .line 142
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 105
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranBugReport$Builder;->build()Lcom/squareup/comms/protos/buyer/BranBugReport;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final done(Z)Lcom/squareup/comms/protos/buyer/BranBugReport$Builder;
    .locals 0

    .line 137
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranBugReport$Builder;->done:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final id(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/BranBugReport$Builder;
    .locals 1

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranBugReport$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public final serial(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/BranBugReport$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranBugReport$Builder;->serial:Ljava/lang/String;

    return-object p0
.end method
