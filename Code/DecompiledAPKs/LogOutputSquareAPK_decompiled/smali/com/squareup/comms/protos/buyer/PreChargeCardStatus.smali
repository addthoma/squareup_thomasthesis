.class public final Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;
.super Lcom/squareup/wire/AndroidMessage;
.source "PreChargeCardStatus.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;,
        Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;",
        "Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPreChargeCardStatus.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PreChargeCardStatus.kt\ncom/squareup/comms/protos/buyer/PreChargeCardStatus\n*L\n1#1,200:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u00152\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0014\u0015B+\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ0\u0010\u000b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00042\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tJ\u0013\u0010\u000c\u001a\u00020\u00042\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0096\u0002J\u0008\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u0002H\u0016J\u0008\u0010\u0012\u001a\u00020\u0013H\u0016R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;",
        "inserted",
        "",
        "chip_card_swiped",
        "pre_swipe",
        "Lcom/squareup/comms/protos/buyer/PreSwipe;",
        "unknownFields",
        "Lokio/ByteString;",
        "(ZZLcom/squareup/comms/protos/buyer/PreSwipe;Lokio/ByteString;)V",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "newBuilder",
        "toString",
        "",
        "Builder",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Companion;


# instance fields
.field public final chip_card_swiped:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation
.end field

.field public final inserted:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation
.end field

.field public final pre_swipe:Lcom/squareup/comms/protos/buyer/PreSwipe;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.buyer.PreSwipe#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->Companion:Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Companion;

    .line 152
    new-instance v0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Companion$ADAPTER$1;

    .line 153
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 154
    const-class v2, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 197
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ZZLcom/squareup/comms/protos/buyer/PreSwipe;Lokio/ByteString;)V
    .locals 1

    const-string v0, "unknownFields"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    sget-object v0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    iput-boolean p1, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->inserted:Z

    iput-boolean p2, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->chip_card_swiped:Z

    iput-object p3, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->pre_swipe:Lcom/squareup/comms/protos/buyer/PreSwipe;

    return-void
.end method

.method public synthetic constructor <init>(ZZLcom/squareup/comms/protos/buyer/PreSwipe;Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_0

    const/4 p3, 0x0

    .line 59
    check-cast p3, Lcom/squareup/comms/protos/buyer/PreSwipe;

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    .line 60
    sget-object p4, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;-><init>(ZZLcom/squareup/comms/protos/buyer/PreSwipe;Lokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;ZZLcom/squareup/comms/protos/buyer/PreSwipe;Lokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    .line 101
    iget-boolean p1, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->inserted:Z

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    .line 102
    iget-boolean p2, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->chip_card_swiped:Z

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    .line 103
    iget-object p3, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->pre_swipe:Lcom/squareup/comms/protos/buyer/PreSwipe;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    .line 104
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->unknownFields()Lokio/ByteString;

    move-result-object p4

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->copy(ZZLcom/squareup/comms/protos/buyer/PreSwipe;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(ZZLcom/squareup/comms/protos/buyer/PreSwipe;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;
    .locals 1

    const-string v0, "unknownFields"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    new-instance v0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;-><init>(ZZLcom/squareup/comms/protos/buyer/PreSwipe;Lokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 72
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 73
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 77
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->inserted:Z

    iget-boolean v3, p1, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->inserted:Z

    if-ne v0, v3, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->chip_card_swiped:Z

    iget-boolean v3, p1, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->chip_card_swiped:Z

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->pre_swipe:Lcom/squareup/comms/protos/buyer/PreSwipe;

    iget-object p1, p1, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->pre_swipe:Lcom/squareup/comms/protos/buyer/PreSwipe;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 2

    .line 81
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_1

    .line 83
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 84
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->inserted:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->chip_card_swiped:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 86
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->pre_swipe:Lcom/squareup/comms/protos/buyer/PreSwipe;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 87
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;
    .locals 2

    .line 63
    new-instance v0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;-><init>()V

    .line 64
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->inserted:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;->inserted:Ljava/lang/Boolean;

    .line 65
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->chip_card_swiped:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;->chip_card_swiped:Ljava/lang/Boolean;

    .line 66
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->pre_swipe:Lcom/squareup/comms/protos/buyer/PreSwipe;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;->pre_swipe:Lcom/squareup/comms/protos/buyer/PreSwipe;

    .line 67
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->newBuilder()Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 94
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "inserted="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->inserted:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 95
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "chip_card_swiped="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->chip_card_swiped:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 96
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->pre_swipe:Lcom/squareup/comms/protos/buyer/PreSwipe;

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pre_swipe="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;->pre_swipe:Lcom/squareup/comms/protos/buyer/PreSwipe;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 97
    :cond_0
    move-object v3, v0

    check-cast v3, Ljava/lang/Iterable;

    const-string v0, "PreChargeCardStatus{"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const-string v0, ", "

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string v0, "}"

    move-object v6, v0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
