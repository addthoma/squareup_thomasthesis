.class public final Lcom/squareup/comms/protos/buyer/CardInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardInfo.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/CardInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/CardInfo;",
        "Lcom/squareup/comms/protos/buyer/CardInfo$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u000e\u0010\u0006\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007J\u0008\u0010\u000e\u001a\u00020\u0002H\u0016J\u0015\u0010\u0008\u001a\u00020\u00002\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u000fJ\u000e\u0010\u000b\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\u0007J\u0015\u0010\u000c\u001a\u00020\u00002\u0008\u0010\u000c\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u000fJ\u000e\u0010\r\u001a\u00020\u00002\u0006\u0010\r\u001a\u00020\u0007R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0008\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\nR\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000c\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\nR\u0014\u0010\r\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/CardInfo$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/CardInfo;",
        "()V",
        "application",
        "Lcom/squareup/comms/protos/buyer/EmvApplication;",
        "brand_short_code",
        "",
        "cvm_performed_swig_value",
        "",
        "Ljava/lang/Integer;",
        "last_4",
        "magswipe_track_mask",
        "name",
        "build",
        "(Ljava/lang/Integer;)Lcom/squareup/comms/protos/buyer/CardInfo$Builder;",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public application:Lcom/squareup/comms/protos/buyer/EmvApplication;

.field public brand_short_code:Ljava/lang/String;

.field public cvm_performed_swig_value:Ljava/lang/Integer;

.field public last_4:Ljava/lang/String;

.field public magswipe_track_mask:Ljava/lang/Integer;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 129
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public final application(Lcom/squareup/comms/protos/buyer/EmvApplication;)Lcom/squareup/comms/protos/buyer/CardInfo$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/CardInfo$Builder;->application:Lcom/squareup/comms/protos/buyer/EmvApplication;

    return-object p0
.end method

.method public final brand_short_code(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/CardInfo$Builder;
    .locals 1

    const-string v0, "brand_short_code"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/CardInfo$Builder;->brand_short_code:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/buyer/CardInfo;
    .locals 11

    .line 178
    new-instance v8, Lcom/squareup/comms/protos/buyer/CardInfo;

    .line 179
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/CardInfo$Builder;->brand_short_code:Ljava/lang/String;

    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz v1, :cond_2

    .line 181
    iget-object v4, p0, Lcom/squareup/comms/protos/buyer/CardInfo$Builder;->last_4:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 182
    iget-object v5, p0, Lcom/squareup/comms/protos/buyer/CardInfo$Builder;->name:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 183
    iget-object v6, p0, Lcom/squareup/comms/protos/buyer/CardInfo$Builder;->application:Lcom/squareup/comms/protos/buyer/EmvApplication;

    .line 184
    iget-object v7, p0, Lcom/squareup/comms/protos/buyer/CardInfo$Builder;->magswipe_track_mask:Ljava/lang/Integer;

    .line 185
    iget-object v9, p0, Lcom/squareup/comms/protos/buyer/CardInfo$Builder;->cvm_performed_swig_value:Ljava/lang/Integer;

    .line 186
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/CardInfo$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v8

    move-object v2, v4

    move-object v3, v5

    move-object v4, v6

    move-object v5, v7

    move-object v6, v9

    move-object v7, v10

    .line 178
    invoke-direct/range {v0 .. v7}, Lcom/squareup/comms/protos/buyer/CardInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/comms/protos/buyer/EmvApplication;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v8

    :cond_0
    new-array v1, v3, [Ljava/lang/Object;

    aput-object v5, v1, v2

    const-string v2, "name"

    aput-object v2, v1, v0

    .line 182
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v1, v3, [Ljava/lang/Object;

    aput-object v4, v1, v2

    const-string v2, "last_4"

    aput-object v2, v1, v0

    .line 181
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_2
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v2

    const-string v1, "brand_short_code"

    aput-object v1, v3, v0

    .line 179
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 129
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/CardInfo$Builder;->build()Lcom/squareup/comms/protos/buyer/CardInfo;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final cvm_performed_swig_value(Ljava/lang/Integer;)Lcom/squareup/comms/protos/buyer/CardInfo$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/CardInfo$Builder;->cvm_performed_swig_value:Ljava/lang/Integer;

    return-object p0
.end method

.method public final last_4(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/CardInfo$Builder;
    .locals 1

    const-string v0, "last_4"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/CardInfo$Builder;->last_4:Ljava/lang/String;

    return-object p0
.end method

.method public final magswipe_track_mask(Ljava/lang/Integer;)Lcom/squareup/comms/protos/buyer/CardInfo$Builder;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/CardInfo$Builder;->magswipe_track_mask:Ljava/lang/Integer;

    return-object p0
.end method

.method public final name(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/CardInfo$Builder;
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/CardInfo$Builder;->name:Ljava/lang/String;

    return-object p0
.end method
