.class public final Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LogFirmwareEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;",
        "Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\r\u001a\u00020\u0002H\u0016J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u000e\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\u0008J\u000e\u0010\t\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0008J\u000e\u0010\n\u001a\u00020\u00002\u0006\u0010\n\u001a\u00020\u000bR\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000c\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;",
        "()V",
        "event",
        "",
        "Ljava/lang/Integer;",
        "message",
        "",
        "source",
        "timestamp",
        "",
        "Ljava/lang/Long;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public event:Ljava/lang/Integer;

.field public message:Ljava/lang/String;

.field public source:Ljava/lang/String;

.field public timestamp:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 233
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;
    .locals 11

    .line 266
    new-instance v7, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;

    .line 267
    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;->timestamp:Ljava/lang/Long;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 268
    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;->event:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 269
    iget-object v8, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;->source:Ljava/lang/String;

    if-eqz v8, :cond_1

    .line 270
    iget-object v9, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;->message:Ljava/lang/String;

    if-eqz v9, :cond_0

    .line 271
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v7

    move-wide v1, v4

    move v3, v6

    move-object v4, v8

    move-object v5, v9

    move-object v6, v10

    .line 266
    invoke-direct/range {v0 .. v6}, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;-><init>(JILjava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7

    :cond_0
    new-array v0, v3, [Ljava/lang/Object;

    aput-object v9, v0, v2

    const-string v2, "message"

    aput-object v2, v0, v1

    .line 270
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v0, v3, [Ljava/lang/Object;

    aput-object v8, v0, v2

    const-string v2, "source"

    aput-object v2, v0, v1

    .line 269
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_2
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "event"

    aput-object v0, v3, v1

    .line 268
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_3
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "timestamp"

    aput-object v0, v3, v1

    .line 267
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 233
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;->build()Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final event(I)Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;
    .locals 0

    .line 252
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;->event:Ljava/lang/Integer;

    return-object p0
.end method

.method public final message(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;->message:Ljava/lang/String;

    return-object p0
.end method

.method public final source(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;
    .locals 1

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 257
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;->source:Ljava/lang/String;

    return-object p0
.end method

.method public final timestamp(J)Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;
    .locals 0

    .line 247
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;->timestamp:Ljava/lang/Long;

    return-object p0
.end method
