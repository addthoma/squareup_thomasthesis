.class public final Lcom/squareup/comms/protos/buyer/MagSwipe$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MagSwipe.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/MagSwipe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/MagSwipe;",
        "Lcom/squareup/comms/protos/buyer/MagSwipe$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0008\u001a\u00020\u0002H\u0016J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u0006\u001a\u00020\u00002\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/MagSwipe$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/MagSwipe;",
        "()V",
        "card",
        "Lcom/squareup/comms/protos/common/Card;",
        "card_reader_info",
        "Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public card:Lcom/squareup/comms/protos/common/Card;

.field public card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 83
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/MagSwipe;
    .locals 4

    .line 103
    new-instance v0, Lcom/squareup/comms/protos/buyer/MagSwipe;

    .line 104
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/MagSwipe$Builder;->card:Lcom/squareup/comms/protos/common/Card;

    .line 105
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/MagSwipe$Builder;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    .line 106
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/MagSwipe$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    .line 103
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/comms/protos/buyer/MagSwipe;-><init>(Lcom/squareup/comms/protos/common/Card;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 83
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/MagSwipe$Builder;->build()Lcom/squareup/comms/protos/buyer/MagSwipe;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final card(Lcom/squareup/comms/protos/common/Card;)Lcom/squareup/comms/protos/buyer/MagSwipe$Builder;
    .locals 0

    .line 94
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/MagSwipe$Builder;->card:Lcom/squareup/comms/protos/common/Card;

    return-object p0
.end method

.method public final card_reader_info(Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;)Lcom/squareup/comms/protos/buyer/MagSwipe$Builder;
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/MagSwipe$Builder;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    return-object p0
.end method
