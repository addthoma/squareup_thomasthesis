.class public final Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;
.super Lcom/squareup/wire/AndroidMessage;
.source "LoyaltyCartBannerClicked.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked$Builder;,
        Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;",
        "Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoyaltyCartBannerClicked.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoyaltyCartBannerClicked.kt\ncom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked\n*L\n1#1,162:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u00132\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0012\u0013B\u001f\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J$\u0010\t\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007J\u0013\u0010\n\u001a\u00020\u00042\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u0096\u0002J\u0008\u0010\r\u001a\u00020\u000eH\u0016J\u0008\u0010\u000f\u001a\u00020\u0002H\u0016J\u0008\u0010\u0010\u001a\u00020\u0011H\u0016R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked$Builder;",
        "clicked_on_idle",
        "",
        "already_checked_in",
        "unknownFields",
        "Lokio/ByteString;",
        "(ZZLokio/ByteString;)V",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "newBuilder",
        "toString",
        "",
        "Builder",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked$Companion;


# instance fields
.field public final already_checked_in:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation
.end field

.field public final clicked_on_idle:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->Companion:Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked$Companion;

    .line 118
    new-instance v0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked$Companion$ADAPTER$1;

    .line 120
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 121
    const-class v2, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 159
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ZZLokio/ByteString;)V
    .locals 1

    const-string v0, "unknownFields"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    sget-object v0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    iput-boolean p1, p0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->clicked_on_idle:Z

    iput-boolean p2, p0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->already_checked_in:Z

    return-void
.end method

.method public synthetic constructor <init>(ZZLokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 42
    sget-object p3, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;-><init>(ZZLokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;ZZLokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    .line 81
    iget-boolean p1, p0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->clicked_on_idle:Z

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    .line 82
    iget-boolean p2, p0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->already_checked_in:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    .line 83
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->unknownFields()Lokio/ByteString;

    move-result-object p3

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->copy(ZZLokio/ByteString;)Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(ZZLokio/ByteString;)Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;
    .locals 1

    const-string v0, "unknownFields"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    new-instance v0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;-><init>(ZZLokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 54
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 55
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 58
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->clicked_on_idle:Z

    iget-boolean v3, p1, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->clicked_on_idle:Z

    if-ne v0, v3, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->already_checked_in:Z

    iget-boolean p1, p1, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->already_checked_in:Z

    if-ne v0, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 2

    .line 62
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 65
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->clicked_on_idle:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 66
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->already_checked_in:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_0
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked$Builder;
    .locals 2

    .line 46
    new-instance v0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked$Builder;-><init>()V

    .line 47
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->clicked_on_idle:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked$Builder;->clicked_on_idle:Ljava/lang/Boolean;

    .line 48
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->already_checked_in:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked$Builder;->already_checked_in:Ljava/lang/Boolean;

    .line 49
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->newBuilder()Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 74
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clicked_on_idle="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->clicked_on_idle:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 75
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "already_checked_in="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;->already_checked_in:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 76
    move-object v3, v0

    check-cast v3, Ljava/lang/Iterable;

    const-string v0, "LoyaltyCartBannerClicked{"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const-string v0, ", "

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string v0, "}"

    .line 77
    move-object v6, v0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    .line 76
    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
