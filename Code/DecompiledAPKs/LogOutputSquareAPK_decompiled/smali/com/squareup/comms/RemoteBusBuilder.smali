.class public abstract Lcom/squareup/comms/RemoteBusBuilder;
.super Ljava/lang/Object;
.source "RemoteBusBuilder.java"


# static fields
.field private static MAX_RECONNECT_TIMEOUT_MS:I = 0x1388


# instance fields
.field private built:Z

.field private healthChecker:Lcom/squareup/comms/HealthChecker;

.field protected final ioThread:Lcom/squareup/comms/common/IoThread;

.field private isClient:Z

.field private localHost:Ljava/lang/String;

.field private maxReconnectTimetoutMs:I

.field private port:I

.field private remoteHost:Ljava/lang/String;

.field private final serializer:Lcom/squareup/comms/Serializer;

.field private final socketConnectionFailureListener:Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/comms/Serializer;Ljava/lang/String;I)V
    .locals 1

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    sget v0, Lcom/squareup/comms/RemoteBusBuilder;->MAX_RECONNECT_TIMEOUT_MS:I

    iput v0, p0, Lcom/squareup/comms/RemoteBusBuilder;->maxReconnectTimetoutMs:I

    .line 22
    new-instance v0, Lcom/squareup/comms/X2NullHealthChecker;

    invoke-direct {v0}, Lcom/squareup/comms/X2NullHealthChecker;-><init>()V

    iput-object v0, p0, Lcom/squareup/comms/RemoteBusBuilder;->healthChecker:Lcom/squareup/comms/HealthChecker;

    const-string v0, "serializer"

    .line 31
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/comms/Serializer;

    iput-object p1, p0, Lcom/squareup/comms/RemoteBusBuilder;->serializer:Lcom/squareup/comms/Serializer;

    .line 32
    iput-object p2, p0, Lcom/squareup/comms/RemoteBusBuilder;->localHost:Ljava/lang/String;

    .line 33
    iput p3, p0, Lcom/squareup/comms/RemoteBusBuilder;->port:I

    const/4 p1, 0x0

    .line 34
    iput-boolean p1, p0, Lcom/squareup/comms/RemoteBusBuilder;->isClient:Z

    .line 35
    new-instance p1, Lcom/squareup/comms/net/socket/SocketConnectionFailureListener$NoSocketConnectionErrorListener;

    invoke-direct {p1}, Lcom/squareup/comms/net/socket/SocketConnectionFailureListener$NoSocketConnectionErrorListener;-><init>()V

    iput-object p1, p0, Lcom/squareup/comms/RemoteBusBuilder;->socketConnectionFailureListener:Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;

    .line 36
    new-instance p1, Lcom/squareup/comms/common/IoThread;

    const-string p2, "server"

    invoke-direct {p1, p2}, Lcom/squareup/comms/common/IoThread;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/comms/RemoteBusBuilder;->ioThread:Lcom/squareup/comms/common/IoThread;

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/comms/Serializer;Ljava/lang/String;ILjava/lang/String;Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;)V
    .locals 1

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    sget v0, Lcom/squareup/comms/RemoteBusBuilder;->MAX_RECONNECT_TIMEOUT_MS:I

    iput v0, p0, Lcom/squareup/comms/RemoteBusBuilder;->maxReconnectTimetoutMs:I

    .line 22
    new-instance v0, Lcom/squareup/comms/X2NullHealthChecker;

    invoke-direct {v0}, Lcom/squareup/comms/X2NullHealthChecker;-><init>()V

    iput-object v0, p0, Lcom/squareup/comms/RemoteBusBuilder;->healthChecker:Lcom/squareup/comms/HealthChecker;

    const-string v0, "serializer"

    .line 41
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/comms/Serializer;

    iput-object p1, p0, Lcom/squareup/comms/RemoteBusBuilder;->serializer:Lcom/squareup/comms/Serializer;

    const-string p1, "remoteHost"

    .line 42
    invoke-static {p2, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/comms/RemoteBusBuilder;->remoteHost:Ljava/lang/String;

    .line 43
    iput p3, p0, Lcom/squareup/comms/RemoteBusBuilder;->port:I

    .line 44
    iput-object p4, p0, Lcom/squareup/comms/RemoteBusBuilder;->localHost:Ljava/lang/String;

    const/4 p1, 0x1

    .line 45
    iput-boolean p1, p0, Lcom/squareup/comms/RemoteBusBuilder;->isClient:Z

    .line 46
    iput-object p5, p0, Lcom/squareup/comms/RemoteBusBuilder;->socketConnectionFailureListener:Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;

    .line 47
    new-instance p1, Lcom/squareup/comms/common/IoThread;

    const-string p2, "client"

    invoke-direct {p1, p2}, Lcom/squareup/comms/common/IoThread;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/comms/RemoteBusBuilder;->ioThread:Lcom/squareup/comms/common/IoThread;

    return-void
.end method

.method private createClient()Lcom/squareup/comms/RemoteBusImpl;
    .locals 8

    .line 83
    new-instance v7, Lcom/squareup/comms/net/socket/ClientConnectionFactory;

    iget-object v1, p0, Lcom/squareup/comms/RemoteBusBuilder;->ioThread:Lcom/squareup/comms/common/IoThread;

    iget-object v2, p0, Lcom/squareup/comms/RemoteBusBuilder;->localHost:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/comms/RemoteBusBuilder;->remoteHost:Ljava/lang/String;

    iget v4, p0, Lcom/squareup/comms/RemoteBusBuilder;->port:I

    iget v5, p0, Lcom/squareup/comms/RemoteBusBuilder;->maxReconnectTimetoutMs:I

    iget-object v6, p0, Lcom/squareup/comms/RemoteBusBuilder;->socketConnectionFailureListener:Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;-><init>(Lcom/squareup/comms/common/IoThread;Ljava/lang/String;Ljava/lang/String;IILcom/squareup/comms/net/socket/SocketConnectionFailureListener;)V

    .line 90
    new-instance v0, Lcom/squareup/comms/net/Channel;

    iget-object v1, p0, Lcom/squareup/comms/RemoteBusBuilder;->ioThread:Lcom/squareup/comms/common/IoThread;

    invoke-direct {v0, v1, v7}, Lcom/squareup/comms/net/Channel;-><init>(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/net/ConnectionFactory;)V

    .line 91
    new-instance v1, Lcom/squareup/comms/RemoteBusImpl;

    iget-object v2, p0, Lcom/squareup/comms/RemoteBusBuilder;->ioThread:Lcom/squareup/comms/common/IoThread;

    iget-object v3, p0, Lcom/squareup/comms/RemoteBusBuilder;->serializer:Lcom/squareup/comms/Serializer;

    iget-object v4, p0, Lcom/squareup/comms/RemoteBusBuilder;->healthChecker:Lcom/squareup/comms/HealthChecker;

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/squareup/comms/RemoteBusImpl;-><init>(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/net/Channel;Lcom/squareup/comms/Serializer;Lcom/squareup/comms/HealthChecker;)V

    return-object v1
.end method

.method private createServer()Lcom/squareup/comms/RemoteBusImpl;
    .locals 5

    .line 69
    new-instance v0, Lcom/squareup/comms/net/socket/ServerConnectionFactory;

    iget-object v1, p0, Lcom/squareup/comms/RemoteBusBuilder;->ioThread:Lcom/squareup/comms/common/IoThread;

    iget-object v2, p0, Lcom/squareup/comms/RemoteBusBuilder;->localHost:Ljava/lang/String;

    iget v3, p0, Lcom/squareup/comms/RemoteBusBuilder;->port:I

    iget v4, p0, Lcom/squareup/comms/RemoteBusBuilder;->maxReconnectTimetoutMs:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/comms/net/socket/ServerConnectionFactory;-><init>(Lcom/squareup/comms/common/IoThread;Ljava/lang/String;II)V

    .line 74
    new-instance v1, Lcom/squareup/comms/net/Channel;

    iget-object v2, p0, Lcom/squareup/comms/RemoteBusBuilder;->ioThread:Lcom/squareup/comms/common/IoThread;

    invoke-direct {v1, v2, v0}, Lcom/squareup/comms/net/Channel;-><init>(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/net/ConnectionFactory;)V

    .line 75
    new-instance v0, Lcom/squareup/comms/RemoteBusImpl;

    iget-object v2, p0, Lcom/squareup/comms/RemoteBusBuilder;->ioThread:Lcom/squareup/comms/common/IoThread;

    iget-object v3, p0, Lcom/squareup/comms/RemoteBusBuilder;->serializer:Lcom/squareup/comms/Serializer;

    iget-object v4, p0, Lcom/squareup/comms/RemoteBusBuilder;->healthChecker:Lcom/squareup/comms/HealthChecker;

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/squareup/comms/RemoteBusImpl;-><init>(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/net/Channel;Lcom/squareup/comms/Serializer;Lcom/squareup/comms/HealthChecker;)V

    return-object v0
.end method


# virtual methods
.method public build()Lcom/squareup/comms/RemoteBus;
    .locals 2

    .line 60
    iget-boolean v0, p0, Lcom/squareup/comms/RemoteBusBuilder;->isClient:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/comms/RemoteBusBuilder;->createClient()Lcom/squareup/comms/RemoteBusImpl;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/squareup/comms/RemoteBusBuilder;->createServer()Lcom/squareup/comms/RemoteBusImpl;

    move-result-object v0

    .line 61
    :goto_0
    iget-boolean v1, p0, Lcom/squareup/comms/RemoteBusBuilder;->built:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 64
    iput-boolean v1, p0, Lcom/squareup/comms/RemoteBusBuilder;->built:Z

    return-object v0

    .line 62
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Should not share builder instances"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public healthChecker(Lcom/squareup/comms/HealthChecker;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/squareup/comms/RemoteBusBuilder;->healthChecker:Lcom/squareup/comms/HealthChecker;

    return-void
.end method

.method public maxReconnectTimeoutMs(I)Lcom/squareup/comms/RemoteBusBuilder;
    .locals 0

    .line 55
    iput p1, p0, Lcom/squareup/comms/RemoteBusBuilder;->maxReconnectTimetoutMs:I

    return-object p0
.end method
