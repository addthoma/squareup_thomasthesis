.class public Lcom/squareup/comms/X2NullHealthChecker;
.super Ljava/lang/Object;
.source "X2NullHealthChecker.java"

# interfaces
.implements Lcom/squareup/comms/HealthChecker;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public start(Lcom/squareup/comms/MessagePoster;Lcom/squareup/comms/HealthChecker$Callback;)V
    .locals 0

    .line 26
    invoke-interface {p2}, Lcom/squareup/comms/HealthChecker$Callback;->onHealthy()V

    return-void
.end method

.method public stop()V
    .locals 0

    return-void
.end method

.method public tryConsumeMessage(Lcom/squareup/comms/MessagePoster;Lcom/squareup/wire/Message;)Z
    .locals 1

    .line 17
    instance-of v0, p2, Lcom/squareup/comms/protos/common/HealthCheckRequest;

    if-eqz v0, :cond_0

    .line 18
    new-instance p2, Lcom/squareup/comms/protos/common/HealthCheckResponse;

    invoke-direct {p2}, Lcom/squareup/comms/protos/common/HealthCheckResponse;-><init>()V

    invoke-interface {p1, p2}, Lcom/squareup/comms/MessagePoster;->post(Lcom/squareup/wire/Message;)V

    const/4 p1, 0x1

    return p1

    .line 22
    :cond_0
    instance-of p1, p2, Lcom/squareup/comms/protos/common/HealthCheckResponse;

    return p1
.end method
