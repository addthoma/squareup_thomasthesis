.class final Lcom/squareup/orders/CreateOrderRequest$Tax$ProtoAdapter_Tax;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CreateOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/CreateOrderRequest$Tax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Tax"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/CreateOrderRequest$Tax;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1040
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/CreateOrderRequest$Tax;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/CreateOrderRequest$Tax;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1063
    new-instance v0, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;-><init>()V

    .line 1064
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1065
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 1079
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1077
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->percentage(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;

    goto :goto_0

    .line 1071
    :cond_1
    :try_start_0
    sget-object v4, Lcom/squareup/orders/model/Order$LineItem$Tax$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    invoke-virtual {v0, v4}, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->type(Lcom/squareup/orders/model/Order$LineItem$Tax$Type;)Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 1073
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 1068
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->name(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;

    goto :goto_0

    .line 1067
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;

    goto :goto_0

    .line 1083
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1084
    invoke-virtual {v0}, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->build()Lcom/squareup/orders/CreateOrderRequest$Tax;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1038
    invoke-virtual {p0, p1}, Lcom/squareup/orders/CreateOrderRequest$Tax$ProtoAdapter_Tax;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/CreateOrderRequest$Tax;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/CreateOrderRequest$Tax;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1054
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$Tax;->catalog_object_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1055
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$Tax;->name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1056
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Tax$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$Tax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1057
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$Tax;->percentage:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1058
    invoke-virtual {p2}, Lcom/squareup/orders/CreateOrderRequest$Tax;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1038
    check-cast p2, Lcom/squareup/orders/CreateOrderRequest$Tax;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/CreateOrderRequest$Tax$ProtoAdapter_Tax;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/CreateOrderRequest$Tax;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/CreateOrderRequest$Tax;)I
    .locals 4

    .line 1045
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/CreateOrderRequest$Tax;->catalog_object_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/CreateOrderRequest$Tax;->name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 1046
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$Tax$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/CreateOrderRequest$Tax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    const/4 v3, 0x3

    .line 1047
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/CreateOrderRequest$Tax;->percentage:Ljava/lang/String;

    const/4 v3, 0x4

    .line 1048
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1049
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Tax;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1038
    check-cast p1, Lcom/squareup/orders/CreateOrderRequest$Tax;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/CreateOrderRequest$Tax$ProtoAdapter_Tax;->encodedSize(Lcom/squareup/orders/CreateOrderRequest$Tax;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/CreateOrderRequest$Tax;)Lcom/squareup/orders/CreateOrderRequest$Tax;
    .locals 0

    .line 1089
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Tax;->newBuilder()Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;

    move-result-object p1

    .line 1090
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1091
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->build()Lcom/squareup/orders/CreateOrderRequest$Tax;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1038
    check-cast p1, Lcom/squareup/orders/CreateOrderRequest$Tax;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/CreateOrderRequest$Tax$ProtoAdapter_Tax;->redact(Lcom/squareup/orders/CreateOrderRequest$Tax;)Lcom/squareup/orders/CreateOrderRequest$Tax;

    move-result-object p1

    return-object p1
.end method
