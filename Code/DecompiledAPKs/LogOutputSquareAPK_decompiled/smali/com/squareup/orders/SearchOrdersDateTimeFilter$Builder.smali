.class public final Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SearchOrdersDateTimeFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/SearchOrdersDateTimeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/SearchOrdersDateTimeFilter;",
        "Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public closed_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

.field public created_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

.field public updated_at:Lcom/squareup/protos/connect/v2/common/TimeRange;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 147
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/SearchOrdersDateTimeFilter;
    .locals 5

    .line 190
    new-instance v0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->created_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    iget-object v2, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->updated_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    iget-object v3, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->closed_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/orders/SearchOrdersDateTimeFilter;-><init>(Lcom/squareup/protos/connect/v2/common/TimeRange;Lcom/squareup/protos/connect/v2/common/TimeRange;Lcom/squareup/protos/connect/v2/common/TimeRange;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 140
    invoke-virtual {p0}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->build()Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    move-result-object v0

    return-object v0
.end method

.method public closed_at(Lcom/squareup/protos/connect/v2/common/TimeRange;)Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->closed_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    return-object p0
.end method

.method public created_at(Lcom/squareup/protos/connect/v2/common/TimeRange;)Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->created_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    return-object p0
.end method

.method public updated_at(Lcom/squareup/protos/connect/v2/common/TimeRange;)Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->updated_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    return-object p0
.end method
