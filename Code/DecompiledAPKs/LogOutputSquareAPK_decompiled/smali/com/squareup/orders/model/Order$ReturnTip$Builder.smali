.class public final Lcom/squareup/orders/model/Order$ReturnTip$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$ReturnTip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$ReturnTip;",
        "Lcom/squareup/orders/model/Order$ReturnTip$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public applied_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public source_tip_uid:Ljava/lang/String;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15043
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public applied_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnTip$Builder;
    .locals 0

    .line 15067
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$ReturnTip;
    .locals 5

    .line 15073
    new-instance v0, Lcom/squareup/orders/model/Order$ReturnTip;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->uid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->source_tip_uid:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/orders/model/Order$ReturnTip;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 15036
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->build()Lcom/squareup/orders/model/Order$ReturnTip;

    move-result-object v0

    return-object v0
.end method

.method public source_tip_uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnTip$Builder;
    .locals 0

    .line 15062
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->source_tip_uid:Ljava/lang/String;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnTip$Builder;
    .locals 0

    .line 15052
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method
