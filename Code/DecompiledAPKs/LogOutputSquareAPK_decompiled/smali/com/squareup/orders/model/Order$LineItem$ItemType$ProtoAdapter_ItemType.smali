.class final Lcom/squareup/orders/model/Order$LineItem$ItemType$ProtoAdapter_ItemType;
.super Lcom/squareup/wire/EnumAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$LineItem$ItemType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ItemType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/orders/model/Order$LineItem$ItemType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 3212
    const-class v0, Lcom/squareup/orders/model/Order$LineItem$ItemType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/orders/model/Order$LineItem$ItemType;
    .locals 0

    .line 3217
    invoke-static {p1}, Lcom/squareup/orders/model/Order$LineItem$ItemType;->fromValue(I)Lcom/squareup/orders/model/Order$LineItem$ItemType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 3210
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$LineItem$ItemType$ProtoAdapter_ItemType;->fromValue(I)Lcom/squareup/orders/model/Order$LineItem$ItemType;

    move-result-object p1

    return-object p1
.end method
