.class final Lcom/squareup/orders/model/Order$ReservedReward$ProtoAdapter_ReservedReward;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$ReservedReward;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReservedReward"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/model/Order$ReservedReward;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 15584
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/model/Order$ReservedReward;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$ReservedReward;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15605
    new-instance v0, Lcom/squareup/orders/model/Order$ReservedReward$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReservedReward$Builder;-><init>()V

    .line 15606
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 15607
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 15613
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 15611
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->pricing_rule_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReservedReward$Builder;

    goto :goto_0

    .line 15610
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->reward_tier_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReservedReward$Builder;

    goto :goto_0

    .line 15609
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReservedReward$Builder;

    goto :goto_0

    .line 15617
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 15618
    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->build()Lcom/squareup/orders/model/Order$ReservedReward;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15582
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$ReservedReward$ProtoAdapter_ReservedReward;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$ReservedReward;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$ReservedReward;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15597
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReservedReward;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15598
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReservedReward;->reward_tier_id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15599
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReservedReward;->pricing_rule_id:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15600
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$ReservedReward;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15582
    check-cast p2, Lcom/squareup/orders/model/Order$ReservedReward;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/model/Order$ReservedReward$ProtoAdapter_ReservedReward;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$ReservedReward;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/model/Order$ReservedReward;)I
    .locals 4

    .line 15589
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$ReservedReward;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReservedReward;->reward_tier_id:Ljava/lang/String;

    const/4 v3, 0x2

    .line 15590
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReservedReward;->pricing_rule_id:Ljava/lang/String;

    const/4 v3, 0x3

    .line 15591
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15592
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReservedReward;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 15582
    check-cast p1, Lcom/squareup/orders/model/Order$ReservedReward;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$ReservedReward$ProtoAdapter_ReservedReward;->encodedSize(Lcom/squareup/orders/model/Order$ReservedReward;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/model/Order$ReservedReward;)Lcom/squareup/orders/model/Order$ReservedReward;
    .locals 0

    .line 15623
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReservedReward;->newBuilder()Lcom/squareup/orders/model/Order$ReservedReward$Builder;

    move-result-object p1

    .line 15624
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 15625
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->build()Lcom/squareup/orders/model/Order$ReservedReward;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15582
    check-cast p1, Lcom/squareup/orders/model/Order$ReservedReward;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$ReservedReward$ProtoAdapter_ReservedReward;->redact(Lcom/squareup/orders/model/Order$ReservedReward;)Lcom/squareup/orders/model/Order$ReservedReward;

    move-result-object p1

    return-object p1
.end method
