.class public final Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FulfillmentPickupDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$ProtoAdapter_FulfillmentPickupDetails;,
        Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;",
        "Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACCEPTANCE_ACKNOWLEDGED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_ACCEPTED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_AUTO_COMPLETE_DURATION:Ljava/lang/String; = ""

.field public static final DEFAULT_CANCELED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_CANCEL_REASON:Ljava/lang/String; = ""

.field public static final DEFAULT_EXPIRED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_EXPIRES_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_PICKED_UP_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_PICKUP_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_PICKUP_WINDOW_DURATION:Ljava/lang/String; = ""

.field public static final DEFAULT_PLACED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_PREP_TIME_DURATION:Ljava/lang/String; = ""

.field public static final DEFAULT_READY_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_REJECTED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_SCHEDULE_TYPE:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

.field private static final serialVersionUID:J


# instance fields
.field public final acceptance_acknowledged_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x12
    .end annotation
.end field

.field public final accepted_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final auto_complete_duration:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x13
    .end annotation
.end field

.field public final cancel_reason:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x19
    .end annotation
.end field

.field public final canceled_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x18
    .end annotation
.end field

.field public final expired_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc
    .end annotation
.end field

.field public final expires_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final picked_up_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x17
    .end annotation
.end field

.field public final pickup_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final pickup_window_duration:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x14
    .end annotation
.end field

.field public final placed_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final prep_time_duration:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x15
    .end annotation
.end field

.field public final ready_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$FulfillmentRecipient#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final rejected_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$FulfillmentPickupDetailsScheduleType#ADAPTER"
        tag = 0xd
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7728
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$ProtoAdapter_FulfillmentPickupDetails;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$ProtoAdapter_FulfillmentPickupDetails;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 7736
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;->SCHEDULED:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    sput-object v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->DEFAULT_SCHEDULE_TYPE:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;Lokio/ByteString;)V
    .locals 1

    .line 7996
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 7997
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    .line 7998
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->expires_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->expires_at:Ljava/lang/String;

    .line 7999
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->auto_complete_duration:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->auto_complete_duration:Ljava/lang/String;

    .line 8000
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    .line 8001
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->pickup_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->pickup_at:Ljava/lang/String;

    .line 8002
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->pickup_window_duration:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->pickup_window_duration:Ljava/lang/String;

    .line 8003
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->prep_time_duration:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->prep_time_duration:Ljava/lang/String;

    .line 8004
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->note:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->note:Ljava/lang/String;

    .line 8005
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->placed_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->placed_at:Ljava/lang/String;

    .line 8006
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->accepted_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->accepted_at:Ljava/lang/String;

    .line 8007
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->acceptance_acknowledged_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->acceptance_acknowledged_at:Ljava/lang/String;

    .line 8008
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->rejected_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->rejected_at:Ljava/lang/String;

    .line 8009
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->ready_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->ready_at:Ljava/lang/String;

    .line 8010
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->expired_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->expired_at:Ljava/lang/String;

    .line 8011
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->picked_up_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->picked_up_at:Ljava/lang/String;

    .line 8012
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->canceled_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->canceled_at:Ljava/lang/String;

    .line 8013
    iget-object p1, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->cancel_reason:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->cancel_reason:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 8043
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 8044
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    .line 8045
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    .line 8046
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->expires_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->expires_at:Ljava/lang/String;

    .line 8047
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->auto_complete_duration:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->auto_complete_duration:Ljava/lang/String;

    .line 8048
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    .line 8049
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->pickup_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->pickup_at:Ljava/lang/String;

    .line 8050
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->pickup_window_duration:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->pickup_window_duration:Ljava/lang/String;

    .line 8051
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->prep_time_duration:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->prep_time_duration:Ljava/lang/String;

    .line 8052
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->note:Ljava/lang/String;

    .line 8053
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->placed_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->placed_at:Ljava/lang/String;

    .line 8054
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->accepted_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->accepted_at:Ljava/lang/String;

    .line 8055
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->acceptance_acknowledged_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->acceptance_acknowledged_at:Ljava/lang/String;

    .line 8056
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->rejected_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->rejected_at:Ljava/lang/String;

    .line 8057
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->ready_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->ready_at:Ljava/lang/String;

    .line 8058
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->expired_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->expired_at:Ljava/lang/String;

    .line 8059
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->picked_up_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->picked_up_at:Ljava/lang/String;

    .line 8060
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->canceled_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->canceled_at:Ljava/lang/String;

    .line 8061
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->cancel_reason:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->cancel_reason:Ljava/lang/String;

    .line 8062
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 8067
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_11

    .line 8069
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 8070
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8071
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->expires_at:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8072
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->auto_complete_duration:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8073
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8074
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->pickup_at:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8075
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->pickup_window_duration:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8076
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->prep_time_duration:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8077
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->note:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8078
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->placed_at:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8079
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->accepted_at:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8080
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->acceptance_acknowledged_at:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8081
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->rejected_at:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8082
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->ready_at:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8083
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->expired_at:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8084
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->picked_up_at:Ljava/lang/String;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8085
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->canceled_at:Ljava/lang/String;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8086
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->cancel_reason:Ljava/lang/String;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_10
    add-int/2addr v0, v2

    .line 8087
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_11
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;
    .locals 2

    .line 8018
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;-><init>()V

    .line 8019
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    .line 8020
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->expires_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->expires_at:Ljava/lang/String;

    .line 8021
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->auto_complete_duration:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->auto_complete_duration:Ljava/lang/String;

    .line 8022
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    .line 8023
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->pickup_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->pickup_at:Ljava/lang/String;

    .line 8024
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->pickup_window_duration:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->pickup_window_duration:Ljava/lang/String;

    .line 8025
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->prep_time_duration:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->prep_time_duration:Ljava/lang/String;

    .line 8026
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->note:Ljava/lang/String;

    .line 8027
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->placed_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->placed_at:Ljava/lang/String;

    .line 8028
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->accepted_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->accepted_at:Ljava/lang/String;

    .line 8029
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->acceptance_acknowledged_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->acceptance_acknowledged_at:Ljava/lang/String;

    .line 8030
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->rejected_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->rejected_at:Ljava/lang/String;

    .line 8031
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->ready_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->ready_at:Ljava/lang/String;

    .line 8032
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->expired_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->expired_at:Ljava/lang/String;

    .line 8033
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->picked_up_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->picked_up_at:Ljava/lang/String;

    .line 8034
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->canceled_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->canceled_at:Ljava/lang/String;

    .line 8035
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->cancel_reason:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->cancel_reason:Ljava/lang/String;

    .line 8036
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 7727
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->newBuilder()Lcom/squareup/orders/model/Order$FulfillmentPickupDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 8094
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8095
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    if-eqz v1, :cond_0

    const-string v1, ", recipient="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8096
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->expires_at:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", expires_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->expires_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8097
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->auto_complete_duration:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", auto_complete_duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->auto_complete_duration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8098
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    if-eqz v1, :cond_3

    const-string v1, ", schedule_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8099
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->pickup_at:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", pickup_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->pickup_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8100
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->pickup_window_duration:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", pickup_window_duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->pickup_window_duration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8101
    :cond_5
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->prep_time_duration:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", prep_time_duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->prep_time_duration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8102
    :cond_6
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->note:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8103
    :cond_7
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->placed_at:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", placed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->placed_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8104
    :cond_8
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->accepted_at:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", accepted_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->accepted_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8105
    :cond_9
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->acceptance_acknowledged_at:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", acceptance_acknowledged_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->acceptance_acknowledged_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8106
    :cond_a
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->rejected_at:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", rejected_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->rejected_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8107
    :cond_b
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->ready_at:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", ready_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->ready_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8108
    :cond_c
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->expired_at:Ljava/lang/String;

    if-eqz v1, :cond_d

    const-string v1, ", expired_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->expired_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8109
    :cond_d
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->picked_up_at:Ljava/lang/String;

    if-eqz v1, :cond_e

    const-string v1, ", picked_up_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->picked_up_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8110
    :cond_e
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->canceled_at:Ljava/lang/String;

    if-eqz v1, :cond_f

    const-string v1, ", canceled_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->canceled_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8111
    :cond_f
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->cancel_reason:Ljava/lang/String;

    if-eqz v1, :cond_10

    const-string v1, ", cancel_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->cancel_reason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_10
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FulfillmentPickupDetails{"

    .line 8112
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
