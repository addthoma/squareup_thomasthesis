.class public final Lcom/squareup/orders/model/Order$LineItem$Tax;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$LineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Tax"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$LineItem$Tax$ProtoAdapter_Tax;,
        Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;,
        Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope;,
        Lcom/squareup/orders/model/Order$LineItem$Tax$Type;,
        Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$LineItem$Tax;",
        "Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$LineItem$Tax;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CALCULATION_PHASE:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

.field public static final DEFAULT_CATALOG_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_SCOPE:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

.field public static final DEFAULT_TYPE:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final applied_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final catalog_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final catalog_object_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x3
    .end annotation
.end field

.field public final metadata:Ljava/util/Map;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        keyAdapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x9
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final percentage:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$Tax$Scope#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$Tax$Type#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 4382
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Tax$ProtoAdapter_Tax;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$LineItem$Tax$ProtoAdapter_Tax;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 4390
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Tax;->DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

    .line 4394
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Tax$Type;->ADDITIVE:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Tax;->DEFAULT_TYPE:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    .line 4398
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;->TAX_SUBTOTAL_PHASE:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Tax;->DEFAULT_CALCULATION_PHASE:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    .line 4400
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;->OTHER_TAX_SCOPE:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Tax;->DEFAULT_SCOPE:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$Type;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;Ljava/util/Map;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem$Tax$Type;",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;",
            ")V"
        }
    .end annotation

    .line 4535
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/orders/model/Order$LineItem$Tax;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$Type;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;Ljava/util/Map;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$Type;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;Ljava/util/Map;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem$Tax$Type;",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 4543
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 4544
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->uid:Ljava/lang/String;

    .line 4545
    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->catalog_object_id:Ljava/lang/String;

    .line 4546
    iput-object p3, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->catalog_object_version:Ljava/lang/Long;

    .line 4547
    iput-object p4, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->name:Ljava/lang/String;

    .line 4548
    iput-object p5, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    .line 4549
    iput-object p6, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->percentage:Ljava/lang/String;

    .line 4550
    iput-object p7, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    const-string p1, "metadata"

    .line 4551
    invoke-static {p1, p8}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->metadata:Ljava/util/Map;

    .line 4552
    iput-object p9, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 4553
    iput-object p10, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 4576
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$LineItem$Tax;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 4577
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$LineItem$Tax;

    .line 4578
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$Tax;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$LineItem$Tax;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Tax;->uid:Ljava/lang/String;

    .line 4579
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->catalog_object_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Tax;->catalog_object_id:Ljava/lang/String;

    .line 4580
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->catalog_object_version:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Tax;->catalog_object_version:Ljava/lang/Long;

    .line 4581
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Tax;->name:Ljava/lang/String;

    .line 4582
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Tax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    .line 4583
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->percentage:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Tax;->percentage:Ljava/lang/String;

    .line 4584
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Tax;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    .line 4585
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->metadata:Ljava/util/Map;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Tax;->metadata:Ljava/util/Map;

    .line 4586
    invoke-interface {v1, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Tax;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 4587
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem$Tax;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    .line 4588
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 4593
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 4595
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$Tax;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 4596
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->uid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4597
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4598
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4599
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4600
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$LineItem$Tax$Type;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4601
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4602
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4603
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->metadata:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4604
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4605
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 4606
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;
    .locals 2

    .line 4558
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;-><init>()V

    .line 4559
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->uid:Ljava/lang/String;

    .line 4560
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->catalog_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->catalog_object_id:Ljava/lang/String;

    .line 4561
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->catalog_object_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->catalog_object_version:Ljava/lang/Long;

    .line 4562
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->name:Ljava/lang/String;

    .line 4563
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    .line 4564
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->percentage:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->percentage:Ljava/lang/String;

    .line 4565
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    .line 4566
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->metadata:Ljava/util/Map;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->metadata:Ljava/util/Map;

    .line 4567
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 4568
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    .line 4569
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$Tax;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 4381
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$Tax;->newBuilder()Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 4613
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4614
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4615
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", catalog_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->catalog_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4616
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", catalog_object_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->catalog_object_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4617
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4618
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    if-eqz v1, :cond_4

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4619
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->percentage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4620
    :cond_5
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    if-eqz v1, :cond_6

    const-string v1, ", calculation_phase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4621
    :cond_6
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->metadata:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, ", metadata=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4622
    :cond_7
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_8

    const-string v1, ", applied_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4623
    :cond_8
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    if-eqz v1, :cond_9

    const-string v1, ", scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Tax{"

    .line 4624
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
