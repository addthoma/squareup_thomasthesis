.class public final Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$LineItem$Modifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$LineItem$Modifier;",
        "Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public catalog_object_id:Ljava/lang/String;

.field public catalog_object_version:Ljava/lang/Long;

.field public name:Ljava/lang/String;

.field public total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4243
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public base_price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;
    .locals 0

    .line 4290
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$LineItem$Modifier;
    .locals 9

    .line 4309
    new-instance v8, Lcom/squareup/orders/model/Order$LineItem$Modifier;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->uid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->catalog_object_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->catalog_object_version:Ljava/lang/Long;

    iget-object v4, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v6, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/orders/model/Order$LineItem$Modifier;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4230
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->build()Lcom/squareup/orders/model/Order$LineItem$Modifier;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;
    .locals 0

    .line 4262
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->catalog_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_object_version(Ljava/lang/Long;)Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;
    .locals 0

    .line 4267
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->catalog_object_version:Ljava/lang/Long;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;
    .locals 0

    .line 4277
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public total_price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;
    .locals 0

    .line 4303
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;
    .locals 0

    .line 4252
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method
