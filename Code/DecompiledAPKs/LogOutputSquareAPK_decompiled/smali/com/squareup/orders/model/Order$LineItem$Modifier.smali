.class public final Lcom/squareup/orders/model/Order$LineItem$Modifier;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$LineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Modifier"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$LineItem$Modifier$ProtoAdapter_Modifier;,
        Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$LineItem$Modifier;",
        "Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$LineItem$Modifier;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CATALOG_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final base_price_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final catalog_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final catalog_object_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x6
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final total_price_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 4080
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Modifier$ProtoAdapter_Modifier;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$LineItem$Modifier$ProtoAdapter_Modifier;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 4088
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;)V
    .locals 8

    .line 4161
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/orders/model/Order$LineItem$Modifier;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 4166
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 4167
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->uid:Ljava/lang/String;

    .line 4168
    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->catalog_object_id:Ljava/lang/String;

    .line 4169
    iput-object p3, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->catalog_object_version:Ljava/lang/Long;

    .line 4170
    iput-object p4, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->name:Ljava/lang/String;

    .line 4171
    iput-object p5, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 4172
    iput-object p6, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 4191
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$LineItem$Modifier;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 4192
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$LineItem$Modifier;

    .line 4193
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$Modifier;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$LineItem$Modifier;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Modifier;->uid:Ljava/lang/String;

    .line 4194
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->catalog_object_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Modifier;->catalog_object_id:Ljava/lang/String;

    .line 4195
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->catalog_object_version:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Modifier;->catalog_object_version:Ljava/lang/Long;

    .line 4196
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Modifier;->name:Ljava/lang/String;

    .line 4197
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 4198
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem$Modifier;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 4199
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 4204
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 4206
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$Modifier;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 4207
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->uid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4208
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4209
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4210
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4211
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4212
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 4213
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;
    .locals 2

    .line 4177
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;-><init>()V

    .line 4178
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->uid:Ljava/lang/String;

    .line 4179
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->catalog_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->catalog_object_id:Ljava/lang/String;

    .line 4180
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->catalog_object_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->catalog_object_version:Ljava/lang/Long;

    .line 4181
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->name:Ljava/lang/String;

    .line 4182
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 4183
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 4184
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$Modifier;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 4079
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$Modifier;->newBuilder()Lcom/squareup/orders/model/Order$LineItem$Modifier$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 4220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4221
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4222
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", catalog_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->catalog_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4223
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", catalog_object_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->catalog_object_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4224
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4225
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", base_price_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4226
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", total_price_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Modifier;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Modifier{"

    .line 4227
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
