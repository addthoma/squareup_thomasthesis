.class final Lcom/squareup/orders/SearchOrdersFulfillmentFilter$ProtoAdapter_SearchOrdersFulfillmentFilter;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SearchOrdersFulfillmentFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/SearchOrdersFulfillmentFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SearchOrdersFulfillmentFilter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/SearchOrdersFulfillmentFilter;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 154
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/SearchOrdersFulfillmentFilter;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 173
    new-instance v0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;-><init>()V

    .line 174
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 175
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 194
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 187
    :cond_0
    :try_start_0
    iget-object v4, v0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->fulfillment_states:Ljava/util/List;

    sget-object v5, Lcom/squareup/orders/model/Order$Fulfillment$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 189
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 179
    :cond_1
    :try_start_1
    iget-object v4, v0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->fulfillment_types:Ljava/util/List;

    sget-object v5, Lcom/squareup/orders/model/Order$FulfillmentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 181
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 198
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 199
    invoke-virtual {v0}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->build()Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 152
    invoke-virtual {p0, p1}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$ProtoAdapter_SearchOrdersFulfillmentFilter;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/SearchOrdersFulfillmentFilter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 166
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_types:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 167
    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_states:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 168
    invoke-virtual {p2}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 152
    check-cast p2, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$ProtoAdapter_SearchOrdersFulfillmentFilter;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/SearchOrdersFulfillmentFilter;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/SearchOrdersFulfillmentFilter;)I
    .locals 4

    .line 159
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_types:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 160
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_states:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 161
    invoke-virtual {p1}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 152
    check-cast p1, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$ProtoAdapter_SearchOrdersFulfillmentFilter;->encodedSize(Lcom/squareup/orders/SearchOrdersFulfillmentFilter;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/SearchOrdersFulfillmentFilter;)Lcom/squareup/orders/SearchOrdersFulfillmentFilter;
    .locals 0

    .line 204
    invoke-virtual {p1}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->newBuilder()Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;

    move-result-object p1

    .line 205
    invoke-virtual {p1}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 206
    invoke-virtual {p1}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->build()Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 152
    check-cast p1, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$ProtoAdapter_SearchOrdersFulfillmentFilter;->redact(Lcom/squareup/orders/SearchOrdersFulfillmentFilter;)Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    move-result-object p1

    return-object p1
.end method
