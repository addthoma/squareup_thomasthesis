.class public Lcom/epson/epos2/printer/HybridPrinterStatusInfo;
.super Ljava/lang/Object;
.source "HybridPrinterStatusInfo.java"


# instance fields
.field private autoRecoverError:I

.field private connection:I

.field private coverOpen:I

.field private drawer:I

.field private errorStatus:I

.field private insertionWaiting:I

.field private online:I

.field private panelSwitch:I

.field private paper:I

.field private paperFeed:I

.field private removalWaiting:I

.field private slipPaper:I

.field private waitOnline:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private setAutoRecoverError(I)V
    .locals 0

    .line 95
    iput p1, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->autoRecoverError:I

    return-void
.end method

.method private setConnection(I)V
    .locals 0

    .line 23
    iput p1, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->connection:I

    return-void
.end method

.method private setCoverOpen(I)V
    .locals 0

    .line 39
    iput p1, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->coverOpen:I

    return-void
.end method

.method private setDrawer(I)V
    .locals 0

    .line 79
    iput p1, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->drawer:I

    return-void
.end method

.method private setErrorStatus(I)V
    .locals 0

    .line 87
    iput p1, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->errorStatus:I

    return-void
.end method

.method private setInsertionWaiting(I)V
    .locals 0

    .line 103
    iput p1, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->insertionWaiting:I

    return-void
.end method

.method private setOnline(I)V
    .locals 0

    .line 31
    iput p1, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->online:I

    return-void
.end method

.method private setPanelSwitch(I)V
    .locals 0

    .line 63
    iput p1, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->panelSwitch:I

    return-void
.end method

.method private setPaper(I)V
    .locals 0

    .line 47
    iput p1, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->paper:I

    return-void
.end method

.method private setPaperFeed(I)V
    .locals 0

    .line 55
    iput p1, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->paperFeed:I

    return-void
.end method

.method private setRemovalWaiting(I)V
    .locals 0

    .line 111
    iput p1, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->removalWaiting:I

    return-void
.end method

.method private setSlipPaper(I)V
    .locals 0

    .line 119
    iput p1, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->slipPaper:I

    return-void
.end method

.method private setWaitOnline(I)V
    .locals 0

    .line 71
    iput p1, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->waitOnline:I

    return-void
.end method


# virtual methods
.method public getAutoRecoverError()I
    .locals 1

    .line 91
    iget v0, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->autoRecoverError:I

    return v0
.end method

.method public getConnection()I
    .locals 1

    .line 19
    iget v0, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->connection:I

    return v0
.end method

.method public getCoverOpen()I
    .locals 1

    .line 35
    iget v0, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->coverOpen:I

    return v0
.end method

.method public getDrawer()I
    .locals 1

    .line 75
    iget v0, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->drawer:I

    return v0
.end method

.method public getErrorStatus()I
    .locals 1

    .line 83
    iget v0, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->errorStatus:I

    return v0
.end method

.method public getInsertionWaiting()I
    .locals 1

    .line 99
    iget v0, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->insertionWaiting:I

    return v0
.end method

.method public getOnline()I
    .locals 1

    .line 27
    iget v0, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->online:I

    return v0
.end method

.method public getPanelSwitch()I
    .locals 1

    .line 59
    iget v0, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->panelSwitch:I

    return v0
.end method

.method public getPaper()I
    .locals 1

    .line 43
    iget v0, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->paper:I

    return v0
.end method

.method public getPaperFeed()I
    .locals 1

    .line 51
    iget v0, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->paperFeed:I

    return v0
.end method

.method public getRemovalWaiting()I
    .locals 1

    .line 107
    iget v0, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->removalWaiting:I

    return v0
.end method

.method public getSlipPaper()I
    .locals 1

    .line 115
    iget v0, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->slipPaper:I

    return v0
.end method

.method public getWaitOnline()I
    .locals 1

    .line 67
    iget v0, p0, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->waitOnline:I

    return v0
.end method
