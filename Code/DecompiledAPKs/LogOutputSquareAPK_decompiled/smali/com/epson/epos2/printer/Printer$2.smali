.class Lcom/epson/epos2/printer/Printer$2;
.super Lcom/epson/epos2/printer/Printer$InnerThread;
.source "Printer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/epos2/printer/Printer;->resetMaintenanceCounter(IILcom/epson/epos2/printer/MaintenanceCounterListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/epos2/printer/Printer;


# direct methods
.method constructor <init>(Lcom/epson/epos2/printer/Printer;Lcom/epson/epos2/printer/Printer;Ljava/lang/Object;JII[I[I)V
    .locals 0

    .line 1225
    iput-object p1, p0, Lcom/epson/epos2/printer/Printer$2;->this$0:Lcom/epson/epos2/printer/Printer;

    invoke-direct/range {p0 .. p9}, Lcom/epson/epos2/printer/Printer$InnerThread;-><init>(Lcom/epson/epos2/printer/Printer;Lcom/epson/epos2/printer/Printer;Ljava/lang/Object;JII[I[I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 1227
    iget-object v0, p0, Lcom/epson/epos2/printer/Printer$2;->mPrn:Lcom/epson/epos2/printer/Printer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/epson/epos2/printer/Printer$2;->mListener:Ljava/lang/Object;

    if-nez v0, :cond_0

    goto :goto_0

    .line 1230
    :cond_0
    iget-object v0, p0, Lcom/epson/epos2/printer/Printer$2;->mPrn:Lcom/epson/epos2/printer/Printer;

    iget-wide v1, p0, Lcom/epson/epos2/printer/Printer$2;->mHandle:J

    iget v3, p0, Lcom/epson/epos2/printer/Printer$2;->mTimeout:I

    iget v4, p0, Lcom/epson/epos2/printer/Printer$2;->mType:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/epson/epos2/printer/Printer;->nativeEpos2ResetMaintenanceCounter(JII)I

    move-result v0

    .line 1231
    iget-object v1, p0, Lcom/epson/epos2/printer/Printer$2;->this$0:Lcom/epson/epos2/printer/Printer;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "code->"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 1232
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "type->"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, p0, Lcom/epson/epos2/printer/Printer$2;->mType:I

    .line 1233
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const-string v3, "onResetMaintenanceCounter"

    .line 1231
    invoke-virtual {v1, v3, v2}, Lcom/epson/epos2/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1234
    iget-object v1, p0, Lcom/epson/epos2/printer/Printer$2;->mListener:Ljava/lang/Object;

    check-cast v1, Lcom/epson/epos2/printer/MaintenanceCounterListener;

    iget v2, p0, Lcom/epson/epos2/printer/Printer$2;->mType:I

    invoke-interface {v1, v0, v2}, Lcom/epson/epos2/printer/MaintenanceCounterListener;->onResetMaintenanceCounter(II)V

    :cond_1
    :goto_0
    return-void
.end method
