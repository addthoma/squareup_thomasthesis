.class public Lcom/epson/epos2/cat/AuthorizeResult;
.super Ljava/lang/Object;
.source "AuthorizeResult.java"


# instance fields
.field private accountNumber:Ljava/lang/String;

.field private approvalCode:Ljava/lang/String;

.field private balance:I

.field private kid:Ljava/lang/String;

.field private paymentCondition:I

.field private settledAmount:I

.field private slipNumber:Ljava/lang/String;

.field private transactionNumber:Ljava/lang/String;

.field private voidSlipNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private setAccountNumber(Ljava/lang/String;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/epson/epos2/cat/AuthorizeResult;->accountNumber:Ljava/lang/String;

    return-void
.end method

.method private setApprovalCode(Ljava/lang/String;)V
    .locals 0

    .line 76
    iput-object p1, p0, Lcom/epson/epos2/cat/AuthorizeResult;->approvalCode:Ljava/lang/String;

    return-void
.end method

.method private setBalance(I)V
    .locals 0

    .line 104
    iput p1, p0, Lcom/epson/epos2/cat/AuthorizeResult;->balance:I

    return-void
.end method

.method private setKid(Ljava/lang/String;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/epson/epos2/cat/AuthorizeResult;->kid:Ljava/lang/String;

    return-void
.end method

.method private setPaymentCondition(I)V
    .locals 0

    .line 90
    iput p1, p0, Lcom/epson/epos2/cat/AuthorizeResult;->paymentCondition:I

    return-void
.end method

.method private setSettledAmount(I)V
    .locals 0

    .line 55
    iput p1, p0, Lcom/epson/epos2/cat/AuthorizeResult;->settledAmount:I

    return-void
.end method

.method private setSlipNumber(Ljava/lang/String;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/epson/epos2/cat/AuthorizeResult;->slipNumber:Ljava/lang/String;

    return-void
.end method

.method private setTransactionNumber(Ljava/lang/String;)V
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/epson/epos2/cat/AuthorizeResult;->transactionNumber:Ljava/lang/String;

    return-void
.end method

.method private setVoidSlipNumber(Ljava/lang/String;)V
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/epson/epos2/cat/AuthorizeResult;->voidSlipNumber:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAccountNumber()Ljava/lang/String;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/epson/epos2/cat/AuthorizeResult;->accountNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getApprovalCode()Ljava/lang/String;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/epson/epos2/cat/AuthorizeResult;->approvalCode:Ljava/lang/String;

    return-object v0
.end method

.method public getBalance()I
    .locals 1

    .line 101
    iget v0, p0, Lcom/epson/epos2/cat/AuthorizeResult;->balance:I

    return v0
.end method

.method public getKid()Ljava/lang/String;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/epson/epos2/cat/AuthorizeResult;->kid:Ljava/lang/String;

    return-object v0
.end method

.method public getPaymentCondition()I
    .locals 1

    .line 87
    iget v0, p0, Lcom/epson/epos2/cat/AuthorizeResult;->paymentCondition:I

    return v0
.end method

.method public getSettledAmount()I
    .locals 1

    .line 52
    iget v0, p0, Lcom/epson/epos2/cat/AuthorizeResult;->settledAmount:I

    return v0
.end method

.method public getSlipNumber()Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/epson/epos2/cat/AuthorizeResult;->slipNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionNumber()Ljava/lang/String;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/epson/epos2/cat/AuthorizeResult;->transactionNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getVoidSlipNumber()Ljava/lang/String;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/epson/epos2/cat/AuthorizeResult;->voidSlipNumber:Ljava/lang/String;

    return-object v0
.end method
