.class public Lcom/epson/epos2/discovery/FilterOption;
.super Ljava/lang/Object;
.source "FilterOption.java"


# instance fields
.field private bondedDevices:I

.field private broadcast:Ljava/lang/String;

.field private deviceModel:I

.field private deviceType:I

.field private epsonFilter:I

.field private portType:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 43
    iput v0, p0, Lcom/epson/epos2/discovery/FilterOption;->portType:I

    const-string v1, "255.255.255.255"

    .line 44
    iput-object v1, p0, Lcom/epson/epos2/discovery/FilterOption;->broadcast:Ljava/lang/String;

    .line 45
    iput v0, p0, Lcom/epson/epos2/discovery/FilterOption;->deviceModel:I

    .line 46
    iput v0, p0, Lcom/epson/epos2/discovery/FilterOption;->epsonFilter:I

    .line 47
    iput v0, p0, Lcom/epson/epos2/discovery/FilterOption;->bondedDevices:I

    return-void
.end method


# virtual methods
.method public getBondedDevices()I
    .locals 1

    .line 92
    iget v0, p0, Lcom/epson/epos2/discovery/FilterOption;->bondedDevices:I

    return v0
.end method

.method public getBroadcast()Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/epson/epos2/discovery/FilterOption;->broadcast:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceModel()I
    .locals 1

    .line 67
    iget v0, p0, Lcom/epson/epos2/discovery/FilterOption;->deviceModel:I

    return v0
.end method

.method public getDeviceType()I
    .locals 1

    .line 83
    iget v0, p0, Lcom/epson/epos2/discovery/FilterOption;->deviceType:I

    return v0
.end method

.method public getEpsonFilter()I
    .locals 1

    .line 75
    iget v0, p0, Lcom/epson/epos2/discovery/FilterOption;->epsonFilter:I

    return v0
.end method

.method public getPortType()I
    .locals 1

    .line 51
    iget v0, p0, Lcom/epson/epos2/discovery/FilterOption;->portType:I

    return v0
.end method

.method public setBondedDevices(I)V
    .locals 0

    .line 96
    iput p1, p0, Lcom/epson/epos2/discovery/FilterOption;->bondedDevices:I

    return-void
.end method

.method public setBroadcast(Ljava/lang/String;)V
    .locals 0

    .line 63
    iput-object p1, p0, Lcom/epson/epos2/discovery/FilterOption;->broadcast:Ljava/lang/String;

    return-void
.end method

.method public setDeviceModel(I)V
    .locals 0

    .line 71
    iput p1, p0, Lcom/epson/epos2/discovery/FilterOption;->deviceModel:I

    return-void
.end method

.method public setDeviceType(I)V
    .locals 0

    .line 87
    iput p1, p0, Lcom/epson/epos2/discovery/FilterOption;->deviceType:I

    return-void
.end method

.method public setEpsonFilter(I)V
    .locals 0

    .line 79
    iput p1, p0, Lcom/epson/epos2/discovery/FilterOption;->epsonFilter:I

    return-void
.end method

.method public setPortType(I)V
    .locals 0

    .line 55
    iput p1, p0, Lcom/epson/epos2/discovery/FilterOption;->portType:I

    return-void
.end method
