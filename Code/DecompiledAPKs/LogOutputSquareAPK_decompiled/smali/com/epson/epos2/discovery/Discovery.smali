.class public Lcom/epson/epos2/discovery/Discovery;
.super Ljava/lang/Object;
.source "Discovery.java"


# static fields
.field public static final FALSE:I = 0x0

.field public static final FILTER_NAME:I = 0x0

.field public static final FILTER_NONE:I = 0x1

.field public static final MODEL_ALL:I = 0x0

.field private static final MODEL_TM_INTELLIGENT:I = 0x2

.field private static final MODEL_TM_PRINTER:I = 0x1

.field private static final NO_EXCEPTION:I = 0x0

.field public static final PORTTYPE_ALL:I = 0x0

.field public static final PORTTYPE_BLUETOOTH:I = 0x2

.field public static final PORTTYPE_TCP:I = 0x1

.field public static final PORTTYPE_USB:I = 0x3

.field public static final TRUE:I = 0x1

.field public static final TYPE_ALL:I = 0x0

.field public static final TYPE_CAT:I = 0x9

.field public static final TYPE_CCHANGER:I = 0x7

.field public static final TYPE_DISPLAY:I = 0x3

.field public static final TYPE_HYBRID_PRINTER:I = 0x2

.field public static final TYPE_KEYBOARD:I = 0x4

.field public static final TYPE_MSR:I = 0xa

.field public static final TYPE_OTHER_PERIPHERAL:I = 0xb

.field public static final TYPE_POS_KEYBOARD:I = 0x8

.field public static final TYPE_PRINTER:I = 0x1

.field public static final TYPE_SCANNER:I = 0x5

.field public static final TYPE_SERIAL:I = 0x6

.field private static mClassOutputLog:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private static mDiscoveryListener:Lcom/epson/epos2/discovery/DiscoveryListener;

.field private static mOutputExceptionMethod:Ljava/lang/reflect/Method;

.field private static mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

.field private static mOutputLogEventMethod:Ljava/lang/reflect/Method;

.field private static mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

.field private static mRunning:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "epos2"

    .line 24
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 25
    invoke-static {}, Lcom/epson/epos2/discovery/Discovery;->initializeOuputLogFunctions()V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x0

    .line 121
    sput-object v0, Lcom/epson/epos2/discovery/Discovery;->mDiscoveryListener:Lcom/epson/epos2/discovery/DiscoveryListener;

    const/4 v0, 0x0

    .line 123
    sput-boolean v0, Lcom/epson/epos2/discovery/Discovery;->mRunning:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static initializeOuputLogFunctions()V
    .locals 8

    :try_start_0
    const-string v0, "com.epson.epos2.OutputLog"

    .line 282
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/epson/epos2/discovery/Discovery;->mClassOutputLog:Ljava/lang/Class;

    .line 284
    sget-object v0, Lcom/epson/epos2/discovery/Discovery;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogCallFunction"

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    const-class v4, [Ljava/lang/Object;

    const/4 v7, 0x2

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/epson/epos2/discovery/Discovery;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 285
    sget-object v0, Lcom/epson/epos2/discovery/Discovery;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 286
    sget-object v0, Lcom/epson/epos2/discovery/Discovery;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogReturnFunction"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v7

    const-class v4, [Ljava/lang/Object;

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/epson/epos2/discovery/Discovery;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 287
    sget-object v0, Lcom/epson/epos2/discovery/Discovery;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 288
    sget-object v0, Lcom/epson/epos2/discovery/Discovery;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputException"

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    const-class v4, Ljava/lang/Exception;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/epson/epos2/discovery/Discovery;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 289
    sget-object v0, Lcom/epson/epos2/discovery/Discovery;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 290
    sget-object v0, Lcom/epson/epos2/discovery/Discovery;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogEvent"

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    aput-object v3, v2, v5

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v6

    const-class v3, [Ljava/lang/Object;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/epson/epos2/discovery/Discovery;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 291
    sget-object v0, Lcom/epson/epos2/discovery/Discovery;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 294
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private static isRunning()Z
    .locals 1

    .line 126
    sget-boolean v0, Lcom/epson/epos2/discovery/Discovery;->mRunning:Z

    return v0
.end method

.method private static final native nativeEpos2DiscoveryStart(Ljava/lang/Object;Lcom/epson/epos2/discovery/FilterOption;)I
.end method

.method private static final native nativeEpos2DiscoveryStop()I
.end method

.method protected static onDiscovery(Lcom/epson/epos2/discovery/DeviceInfo;)V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const-string v3, "onDiscovery"

    .line 258
    invoke-static {v3, v1}, Lcom/epson/epos2/discovery/Discovery;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 260
    sget-object v1, Lcom/epson/epos2/discovery/Discovery;->mDiscoveryListener:Lcom/epson/epos2/discovery/DiscoveryListener;

    if-eqz v1, :cond_0

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Object;

    const-string v4, "deviceType->"

    aput-object v4, v1, v2

    .line 264
    invoke-virtual {p0}, Lcom/epson/epos2/discovery/DeviceInfo;->getDeviceType()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v0

    const/4 v4, 0x2

    const-string v5, "target->"

    aput-object v5, v1, v4

    const/4 v4, 0x3

    .line 265
    invoke-virtual {p0}, Lcom/epson/epos2/discovery/DeviceInfo;->getTarget()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v4, 0x4

    const-string v5, "deviceName->"

    aput-object v5, v1, v4

    const/4 v4, 0x5

    .line 266
    invoke-virtual {p0}, Lcom/epson/epos2/discovery/DeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v4, 0x6

    const-string v5, "ipAddress->"

    aput-object v5, v1, v4

    const/4 v4, 0x7

    .line 267
    invoke-virtual {p0}, Lcom/epson/epos2/discovery/DeviceInfo;->getIpAddress()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    const/16 v4, 0x8

    const-string v5, "macAddress->"

    aput-object v5, v1, v4

    const/16 v4, 0x9

    .line 268
    invoke-virtual {p0}, Lcom/epson/epos2/discovery/DeviceInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    const/16 v4, 0xa

    const-string v5, "bdAddress->"

    aput-object v5, v1, v4

    const/16 v4, 0xb

    .line 269
    invoke-virtual {p0}, Lcom/epson/epos2/discovery/DeviceInfo;->getBdAddress()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    .line 263
    invoke-static {v3, v1}, Lcom/epson/epos2/discovery/Discovery;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 271
    sget-object v1, Lcom/epson/epos2/discovery/Discovery;->mDiscoveryListener:Lcom/epson/epos2/discovery/DiscoveryListener;

    invoke-interface {v1, p0}, Lcom/epson/epos2/discovery/DiscoveryListener;->onDiscovery(Lcom/epson/epos2/discovery/DeviceInfo;)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    aput-object p0, v0, v2

    .line 275
    invoke-static {v3, v2, v0}, Lcom/epson/epos2/discovery/Discovery;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private static outputException(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 4

    .line 318
    :try_start_0
    sget-object v0, Lcom/epson/epos2/discovery/Discovery;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    sget-object v1, Lcom/epson/epos2/discovery/Discovery;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 p0, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, p0

    const/4 p0, 0x2

    aput-object p1, v2, p0

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private static varargs outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4

    .line 300
    :try_start_0
    sget-object v0, Lcom/epson/epos2/discovery/Discovery;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    sget-object v1, Lcom/epson/epos2/discovery/Discovery;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 p0, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, p0

    const/4 p0, 0x2

    aput-object p1, v2, p0

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private static varargs outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4

    .line 327
    :try_start_0
    sget-object v0, Lcom/epson/epos2/discovery/Discovery;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    sget-object v1, Lcom/epson/epos2/discovery/Discovery;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 p0, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, p0

    const/4 p0, 0x2

    aput-object p1, v2, p0

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private static varargs outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    .locals 4

    .line 309
    :try_start_0
    sget-object v0, Lcom/epson/epos2/discovery/Discovery;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    sget-object v1, Lcom/epson/epos2/discovery/Discovery;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 p0, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, p0

    const/4 p0, 0x2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, p0

    const/4 p0, 0x3

    aput-object p2, v2, p0

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private static setRunning(Z)V
    .locals 0

    .line 130
    sput-boolean p0, Lcom/epson/epos2/discovery/Discovery;->mRunning:Z

    return-void
.end method

.method public static declared-synchronized start(Landroid/content/Context;Lcom/epson/epos2/discovery/FilterOption;Lcom/epson/epos2/discovery/DiscoveryListener;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const-class v0, Lcom/epson/epos2/discovery/Discovery;

    monitor-enter v0

    :try_start_0
    const-string v1, "start"

    const/16 v2, 0x8

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    .line 189
    invoke-virtual {p1}, Lcom/epson/epos2/discovery/FilterOption;->getPortType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x1

    aput-object v5, v3, v6

    invoke-virtual {p1}, Lcom/epson/epos2/discovery/FilterOption;->getBroadcast()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x2

    aput-object v5, v3, v7

    invoke-virtual {p1}, Lcom/epson/epos2/discovery/FilterOption;->getDeviceModel()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v8, 0x3

    aput-object v5, v3, v8

    invoke-virtual {p1}, Lcom/epson/epos2/discovery/FilterOption;->getEpsonFilter()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v9, 0x4

    aput-object v5, v3, v9

    invoke-virtual {p1}, Lcom/epson/epos2/discovery/FilterOption;->getDeviceType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v10, 0x5

    aput-object v5, v3, v10

    invoke-virtual {p1}, Lcom/epson/epos2/discovery/FilterOption;->getBondedDevices()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v11, 0x6

    aput-object v5, v3, v11

    const/4 v5, 0x7

    aput-object p2, v3, v5

    invoke-static {v1, v3}, Lcom/epson/epos2/discovery/Discovery;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    :try_start_1
    sget-boolean v1, Lcom/epson/epos2/discovery/Discovery;->mRunning:Z

    if-eq v1, v6, :cond_2

    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 201
    sput-object p2, Lcom/epson/epos2/discovery/Discovery;->mDiscoveryListener:Lcom/epson/epos2/discovery/DiscoveryListener;

    .line 203
    invoke-static {p0, p1}, Lcom/epson/epos2/discovery/Discovery;->nativeEpos2DiscoveryStart(Ljava/lang/Object;Lcom/epson/epos2/discovery/FilterOption;)I

    move-result v1

    if-nez v1, :cond_0

    .line 208
    sput-boolean v6, Lcom/epson/epos2/discovery/Discovery;->mRunning:Z
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    const-string v1, "start"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v4

    .line 216
    invoke-virtual {p1}, Lcom/epson/epos2/discovery/FilterOption;->getPortType()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v2, v6

    invoke-virtual {p1}, Lcom/epson/epos2/discovery/FilterOption;->getBroadcast()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v2, v7

    invoke-virtual {p1}, Lcom/epson/epos2/discovery/FilterOption;->getDeviceModel()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v2, v8

    invoke-virtual {p1}, Lcom/epson/epos2/discovery/FilterOption;->getEpsonFilter()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v2, v9

    invoke-virtual {p1}, Lcom/epson/epos2/discovery/FilterOption;->getDeviceType()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v2, v10

    invoke-virtual {p1}, Lcom/epson/epos2/discovery/FilterOption;->getBondedDevices()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v2, v11

    aput-object p2, v2, v5

    invoke-static {v1, v4, v2}, Lcom/epson/epos2/discovery/Discovery;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 217
    monitor-exit v0

    return-void

    :cond_0
    const/4 p0, 0x0

    .line 205
    :try_start_3
    sput-object p0, Lcom/epson/epos2/discovery/Discovery;->mDiscoveryListener:Lcom/epson/epos2/discovery/DiscoveryListener;

    .line 206
    new-instance p0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {p0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw p0

    .line 199
    :cond_1
    new-instance p0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {p0, v6}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw p0

    .line 195
    :cond_2
    new-instance p0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {p0, v10}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw p0
    :try_end_3
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_0
    move-exception p0

    :try_start_4
    const-string p1, "start"

    .line 211
    invoke-static {p1, p0}, Lcom/epson/epos2/discovery/Discovery;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    const-string p1, "start"

    .line 212
    invoke-virtual {p0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result p2

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p1, p2, v1}, Lcom/epson/epos2/discovery/Discovery;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 213
    throw p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized stop()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const-class v0, Lcom/epson/epos2/discovery/Discovery;

    monitor-enter v0

    :try_start_0
    const-string v1, "stop"

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/Object;

    .line 231
    invoke-static {v1, v3}, Lcom/epson/epos2/discovery/Discovery;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    :try_start_1
    sget-boolean v1, Lcom/epson/epos2/discovery/Discovery;->mRunning:Z

    if-eqz v1, :cond_1

    .line 239
    invoke-static {}, Lcom/epson/epos2/discovery/Discovery;->nativeEpos2DiscoveryStop()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 243
    sput-object v1, Lcom/epson/epos2/discovery/Discovery;->mDiscoveryListener:Lcom/epson/epos2/discovery/DiscoveryListener;

    .line 244
    sput-boolean v2, Lcom/epson/epos2/discovery/Discovery;->mRunning:Z
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    const-string v1, "stop"

    new-array v3, v2, [Ljava/lang/Object;

    .line 252
    invoke-static {v1, v2, v3}, Lcom/epson/epos2/discovery/Discovery;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 253
    monitor-exit v0

    return-void

    .line 241
    :cond_0
    :try_start_3
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3

    .line 237
    :cond_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    const/4 v3, 0x5

    invoke-direct {v1, v3}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_3
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_0
    move-exception v1

    :try_start_4
    const-string v3, "stop"

    .line 247
    invoke-static {v3, v1}, Lcom/epson/epos2/discovery/Discovery;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    const-string v3, "stop"

    .line 248
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v4, v2}, Lcom/epson/epos2/discovery/Discovery;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 249
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
