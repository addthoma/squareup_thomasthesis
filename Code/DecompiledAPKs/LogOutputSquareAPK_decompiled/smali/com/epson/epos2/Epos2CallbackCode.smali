.class public Lcom/epson/epos2/Epos2CallbackCode;
.super Ljava/lang/Object;
.source "Epos2CallbackCode.java"


# static fields
.field public static final CODE_CANCELED:I = 0x12

.field public static final CODE_ERR_AUTORECOVER:I = 0x3

.field public static final CODE_ERR_BATTERY_LOW:I = 0xf

.field public static final CODE_ERR_CANCEL_FAILED:I = 0x1b

.field public static final CODE_ERR_CONNECT:I = 0x23

.field public static final CODE_ERR_COVER_OPEN:I = 0x4

.field public static final CODE_ERR_CUTTER:I = 0x5

.field public static final CODE_ERR_DEVICE_BUSY:I = 0x21

.field public static final CODE_ERR_DISCONNECT:I = 0x24

.field public static final CODE_ERR_EMPTY:I = 0x7

.field public static final CODE_ERR_FAILURE:I = 0xff

.field public static final CODE_ERR_ILLEGAL:I = 0x1e

.field public static final CODE_ERR_ILLEGAL_LENGTH:I = 0x14

.field public static final CODE_ERR_INSERTED:I = 0x1f

.field public static final CODE_ERR_INVALID_WINDOW:I = 0xb

.field public static final CODE_ERR_IN_USE:I = 0x22

.field public static final CODE_ERR_JOB_NOT_FOUND:I = 0xc

.field public static final CODE_ERR_MECHANICAL:I = 0x6

.field public static final CODE_ERR_MEMORY:I = 0x25

.field public static final CODE_ERR_NOISE_DETECTED:I = 0x18

.field public static final CODE_ERR_NOT_FOUND:I = 0x2

.field public static final CODE_ERR_NO_MAGNETIC_DATA:I = 0x15

.field public static final CODE_ERR_NO_MICR_DATA:I = 0x13

.field public static final CODE_ERR_PAPER_JAM:I = 0x19

.field public static final CODE_ERR_PAPER_PULLED_OUT:I = 0x1a

.field public static final CODE_ERR_PAPER_TYPE:I = 0x1c

.field public static final CODE_ERR_PARAM:I = 0x27

.field public static final CODE_ERR_PORT:I = 0xa

.field public static final CODE_ERR_PROCESSING:I = 0x26

.field public static final CODE_ERR_READ:I = 0x17

.field public static final CODE_ERR_RECOGNITION:I = 0x16

.field public static final CODE_ERR_REQUEST_ENTITY_TOO_LARGE:I = 0x11

.field public static final CODE_ERR_SPOOLER:I = 0xe

.field public static final CODE_ERR_SYSTEM:I = 0x9

.field public static final CODE_ERR_TIMEOUT:I = 0x1

.field public static final CODE_ERR_TOO_MANY_REQUESTS:I = 0x10

.field public static final CODE_ERR_UNRECOVERABLE:I = 0x8

.field public static final CODE_ERR_WAIT_INSERTION:I = 0x1d

.field public static final CODE_ERR_WAIT_REMOVAL:I = 0x20

.field public static final CODE_PRINTING:I = 0xd

.field public static final CODE_SUCCESS:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
