.class public Lcom/epson/epsonio/usb/UsbConnecter;
.super Ljava/lang/Object;
.source "UsbConnecter.java"


# static fields
.field private static final M_INVALID_HANDLE_INDEX:I = -0x1

.field private static final USB_ENDPOINT_IN:I = 0x80

.field private static final USB_ENDPOINT_OUT:I = 0x0

.field private static final USB_FLOW_CONTROL_DISABLE:I = 0x2

.field private static final USB_FLOW_CONTROL_ENABLE:I = 0x0

.field private static final USB_OFFLINE_STATUS_OFFLINE:I = 0x1

.field private static final USB_RECIP_INTERFACE:I = 0x1

.field private static final USB_REQ_CLEAR_FEATURE:I = 0x1

.field private static final USB_REQ_SET_FEATURE:I = 0x3

.field private static final USB_SUPPORT_FLOW_CONTROL:I = 0x2

.field private static final USB_TYPE_VENDOR:I = 0x40

.field private static final VSR_CLEAR_TM_COUNT:I = 0x11

.field private static final VSR_GET_DM_STATUS:I = 0x3

.field private static final VSR_GET_TM_COUNT:I = 0x12

.field private static final VSR_GET_TM_STATUS:I = 0x1

.field private static final VSR_GET_UB_CONFIGURATION:I = 0x0

.field private static final VSR_SET_TM_STATUS:I = 0x2


# instance fields
.field private mConnection:Landroid/hardware/usb/UsbDeviceConnection;

.field private mHandle:I

.field private mUsbDevice:Landroid/hardware/usb/UsbDevice;

.field private mUsbInterface:Landroid/hardware/usb/UsbInterface;

.field private mUsbReceiveEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private mUsbSendEndpoint:Landroid/hardware/usb/UsbEndpoint;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 61
    iput v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mHandle:I

    const/4 v0, 0x0

    .line 62
    iput-object v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    .line 63
    iput-object v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mConnection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 64
    iput-object v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbInterface:Landroid/hardware/usb/UsbInterface;

    .line 65
    iput-object v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbSendEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 66
    iput-object v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbReceiveEndpoint:Landroid/hardware/usb/UsbEndpoint;

    return-void
.end method


# virtual methods
.method public balkRead([BI[II)I
    .locals 3

    if-nez p3, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    .line 200
    aput v0, p3, v0

    .line 202
    iget-object v1, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mConnection:Landroid/hardware/usb/UsbDeviceConnection;

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbReceiveEndpoint:Landroid/hardware/usb/UsbEndpoint;

    if-nez v2, :cond_1

    goto :goto_0

    .line 208
    :cond_1
    invoke-virtual {v1, v2, p1, p2, p4}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result p1

    if-ltz p1, :cond_2

    .line 213
    aput p1, p3, v0

    return v0

    :cond_2
    const/16 p1, 0xff

    return p1

    :cond_3
    :goto_0
    const/4 p1, 0x2

    return p1
.end method

.method public balkWrite([BI[II)I
    .locals 6

    const/4 v0, 0x1

    if-nez p3, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    .line 165
    aput v1, p3, v1

    .line 167
    iget-object v2, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mConnection:Landroid/hardware/usb/UsbDeviceConnection;

    const/4 v3, 0x2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbSendEndpoint:Landroid/hardware/usb/UsbEndpoint;

    if-nez v2, :cond_1

    goto :goto_0

    :cond_1
    new-array v2, v3, [B

    .line 173
    invoke-virtual {p0, v0, v2}, Lcom/epson/epsonio/usb/UsbConnecter;->ioctrlRev(I[B)I

    const/16 v3, 0x11

    .line 174
    invoke-virtual {p0, v3, v1, v1}, Lcom/epson/epsonio/usb/UsbConnecter;->ioctrlSend(III)I

    const/16 v3, 0x12

    .line 175
    invoke-virtual {p0, v3, v2}, Lcom/epson/epsonio/usb/UsbConnecter;->ioctrlRev(I[B)I

    .line 177
    iget-object v4, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v5, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbSendEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {v4, v5, p1, p2, p4}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result p1

    if-gez p1, :cond_2

    const/4 p1, 0x3

    .line 183
    invoke-virtual {p0, p1, v1, v1}, Lcom/epson/epsonio/usb/UsbConnecter;->ioctrlSend(III)I

    .line 184
    invoke-virtual {p0, v0, v1, v1}, Lcom/epson/epsonio/usb/UsbConnecter;->ioctrlSend(III)I

    .line 186
    :cond_2
    invoke-virtual {p0, v3, v2}, Lcom/epson/epsonio/usb/UsbConnecter;->ioctrlRev(I[B)I

    .line 188
    aget-byte p1, v2, v1

    and-int/lit16 p1, p1, 0xff

    aget-byte p2, v2, v0

    and-int/lit16 p2, p2, 0xff

    shl-int/lit8 p2, p2, 0x8

    or-int/2addr p1, p2

    aput p1, p3, v1

    return v1

    :cond_3
    :goto_0
    return v3
.end method

.method public checkDevice(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2

    .line 333
    iget-object v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    if-nez v0, :cond_0

    return-object v1

    .line 337
    :cond_0
    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    .line 338
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_1
    return-object v1
.end method

.method public checkHandle(I)Ljava/lang/Boolean;
    .locals 3

    .line 322
    iget v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mHandle:I

    const/4 v1, 0x0

    const/4 v2, -0x1

    if-ne v2, v0, :cond_0

    .line 323
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_0
    if-ne v0, p1, :cond_1

    const/4 v1, 0x1

    .line 326
    :cond_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public close()I
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mConnection:Landroid/hardware/usb/UsbDeviceConnection;

    if-nez v0, :cond_0

    const/4 v0, 0x6

    return v0

    .line 137
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Landroid/hardware/usb/UsbDeviceConnection;->close()V

    .line 138
    iget-object v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0, v1}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    const/4 v0, -0x1

    .line 140
    iput v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mHandle:I

    const/4 v0, 0x0

    .line 142
    iput-object v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mConnection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 143
    iput-object v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    .line 144
    iput-object v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mConnection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 145
    iput-object v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbInterface:Landroid/hardware/usb/UsbInterface;

    .line 146
    iput-object v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbSendEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 147
    iput-object v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbReceiveEndpoint:Landroid/hardware/usb/UsbEndpoint;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    return v0

    :catch_0
    const/16 v0, 0xff

    return v0
.end method

.method public connect(ILandroid/hardware/usb/UsbManager;Landroid/hardware/usb/UsbDevice;)I
    .locals 5

    const/16 v0, 0xff

    if-nez p3, :cond_0

    return v0

    .line 76
    :cond_0
    iput-object p3, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    .line 80
    :try_start_0
    iget-object p3, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    const/4 v1, 0x0

    invoke-virtual {p3, v1}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object p3

    iput-object p3, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbInterface:Landroid/hardware/usb/UsbInterface;

    .line 81
    iget-object p3, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbInterface:Landroid/hardware/usb/UsbInterface;

    if-nez p3, :cond_1

    return v0

    .line 85
    :cond_1
    iget-object p3, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {p3}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result p3

    const/4 v2, 0x0

    .line 86
    iput-object v2, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbSendEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 87
    iput-object v2, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbReceiveEndpoint:Landroid/hardware/usb/UsbEndpoint;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p3, :cond_4

    .line 91
    iget-object v3, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v3, v2}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v3

    invoke-virtual {v3}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v3

    if-nez v3, :cond_2

    .line 94
    iget-object v3, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbSendEndpoint:Landroid/hardware/usb/UsbEndpoint;

    if-nez v3, :cond_3

    .line 95
    iget-object v3, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v3, v2}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v3

    iput-object v3, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbSendEndpoint:Landroid/hardware/usb/UsbEndpoint;

    goto :goto_1

    :cond_2
    const/16 v4, 0x80

    if-ne v4, v3, :cond_3

    .line 100
    iget-object v3, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbReceiveEndpoint:Landroid/hardware/usb/UsbEndpoint;

    if-nez v3, :cond_3

    .line 101
    iget-object v3, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v3, v2}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v3

    iput-object v3, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbReceiveEndpoint:Landroid/hardware/usb/UsbEndpoint;

    :cond_3
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 111
    :cond_4
    iget-object p3, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {p2, p3}, Landroid/hardware/usb/UsbManager;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object p2

    iput-object p2, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mConnection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 112
    iget-object p2, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mConnection:Landroid/hardware/usb/UsbDeviceConnection;

    if-eqz p2, :cond_5

    .line 113
    iget-object p2, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object p3, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbInterface:Landroid/hardware/usb/UsbInterface;

    const/4 v2, 0x1

    invoke-virtual {p2, p3, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result p2

    if-nez p2, :cond_5

    return v0

    .line 118
    :cond_5
    iput p1, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mHandle:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    :catch_0
    return v0
.end method

.method public enableFlowControl(I)I
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-ne v0, p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    :goto_0
    new-array v3, v1, [B

    .line 233
    invoke-virtual {p0, v2, v3}, Lcom/epson/epsonio/usb/UsbConnecter;->ioctrlRev(I[B)I

    move-result v4

    if-eqz v4, :cond_1

    return v4

    .line 240
    :cond_1
    aget-byte v4, v3, v2

    and-int/2addr v4, v1

    if-nez v4, :cond_2

    return v2

    .line 248
    :cond_2
    :goto_1
    invoke-virtual {p0, v1, p1, v2}, Lcom/epson/epsonio/usb/UsbConnecter;->ioctrlSend(III)I

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_2

    .line 254
    :cond_3
    invoke-virtual {p0, v0, v3}, Lcom/epson/epsonio/usb/UsbConnecter;->ioctrlRev(I[B)I

    move-result v4

    if-eqz v4, :cond_4

    goto :goto_2

    .line 260
    :cond_4
    aget-byte v4, v3, v2

    and-int/2addr v4, v1

    if-ne v4, p1, :cond_5

    const/4 v4, 0x0

    :goto_2
    return v4

    :cond_5
    const-wide/16 v4, 0x64

    .line 266
    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    nop

    goto :goto_1
.end method

.method public getOnlineDMStatus([I)I
    .locals 1

    const/4 v0, 0x3

    .line 283
    invoke-virtual {p0, p1, v0}, Lcom/epson/epsonio/usb/UsbConnecter;->getOnlineStatus([II)I

    move-result p1

    return p1
.end method

.method protected getOnlineStatus([II)I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [B

    const/4 v1, 0x1

    if-eqz p1, :cond_3

    .line 293
    array-length v2, p1

    if-le v1, v2, :cond_0

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    aput-byte v2, v0, v2

    .line 300
    invoke-virtual {p0, p2, v0}, Lcom/epson/epsonio/usb/UsbConnecter;->ioctrlRev(I[B)I

    move-result p2

    if-eqz p2, :cond_1

    const/16 p1, 0xff

    return p1

    .line 308
    :cond_1
    aget-byte p2, v0, v2

    and-int/2addr p2, v1

    if-nez p2, :cond_2

    .line 309
    aput v1, p1, v2

    goto :goto_0

    .line 312
    :cond_2
    aput v2, p1, v2

    :goto_0
    return v2

    :cond_3
    :goto_1
    return v1
.end method

.method public getOnlineTMStatus([I)I
    .locals 1

    const/4 v0, 0x1

    .line 280
    invoke-virtual {p0, p1, v0}, Lcom/epson/epsonio/usb/UsbConnecter;->getOnlineStatus([II)I

    move-result p1

    return p1
.end method

.method protected ioctrlRev(I[B)I
    .locals 12

    .line 348
    iget-object v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mConnection:Landroid/hardware/usb/UsbDeviceConnection;

    const/16 v1, 0xff

    if-nez v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x2

    if-ge v2, v3, :cond_2

    .line 354
    iget-object v4, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mConnection:Landroid/hardware/usb/UsbDeviceConnection;

    const/16 v5, 0xc1

    const/4 v7, 0x0

    iget-object v3, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbInterface:Landroid/hardware/usb/UsbInterface;

    .line 357
    invoke-virtual {v3}, Landroid/hardware/usb/UsbInterface;->getId()I

    move-result v8

    array-length v10, p2

    const/16 v11, 0x64

    move v6, p1

    move-object v9, p2

    .line 354
    invoke-virtual/range {v4 .. v11}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result v3

    if-ltz v3, :cond_1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/16 v0, 0xff

    :cond_2
    return v0
.end method

.method protected ioctrlSend(III)I
    .locals 9

    .line 378
    iget-object v0, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mConnection:Landroid/hardware/usb/UsbDeviceConnection;

    const/16 v8, 0xff

    if-nez v0, :cond_0

    return v8

    :cond_0
    const/16 v1, 0x41

    .line 383
    iget-object v2, p0, Lcom/epson/epsonio/usb/UsbConnecter;->mUsbInterface:Landroid/hardware/usb/UsbInterface;

    .line 386
    invoke-virtual {v2}, Landroid/hardware/usb/UsbInterface;->getId()I

    move-result v4

    const/4 v5, 0x0

    const/16 v7, 0x64

    move v2, p1

    move v3, p2

    move v6, p3

    .line 383
    invoke-virtual/range {v0 .. v7}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result p1

    if-ltz p1, :cond_1

    const/4 v8, 0x0

    :cond_1
    return v8
.end method
