.class public Lcom/epson/epsonio/SupportUsb;
.super Ljava/lang/Object;
.source "SupportUsb.java"


# static fields
.field static bCheckusb:Z = true


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "android.hardware.usb.UsbDevice"

    .line 24
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    .line 27
    sput-boolean v0, Lcom/epson/epsonio/SupportUsb;->bCheckusb:Z

    :goto_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getResult([II)[Lcom/epson/epsonio/DeviceInfo;
    .locals 1

    .line 57
    sget-boolean v0, Lcom/epson/epsonio/SupportUsb;->bCheckusb:Z

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 61
    :cond_0
    invoke-static {p0, p1}, Lcom/epson/epsonio/usb/DevUsb;->getResult([II)[Lcom/epson/epsonio/DeviceInfo;

    move-result-object p0

    return-object p0
.end method

.method public static isSupport()Ljava/lang/Boolean;
    .locals 1

    .line 35
    sget-boolean v0, Lcom/epson/epsonio/SupportUsb;->bCheckusb:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static start(Landroid/content/Context;ILjava/lang/String;)I
    .locals 1

    .line 42
    sget-boolean v0, Lcom/epson/epsonio/SupportUsb;->bCheckusb:Z

    if-nez v0, :cond_0

    const/4 p0, 0x6

    return p0

    .line 46
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/epson/epsonio/usb/DevUsb;->start(Landroid/content/Context;ILjava/lang/String;)I

    move-result p0

    return p0
.end method

.method public static stop()I
    .locals 1

    .line 68
    sget-boolean v0, Lcom/epson/epsonio/SupportUsb;->bCheckusb:Z

    if-nez v0, :cond_0

    const/4 v0, 0x6

    return v0

    .line 72
    :cond_0
    invoke-static {}, Lcom/epson/epsonio/usb/DevUsb;->stop()I

    move-result v0

    return v0
.end method
