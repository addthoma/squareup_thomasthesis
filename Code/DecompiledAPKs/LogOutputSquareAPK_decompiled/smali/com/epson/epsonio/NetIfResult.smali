.class public Lcom/epson/epsonio/NetIfResult;
.super Ljava/lang/Object;
.source "NetIfResult.java"


# instance fields
.field public ipAddress:[B

.field public macAddress:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method NetIfgetIpAddress()[B
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/epson/epsonio/NetIfResult;->ipAddress:[B

    return-object v0
.end method

.method NetIfgetMacAddress()[B
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/epson/epsonio/NetIfResult;->macAddress:[B

    return-object v0
.end method

.method NetIfsetIpAddress([B)V
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/epson/epsonio/NetIfResult;->ipAddress:[B

    return-void
.end method

.method NetIfsetMacAddress([B)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/epson/epsonio/NetIfResult;->macAddress:[B

    return-void
.end method
