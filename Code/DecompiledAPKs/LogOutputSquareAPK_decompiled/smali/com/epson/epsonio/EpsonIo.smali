.class public Lcom/epson/epsonio/EpsonIo;
.super Ljava/lang/Object;
.source "EpsonIo.java"


# static fields
.field private static final M_CLOSE_HANDLE:J


# instance fields
.field private mHandle:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    :try_start_0
    const-string v0, "epos2"

    .line 25
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "epsonio"

    .line 29
    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_2

    :catch_1
    :goto_0
    return-void

    .line 32
    :catch_2
    throw v0
.end method

.method public constructor <init>()V
    .locals 2

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 44
    iput-wide v0, p0, Lcom/epson/epsonio/EpsonIo;->mHandle:J

    return-void
.end method

.method private native closeDevice(J)I
.end method

.method private native eposSetAVDInfo()V
.end method

.method private native openDevice([JILjava/lang/String;Ljava/lang/String;Landroid/content/Context;J)I
.end method

.method private native readData(J[BIII[I)I
.end method

.method private native writeData(J[BIII[I)I
.end method


# virtual methods
.method public close()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epsonio/EpsonIoException;
        }
    .end annotation

    .line 119
    iget-wide v0, p0, Lcom/epson/epsonio/EpsonIo;->mHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v2, v0

    if-eqz v4, :cond_1

    .line 125
    :try_start_0
    invoke-direct {p0, v0, v1}, Lcom/epson/epsonio/EpsonIo;->closeDevice(J)I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    .line 137
    iput-wide v2, p0, Lcom/epson/epsonio/EpsonIo;->mHandle:J

    return-void

    .line 134
    :cond_0
    new-instance v1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {v1, v0}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw v1

    :catch_0
    move-exception v0

    .line 128
    new-instance v1, Lcom/epson/epsonio/EpsonIoException;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v0}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 v0, 0xff

    .line 129
    invoke-virtual {v1, v0}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 130
    throw v1

    .line 121
    :cond_1
    new-instance v0, Lcom/epson/epsonio/EpsonIoException;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw v0
.end method

.method protected finalize()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const-wide/16 v0, 0x0

    .line 48
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    iget-wide v2, p0, Lcom/epson/epsonio/EpsonIo;->mHandle:J

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    .line 52
    invoke-direct {p0, v2, v3}, Lcom/epson/epsonio/EpsonIo;->closeDevice(J)I

    .line 53
    iput-wide v0, p0, Lcom/epson/epsonio/EpsonIo;->mHandle:J

    :cond_0
    return-void

    :catchall_0
    move-exception v2

    .line 51
    iget-wide v3, p0, Lcom/epson/epsonio/EpsonIo;->mHandle:J

    cmp-long v5, v0, v3

    if-eqz v5, :cond_1

    .line 52
    invoke-direct {p0, v3, v4}, Lcom/epson/epsonio/EpsonIo;->closeDevice(J)I

    .line 53
    iput-wide v0, p0, Lcom/epson/epsonio/EpsonIo;->mHandle:J

    .line 55
    :cond_1
    throw v2
.end method

.method public open(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epsonio/EpsonIoException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 62
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/epson/epsonio/EpsonIo;->open(ILjava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    return-void
.end method

.method public open(ILjava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epsonio/EpsonIoException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v9, v0, [J

    .line 74
    iget-wide v1, p0, Lcom/epson/epsonio/EpsonIo;->mHandle:J

    const/4 v3, 0x6

    const-wide/16 v4, 0x0

    cmp-long v6, v4, v1

    if-nez v6, :cond_6

    const/16 v1, 0x103

    if-ne p1, v1, :cond_1

    .line 80
    invoke-static {}, Lcom/epson/epsonio/SupportUsb;->isSupport()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 81
    :cond_0
    new-instance p1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p1, v3}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p1

    .line 86
    :cond_1
    :goto_0
    sget-object v1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v2, "sdk"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v10, 0x0

    if-nez v1, :cond_3

    sget-object v1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v2, "google_sdk"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v2, "sdk_phone_armv7"

    .line 87
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v2, "sdk_google_phone_armv7"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :cond_3
    :goto_1
    if-eqz v0, :cond_4

    .line 89
    invoke-direct {p0}, Lcom/epson/epsonio/EpsonIo;->eposSetAVDInfo()V

    :cond_4
    const-wide/16 v7, 0x3a98

    move-object v1, p0

    move-object v2, v9

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    .line 99
    :try_start_0
    invoke-direct/range {v1 .. v8}, Lcom/epson/epsonio/EpsonIo;->openDevice([JILjava/lang/String;Ljava/lang/String;Landroid/content/Context;J)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_5

    .line 113
    aget-wide p1, v9, v10

    iput-wide p1, p0, Lcom/epson/epsonio/EpsonIo;->mHandle:J

    return-void

    .line 110
    :cond_5
    new-instance p2, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p2, p1}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p2

    :catch_0
    move-exception p1

    .line 104
    new-instance p2, Lcom/epson/epsonio/EpsonIoException;

    const/4 p3, 0x0

    invoke-direct {p2, p3, p1}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 p1, 0xff

    .line 105
    invoke-virtual {p2, p1}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 106
    throw p2

    .line 76
    :cond_6
    new-instance p1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p1, v3}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p1
.end method

.method public read([BIII)I
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epsonio/EpsonIoException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v9, v0, [I

    const/4 v10, 0x0

    aput v10, v9, v10

    .line 194
    iget-wide v2, p0, Lcom/epson/epsonio/EpsonIo;->mHandle:J

    const-wide/16 v4, 0x0

    cmp-long v1, v4, v2

    if-eqz v1, :cond_6

    if-eqz p1, :cond_5

    if-ltz p2, :cond_4

    if-ltz p3, :cond_3

    .line 211
    array-length v1, p1

    add-int v4, p2, p3

    if-lt v1, v4, :cond_2

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    move v6, p3

    move v7, p4

    move-object v8, v9

    .line 218
    :try_start_0
    invoke-direct/range {v1 .. v8}, Lcom/epson/epsonio/EpsonIo;->readData(J[BIII[I)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_1

    const/4 p2, 0x4

    if-ne p2, p1, :cond_0

    goto :goto_0

    .line 231
    :cond_0
    new-instance p2, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p2, p1}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p2

    .line 235
    :cond_1
    :goto_0
    aget p1, v9, v10

    return p1

    :catch_0
    move-exception p1

    .line 221
    new-instance p2, Lcom/epson/epsonio/EpsonIoException;

    const/4 p3, 0x0

    invoke-direct {p2, p3, p1}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 p1, 0xff

    .line 222
    invoke-virtual {p2, p1}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 223
    throw p2

    .line 212
    :cond_2
    new-instance p1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p1, v0}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p1

    .line 208
    :cond_3
    new-instance p1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p1, v0}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p1

    .line 204
    :cond_4
    new-instance p1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p1, v0}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p1

    .line 200
    :cond_5
    new-instance p1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p1, v0}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p1

    .line 196
    :cond_6
    new-instance p1, Lcom/epson/epsonio/EpsonIoException;

    const/4 p2, 0x6

    invoke-direct {p1, p2}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p1
.end method

.method public write([BIII)I
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epsonio/EpsonIoException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v9, v0, [I

    const/4 v10, 0x0

    aput v10, v9, v10

    .line 145
    iget-wide v2, p0, Lcom/epson/epsonio/EpsonIo;->mHandle:J

    const-wide/16 v4, 0x0

    cmp-long v1, v4, v2

    if-eqz v1, :cond_6

    if-eqz p1, :cond_5

    if-ltz p2, :cond_4

    if-ltz p3, :cond_3

    .line 162
    array-length v1, p1

    add-int v4, p2, p3

    if-lt v1, v4, :cond_2

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    move v6, p3

    move v7, p4

    move-object v8, v9

    .line 169
    :try_start_0
    invoke-direct/range {v1 .. v8}, Lcom/epson/epsonio/EpsonIo;->writeData(J[BIII[I)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_1

    const/4 p2, 0x4

    if-ne p2, p1, :cond_0

    goto :goto_0

    .line 182
    :cond_0
    new-instance p2, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p2, p1}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p2

    .line 186
    :cond_1
    :goto_0
    aget p1, v9, v10

    return p1

    :catch_0
    move-exception p1

    .line 172
    new-instance p2, Lcom/epson/epsonio/EpsonIoException;

    const/4 p3, 0x0

    invoke-direct {p2, p3, p1}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 p1, 0xff

    .line 173
    invoke-virtual {p2, p1}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 174
    throw p2

    .line 163
    :cond_2
    new-instance p1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p1, v0}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p1

    .line 159
    :cond_3
    new-instance p1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p1, v0}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p1

    .line 155
    :cond_4
    new-instance p1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p1, v0}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p1

    .line 151
    :cond_5
    new-instance p1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {p1, v0}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p1

    .line 147
    :cond_6
    new-instance p1, Lcom/epson/epsonio/EpsonIoException;

    const/4 p2, 0x6

    invoke-direct {p1, p2}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw p1
.end method
