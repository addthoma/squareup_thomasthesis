.class abstract Lcom/epson/eposdevice/keyboard/NativeKeyboard;
.super Ljava/lang/Object;
.source "NativeKeyboard.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected native nativeKbdSetPrefix(J[B)I
.end method

.method protected abstract nativeOnKbdKeyPress(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method protected abstract nativeOnKbdString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method protected native nativeSetKbdKeyPressCallback(JLcom/epson/eposdevice/keyboard/NativeKeyboard;)I
.end method

.method protected native nativeSetKbdStringCallback(JLcom/epson/eposdevice/keyboard/NativeKeyboard;)I
.end method
