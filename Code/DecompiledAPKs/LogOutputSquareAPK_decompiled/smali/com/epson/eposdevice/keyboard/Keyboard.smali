.class public abstract Lcom/epson/eposdevice/keyboard/Keyboard;
.super Lcom/epson/eposdevice/keyboard/NativeKeyboard;
.source "Keyboard.java"


# static fields
.field public static final VK_0:B = 0x30t

.field public static final VK_1:B = 0x31t

.field public static final VK_2:B = 0x32t

.field public static final VK_3:B = 0x33t

.field public static final VK_4:B = 0x34t

.field public static final VK_5:B = 0x35t

.field public static final VK_6:B = 0x36t

.field public static final VK_7:B = 0x37t

.field public static final VK_8:B = 0x38t

.field public static final VK_9:B = 0x39t

.field public static final VK_A:B = 0x41t

.field public static final VK_ADD:B = 0x6bt

.field public static final VK_B:B = 0x42t

.field public static final VK_BACK:B = 0x8t

.field public static final VK_C:B = 0x43t

.field public static final VK_CAPITAL:B = 0x14t

.field public static final VK_CONTROL:B = 0x11t

.field public static final VK_CONVERT:B = 0x1ct

.field public static final VK_D:B = 0x44t

.field public static final VK_DELETE:B = 0x2et

.field public static final VK_DOWN:B = 0x28t

.field public static final VK_E:B = 0x45t

.field public static final VK_END:B = 0x23t

.field public static final VK_ESCAPE:B = 0x1bt

.field public static final VK_F:B = 0x46t

.field public static final VK_F1:B = 0x70t

.field public static final VK_F10:B = 0x79t

.field public static final VK_F11:B = 0x7at

.field public static final VK_F12:B = 0x7bt

.field public static final VK_F2:B = 0x71t

.field public static final VK_F3:B = 0x72t

.field public static final VK_F4:B = 0x73t

.field public static final VK_F5:B = 0x74t

.field public static final VK_F6:B = 0x75t

.field public static final VK_F7:B = 0x76t

.field public static final VK_F8:B = 0x77t

.field public static final VK_F9:B = 0x78t

.field public static final VK_G:B = 0x47t

.field public static final VK_H:B = 0x48t

.field public static final VK_HOME:B = 0x24t

.field public static final VK_I:B = 0x49t

.field public static final VK_INSERT:B = 0x2dt

.field public static final VK_J:B = 0x4at

.field public static final VK_K:B = 0x4bt

.field public static final VK_L:B = 0x4ct

.field public static final VK_LEFT:B = 0x25t

.field public static final VK_M:B = 0x4dt

.field public static final VK_MENU:B = 0x12t

.field public static final VK_MULTIPLY:B = 0x6at

.field public static final VK_N:B = 0x4et

.field public static final VK_NEXT:B = 0x22t

.field public static final VK_NONCONVERT:B = 0x1dt

.field public static final VK_O:B = 0x4ft

.field public static final VK_OEM_1:B = -0x46t

.field public static final VK_OEM_2:B = -0x41t

.field public static final VK_OEM_3:B = -0x40t

.field public static final VK_OEM_4:B = -0x25t

.field public static final VK_OEM_5:B = -0x24t

.field public static final VK_OEM_6:B = -0x23t

.field public static final VK_OEM_7:B = -0x22t

.field public static final VK_OEM_ATTN:B = -0x10t

.field public static final VK_OEM_COMMA:B = -0x44t

.field public static final VK_OEM_MINUS:B = -0x43t

.field public static final VK_OEM_PERIOD:B = -0x42t

.field public static final VK_OEM_PLUS:B = -0x45t

.field public static final VK_P:B = 0x50t

.field public static final VK_PRIOR:B = 0x21t

.field public static final VK_Q:B = 0x51t

.field public static final VK_R:B = 0x52t

.field public static final VK_RETURN:B = 0xdt

.field public static final VK_RIGHT:B = 0x27t

.field public static final VK_S:B = 0x53t

.field public static final VK_SHIFT:B = 0x10t

.field public static final VK_SPACE:B = 0x20t

.field public static final VK_SUBTRACT:B = 0x6dt

.field public static final VK_T:B = 0x54t

.field public static final VK_TAB:B = 0x9t

.field public static final VK_U:B = 0x55t

.field public static final VK_UP:B = 0x26t

.field public static final VK_V:B = 0x56t

.field public static final VK_W:B = 0x57t

.field public static final VK_X:B = 0x58t

.field public static final VK_Y:B = 0x59t

.field public static final VK_Z:B = 0x5at


# instance fields
.field private mKbdHandle:J

.field private mKeyPressListener:Lcom/epson/eposdevice/keyboard/KeyPressListener;

.field private mStringListener:Lcom/epson/eposdevice/keyboard/StringListener;


# direct methods
.method protected constructor <init>(J)V
    .locals 2

    .line 111
    invoke-direct {p0}, Lcom/epson/eposdevice/keyboard/NativeKeyboard;-><init>()V

    const/4 v0, 0x0

    .line 93
    iput-object v0, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mKeyPressListener:Lcom/epson/eposdevice/keyboard/KeyPressListener;

    .line 94
    iput-object v0, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mStringListener:Lcom/epson/eposdevice/keyboard/StringListener;

    const-wide/16 v0, 0x0

    .line 179
    iput-wide v0, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mKbdHandle:J

    .line 112
    iput-wide p1, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mKbdHandle:J

    return-void
.end method

.method private OnKbdKeyPress(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    .line 98
    iget-object v0, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mKeyPressListener:Lcom/epson/eposdevice/keyboard/KeyPressListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    .line 99
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object p4, v0, v1

    const-string v1, "onKbdKeyPress"

    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/keyboard/Keyboard;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    iget-object v0, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mKeyPressListener:Lcom/epson/eposdevice/keyboard/KeyPressListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/epson/eposdevice/keyboard/KeyPressListener;->onKbdKeyPress(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method private OnKbdString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .line 105
    iget-object v0, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mStringListener:Lcom/epson/eposdevice/keyboard/StringListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    const/4 v1, 0x3

    .line 106
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "onKbdString"

    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/keyboard/Keyboard;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    iget-object v0, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mStringListener:Lcom/epson/eposdevice/keyboard/StringListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/epson/eposdevice/keyboard/StringListener;->onKbdString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected checkHandle()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    .line 124
    iget-wide v0, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mKbdHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    return-void

    .line 125
    :cond_0
    new-instance v0, Lcom/epson/eposdevice/EposException;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
.end method

.method protected getInnerHandle()J
    .locals 2

    .line 115
    iget-wide v0, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mKbdHandle:J

    return-wide v0
.end method

.method protected innerDeleteInstance()V
    .locals 3

    .line 118
    iget-wide v0, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mKbdHandle:J

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/keyboard/Keyboard;->nativeSetKbdKeyPressCallback(JLcom/epson/eposdevice/keyboard/NativeKeyboard;)I

    .line 119
    iget-wide v0, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mKbdHandle:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/keyboard/Keyboard;->nativeSetKbdStringCallback(JLcom/epson/eposdevice/keyboard/NativeKeyboard;)I

    const-wide/16 v0, 0x0

    .line 120
    iput-wide v0, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mKbdHandle:J

    return-void
.end method

.method protected nativeOnKbdKeyPress(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .line 171
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/epson/eposdevice/keyboard/Keyboard;->OnKbdKeyPress(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method protected nativeOnKbdString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .line 176
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/epson/eposdevice/keyboard/Keyboard;->OnKbdString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method protected abstract outputException(Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method protected varargs abstract outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method protected varargs abstract outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method protected varargs abstract outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method public setKeyPressEventCallback(Lcom/epson/eposdevice/keyboard/KeyPressListener;)V
    .locals 5

    .line 145
    iget-wide v0, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mKbdHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 147
    iput-object p1, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mKeyPressListener:Lcom/epson/eposdevice/keyboard/KeyPressListener;

    .line 148
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/keyboard/Keyboard;->nativeSetKbdKeyPressCallback(JLcom/epson/eposdevice/keyboard/NativeKeyboard;)I

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 151
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/keyboard/Keyboard;->nativeSetKbdKeyPressCallback(JLcom/epson/eposdevice/keyboard/NativeKeyboard;)I

    .line 152
    iput-object p1, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mKeyPressListener:Lcom/epson/eposdevice/keyboard/KeyPressListener;

    :cond_1
    :goto_0
    return-void
.end method

.method public setPrefix([B)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "setPrefix"

    .line 130
    invoke-virtual {p0, v3, v1}, Lcom/epson/eposdevice/keyboard/Keyboard;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/keyboard/Keyboard;->checkHandle()V

    .line 133
    iget-wide v4, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mKbdHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/keyboard/Keyboard;->nativeKbdSetPrefix(J[B)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 142
    invoke-virtual {p0, v3, v0}, Lcom/epson/eposdevice/keyboard/Keyboard;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 135
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 139
    invoke-virtual {p0, v3, p1}, Lcom/epson/eposdevice/keyboard/Keyboard;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 140
    throw p1
.end method

.method public setStringEventCallback(Lcom/epson/eposdevice/keyboard/StringListener;)V
    .locals 5

    .line 157
    iget-wide v0, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mKbdHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 159
    iput-object p1, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mStringListener:Lcom/epson/eposdevice/keyboard/StringListener;

    .line 160
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/keyboard/Keyboard;->nativeSetKbdStringCallback(JLcom/epson/eposdevice/keyboard/NativeKeyboard;)I

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 163
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/keyboard/Keyboard;->nativeSetKbdStringCallback(JLcom/epson/eposdevice/keyboard/NativeKeyboard;)I

    .line 164
    iput-object p1, p0, Lcom/epson/eposdevice/keyboard/Keyboard;->mStringListener:Lcom/epson/eposdevice/keyboard/StringListener;

    :cond_1
    :goto_0
    return-void
.end method
