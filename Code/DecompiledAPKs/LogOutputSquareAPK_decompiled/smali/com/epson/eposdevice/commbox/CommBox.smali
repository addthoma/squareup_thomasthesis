.class public abstract Lcom/epson/eposdevice/commbox/CommBox;
.super Lcom/epson/eposdevice/commbox/NativeCommBox;
.source "CommBox.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/eposdevice/commbox/CommBox$CommBoxSendDataCallbackAdapter;,
        Lcom/epson/eposdevice/commbox/CommBox$CommBoxHistoryCallbackAdapter;
    }
.end annotation


# instance fields
.field private mCommBoxHandle:J

.field private mReceiveListener:Lcom/epson/eposdevice/commbox/ReceiveListener;


# direct methods
.method protected constructor <init>(J)V
    .locals 2

    .line 21
    invoke-direct {p0}, Lcom/epson/eposdevice/commbox/NativeCommBox;-><init>()V

    const-wide/16 v0, 0x0

    .line 10
    iput-wide v0, p0, Lcom/epson/eposdevice/commbox/CommBox;->mCommBoxHandle:J

    const/4 v0, 0x0

    .line 11
    iput-object v0, p0, Lcom/epson/eposdevice/commbox/CommBox;->mReceiveListener:Lcom/epson/eposdevice/commbox/ReceiveListener;

    .line 22
    iput-wide p1, p0, Lcom/epson/eposdevice/commbox/CommBox;->mCommBoxHandle:J

    return-void
.end method

.method private OnCommBoxReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 15
    iget-object v0, p0, Lcom/epson/eposdevice/commbox/CommBox;->mReceiveListener:Lcom/epson/eposdevice/commbox/ReceiveListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    const/4 v1, 0x3

    aput-object p4, v0, v1

    const-string v1, "onCommBoxReceive"

    .line 16
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/commbox/CommBox;->outputBoxLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17
    iget-object v0, p0, Lcom/epson/eposdevice/commbox/CommBox;->mReceiveListener:Lcom/epson/eposdevice/commbox/ReceiveListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/epson/eposdevice/commbox/ReceiveListener;->onCommBoxReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getCommHistory([ILcom/epson/eposdevice/commbox/GetCommHistoryListener;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const-string v3, "getCommHistory"

    .line 34
    invoke-virtual {p0, v3, v1}, Lcom/epson/eposdevice/commbox/CommBox;->outputBoxLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    :try_start_0
    iget-wide v4, p0, Lcom/epson/eposdevice/commbox/CommBox;->mCommBoxHandle:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_2

    if-eqz p2, :cond_1

    if-eqz p1, :cond_1

    .line 39
    array-length v1, p1

    if-eqz v1, :cond_1

    new-array v1, v0, [J

    .line 44
    iget-wide v4, p0, Lcom/epson/eposdevice/commbox/CommBox;->mCommBoxHandle:J

    new-instance v6, Lcom/epson/eposdevice/commbox/CommBox$CommBoxHistoryCallbackAdapter;

    invoke-direct {v6, p0, p2}, Lcom/epson/eposdevice/commbox/CommBox$CommBoxHistoryCallbackAdapter;-><init>(Lcom/epson/eposdevice/commbox/CommBox;Lcom/epson/eposdevice/commbox/GetCommHistoryListener;)V

    invoke-virtual {p0, v4, v5, v1, v6}, Lcom/epson/eposdevice/commbox/CommBox;->nativeCommBoxGetHistory(J[JLcom/epson/eposdevice/commbox/NativeCommBox$NativeCommBoxHistoryCallbackAdapter;)I

    move-result v4

    if-nez v4, :cond_0

    .line 49
    aget-wide v4, v1, v2

    long-to-int v1, v4

    aput v1, p1, v2
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    new-array p1, v0, [Ljava/lang/Object;

    aput-object p2, p1, v2

    .line 56
    invoke-virtual {p0, v3, p1}, Lcom/epson/eposdevice/commbox/CommBox;->outputBoxLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 46
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v4}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1

    .line 40
    :cond_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1

    .line 37
    :cond_2
    new-instance p1, Lcom/epson/eposdevice/EposException;

    const/4 p2, 0x5

    invoke-direct {p1, p2}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 53
    invoke-virtual {p0, v3, p1}, Lcom/epson/eposdevice/commbox/CommBox;->outputBoxException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 54
    throw p1
.end method

.method protected getInnerHandle()J
    .locals 2

    .line 26
    iget-wide v0, p0, Lcom/epson/eposdevice/commbox/CommBox;->mCommBoxHandle:J

    return-wide v0
.end method

.method protected innerDeleteInstance()V
    .locals 2

    const-wide/16 v0, 0x0

    .line 30
    iput-wide v0, p0, Lcom/epson/eposdevice/commbox/CommBox;->mCommBoxHandle:J

    return-void
.end method

.method protected nativeOnCommBoxReceive(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 100
    invoke-direct {p0, p3, p4, p5, p6}, Lcom/epson/eposdevice/commbox/CommBox;->OnCommBoxReceive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected abstract outputBoxException(Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method protected varargs abstract outputBoxLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method protected varargs abstract outputBoxLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method protected varargs abstract outputBoxLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method public sendData(Ljava/lang/String;Ljava/lang/String;[ILcom/epson/eposdevice/commbox/SendDataListener;)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v8, p0

    move-object/from16 v0, p3

    move-object/from16 v9, p4

    const/4 v10, 0x4

    new-array v1, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p1, v1, v11

    const/4 v12, 0x1

    aput-object p2, v1, v12

    const/4 v13, 0x2

    aput-object v0, v1, v13

    const/4 v14, 0x3

    aput-object v9, v1, v14

    const-string v15, "sendData"

    .line 60
    invoke-virtual {v8, v15, v1}, Lcom/epson/eposdevice/commbox/CommBox;->outputBoxLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    :try_start_0
    iget-wide v1, v8, Lcom/epson/eposdevice/commbox/CommBox;->mCommBoxHandle:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-eqz v5, :cond_2

    if-eqz p1, :cond_1

    if-eqz v9, :cond_1

    if-eqz v0, :cond_1

    .line 65
    array-length v1, v0

    if-eqz v1, :cond_1

    new-array v7, v12, [J

    .line 69
    iget-wide v2, v8, Lcom/epson/eposdevice/commbox/CommBox;->mCommBoxHandle:J

    new-instance v6, Lcom/epson/eposdevice/commbox/CommBox$CommBoxSendDataCallbackAdapter;

    invoke-direct {v6, v8, v9}, Lcom/epson/eposdevice/commbox/CommBox$CommBoxSendDataCallbackAdapter;-><init>(Lcom/epson/eposdevice/commbox/CommBox;Lcom/epson/eposdevice/commbox/SendDataListener;)V

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v16, v6

    move-object v6, v7

    move-object/from16 v17, v7

    move-object/from16 v7, v16

    invoke-virtual/range {v1 .. v7}, Lcom/epson/eposdevice/commbox/CommBox;->nativeCommBoxSendData(JLjava/lang/String;Ljava/lang/String;[JLcom/epson/eposdevice/commbox/NativeCommBox$NativeCommBoxSendDataCallbackAdapter;)I

    move-result v1

    if-nez v1, :cond_0

    .line 74
    aget-wide v1, v17, v11

    long-to-int v2, v1

    aput v2, v0, v11
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    new-array v1, v10, [Ljava/lang/Object;

    aput-object p1, v1, v11

    aput-object p2, v1, v12

    aput-object v0, v1, v13

    aput-object v9, v1, v14

    .line 81
    invoke-virtual {v8, v15, v1}, Lcom/epson/eposdevice/commbox/CommBox;->outputBoxLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 71
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    .line 66
    :cond_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v12}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    .line 63
    :cond_2
    new-instance v0, Lcom/epson/eposdevice/EposException;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 78
    invoke-virtual {v8, v15, v0}, Lcom/epson/eposdevice/commbox/CommBox;->outputBoxException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 79
    throw v0
.end method

.method public setReceiveEventCallback(Lcom/epson/eposdevice/commbox/ReceiveListener;)V
    .locals 5

    .line 85
    iget-wide v0, p0, Lcom/epson/eposdevice/commbox/CommBox;->mCommBoxHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 89
    iput-object p1, p0, Lcom/epson/eposdevice/commbox/CommBox;->mReceiveListener:Lcom/epson/eposdevice/commbox/ReceiveListener;

    .line 90
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/commbox/CommBox;->nativeSetCommBoxReceiveEventCallback(JLcom/epson/eposdevice/commbox/NativeCommBox;)I

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 93
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/commbox/CommBox;->nativeSetCommBoxReceiveEventCallback(JLcom/epson/eposdevice/commbox/NativeCommBox;)I

    .line 94
    iput-object p1, p0, Lcom/epson/eposdevice/commbox/CommBox;->mReceiveListener:Lcom/epson/eposdevice/commbox/ReceiveListener;

    :goto_0
    return-void
.end method
