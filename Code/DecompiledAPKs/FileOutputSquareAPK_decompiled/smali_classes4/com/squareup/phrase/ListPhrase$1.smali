.class final Lcom/squareup/phrase/ListPhrase$1;
.super Ljava/util/AbstractList;
.source "ListPhrase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/phrase/ListPhrase;->asList(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic val$first:Ljava/lang/Object;

.field final synthetic val$rest:[Ljava/lang/Object;

.field final synthetic val$second:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/phrase/ListPhrase$1;->val$first:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/phrase/ListPhrase$1;->val$second:Ljava/lang/Object;

    iput-object p3, p0, Lcom/squareup/phrase/ListPhrase$1;->val$rest:[Ljava/lang/Object;

    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    return-void
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/squareup/phrase/ListPhrase$1;->val$rest:[Ljava/lang/Object;

    add-int/lit8 p1, p1, -0x2

    aget-object p1, v0, p1

    return-object p1

    .line 199
    :cond_0
    iget-object p1, p0, Lcom/squareup/phrase/ListPhrase$1;->val$second:Ljava/lang/Object;

    return-object p1

    .line 197
    :cond_1
    iget-object p1, p0, Lcom/squareup/phrase/ListPhrase$1;->val$first:Ljava/lang/Object;

    return-object p1
.end method

.method public size()I
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/squareup/phrase/ListPhrase$1;->val$rest:[Ljava/lang/Object;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method
