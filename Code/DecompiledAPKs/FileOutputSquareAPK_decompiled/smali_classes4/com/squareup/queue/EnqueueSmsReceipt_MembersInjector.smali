.class public final Lcom/squareup/queue/EnqueueSmsReceipt_MembersInjector;
.super Ljava/lang/Object;
.source "EnqueueSmsReceipt_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/queue/EnqueueSmsReceipt;",
        ">;"
    }
.end annotation


# instance fields
.field private final lastCapturePaymentIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final taskQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/queue/EnqueueSmsReceipt_MembersInjector;->lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/queue/EnqueueSmsReceipt_MembersInjector;->lastCapturePaymentIdProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/queue/EnqueueSmsReceipt_MembersInjector;->taskQueueProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/queue/EnqueueSmsReceipt;",
            ">;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/queue/EnqueueSmsReceipt_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/queue/EnqueueSmsReceipt_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/EnqueueSmsReceipt;)V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/queue/EnqueueSmsReceipt_MembersInjector;->lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/queue/PostPaymentTask_MembersInjector;->injectLastLocalPaymentServerId(Lcom/squareup/queue/PostPaymentTask;Lcom/squareup/settings/LocalSetting;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/queue/EnqueueSmsReceipt_MembersInjector;->lastCapturePaymentIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/queue/PostPaymentTask_MembersInjector;->injectLastCapturePaymentId(Lcom/squareup/queue/PostPaymentTask;Lcom/squareup/settings/LocalSetting;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/queue/EnqueueSmsReceipt_MembersInjector;->taskQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-static {p1, v0}, Lcom/squareup/queue/PostPaymentTask_MembersInjector;->injectTaskQueue(Lcom/squareup/queue/PostPaymentTask;Lcom/squareup/queue/retrofit/RetrofitQueue;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/queue/EnqueueSmsReceipt;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/EnqueueSmsReceipt_MembersInjector;->injectMembers(Lcom/squareup/queue/EnqueueSmsReceipt;)V

    return-void
.end method
