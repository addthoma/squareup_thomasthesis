.class public Lcom/squareup/queue/QueueService;
.super Landroid/app/Service;
.source "QueueService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/QueueService$ErrorType;,
        Lcom/squareup/queue/QueueService$RetrofitQueueStarter;,
        Lcom/squareup/queue/QueueService$PrioritizedQueues;,
        Lcom/squareup/queue/QueueService$TaskRequiresRetry;,
        Lcom/squareup/queue/QueueService$Starter;,
        Lcom/squareup/queue/QueueService$BootReceiver;,
        Lcom/squareup/queue/QueueService$TaskCallback;,
        Lcom/squareup/queue/QueueService$LoggedInDependencies;
    }
.end annotation


# static fields
.field private static final EXECUTING_TASK:Z = true

.field private static final NOT_EXECUTING_TASK:Z = false

.field private static final REASON_FOR_STARTING:Ljava/lang/String; = "reason"


# instance fields
.field private appScope:Lmortar/MortarScope;

.field authenticator:Lcom/squareup/account/LegacyAuthenticator;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field clock:Lcom/squareup/util/Clock;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field eventSink:Lcom/squareup/badbus/BadEventSink;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field foregroundServiceStarter:Lcom/squareup/foregroundservice/ForegroundServiceStarter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field gson:Lcom/google/gson/Gson;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field lastQueueServiceStart:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private lastTaskLog:Ljava/lang/String;

.field loggedOutTaskWatcher:Lcom/squareup/queue/TaskWatcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field ohSnapLogger:Lcom/squareup/log/OhSnapLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final prioritizedQueues:Lcom/squareup/queue/QueueService$PrioritizedQueues;

.field queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field remoteLogger:Lcom/squareup/logging/RemoteLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field runningTask:Lcom/squareup/queue/retrofit/RetrofitTask;

.field storeAndForwardQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 90
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 112
    new-instance v0, Lcom/squareup/queue/QueueService$PrioritizedQueues;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/queue/QueueService$PrioritizedQueues;-><init>(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/QueueService$1;)V

    iput-object v0, p0, Lcom/squareup/queue/QueueService;->prioritizedQueues:Lcom/squareup/queue/QueueService$PrioritizedQueues;

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/queue/QueueService;)Ljava/lang/String;
    .locals 0

    .line 90
    iget-object p0, p0, Lcom/squareup/queue/QueueService;->lastTaskLog:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/queue/QueueService;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0

    .line 90
    invoke-direct {p0, p1, p2}, Lcom/squareup/queue/QueueService;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/queue/QueueService;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0

    .line 90
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/queue/QueueService;->logError(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/QueueService$ErrorType;)V
    .locals 0

    .line 90
    invoke-direct {p0, p1}, Lcom/squareup/queue/QueueService;->retryLater(Lcom/squareup/queue/QueueService$ErrorType;)V

    return-void
.end method

.method static synthetic access$600(Lcom/squareup/queue/QueueService;)Lcom/squareup/queue/QueueService$LoggedInDependencies;
    .locals 0

    .line 90
    invoke-direct {p0}, Lcom/squareup/queue/QueueService;->getLoggedInDependenciesIfLoggedIn()Lcom/squareup/queue/QueueService$LoggedInDependencies;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/queue/QueueService;)Lmortar/MortarScope;
    .locals 0

    .line 90
    iget-object p0, p0, Lcom/squareup/queue/QueueService;->appScope:Lmortar/MortarScope;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/queue/QueueService;Lmortar/MortarScope;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/TaskWatcher;)Z
    .locals 0

    .line 90
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/queue/QueueService;->startTaskFrom(Lmortar/MortarScope;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/TaskWatcher;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$900(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/retrofit/RetrofitTask;)V
    .locals 0

    .line 90
    invoke-direct {p0, p1}, Lcom/squareup/queue/QueueService;->setRunningTask(Lcom/squareup/queue/retrofit/RetrofitTask;)V

    return-void
.end method

.method private getLoggedInDependenciesIfLoggedIn()Lcom/squareup/queue/QueueService$LoggedInDependencies;
    .locals 2

    .line 210
    iget-object v0, p0, Lcom/squareup/queue/QueueService;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    invoke-interface {v0}, Lcom/squareup/account/LegacyAuthenticator;->isLoggedIn()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "logged out"

    .line 212
    invoke-direct {p0, v1, v0}, Lcom/squareup/queue/QueueService;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0

    .line 216
    :cond_0
    const-class v0, Lcom/squareup/queue/TransactionTasksComponent;

    .line 217
    invoke-static {v0}, Lcom/squareup/MortarLoggedIn;->getLoggedInComponent(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/TransactionTasksComponent;

    .line 218
    invoke-interface {v0}, Lcom/squareup/queue/TransactionTasksComponent;->queueServiceLoggedInDependencies()Lcom/squareup/queue/QueueService$LoggedInDependencies;

    move-result-object v0

    return-object v0
.end method

.method private varargs log(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .line 308
    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 309
    iget-object p2, p0, Lcom/squareup/queue/QueueService;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    .line 310
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p2, v1

    const/4 v0, 0x1

    aput-object p1, p2, v0

    const-string p1, "%s %s"

    invoke-static {p1, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private logAllCanceledForTag(Ljava/lang/String;)V
    .locals 9

    .line 320
    iget-object v0, p0, Lcom/squareup/queue/QueueService;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    invoke-interface {v0, p1}, Lcom/squareup/backgroundjob/BackgroundJobManager;->getAllJobRequestsForTag(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 321
    invoke-static {}, Lcom/evernote/android/job/JobConfig;->getClock()Lcom/evernote/android/job/util/Clock;

    move-result-object v1

    invoke-interface {v1}, Lcom/evernote/android/job/util/Clock;->currentTimeMillis()J

    move-result-wide v1

    .line 322
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/evernote/android/job/JobRequest;

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    .line 324
    invoke-virtual {v3}, Lcom/evernote/android/job/JobRequest;->getJobId()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v6

    invoke-virtual {v3}, Lcom/evernote/android/job/JobRequest;->getTag()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v7, v5

    invoke-virtual {v3}, Lcom/evernote/android/job/JobRequest;->getScheduledAt()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v7, v4

    const/4 v3, 0x3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v3

    const-string v3, "Canceling background job request: id=%d, tag=%s, scheduled_at=%d, canceled_at=%d"

    .line 323
    invoke-direct {p0, v3, v7}, Lcom/squareup/queue/QueueService;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/QueueService;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    invoke-interface {v0, p1}, Lcom/squareup/backgroundjob/BackgroundJobManager;->getAllJobsForTag(Ljava/lang/String;)Ljava/util/Set;

    move-result-object p1

    .line 328
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/evernote/android/job/Job;

    .line 329
    invoke-virtual {v0}, Lcom/evernote/android/job/Job;->isFinished()Z

    move-result v3

    if-nez v3, :cond_1

    new-array v3, v4, [Ljava/lang/Object;

    .line 330
    invoke-virtual {v0}, Lcom/evernote/android/job/Job;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v5

    const-string v0, "Canceling background job: %s, canceled_at=%d"

    invoke-direct {p0, v0, v3}, Lcom/squareup/queue/QueueService;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private varargs logError(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .line 314
    invoke-static {p2, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 315
    iget-object p3, p0, Lcom/squareup/queue/QueueService;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p3, v0, p2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 316
    iget-object p2, p0, Lcom/squareup/queue/QueueService;->remoteLogger:Lcom/squareup/logging/RemoteLogger;

    invoke-interface {p2, p1}, Lcom/squareup/logging/RemoteLogger;->w(Ljava/lang/Throwable;)V

    return-void
.end method

.method private retryLater(Lcom/squareup/queue/QueueService$ErrorType;)V
    .locals 4

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "Stopping task queue, retrying later."

    .line 419
    invoke-direct {p0, v2, v1}, Lcom/squareup/queue/QueueService;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 421
    sget-object v1, Lcom/squareup/queue/QueueService$ErrorType;->NETWORK:Lcom/squareup/queue/QueueService$ErrorType;

    const-string v2, "Retrying later"

    if-ne p1, v1, :cond_0

    .line 422
    invoke-static {v2}, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->startQueueServiceRequestWithNetwork(Ljava/lang/String;)Lcom/evernote/android/job/JobRequest;

    move-result-object p1

    goto :goto_0

    .line 423
    :cond_0
    invoke-static {v2}, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->startQueueServiceRequest(Ljava/lang/String;)Lcom/evernote/android/job/JobRequest;

    move-result-object p1

    .line 424
    :goto_0
    iget-object v1, p0, Lcom/squareup/queue/QueueService;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    invoke-interface {v1, p1}, Lcom/squareup/backgroundjob/BackgroundJobManager;->schedule(Lcom/evernote/android/job/JobRequest;)V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    .line 425
    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getJobId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x1

    .line 426
    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getTag()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x2

    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getScheduledAt()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    aput-object p1, v1, v0

    const-string p1, "Scheduling background job request: id=%d, tag=%s, scheduled_at=%d"

    .line 425
    invoke-direct {p0, p1, v1}, Lcom/squareup/queue/QueueService;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 427
    invoke-virtual {p0}, Lcom/squareup/queue/QueueService;->stopSelf()V

    .line 430
    iget-object p1, p0, Lcom/squareup/queue/QueueService;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance v0, Lcom/squareup/queue/QueueService$TaskRequiresRetry;

    invoke-direct {v0}, Lcom/squareup/queue/QueueService$TaskRequiresRetry;-><init>()V

    invoke-interface {p1, v0}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    return-void
.end method

.method private setRunningTask(Lcom/squareup/queue/retrofit/RetrofitTask;)V
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 299
    iput-object p1, p0, Lcom/squareup/queue/QueueService;->runningTask:Lcom/squareup/queue/retrofit/RetrofitTask;

    goto :goto_0

    .line 302
    :cond_0
    iput-object p1, p0, Lcom/squareup/queue/QueueService;->runningTask:Lcom/squareup/queue/retrofit/RetrofitTask;

    .line 303
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/squareup/util/Objects;->getHumanClassName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/QueueService;->gson:Lcom/google/gson/Gson;

    invoke-interface {p1}, Lcom/squareup/queue/retrofit/RetrofitTask;->secureCopyWithoutPIIForLogs()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/queue/QueueService;->lastTaskLog:Ljava/lang/String;

    :goto_0
    return-void
.end method

.method private startTaskFrom(Lmortar/MortarScope;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/TaskWatcher;)Z
    .locals 11

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    .line 226
    invoke-interface {p2}, Lcom/squareup/queue/retrofit/RetrofitQueue;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const-string v3, "looking at queue %s: Approximate size: %s"

    invoke-direct {p0, v3, v1}, Lcom/squareup/queue/QueueService;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 228
    invoke-interface {p2}, Lcom/squareup/queue/retrofit/RetrofitQueue;->peek()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/queue/retrofit/RetrofitTask;

    if-nez v10, :cond_0

    return v2

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 231
    iget-object v1, p0, Lcom/squareup/queue/QueueService;->lastTaskLog:Ljava/lang/String;

    aput-object v1, v0, v2

    .line 232
    invoke-interface {p2}, Lcom/squareup/queue/retrofit/RetrofitQueue;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const-string v1, "Executing %s. Approximately %d tasks remaining including this one"

    .line 231
    invoke-direct {p0, v1, v0}, Lcom/squareup/queue/QueueService;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 235
    invoke-direct {p0, v10}, Lcom/squareup/queue/QueueService;->setRunningTask(Lcom/squareup/queue/retrofit/RetrofitTask;)V

    .line 236
    invoke-interface {p2, v4}, Lcom/squareup/queue/retrofit/RetrofitQueue;->delayClose(Z)V

    .line 237
    new-instance v8, Lcom/squareup/queue/QueueService$TaskCallback;

    invoke-direct {v8, p0, p2, v10, p3}, Lcom/squareup/queue/QueueService$TaskCallback;-><init>(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/retrofit/RetrofitTask;Lcom/squareup/queue/TaskWatcher;)V

    .line 240
    invoke-virtual {p3, v10}, Lcom/squareup/queue/TaskWatcher;->startTask(Lcom/squareup/queue/retrofit/RetrofitTask;)Lio/reactivex/Completable;

    move-result-object p3

    new-instance v0, Lcom/squareup/queue/-$$Lambda$QueueService$GDQKfEVtAr_mr7_xVNO7wnz_eTw;

    move-object v5, v0

    move-object v6, p0

    move-object v7, p2

    move-object v9, p1

    invoke-direct/range {v5 .. v10}, Lcom/squareup/queue/-$$Lambda$QueueService$GDQKfEVtAr_mr7_xVNO7wnz_eTw;-><init>(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/QueueService$TaskCallback;Lmortar/MortarScope;Lcom/squareup/queue/retrofit/RetrofitTask;)V

    .line 241
    invoke-virtual {p3, v0}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    move-result-object p2

    .line 239
    invoke-static {p1, p2}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return v4
.end method


# virtual methods
.method public synthetic lambda$startTaskFrom$0$QueueService(Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/QueueService$TaskCallback;Lmortar/MortarScope;Lcom/squareup/queue/retrofit/RetrofitTask;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 242
    new-instance v0, Lcom/squareup/server/CancelableSquareCallback;

    new-instance v1, Lcom/squareup/queue/QueueService$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/queue/QueueService$1;-><init>(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/QueueService$TaskCallback;)V

    invoke-direct {v0, v1}, Lcom/squareup/server/CancelableSquareCallback;-><init>(Lcom/squareup/server/SquareCallback;)V

    .line 289
    invoke-virtual {v0, p3}, Lcom/squareup/server/CancelableSquareCallback;->cancelWhenScopeExits(Lmortar/MortarScope;)V

    .line 291
    invoke-interface {p4, v0}, Lcom/squareup/queue/retrofit/RetrofitTask;->execute(Ljava/lang/Object;)V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public onCreate()V
    .locals 2

    .line 117
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 118
    invoke-static {}, Lcom/squareup/mortar/AppContextWrapper;->appContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/QueueService;->appScope:Lmortar/MortarScope;

    .line 119
    iget-object v0, p0, Lcom/squareup/queue/QueueService;->appScope:Lmortar/MortarScope;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/squareup/util/RuntimeEnvironment;->isInstrumentationTest()Z

    move-result v0

    if-nez v0, :cond_1

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/QueueService;->appScope:Lmortar/MortarScope;

    const-class v1, Lcom/squareup/queue/QueueRootModule$Component;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/QueueRootModule$Component;

    invoke-interface {v0, p0}, Lcom/squareup/queue/QueueRootModule$Component;->inject(Lcom/squareup/queue/QueueService;)V

    :cond_1
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    if-eqz p1, :cond_0

    const-string p2, "reason"

    .line 138
    invoke-virtual {p1, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_0
    const-string p2, "null intent"

    .line 145
    :goto_0
    iget-object v0, p0, Lcom/squareup/queue/QueueService;->appScope:Lmortar/MortarScope;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lmortar/MortarScope;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    goto/16 :goto_1

    .line 151
    :cond_1
    iget-object v0, p0, Lcom/squareup/queue/QueueService;->lastQueueServiceStart:Lcom/squareup/settings/LocalSetting;

    iget-object v4, p0, Lcom/squareup/queue/QueueService;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v4}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v4}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    if-eqz p1, :cond_2

    .line 154
    iget-object v0, p0, Lcom/squareup/queue/QueueService;->foregroundServiceStarter:Lcom/squareup/foregroundservice/ForegroundServiceStarter;

    invoke-interface {v0, p0, p1}, Lcom/squareup/foregroundservice/ForegroundServiceStarter;->maybePromoteToForeground(Landroid/app/Service;Landroid/content/Intent;)V

    .line 158
    :cond_2
    iget-object p1, p0, Lcom/squareup/queue/QueueService;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    invoke-interface {p1}, Lcom/squareup/queue/QueueServiceStarter;->isPaused()Z

    move-result p1

    if-eqz p1, :cond_3

    new-array p1, v1, [Ljava/lang/Object;

    .line 159
    iget-object p3, p0, Lcom/squareup/queue/QueueService;->lastTaskLog:Ljava/lang/String;

    aput-object p3, p1, v2

    aput-object p2, p1, v3

    const-string p2, "Queue processing paused, skip execute task=%s - Started because [%s]"

    invoke-direct {p0, p2, p1}, Lcom/squareup/queue/QueueService;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    return v3

    .line 164
    :cond_3
    iget-object p1, p0, Lcom/squareup/queue/QueueService;->runningTask:Lcom/squareup/queue/retrofit/RetrofitTask;

    if-eqz p1, :cond_4

    new-array p1, v1, [Ljava/lang/Object;

    .line 165
    iget-object p3, p0, Lcom/squareup/queue/QueueService;->lastTaskLog:Ljava/lang/String;

    aput-object p3, p1, v2

    aput-object p2, p1, v3

    const-string p2, "Already running, skip execute task=%s - Started because [%s]"

    invoke-direct {p0, p2, p1}, Lcom/squareup/queue/QueueService;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    return v3

    .line 170
    :cond_4
    sget-object p1, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->START_TAG:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/queue/QueueService;->logAllCanceledForTag(Ljava/lang/String;)V

    .line 171
    iget-object p1, p0, Lcom/squareup/queue/QueueService;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    sget-object v0, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->START_TAG:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/backgroundjob/BackgroundJobManager;->cancelAllForTag(Ljava/lang/String;)I

    .line 173
    iget-object p1, p0, Lcom/squareup/queue/QueueService;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {p1}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 174
    sget-object p1, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->START_WITH_NETWORK_TAG:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/queue/QueueService;->logAllCanceledForTag(Ljava/lang/String;)V

    .line 175
    iget-object p1, p0, Lcom/squareup/queue/QueueService;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    sget-object v0, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->START_WITH_NETWORK_TAG:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/backgroundjob/BackgroundJobManager;->cancelAllForTag(Ljava/lang/String;)I

    .line 178
    :cond_5
    iget-object p1, p0, Lcom/squareup/queue/QueueService;->prioritizedQueues:Lcom/squareup/queue/QueueService$PrioritizedQueues;

    invoke-virtual {p1}, Lcom/squareup/queue/QueueService$PrioritizedQueues;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;

    .line 179
    invoke-virtual {v0}, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;->startNextTask()Z

    move-result v4

    if-eqz v4, :cond_6

    new-array p1, v1, [Ljava/lang/Object;

    .line 180
    invoke-static {v0}, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;->access$100(Lcom/squareup/queue/QueueService$RetrofitQueueStarter;)Ljava/lang/String;

    move-result-object p3

    aput-object p3, p1, v2

    aput-object p2, p1, v3

    const-string p2, "Action: %s - Started because [%s]"

    invoke-direct {p0, p2, p1}, Lcom/squareup/queue/QueueService;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    return v3

    .line 185
    :cond_7
    invoke-virtual {p0, p3}, Lcom/squareup/queue/QueueService;->stopSelf(I)V

    new-array p1, v3, [Ljava/lang/Object;

    aput-object p2, p1, v2

    const-string p2, "Action: Stopped - Started because [%s]"

    .line 186
    invoke-direct {p0, p2, p1}, Lcom/squareup/queue/QueueService;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    return v3

    :cond_8
    :goto_1
    new-array p1, v1, [Ljava/lang/Object;

    .line 146
    iget-object p3, p0, Lcom/squareup/queue/QueueService;->lastTaskLog:Ljava/lang/String;

    aput-object p3, p1, v2

    aput-object p2, p1, v3

    const-string p2, "App scope destroyed, skip execute task=%s - Started because [%s]"

    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return v3
.end method

.method public restart(Ljava/lang/String;)V
    .locals 1

    .line 415
    iget-object v0, p0, Lcom/squareup/queue/QueueService;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    invoke-interface {v0, p1}, Lcom/squareup/queue/QueueServiceStarter;->start(Ljava/lang/String;)V

    return-void
.end method
