.class public Lcom/squareup/queue/crm/AttachContactPostPaymentTask;
.super Lcom/squareup/queue/PostPaymentTask;
.source "AttachContactPostPaymentTask.java"


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final contactToken:Ljava/lang/String;

.field private final merchantToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/squareup/queue/PostPaymentTask;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/queue/crm/AttachContactPostPaymentTask;->contactToken:Ljava/lang/String;

    .line 16
    iput-object p2, p0, Lcom/squareup/queue/crm/AttachContactPostPaymentTask;->merchantToken:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getContactToken()Ljava/lang/String;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/queue/crm/AttachContactPostPaymentTask;->contactToken:Ljava/lang/String;

    return-object v0
.end method

.method public getMerchantToken()Ljava/lang/String;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/queue/crm/AttachContactPostPaymentTask;->merchantToken:Ljava/lang/String;

    return-object v0
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 47
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/crm/AttachContactPostPaymentTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/crm/AttachContactPostPaymentTask;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method protected taskFor(Ljava/lang/String;)Lcom/squareup/queue/retrofit/RetrofitTask;
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/queue/crm/AttachContactTask$Builder;

    invoke-direct {v0}, Lcom/squareup/queue/crm/AttachContactTask$Builder;-><init>()V

    .line 29
    invoke-virtual {v0, p1}, Lcom/squareup/queue/crm/AttachContactTask$Builder;->paymentId(Ljava/lang/String;)Lcom/squareup/queue/crm/AttachContactTask$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/queue/crm/AttachContactPostPaymentTask;->contactToken:Ljava/lang/String;

    .line 30
    invoke-virtual {p1, v0}, Lcom/squareup/queue/crm/AttachContactTask$Builder;->contactToken(Ljava/lang/String;)Lcom/squareup/queue/crm/AttachContactTask$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/queue/crm/AttachContactPostPaymentTask;->merchantToken:Ljava/lang/String;

    .line 31
    invoke-virtual {p1, v0}, Lcom/squareup/queue/crm/AttachContactTask$Builder;->merchantToken(Ljava/lang/String;)Lcom/squareup/queue/crm/AttachContactTask$Builder;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lcom/squareup/queue/crm/AttachContactTask$Builder;->build()Lcom/squareup/queue/crm/AttachContactTask;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AttachContactPostPaymentTask{contactToken=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/crm/AttachContactPostPaymentTask;->contactToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", merchantToken=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/crm/AttachContactPostPaymentTask;->merchantToken:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
