.class public Lcom/squareup/queue/crm/AttachContactTask$Builder;
.super Ljava/lang/Object;
.source "AttachContactTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/crm/AttachContactTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private contactToken:Ljava/lang/String;

.field private merchantToken:Ljava/lang/String;

.field private paymentId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/queue/crm/AttachContactTask$Builder;)Ljava/lang/String;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/queue/crm/AttachContactTask$Builder;->paymentId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/queue/crm/AttachContactTask$Builder;)Ljava/lang/String;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/queue/crm/AttachContactTask$Builder;->contactToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/queue/crm/AttachContactTask$Builder;)Ljava/lang/String;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/queue/crm/AttachContactTask$Builder;->merchantToken:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/queue/crm/AttachContactTask;
    .locals 2

    .line 65
    new-instance v0, Lcom/squareup/queue/crm/AttachContactTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/queue/crm/AttachContactTask;-><init>(Lcom/squareup/queue/crm/AttachContactTask$Builder;Lcom/squareup/queue/crm/AttachContactTask$1;)V

    return-object v0
.end method

.method public contactToken(Ljava/lang/String;)Lcom/squareup/queue/crm/AttachContactTask$Builder;
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/squareup/queue/crm/AttachContactTask$Builder;->contactToken:Ljava/lang/String;

    return-object p0
.end method

.method public merchantToken(Ljava/lang/String;)Lcom/squareup/queue/crm/AttachContactTask$Builder;
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/squareup/queue/crm/AttachContactTask$Builder;->merchantToken:Ljava/lang/String;

    return-object p0
.end method

.method public paymentId(Ljava/lang/String;)Lcom/squareup/queue/crm/AttachContactTask$Builder;
    .locals 0

    .line 50
    iput-object p1, p0, Lcom/squareup/queue/crm/AttachContactTask$Builder;->paymentId:Ljava/lang/String;

    return-object p0
.end method
