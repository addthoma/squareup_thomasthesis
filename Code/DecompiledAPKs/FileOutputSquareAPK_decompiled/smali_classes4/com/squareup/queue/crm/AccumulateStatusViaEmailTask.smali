.class public Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;
.super Lcom/squareup/queue/RpcThreadTask;
.source "AccumulateStatusViaEmailTask.java"

# interfaces
.implements Lcom/squareup/queue/QueueModule$LoggedInQueueModuleTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/RpcThreadTask<",
        "Lcom/squareup/server/SimpleResponse;",
        "Lcom/squareup/queue/QueueModule$Component;",
        ">;",
        "Lcom/squareup/queue/QueueModule$LoggedInQueueModuleTask;"
    }
.end annotation


# instance fields
.field final billIdPair:Lcom/squareup/protos/client/IdPair;

.field final cart:Lcom/squareup/protos/client/bills/Cart;

.field final emailAddress:Ljava/lang/String;

.field final emailId:Ljava/lang/String;

.field transient loyalty:Lcom/squareup/server/loyalty/LoyaltyService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field final tenderIdPair:Lcom/squareup/protos/client/IdPair;

.field final tenderType:Lcom/squareup/protos/client/bills/Tender$Type;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/Cart;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/squareup/queue/RpcThreadTask;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->emailAddress:Ljava/lang/String;

    .line 51
    iput-object p2, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->emailId:Ljava/lang/String;

    .line 52
    iput-object p3, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->billIdPair:Lcom/squareup/protos/client/IdPair;

    .line 53
    iput-object p4, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->tenderIdPair:Lcom/squareup/protos/client/IdPair;

    .line 54
    iput-object p5, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 55
    iput-object p6, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 57
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    const-string p2, "At least one of emailAddress and emailId should be set."

    invoke-static {p1, p2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$callOnRpcThread$1(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 112
    sget-object v0, Lcom/squareup/queue/crm/-$$Lambda$AccumulateStatusViaEmailTask$NH_8gAYo50acV1gwGWVwHJJAC-I;->INSTANCE:Lcom/squareup/queue/crm/-$$Lambda$AccumulateStatusViaEmailTask$NH_8gAYo50acV1gwGWVwHJJAC-I;

    invoke-static {p0, v0}, Lcom/squareup/receiving/StandardReceiver;->rejectIfNot(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$0(Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method


# virtual methods
.method protected callOnRpcThread()Lcom/squareup/server/SimpleResponse;
    .locals 3

    .line 84
    iget-object v0, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->emailAddress:Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->emailId:Ljava/lang/String;

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->tenderIdPair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->billIdPair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-nez v0, :cond_1

    goto :goto_1

    .line 93
    :cond_1
    new-instance v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->billIdPair:Lcom/squareup/protos/client/IdPair;

    .line 94
    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->tenderIdPair:Lcom/squareup/protos/client/IdPair;

    .line 95
    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 96
    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->tender_type(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 97
    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    move-result-object v0

    .line 100
    iget-object v2, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->emailId:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 101
    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->email_address_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    goto :goto_0

    .line 103
    :cond_2
    iget-object v2, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->emailAddress:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->email_address(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    .line 108
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

    .line 109
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/squareup/server/loyalty/LoyaltyService;->accumulateLoyaltyStatus(Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    sget-object v2, Lcom/squareup/queue/crm/-$$Lambda$AccumulateStatusViaEmailTask$e_BCie1y20KprDGVBtNtM0L6iqY;->INSTANCE:Lcom/squareup/queue/crm/-$$Lambda$AccumulateStatusViaEmailTask$e_BCie1y20KprDGVBtNtM0L6iqY;

    .line 111
    invoke-virtual {v0, v2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    .line 115
    new-instance v2, Lcom/squareup/server/SimpleResponse;

    instance-of v0, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-direct {v2, v0}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 117
    :catch_0
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    invoke-direct {v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    return-object v0

    .line 90
    :cond_3
    :goto_1
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    invoke-direct {v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    return-object v0
.end method

.method protected bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->callOnRpcThread()Lcom/squareup/server/SimpleResponse;

    move-result-object v0

    return-object v0
.end method

.method protected handleResponseOnMainThread(Lcom/squareup/server/SimpleResponse;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    return-object p1
.end method

.method protected bridge synthetic handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/server/SimpleResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->handleResponseOnMainThread(Lcom/squareup/server/SimpleResponse;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method

.method public inject(Lcom/squareup/queue/QueueModule$Component;)V
    .locals 0

    .line 80
    invoke-interface {p1, p0}, Lcom/squareup/queue/QueueModule$Component;->inject(Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/queue/QueueModule$Component;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->inject(Lcom/squareup/queue/QueueModule$Component;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 8

    .line 69
    new-instance v7, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;

    iget-object v0, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->emailAddress:Ljava/lang/String;

    .line 70
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BLANK"

    goto :goto_0

    :cond_0
    const-string v0, "REDACTED"

    :goto_0
    move-object v1, v0

    iget-object v2, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->emailId:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->billIdPair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->tenderIdPair:Lcom/squareup/protos/client/IdPair;

    iget-object v5, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    iget-object v6, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->cart:Lcom/squareup/protos/client/bills/Cart;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/Cart;)V

    return-object v7
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    .line 62
    iget-object v1, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->emailAddress:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->emailId:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->tenderIdPair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "AccumulateStatusViaEmailTask{emailAddress=\'%s\', emailId=\'%s\', tenderIdPair=\'%s\', tenderType=\'%s\', cart=\'%s\'}"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
