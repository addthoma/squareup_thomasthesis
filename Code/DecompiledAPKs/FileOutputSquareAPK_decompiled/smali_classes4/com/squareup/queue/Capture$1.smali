.class Lcom/squareup/queue/Capture$1;
.super Lcom/squareup/server/SquareCallback;
.source "Capture.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/queue/Capture;->execute(Lcom/squareup/server/SquareCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/server/SquareCallback<",
        "Lcom/squareup/server/payment/CaptureResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/queue/Capture;

.field final synthetic val$callback:Lcom/squareup/server/SquareCallback;


# direct methods
.method constructor <init>(Lcom/squareup/queue/Capture;Lcom/squareup/server/SquareCallback;)V
    .locals 0

    .line 349
    iput-object p1, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    iput-object p2, p0, Lcom/squareup/queue/Capture$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-direct {p0}, Lcom/squareup/server/SquareCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/server/payment/CaptureResponse;)V
    .locals 3

    .line 354
    iget-object v0, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    iget-object v0, v0, Lcom/squareup/queue/Capture;->messageQueue:Lcom/squareup/util/SquareMessageQueue;

    iget-object v1, p0, Lcom/squareup/queue/Capture$1;->val$callback:Lcom/squareup/server/SquareCallback;

    new-instance v2, Lcom/squareup/queue/-$$Lambda$Capture$1$8CEqp7WjwDcb5j7XbIX0m-p_IM4;

    invoke-direct {v2, p0, p1, v1}, Lcom/squareup/queue/-$$Lambda$Capture$1$8CEqp7WjwDcb5j7XbIX0m-p_IM4;-><init>(Lcom/squareup/queue/Capture$1;Lcom/squareup/server/payment/CaptureResponse;Lcom/squareup/server/SquareCallback;)V

    invoke-interface {v0, v2}, Lcom/squareup/util/SquareMessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 349
    check-cast p1, Lcom/squareup/server/payment/CaptureResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/Capture$1;->call(Lcom/squareup/server/payment/CaptureResponse;)V

    return-void
.end method

.method public clientError(Lcom/squareup/server/payment/CaptureResponse;I)V
    .locals 1

    .line 396
    iget-object v0, p0, Lcom/squareup/queue/Capture$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/server/SquareCallback;->clientError(Ljava/lang/Object;I)V

    return-void
.end method

.method public bridge synthetic clientError(Ljava/lang/Object;I)V
    .locals 0

    .line 349
    check-cast p1, Lcom/squareup/server/payment/CaptureResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/queue/Capture$1;->clientError(Lcom/squareup/server/payment/CaptureResponse;I)V

    return-void
.end method

.method public synthetic lambda$call$0$Capture$1(Lcom/squareup/server/payment/CaptureResponse;Lcom/squareup/server/SquareCallback;)Z
    .locals 6

    .line 356
    :try_start_0
    invoke-virtual {p1}, Lcom/squareup/server/payment/CaptureResponse;->isSuccessful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    iget-object v0, v0, Lcom/squareup/queue/Capture;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    iget-object v1, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    invoke-interface {v0, v1}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logCaptureProcessed(Lcom/squareup/queue/Capture;)V

    .line 359
    iget-object v0, p1, Lcom/squareup/server/payment/CaptureResponse;->payment_id:Ljava/lang/String;

    .line 361
    iget-object v1, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    iget-object v1, v1, Lcom/squareup/queue/Capture;->afterCapture:Lcom/squareup/queue/AfterCapture;

    iget-object v2, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    invoke-static {v2}, Lcom/squareup/queue/Capture;->access$100(Lcom/squareup/queue/Capture;)Lcom/squareup/queue/FutureReceipt;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    invoke-static {v3}, Lcom/squareup/queue/Capture;->access$200(Lcom/squareup/queue/Capture;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/squareup/queue/AfterCapture;->enqueueForCapturedPayment(Ljava/lang/String;Lcom/squareup/queue/FutureReceipt;Ljava/lang/String;)V

    goto :goto_0

    .line 363
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    iget-object v0, v0, Lcom/squareup/queue/Capture;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    iget-object v1, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    .line 364
    invoke-virtual {p1}, Lcom/squareup/server/payment/CaptureResponse;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 363
    invoke-interface {v0, v1, v2}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logCaptureFailed(Lcom/squareup/queue/Capture;Ljava/lang/String;)V

    .line 366
    iget-object v0, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/queue/Capture;->access$302(Lcom/squareup/queue/Capture;Z)Z

    .line 367
    iget-object v0, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    invoke-static {v0}, Lcom/squareup/queue/Capture;->access$000(Lcom/squareup/queue/Capture;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 371
    :goto_0
    iget-object v0, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    invoke-static {v0}, Lcom/squareup/queue/Capture;->access$400(Lcom/squareup/queue/Capture;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/server/payment/CaptureResponse;->isSuccessful()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 372
    iget-object v0, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    iget-object v0, v0, Lcom/squareup/queue/Capture;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v1, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    invoke-static {v1}, Lcom/squareup/queue/Capture;->access$400(Lcom/squareup/queue/Capture;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/server/payment/CaptureResponse;->payment_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    invoke-static {v4}, Lcom/squareup/queue/Capture;->access$500(Lcom/squareup/queue/Capture;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/tickets/Tickets;->updateTerminalId(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)V

    .line 376
    :cond_1
    invoke-virtual {p2, p1}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    const/4 p1, 0x0

    return p1

    :catchall_0
    move-exception v0

    .line 371
    iget-object v1, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    invoke-static {v1}, Lcom/squareup/queue/Capture;->access$400(Lcom/squareup/queue/Capture;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/squareup/server/payment/CaptureResponse;->isSuccessful()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 372
    iget-object v1, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    iget-object v1, v1, Lcom/squareup/queue/Capture;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v2, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    invoke-static {v2}, Lcom/squareup/queue/Capture;->access$400(Lcom/squareup/queue/Capture;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/squareup/protos/client/IdPair;

    iget-object v4, p1, Lcom/squareup/server/payment/CaptureResponse;->payment_id:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    invoke-static {v5}, Lcom/squareup/queue/Capture;->access$500(Lcom/squareup/queue/Capture;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2, v3}, Lcom/squareup/tickets/Tickets;->updateTerminalId(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)V

    .line 376
    :cond_2
    invoke-virtual {p2, p1}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    .line 377
    throw v0
.end method

.method public networkError()V
    .locals 1

    .line 404
    iget-object v0, p0, Lcom/squareup/queue/Capture$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0}, Lcom/squareup/server/SquareCallback;->networkError()V

    return-void
.end method

.method public serverError(I)V
    .locals 1

    .line 400
    iget-object v0, p0, Lcom/squareup/queue/Capture$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0, p1}, Lcom/squareup/server/SquareCallback;->serverError(I)V

    return-void
.end method

.method public sessionExpired()V
    .locals 1

    .line 392
    iget-object v0, p0, Lcom/squareup/queue/Capture$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0}, Lcom/squareup/server/SquareCallback;->sessionExpired()V

    return-void
.end method

.method public unexpectedError(Ljava/lang/Throwable;)V
    .locals 2

    .line 385
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/Capture$1;->this$0:Lcom/squareup/queue/Capture;

    invoke-static {v0}, Lcom/squareup/queue/Capture;->access$000(Lcom/squareup/queue/Capture;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 387
    iget-object v0, p0, Lcom/squareup/queue/Capture$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0, p1}, Lcom/squareup/server/SquareCallback;->unexpectedError(Ljava/lang/Throwable;)V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/squareup/queue/Capture$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v1, p1}, Lcom/squareup/server/SquareCallback;->unexpectedError(Ljava/lang/Throwable;)V

    .line 388
    throw v0
.end method
