.class public final Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;
.super Ljava/lang/Object;
.source "PendingCapturesModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/PendingCapturesModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory$GetEntryQuery;,
        Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory$RipenedCountQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/queue/sqlite/PendingCapturesModel;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final creator:Lcom/squareup/queue/sqlite/PendingCapturesModel$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/sqlite/PendingCapturesModel$Creator<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/queue/sqlite/PendingCapturesModel$Creator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/PendingCapturesModel$Creator<",
            "TT;>;)V"
        }
    .end annotation

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;->creator:Lcom/squareup/queue/sqlite/PendingCapturesModel$Creator;

    return-void
.end method


# virtual methods
.method public allEntries()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 129
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "pending_captures"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT *\nFROM pending_captures\nORDER BY timestamp_ms DESC"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public allEntriesMapper()Lcom/squareup/queue/sqlite/PendingCapturesModel$Mapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/sqlite/PendingCapturesModel$Mapper<",
            "TT;>;"
        }
    .end annotation

    .line 171
    new-instance v0, Lcom/squareup/queue/sqlite/PendingCapturesModel$Mapper;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/PendingCapturesModel$Mapper;-><init>(Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;)V

    return-object v0
.end method

.method public count()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 93
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "pending_captures"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT COUNT(*)\nFROM pending_captures"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public countMapper()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 137
    new-instance v0, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory$1;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory$1;-><init>(Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;)V

    return-object v0
.end method

.method public firstEntry()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 106
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "pending_captures"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT *\nFROM pending_captures\nWHERE _id = ( SELECT MIN(_id) FROM pending_captures )"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public firstEntryMapper()Lcom/squareup/queue/sqlite/PendingCapturesModel$Mapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/sqlite/PendingCapturesModel$Mapper<",
            "TT;>;"
        }
    .end annotation

    .line 156
    new-instance v0, Lcom/squareup/queue/sqlite/PendingCapturesModel$Mapper;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/PendingCapturesModel$Mapper;-><init>(Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;)V

    return-object v0
.end method

.method public getEntry(Ljava/lang/String;)Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 1

    .line 115
    new-instance v0, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory$GetEntryQuery;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory$GetEntryQuery;-><init>(Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;Ljava/lang/String;)V

    return-object v0
.end method

.method public getEntryMapper()Lcom/squareup/queue/sqlite/PendingCapturesModel$Mapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/sqlite/PendingCapturesModel$Mapper<",
            "TT;>;"
        }
    .end annotation

    .line 161
    new-instance v0, Lcom/squareup/queue/sqlite/PendingCapturesModel$Mapper;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/PendingCapturesModel$Mapper;-><init>(Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;)V

    return-object v0
.end method

.method public oldestEntry()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 120
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "pending_captures"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT *\nFROM pending_captures\nWHERE timestamp_ms = ( SELECT MIN(timestamp_ms) FROM pending_captures )"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public oldestEntryMapper()Lcom/squareup/queue/sqlite/PendingCapturesModel$Mapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/sqlite/PendingCapturesModel$Mapper<",
            "TT;>;"
        }
    .end annotation

    .line 166
    new-instance v0, Lcom/squareup/queue/sqlite/PendingCapturesModel$Mapper;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/PendingCapturesModel$Mapper;-><init>(Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;)V

    return-object v0
.end method

.method public ripenedCount(J)Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 1

    .line 101
    new-instance v0, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory$RipenedCountQuery;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory$RipenedCountQuery;-><init>(Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;J)V

    return-object v0
.end method

.method public ripenedCountMapper()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 146
    new-instance v0, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory$2;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory$2;-><init>(Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;)V

    return-object v0
.end method
