.class final Lcom/squareup/queue/sqlite/QueueStoresKt$queryOptionalResults$1;
.super Lkotlin/jvm/internal/Lambda;
.source "QueueStores.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/queue/sqlite/QueueStoresKt;->queryOptionalResults(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
        "Lcom/squareup/util/Optional<",
        "+TT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/util/Optional;",
        "T",
        "",
        "it",
        "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $queryMapper:Lkotlin/jvm/functions/Function1;


# direct methods
.method constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/queue/sqlite/QueueStoresKt$queryOptionalResults$1;->$queryMapper:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/sqlbrite3/SqlBrite$Query;)Lcom/squareup/util/Optional;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ")",
            "Lcom/squareup/util/Optional<",
            "TT;>;"
        }
    .end annotation

    .line 112
    sget-object v0, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/QueueStoresKt$queryOptionalResults$1;->$queryMapper:Lkotlin/jvm/functions/Function1;

    const-string v2, "it"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/util/Optional$Companion;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/sqlbrite3/SqlBrite$Query;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/QueueStoresKt$queryOptionalResults$1;->invoke(Lcom/squareup/sqlbrite3/SqlBrite$Query;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method
