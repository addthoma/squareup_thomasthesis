.class public final Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore$Companion;
.super Ljava/lang/Object;
.source "LocalPaymentSqliteStore.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLocalPaymentSqliteStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LocalPaymentSqliteStore.kt\ncom/squareup/queue/sqlite/LocalPaymentSqliteStore$Companion\n*L\n1#1,213:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J,\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0001\u0010\t\u001a\u00020\n2\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u000cH\u0007\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore$Companion;",
        "",
        "()V",
        "create",
        "Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;",
        "context",
        "Landroid/app/Application;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "userDir",
        "Ljava/io/File;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "queue_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 185
    invoke-direct {p0}, Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final create(Landroid/app/Application;Lcom/squareup/util/Clock;Ljava/io/File;Lio/reactivex/Scheduler;)Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;
    .locals 7
    .param p3    # Ljava/io/File;
        .annotation runtime Lcom/squareup/user/UserDirectory;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/LegacyMainScheduler;
        .end annotation
    .end param
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userDir"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    new-instance v0, Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;

    .line 193
    new-instance v1, Lcom/squareup/sqlbrite3/SqlBrite$Builder;

    invoke-direct {v1}, Lcom/squareup/sqlbrite3/SqlBrite$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/sqlbrite3/SqlBrite$Builder;->build()Lcom/squareup/sqlbrite3/SqlBrite;

    move-result-object v5

    const-string v1, "SqlBrite.Builder().build()"

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v6, p4

    .line 192
    invoke-direct/range {v1 .. v6}, Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;-><init>(Landroid/app/Application;Lcom/squareup/util/Clock;Ljava/io/File;Lcom/squareup/sqlbrite3/SqlBrite;Lio/reactivex/Scheduler;)V

    .line 195
    invoke-static {v0}, Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;->access$preload(Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;)V

    return-object v0
.end method
