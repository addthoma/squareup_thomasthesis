.class public final synthetic Lcom/squareup/queue/sqlite/-$$Lambda$eeKY7pIdP6j5Xjf_mMKQAml8xRA;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# instance fields
.field private final synthetic f$0:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/-$$Lambda$eeKY7pIdP6j5Xjf_mMKQAml8xRA;->f$0:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/queue/sqlite/-$$Lambda$eeKY7pIdP6j5Xjf_mMKQAml8xRA;->f$0:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;

    check-cast p1, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;

    invoke-virtual {v0, p1}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;->toQueueEntry(Lcom/squareup/queue/sqlite/StoredPaymentsEntry;)Lcom/squareup/payment/offline/StoredPayment;

    move-result-object p1

    return-object p1
.end method
