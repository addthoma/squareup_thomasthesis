.class public final Lcom/squareup/queue/sqlite/PendingCapturesModel$InsertEntry;
.super Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;
.source "PendingCapturesModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/PendingCapturesModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InsertEntry"
.end annotation


# direct methods
.method public constructor <init>(Landroidx/sqlite/db/SupportSQLiteDatabase;)V
    .locals 1

    const-string v0, "INSERT INTO pending_captures (entry_id, timestamp_ms, data)\nVALUES (?, ?, ?)"

    .line 216
    invoke-interface {p1, v0}, Landroidx/sqlite/db/SupportSQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroidx/sqlite/db/SupportSQLiteStatement;

    move-result-object p1

    const-string v0, "pending_captures"

    invoke-direct {p0, v0, p1}, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;-><init>(Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteStatement;)V

    return-void
.end method


# virtual methods
.method public bind(Ljava/lang/String;J[B)V
    .locals 1

    const/4 v0, 0x1

    .line 222
    invoke-virtual {p0, v0, p1}, Lcom/squareup/queue/sqlite/PendingCapturesModel$InsertEntry;->bindString(ILjava/lang/String;)V

    const/4 p1, 0x2

    .line 223
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/queue/sqlite/PendingCapturesModel$InsertEntry;->bindLong(IJ)V

    const/4 p1, 0x3

    .line 224
    invoke-virtual {p0, p1, p4}, Lcom/squareup/queue/sqlite/PendingCapturesModel$InsertEntry;->bindBlob(I[B)V

    return-void
.end method
