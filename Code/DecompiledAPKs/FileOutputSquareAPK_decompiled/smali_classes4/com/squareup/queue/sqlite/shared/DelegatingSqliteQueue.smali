.class public Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;
.super Ljava/lang/Object;
.source "DelegatingSqliteQueue.kt"

# interfaces
.implements Lcom/squareup/queue/sqlite/shared/SqliteQueue;
.implements Lcom/squareup/queue/sqlite/shared/SqliteQueueMonitor;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/queue/sqlite/shared/SqliteQueue<",
        "TT;>;",
        "Lcom/squareup/queue/sqlite/shared/SqliteQueueMonitor;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0016\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u00032\u00020\u0004B\u0013\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0003\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00028\u0000H\u0096\u0001\u00a2\u0006\u0002\u0010\nJ\u0015\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\r0\u000cH\u0097\u0001J\t\u0010\u000e\u001a\u00020\u0008H\u0096\u0001J\u001d\u0010\u000f\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\u00110\u00102\u0006\u0010\u0012\u001a\u00020\u0013H\u0096\u0001J\u0015\u0010\u0014\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\u00110\u000cH\u0096\u0001J\u000f\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0010H\u0096\u0001J\u000f\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0010H\u0096\u0001J\u000f\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u000cH\u0096\u0001R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;",
        "T",
        "",
        "Lcom/squareup/queue/sqlite/shared/SqliteQueue;",
        "Lcom/squareup/queue/sqlite/shared/SqliteQueueMonitor;",
        "queueDelegate",
        "(Lcom/squareup/queue/sqlite/shared/SqliteQueue;)V",
        "add",
        "Lio/reactivex/Completable;",
        "entry",
        "(Ljava/lang/Object;)Lio/reactivex/Completable;",
        "allEntries",
        "Lio/reactivex/Observable;",
        "",
        "close",
        "fetchEntry",
        "Lio/reactivex/Single;",
        "Lcom/squareup/util/Optional;",
        "entryId",
        "",
        "peekFirst",
        "removeAll",
        "",
        "removeFirst",
        "size",
        "queue_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final queueDelegate:Lcom/squareup/queue/sqlite/shared/SqliteQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/sqlite/shared/SqliteQueue<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/queue/sqlite/shared/SqliteQueue;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/shared/SqliteQueue<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "queueDelegate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->queueDelegate:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)Lio/reactivex/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lio/reactivex/Completable;"
        }
    .end annotation

    const-string v0, "entry"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->queueDelegate:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    invoke-interface {v0, p1}, Lcom/squareup/queue/sqlite/shared/SqliteQueue;->add(Ljava/lang/Object;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method

.method public allEntries()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "TT;>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Memory pressure, do not use"
    .end annotation

    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->queueDelegate:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    invoke-interface {v0}, Lcom/squareup/queue/sqlite/shared/SqliteQueue;->allEntries()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public close()Lio/reactivex/Completable;
    .locals 1

    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->queueDelegate:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    invoke-interface {v0}, Lcom/squareup/queue/sqlite/shared/SqliteQueue;->close()Lio/reactivex/Completable;

    move-result-object v0

    return-object v0
.end method

.method public fetchEntry(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "TT;>;>;"
        }
    .end annotation

    const-string v0, "entryId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->queueDelegate:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    invoke-interface {v0, p1}, Lcom/squareup/queue/sqlite/shared/SqliteQueue;->fetchEntry(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public peekFirst()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "TT;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->queueDelegate:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    invoke-interface {v0}, Lcom/squareup/queue/sqlite/shared/SqliteQueue;->peekFirst()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public removeAll()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->queueDelegate:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    invoke-interface {v0}, Lcom/squareup/queue/sqlite/shared/SqliteQueue;->removeAll()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public removeFirst()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->queueDelegate:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    invoke-interface {v0}, Lcom/squareup/queue/sqlite/shared/SqliteQueue;->removeFirst()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public size()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->queueDelegate:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    invoke-interface {v0}, Lcom/squareup/queue/sqlite/shared/SqliteQueue;->size()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
