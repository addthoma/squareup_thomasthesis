.class Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory$3;
.super Ljava/lang/Object;
.source "StoredPaymentsModel.java"

# interfaces
.implements Lcom/squareup/sqldelight/prerelease/RowMapper;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;->allDistinctEntryIdsMapper()Lcom/squareup/sqldelight/prerelease/RowMapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/sqldelight/prerelease/RowMapper<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;


# direct methods
.method constructor <init>(Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;)V
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory$3;->this$0:Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic map(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 0

    .line 220
    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory$3;->map(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public map(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 223
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
