.class final Lcom/squareup/queue/sqlite/SqliteRetrofitQueue$sqliteCloser$1;
.super Ljava/lang/Object;
.source "SqliteRetrofitQueue.kt"

# interfaces
.implements Lrx/functions/Action0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;-><init>(Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;Lcom/squareup/tape/TaskInjector;Lcom/squareup/queue/QueueServiceStarter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;


# direct methods
.method constructor <init>(Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue$sqliteCloser$1;->this$0:Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()V
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue$sqliteCloser$1;->this$0:Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;

    invoke-static {v0}, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;->access$getSqliteQueue$p(Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;)Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->close()Lio/reactivex/Completable;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lio/reactivex/Completable;->subscribe()Lio/reactivex/disposables/Disposable;

    return-void
.end method
