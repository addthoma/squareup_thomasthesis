.class public final Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory;
.super Ljava/lang/Object;
.source "LocalPaymentModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/LocalPaymentModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory$GetEntryQuery;,
        Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory$RipenedCountQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/queue/sqlite/LocalPaymentModel;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final creator:Lcom/squareup/queue/sqlite/LocalPaymentModel$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/sqlite/LocalPaymentModel$Creator<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/queue/sqlite/LocalPaymentModel$Creator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/LocalPaymentModel$Creator<",
            "TT;>;)V"
        }
    .end annotation

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory;->creator:Lcom/squareup/queue/sqlite/LocalPaymentModel$Creator;

    return-void
.end method


# virtual methods
.method public allEntries()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 120
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "local_payments_tasks"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT *\nFROM local_payments_tasks\nORDER BY timestamp_ms DESC"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public allEntriesMapper()Lcom/squareup/queue/sqlite/LocalPaymentModel$Mapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/sqlite/LocalPaymentModel$Mapper<",
            "TT;>;"
        }
    .end annotation

    .line 157
    new-instance v0, Lcom/squareup/queue/sqlite/LocalPaymentModel$Mapper;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/LocalPaymentModel$Mapper;-><init>(Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory;)V

    return-object v0
.end method

.method public count()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 93
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "local_payments_tasks"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT COUNT(*)\nFROM local_payments_tasks"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public countMapper()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 128
    new-instance v0, Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory$1;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory$1;-><init>(Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory;)V

    return-object v0
.end method

.method public firstEntry()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 106
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "local_payments_tasks"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT *\nFROM local_payments_tasks\nWHERE _id = ( SELECT MIN(_id) FROM local_payments_tasks )"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public firstEntryMapper()Lcom/squareup/queue/sqlite/LocalPaymentModel$Mapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/sqlite/LocalPaymentModel$Mapper<",
            "TT;>;"
        }
    .end annotation

    .line 147
    new-instance v0, Lcom/squareup/queue/sqlite/LocalPaymentModel$Mapper;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/LocalPaymentModel$Mapper;-><init>(Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory;)V

    return-object v0
.end method

.method public getEntry(Ljava/lang/String;)Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 1

    .line 115
    new-instance v0, Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory$GetEntryQuery;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory$GetEntryQuery;-><init>(Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory;Ljava/lang/String;)V

    return-object v0
.end method

.method public getEntryMapper()Lcom/squareup/queue/sqlite/LocalPaymentModel$Mapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/sqlite/LocalPaymentModel$Mapper<",
            "TT;>;"
        }
    .end annotation

    .line 152
    new-instance v0, Lcom/squareup/queue/sqlite/LocalPaymentModel$Mapper;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/LocalPaymentModel$Mapper;-><init>(Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory;)V

    return-object v0
.end method

.method public ripenedCount(J)Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 1

    .line 101
    new-instance v0, Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory$RipenedCountQuery;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory$RipenedCountQuery;-><init>(Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory;J)V

    return-object v0
.end method

.method public ripenedCountMapper()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 137
    new-instance v0, Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory$2;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory$2;-><init>(Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory;)V

    return-object v0
.end method
