.class final Lcom/squareup/queue/sqlite/StoredPaymentsEntry$sam$com_squareup_queue_sqlite_StoredPaymentsModel_AllDistinctEntriesCreator$0;
.super Ljava/lang/Object;
.source "StoredPaymentsEntry.kt"

# interfaces
.implements Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesCreator;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic function:Lkotlin/jvm/functions/Function4;


# direct methods
.method constructor <init>(Lkotlin/jvm/functions/Function4;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$sam$com_squareup_queue_sqlite_StoredPaymentsModel_AllDistinctEntriesCreator$0;->function:Lkotlin/jvm/functions/Function4;

    return-void
.end method


# virtual methods
.method public final synthetic create(Ljava/lang/Long;Ljava/lang/String;J[B)Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesModel;
    .locals 1

    const-string v0, "entry_id"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$sam$com_squareup_queue_sqlite_StoredPaymentsModel_AllDistinctEntriesCreator$0;->function:Lkotlin/jvm/functions/Function4;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-interface {v0, p1, p2, p3, p5}, Lkotlin/jvm/functions/Function4;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesModel;

    return-object p1
.end method
