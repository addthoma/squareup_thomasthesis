.class public final Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;
.super Ljava/lang/Object;
.source "QueueModule_ProvideLoggedInTaskWatcherFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/TaskWatcher;",
        ">;"
    }
.end annotation


# instance fields
.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final fileThreadSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final lastTaskRequiresRetryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p2, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p3, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;->preferencesProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p4, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;->gsonProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p5, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;->lastTaskRequiresRetryProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p6, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p7, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;->fileThreadSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)",
            "Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;"
        }
    .end annotation

    .line 62
    new-instance v8, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static provideLoggedInTaskWatcher(Lio/reactivex/Scheduler;Lcom/squareup/log/OhSnapLogger;Landroid/content/SharedPreferences;Lcom/google/gson/Gson;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;)Lcom/squareup/queue/TaskWatcher;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/log/OhSnapLogger;",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lio/reactivex/Scheduler;",
            ")",
            "Lcom/squareup/queue/TaskWatcher;"
        }
    .end annotation

    .line 69
    invoke-static/range {p0 .. p6}, Lcom/squareup/queue/QueueModule;->provideLoggedInTaskWatcher(Lio/reactivex/Scheduler;Lcom/squareup/log/OhSnapLogger;Landroid/content/SharedPreferences;Lcom/google/gson/Gson;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;)Lcom/squareup/queue/TaskWatcher;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/TaskWatcher;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/TaskWatcher;
    .locals 8

    .line 53
    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/log/OhSnapLogger;

    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/gson/Gson;

    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;->lastTaskRequiresRetryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;->fileThreadSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lio/reactivex/Scheduler;

    invoke-static/range {v1 .. v7}, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;->provideLoggedInTaskWatcher(Lio/reactivex/Scheduler;Lcom/squareup/log/OhSnapLogger;Landroid/content/SharedPreferences;Lcom/google/gson/Gson;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;)Lcom/squareup/queue/TaskWatcher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/queue/QueueModule_ProvideLoggedInTaskWatcherFactory;->get()Lcom/squareup/queue/TaskWatcher;

    move-result-object v0

    return-object v0
.end method
