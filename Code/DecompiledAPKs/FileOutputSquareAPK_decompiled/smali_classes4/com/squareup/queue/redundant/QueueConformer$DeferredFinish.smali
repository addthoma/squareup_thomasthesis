.class Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;
.super Ljava/lang/Object;
.source "QueueConformer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/redundant/QueueConformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeferredFinish"
.end annotation


# instance fields
.field private canceled:Z

.field private final runnable:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/squareup/queue/redundant/QueueConformer;


# direct methods
.method constructor <init>(Lcom/squareup/queue/redundant/QueueConformer;)V
    .locals 1

    .line 410
    iput-object p1, p0, Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;->this$0:Lcom/squareup/queue/redundant/QueueConformer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 412
    iget-object p1, p0, Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;->this$0:Lcom/squareup/queue/redundant/QueueConformer;

    new-instance v0, Lcom/squareup/queue/redundant/-$$Lambda$UmnbZOCChZV4AGIro5jU9WXKOR4;

    invoke-direct {v0, p1}, Lcom/squareup/queue/redundant/-$$Lambda$UmnbZOCChZV4AGIro5jU9WXKOR4;-><init>(Lcom/squareup/queue/redundant/QueueConformer;)V

    iput-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;->runnable:Ljava/lang/Runnable;

    const/4 p1, 0x0

    .line 414
    iput-boolean p1, p0, Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;->canceled:Z

    return-void
.end method


# virtual methods
.method cancel()V
    .locals 2

    const/4 v0, 0x1

    .line 424
    iput-boolean v0, p0, Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;->canceled:Z

    .line 425
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;->this$0:Lcom/squareup/queue/redundant/QueueConformer;

    invoke-static {v0}, Lcom/squareup/queue/redundant/QueueConformer;->access$000(Lcom/squareup/queue/redundant/QueueConformer;)Lcom/squareup/thread/executor/SerialExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;->runnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/SerialExecutor;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method

.method finish()V
    .locals 2

    .line 417
    iget-boolean v0, p0, Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;->canceled:Z

    if-eqz v0, :cond_0

    return-void

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;->this$0:Lcom/squareup/queue/redundant/QueueConformer;

    invoke-static {v0}, Lcom/squareup/queue/redundant/QueueConformer;->access$000(Lcom/squareup/queue/redundant/QueueConformer;)Lcom/squareup/thread/executor/SerialExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;->runnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/SerialExecutor;->post(Ljava/lang/Runnable;)V

    return-void
.end method
