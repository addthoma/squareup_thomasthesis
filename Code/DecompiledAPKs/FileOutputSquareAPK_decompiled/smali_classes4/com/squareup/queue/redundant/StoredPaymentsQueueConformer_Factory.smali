.class public final Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer_Factory;
.super Ljava/lang/Object;
.source "StoredPaymentsQueueConformer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;",
        ">;"
    }
.end annotation


# instance fields
.field private final corruptQueueRecorderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ">;"
        }
    .end annotation
.end field

.field private final fileThreadExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/SerialExecutor;",
            ">;"
        }
    .end annotation
.end field

.field private final redundantQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final tapeQueueListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/SerialExecutor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer_Factory;->redundantQueueProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer_Factory;->tapeQueueListenerProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer_Factory;->fileThreadExecutorProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer_Factory;->corruptQueueRecorderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/SerialExecutor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ">;)",
            "Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer_Factory;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/queue/CorruptQueueRecorder;)Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;",
            "Lcom/squareup/thread/executor/SerialExecutor;",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ")",
            "Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;"
        }
    .end annotation

    .line 55
    new-instance v0, Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;-><init>(Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/queue/CorruptQueueRecorder;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer_Factory;->redundantQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;

    iget-object v1, p0, Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer_Factory;->tapeQueueListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;

    iget-object v2, p0, Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer_Factory;->fileThreadExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/thread/executor/SerialExecutor;

    iget-object v3, p0, Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer_Factory;->corruptQueueRecorderProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/queue/CorruptQueueRecorder;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer_Factory;->newInstance(Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/queue/CorruptQueueRecorder;)Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer_Factory;->get()Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;

    move-result-object v0

    return-object v0
.end method
