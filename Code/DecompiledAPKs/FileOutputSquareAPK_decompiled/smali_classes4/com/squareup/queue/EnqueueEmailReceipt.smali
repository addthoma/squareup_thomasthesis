.class public Lcom/squareup/queue/EnqueueEmailReceipt;
.super Lcom/squareup/queue/PostPaymentTask;
.source "EnqueueEmailReceipt.java"


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final email:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/queue/EnqueueEmailReceipt;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/queue/PostPaymentTask;-><init>()V

    .line 20
    iget-object p1, p1, Lcom/squareup/queue/EnqueueEmailReceipt;->email:Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string p1, "REDACTED_email"

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lcom/squareup/queue/EnqueueEmailReceipt;->email:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/squareup/queue/PostPaymentTask;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/queue/EnqueueEmailReceipt;->email:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getEmail()Ljava/lang/String;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/queue/EnqueueEmailReceipt;->email:Ljava/lang/String;

    return-object v0
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 45
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/EnqueueEmailReceipt;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 7
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/EnqueueEmailReceipt;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/queue/EnqueueEmailReceipt;

    invoke-direct {v0, p0}, Lcom/squareup/queue/EnqueueEmailReceipt;-><init>(Lcom/squareup/queue/EnqueueEmailReceipt;)V

    return-object v0
.end method

.method protected taskFor(Ljava/lang/String;)Lcom/squareup/queue/retrofit/RetrofitTask;
    .locals 2

    .line 24
    new-instance v0, Lcom/squareup/queue/EmailReceipt$Builder;

    invoke-direct {v0}, Lcom/squareup/queue/EmailReceipt$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/queue/EnqueueEmailReceipt;->paymentType:Lcom/squareup/PaymentType;

    .line 25
    invoke-virtual {v0, p1, v1}, Lcom/squareup/queue/EmailReceipt$Builder;->paymentIdOrLegacyBillId(Ljava/lang/String;Lcom/squareup/PaymentType;)Lcom/squareup/queue/EmailReceipt$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/queue/EnqueueEmailReceipt;->email:Ljava/lang/String;

    .line 26
    invoke-virtual {p1, v0}, Lcom/squareup/queue/EmailReceipt$Builder;->email(Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;

    move-result-object p1

    .line 27
    invoke-virtual {p1}, Lcom/squareup/queue/EmailReceipt$Builder;->build()Lcom/squareup/queue/EmailReceipt;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EnqueueEmailReceipt{email=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/EnqueueEmailReceipt;->email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
