.class public interface abstract Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;
.super Ljava/lang/Object;
.source "MissedLoyaltyEnqueuer.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\"\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tH&\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;",
        "",
        "enqueueMissedLoyaltyOpportunity",
        "",
        "reasonForNoStars",
        "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "creatorDetails",
        "Lcom/squareup/protos/client/CreatorDetails;",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract enqueueMissedLoyaltyOpportunity(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;Lcom/squareup/payment/Transaction;Lcom/squareup/protos/client/CreatorDetails;)V
.end method
