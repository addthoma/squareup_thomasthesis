.class public final Lcom/squareup/queue/loyalty/RealMissedLoyaltyEnqueuer;
.super Ljava/lang/Object;
.source "RealMissedLoyaltyEnqueuer.kt"

# interfaces
.implements Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\"\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/queue/loyalty/RealMissedLoyaltyEnqueuer;",
        "Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;",
        "retrofitQueue",
        "Lcom/squareup/queue/retrofit/RetrofitQueue;",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "(Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/loyalty/LoyaltySettings;)V",
        "enqueueMissedLoyaltyOpportunity",
        "",
        "reasonForNoStars",
        "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "creatorDetails",
        "Lcom/squareup/protos/client/CreatorDetails;",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;


# direct methods
.method public constructor <init>(Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/loyalty/LoyaltySettings;)V
    .locals 1
    .param p1    # Lcom/squareup/queue/retrofit/RetrofitQueue;
        .annotation runtime Lcom/squareup/queue/Tasks;
        .end annotation
    .end param

    const-string v0, "retrofitQueue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltySettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/loyalty/RealMissedLoyaltyEnqueuer;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    iput-object p2, p0, Lcom/squareup/queue/loyalty/RealMissedLoyaltyEnqueuer;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    return-void
.end method


# virtual methods
.method public enqueueMissedLoyaltyOpportunity(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;Lcom/squareup/payment/Transaction;Lcom/squareup/protos/client/CreatorDetails;)V
    .locals 2

    const-string v0, "reasonForNoStars"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transaction"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/queue/loyalty/RealMissedLoyaltyEnqueuer;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->hasLoyaltyProgram()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->hasReceiptForLastPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23
    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object p2

    const-string v0, "transaction.requireReceiptForLastPayment()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/payment/PaymentReceipt;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object p2

    if-eqz p2, :cond_0

    iget-object p2, p2, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_1

    .line 28
    iget-object v0, p0, Lcom/squareup/queue/loyalty/RealMissedLoyaltyEnqueuer;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 29
    new-instance v1, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;

    invoke-direct {v1}, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;-><init>()V

    .line 30
    invoke-virtual {v1, p2}, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;->paymentId(Ljava/lang/String;)Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;

    move-result-object p2

    .line 32
    new-instance v1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;-><init>()V

    .line 33
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;->reason(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;

    move-result-object p1

    .line 34
    invoke-static {}, Lcom/squareup/util/ISO8601Dates;->now()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;->event_time(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;

    move-result-object p1

    .line 35
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;

    move-result-object p1

    .line 36
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity$Builder;->build()Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    move-result-object p1

    .line 31
    invoke-virtual {p2, p1}, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;->missedLoyaltyOpportunity(Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;)Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;

    move-result-object p1

    .line 38
    invoke-virtual {p1}, Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask$Builder;->build()Lcom/squareup/queue/loyalty/MissedLoyaltyOpportunityTask;

    move-result-object p1

    .line 28
    invoke-interface {v0, p1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method
