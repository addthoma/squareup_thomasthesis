.class final enum Lcom/squareup/queue/QueueService$ErrorType;
.super Ljava/lang/Enum;
.source "QueueService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/QueueService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ErrorType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/queue/QueueService$ErrorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/queue/QueueService$ErrorType;

.field public static final enum CLIENT:Lcom/squareup/queue/QueueService$ErrorType;

.field public static final enum NETWORK:Lcom/squareup/queue/QueueService$ErrorType;

.field public static final enum SERVER:Lcom/squareup/queue/QueueService$ErrorType;

.field public static final enum UNEXPECTED:Lcom/squareup/queue/QueueService$ErrorType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 574
    new-instance v0, Lcom/squareup/queue/QueueService$ErrorType;

    const/4 v1, 0x0

    const-string v2, "SERVER"

    invoke-direct {v0, v2, v1}, Lcom/squareup/queue/QueueService$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/queue/QueueService$ErrorType;->SERVER:Lcom/squareup/queue/QueueService$ErrorType;

    new-instance v0, Lcom/squareup/queue/QueueService$ErrorType;

    const/4 v2, 0x1

    const-string v3, "CLIENT"

    invoke-direct {v0, v3, v2}, Lcom/squareup/queue/QueueService$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/queue/QueueService$ErrorType;->CLIENT:Lcom/squareup/queue/QueueService$ErrorType;

    new-instance v0, Lcom/squareup/queue/QueueService$ErrorType;

    const/4 v3, 0x2

    const-string v4, "NETWORK"

    invoke-direct {v0, v4, v3}, Lcom/squareup/queue/QueueService$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/queue/QueueService$ErrorType;->NETWORK:Lcom/squareup/queue/QueueService$ErrorType;

    new-instance v0, Lcom/squareup/queue/QueueService$ErrorType;

    const/4 v4, 0x3

    const-string v5, "UNEXPECTED"

    invoke-direct {v0, v5, v4}, Lcom/squareup/queue/QueueService$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/queue/QueueService$ErrorType;->UNEXPECTED:Lcom/squareup/queue/QueueService$ErrorType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/queue/QueueService$ErrorType;

    .line 573
    sget-object v5, Lcom/squareup/queue/QueueService$ErrorType;->SERVER:Lcom/squareup/queue/QueueService$ErrorType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/queue/QueueService$ErrorType;->CLIENT:Lcom/squareup/queue/QueueService$ErrorType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/queue/QueueService$ErrorType;->NETWORK:Lcom/squareup/queue/QueueService$ErrorType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/queue/QueueService$ErrorType;->UNEXPECTED:Lcom/squareup/queue/QueueService$ErrorType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/queue/QueueService$ErrorType;->$VALUES:[Lcom/squareup/queue/QueueService$ErrorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 573
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/queue/QueueService$ErrorType;
    .locals 1

    .line 573
    const-class v0, Lcom/squareup/queue/QueueService$ErrorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/QueueService$ErrorType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/queue/QueueService$ErrorType;
    .locals 1

    .line 573
    sget-object v0, Lcom/squareup/queue/QueueService$ErrorType;->$VALUES:[Lcom/squareup/queue/QueueService$ErrorType;

    invoke-virtual {v0}, [Lcom/squareup/queue/QueueService$ErrorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/queue/QueueService$ErrorType;

    return-object v0
.end method
