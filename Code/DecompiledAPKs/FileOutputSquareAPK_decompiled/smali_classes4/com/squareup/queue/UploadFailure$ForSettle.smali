.class public final Lcom/squareup/queue/UploadFailure$ForSettle;
.super Lcom/squareup/queue/UploadFailure;
.source "UploadFailure.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/UploadFailure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ForSettle"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/UploadFailure<",
        "Lcom/squareup/queue/SettleBill;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/queue/SettleBill;)V
    .locals 1

    .line 66
    invoke-virtual {p1}, Lcom/squareup/queue/SettleBill;->secureCopyWithoutPIIForLogs()Lcom/squareup/queue/SettleBill;

    move-result-object p1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/squareup/queue/UploadFailure;-><init>(Lcom/squareup/queue/retrofit/RetrofitTask;Lcom/squareup/queue/UploadFailure$1;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-super {p0, p1}, Lcom/squareup/queue/UploadFailure;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method getMessagePrefix()Ljava/lang/String;
    .locals 1

    const-string v0, "FAILED TO PROCESS SETTLE: "

    return-object v0
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 74
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/UploadFailure$ForSettle;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/UploadFailure$ForSettle;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method
