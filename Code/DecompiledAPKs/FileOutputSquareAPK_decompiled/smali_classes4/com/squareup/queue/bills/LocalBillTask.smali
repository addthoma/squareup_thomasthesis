.class public abstract Lcom/squareup/queue/bills/LocalBillTask;
.super Lcom/squareup/queue/bills/BillTask;
.source "LocalBillTask.java"

# interfaces
.implements Lcom/squareup/queue/LocalPaymentRetrofitTask;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field transient addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final adjustments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation
.end field

.field transient billCreationService:Lcom/squareup/server/bills/BillCreationService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final billUuid:Ljava/lang/String;

.field private final cart:Lcom/squareup/protos/client/bills/Cart;

.field private final cashDrawerShiftId:Ljava/lang/String;

.field private final employeeToken:Ljava/lang/String;

.field private final itemizations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation
.end field

.field transient lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;
    .annotation runtime Lcom/squareup/settings/LastLocalPaymentServerId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient localTenderCache:Lcom/squareup/print/LocalTenderCache;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final merchantToken:Ljava/lang/String;

.field private requestBillId:Lcom/squareup/protos/client/IdPair;

.field private final singleTenderType:Lcom/squareup/protos/client/bills/Tender$Type;

.field transient tenderProtoFactory:Lcom/squareup/payment/tender/TenderProtoFactory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final tenderUuid:Ljava/lang/String;

.field private final time:J

.field private final tip:Lcom/squareup/protos/common/Money;

.field private final total:Lcom/squareup/protos/common/Money;

.field transient transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/protos/client/bills/Tender$Type;Ljava/lang/String;Ljava/lang/String;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Tender$Type;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 79
    invoke-direct {p0, p12}, Lcom/squareup/queue/bills/BillTask;-><init>(Ljava/lang/String;)V

    .line 80
    iput-object p1, p0, Lcom/squareup/queue/bills/LocalBillTask;->singleTenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 81
    iput-object p2, p0, Lcom/squareup/queue/bills/LocalBillTask;->billUuid:Ljava/lang/String;

    .line 82
    iput-object p3, p0, Lcom/squareup/queue/bills/LocalBillTask;->tenderUuid:Ljava/lang/String;

    .line 83
    iput-wide p4, p0, Lcom/squareup/queue/bills/LocalBillTask;->time:J

    .line 84
    iput-object p6, p0, Lcom/squareup/queue/bills/LocalBillTask;->total:Lcom/squareup/protos/common/Money;

    .line 85
    iput-object p7, p0, Lcom/squareup/queue/bills/LocalBillTask;->tip:Lcom/squareup/protos/common/Money;

    .line 86
    iput-object p10, p0, Lcom/squareup/queue/bills/LocalBillTask;->merchantToken:Ljava/lang/String;

    .line 87
    iput-object p11, p0, Lcom/squareup/queue/bills/LocalBillTask;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 88
    iput-object p8, p0, Lcom/squareup/queue/bills/LocalBillTask;->itemizations:Ljava/util/List;

    .line 89
    iput-object p9, p0, Lcom/squareup/queue/bills/LocalBillTask;->adjustments:Ljava/util/List;

    .line 90
    iput-object p13, p0, Lcom/squareup/queue/bills/LocalBillTask;->cashDrawerShiftId:Ljava/lang/String;

    .line 91
    iput-object p14, p0, Lcom/squareup/queue/bills/LocalBillTask;->employeeToken:Ljava/lang/String;

    .line 92
    invoke-static {p11, p6, p7}, Lcom/squareup/queue/bills/LocalBillTask;->sanityCheck(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method private cleanBillAndTenderServerId()V
    .locals 2

    .line 327
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private getCachedBillServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 311
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/AddTendersRequestServerIds;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 312
    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/settings/AddTendersRequestServerIds;->getBillServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private getCachedTenderServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 316
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/AddTendersRequestServerIds;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 317
    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/settings/AddTendersRequestServerIds;->getTenderServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private saveServerIds(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V
    .locals 1

    .line 321
    new-instance v0, Lcom/squareup/settings/AddTendersRequestServerIds;

    invoke-direct {v0, p1, p2}, Lcom/squareup/settings/AddTendersRequestServerIds;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V

    .line 323
    iget-object p1, p0, Lcom/squareup/queue/bills/LocalBillTask;->addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public asBill(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 9

    .line 102
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 103
    new-instance v5, Ljava/util/Date;

    iget-wide v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->time:J

    invoke-direct {v5, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 104
    invoke-virtual {p0, v5}, Lcom/squareup/queue/bills/LocalBillTask;->asTenderHistory(Ljava/util/Date;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    invoke-virtual {p0}, Lcom/squareup/queue/bills/LocalBillTask;->getItemizations()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/server/payment/AdjusterMessages;->fromItemizationMessages(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/billhistory/Bills;->createBillNote(Ljava/util/List;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    move-result-object v6

    .line 107
    new-instance p1, Lcom/squareup/billhistory/model/BillHistory$Builder;

    invoke-virtual {p0}, Lcom/squareup/queue/bills/LocalBillTask;->getBillUuid()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->forPayment(Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v1

    invoke-static {p0}, Lcom/squareup/payment/OrderTaskHelper;->orderSnapshotForTask(Lcom/squareup/queue/PaymentTask;)Lcom/squareup/payment/OrderSnapshot;

    move-result-object v3

    .line 108
    invoke-virtual {p0}, Lcom/squareup/queue/bills/LocalBillTask;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v4

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistoryId;Ljava/util/List;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;Ljava/util/List;Z)V

    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 109
    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setCart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    .line 110
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    return-object p1
.end method

.method protected abstract asTenderHistory(Ljava/util/Date;)Lcom/squareup/billhistory/model/TenderHistory;
.end method

.method public callOnRpcThread()Lcom/squareup/protos/client/bills/CompleteBillResponse;
    .locals 7

    .line 189
    invoke-virtual {p0}, Lcom/squareup/queue/bills/LocalBillTask;->getBillUuid()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/queue/bills/LocalBillTask;->getCachedBillServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    .line 190
    invoke-virtual {p0}, Lcom/squareup/queue/bills/LocalBillTask;->getTenderUuid()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/queue/bills/LocalBillTask;->getCachedTenderServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v0, :cond_1

    .line 195
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    const-string v1, ""

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 197
    invoke-virtual {p0}, Lcom/squareup/queue/bills/LocalBillTask;->getAddTendersRequest()Lcom/squareup/protos/client/bills/AddTendersRequest;

    move-result-object v0

    .line 199
    iget-object v1, p0, Lcom/squareup/queue/bills/LocalBillTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-interface {v1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logAddTendersRequest(Lcom/squareup/protos/client/bills/AddTendersRequest;)V

    .line 201
    iget-object v1, p0, Lcom/squareup/queue/bills/LocalBillTask;->billCreationService:Lcom/squareup/server/bills/BillCreationService;

    .line 202
    invoke-interface {v1, v0, v2}, Lcom/squareup/server/bills/BillCreationService;->addTenders(Lcom/squareup/protos/client/bills/AddTendersRequest;Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddTendersResponse;

    move-result-object v0

    .line 203
    iget-object v1, p0, Lcom/squareup/queue/bills/LocalBillTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-interface {v1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logAddTendersResponse(Lcom/squareup/protos/client/bills/AddTendersResponse;)V

    .line 205
    iget-object v1, v0, Lcom/squareup/protos/client/bills/AddTendersResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v1, v1, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 211
    iget-object v1, v0, Lcom/squareup/protos/client/bills/AddTendersResponse;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 212
    iget-object v3, v0, Lcom/squareup/protos/client/bills/AddTendersResponse;->tender:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/AddedTender;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 216
    iget-object v5, p0, Lcom/squareup/queue/bills/LocalBillTask;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    iget-object v6, v3, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-interface {v5, v6}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 220
    invoke-direct {p0, v1, v3}, Lcom/squareup/queue/bills/LocalBillTask;->saveServerIds(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V

    .line 224
    iget-object v5, p0, Lcom/squareup/queue/bills/LocalBillTask;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    invoke-virtual {p0}, Lcom/squareup/queue/bills/LocalBillTask;->getUuid()Ljava/lang/String;

    move-result-object v6

    iget-object v0, v0, Lcom/squareup/protos/client/bills/AddTendersResponse;->tender:Ljava/util/List;

    .line 225
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/AddedTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->read_only_receipt_number:Ljava/lang/String;

    iget-object v4, v3, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 224
    invoke-virtual {v5, v6, v0, v4}, Lcom/squareup/print/LocalTenderCache;->setLast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    move-object v1, v3

    goto :goto_0

    .line 206
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/AddTendersResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AddTenders call failed for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 208
    throw v1

    .line 229
    :cond_1
    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/squareup/queue/bills/LocalBillTask;->getCompleteBillRequest(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CompleteBillRequest;

    move-result-object v0

    .line 231
    iget-object v1, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, p0, Lcom/squareup/queue/bills/LocalBillTask;->requestBillId:Lcom/squareup/protos/client/IdPair;

    .line 233
    iget-object v1, p0, Lcom/squareup/queue/bills/LocalBillTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-interface {v1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logCompleteBillEnqueued(Lcom/squareup/protos/client/bills/CompleteBillRequest;)V

    .line 235
    iget-object v1, p0, Lcom/squareup/queue/bills/LocalBillTask;->billCreationService:Lcom/squareup/server/bills/BillCreationService;

    .line 236
    invoke-interface {v1, v0, v2}, Lcom/squareup/server/bills/BillCreationService;->completeBill(Lcom/squareup/protos/client/bills/CompleteBillRequest;Ljava/lang/String;)Lcom/squareup/protos/client/bills/CompleteBillResponse;

    move-result-object v0

    .line 237
    iget-object v1, p0, Lcom/squareup/queue/bills/LocalBillTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-interface {v1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logCompleteBillResponse(Lcom/squareup/protos/client/bills/CompleteBillResponse;)V

    .line 239
    iget-object v1, v0, Lcom/squareup/protos/client/bills/CompleteBillResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v1, v1, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 246
    invoke-direct {p0}, Lcom/squareup/queue/bills/LocalBillTask;->cleanBillAndTenderServerId()V

    return-object v0

    .line 240
    :cond_2
    new-instance v1, Ljava/lang/RuntimeException;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CompleteBillResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CompleteBill call failed for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 242
    throw v1
.end method

.method public bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 50
    invoke-virtual {p0}, Lcom/squareup/queue/bills/LocalBillTask;->callOnRpcThread()Lcom/squareup/protos/client/bills/CompleteBillResponse;

    move-result-object v0

    return-object v0
.end method

.method public getAddTendersRequest()Lcom/squareup/protos/client/bills/AddTendersRequest;
    .locals 5

    .line 117
    new-instance v0, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/queue/bills/LocalBillTask;->tenderUuid:Ljava/lang/String;

    .line 118
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v0

    .line 119
    invoke-virtual {v0}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    .line 121
    iget-object v1, p0, Lcom/squareup/queue/bills/LocalBillTask;->tenderProtoFactory:Lcom/squareup/payment/tender/TenderProtoFactory;

    invoke-interface {v1}, Lcom/squareup/payment/tender/TenderProtoFactory;->startTender()Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v1

    .line 122
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/Tender$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/queue/bills/LocalBillTask;->singleTenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 123
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Tender$Builder;->type(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v1

    .line 124
    invoke-virtual {p0}, Lcom/squareup/queue/bills/LocalBillTask;->getSingleTenderMethod()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Tender$Builder;->method(Lcom/squareup/protos/client/bills/Tender$Method;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v1

    .line 125
    invoke-virtual {p0}, Lcom/squareup/queue/bills/LocalBillTask;->getDate()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Tender$Builder;->tendered_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/queue/bills/LocalBillTask;->total:Lcom/squareup/protos/common/Money;

    .line 127
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/queue/bills/LocalBillTask;->tip:Lcom/squareup/protos/common/Money;

    .line 128
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;

    move-result-object v2

    .line 129
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Amounts;

    move-result-object v2

    .line 126
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Tender$Builder;->amounts(Lcom/squareup/protos/client/bills/Tender$Amounts;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/queue/bills/LocalBillTask;->cashDrawerShiftId:Ljava/lang/String;

    .line 130
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Tender$Builder;->write_only_client_cash_drawer_shift_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v1

    .line 132
    iget-object v2, p0, Lcom/squareup/queue/bills/LocalBillTask;->employeeToken:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 133
    new-instance v2, Lcom/squareup/protos/client/CreatorDetails$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/CreatorDetails$Builder;-><init>()V

    new-instance v3, Lcom/squareup/protos/client/Employee$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/Employee$Builder;-><init>()V

    iget-object v4, p0, Lcom/squareup/queue/bills/LocalBillTask;->employeeToken:Ljava/lang/String;

    .line 135
    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/Employee$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v3

    .line 136
    invoke-virtual {v3}, Lcom/squareup/protos/client/Employee$Builder;->build()Lcom/squareup/protos/client/Employee;

    move-result-object v3

    .line 134
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/CreatorDetails$Builder;->employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/CreatorDetails$Builder;

    move-result-object v2

    .line 137
    invoke-virtual {v2}, Lcom/squareup/protos/client/CreatorDetails$Builder;->build()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v2

    .line 138
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Tender$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Tender$Builder;

    .line 141
    :cond_0
    new-instance v2, Lcom/squareup/protos/client/bills/AddTender$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/AddTender$Builder;-><init>()V

    .line 142
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$Builder;->build()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/squareup/protos/client/bills/AddTender$Builder;->tender(Lcom/squareup/protos/client/bills/Tender;)Lcom/squareup/protos/client/bills/AddTender$Builder;

    move-result-object v1

    .line 143
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/AddTender$Builder;->build()Lcom/squareup/protos/client/bills/AddTender;

    move-result-object v1

    .line 145
    new-instance v2, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;-><init>()V

    .line 146
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;

    move-result-object v0

    const/4 v2, 0x0

    .line 147
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;->is_canceled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;

    move-result-object v0

    .line 150
    new-instance v2, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/queue/bills/LocalBillTask;->merchantToken:Ljava/lang/String;

    .line 151
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object v2

    new-instance v3, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 153
    invoke-virtual {p0}, Lcom/squareup/queue/bills/LocalBillTask;->getBillUuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v3

    .line 154
    invoke-virtual {v3}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 152
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object v2

    .line 155
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->add_tender(Ljava/util/List;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object v1

    .line 156
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_info(Ljava/util/List;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/bills/LocalBillTask;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 157
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object v0

    .line 158
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersRequest;

    move-result-object v0

    return-object v0
.end method

.method public getAdjustments()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation

    .line 294
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->adjustments:Ljava/util/List;

    return-object v0
.end method

.method public getBillId()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 252
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->requestBillId:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public getBillUuid()Ljava/lang/String;
    .locals 1

    .line 298
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->billUuid:Ljava/lang/String;

    return-object v0
.end method

.method public getCart()Lcom/squareup/protos/client/bills/Cart;
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object v0
.end method

.method public getCompleteBillRequest(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CompleteBillRequest;
    .locals 3

    .line 163
    invoke-virtual {p0}, Lcom/squareup/queue/bills/LocalBillTask;->getDate()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v0

    .line 165
    new-instance v1, Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;-><init>()V

    .line 166
    invoke-virtual {v1, p2}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    move-result-object p2

    new-instance v1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/queue/bills/LocalBillTask;->total:Lcom/squareup/protos/common/Money;

    .line 168
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->total_charged_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/queue/bills/LocalBillTask;->tip:Lcom/squareup/protos/common/Money;

    .line 169
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    move-result-object v1

    .line 170
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    move-result-object v1

    .line 167
    invoke-virtual {p2, v1}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->amounts(Lcom/squareup/protos/client/bills/CompleteTender$Amounts;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    move-result-object p2

    .line 171
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->build()Lcom/squareup/protos/client/bills/CompleteTender;

    move-result-object p2

    .line 173
    new-instance v1, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;-><init>()V

    .line 174
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object p1

    new-instance v1, Lcom/squareup/protos/client/Merchant$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/Merchant$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/queue/bills/LocalBillTask;->merchantToken:Ljava/lang/String;

    .line 176
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/Merchant$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/Merchant$Builder;

    move-result-object v1

    .line 177
    invoke-virtual {v1}, Lcom/squareup/protos/client/Merchant$Builder;->build()Lcom/squareup/protos/client/Merchant;

    move-result-object v1

    .line 175
    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->merchant(Lcom/squareup/protos/client/Merchant;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object p1

    .line 178
    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->tender(Ljava/util/List;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/queue/bills/LocalBillTask;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 179
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;-><init>()V

    .line 181
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Bill$Dates$Builder;

    move-result-object p2

    .line 182
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->completed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Bill$Dates$Builder;

    move-result-object p2

    .line 183
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->build()Lcom/squareup/protos/client/bills/Bill$Dates;

    move-result-object p2

    .line 180
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->dates(Lcom/squareup/protos/client/bills/Bill$Dates;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->COMPLETE_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    .line 184
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->complete_bill_type(Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object p1

    .line 185
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->build()Lcom/squareup/protos/client/bills/CompleteBillRequest;

    move-result-object p1

    return-object p1
.end method

.method public getDate()Lcom/squareup/protos/client/ISO8601Date;
    .locals 4

    .line 96
    new-instance v0, Lcom/squareup/protos/client/ISO8601Date$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ISO8601Date$Builder;-><init>()V

    new-instance v1, Ljava/util/Date;

    iget-wide v2, p0, Lcom/squareup/queue/bills/LocalBillTask;->time:J

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 97
    invoke-static {v1}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/ISO8601Date$Builder;->date_string(Ljava/lang/String;)Lcom/squareup/protos/client/ISO8601Date$Builder;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lcom/squareup/protos/client/ISO8601Date$Builder;->build()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v0

    return-object v0
.end method

.method public getEmployeeToken()Ljava/lang/String;
    .locals 1

    .line 307
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->employeeToken:Ljava/lang/String;

    return-object v0
.end method

.method public getItemizations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation

    .line 290
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->itemizations:Ljava/util/List;

    return-object v0
.end method

.method public getMerchantToken()Ljava/lang/String;
    .locals 1

    .line 274
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->merchantToken:Ljava/lang/String;

    return-object v0
.end method

.method protected abstract getSingleTenderMethod()Lcom/squareup/protos/client/bills/Tender$Method;
.end method

.method public getSingleTenderType()Lcom/squareup/protos/client/bills/Tender$Type;
    .locals 1

    .line 270
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->singleTenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object v0
.end method

.method public getTenderUuid()Ljava/lang/String;
    .locals 1

    .line 302
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->tenderUuid:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .line 266
    iget-wide v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->time:J

    return-wide v0
.end method

.method public getTip()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 286
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->tip:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getTotal()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 282
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->total:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getUuid()Ljava/lang/String;
    .locals 1

    .line 262
    iget-object v0, p0, Lcom/squareup/queue/bills/LocalBillTask;->billUuid:Ljava/lang/String;

    return-object v0
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method
