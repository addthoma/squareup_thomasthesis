.class public final Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;
.super Ljava/lang/Object;
.source "NoSaleBillTask_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/queue/bills/NoSaleBillTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final addTendersRequestServerIdsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;>;"
        }
    .end annotation
.end field

.field private final billCreationServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;"
        }
    .end annotation
.end field

.field private final lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final localTenderCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/LocalTenderCache;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final rpcSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderProtoFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderProtoFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/LocalTenderCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderProtoFactory;",
            ">;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p2, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p3, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->ticketsProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p4, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p5, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->addTendersRequestServerIdsProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p6, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->billCreationServiceProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p7, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p8, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->localTenderCacheProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p9, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->tenderProtoFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/LocalTenderCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderProtoFactory;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/queue/bills/NoSaleBillTask;",
            ">;"
        }
    .end annotation

    .line 66
    new-instance v10, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/bills/NoSaleBillTask;)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectMainScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectRpcScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/Tickets;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/BillTask_MembersInjector;->injectTickets(Lcom/squareup/queue/bills/BillTask;Lcom/squareup/tickets/Tickets;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->injectLastLocalPaymentServerId(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/settings/LocalSetting;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->addTendersRequestServerIdsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->injectAddTendersRequestServerIds(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/settings/LocalSetting;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->billCreationServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/bills/BillCreationService;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->injectBillCreationService(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/server/bills/BillCreationService;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->injectTransactionLedgerManager(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->localTenderCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/LocalTenderCache;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->injectLocalTenderCache(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/print/LocalTenderCache;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->tenderProtoFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/TenderProtoFactory;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/LocalBillTask_MembersInjector;->injectTenderProtoFactory(Lcom/squareup/queue/bills/LocalBillTask;Lcom/squareup/payment/tender/TenderProtoFactory;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/queue/bills/NoSaleBillTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/NoSaleBillTask_MembersInjector;->injectMembers(Lcom/squareup/queue/bills/NoSaleBillTask;)V

    return-void
.end method
