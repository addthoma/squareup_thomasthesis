.class public Lcom/squareup/queue/bills/NoSaleBillTask;
.super Lcom/squareup/queue/bills/LocalBillTask;
.source "NoSaleBillTask.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final serialVersionUID:J


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;JLcom/squareup/protos/common/CurrencyCode;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 15

    .line 22
    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->NO_SALE:Lcom/squareup/protos/client/bills/Tender$Type;

    const-wide/16 v2, 0x0

    move-object/from16 v0, p5

    invoke-static {v2, v3, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    move-object/from16 v13, p9

    move-object/from16 v14, p10

    invoke-direct/range {v0 .. v14}, Lcom/squareup/queue/bills/LocalBillTask;-><init>(Lcom/squareup/protos/client/bills/Tender$Type;Ljava/lang/String;Ljava/lang/String;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected asTenderHistory(Ljava/util/Date;)Lcom/squareup/billhistory/model/TenderHistory;
    .locals 2

    .line 27
    new-instance v0, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;-><init>()V

    .line 28
    invoke-virtual {p0}, Lcom/squareup/queue/bills/NoSaleBillTask;->getBillUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;->id(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;

    .line 29
    invoke-virtual {p0}, Lcom/squareup/queue/bills/NoSaleBillTask;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;

    move-result-object v0

    .line 30
    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;->timestamp(Ljava/util/Date;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;

    .line 31
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/NoSaleTenderHistory$Builder;->build()Lcom/squareup/billhistory/model/NoSaleTenderHistory;

    move-result-object p1

    return-object p1
.end method

.method protected getSingleTenderMethod()Lcom/squareup/protos/client/bills/Tender$Method;
    .locals 2

    .line 35
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/bills/NoSaleTender$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/NoSaleTender$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/NoSaleTender$Builder;->build()Lcom/squareup/protos/client/bills/NoSaleTender;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->no_sale_tender(Lcom/squareup/protos/client/bills/NoSaleTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v0

    return-object v0
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 39
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/bills/NoSaleBillTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/NoSaleBillTask;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method
