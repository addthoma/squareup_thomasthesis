.class public Lcom/squareup/queue/QueueService$Starter;
.super Ljava/lang/Object;
.source "QueueService.java"

# interfaces
.implements Lcom/squareup/queue/QueueServiceStarter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/QueueService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Starter"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final foregroundServiceStarter:Lcom/squareup/foregroundservice/ForegroundServiceStarter;

.field paused:Z


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/foregroundservice/ForegroundServiceStarter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456
    iput-object p1, p0, Lcom/squareup/queue/QueueService$Starter;->context:Landroid/content/Context;

    .line 457
    iput-object p2, p0, Lcom/squareup/queue/QueueService$Starter;->foregroundServiceStarter:Lcom/squareup/foregroundservice/ForegroundServiceStarter;

    const/4 p1, 0x0

    .line 458
    iput-boolean p1, p0, Lcom/squareup/queue/QueueService$Starter;->paused:Z

    return-void
.end method

.method private createQueueServiceIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .line 478
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/squareup/queue/QueueService$Starter;->context:Landroid/content/Context;

    const-class v2, Lcom/squareup/queue/QueueService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "reason"

    .line 479
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method isDebugBuild()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isPaused()Z
    .locals 1

    .line 484
    invoke-virtual {p0}, Lcom/squareup/queue/QueueService$Starter;->isDebugBuild()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/queue/QueueService$Starter;->paused:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public pause()V
    .locals 1

    .line 488
    invoke-virtual {p0}, Lcom/squareup/queue/QueueService$Starter;->isDebugBuild()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 491
    iput-boolean v0, p0, Lcom/squareup/queue/QueueService$Starter;->paused:Z

    return-void
.end method

.method public resume()V
    .locals 1

    .line 495
    invoke-virtual {p0}, Lcom/squareup/queue/QueueService$Starter;->isDebugBuild()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 498
    iput-boolean v0, p0, Lcom/squareup/queue/QueueService$Starter;->paused:Z

    const-string v0, "Resumed"

    .line 499
    invoke-virtual {p0, v0}, Lcom/squareup/queue/QueueService$Starter;->start(Ljava/lang/String;)V

    return-void
.end method

.method public start(Ljava/lang/String;)V
    .locals 1

    .line 462
    invoke-virtual {p0}, Lcom/squareup/queue/QueueService$Starter;->isDebugBuild()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/queue/QueueService$Starter;->paused:Z

    if-eqz v0, :cond_0

    return-void

    .line 465
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/queue/QueueService$Starter;->createQueueServiceIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    .line 466
    iget-object v0, p0, Lcom/squareup/queue/QueueService$Starter;->foregroundServiceStarter:Lcom/squareup/foregroundservice/ForegroundServiceStarter;

    invoke-interface {v0, p1}, Lcom/squareup/foregroundservice/ForegroundServiceStarter;->startInForeground(Landroid/content/Intent;)V

    return-void
.end method

.method public startWaitingForForeground(Ljava/lang/String;)V
    .locals 1

    .line 470
    invoke-virtual {p0}, Lcom/squareup/queue/QueueService$Starter;->isDebugBuild()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/queue/QueueService$Starter;->paused:Z

    if-eqz v0, :cond_0

    return-void

    .line 473
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/queue/QueueService$Starter;->createQueueServiceIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    .line 474
    iget-object v0, p0, Lcom/squareup/queue/QueueService$Starter;->foregroundServiceStarter:Lcom/squareup/foregroundservice/ForegroundServiceStarter;

    invoke-interface {v0, p1}, Lcom/squareup/foregroundservice/ForegroundServiceStarter;->startWaitingForForeground(Landroid/content/Intent;)V

    return-void
.end method
