.class Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;
.super Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;
.source "SalesReportRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CategorySalesRowViewHolder"
.end annotation


# instance fields
.field private final caretView:Landroid/view/View;

.field private final nameView:Landroid/widget/TextView;

.field private final priceView:Landroid/widget/TextView;

.field private final quantityView:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

.field private final threeColumns:Z


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V
    .locals 1

    .line 582
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    .line 583
    invoke-direct {p0, p2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 584
    sget v0, Lcom/squareup/reports/applet/R$id;->sales_category_name:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->nameView:Landroid/widget/TextView;

    .line 585
    sget v0, Lcom/squareup/reports/applet/R$id;->sales_category_quantity:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->quantityView:Landroid/widget/TextView;

    .line 586
    sget v0, Lcom/squareup/reports/applet/R$id;->sales_category_price:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->priceView:Landroid/widget/TextView;

    .line 587
    sget v0, Lcom/squareup/reports/applet/R$id;->sales_category_caret:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->caretView:Landroid/view/View;

    .line 588
    instance-of v0, p2, Lcom/squareup/widgets/HorizontalThreeChildLayout;

    iput-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->threeColumns:Z

    .line 589
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder$1;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;)V
    .locals 0

    .line 575
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->onClick()V

    return-void
.end method

.method private onClick()V
    .locals 3

    .line 597
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->getPosition()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$1300(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 598
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$000(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getCategorySalesRow(I)Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;

    move-result-object v0

    .line 599
    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->caretView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->rotation(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 602
    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->caretView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->rotation(F)Landroid/view/ViewPropertyAnimator;

    .line 604
    :goto_0
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$000(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->getPosition()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {v2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$1300(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->onCategoryToggled(I)V

    return-void
.end method


# virtual methods
.method public bind()V
    .locals 4

    .line 608
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->getPosition()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$1300(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 609
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$000(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getCategorySalesRow(I)Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;

    move-result-object v0

    .line 610
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->nameView:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->formattedName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 611
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->quantityView:Landroid/widget/TextView;

    iget-boolean v3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->threeColumns:Z

    invoke-virtual {v1, v2, v0, v3}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->setQuantityViewText(Landroid/widget/TextView;Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;Z)V

    .line 612
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->priceView:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->formattedMoney:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 613
    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->caretView:Landroid/view/View;

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    goto :goto_0

    .line 616
    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;->caretView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    :goto_0
    return-void
.end method
