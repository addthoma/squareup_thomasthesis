.class public final Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter_Factory;
.super Ljava/lang/Object;
.source "ChartDateFormatter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;",
        ">;"
    }
.end annotation


# instance fields
.field private final dateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final shortDateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final shortNoYearDateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter_Factory;->shortNoYearDateFormatProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter_Factory;->shortDateFormatProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter_Factory;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter_Factory;"
        }
    .end annotation

    .line 49
    new-instance v6, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/util/Res;Ljava/util/Locale;)Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;
    .locals 7

    .line 54
    new-instance v6, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;-><init>(Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/util/Res;Ljava/util/Locale;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;
    .locals 5

    .line 42
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter_Factory;->shortNoYearDateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter_Factory;->shortDateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/text/DateFormat;

    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v4, p0, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Locale;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter_Factory;->newInstance(Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/util/Res;Ljava/util/Locale;)Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter_Factory;->get()Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;

    move-result-object v0

    return-object v0
.end method
