.class public final Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder_Factory;
.super Ljava/lang/Object;
.source "ReportConfigBuilder_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final timeFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder_Factory;->timeFormatProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder_Factory;->resProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder_Factory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;
    .locals 1

    .line 49
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;-><init>(Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder_Factory;->timeFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder_Factory;->newInstance(Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder_Factory;->get()Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

    move-result-object v0

    return-object v0
.end method
