.class public Lcom/squareup/reports/applet/sales/v1/ReportConfigScope;
.super Lcom/squareup/reports/applet/InReportsAppletScope;
.source "ReportConfigScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v1/ReportConfigScope$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScope;

    invoke-direct {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScope;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScope;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigScope;

    .line 33
    sget-object v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScope;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/reports/applet/InReportsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 2

    .line 21
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    const-class v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigScope$Component;

    .line 22
    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigScope$Component;

    invoke-interface {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScope$Component;->editReportConfigRunner()Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    move-result-object p1

    .line 21
    invoke-virtual {v0, p1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    return-void
.end method
