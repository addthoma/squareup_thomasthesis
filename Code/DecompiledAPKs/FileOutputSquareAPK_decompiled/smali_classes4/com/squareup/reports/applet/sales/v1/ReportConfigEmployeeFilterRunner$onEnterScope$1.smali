.class final Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$onEnterScope$1;
.super Ljava/lang/Object;
.source "ReportConfigEmployeeFilterRunner.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReportConfigEmployeeFilterRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReportConfigEmployeeFilterRunner.kt\ncom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$onEnterScope$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Flows.kt\ncom/squareup/container/Flows\n*L\n1#1,105:1\n704#2:106\n777#2,2:107\n1360#2:109\n1429#2,3:110\n75#3:113\n*E\n*S KotlinDebug\n*F\n+ 1 ReportConfigEmployeeFilterRunner.kt\ncom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$onEnterScope$1\n*L\n54#1:106\n54#1,2:107\n55#1:109\n55#1,3:110\n63#1:113\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "result",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterResult;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$onEnterScope$1;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterResult;)V
    .locals 3

    .line 52
    instance-of v0, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterResult$CommitSelectedEmployees;

    if-eqz v0, :cond_3

    .line 53
    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterResult$CommitSelectedEmployees;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterResult$CommitSelectedEmployees;->getListedEmployees()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 107
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;

    .line 54
    invoke-virtual {v2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 108
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 109
    new-instance p1, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 110
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 111
    check-cast v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;

    .line 55
    invoke-virtual {v1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 112
    :cond_2
    check-cast p1, Ljava/util/List;

    .line 56
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$onEnterScope$1;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    invoke-static {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->access$getReportConfigRunner$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->setEmployeeFilter(Ljava/util/List;)V

    goto :goto_2

    .line 58
    :cond_3
    instance-of v0, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterResult$CommitAllEmployees;

    if-eqz v0, :cond_4

    .line 59
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$onEnterScope$1;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    invoke-static {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->access$getReportConfigRunner$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    move-result-object p1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->setEmployeeFilter(Ljava/util/List;)V

    goto :goto_2

    .line 61
    :cond_4
    instance-of p1, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterResult$Cancel;

    .line 63
    :goto_2
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$onEnterScope$1;->this$0:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    invoke-static {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;->access$getFlow$p(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;)Lflow/Flow;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    .line 113
    const-class v2, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterResult;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner$onEnterScope$1;->call(Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterResult;)V

    return-void
.end method
