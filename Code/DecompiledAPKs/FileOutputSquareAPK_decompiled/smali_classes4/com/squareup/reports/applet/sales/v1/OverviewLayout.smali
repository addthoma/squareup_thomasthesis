.class public Lcom/squareup/reports/applet/sales/v1/OverviewLayout;
.super Landroid/view/ViewGroup;
.source "OverviewLayout.java"


# instance fields
.field private horizontal:Z

.field private final horizontalGap:I

.field private final verticalGap:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 33
    sget p2, Lcom/squareup/marin/R$dimen;->marin_sales_overview_horizontal_gap:I

    .line 34
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->horizontalGap:I

    .line 35
    sget p2, Lcom/squareup/marin/R$dimen;->marin_sales_overview_vertical_gap:I

    .line 36
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->verticalGap:I

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 3

    .line 76
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->getChildCount()I

    move-result p1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->getPaddingLeft()I

    move-result p2

    .line 79
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->getPaddingTop()I

    move-result p3

    const/4 p4, 0x0

    :goto_0
    if-ge p4, p1, :cond_1

    .line 81
    invoke-virtual {p0, p4}, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p5

    .line 82
    iget-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->horizontal:Z

    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {p5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 84
    invoke-virtual {p5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int v2, p2, v0

    add-int/2addr v1, p3

    .line 85
    invoke-virtual {p5, p2, p3, v2, v1}, Landroid/view/View;->layout(IIII)V

    .line 86
    iget p5, p0, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->horizontalGap:I

    add-int/2addr v0, p5

    add-int/2addr p2, v0

    goto :goto_1

    .line 88
    :cond_0
    invoke-virtual {p5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 89
    invoke-virtual {p5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, p2

    add-int v2, p3, v1

    .line 90
    invoke-virtual {p5, p2, p3, v0, v2}, Landroid/view/View;->layout(IIII)V

    .line 91
    iget p5, p0, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->verticalGap:I

    add-int/2addr v1, p5

    add-int/2addr p3, v1

    :goto_1
    add-int/lit8 p4, p4, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 12

    .line 40
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->getChildCount()I

    move-result v0

    .line 42
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    .line 43
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    sub-int v1, p1, v1

    const/high16 v2, -0x80000000

    .line 44
    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/4 v3, 0x0

    move v6, v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_0
    if-ge v4, v0, :cond_1

    .line 51
    invoke-virtual {p0, v4}, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 52
    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v10

    const/16 v11, 0x8

    if-ne v10, v11, :cond_0

    add-int/lit8 v6, v6, -0x1

    .line 55
    :cond_0
    invoke-virtual {v9, v2, p2}, Landroid/view/View;->measure(II)V

    .line 56
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    .line 57
    invoke-static {v8, v10}, Ljava/lang/Math;->max(II)I

    move-result v8

    add-int/2addr v7, v10

    .line 59
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v5, v9

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    const/4 p2, 0x1

    sub-int/2addr v6, p2

    .line 61
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->horizontalGap:I

    mul-int v0, v0, v6

    add-int/2addr v5, v0

    .line 62
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->verticalGap:I

    mul-int v6, v6, v0

    add-int/2addr v7, v6

    .line 64
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->getPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    if-ge v5, v1, :cond_2

    .line 67
    iput-boolean p2, p0, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->horizontal:Z

    add-int/2addr v8, v0

    .line 68
    invoke-virtual {p0, p1, v8}, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->setMeasuredDimension(II)V

    goto :goto_1

    .line 70
    :cond_2
    iput-boolean v3, p0, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->horizontal:Z

    add-int/2addr v7, v0

    .line 71
    invoke-virtual {p0, p1, v7}, Lcom/squareup/reports/applet/sales/v1/OverviewLayout;->setMeasuredDimension(II)V

    :goto_1
    return-void
.end method
