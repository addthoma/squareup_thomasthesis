.class public final Lcom/squareup/reports/applet/DisputesEntry;
.super Lcom/squareup/applet/AppletSectionsListEntry;
.source "ReportsAppletSections.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/reports/applet/DisputesEntry;",
        "Lcom/squareup/applet/AppletSectionsListEntry;",
        "section",
        "Lcom/squareup/reports/applet/disputes/DisputesSection;",
        "res",
        "Lcom/squareup/util/Res;",
        "handlesDisputes",
        "Lcom/squareup/disputes/api/HandlesDisputes;",
        "(Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/util/Res;Lcom/squareup/disputes/api/HandlesDisputes;)V",
        "getValueTextObservable",
        "Lio/reactivex/Observable;",
        "",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/util/Res;Lcom/squareup/disputes/api/HandlesDisputes;)V
    .locals 1

    const-string v0, "section"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handlesDisputes"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    check-cast p1, Lcom/squareup/applet/AppletSection;

    sget v0, Lcom/squareup/disputes/R$string;->reports_disputes:I

    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    iput-object p3, p0, Lcom/squareup/reports/applet/DisputesEntry;->handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

    return-void
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/reports/applet/DisputesEntry;)Lcom/squareup/util/Res;
    .locals 0

    .line 58
    iget-object p0, p0, Lcom/squareup/reports/applet/DisputesEntry;->res:Lcom/squareup/util/Res;

    return-object p0
.end method


# virtual methods
.method public getValueTextObservable()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/reports/applet/DisputesEntry;->handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

    invoke-interface {v0}, Lcom/squareup/disputes/api/HandlesDisputes;->disputeNotification()Lio/reactivex/Observable;

    move-result-object v0

    .line 66
    new-instance v1, Lcom/squareup/reports/applet/DisputesEntry$getValueTextObservable$1;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/DisputesEntry$getValueTextObservable$1;-><init>(Lcom/squareup/reports/applet/DisputesEntry;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "handlesDisputes.disputeN\u2026\n            \"\"\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
