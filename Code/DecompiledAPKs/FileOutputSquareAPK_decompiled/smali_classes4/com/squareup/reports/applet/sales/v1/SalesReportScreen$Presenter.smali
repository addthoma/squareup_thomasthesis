.class Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "SalesReportScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/SalesReportScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter$OverviewAmounts;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/reports/applet/sales/v1/SalesReportView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private config:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

.field private final customReportEndpoint:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

.field private final device:Lcom/squareup/util/Device;

.field private final downloadReport:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final eventSink:Lcom/squareup/badbus/BadEventSink;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final formatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private loading:Z

.field private final moneyValueFormatter:Lcom/github/mikephil/charting/utils/ValueFormatter;

.field private final printerDispatcher:Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;

.field private report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

.field private final reportConfigSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final salesReportPayloadFactory:Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private visibleCategoryRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;Lrx/subjects/BehaviorSubject;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;",
            "Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;",
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 103
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 88
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->downloadReport:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 104
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->flow:Lflow/Flow;

    .line 105
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 106
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->device:Lcom/squareup/util/Device;

    .line 107
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->formatter:Lcom/squareup/text/Formatter;

    .line 108
    iput-object p5, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->eventSink:Lcom/squareup/badbus/BadEventSink;

    .line 109
    iput-object p6, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->customReportEndpoint:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    .line 110
    iput-object p7, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->printerDispatcher:Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;

    .line 111
    iput-object p8, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->reportConfigSubject:Lrx/subjects/BehaviorSubject;

    .line 112
    iput-object p11, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 113
    iput-object p12, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    .line 114
    iput-object p13, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 115
    iput-object p14, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->salesReportPayloadFactory:Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;

    .line 117
    new-instance p1, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$Ww6V-ZqmPFgxN4TgoQywptJIMc0;

    invoke-direct {p1, p9, p10}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$Ww6V-ZqmPFgxN4TgoQywptJIMc0;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;)V

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->moneyValueFormatter:Lcom/github/mikephil/charting/utils/ValueFormatter;

    return-void
.end method

.method private downloadReport()V
    .locals 2

    const/4 v0, 0x1

    .line 244
    iput-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->loading:Z

    .line 245
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->showLoadingState()V

    .line 246
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->downloadReport:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->config:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private errorResponseToErrorType(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;",
            ">;)",
            "Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;"
        }
    .end annotation

    .line 139
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_0

    .line 140
    sget-object p1, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->NETWORK_ERROR:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    return-object p1

    .line 141
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_1

    .line 142
    sget-object p1, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->CLIENT_ERROR:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    return-object p1

    .line 143
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v0, :cond_2

    .line 144
    sget-object p1, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->SERVER_ERROR:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    return-object p1

    .line 145
    :cond_2
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-eqz v0, :cond_3

    .line 146
    sget-object p1, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->SESSION_EXPIRED:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    return-object p1

    .line 148
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response with given type not expected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private hideEmailAndPrintButtons()V
    .locals 1

    .line 233
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 234
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->hideSecondaryButton()V

    .line 235
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->hideThirdButton()V

    return-void
.end method

.method public static synthetic lambda$7Ow8AvV-TC5YJ2gnWMuDbaOorXE(Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->onReceivedFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V

    return-void
.end method

.method public static synthetic lambda$hFhZUnbG7HhnTXpfTzW0RiGaIoM(Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->onReceivedReport(Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;)V

    return-void
.end method

.method static synthetic lambda$new$0(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;F)Ljava/lang/String;
    .locals 2

    float-to-long v0, p2

    .line 118
    invoke-static {v0, v1, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    .line 119
    invoke-interface {p1, p0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private onReceivedFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "TT;>;)V"
        }
    .end annotation

    .line 297
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error;

    .line 298
    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->errorResponseToErrorType(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    move-result-object v0

    .line 300
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent;

    invoke-direct {v2, v0}, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent;-><init>(Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;)V

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 301
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->showError()V

    .line 303
    sget-object v1, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$1;->$SwitchMap$com$squareup$reports$applet$sales$v1$LoadingReportFailedEvent$ErrorType:[I

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 312
    :cond_0
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance v0, Lcom/squareup/account/AccountEvents$SessionExpired;

    invoke-direct {v0}, Lcom/squareup/account/AccountEvents$SessionExpired;-><init>()V

    invoke-interface {p1, v0}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 305
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v1, v3

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    .line 307
    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getStatusCode()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v2

    const-string p1, "Unexpected client error for sales report with response=%s, statusCode=%s"

    .line 305
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method private onReceivedReport(Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;)V
    .locals 3

    .line 274
    iget-object v0, p1, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->config:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->config:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 277
    iput-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->loading:Z

    .line 278
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    .line 279
    iget-boolean v0, p1, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->hasTransactions:Z

    if-eqz v0, :cond_3

    .line 280
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->visibleCategoryRows:Ljava/util/List;

    .line 281
    iget-object p1, p1, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->categoryRows:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;

    .line 282
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->config:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iget-boolean v1, v1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->itemDetails:Z

    if-nez v1, :cond_2

    iget-object v1, v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->rowType:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    sget-object v2, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->CATEGORY:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    if-ne v1, v2, :cond_1

    .line 284
    :cond_2
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->visibleCategoryRows:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->config:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iget-boolean v1, v1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->itemDetails:Z

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->setOpened(Z)V

    goto :goto_0

    .line 289
    :cond_3
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->showReport()V

    return-void
.end method

.method private showEmailAndPrintButtons()V
    .locals 2

    .line 219
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 220
    new-instance v1, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$VjW5ks2MQedLOjgyMK6tKMlvYnU;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$VjW5ks2MQedLOjgyMK6tKMlvYnU;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showSecondaryButton(Ljava/lang/Runnable;)V

    .line 224
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->printerDispatcher:Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;

    invoke-interface {v1}, Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;->canPrintSalesReport()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 225
    new-instance v1, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$F3gym6DdmYfJ7JBI4E82V5CW4DM;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$F3gym6DdmYfJ7JBI4E82V5CW4DM;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showThirdButton(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method private showError()V
    .locals 1

    const/4 v0, 0x0

    .line 330
    iput-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->loading:Z

    .line 331
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->hideEmailAndPrintButtons()V

    .line 332
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->showErrorState()V

    return-void
.end method

.method private showLoadingState()V
    .locals 1

    .line 336
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->hideEmailAndPrintButtons()V

    .line 337
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->showLoadingState()V

    return-void
.end method

.method private showReport()V
    .locals 2

    .line 318
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;

    .line 319
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    iget-boolean v1, v1, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->hasTransactions:Z

    if-eqz v1, :cond_0

    .line 320
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->showEmailAndPrintButtons()V

    .line 321
    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->showReportSection()V

    .line 322
    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->update()V

    goto :goto_0

    .line 324
    :cond_0
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->hideEmailAndPrintButtons()V

    .line 325
    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->showNoTransactions()V

    :goto_0
    return-void
.end method

.method private updateActionBar()V
    .locals 4

    .line 173
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/reports/applet/R$string;->reports_sales:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 174
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 175
    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v2}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    .line 176
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 177
    invoke-virtual {v2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v2, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$kQOYmv05Ykr0QMeU190fuKmFIXQ;

    invoke-direct {v2, p0}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$kQOYmv05Ykr0QMeU190fuKmFIXQ;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;)V

    .line 178
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 180
    invoke-virtual {v1, v2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 184
    :goto_0
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/reports/applet/R$string;->sales_report_email_report:I

    .line 185
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 184
    invoke-virtual {v1, v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 186
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PRINTER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/reports/applet/R$string;->sales_report_print_report:I

    .line 187
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 186
    invoke-virtual {v1, v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setThirdButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 188
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BURGER_SETTINGS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/reports/applet/R$string;->sales_report_hint_customize:I

    .line 189
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 188
    invoke-virtual {v1, v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setButton4GlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 190
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$YzK8I7Q5F3NVS0KmHoa-SORA3eE;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$YzK8I7Q5F3NVS0KmHoa-SORA3eE;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;)V

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showButton4(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 194
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method


# virtual methods
.method getCategorySalesRow(I)Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;
    .locals 1

    .line 377
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->visibleCategoryRows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;

    return-object p1
.end method

.method getCategorySalesRowCount()I
    .locals 1

    .line 362
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->hasTransactions:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->visibleCategoryRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method getChartData()Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;
    .locals 1

    .line 269
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->chartData:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;

    return-object v0
.end method

.method getDiscountRow(I)Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;
    .locals 1

    .line 422
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->discountRows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;

    return-object p1
.end method

.method getDiscountsRowCount()I
    .locals 1

    .line 355
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->hasTransactions:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->discountRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method getFormattedDateRange()Ljava/lang/String;
    .locals 1

    .line 426
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->config:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->formattedDateRange:Ljava/lang/String;

    return-object v0
.end method

.method getMoneyValueFormatter()Lcom/github/mikephil/charting/utils/ValueFormatter;
    .locals 1

    .line 430
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->moneyValueFormatter:Lcom/github/mikephil/charting/utils/ValueFormatter;

    return-object v0
.end method

.method getOverviewAmounts()Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter$OverviewAmounts;
    .locals 6

    .line 250
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->salesSummary:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    .line 251
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->formatter:Lcom/squareup/text/Formatter;

    iget-object v2, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 252
    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 253
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    .line 254
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->transaction_count:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 255
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 256
    iget-object v4, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->formatter:Lcom/squareup/text/Formatter;

    invoke-interface {v4, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 259
    iget-object v4, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v5, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_COVER_COUNTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v4, v5}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 260
    invoke-virtual {v4}, Lcom/squareup/settings/server/AccountStatusSettings;->getSubscriptions()Lcom/squareup/settings/server/Subscriptions;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/settings/server/Subscriptions;->hasOrHadRestaurantsSubscription()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v4, v4, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->cover_count:Ljava/lang/Long;

    if-eqz v4, :cond_0

    .line 262
    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->cover_count:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 264
    :goto_0
    new-instance v4, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter$OverviewAmounts;

    invoke-direct {v4, v1, v2, v3, v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter$OverviewAmounts;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v4
.end method

.method getPaymentsRow(I)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;
    .locals 1

    .line 373
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->paymentsRows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    return-object p1
.end method

.method getPaymentsRowCount()I
    .locals 1

    .line 348
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->hasTransactions:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->paymentsRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method getSalesRow(I)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;
    .locals 1

    .line 369
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->salesRows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    return-object p1
.end method

.method getSalesSummaryRowCount()I
    .locals 1

    .line 341
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->hasTransactions:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->salesRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public synthetic lambda$onEnterScope$1$SalesReportScreen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 130
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$hFhZUnbG7HhnTXpfTzW0RiGaIoM;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$hFhZUnbG7HhnTXpfTzW0RiGaIoM;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;)V

    new-instance v1, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$7Ow8AvV-TC5YJ2gnWMuDbaOorXE;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$7Ow8AvV-TC5YJ2gnWMuDbaOorXE;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$showEmailAndPrintButtons$4$SalesReportScreen$Presenter()V
    .locals 3

    .line 221
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->REPORTS_EMAIL_SALES_REPORT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 222
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->config:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    invoke-direct {v1, v2}, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$showEmailAndPrintButtons$5$SalesReportScreen$Presenter()V
    .locals 3

    .line 226
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->REPORTS_PRINT_SALES_REPORT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 227
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->printerDispatcher:Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->salesReportPayloadFactory:Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    invoke-virtual {v1, v2}, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;->buildPayload(Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;)Lcom/squareup/salesreport/print/SalesReportPayload;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;->print(Lcom/squareup/salesreport/print/SalesReportPayload;)V

    return-void
.end method

.method public synthetic lambda$updateActionBar$2$SalesReportScreen$Presenter()V
    .locals 2

    .line 178
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public synthetic lambda$updateActionBar$3$SalesReportScreen$Presenter()V
    .locals 2

    .line 191
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->REPORTS_CUSTOM_SALES_REPORT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 192
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->onCustomizeReport()V

    return-void
.end method

.method onCategoryToggled(I)V
    .locals 9

    .line 381
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->visibleCategoryRows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;

    .line 382
    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->isOpened()Z

    move-result v1

    xor-int/lit8 v2, v1, 0x1

    .line 383
    invoke-virtual {v0, v2}, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->setOpened(Z)V

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    add-int/2addr p1, v3

    .line 388
    :goto_0
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->visibleCategoryRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 389
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->visibleCategoryRows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;

    .line 390
    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->rowType:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    sget-object v1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->CATEGORY:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    if-ne v0, v1, :cond_0

    goto :goto_1

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->visibleCategoryRows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 396
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;

    invoke-virtual {v0, p1, v2}, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->categoryRangeRemoved(II)V

    goto :goto_5

    .line 398
    :cond_2
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    iget-object v1, v1, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->categoryRows:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_2
    if-ge v2, v1, :cond_6

    .line 403
    iget-object v6, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    iget-object v6, v6, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->categoryRows:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;

    if-nez v4, :cond_3

    if-ne v6, v0, :cond_5

    const/4 v4, 0x1

    goto :goto_3

    .line 409
    :cond_3
    iget-object v7, v6, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->rowType:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    sget-object v8, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->CATEGORY:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    if-ne v7, v8, :cond_4

    goto :goto_4

    :cond_4
    add-int/lit8 v5, v5, 0x1

    .line 413
    iget-object v7, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->visibleCategoryRows:Ljava/util/List;

    add-int v8, p1, v5

    invoke-interface {v7, v8, v6}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_5
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 417
    :cond_6
    :goto_4
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;

    add-int/2addr p1, v3

    invoke-virtual {v0, p1, v5}, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->categoryRangeInserted(II)V

    :goto_5
    return-void
.end method

.method onConfigChanged(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)V
    .locals 3

    .line 153
    iget-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->loading:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->config:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    if-ne v0, p1, :cond_0

    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->config:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 159
    :goto_0
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->config:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    const/4 v1, 0x0

    .line 160
    iput-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    .line 161
    iput-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->visibleCategoryRows:Ljava/util/List;

    .line 162
    iget-boolean v1, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->defaultReport:Z

    if-eqz v1, :cond_2

    .line 163
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->REPORTS_DEFAULT_SALES_REPORT:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {p1, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    goto :goto_1

    .line 165
    :cond_2
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/reports/applet/sales/v1/ViewCustomSalesReportEvent;

    invoke-direct {v2, p1}, Lcom/squareup/reports/applet/sales/v1/ViewCustomSalesReportEvent;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)V

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :goto_1
    if-eqz v0, :cond_3

    .line 167
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 168
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->downloadReport()V

    :cond_3
    return-void
.end method

.method onCustomizeReport()V
    .locals 2

    .line 215
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 124
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->reportConfigSubject:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$CX0ySSJBULoo26XryHLqSUUA9rY;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$CX0ySSJBULoo26XryHLqSUUA9rY;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->downloadReport:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->customReportEndpoint:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$gTuCmJOUfL9AL6nOkwBtZigsch4;

    invoke-direct {v2, v1}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$gTuCmJOUfL9AL6nOkwBtZigsch4;-><init>(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;)V

    .line 128
    invoke-virtual {v0, v2}, Lcom/jakewharton/rxrelay/PublishRelay;->flatMapSingle(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$0ZAZPuqsuyMmXFKUA53yppuHMew;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$0ZAZPuqsuyMmXFKUA53yppuHMew;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;)V

    .line 129
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 127
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 198
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 200
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->updateActionBar()V

    .line 203
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->config:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    const-string v0, "config"

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 205
    iget-boolean p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->loading:Z

    if-eqz p1, :cond_0

    .line 206
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->showLoadingState()V

    goto :goto_0

    .line 207
    :cond_0
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->report:Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    if-nez p1, :cond_1

    .line 208
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->downloadReport()V

    goto :goto_0

    .line 210
    :cond_1
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->showReport()V

    :goto_0
    return-void
.end method

.method retryDownloadReport()V
    .locals 2

    .line 239
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->REPORTS_RETRY_LOAD_REPORT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 240
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->downloadReport()V

    return-void
.end method
