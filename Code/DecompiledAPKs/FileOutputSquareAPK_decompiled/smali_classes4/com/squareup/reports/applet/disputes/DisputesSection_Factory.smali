.class public final Lcom/squareup/reports/applet/disputes/DisputesSection_Factory;
.super Ljava/lang/Object;
.source "DisputesSection_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/disputes/DisputesSection;",
        ">;"
    }
.end annotation


# instance fields
.field private final handlesDisputesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/reports/applet/disputes/DisputesSection_Factory;->handlesDisputesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/reports/applet/disputes/DisputesSection_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;)",
            "Lcom/squareup/reports/applet/disputes/DisputesSection_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/reports/applet/disputes/DisputesSection_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/disputes/DisputesSection_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/disputes/api/HandlesDisputes;)Lcom/squareup/reports/applet/disputes/DisputesSection;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/reports/applet/disputes/DisputesSection;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/disputes/DisputesSection;-><init>(Lcom/squareup/disputes/api/HandlesDisputes;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/disputes/DisputesSection;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/reports/applet/disputes/DisputesSection_Factory;->handlesDisputesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/disputes/api/HandlesDisputes;

    invoke-static {v0}, Lcom/squareup/reports/applet/disputes/DisputesSection_Factory;->newInstance(Lcom/squareup/disputes/api/HandlesDisputes;)Lcom/squareup/reports/applet/disputes/DisputesSection;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/reports/applet/disputes/DisputesSection_Factory;->get()Lcom/squareup/reports/applet/disputes/DisputesSection;

    move-result-object v0

    return-object v0
.end method
