.class public final Lcom/squareup/reports/applet/ReportsAppletScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "ReportsAppletScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$Responsive;
    phone = Lcom/squareup/reports/applet/ReportsAppletParentComponent$PhoneComponent;
    tablet = Lcom/squareup/reports/applet/ReportsAppletParentComponent$TabletComponent;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/ReportsAppletScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/reports/applet/ReportsAppletScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    new-instance v0, Lcom/squareup/reports/applet/ReportsAppletScope;

    invoke-direct {v0}, Lcom/squareup/reports/applet/ReportsAppletScope;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/ReportsAppletScope;->INSTANCE:Lcom/squareup/reports/applet/ReportsAppletScope;

    .line 26
    sget-object v0, Lcom/squareup/reports/applet/ReportsAppletScope;->INSTANCE:Lcom/squareup/reports/applet/ReportsAppletScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/ReportsAppletScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 21
    const-class v0, Lcom/squareup/reports/applet/ReportsAppletParentComponent$BaseComponent;

    .line 22
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/ReportsAppletParentComponent$BaseComponent;

    invoke-interface {v0}, Lcom/squareup/reports/applet/ReportsAppletParentComponent$BaseComponent;->reportsAppletScopeRunner()Lcom/squareup/reports/applet/ReportsAppletScopeRunner;

    move-result-object v0

    .line 23
    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
