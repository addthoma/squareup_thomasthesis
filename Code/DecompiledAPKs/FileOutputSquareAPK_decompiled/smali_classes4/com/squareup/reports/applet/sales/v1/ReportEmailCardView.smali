.class public Lcom/squareup/reports/applet/sales/v1/ReportEmailCardView;
.super Lcom/squareup/reports/applet/BaseEmailCardView;
.source "ReportEmailCardView.java"


# instance fields
.field presenter:Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1, p2}, Lcom/squareup/reports/applet/BaseEmailCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    const-class p2, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Component;->inject(Lcom/squareup/reports/applet/sales/v1/ReportEmailCardView;)V

    return-void
.end method


# virtual methods
.method public getPresenter()Lcom/squareup/reports/applet/BaseEmailCardPresenter;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailCardView;->presenter:Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;

    return-object v0
.end method
