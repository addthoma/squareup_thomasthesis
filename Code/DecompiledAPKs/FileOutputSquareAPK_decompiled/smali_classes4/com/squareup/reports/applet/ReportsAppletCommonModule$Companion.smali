.class public final Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;
.super Ljava/lang/Object;
.source "ReportsAppletCommonModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/ReportsAppletCommonModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000p\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0087\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J(\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0007J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0007J\u000e\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014H\u0007JH\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0007\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;",
        "",
        "()V",
        "provideAppletMasterViewPresenter",
        "Lcom/squareup/applet/AppletMasterViewPresenter;",
        "actionBarNavigationHelper",
        "Lcom/squareup/applet/ActionBarNavigationHelper;",
        "reportsAppletHeader",
        "Lcom/squareup/reports/applet/ReportsAppletHeader;",
        "res",
        "Lcom/squareup/util/Res;",
        "appletSelection",
        "Lcom/squareup/applet/AppletSelection;",
        "provideCoreSettingsCardParams",
        "Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;",
        "device",
        "Lcom/squareup/util/Device;",
        "flow",
        "Lflow/Flow;",
        "provideReportConfig",
        "Lrx/subjects/BehaviorSubject;",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
        "provideReportsAppletSectionsListPresenter",
        "Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;",
        "sections",
        "Lcom/squareup/reports/applet/ReportsAppletSectionsList;",
        "permissionGatekeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "employeeManagement",
        "Lcom/squareup/permissions/EmployeeManagement;",
        "cashManagementPermissionHolder",
        "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
        "salesReportDetailLevelHolder",
        "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 57
    invoke-direct {p0}, Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideAppletMasterViewPresenter(Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/reports/applet/ReportsAppletHeader;Lcom/squareup/util/Res;Lcom/squareup/applet/AppletSelection;)Lcom/squareup/applet/AppletMasterViewPresenter;
    .locals 1
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/reports/applet/ReportsAppletScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "actionBarNavigationHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reportsAppletHeader"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appletSelection"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    new-instance v0, Lcom/squareup/applet/AppletMasterViewPresenter;

    .line 94
    invoke-interface {p2}, Lcom/squareup/reports/applet/ReportsAppletHeader;->getHeaderId()I

    move-result p2

    invoke-interface {p3, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 92
    invoke-direct {v0, p1, p2, p4}, Lcom/squareup/applet/AppletMasterViewPresenter;-><init>(Lcom/squareup/applet/ActionBarNavigationHelper;Ljava/lang/String;Lcom/squareup/applet/AppletSelection;)V

    return-object v0
.end method

.method public final provideCoreSettingsCardParams(Lcom/squareup/util/Device;Lflow/Flow;)Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    new-instance v0, Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;

    invoke-direct {v0, p2, p1}, Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;-><init>(Lflow/Flow;Lcom/squareup/util/Device;)V

    return-object v0
.end method

.method public final provideReportConfig()Lrx/subjects/BehaviorSubject;
    .locals 2
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/reports/applet/ReportsAppletScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 103
    invoke-static {}, Lrx/subjects/BehaviorSubject;->create()Lrx/subjects/BehaviorSubject;

    move-result-object v0

    const-string v1, "BehaviorSubject.create()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final provideReportsAppletSectionsListPresenter(Lflow/Flow;Lcom/squareup/reports/applet/ReportsAppletSectionsList;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;Lcom/squareup/settings/server/Features;)Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;
    .locals 10
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/reports/applet/ReportsAppletScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "flow"

    move-object v3, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sections"

    move-object v2, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionGatekeeper"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagement"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cashManagementPermissionHolder"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesReportDetailLevelHolder"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    new-instance v0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;-><init>(Lcom/squareup/reports/applet/ReportsAppletSectionsList;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method
