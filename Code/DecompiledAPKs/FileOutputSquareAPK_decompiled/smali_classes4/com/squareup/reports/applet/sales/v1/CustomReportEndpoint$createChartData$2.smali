.class final Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;
.super Ljava/lang/Object;
.source "CustomReportEndpoint.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->createChartData(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)Lrx/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCustomReportEndpoint.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CustomReportEndpoint.kt\ncom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2\n*L\n1#1,680:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u00012\u001a\u0010\u0004\u001a\u0016\u0012\u0004\u0012\u00020\u0005 \u0006*\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;",
        "successOrFailure",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $end:Ljava/util/Calendar;

.field final synthetic $groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field final synthetic $start:Ljava/util/Calendar;

.field final synthetic this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Ljava/util/Calendar;Ljava/util/Calendar;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->$groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->$start:Ljava/util/Calendar;

    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->$end:Ljava/util/Calendar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/util/Optional;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ">;)",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;",
            ">;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 283
    instance-of v2, v1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v2, :cond_13

    check-cast v1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    .line 289
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/List;

    .line 292
    iget-object v3, v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x0

    if-lez v3, :cond_11

    .line 293
    iget-object v3, v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    iget-object v3, v3, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v5, 0x0

    const-string v7, "currency"

    .line 295
    invoke-static {v3, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v5, v6, v3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 296
    iget-object v5, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    invoke-static {v5}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->access$getMoneyFormatter$p(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;)Lcom/squareup/text/Formatter;

    move-result-object v5

    .line 297
    invoke-interface {v5, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 298
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 300
    new-instance v6, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    invoke-direct {v6}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;-><init>()V

    .line 302
    iget-object v7, v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    .line 304
    iget-object v8, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->$groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    sget-object v9, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v8}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ordinal()I

    move-result v8

    aget v8, v9, v8

    const/4 v9, 0x1

    if-eq v8, v9, :cond_d

    const/4 v10, 0x2

    const-string v11, "null cannot be cast to non-null type java.util.Calendar"

    if-eq v8, v10, :cond_6

    .line 382
    iget-object v8, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->$start:Ljava/util/Calendar;

    invoke-virtual {v8, v9}, Ljava/util/Calendar;->get(I)I

    move-result v8

    iget-object v12, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->$end:Ljava/util/Calendar;

    invoke-virtual {v12, v9}, Ljava/util/Calendar;->get(I)I

    move-result v12

    if-eq v8, v12, :cond_0

    const/4 v8, 0x1

    goto :goto_0

    :cond_0
    const/4 v8, 0x0

    .line 383
    :goto_0
    iget-object v12, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->$start:Ljava/util/Calendar;

    invoke-virtual {v12}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v12

    if-eqz v12, :cond_5

    check-cast v12, Ljava/util/Calendar;

    .line 384
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v13

    .line 385
    iget-object v14, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->$end:Ljava/util/Calendar;

    invoke-virtual {v14}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v14

    if-eqz v14, :cond_4

    check-cast v14, Ljava/util/Calendar;

    .line 386
    invoke-virtual {v14, v10, v9}, Ljava/util/Calendar;->add(II)V

    const/4 v11, 0x0

    .line 389
    :goto_1
    invoke-static {v12, v14}, Lcom/squareup/util/Times;->sameMonth(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v15

    if-nez v15, :cond_3

    if-ge v11, v7, :cond_1

    .line 392
    iget-object v15, v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    invoke-interface {v15, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    const-string v4, "reportIndexMonth"

    .line 395
    invoke-static {v13, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v9, v15, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v9, v9, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iget-object v9, v9, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v9, v9, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    const-string v10, "report.group_by_value.mo\u2026h.begin_time.instant_usec"

    invoke-static {v9, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-virtual {v4, v9, v10}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v9

    invoke-virtual {v13, v9, v10}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 397
    invoke-static {v12, v13}, Lcom/squareup/util/Times;->sameMonth(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 400
    iget-object v4, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    invoke-static {v4}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->access$getMoneyFormatter$p(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;)Lcom/squareup/text/Formatter;

    move-result-object v4

    iget-object v9, v15, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v9, v9, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v9, v9, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v4, v9}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 401
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 403
    new-instance v9, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;

    invoke-direct {v9, v15, v4}, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;Ljava/lang/String;)V

    .line 402
    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v11, v11, 0x1

    const/4 v4, 0x1

    goto :goto_2

    :cond_1
    const/4 v4, 0x0

    :goto_2
    if-nez v4, :cond_2

    .line 411
    iget-object v4, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    invoke-static {v4, v12}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->access$buildTimeRange(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Ljava/util/Calendar;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    move-result-object v4

    iput-object v4, v6, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 413
    new-instance v4, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;

    .line 414
    iget-object v9, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->$groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {v6}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    move-result-object v10

    .line 413
    invoke-direct {v4, v3, v9, v10, v5}, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;Ljava/lang/String;)V

    .line 412
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    const/4 v4, 0x2

    const/4 v9, 0x1

    .line 419
    invoke-virtual {v12, v4, v9}, Ljava/util/Calendar;->add(II)V

    const/4 v4, 0x0

    const/4 v10, 0x2

    goto :goto_1

    :cond_3
    move v4, v8

    goto/16 :goto_9

    .line 385
    :cond_4
    new-instance v1, Lkotlin/TypeCastException;

    invoke-direct {v1, v11}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 383
    :cond_5
    new-instance v1, Lkotlin/TypeCastException;

    invoke-direct {v1, v11}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 338
    :cond_6
    iget-object v4, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->$start:Ljava/util/Calendar;

    invoke-virtual {v4, v9}, Ljava/util/Calendar;->get(I)I

    move-result v4

    iget-object v8, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->$end:Ljava/util/Calendar;

    invoke-virtual {v8, v9}, Ljava/util/Calendar;->get(I)I

    move-result v8

    if-eq v4, v8, :cond_7

    const/4 v4, 0x1

    goto :goto_3

    :cond_7
    const/4 v4, 0x0

    .line 340
    :goto_3
    iget-object v8, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->$start:Ljava/util/Calendar;

    invoke-virtual {v8}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_c

    check-cast v8, Ljava/util/Calendar;

    .line 341
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 343
    iget-object v10, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->$end:Ljava/util/Calendar;

    invoke-virtual {v10}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_b

    check-cast v10, Ljava/util/Calendar;

    const/4 v11, 0x6

    const/4 v12, 0x1

    .line 344
    invoke-virtual {v10, v11, v12}, Ljava/util/Calendar;->add(II)V

    const/4 v12, 0x0

    .line 347
    :goto_4
    invoke-static {v8, v10}, Lcom/squareup/util/Times;->sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v13

    if-nez v13, :cond_12

    if-ge v12, v7, :cond_8

    .line 350
    iget-object v13, v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    invoke-interface {v13, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    const-string v14, "reportIndexDay"

    .line 353
    invoke-static {v9, v14}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v14, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v15, v13, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v15, v15, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iget-object v15, v15, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v15, v15, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    const-string v11, "report.group_by_value.day.begin_time.instant_usec"

    invoke-static {v15, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v16, v10

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v14, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 355
    invoke-static {v8, v9}, Lcom/squareup/util/Times;->sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 357
    iget-object v10, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    invoke-static {v10}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->access$getMoneyFormatter$p(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;)Lcom/squareup/text/Formatter;

    move-result-object v10

    .line 358
    iget-object v11, v13, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v11, v11, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v11, v11, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v10, v11}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v10

    .line 359
    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    .line 361
    new-instance v11, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;

    invoke-direct {v11, v13, v10}, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;Ljava/lang/String;)V

    .line 360
    invoke-interface {v2, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v12, v12, 0x1

    const/4 v10, 0x1

    goto :goto_5

    :cond_8
    move-object/from16 v16, v10

    :cond_9
    const/4 v10, 0x0

    :goto_5
    if-nez v10, :cond_a

    .line 371
    iget-object v10, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    invoke-static {v10, v8}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->access$buildTimeRange(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Ljava/util/Calendar;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    move-result-object v10

    iput-object v10, v6, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 373
    new-instance v10, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;

    .line 374
    iget-object v11, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->$groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {v6}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    move-result-object v13

    .line 373
    invoke-direct {v10, v3, v11, v13, v5}, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;Ljava/lang/String;)V

    .line 372
    invoke-interface {v2, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    const/4 v10, 0x6

    const/4 v11, 0x1

    .line 378
    invoke-virtual {v8, v10, v11}, Ljava/util/Calendar;->add(II)V

    move-object/from16 v10, v16

    const/4 v11, 0x6

    goto :goto_4

    .line 343
    :cond_b
    new-instance v1, Lkotlin/TypeCastException;

    invoke-direct {v1, v11}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 340
    :cond_c
    new-instance v1, Lkotlin/TypeCastException;

    invoke-direct {v1, v11}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_d
    const/4 v11, 0x1

    .line 306
    iget-object v4, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->$start:Ljava/util/Calendar;

    const/16 v8, 0xb

    invoke-virtual {v4, v8}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 307
    iget-object v9, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->$end:Ljava/util/Calendar;

    invoke-virtual {v9, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    if-gt v4, v8, :cond_11

    move v9, v4

    const/4 v4, 0x0

    :goto_6
    if-ge v4, v7, :cond_f

    .line 312
    iget-object v10, v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    invoke-interface {v10, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    .line 313
    iget-object v12, v10, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v12, v12, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour_of_day:Ljava/lang/Integer;

    if-nez v12, :cond_e

    goto :goto_7

    :cond_e
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    if-ne v9, v12, :cond_f

    .line 316
    iget-object v12, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    invoke-static {v12}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->access$getMoneyFormatter$p(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;)Lcom/squareup/text/Formatter;

    move-result-object v12

    iget-object v13, v10, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v13, v13, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v13, v13, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v12, v13}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v12

    .line 317
    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    .line 319
    new-instance v13, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;

    invoke-direct {v13, v10, v12}, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;Ljava/lang/String;)V

    .line 318
    invoke-interface {v2, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    move v10, v4

    const/4 v4, 0x1

    goto :goto_8

    :cond_f
    :goto_7
    move v10, v4

    const/4 v4, 0x0

    :goto_8
    if-nez v4, :cond_10

    .line 327
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v6, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->hour_of_day:Ljava/lang/Integer;

    .line 329
    new-instance v4, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;

    .line 330
    iget-object v12, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->$groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {v6}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    move-result-object v13

    .line 329
    invoke-direct {v4, v3, v12, v13, v5}, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;Ljava/lang/String;)V

    .line 328
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_10
    if-eq v9, v8, :cond_11

    add-int/lit8 v9, v9, 0x1

    move v4, v10

    goto :goto_6

    :cond_11
    const/4 v4, 0x0

    .line 424
    :cond_12
    :goto_9
    new-instance v1, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;

    invoke-direct {v1, v2, v4}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;-><init>(Ljava/util/List;Z)V

    .line 425
    sget-object v2, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    .line 426
    new-instance v3, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-direct {v3, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;-><init>(Ljava/lang/Object;)V

    .line 425
    invoke-virtual {v2, v3}, Lcom/squareup/util/Optional$Companion;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v1

    return-object v1

    .line 284
    :cond_13
    instance-of v2, v1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v2, :cond_14

    sget-object v2, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    .line 285
    iget-object v3, v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    check-cast v1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-static {v3, v1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->access$mapFailure(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    move-result-object v1

    .line 284
    invoke-virtual {v2, v1}, Lcom/squareup/util/Optional$Companion;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v1

    return-object v1

    :cond_14
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 70
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$createChartData$2;->call(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method
