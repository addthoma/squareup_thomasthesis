.class public final Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$onReact$3$$special$$inlined$onEvent$4;
.super Lkotlin/jvm/internal/Lambda;
.source "EventSelectBuilder.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$onReact$3;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "TE;",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent$Commit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEventSelectBuilder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EventSelectBuilder.kt\ncom/squareup/workflow/legacy/rx2/EventSelectBuilder$onEvent$1\n*L\n1#1,170:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u0004\u0018\u0001H\u0001\"\n\u0008\u0000\u0010\u0001\u0018\u0001*\u0002H\u0002\"\u0008\u0008\u0001\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0004*\u00020\u00032\u0006\u0010\u0005\u001a\u0002H\u0002H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "<anonymous>",
        "T",
        "E",
        "",
        "R",
        "it",
        "invoke",
        "(Ljava/lang/Object;)Ljava/lang/Object;",
        "com/squareup/workflow/legacy/rx2/EventSelectBuilder$onEvent$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$onReact$3$$special$$inlined$onEvent$4;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$onReact$3$$special$$inlined$onEvent$4;

    invoke-direct {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$onReact$3$$special$$inlined$onEvent$4;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$onReact$3$$special$$inlined$onEvent$4;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$onReact$3$$special$$inlined$onEvent$4;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent$Commit;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    instance-of v0, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent$Commit;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterEvent$Commit;

    return-object p1
.end method
