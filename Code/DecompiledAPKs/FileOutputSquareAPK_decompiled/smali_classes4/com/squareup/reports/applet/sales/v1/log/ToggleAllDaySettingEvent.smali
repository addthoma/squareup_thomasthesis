.class public final Lcom/squareup/reports/applet/sales/v1/log/ToggleAllDaySettingEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "ToggleAllDaySettingEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0004\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/reports/applet/sales/v1/log/ToggleAllDaySettingEvent;",
        "Lcom/squareup/analytics/event/v1/ActionEvent;",
        "state",
        "",
        "(Z)V",
        "getState",
        "()Z",
        "setState",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private state:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    .line 6
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->REPORTS_TOGGLE_ALL_DAY_SETTING:Lcom/squareup/analytics/RegisterActionName;

    check-cast v0, Lcom/squareup/analytics/EventNamedAction;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    iput-boolean p1, p0, Lcom/squareup/reports/applet/sales/v1/log/ToggleAllDaySettingEvent;->state:Z

    return-void
.end method


# virtual methods
.method public final getState()Z
    .locals 1

    .line 6
    iget-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/log/ToggleAllDaySettingEvent;->state:Z

    return v0
.end method

.method public final setState(Z)V
    .locals 0

    .line 6
    iput-boolean p1, p0, Lcom/squareup/reports/applet/sales/v1/log/ToggleAllDaySettingEvent;->state:Z

    return-void
.end method
