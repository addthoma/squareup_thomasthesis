.class public Lcom/squareup/reports/applet/RealReportsAppletEntryPoint;
.super Lcom/squareup/reports/applet/ReportsAppletEntryPoint;
.source "RealReportsAppletEntryPoint.java"


# static fields
.field private static final LAST_SECTION_NAME_KEY:Ljava/lang/String; = "last-reports-applet-section-name"


# direct methods
.method constructor <init>(Landroid/content/SharedPreferences;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;Lcom/squareup/reports/applet/drawer/DrawerHistorySection;Lcom/squareup/reports/applet/sales/SalesReportSection;Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    new-instance v0, Lcom/squareup/settings/StringLocalSetting;

    const-string v1, "last-reports-applet-section-name"

    invoke-direct {v0, p1, v1}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    const/4 p1, 0x4

    new-array p1, p1, [Lcom/squareup/applet/AppletSection;

    const/4 v1, 0x0

    aput-object p3, p1, v1

    const/4 p3, 0x1

    aput-object p4, p1, p3

    const/4 p3, 0x2

    aput-object p6, p1, p3

    const/4 p3, 0x3

    aput-object p7, p1, p3

    invoke-direct {p0, v0, p2, p5, p1}, Lcom/squareup/reports/applet/ReportsAppletEntryPoint;-><init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/applet/AppletSection;[Lcom/squareup/applet/AppletSection;)V

    return-void
.end method
