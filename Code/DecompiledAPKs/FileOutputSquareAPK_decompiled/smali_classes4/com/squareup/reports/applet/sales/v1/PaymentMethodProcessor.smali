.class public Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;
.super Ljava/lang/Object;
.source "PaymentMethodProcessor.java"


# instance fields
.field private final currency:Lcom/squareup/protos/common/CurrencyCode;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;->currency:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method


# virtual methods
.method public buildPaymentMethods(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/reports/applet/sales/v1/PaymentMethods;
    .locals 8

    .line 23
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;->currency:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v1, 0x0

    .line 24
    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 32
    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    move-object v2, v0

    move-object v3, v2

    move-object v4, v3

    move-object v5, v4

    move-object v6, v5

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    .line 33
    iget-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    sget-object v7, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->PAYMENT_METHOD_SEPARATED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {v1, v7}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 34
    iget-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_collected_money:Lcom/squareup/protos/common/Money;

    .line 35
    sget-object v7, Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor$1;->$SwitchMap$com$squareup$protos$beemo$api$v3$reporting$GroupByValue$PaymentMethod:[I

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_method:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ordinal()I

    move-result v0

    aget v0, v7, v0

    const/4 v7, 0x1

    if-eq v0, v7, :cond_3

    const/4 v7, 0x2

    if-eq v0, v7, :cond_2

    const/4 v7, 0x3

    if-eq v0, v7, :cond_1

    const/4 v7, 0x4

    if-eq v0, v7, :cond_0

    .line 49
    invoke-static {v5, v1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    move-object v5, v0

    goto :goto_0

    :cond_0
    move-object v6, v1

    goto :goto_0

    :cond_1
    move-object v2, v1

    goto :goto_0

    :cond_2
    move-object v4, v1

    goto :goto_0

    :cond_3
    move-object v3, v1

    goto :goto_0

    .line 54
    :cond_4
    new-instance p1, Lcom/squareup/reports/applet/sales/v1/PaymentMethods;

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/reports/applet/sales/v1/PaymentMethods;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-object p1
.end method
