.class public Lcom/squareup/reports/applet/sales/v1/ViewCustomSalesReportEvent;
.super Lcom/squareup/analytics/event/v1/ViewEvent;
.source "ViewCustomSalesReportEvent.java"


# instance fields
.field public final date_end:Ljava/lang/String;

.field public final date_start:Ljava/lang/String;

.field public final devices:Ljava/lang/String;

.field public final full_days:Z


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)V
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->REPORTS_CUSTOM_SALES_REPORT:Lcom/squareup/analytics/RegisterViewName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ViewEvent;-><init>(Lcom/squareup/analytics/EventNamedView;)V

    const-string v0, "all"

    .line 18
    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ViewCustomSalesReportEvent;->devices:Ljava/lang/String;

    .line 19
    iget-boolean v0, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->fullDays:Z

    iput-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/ViewCustomSalesReportEvent;->full_days:Z

    .line 20
    iget-object v0, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    invoke-static {v0}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ViewCustomSalesReportEvent;->date_start:Ljava/lang/String;

    .line 21
    iget-object p1, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    invoke-static {p1}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ViewCustomSalesReportEvent;->date_end:Ljava/lang/String;

    return-void
.end method
