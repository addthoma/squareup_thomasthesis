.class public final Lcom/squareup/reports/applet/drawer/PaidInOutScreen;
.super Lcom/squareup/reports/applet/InReportsAppletScope;
.source "PaidInOutScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/reports/applet/drawer/PaidInOutScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/drawer/PaidInOutScreen$Component;,
        Lcom/squareup/reports/applet/drawer/PaidInOutScreen$Module;,
        Lcom/squareup/reports/applet/drawer/PaidInOutScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/drawer/PaidInOutScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/reports/applet/drawer/PaidInOutScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/reports/applet/drawer/PaidInOutScreen;

    invoke-direct {v0}, Lcom/squareup/reports/applet/drawer/PaidInOutScreen;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/drawer/PaidInOutScreen;->INSTANCE:Lcom/squareup/reports/applet/drawer/PaidInOutScreen;

    .line 80
    sget-object v0, Lcom/squareup/reports/applet/drawer/PaidInOutScreen;->INSTANCE:Lcom/squareup/reports/applet/drawer/PaidInOutScreen;

    .line 81
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/drawer/PaidInOutScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/squareup/reports/applet/InReportsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 38
    const-class v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 84
    sget v0, Lcom/squareup/reports/applet/R$layout;->paid_in_out_main_view:I

    return v0
.end method
