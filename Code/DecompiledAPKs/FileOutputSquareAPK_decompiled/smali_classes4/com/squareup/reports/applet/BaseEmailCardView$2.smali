.class Lcom/squareup/reports/applet/BaseEmailCardView$2;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "BaseEmailCardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/BaseEmailCardView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/reports/applet/BaseEmailCardView;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/BaseEmailCardView;)V
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/squareup/reports/applet/BaseEmailCardView$2;->this$0:Lcom/squareup/reports/applet/BaseEmailCardView;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView$2;->this$0:Lcom/squareup/reports/applet/BaseEmailCardView;

    invoke-static {v0}, Lcom/squareup/reports/applet/BaseEmailCardView;->access$200(Lcom/squareup/reports/applet/BaseEmailCardView;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/reports/applet/BaseEmailCardView$2;->this$0:Lcom/squareup/reports/applet/BaseEmailCardView;

    invoke-static {v1}, Lcom/squareup/reports/applet/BaseEmailCardView;->access$100(Lcom/squareup/reports/applet/BaseEmailCardView;)Lcom/squareup/reports/applet/BaseEmailCardPresenter;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/reports/applet/BaseEmailCardPresenter;->isValidEmail(Ljava/lang/String;)Z

    move-result p1

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method
