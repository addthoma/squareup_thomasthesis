.class final Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner$onEnterScope$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSalesReportWorkflowRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSalesReportWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSalesReportWorkflowRunner.kt\ncom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner$onEnterScope$2\n+ 2 PosContainer.kt\ncom/squareup/ui/main/PosContainers\n*L\n1#1,49:1\n152#2:50\n*E\n*S KotlinDebug\n*F\n+ 1 RealSalesReportWorkflowRunner.kt\ncom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner$onEnterScope$2\n*L\n39#1:50\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/salesreport/SalesReportOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner$onEnterScope$2;->invoke(Lcom/squareup/salesreport/SalesReportOutput;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/salesreport/SalesReportOutput;)V
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget-object v0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportOutput;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    goto :goto_0

    .line 39
    :cond_0
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;->access$getContainer$p(Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;

    move-result-object p1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    .line 50
    const-class v2, Lcom/squareup/container/WorkflowTreeKey;

    aput-object v2, v0, v1

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/PosContainer;->goBackPast([Ljava/lang/Class;)V

    goto :goto_0

    .line 38
    :cond_1
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;->access$getFeeTutorial$p(Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;)Lcom/squareup/feetutorial/FeeTutorial;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/feetutorial/FeeTutorial;->activate()V

    :goto_0
    return-void
.end method
