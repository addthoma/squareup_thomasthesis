.class Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$SalesRowViewHolder;
.super Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;
.source "SalesReportRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SalesRowViewHolder"
.end annotation


# instance fields
.field private final salesRowView:Lcom/squareup/ui/account/view/LineRow;

.field final synthetic this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V
    .locals 0

    .line 520
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$SalesRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    .line 521
    invoke-direct {p0, p2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 522
    sget p1, Lcom/squareup/reports/applet/R$id;->sales_report_row:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/LineRow;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$SalesRowViewHolder;->salesRowView:Lcom/squareup/ui/account/view/LineRow;

    return-void
.end method


# virtual methods
.method public bind()V
    .locals 3

    .line 526
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$SalesRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$000(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$SalesRowViewHolder;->getLayoutPosition()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$SalesRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {v2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$800(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getSalesRow(I)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$SalesRowViewHolder;->salesRowView:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;->apply(Lcom/squareup/ui/account/view/LineRow;)V

    return-void
.end method
