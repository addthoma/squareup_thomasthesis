.class public interface abstract Lcom/squareup/reports/applet/sales/v1/ReportConfigScope$Component;
.super Ljava/lang/Object;
.source "ReportConfigScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract editReportConfigRunner()Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;
.end method

.method public abstract reportConfigCard()Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Component;
.end method

.method public abstract reportConfigEmployeeFilterCoordinator()Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen$Component;
.end method
