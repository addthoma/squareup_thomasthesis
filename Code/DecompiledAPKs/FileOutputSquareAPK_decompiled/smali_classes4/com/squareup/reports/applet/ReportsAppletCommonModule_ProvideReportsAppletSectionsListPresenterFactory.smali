.class public final Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;
.super Ljava/lang/Object;
.source "ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final cashManagementPermissionHolderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final salesReportDetailLevelHolderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final sectionsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsAppletSectionsList;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsAppletSectionsList;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->flowProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p2, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->sectionsProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p3, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->deviceProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p4, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p5, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->employeeManagementProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p6, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->cashManagementPermissionHolderProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p7, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->salesReportDetailLevelHolderProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p8, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsAppletSectionsList;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;"
        }
    .end annotation

    .line 69
    new-instance v9, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static provideReportsAppletSectionsListPresenter(Lflow/Flow;Lcom/squareup/reports/applet/ReportsAppletSectionsList;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;Lcom/squareup/settings/server/Features;)Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;
    .locals 0

    .line 77
    invoke-static/range {p0 .. p7}, Lcom/squareup/reports/applet/ReportsAppletCommonModule;->provideReportsAppletSectionsListPresenter(Lflow/Flow;Lcom/squareup/reports/applet/ReportsAppletSectionsList;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;Lcom/squareup/settings/server/Features;)Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;
    .locals 9

    .line 59
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->sectionsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/reports/applet/ReportsAppletSectionsList;

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->cashManagementPermissionHolderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->salesReportDetailLevelHolderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v8}, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->provideReportsAppletSectionsListPresenter(Lflow/Flow;Lcom/squareup/reports/applet/ReportsAppletSectionsList;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;Lcom/squareup/settings/server/Features;)Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideReportsAppletSectionsListPresenterFactory;->get()Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    move-result-object v0

    return-object v0
.end method
