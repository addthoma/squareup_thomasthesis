.class final Lcom/squareup/pushmessages/RealPushServiceRegistrar$register$3;
.super Ljava/lang/Object;
.source "RealPushServiceRegistrar.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/pushmessages/RealPushServiceRegistrar;->register()Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "serverUpdateResult",
        "Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/pushmessages/RealPushServiceRegistrar;


# direct methods
.method constructor <init>(Lcom/squareup/pushmessages/RealPushServiceRegistrar;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar$register$3;->this$0:Lcom/squareup/pushmessages/RealPushServiceRegistrar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult;)V
    .locals 1

    .line 95
    instance-of v0, p1, Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult$PushServiceRegistrationError;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult$PushServiceRegistrationError;

    invoke-virtual {p1}, Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult$PushServiceRegistrationError;->logError()V

    goto :goto_0

    .line 96
    :cond_0
    instance-of v0, p1, Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult$ServerRegistration;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar$register$3;->this$0:Lcom/squareup/pushmessages/RealPushServiceRegistrar;

    check-cast p1, Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult$ServerRegistration;

    invoke-static {v0, p1}, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->access$onServerRegistrationUpdateResponse(Lcom/squareup/pushmessages/RealPushServiceRegistrar;Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult$ServerRegistration;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult;

    invoke-virtual {p0, p1}, Lcom/squareup/pushmessages/RealPushServiceRegistrar$register$3;->accept(Lcom/squareup/pushmessages/RealPushServiceRegistrar$UpdateRegistrationResult;)V

    return-void
.end method
