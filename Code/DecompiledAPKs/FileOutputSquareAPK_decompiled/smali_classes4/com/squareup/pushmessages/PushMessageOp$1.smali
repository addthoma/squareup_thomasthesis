.class final enum Lcom/squareup/pushmessages/PushMessageOp$1;
.super Lcom/squareup/pushmessages/PushMessageOp;
.source "PushMessageOp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/pushmessages/PushMessageOp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/pushmessages/PushMessageOp;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/pushmessages/PushMessageOp$1;)V

    return-void
.end method


# virtual methods
.method createMessage(Ljava/util/Map;)Lcom/squareup/pushmessages/PushMessage;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/pushmessages/PushMessage;"
        }
    .end annotation

    .line 33
    invoke-static {p1}, Lcom/squareup/pushmessages/PushMessageOp;->access$100(Ljava/util/Map;)Lcom/squareup/pushmessages/PushMessageOp$Alert;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 34
    new-instance v1, Lcom/squareup/pushmessages/PushMessage$PushNotification;

    iget-object v2, p1, Lcom/squareup/pushmessages/PushMessageOp$Alert;->title:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/pushmessages/PushMessageOp$Alert;->body:Ljava/lang/String;

    invoke-direct {v1, v2, p1, v0}, Lcom/squareup/pushmessages/PushMessage$PushNotification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    return-object v0
.end method
