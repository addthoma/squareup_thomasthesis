.class public final Lcom/squareup/pushmessages/PushMessageModule$Companion;
.super Ljava/lang/Object;
.source "PushMessageModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/pushmessages/PushMessageModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0087\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007H\u0007J \u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0007J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0007\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/pushmessages/PushMessageModule$Companion;",
        "",
        "()V",
        "providePushServiceEnabled",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "preferences",
        "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
        "providePushServiceRegistration",
        "Lcom/squareup/settings/LocalSetting;",
        "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
        "Landroid/content/SharedPreferences;",
        "gson",
        "Lcom/google/gson/Gson;",
        "providePushServicesAsSetForLoggedIn",
        "Lmortar/Scoped;",
        "delegate",
        "Lcom/squareup/pushmessages/RealPushMessageDelegate;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/pushmessages/PushMessageModule$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final providePushServiceEnabled(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .param p1    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/DeviceSettings;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/pushmessages/PushServiceEnabled;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "preferences"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "gcm-enabled"

    invoke-virtual {p1, v1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p1

    const-string v0, "preferences.getBoolean(\"gcm-enabled\", false)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final providePushServiceRegistration(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .param p1    # Landroid/content/SharedPreferences;
        .annotation runtime Lcom/squareup/settings/DeviceSettings;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "preferences"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gson"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    const-class v0, Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;

    const-string v1, "gcm-registration"

    .line 40
    invoke-static {p1, v1, p2, v0}, Lcom/squareup/settings/GsonLocalSetting;->forClass(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/Class;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object p1

    .line 43
    invoke-virtual {p1}, Lcom/squareup/settings/GsonLocalSetting;->get()Ljava/lang/Object;

    move-result-object p2

    if-nez p2, :cond_0

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    const-string v0, "Initializing PushServiceRegistration"

    .line 44
    invoke-static {v0, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    new-instance p2, Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;

    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p2, v0, v1}, Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p1, p2}, Lcom/squareup/settings/GsonLocalSetting;->set(Ljava/lang/Object;)V

    :cond_0
    const-string p2, "setting"

    .line 47
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/settings/LocalSetting;

    return-object p1
.end method

.method public final providePushServicesAsSetForLoggedIn(Lcom/squareup/pushmessages/RealPushMessageDelegate;)Lmortar/Scoped;
    .locals 1
    .annotation runtime Lcom/squareup/ForApp;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "delegate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    check-cast p1, Lmortar/Scoped;

    return-object p1
.end method
