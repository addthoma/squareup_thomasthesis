.class public final Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;
.super Ljava/lang/Object;
.source "RealPushServiceRegistrar_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/pushmessages/RealPushServiceRegistrar;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/push/PushService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/register/RegisterPushNotificationService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/push/PushServiceAvailability;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/push/PushService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/register/RegisterPushNotificationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/push/PushServiceAvailability;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p2, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 43
    iput-object p3, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 44
    iput-object p4, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 45
    iput-object p5, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 46
    iput-object p6, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 47
    iput-object p7, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 48
    iput-object p8, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->arg7Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/push/PushService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/register/RegisterPushNotificationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/push/PushServiceAvailability;",
            ">;)",
            "Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;"
        }
    .end annotation

    .line 62
    new-instance v9, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/push/PushService;Lcom/squareup/badbus/BadEventSink;Lcom/f2prateek/rx/preferences2/Preference;ILcom/squareup/settings/LocalSetting;Lcom/squareup/pushmessages/register/RegisterPushNotificationService;Lio/reactivex/Scheduler;Lcom/squareup/push/PushServiceAvailability;)Lcom/squareup/pushmessages/RealPushServiceRegistrar;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/push/PushService;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;I",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
            ">;",
            "Lcom/squareup/pushmessages/register/RegisterPushNotificationService;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/push/PushServiceAvailability;",
            ")",
            "Lcom/squareup/pushmessages/RealPushServiceRegistrar;"
        }
    .end annotation

    .line 68
    new-instance v9, Lcom/squareup/pushmessages/RealPushServiceRegistrar;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/pushmessages/RealPushServiceRegistrar;-><init>(Lcom/squareup/push/PushService;Lcom/squareup/badbus/BadEventSink;Lcom/f2prateek/rx/preferences2/Preference;ILcom/squareup/settings/LocalSetting;Lcom/squareup/pushmessages/register/RegisterPushNotificationService;Lio/reactivex/Scheduler;Lcom/squareup/push/PushServiceAvailability;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/pushmessages/RealPushServiceRegistrar;
    .locals 9

    .line 53
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/push/PushService;

    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/badbus/BadEventSink;

    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/pushmessages/register/RegisterPushNotificationService;

    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/push/PushServiceAvailability;

    invoke-static/range {v1 .. v8}, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->newInstance(Lcom/squareup/push/PushService;Lcom/squareup/badbus/BadEventSink;Lcom/f2prateek/rx/preferences2/Preference;ILcom/squareup/settings/LocalSetting;Lcom/squareup/pushmessages/register/RegisterPushNotificationService;Lio/reactivex/Scheduler;Lcom/squareup/push/PushServiceAvailability;)Lcom/squareup/pushmessages/RealPushServiceRegistrar;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/pushmessages/RealPushServiceRegistrar_Factory;->get()Lcom/squareup/pushmessages/RealPushServiceRegistrar;

    move-result-object v0

    return-object v0
.end method
