.class final Lcom/squareup/pushmessages/RealPushMessageDelegate$onEnterScope$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealPushMessageDelegate.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/pushmessages/RealPushMessageDelegate;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/push/PushServiceMessage;",
        "Lcom/squareup/pushmessages/PushMessage;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealPushMessageDelegate.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealPushMessageDelegate.kt\ncom/squareup/pushmessages/RealPushMessageDelegate$onEnterScope$1\n*L\n1#1,57:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/pushmessages/PushMessage;",
        "pushMessage",
        "Lcom/squareup/push/PushServiceMessage;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/pushmessages/RealPushMessageDelegate;


# direct methods
.method constructor <init>(Lcom/squareup/pushmessages/RealPushMessageDelegate;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/pushmessages/RealPushMessageDelegate$onEnterScope$1;->this$0:Lcom/squareup/pushmessages/RealPushMessageDelegate;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/push/PushServiceMessage;)Lcom/squareup/pushmessages/PushMessage;
    .locals 1

    .line 32
    invoke-virtual {p1}, Lcom/squareup/push/PushServiceMessage;->getData()Ljava/util/Map;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/pushmessages/PushMessageOp;->fromParams(Ljava/util/Map;)Lcom/squareup/pushmessages/PushMessage;

    move-result-object p1

    .line 33
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushMessageDelegate$onEnterScope$1;->this$0:Lcom/squareup/pushmessages/RealPushMessageDelegate;

    invoke-static {v0, p1}, Lcom/squareup/pushmessages/RealPushMessageDelegate;->access$logPushMessage(Lcom/squareup/pushmessages/RealPushMessageDelegate;Lcom/squareup/pushmessages/PushMessage;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/push/PushServiceMessage;

    invoke-virtual {p0, p1}, Lcom/squareup/pushmessages/RealPushMessageDelegate$onEnterScope$1;->invoke(Lcom/squareup/push/PushServiceMessage;)Lcom/squareup/pushmessages/PushMessage;

    move-result-object p1

    return-object p1
.end method
