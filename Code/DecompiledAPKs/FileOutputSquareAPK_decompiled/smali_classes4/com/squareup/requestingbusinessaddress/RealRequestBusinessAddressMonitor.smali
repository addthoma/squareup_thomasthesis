.class public final Lcom/squareup/requestingbusinessaddress/RealRequestBusinessAddressMonitor;
.super Ljava/lang/Object;
.source "RealRequestBusinessAddressMonitor.kt"

# interfaces
.implements Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B)\u0008\u0007\u0012\u000e\u0008\u0001\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u000e\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u000bH\u0016J\u0008\u0010\u000c\u001a\u00020\rH\u0016J\u0008\u0010\u000e\u001a\u00020\rH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/requestingbusinessaddress/RealRequestBusinessAddressMonitor;",
        "Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;",
        "hasUserResolvedBusinessAddress",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;)V",
        "addressRequiresValidation",
        "Lio/reactivex/Observable;",
        "setBusinessAddressUnverified",
        "",
        "setBusinessAddressVerified",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final hasUserResolvedBusinessAddress:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;


# direct methods
.method public constructor <init>(Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p1    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/requestingbusinessaddress/HasUserResolvedBusinessAddress;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lio/reactivex/Scheduler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "hasUserResolvedBusinessAddress"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/requestingbusinessaddress/RealRequestBusinessAddressMonitor;->hasUserResolvedBusinessAddress:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p2, p0, Lcom/squareup/requestingbusinessaddress/RealRequestBusinessAddressMonitor;->features:Lcom/squareup/settings/server/Features;

    iput-object p3, p0, Lcom/squareup/requestingbusinessaddress/RealRequestBusinessAddressMonitor;->mainScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method public static final synthetic access$getHasUserResolvedBusinessAddress$p(Lcom/squareup/requestingbusinessaddress/RealRequestBusinessAddressMonitor;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/requestingbusinessaddress/RealRequestBusinessAddressMonitor;->hasUserResolvedBusinessAddress:Lcom/f2prateek/rx/preferences2/Preference;

    return-object p0
.end method


# virtual methods
.method public addressRequiresValidation()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/requestingbusinessaddress/RealRequestBusinessAddressMonitor;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REQUEST_BUSINESS_ADDRESS_BADGE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v0

    .line 22
    new-instance v1, Lcom/squareup/requestingbusinessaddress/RealRequestBusinessAddressMonitor$addressRequiresValidation$1;

    invoke-direct {v1, p0}, Lcom/squareup/requestingbusinessaddress/RealRequestBusinessAddressMonitor$addressRequiresValidation$1;-><init>(Lcom/squareup/requestingbusinessaddress/RealRequestBusinessAddressMonitor;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/squareup/requestingbusinessaddress/RealRequestBusinessAddressMonitor;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "features.featureEnabled(\u2026.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public setBusinessAddressUnverified()V
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/squareup/requestingbusinessaddress/RealRequestBusinessAddressMonitor;->hasUserResolvedBusinessAddress:Lcom/f2prateek/rx/preferences2/Preference;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public setBusinessAddressVerified()V
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/squareup/requestingbusinessaddress/RealRequestBusinessAddressMonitor;->hasUserResolvedBusinessAddress:Lcom/f2prateek/rx/preferences2/Preference;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method
