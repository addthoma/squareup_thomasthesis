.class public final Lcom/squareup/posapp/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/posapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abc_action_bar_content_inset_material:I = 0x7f070000

.field public static final abc_action_bar_content_inset_with_nav:I = 0x7f070001

.field public static final abc_action_bar_default_height_material:I = 0x7f070002

.field public static final abc_action_bar_default_padding_end_material:I = 0x7f070003

.field public static final abc_action_bar_default_padding_start_material:I = 0x7f070004

.field public static final abc_action_bar_elevation_material:I = 0x7f070005

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f070006

.field public static final abc_action_bar_overflow_padding_end_material:I = 0x7f070007

.field public static final abc_action_bar_overflow_padding_start_material:I = 0x7f070008

.field public static final abc_action_bar_stacked_max_height:I = 0x7f070009

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f07000a

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f07000b

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f07000c

.field public static final abc_action_button_min_height_material:I = 0x7f07000d

.field public static final abc_action_button_min_width_material:I = 0x7f07000e

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f07000f

.field public static final abc_alert_dialog_button_bar_height:I = 0x7f070010

.field public static final abc_alert_dialog_button_dimen:I = 0x7f070011

.field public static final abc_button_inset_horizontal_material:I = 0x7f070012

.field public static final abc_button_inset_vertical_material:I = 0x7f070013

.field public static final abc_button_padding_horizontal_material:I = 0x7f070014

.field public static final abc_button_padding_vertical_material:I = 0x7f070015

.field public static final abc_cascading_menus_min_smallest_width:I = 0x7f070016

.field public static final abc_config_prefDialogWidth:I = 0x7f070017

.field public static final abc_control_corner_material:I = 0x7f070018

.field public static final abc_control_inset_material:I = 0x7f070019

.field public static final abc_control_padding_material:I = 0x7f07001a

.field public static final abc_dialog_corner_radius_material:I = 0x7f07001b

.field public static final abc_dialog_fixed_height_major:I = 0x7f07001c

.field public static final abc_dialog_fixed_height_minor:I = 0x7f07001d

.field public static final abc_dialog_fixed_width_major:I = 0x7f07001e

.field public static final abc_dialog_fixed_width_minor:I = 0x7f07001f

.field public static final abc_dialog_list_padding_bottom_no_buttons:I = 0x7f070020

.field public static final abc_dialog_list_padding_top_no_title:I = 0x7f070021

.field public static final abc_dialog_min_width_major:I = 0x7f070022

.field public static final abc_dialog_min_width_minor:I = 0x7f070023

.field public static final abc_dialog_padding_material:I = 0x7f070024

.field public static final abc_dialog_padding_top_material:I = 0x7f070025

.field public static final abc_dialog_title_divider_material:I = 0x7f070026

.field public static final abc_disabled_alpha_material_dark:I = 0x7f070027

.field public static final abc_disabled_alpha_material_light:I = 0x7f070028

.field public static final abc_dropdownitem_icon_width:I = 0x7f070029

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f07002a

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f07002b

.field public static final abc_edit_text_inset_bottom_material:I = 0x7f07002c

.field public static final abc_edit_text_inset_horizontal_material:I = 0x7f07002d

.field public static final abc_edit_text_inset_top_material:I = 0x7f07002e

.field public static final abc_floating_window_z:I = 0x7f07002f

.field public static final abc_list_item_height_large_material:I = 0x7f070030

.field public static final abc_list_item_height_material:I = 0x7f070031

.field public static final abc_list_item_height_small_material:I = 0x7f070032

.field public static final abc_list_item_padding_horizontal_material:I = 0x7f070033

.field public static final abc_panel_menu_list_width:I = 0x7f070034

.field public static final abc_progress_bar_height_material:I = 0x7f070035

.field public static final abc_search_view_preferred_height:I = 0x7f070036

.field public static final abc_search_view_preferred_width:I = 0x7f070037

.field public static final abc_seekbar_track_background_height_material:I = 0x7f070038

.field public static final abc_seekbar_track_progress_height_material:I = 0x7f070039

.field public static final abc_select_dialog_padding_start_material:I = 0x7f07003a

.field public static final abc_switch_padding:I = 0x7f07003b

.field public static final abc_text_size_body_1_material:I = 0x7f07003c

.field public static final abc_text_size_body_2_material:I = 0x7f07003d

.field public static final abc_text_size_button_material:I = 0x7f07003e

.field public static final abc_text_size_caption_material:I = 0x7f07003f

.field public static final abc_text_size_display_1_material:I = 0x7f070040

.field public static final abc_text_size_display_2_material:I = 0x7f070041

.field public static final abc_text_size_display_3_material:I = 0x7f070042

.field public static final abc_text_size_display_4_material:I = 0x7f070043

.field public static final abc_text_size_headline_material:I = 0x7f070044

.field public static final abc_text_size_large_material:I = 0x7f070045

.field public static final abc_text_size_medium_material:I = 0x7f070046

.field public static final abc_text_size_menu_header_material:I = 0x7f070047

.field public static final abc_text_size_menu_material:I = 0x7f070048

.field public static final abc_text_size_small_material:I = 0x7f070049

.field public static final abc_text_size_subhead_material:I = 0x7f07004a

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f07004b

.field public static final abc_text_size_title_material:I = 0x7f07004c

.field public static final abc_text_size_title_material_toolbar:I = 0x7f07004d

.field public static final accessible_keypad_cancel_button_margin:I = 0x7f07004e

.field public static final accessible_keypad_cancel_button_padding:I = 0x7f07004f

.field public static final accessible_keypad_digit_radius:I = 0x7f070050

.field public static final accessible_keypad_margin:I = 0x7f070051

.field public static final accessible_keypad_screen_title_size:I = 0x7f070052

.field public static final accessible_keypad_star_gap:I = 0x7f070053

.field public static final accessible_keypad_star_radius:I = 0x7f070054

.field public static final action_bar_size:I = 0x7f070055

.field public static final activity_applet_list_top_message_panel_horizontal_padding:I = 0x7f070056

.field public static final activity_applet_list_top_message_panel_message_text_size:I = 0x7f070057

.field public static final activity_applet_list_top_message_panel_tap_state_message_top_padding:I = 0x7f070058

.field public static final activity_applet_list_top_message_panel_title_bottom_padding:I = 0x7f070059

.field public static final activity_applet_list_top_message_panel_title_text_size:I = 0x7f07005a

.field public static final activity_applet_list_top_message_panel_vertical_padding:I = 0x7f07005b

.field public static final activity_applet_sticky_header_height:I = 0x7f07005c

.field public static final activity_applet_sticky_header_padding:I = 0x7f07005d

.field public static final activity_horizontal_margin_large:I = 0x7f07005e

.field public static final activity_horizontal_margin_medium:I = 0x7f07005f

.field public static final activity_horizontal_margin_small:I = 0x7f070060

.field public static final activity_horizontal_margin_v_small:I = 0x7f070061

.field public static final activity_vertical_margin_large:I = 0x7f070062

.field public static final activity_vertical_margin_medium:I = 0x7f070063

.field public static final activity_vertical_margin_small:I = 0x7f070064

.field public static final add_money_small_spacing:I = 0x7f070065

.field public static final amount_cell_label:I = 0x7f070066

.field public static final amount_cell_label_responsive_13_14:I = 0x7f070067

.field public static final amount_cell_value:I = 0x7f070068

.field public static final amount_cell_value_responsive_28_44:I = 0x7f070069

.field public static final appcompat_dialog_background_inset:I = 0x7f07006a

.field public static final applet_drawable_padding:I = 0x7f07006b

.field public static final applet_drawer_width:I = 0x7f07006c

.field public static final applet_height:I = 0x7f07006d

.field public static final applet_low_priority_badge_height:I = 0x7f07006e

.field public static final applet_padding_top:I = 0x7f07006f

.field public static final applet_text_size:I = 0x7f070070

.field public static final badge_margin_bottom:I = 0x7f070071

.field public static final badge_margin_right:I = 0x7f070072

.field public static final balance_activities_title_text_size:I = 0x7f070073

.field public static final balance_activity_details_description_spacing_bottom:I = 0x7f070074

.field public static final balance_activity_details_description_spacing_top:I = 0x7f070075

.field public static final balance_activity_details_image_size:I = 0x7f070076

.field public static final balance_activity_details_pill_corner_radius:I = 0x7f070077

.field public static final balance_activity_details_pill_elevation:I = 0x7f070078

.field public static final balance_activity_details_pill_horizontal_padding:I = 0x7f070079

.field public static final balance_activity_details_pill_vertical_padding:I = 0x7f07007a

.field public static final balance_activity_item_offset_horizontal:I = 0x7f07007b

.field public static final balance_activity_item_offset_vertical:I = 0x7f07007c

.field public static final bank_logo_error_shadow_fudge:I = 0x7f07007d

.field public static final bank_logo_error_shadow_radius:I = 0x7f07007e

.field public static final big_text_header_subtitle_size:I = 0x7f07007f

.field public static final big_text_header_title_size:I = 0x7f070080

.field public static final bill_history_detail_empty_view_spacing_between_icon_and_text:I = 0x7f070081

.field public static final bill_history_detail_tender_cashier_row_top_padding:I = 0x7f070082

.field public static final bill_history_detail_tender_customer_circle_diameter:I = 0x7f070083

.field public static final bill_history_detail_tender_customer_circle_margin_end:I = 0x7f070084

.field public static final bill_history_detail_tender_customer_circle_vertical_padding:I = 0x7f070085

.field public static final bill_history_detail_tender_customer_initials:I = 0x7f070086

.field public static final bill_history_detail_tender_customer_name:I = 0x7f070087

.field public static final bottom_sheet_indicator_height:I = 0x7f070088

.field public static final bottom_sheet_indicator_offset:I = 0x7f070089

.field public static final bottom_sheet_indicator_width:I = 0x7f07008a

.field public static final bulk_settle_button_badge_center_offset:I = 0x7f07008b

.field public static final bulk_settle_tender_row_amount_width:I = 0x7f07008c

.field public static final bulk_settle_tender_row_field_spacing:I = 0x7f07008d

.field public static final bulk_settle_tender_row_receipt_number_width:I = 0x7f07008e

.field public static final bulk_settle_tender_row_time_width:I = 0x7f07008f

.field public static final buyer_action_container_helper_text:I = 0x7f070090

.field public static final buyer_action_container_margin:I = 0x7f070091

.field public static final buyer_cart_default_padding:I = 0x7f070092

.field public static final buyer_cart_item_description_right_padding:I = 0x7f070093

.field public static final buyer_cart_item_description_top_padding:I = 0x7f070094

.field public static final buyer_cart_item_padding_bottom:I = 0x7f070095

.field public static final buyer_cart_label_text:I = 0x7f070096

.field public static final buyer_cart_title_text:I = 0x7f070097

.field public static final buyer_cart_title_text_default_bottom_padding:I = 0x7f070098

.field public static final buyer_cart_title_text_default_top_padding:I = 0x7f070099

.field public static final buyer_facing_action_bar_medium_text_size:I = 0x7f07009a

.field public static final buyer_facing_bar_button_height:I = 0x7f07009b

.field public static final buyer_facing_bar_button_margin:I = 0x7f07009c

.field public static final buyer_facing_height:I = 0x7f07009d

.field public static final buyer_facing_input_margin:I = 0x7f07009e

.field public static final buyer_facing_input_padding:I = 0x7f07009f

.field public static final buyer_facing_margin:I = 0x7f0700a0

.field public static final buyer_facing_text_size_secondary:I = 0x7f0700a1

.field public static final buyer_facing_text_size_tile_button:I = 0x7f0700a2

.field public static final buyer_facing_tile_button_height:I = 0x7f0700a3

.field public static final buyer_facing_title_size:I = 0x7f0700a4

.field public static final buyer_noho_action_bar_condensed_height:I = 0x7f0700a5

.field public static final calendar_day_headers_paddingbottom:I = 0x7f0700a6

.field public static final calendar_month_title_bottommargin:I = 0x7f0700a7

.field public static final calendar_month_topmargin:I = 0x7f0700a8

.field public static final calendar_text_medium:I = 0x7f0700a9

.field public static final calendar_text_small:I = 0x7f0700aa

.field public static final capital_offer_subheader_spacing:I = 0x7f0700ab

.field public static final capital_pie_chart_size:I = 0x7f0700ac

.field public static final capital_plan_bullet_size:I = 0x7f0700ad

.field public static final card_customizations_signature_stroke_width:I = 0x7f0700ae

.field public static final card_drawing_margin:I = 0x7f0700af

.field public static final card_elevation:I = 0x7f0700b0

.field public static final cardview_compat_inset_shadow:I = 0x7f0700b1

.field public static final cardview_default_elevation:I = 0x7f0700b2

.field public static final cardview_default_radius:I = 0x7f0700b3

.field public static final cart_footer_banner_margin:I = 0x7f0700b4

.field public static final cart_header_height:I = 0x7f0700b5

.field public static final cart_header_phone_horizontal_margin:I = 0x7f0700b6

.field public static final cart_item_description_right_padding:I = 0x7f0700b7

.field public static final cart_item_description_top_padding:I = 0x7f0700b8

.field public static final cart_item_diff_highlight_height:I = 0x7f0700b9

.field public static final cart_item_diff_highlight_width:I = 0x7f0700ba

.field public static final cart_item_padding_bottom:I = 0x7f0700bb

.field public static final cart_line_height:I = 0x7f0700bc

.field public static final cart_modifiers_gap:I = 0x7f0700bd

.field public static final cart_swipe_controller_delete_button_padding:I = 0x7f0700be

.field public static final cart_total_bottom_padding:I = 0x7f0700bf

.field public static final cart_total_top_padding:I = 0x7f0700c0

.field public static final cash_drawer_modal_content_size:I = 0x7f0700c1

.field public static final cash_drawer_modal_gap:I = 0x7f0700c2

.field public static final cash_drawer_modal_padding:I = 0x7f0700c3

.field public static final cash_drawer_modal_title_size:I = 0x7f0700c4

.field public static final cash_drawer_modal_width:I = 0x7f0700c5

.field public static final checkable_expense_type_progress_bar_size:I = 0x7f0700c6

.field public static final checkout_card_min_height:I = 0x7f0700c7

.field public static final checkout_card_width:I = 0x7f0700c8

.field public static final checkout_jumbo_button_height:I = 0x7f0700c9

.field public static final checkout_tablet_row_height:I = 0x7f0700ca

.field public static final checkout_top_margin:I = 0x7f0700cb

.field public static final color_picker_height:I = 0x7f0700cc

.field public static final compat_button_inset_horizontal_material:I = 0x7f0700cd

.field public static final compat_button_inset_vertical_material:I = 0x7f0700ce

.field public static final compat_button_padding_horizontal_material:I = 0x7f0700cf

.field public static final compat_button_padding_vertical_material:I = 0x7f0700d0

.field public static final compat_control_corner_material:I = 0x7f0700d1

.field public static final compat_notification_large_icon_max_height:I = 0x7f0700d2

.field public static final compat_notification_large_icon_max_width:I = 0x7f0700d3

.field public static final confirm_and_pay_button_height:I = 0x7f0700d4

.field public static final confirm_and_pay_button_text_size:I = 0x7f0700d5

.field public static final coupon_row_radius:I = 0x7f0700d6

.field public static final crm_appointment_icon_font_size:I = 0x7f0700d7

.field public static final crm_appointment_icon_size:I = 0x7f0700d8

.field public static final crm_bottom_card_dialog_width:I = 0x7f0700d9

.field public static final crm_card_divider_height:I = 0x7f0700da

.field public static final crm_cardonfile_unlink_card_button_margin:I = 0x7f0700db

.field public static final crm_cardonfile_unlink_card_glyph_margin:I = 0x7f0700dc

.field public static final crm_contact_list_padding:I = 0x7f0700dd

.field public static final crm_customer_input_disclaimer_text_size:I = 0x7f0700de

.field public static final crm_customer_input_gap_big:I = 0x7f0700df

.field public static final crm_customer_input_gap_small:I = 0x7f0700e0

.field public static final crm_customer_input_main_text_size:I = 0x7f0700e1

.field public static final crm_customer_input_message_text_size:I = 0x7f0700e2

.field public static final crm_customer_input_title_text_size:I = 0x7f0700e3

.field public static final crm_customer_input_width:I = 0x7f0700e4

.field public static final crm_customer_unit_label_row_height:I = 0x7f0700e5

.field public static final crm_customer_unit_row_height:I = 0x7f0700e6

.field public static final crm_customer_unit_row_text_margin:I = 0x7f0700e7

.field public static final crm_filter_bubble_max_width:I = 0x7f0700e8

.field public static final crm_filter_bubble_min_height:I = 0x7f0700e9

.field public static final crm_initial_circle_nonscaling_text_size:I = 0x7f0700ea

.field public static final crm_initial_circle_size:I = 0x7f0700eb

.field public static final crm_messaging_input_gap:I = 0x7f0700ec

.field public static final crm_reward_progress_size:I = 0x7f0700ed

.field public static final crm_reward_status_box_height:I = 0x7f0700ee

.field public static final date_picker_calendar_vertical_padding:I = 0x7f0700ef

.field public static final default_digit_input_text_size:I = 0x7f0700f0

.field public static final default_dimension:I = 0x7f0700f1

.field public static final default_padding:I = 0x7f0700f2

.field public static final default_stamp_stroke_width:I = 0x7f0700f3

.field public static final default_status_bar_font_size:I = 0x7f0700f4

.field public static final default_status_bar_glyph_font_size:I = 0x7f0700f5

.field public static final default_status_bar_height:I = 0x7f0700f6

.field public static final default_status_bar_icon_height:I = 0x7f0700f7

.field public static final default_status_bar_icon_padding_horizontal:I = 0x7f0700f8

.field public static final default_status_bar_padding_full:I = 0x7f0700f9

.field public static final default_status_bar_padding_half:I = 0x7f0700fa

.field public static final default_title_indicator_clip_padding:I = 0x7f0700fb

.field public static final default_title_indicator_footer_indicator_height:I = 0x7f0700fc

.field public static final default_title_indicator_footer_indicator_underline_padding:I = 0x7f0700fd

.field public static final default_title_indicator_footer_line_height:I = 0x7f0700fe

.field public static final default_title_indicator_footer_padding:I = 0x7f0700ff

.field public static final default_title_indicator_text_size:I = 0x7f070100

.field public static final default_title_indicator_title_padding:I = 0x7f070101

.field public static final default_title_indicator_top_padding:I = 0x7f070102

.field public static final deposits_info_phone_image_height:I = 0x7f070103

.field public static final deposits_info_phone_image_width:I = 0x7f070104

.field public static final deposits_report_column_width:I = 0x7f070105

.field public static final deposits_report_sections_gap:I = 0x7f070106

.field public static final design_appbar_elevation:I = 0x7f070107

.field public static final design_bottom_navigation_active_item_max_width:I = 0x7f070108

.field public static final design_bottom_navigation_active_item_min_width:I = 0x7f070109

.field public static final design_bottom_navigation_active_text_size:I = 0x7f07010a

.field public static final design_bottom_navigation_elevation:I = 0x7f07010b

.field public static final design_bottom_navigation_height:I = 0x7f07010c

.field public static final design_bottom_navigation_icon_size:I = 0x7f07010d

.field public static final design_bottom_navigation_item_max_width:I = 0x7f07010e

.field public static final design_bottom_navigation_item_min_width:I = 0x7f07010f

.field public static final design_bottom_navigation_margin:I = 0x7f070110

.field public static final design_bottom_navigation_shadow_height:I = 0x7f070111

.field public static final design_bottom_navigation_text_size:I = 0x7f070112

.field public static final design_bottom_sheet_elevation:I = 0x7f070113

.field public static final design_bottom_sheet_modal_elevation:I = 0x7f070114

.field public static final design_bottom_sheet_peek_height_min:I = 0x7f070115

.field public static final design_fab_border_width:I = 0x7f070116

.field public static final design_fab_elevation:I = 0x7f070117

.field public static final design_fab_image_size:I = 0x7f070118

.field public static final design_fab_size_mini:I = 0x7f070119

.field public static final design_fab_size_normal:I = 0x7f07011a

.field public static final design_fab_translation_z_hovered_focused:I = 0x7f07011b

.field public static final design_fab_translation_z_pressed:I = 0x7f07011c

.field public static final design_navigation_elevation:I = 0x7f07011d

.field public static final design_navigation_icon_padding:I = 0x7f07011e

.field public static final design_navigation_icon_size:I = 0x7f07011f

.field public static final design_navigation_item_horizontal_padding:I = 0x7f070120

.field public static final design_navigation_item_icon_padding:I = 0x7f070121

.field public static final design_navigation_max_width:I = 0x7f070122

.field public static final design_navigation_padding_bottom:I = 0x7f070123

.field public static final design_navigation_separator_vertical_padding:I = 0x7f070124

.field public static final design_snackbar_action_inline_max_width:I = 0x7f070125

.field public static final design_snackbar_action_text_color_alpha:I = 0x7f070126

.field public static final design_snackbar_background_corner_radius:I = 0x7f070127

.field public static final design_snackbar_elevation:I = 0x7f070128

.field public static final design_snackbar_extra_spacing_horizontal:I = 0x7f070129

.field public static final design_snackbar_max_width:I = 0x7f07012a

.field public static final design_snackbar_min_width:I = 0x7f07012b

.field public static final design_snackbar_padding_horizontal:I = 0x7f07012c

.field public static final design_snackbar_padding_vertical:I = 0x7f07012d

.field public static final design_snackbar_padding_vertical_2lines:I = 0x7f07012e

.field public static final design_snackbar_text_size:I = 0x7f07012f

.field public static final design_tab_max_width:I = 0x7f070130

.field public static final design_tab_scrollable_min_width:I = 0x7f070131

.field public static final design_tab_text_size:I = 0x7f070132

.field public static final design_tab_text_size_2line:I = 0x7f070133

.field public static final design_textinput_caption_translate_y:I = 0x7f070134

.field public static final determinate_progress_circle_thickness:I = 0x7f070135

.field public static final dialog_border:I = 0x7f070136

.field public static final dialog_content_hpad:I = 0x7f070137

.field public static final digits_0_through_9_text_size:I = 0x7f070138

.field public static final digits_1_through_9_radius:I = 0x7f070139

.field public static final disabled_alpha_material_dark:I = 0x7f07013a

.field public static final disabled_alpha_material_light:I = 0x7f07013b

.field public static final discount_rule_row_color_strip_thickness:I = 0x7f07013c

.field public static final discount_switch_item_width:I = 0x7f07013d

.field public static final discount_tag_gap:I = 0x7f07013e

.field public static final display_text:I = 0x7f07013f

.field public static final disputes_header_top_margin:I = 0x7f070140

.field public static final disputes_list_padding:I = 0x7f070141

.field public static final done_screen_button_height:I = 0x7f070142

.field public static final done_screen_button_margin:I = 0x7f070143

.field public static final duration_number_picker_margin:I = 0x7f070144

.field public static final edit_entry_label_height:I = 0x7f070145

.field public static final edit_entry_label_text_height:I = 0x7f070146

.field public static final edit_entry_label_width:I = 0x7f070147

.field public static final edit_envelope_button_height:I = 0x7f070148

.field public static final edit_stroke_width:I = 0x7f070149

.field public static final egiftcard_design_border_size:I = 0x7f07014a

.field public static final egiftcard_design_crop_margin:I = 0x7f07014b

.field public static final employee_management_timecards_back_button_width:I = 0x7f07014c

.field public static final employee_management_timecards_button_height:I = 0x7f07014d

.field public static final employee_management_timecards_button_text_size:I = 0x7f07014e

.field public static final employee_management_timecards_clocked_in_green_indicator_margin:I = 0x7f07014f

.field public static final employee_management_timecards_clocked_in_green_indicator_size:I = 0x7f070150

.field public static final employee_management_timecards_current_time_text_size:I = 0x7f070151

.field public static final employee_management_timecards_employee_name_margin:I = 0x7f070152

.field public static final employee_management_timecards_glyph_margin:I = 0x7f070153

.field public static final employee_management_timecards_glyph_margin_bottom_card:I = 0x7f070154

.field public static final employee_management_timecards_glyph_margin_top_card:I = 0x7f070155

.field public static final employee_management_timecards_gutter:I = 0x7f070156

.field public static final employee_management_timecards_gutter_card:I = 0x7f070157

.field public static final employee_management_timecards_margin:I = 0x7f070158

.field public static final employee_management_timecards_sub_header_text_size:I = 0x7f070159

.field public static final employee_management_timecards_summary_margin:I = 0x7f07015a

.field public static final employee_management_timecards_summary_text_size:I = 0x7f07015b

.field public static final emv_title_margin:I = 0x7f07015c

.field public static final emv_title_negative_margin:I = 0x7f07015d

.field public static final emv_total_margin:I = 0x7f07015e

.field public static final enable_device_settings_button_height:I = 0x7f07015f

.field public static final enable_device_settings_button_padding:I = 0x7f070160

.field public static final enable_device_settings_button_width:I = 0x7f070161

.field public static final fastscroll_default_thickness:I = 0x7f070162

.field public static final fastscroll_margin:I = 0x7f070163

.field public static final fastscroll_minimum_range:I = 0x7f070164

.field public static final favorite_tooltip_bottom_arrow:I = 0x7f070165

.field public static final field_error_text_size:I = 0x7f070166

.field public static final gift_card_name_margin_top_tablet_only:I = 0x7f070167

.field public static final glyph_button_padding:I = 0x7f070168

.field public static final glyph_font_size:I = 0x7f070169

.field public static final glyph_logo_image:I = 0x7f07016a

.field public static final glyph_logo_image_text_size:I = 0x7f07016b

.field public static final glyph_logotype_font_size:I = 0x7f07016c

.field public static final glyph_logotype_font_size_world:I = 0x7f07016d

.field public static final glyph_logotype_height:I = 0x7f07016e

.field public static final glyph_logotype_height_world:I = 0x7f07016f

.field public static final glyph_logotype_width_en:I = 0x7f070170

.field public static final glyph_logotype_width_en_world:I = 0x7f070171

.field public static final glyph_logotype_width_es:I = 0x7f070172

.field public static final glyph_logotype_width_es_world:I = 0x7f070173

.field public static final glyph_logotype_width_fr:I = 0x7f070174

.field public static final glyph_logotype_width_fr_world:I = 0x7f070175

.field public static final glyph_logotype_width_ja:I = 0x7f070176

.field public static final glyph_logotype_width_ja_world:I = 0x7f070177

.field public static final glyph_scaling:I = 0x7f070178

.field public static final glyph_shadow_dx:I = 0x7f070179

.field public static final glyph_shadow_dx_45_angle:I = 0x7f07017a

.field public static final glyph_shadow_dy:I = 0x7f07017b

.field public static final glyph_shadow_radius:I = 0x7f07017c

.field public static final glyph_size_circle:I = 0x7f07017d

.field public static final glyph_stroke_width:I = 0x7f07017e

.field public static final glyph_vertical_caret:I = 0x7f07017f

.field public static final grid_delete_button_circle_stroke:I = 0x7f070180

.field public static final grid_delete_button_padding:I = 0x7f070181

.field public static final grid_delete_button_size_with_padding:I = 0x7f070182

.field public static final grid_delete_button_x_stroke:I = 0x7f070183

.field public static final grid_label_text_size:I = 0x7f070184

.field public static final grid_placeholder_text_max_size:I = 0x7f070185

.field public static final grid_plus_button_size:I = 0x7f070186

.field public static final grid_plus_button_stroke:I = 0x7f070187

.field public static final gutter_sheet_horizontal:I = 0x7f070188

.field public static final highlight_alpha_material_colored:I = 0x7f070189

.field public static final highlight_alpha_material_dark:I = 0x7f07018a

.field public static final highlight_alpha_material_light:I = 0x7f07018b

.field public static final hint_alpha_material_dark:I = 0x7f07018c

.field public static final hint_alpha_material_light:I = 0x7f07018d

.field public static final hint_pressed_alpha_material_dark:I = 0x7f07018e

.field public static final hint_pressed_alpha_material_light:I = 0x7f07018f

.field public static final holo_gap:I = 0x7f070190

.field public static final holo_gap_double:I = 0x7f070191

.field public static final holo_gap_half:I = 0x7f070192

.field public static final holo_gap_one_and_half:I = 0x7f070193

.field public static final holo_gap_six:I = 0x7f070194

.field public static final holo_gap_triple:I = 0x7f070195

.field public static final holo_gap_two_and_half:I = 0x7f070196

.field public static final holo_list_row_height:I = 0x7f070197

.field public static final holo_list_row_height_small:I = 0x7f070198

.field public static final home_item_badge_horizontal_spacing:I = 0x7f070199

.field public static final home_item_badge_vertical_spacing:I = 0x7f07019a

.field public static final home_view_sales_frame_shadow_width:I = 0x7f07019b

.field public static final hs__conversation_info_container_height:I = 0x7f07019c

.field public static final hs__download_icon_preview_size:I = 0x7f07019d

.field public static final hs__image_attachment_bubble_border_width:I = 0x7f07019e

.field public static final hs__image_attachment_bubble_corner_radius:I = 0x7f07019f

.field public static final hs__image_msg_preview_size:I = 0x7f0701a0

.field public static final hs__listPreferredItemHeightSmall:I = 0x7f0701a1

.field public static final hs__listPreferredItemPaddingBottom:I = 0x7f0701a2

.field public static final hs__listPreferredItemPaddingLeft:I = 0x7f0701a3

.field public static final hs__listPreferredItemPaddingTop:I = 0x7f0701a4

.field public static final hs__load_more_messages_spinner_size:I = 0x7f0701a5

.field public static final hs__msgActionButtonPadding:I = 0x7f0701a6

.field public static final hs__msgPreferredItemPaddingBottom:I = 0x7f0701a7

.field public static final hs__msgPreferredItemPaddingLeft:I = 0x7f0701a8

.field public static final hs__msgPreferredItemPaddingRight:I = 0x7f0701a9

.field public static final hs__msgPreferredItemPaddingTop:I = 0x7f0701aa

.field public static final hs__picker_empty_view_top_padding:I = 0x7f0701ab

.field public static final hs__picker_header_container_height:I = 0x7f0701ac

.field public static final hs__picker_header_icon_height:I = 0x7f0701ad

.field public static final hs__picker_header_icon_width:I = 0x7f0701ae

.field public static final hs__picker_icon_padding:I = 0x7f0701af

.field public static final hs__picker_top_shadow_height:I = 0x7f0701b0

.field public static final hs__screen_to_conversation_view_ratio:I = 0x7f0701b1

.field public static final hs__textSizeExtraSmall:I = 0x7f0701b2

.field public static final hs__textSizeLarge:I = 0x7f0701b3

.field public static final hs__textSizeMedium:I = 0x7f0701b4

.field public static final hs__textSizeSmall:I = 0x7f0701b5

.field public static final hs__user_msg_left_margin:I = 0x7f0701b6

.field public static final hs_issue_archival_view_horizontal_margin:I = 0x7f0701b7

.field public static final hs_support_recyclerview_item_vertical_padding:I = 0x7f0701b8

.field public static final hud_corner_radius:I = 0x7f0701b9

.field public static final hud_edge:I = 0x7f0701ba

.field public static final hud_glyph_text_gap:I = 0x7f0701bb

.field public static final hud_message_text_margin_top:I = 0x7f0701bc

.field public static final hud_text_line_spacing_extra:I = 0x7f0701bd

.field public static final image_attachment_progress_container_alpha:I = 0x7f0701be

.field public static final installments_glyph_size:I = 0x7f0701bf

.field public static final installments_margin_large:I = 0x7f0701c0

.field public static final installments_margin_medium:I = 0x7f0701c1

.field public static final installments_margin_small:I = 0x7f0701c2

.field public static final installments_qr_code_container_size:I = 0x7f0701c3

.field public static final installments_qr_code_size:I = 0x7f0701c4

.field public static final installments_qr_spinner_size:I = 0x7f0701c5

.field public static final installments_subtitle_size_medium:I = 0x7f0701c6

.field public static final installments_subtitle_size_small:I = 0x7f0701c7

.field public static final installments_title_size:I = 0x7f0701c8

.field public static final installments_title_size_small:I = 0x7f0701c9

.field public static final instant_deposits_progress_bar:I = 0x7f0701ca

.field public static final invoice_bottom_card_dialog_width:I = 0x7f0701cb

.field public static final invoice_date_picker_cell_gap:I = 0x7f0701cc

.field public static final invoice_date_picker_side_padding:I = 0x7f0701cd

.field public static final invoice_edit_bottom_button_height:I = 0x7f0701ce

.field public static final invoice_payment_request_toggle_size:I = 0x7f0701cf

.field public static final invoice_payment_type_logo_size:I = 0x7f0701d0

.field public static final invoice_timeline_bullet_container:I = 0x7f0701d1

.field public static final invoice_timeline_bullet_highlight:I = 0x7f0701d2

.field public static final invoice_timeline_bullet_size:I = 0x7f0701d3

.field public static final invoice_timeline_left_pad:I = 0x7f0701d4

.field public static final item_library_empty_icon_height:I = 0x7f0701d5

.field public static final item_touch_helper_max_drag_scroll_per_frame:I = 0x7f0701d6

.field public static final item_touch_helper_swipe_escape_max_velocity:I = 0x7f0701d7

.field public static final item_touch_helper_swipe_escape_velocity:I = 0x7f0701d8

.field public static final keypad_backspace_half_width:I = 0x7f0701d9

.field public static final keypad_horizontal_divider_padding:I = 0x7f0701da

.field public static final keypad_letters_text_size:I = 0x7f0701db

.field public static final keypad_line_spacing_extra:I = 0x7f0701dc

.field public static final keypad_screen_button_internal_margin:I = 0x7f0701dd

.field public static final keypad_screen_gutter:I = 0x7f0701de

.field public static final keypad_screen_keypad_gutter_horizontal:I = 0x7f0701df

.field public static final keypad_screen_star_gap:I = 0x7f0701e0

.field public static final keypad_screen_star_radius:I = 0x7f0701e1

.field public static final keypad_screen_title_size:I = 0x7f0701e2

.field public static final keypad_text_size:I = 0x7f0701e3

.field public static final label_text:I = 0x7f0701e4

.field public static final landing_button_height:I = 0x7f0701e5

.field public static final landing_gap:I = 0x7f0701e6

.field public static final library_create_new_item_dialog_button_margin:I = 0x7f0701e7

.field public static final library_create_new_item_dialog_padding:I = 0x7f0701e8

.field public static final location_button_border:I = 0x7f0701e9

.field public static final location_margin:I = 0x7f0701ea

.field public static final loyalty_cash_app_banner_height:I = 0x7f0701eb

.field public static final loyalty_cash_app_banner_margin:I = 0x7f0701ec

.field public static final loyalty_cash_app_banner_margin_half:I = 0x7f0701ed

.field public static final loyalty_cash_app_banner_text_size:I = 0x7f0701ee

.field public static final loyalty_cash_app_logo_height:I = 0x7f0701ef

.field public static final loyalty_contents_margin:I = 0x7f0701f0

.field public static final loyalty_contents_welcome_width:I = 0x7f0701f1

.field public static final loyalty_contents_width:I = 0x7f0701f2

.field public static final loyalty_enrollment_slide_up:I = 0x7f0701f3

.field public static final loyalty_phone_edit_text_size:I = 0x7f0701f4

.field public static final loyalty_report_summary_values_text_size:I = 0x7f0701f5

.field public static final loyalty_subtitle_text_size:I = 0x7f0701f6

.field public static final loyalty_subtitle_welcome_text_size:I = 0x7f0701f7

.field public static final loyalty_title_welcome_text_size:I = 0x7f0701f8

.field public static final loyalty_tour_help_text_frame_height:I = 0x7f0701f9

.field public static final marin_action_bar_button_min_width:I = 0x7f0701fa

.field public static final marin_action_bar_button_padding:I = 0x7f0701fb

.field public static final marin_action_bar_header_gutter:I = 0x7f0701fc

.field public static final marin_action_bar_height:I = 0x7f0701fd

.field public static final marin_action_bar_up_text_left_margin:I = 0x7f0701fe

.field public static final marin_actionbar_badge_horizontal:I = 0x7f0701ff

.field public static final marin_actionbar_badge_vertical:I = 0x7f070200

.field public static final marin_applet_grouping_title_height:I = 0x7f070201

.field public static final marin_badge_corner_radius:I = 0x7f070202

.field public static final marin_badge_padding_bottom:I = 0x7f070203

.field public static final marin_badge_padding_left:I = 0x7f070204

.field public static final marin_badge_padding_right:I = 0x7f070205

.field public static final marin_badge_padding_top:I = 0x7f070206

.field public static final marin_badge_stroke:I = 0x7f070207

.field public static final marin_buyer_flanking_view_min_dimen:I = 0x7f070208

.field public static final marin_buyer_gutter:I = 0x7f070209

.field public static final marin_buyer_gutter_half:I = 0x7f07020a

.field public static final marin_buyer_perimeter_gap:I = 0x7f07020b

.field public static final marin_buyer_receipt_gap:I = 0x7f07020c

.field public static final marin_buyer_receipt_gap_large:I = 0x7f07020d

.field public static final marin_buyer_receipt_gutter_horizontal_half:I = 0x7f07020e

.field public static final marin_buyer_receipt_gutter_vertical:I = 0x7f07020f

.field public static final marin_buyer_under_actionbar_vertical_spacing:I = 0x7f070210

.field public static final marin_calendar_max_tile_size:I = 0x7f070211

.field public static final marin_cart_button_width:I = 0x7f070212

.field public static final marin_cart_item_gutter:I = 0x7f070213

.field public static final marin_cart_item_gutter_half:I = 0x7f070214

.field public static final marin_cart_list_gutter_vertical:I = 0x7f070215

.field public static final marin_charge_height:I = 0x7f070216

.field public static final marin_charge_no_tickets_phone_text_size:I = 0x7f070217

.field public static final marin_charge_width:I = 0x7f070218

.field public static final marin_checkable_text_view_min_height:I = 0x7f070219

.field public static final marin_checkable_text_view_side_padding:I = 0x7f07021a

.field public static final marin_checkbox_size:I = 0x7f07021b

.field public static final marin_clickable_x_large_dimen:I = 0x7f07021c

.field public static final marin_dialog_title_height:I = 0x7f07021d

.field public static final marin_divider_height:I = 0x7f07021e

.field public static final marin_divider_width_1dp:I = 0x7f07021f

.field public static final marin_divider_width_1px:I = 0x7f070220

.field public static final marin_drop_down_row_height:I = 0x7f070221

.field public static final marin_font_body_1:I = 0x7f070222

.field public static final marin_font_body_2:I = 0x7f070223

.field public static final marin_gap_big:I = 0x7f070224

.field public static final marin_gap_calendar_vertical_padding:I = 0x7f070225

.field public static final marin_gap_charge_below:I = 0x7f070226

.field public static final marin_gap_flank_drawables:I = 0x7f070227

.field public static final marin_gap_glyph_title_above:I = 0x7f070228

.field public static final marin_gap_glyph_title_below:I = 0x7f070229

.field public static final marin_gap_grid_inner_padding:I = 0x7f07022a

.field public static final marin_gap_grid_outer_padding:I = 0x7f07022b

.field public static final marin_gap_grid_padding_wide:I = 0x7f07022c

.field public static final marin_gap_large:I = 0x7f07022d

.field public static final marin_gap_medium:I = 0x7f07022e

.field public static final marin_gap_medium_lollipop:I = 0x7f07022f

.field public static final marin_gap_medium_negated:I = 0x7f070230

.field public static final marin_gap_medium_plus_min_height:I = 0x7f070231

.field public static final marin_gap_mini:I = 0x7f070232

.field public static final marin_gap_multiline_padding:I = 0x7f070233

.field public static final marin_gap_null_state_favorites_grid:I = 0x7f070234

.field public static final marin_gap_null_state_with_button:I = 0x7f070235

.field public static final marin_gap_payment_pad_height_portrait:I = 0x7f070236

.field public static final marin_gap_payment_pad_inner_margin_portrait:I = 0x7f070237

.field public static final marin_gap_payment_pad_inner_padding_portrait:I = 0x7f070238

.field public static final marin_gap_payment_pad_outer_padding_landscape:I = 0x7f070239

.field public static final marin_gap_reader_image_padding:I = 0x7f07023a

.field public static final marin_gap_refund_policy_padding:I = 0x7f07023b

.field public static final marin_gap_section_header_below:I = 0x7f07023c

.field public static final marin_gap_section_header_extra_large:I = 0x7f07023d

.field public static final marin_gap_section_header_medium:I = 0x7f07023e

.field public static final marin_gap_signature_screen_button_margins:I = 0x7f07023f

.field public static final marin_gap_signature_screen_extra_padding:I = 0x7f070240

.field public static final marin_gap_signature_screen_padding:I = 0x7f070241

.field public static final marin_gap_signature_screen_tip_bar:I = 0x7f070242

.field public static final marin_gap_small:I = 0x7f070243

.field public static final marin_gap_tablet:I = 0x7f070244

.field public static final marin_gap_thumbnail_content:I = 0x7f070245

.field public static final marin_gap_tile_padding_horizontal:I = 0x7f070246

.field public static final marin_gap_tiny:I = 0x7f070247

.field public static final marin_gap_ultra_large:I = 0x7f070248

.field public static final marin_glyph_left_width:I = 0x7f070249

.field public static final marin_glyph_left_width_plus_padding:I = 0x7f07024a

.field public static final marin_grid_minimum_height:I = 0x7f07024b

.field public static final marin_grid_regular_tile_extra_height:I = 0x7f07024c

.field public static final marin_grid_stacked_tile_extra_height:I = 0x7f07024d

.field public static final marin_grid_tile_card_max_dimen:I = 0x7f07024e

.field public static final marin_grid_tile_max_font_size:I = 0x7f07024f

.field public static final marin_grid_tile_min_font_size:I = 0x7f070250

.field public static final marin_grid_tile_tag_max_dimen:I = 0x7f070251

.field public static final marin_grid_tile_text_size:I = 0x7f070252

.field public static final marin_gutter:I = 0x7f070253

.field public static final marin_gutter_applet_section_button_bottom:I = 0x7f070254

.field public static final marin_gutter_applet_section_button_inner:I = 0x7f070255

.field public static final marin_gutter_half:I = 0x7f070256

.field public static final marin_gutter_half_lollipop:I = 0x7f070257

.field public static final marin_gutter_lollipop:I = 0x7f070258

.field public static final marin_gutter_plus_mini:I = 0x7f070259

.field public static final marin_gutter_quarter:I = 0x7f07025a

.field public static final marin_gutter_row_indent:I = 0x7f07025b

.field public static final marin_gutter_sidebar:I = 0x7f07025c

.field public static final marin_inline_element_height:I = 0x7f07025d

.field public static final marin_inline_glyph_button_total_width:I = 0x7f07025e

.field public static final marin_inline_glyph_margin:I = 0x7f07025f

.field public static final marin_inline_glyph_size:I = 0x7f070260

.field public static final marin_invoice_price:I = 0x7f070261

.field public static final marin_keypad_top_bar_height:I = 0x7f070262

.field public static final marin_letterspacing_logo:I = 0x7f070263

.field public static final marin_library_thumbnail_size:I = 0x7f070264

.field public static final marin_lollipop_ball_radius_inner:I = 0x7f070265

.field public static final marin_lollipop_ball_radius_inner_with_stroke:I = 0x7f070266

.field public static final marin_lollipop_ball_size:I = 0x7f070267

.field public static final marin_lollipop_focus_offset:I = 0x7f070268

.field public static final marin_lollipop_focus_radius:I = 0x7f070269

.field public static final marin_lollipop_switch_stroke:I = 0x7f07026a

.field public static final marin_lollipop_switch_width:I = 0x7f07026b

.field public static final marin_max_message_width:I = 0x7f07026c

.field public static final marin_message_height:I = 0x7f07026d

.field public static final marin_min_height:I = 0x7f07026e

.field public static final marin_min_width:I = 0x7f07026f

.field public static final marin_min_width_plus_marin_gap_medium:I = 0x7f070270

.field public static final marin_min_width_plus_marin_gap_tiny:I = 0x7f070271

.field public static final marin_modal_gap_below_message:I = 0x7f070272

.field public static final marin_modal_gap_below_title:I = 0x7f070273

.field public static final marin_modal_gutter:I = 0x7f070274

.field public static final marin_modal_title_text_size:I = 0x7f070275

.field public static final marin_order_entry_tab_buttons_gone_height:I = 0x7f070276

.field public static final marin_order_entry_tab_buttons_height:I = 0x7f070277

.field public static final marin_order_entry_tab_buttons_slim_height:I = 0x7f070278

.field public static final marin_order_entry_tab_drawable_padding:I = 0x7f070279

.field public static final marin_order_entry_tab_vertical_padding:I = 0x7f07027a

.field public static final marin_overview_numbers:I = 0x7f07027b

.field public static final marin_page_indicator_default_diameter:I = 0x7f07027c

.field public static final marin_payment_row_glyph_left_width:I = 0x7f07027d

.field public static final marin_pinpad_glyph_margin:I = 0x7f07027e

.field public static final marin_pinpad_text_margin:I = 0x7f07027f

.field public static final marin_ribbon_height:I = 0x7f070280

.field public static final marin_ribbon_top_margin:I = 0x7f070281

.field public static final marin_ribbon_width:I = 0x7f070282

.field public static final marin_sales_overview_horizontal_gap:I = 0x7f070283

.field public static final marin_sales_overview_vertical_gap:I = 0x7f070284

.field public static final marin_sales_time:I = 0x7f070285

.field public static final marin_sign_line_small_x_dimen:I = 0x7f070286

.field public static final marin_slim_height:I = 0x7f070287

.field public static final marin_small_modal_line_height:I = 0x7f070288

.field public static final marin_split_tender_banner_height:I = 0x7f070289

.field public static final marin_split_tender_banner_text_size:I = 0x7f07028a

.field public static final marin_split_ticket_gutter:I = 0x7f07028b

.field public static final marin_split_ticket_gutter_half:I = 0x7f07028c

.field public static final marin_split_ticket_ticket_gutter_plus_checkbox:I = 0x7f07028d

.field public static final marin_split_ticket_width:I = 0x7f07028e

.field public static final marin_tall_list_row_height:I = 0x7f07028f

.field public static final marin_text_actionbar:I = 0x7f070290

.field public static final marin_text_badge:I = 0x7f070291

.field public static final marin_text_category:I = 0x7f070292

.field public static final marin_text_charge:I = 0x7f070293

.field public static final marin_text_charge_subtitle:I = 0x7f070294

.field public static final marin_text_default:I = 0x7f070295

.field public static final marin_text_display:I = 0x7f070296

.field public static final marin_text_drawer:I = 0x7f070297

.field public static final marin_text_header_title:I = 0x7f070298

.field public static final marin_text_headline:I = 0x7f070299

.field public static final marin_text_help:I = 0x7f07029a

.field public static final marin_text_home_pager:I = 0x7f07029b

.field public static final marin_text_jumbo_button:I = 0x7f07029c

.field public static final marin_text_keypad:I = 0x7f07029d

.field public static final marin_text_keypad_home:I = 0x7f07029e

.field public static final marin_text_landing_link:I = 0x7f07029f

.field public static final marin_text_landing_message:I = 0x7f0702a0

.field public static final marin_text_landing_message_world:I = 0x7f0702a1

.field public static final marin_text_logo:I = 0x7f0702a2

.field public static final marin_text_logo_min:I = 0x7f0702a3

.field public static final marin_text_message:I = 0x7f0702a4

.field public static final marin_text_ribbon:I = 0x7f0702a5

.field public static final marin_text_section_header:I = 0x7f0702a6

.field public static final marin_text_sign_title:I = 0x7f0702a7

.field public static final marin_text_subtext:I = 0x7f0702a8

.field public static final marin_text_tile_color_block_width:I = 0x7f0702a9

.field public static final marin_text_tile_container_height:I = 0x7f0702aa

.field public static final marin_text_tile_inner_padding:I = 0x7f0702ab

.field public static final marin_text_tile_outer_padding:I = 0x7f0702ac

.field public static final marin_text_tile_padding:I = 0x7f0702ad

.field public static final marin_text_title_1:I = 0x7f0702ae

.field public static final marin_text_title_2:I = 0x7f0702af

.field public static final marin_time_width:I = 0x7f0702b0

.field public static final marin_title_subtitle_row_height:I = 0x7f0702b1

.field public static final marin_undo_bar_corners:I = 0x7f0702b2

.field public static final marin_web_browser_progress_stroke_width:I = 0x7f0702b3

.field public static final marin_x2_max_message_width:I = 0x7f0702b4

.field public static final match_parent:I = 0x7f0702b5

.field public static final material_emphasis_disabled:I = 0x7f0702b6

.field public static final material_emphasis_high_type:I = 0x7f0702b7

.field public static final material_emphasis_medium:I = 0x7f0702b8

.field public static final material_text_view_test_line_height:I = 0x7f0702b9

.field public static final material_text_view_test_line_height_override:I = 0x7f0702ba

.field public static final max_drag_scroll_per_frame:I = 0x7f0702bb

.field public static final max_hint_drag:I = 0x7f0702bc

.field public static final merchant_featured_image_min_height:I = 0x7f0702bd

.field public static final merchant_logo_height:I = 0x7f0702be

.field public static final merchant_logo_width:I = 0x7f0702bf

.field public static final merchant_map_image_height:I = 0x7f0702c0

.field public static final message_new_line_spacing:I = 0x7f0702c1

.field public static final message_view_margin:I = 0x7f0702c2

.field public static final mtrl_alert_dialog_background_inset_bottom:I = 0x7f0702c3

.field public static final mtrl_alert_dialog_background_inset_end:I = 0x7f0702c4

.field public static final mtrl_alert_dialog_background_inset_start:I = 0x7f0702c5

.field public static final mtrl_alert_dialog_background_inset_top:I = 0x7f0702c6

.field public static final mtrl_alert_dialog_picker_background_inset:I = 0x7f0702c7

.field public static final mtrl_badge_horizontal_edge_offset:I = 0x7f0702c8

.field public static final mtrl_badge_long_text_horizontal_padding:I = 0x7f0702c9

.field public static final mtrl_badge_radius:I = 0x7f0702ca

.field public static final mtrl_badge_text_horizontal_edge_offset:I = 0x7f0702cb

.field public static final mtrl_badge_text_size:I = 0x7f0702cc

.field public static final mtrl_badge_with_text_radius:I = 0x7f0702cd

.field public static final mtrl_bottomappbar_fabOffsetEndMode:I = 0x7f0702ce

.field public static final mtrl_bottomappbar_fab_bottom_margin:I = 0x7f0702cf

.field public static final mtrl_bottomappbar_fab_cradle_margin:I = 0x7f0702d0

.field public static final mtrl_bottomappbar_fab_cradle_rounded_corner_radius:I = 0x7f0702d1

.field public static final mtrl_bottomappbar_fab_cradle_vertical_offset:I = 0x7f0702d2

.field public static final mtrl_bottomappbar_height:I = 0x7f0702d3

.field public static final mtrl_btn_corner_radius:I = 0x7f0702d4

.field public static final mtrl_btn_dialog_btn_min_width:I = 0x7f0702d5

.field public static final mtrl_btn_disabled_elevation:I = 0x7f0702d6

.field public static final mtrl_btn_disabled_z:I = 0x7f0702d7

.field public static final mtrl_btn_elevation:I = 0x7f0702d8

.field public static final mtrl_btn_focused_z:I = 0x7f0702d9

.field public static final mtrl_btn_hovered_z:I = 0x7f0702da

.field public static final mtrl_btn_icon_btn_padding_left:I = 0x7f0702db

.field public static final mtrl_btn_icon_padding:I = 0x7f0702dc

.field public static final mtrl_btn_inset:I = 0x7f0702dd

.field public static final mtrl_btn_letter_spacing:I = 0x7f0702de

.field public static final mtrl_btn_padding_bottom:I = 0x7f0702df

.field public static final mtrl_btn_padding_left:I = 0x7f0702e0

.field public static final mtrl_btn_padding_right:I = 0x7f0702e1

.field public static final mtrl_btn_padding_top:I = 0x7f0702e2

.field public static final mtrl_btn_pressed_z:I = 0x7f0702e3

.field public static final mtrl_btn_stroke_size:I = 0x7f0702e4

.field public static final mtrl_btn_text_btn_icon_padding:I = 0x7f0702e5

.field public static final mtrl_btn_text_btn_padding_left:I = 0x7f0702e6

.field public static final mtrl_btn_text_btn_padding_right:I = 0x7f0702e7

.field public static final mtrl_btn_text_size:I = 0x7f0702e8

.field public static final mtrl_btn_z:I = 0x7f0702e9

.field public static final mtrl_calendar_action_height:I = 0x7f0702ea

.field public static final mtrl_calendar_action_padding:I = 0x7f0702eb

.field public static final mtrl_calendar_bottom_padding:I = 0x7f0702ec

.field public static final mtrl_calendar_content_padding:I = 0x7f0702ed

.field public static final mtrl_calendar_day_corner:I = 0x7f0702ee

.field public static final mtrl_calendar_day_height:I = 0x7f0702ef

.field public static final mtrl_calendar_day_horizontal_padding:I = 0x7f0702f0

.field public static final mtrl_calendar_day_today_stroke:I = 0x7f0702f1

.field public static final mtrl_calendar_day_vertical_padding:I = 0x7f0702f2

.field public static final mtrl_calendar_day_width:I = 0x7f0702f3

.field public static final mtrl_calendar_days_of_week_height:I = 0x7f0702f4

.field public static final mtrl_calendar_dialog_background_inset:I = 0x7f0702f5

.field public static final mtrl_calendar_header_content_padding:I = 0x7f0702f6

.field public static final mtrl_calendar_header_content_padding_fullscreen:I = 0x7f0702f7

.field public static final mtrl_calendar_header_divider_thickness:I = 0x7f0702f8

.field public static final mtrl_calendar_header_height:I = 0x7f0702f9

.field public static final mtrl_calendar_header_height_fullscreen:I = 0x7f0702fa

.field public static final mtrl_calendar_header_selection_line_height:I = 0x7f0702fb

.field public static final mtrl_calendar_header_text_padding:I = 0x7f0702fc

.field public static final mtrl_calendar_header_toggle_margin_bottom:I = 0x7f0702fd

.field public static final mtrl_calendar_header_toggle_margin_top:I = 0x7f0702fe

.field public static final mtrl_calendar_landscape_header_width:I = 0x7f0702ff

.field public static final mtrl_calendar_maximum_default_fullscreen_minor_axis:I = 0x7f070300

.field public static final mtrl_calendar_month_horizontal_padding:I = 0x7f070301

.field public static final mtrl_calendar_month_vertical_padding:I = 0x7f070302

.field public static final mtrl_calendar_navigation_bottom_padding:I = 0x7f070303

.field public static final mtrl_calendar_navigation_height:I = 0x7f070304

.field public static final mtrl_calendar_navigation_top_padding:I = 0x7f070305

.field public static final mtrl_calendar_pre_l_text_clip_padding:I = 0x7f070306

.field public static final mtrl_calendar_selection_baseline_to_top_fullscreen:I = 0x7f070307

.field public static final mtrl_calendar_selection_text_baseline_to_bottom:I = 0x7f070308

.field public static final mtrl_calendar_selection_text_baseline_to_bottom_fullscreen:I = 0x7f070309

.field public static final mtrl_calendar_selection_text_baseline_to_top:I = 0x7f07030a

.field public static final mtrl_calendar_text_input_padding_top:I = 0x7f07030b

.field public static final mtrl_calendar_title_baseline_to_top:I = 0x7f07030c

.field public static final mtrl_calendar_title_baseline_to_top_fullscreen:I = 0x7f07030d

.field public static final mtrl_calendar_year_corner:I = 0x7f07030e

.field public static final mtrl_calendar_year_height:I = 0x7f07030f

.field public static final mtrl_calendar_year_horizontal_padding:I = 0x7f070310

.field public static final mtrl_calendar_year_vertical_padding:I = 0x7f070311

.field public static final mtrl_calendar_year_width:I = 0x7f070312

.field public static final mtrl_card_checked_icon_margin:I = 0x7f070313

.field public static final mtrl_card_checked_icon_size:I = 0x7f070314

.field public static final mtrl_card_corner_radius:I = 0x7f070315

.field public static final mtrl_card_dragged_z:I = 0x7f070316

.field public static final mtrl_card_elevation:I = 0x7f070317

.field public static final mtrl_card_spacing:I = 0x7f070318

.field public static final mtrl_chip_pressed_translation_z:I = 0x7f070319

.field public static final mtrl_chip_text_size:I = 0x7f07031a

.field public static final mtrl_exposed_dropdown_menu_popup_elevation:I = 0x7f07031b

.field public static final mtrl_exposed_dropdown_menu_popup_vertical_offset:I = 0x7f07031c

.field public static final mtrl_exposed_dropdown_menu_popup_vertical_padding:I = 0x7f07031d

.field public static final mtrl_extended_fab_bottom_padding:I = 0x7f07031e

.field public static final mtrl_extended_fab_corner_radius:I = 0x7f07031f

.field public static final mtrl_extended_fab_disabled_elevation:I = 0x7f070320

.field public static final mtrl_extended_fab_disabled_translation_z:I = 0x7f070321

.field public static final mtrl_extended_fab_elevation:I = 0x7f070322

.field public static final mtrl_extended_fab_end_padding:I = 0x7f070323

.field public static final mtrl_extended_fab_end_padding_icon:I = 0x7f070324

.field public static final mtrl_extended_fab_icon_size:I = 0x7f070325

.field public static final mtrl_extended_fab_icon_text_spacing:I = 0x7f070326

.field public static final mtrl_extended_fab_min_height:I = 0x7f070327

.field public static final mtrl_extended_fab_min_width:I = 0x7f070328

.field public static final mtrl_extended_fab_start_padding:I = 0x7f070329

.field public static final mtrl_extended_fab_start_padding_icon:I = 0x7f07032a

.field public static final mtrl_extended_fab_top_padding:I = 0x7f07032b

.field public static final mtrl_extended_fab_translation_z_base:I = 0x7f07032c

.field public static final mtrl_extended_fab_translation_z_hovered_focused:I = 0x7f07032d

.field public static final mtrl_extended_fab_translation_z_pressed:I = 0x7f07032e

.field public static final mtrl_fab_elevation:I = 0x7f07032f

.field public static final mtrl_fab_min_touch_target:I = 0x7f070330

.field public static final mtrl_fab_translation_z_hovered_focused:I = 0x7f070331

.field public static final mtrl_fab_translation_z_pressed:I = 0x7f070332

.field public static final mtrl_high_ripple_default_alpha:I = 0x7f070333

.field public static final mtrl_high_ripple_focused_alpha:I = 0x7f070334

.field public static final mtrl_high_ripple_hovered_alpha:I = 0x7f070335

.field public static final mtrl_high_ripple_pressed_alpha:I = 0x7f070336

.field public static final mtrl_large_touch_target:I = 0x7f070337

.field public static final mtrl_low_ripple_default_alpha:I = 0x7f070338

.field public static final mtrl_low_ripple_focused_alpha:I = 0x7f070339

.field public static final mtrl_low_ripple_hovered_alpha:I = 0x7f07033a

.field public static final mtrl_low_ripple_pressed_alpha:I = 0x7f07033b

.field public static final mtrl_min_touch_target_size:I = 0x7f07033c

.field public static final mtrl_navigation_elevation:I = 0x7f07033d

.field public static final mtrl_navigation_item_horizontal_padding:I = 0x7f07033e

.field public static final mtrl_navigation_item_icon_padding:I = 0x7f07033f

.field public static final mtrl_navigation_item_icon_size:I = 0x7f070340

.field public static final mtrl_navigation_item_shape_horizontal_margin:I = 0x7f070341

.field public static final mtrl_navigation_item_shape_vertical_margin:I = 0x7f070342

.field public static final mtrl_shape_corner_size_large_component:I = 0x7f070343

.field public static final mtrl_shape_corner_size_medium_component:I = 0x7f070344

.field public static final mtrl_shape_corner_size_small_component:I = 0x7f070345

.field public static final mtrl_snackbar_action_text_color_alpha:I = 0x7f070346

.field public static final mtrl_snackbar_background_corner_radius:I = 0x7f070347

.field public static final mtrl_snackbar_background_overlay_color_alpha:I = 0x7f070348

.field public static final mtrl_snackbar_margin:I = 0x7f070349

.field public static final mtrl_switch_thumb_elevation:I = 0x7f07034a

.field public static final mtrl_textinput_box_corner_radius_medium:I = 0x7f07034b

.field public static final mtrl_textinput_box_corner_radius_small:I = 0x7f07034c

.field public static final mtrl_textinput_box_label_cutout_padding:I = 0x7f07034d

.field public static final mtrl_textinput_box_stroke_width_default:I = 0x7f07034e

.field public static final mtrl_textinput_box_stroke_width_focused:I = 0x7f07034f

.field public static final mtrl_textinput_end_icon_margin_start:I = 0x7f070350

.field public static final mtrl_textinput_outline_box_expanded_padding:I = 0x7f070351

.field public static final mtrl_textinput_start_icon_margin_end:I = 0x7f070352

.field public static final mtrl_toolbar_default_height:I = 0x7f070353

.field public static final multiline_spacing:I = 0x7f070354

.field public static final nav_column_item_height:I = 0x7f070355

.field public static final nav_column_landscape_width:I = 0x7f070356

.field public static final no_scales_container_horizontal_padding:I = 0x7f070357

.field public static final no_scales_container_spacing_between_icon_and_text:I = 0x7f070358

.field public static final no_scales_container_vertical_padding:I = 0x7f070359

.field public static final noho_banner_height:I = 0x7f07035a

.field public static final noho_button_charge_height:I = 0x7f07035b

.field public static final noho_button_height:I = 0x7f07035c

.field public static final noho_button_height_two_lines:I = 0x7f07035d

.field public static final noho_button_horizontal_margin:I = 0x7f07035e

.field public static final noho_button_large_height:I = 0x7f07035f

.field public static final noho_button_two_lines_spacing:I = 0x7f070360

.field public static final noho_card_editor_min_height:I = 0x7f070361

.field public static final noho_checkable_row_widget_size:I = 0x7f070362

.field public static final noho_dialog_button_height:I = 0x7f070363

.field public static final noho_dialog_height:I = 0x7f070364

.field public static final noho_dialog_width:I = 0x7f070365

.field public static final noho_divider_hairline:I = 0x7f070366

.field public static final noho_divider_prominent:I = 0x7f070367

.field public static final noho_dropdown_icon_margin_end:I = 0x7f070368

.field public static final noho_dropdown_item_height:I = 0x7f070369

.field public static final noho_dropdown_item_padding_end:I = 0x7f07036a

.field public static final noho_dropdown_item_padding_start:I = 0x7f07036b

.field public static final noho_dropdown_menu_divider:I = 0x7f07036c

.field public static final noho_edit_default_margin:I = 0x7f07036d

.field public static final noho_edit_label_margin_with_text:I = 0x7f07036e

.field public static final noho_edit_note_margin_before:I = 0x7f07036f

.field public static final noho_edit_search_margin:I = 0x7f070370

.field public static final noho_gap_0:I = 0x7f070371

.field public static final noho_gap_12:I = 0x7f070372

.field public static final noho_gap_16:I = 0x7f070373

.field public static final noho_gap_20:I = 0x7f070374

.field public static final noho_gap_4:I = 0x7f070375

.field public static final noho_gap_40:I = 0x7f070376

.field public static final noho_gap_8:I = 0x7f070377

.field public static final noho_gap_big_not_converted_from_marin:I = 0x7f070378

.field public static final noho_gap_large_not_converted_from_marin:I = 0x7f070379

.field public static final noho_gap_medium_not_converted_from_marin:I = 0x7f07037a

.field public static final noho_gap_small_not_converted_from_marin:I = 0x7f07037b

.field public static final noho_gutter_card_alert_horizontal:I = 0x7f07037c

.field public static final noho_gutter_card_alert_vertical:I = 0x7f07037d

.field public static final noho_gutter_card_horizontal:I = 0x7f07037e

.field public static final noho_gutter_card_vertical:I = 0x7f07037f

.field public static final noho_gutter_detail:I = 0x7f070380

.field public static final noho_gutter_half_not_converted_from_marin:I = 0x7f070381

.field public static final noho_gutter_not_converted_from_marin:I = 0x7f070382

.field public static final noho_gutter_sheet_alert_horizontal:I = 0x7f070383

.field public static final noho_gutter_sheet_alert_vertical:I = 0x7f070384

.field public static final noho_gutter_sheet_horizontal:I = 0x7f070385

.field public static final noho_gutter_sheet_vertical:I = 0x7f070386

.field public static final noho_gutter_sheet_vertical_payment_flow:I = 0x7f070387

.field public static final noho_icon_button_padding:I = 0x7f070388

.field public static final noho_inner_shadow_height:I = 0x7f070389

.field public static final noho_message_null_state_icon_height:I = 0x7f07038a

.field public static final noho_message_text_height:I = 0x7f07038b

.field public static final noho_recycler_edges:I = 0x7f07038c

.field public static final noho_responsive_104_144_200:I = 0x7f07038d

.field public static final noho_responsive_16_16_16:I = 0x7f07038e

.field public static final noho_responsive_16_16_24:I = 0x7f07038f

.field public static final noho_responsive_16_24_32:I = 0x7f070390

.field public static final noho_responsive_24_24_40:I = 0x7f070391

.field public static final noho_responsive_40_40_64:I = 0x7f070392

.field public static final noho_responsive_4_4_8:I = 0x7f070393

.field public static final noho_responsive_56_64_96:I = 0x7f070394

.field public static final noho_responsive_text_13_13_21:I = 0x7f070395

.field public static final noho_responsive_text_14_16_20:I = 0x7f070396

.field public static final noho_responsive_text_16_18_24:I = 0x7f070397

.field public static final noho_responsive_text_18_22_28:I = 0x7f070398

.field public static final noho_responsive_text_24_26_34:I = 0x7f070399

.field public static final noho_responsive_text_32_40_56:I = 0x7f07039a

.field public static final noho_responsive_text_44_48_72:I = 0x7f07039b

.field public static final noho_row_accessory_gap_size:I = 0x7f07039c

.field public static final noho_row_accessory_size:I = 0x7f07039d

.field public static final noho_row_gap_size:I = 0x7f07039e

.field public static final noho_row_height:I = 0x7f07039f

.field public static final noho_row_height_payment_flow:I = 0x7f0703a0

.field public static final noho_row_section_header_lower_gap:I = 0x7f0703a1

.field public static final noho_row_section_header_upper_gap:I = 0x7f0703a2

.field public static final noho_row_standard_height:I = 0x7f0703a3

.field public static final noho_space_large:I = 0x7f0703a4

.field public static final noho_space_small:I = 0x7f0703a5

.field public static final noho_spacing_before_helper_text:I = 0x7f0703a6

.field public static final noho_spacing_large:I = 0x7f0703a7

.field public static final noho_spacing_small:I = 0x7f0703a8

.field public static final noho_squid_keypad_action_bar_offset:I = 0x7f0703a9

.field public static final noho_squid_keypad_button_text_size:I = 0x7f0703aa

.field public static final noho_squid_keypad_cancel_button_icon_height:I = 0x7f0703ab

.field public static final noho_squid_keypad_cancel_button_icon_width:I = 0x7f0703ac

.field public static final noho_squid_keypad_delete_icon_height:I = 0x7f0703ad

.field public static final noho_squid_keypad_delete_icon_width:I = 0x7f0703ae

.field public static final noho_squid_keypad_digit_text_bottom_margin:I = 0x7f0703af

.field public static final noho_squid_keypad_divider_width:I = 0x7f0703b0

.field public static final noho_squid_keypad_done_button_icon_height:I = 0x7f0703b1

.field public static final noho_squid_keypad_done_button_icon_width:I = 0x7f0703b2

.field public static final noho_squid_keypad_margin_bottom:I = 0x7f0703b3

.field public static final noho_squid_keypad_margin_top:I = 0x7f0703b4

.field public static final noho_squid_keypad_padding_horizontal:I = 0x7f0703b5

.field public static final noho_squid_keypad_subtext_bottom_margin:I = 0x7f0703b6

.field public static final noho_squid_keypad_subtext_size:I = 0x7f0703b7

.field public static final noho_squid_keypad_text_size:I = 0x7f0703b8

.field public static final noho_squid_keypad_title_margin_top:I = 0x7f0703b9

.field public static final noho_switch_height:I = 0x7f0703ba

.field public static final noho_switch_padding:I = 0x7f0703bb

.field public static final noho_switch_thumb_size:I = 0x7f0703bc

.field public static final noho_switch_width:I = 0x7f0703bd

.field public static final noho_text_size_banner:I = 0x7f0703be

.field public static final noho_text_size_body:I = 0x7f0703bf

.field public static final noho_text_size_body_2:I = 0x7f0703c0

.field public static final noho_text_size_button:I = 0x7f0703c1

.field public static final noho_text_size_display:I = 0x7f0703c2

.field public static final noho_text_size_display_2:I = 0x7f0703c3

.field public static final noho_text_size_heading:I = 0x7f0703c4

.field public static final noho_text_size_heading_2:I = 0x7f0703c5

.field public static final noho_text_size_helper:I = 0x7f0703c6

.field public static final noho_text_size_hint:I = 0x7f0703c7

.field public static final noho_text_size_input:I = 0x7f0703c8

.field public static final noho_text_size_label:I = 0x7f0703c9

.field public static final noho_text_size_label_2:I = 0x7f0703ca

.field public static final noho_text_size_notification:I = 0x7f0703cb

.field public static final noho_text_size_oversized:I = 0x7f0703cc

.field public static final noho_text_size_row_text_icon:I = 0x7f0703cd

.field public static final noho_text_size_secondary_label:I = 0x7f0703ce

.field public static final noho_text_size_subheader:I = 0x7f0703cf

.field public static final noho_text_size_topbar_action:I = 0x7f0703d0

.field public static final noho_text_size_topbar_title:I = 0x7f0703d1

.field public static final noho_title_over_text_padding:I = 0x7f0703d2

.field public static final noho_topbar_action_width:I = 0x7f0703d3

.field public static final noho_topbar_height:I = 0x7f0703d4

.field public static final noho_topbar_icon:I = 0x7f0703d5

.field public static final noho_topbar_padding:I = 0x7f0703d6

.field public static final notification_action_icon_size:I = 0x7f0703d7

.field public static final notification_action_text_size:I = 0x7f0703d8

.field public static final notification_big_circle_margin:I = 0x7f0703d9

.field public static final notification_content_margin_start:I = 0x7f0703da

.field public static final notification_large_icon_height:I = 0x7f0703db

.field public static final notification_large_icon_width:I = 0x7f0703dc

.field public static final notification_main_column_padding_top:I = 0x7f0703dd

.field public static final notification_media_narrow_margin:I = 0x7f0703de

.field public static final notification_right_icon_size:I = 0x7f0703df

.field public static final notification_right_side_padding_top:I = 0x7f0703e0

.field public static final notification_row_bottom_gap:I = 0x7f0703e1

.field public static final notification_row_corner:I = 0x7f0703e2

.field public static final notification_row_elevation:I = 0x7f0703e3

.field public static final notification_row_horizontal_margin:I = 0x7f0703e4

.field public static final notification_row_max_width:I = 0x7f0703e5

.field public static final notification_row_top_gap:I = 0x7f0703e6

.field public static final notification_small_icon_background_padding:I = 0x7f0703e7

.field public static final notification_small_icon_size_as_large:I = 0x7f0703e8

.field public static final notification_subtext_size:I = 0x7f0703e9

.field public static final notification_title_content_gap:I = 0x7f0703ea

.field public static final notification_top_pad:I = 0x7f0703eb

.field public static final notification_top_pad_large_text:I = 0x7f0703ec

.field public static final onboarding_image_size_large:I = 0x7f0703ed

.field public static final onboarding_image_size_medium:I = 0x7f0703ee

.field public static final onboarding_image_size_small:I = 0x7f0703ef

.field public static final onboarding_paragraph_no_padding:I = 0x7f0703f0

.field public static final onboarding_paragraph_padded:I = 0x7f0703f1

.field public static final online_checkout_loading_progressbar_height:I = 0x7f0703f2

.field public static final online_checkout_loading_progressbar_width:I = 0x7f0703f3

.field public static final online_checkout_qr_code_size:I = 0x7f0703f4

.field public static final open_ticket_row_amount_width:I = 0x7f0703f5

.field public static final open_ticket_row_detail_width:I = 0x7f0703f6

.field public static final order_square_card_image_width:I = 0x7f0703f7

.field public static final orderhub_loading_spinner_large:I = 0x7f0703f8

.field public static final orderhub_order_details_icon_size:I = 0x7f0703f9

.field public static final orderhub_order_details_width:I = 0x7f0703fa

.field public static final orderhub_order_list_top_margin:I = 0x7f0703fb

.field public static final orderhub_order_shipment_address_width:I = 0x7f0703fc

.field public static final overlay_on_collapsing_header:I = 0x7f0703fd

.field public static final page_indicator_margin_bottom:I = 0x7f0703fe

.field public static final pager_height:I = 0x7f0703ff

.field public static final pairing_content_view_line_spacing_extra:I = 0x7f070400

.field public static final pairing_image_gap:I = 0x7f070401

.field public static final pairing_select_a_reader_top_margin:I = 0x7f070402

.field public static final pairing_step_width:I = 0x7f070403

.field public static final passcode_settings_create_passcode_digit_margin:I = 0x7f070404

.field public static final passcode_settings_create_passcode_digit_text_size:I = 0x7f070405

.field public static final passcode_settings_create_passcode_digit_width:I = 0x7f070406

.field public static final passcode_settings_create_passcode_header_margin:I = 0x7f070407

.field public static final passcode_settings_gutter_horizontal:I = 0x7f070408

.field public static final passcode_settings_gutter_vertical:I = 0x7f070409

.field public static final payment_flow_spacing_horizontal:I = 0x7f07040a

.field public static final payment_flow_spacing_vertical:I = 0x7f07040b

.field public static final payment_options_container_margin_bottom:I = 0x7f07040c

.field public static final payment_options_container_margin_horizontal:I = 0x7f07040d

.field public static final payment_options_container_margin_vertical:I = 0x7f07040e

.field public static final payment_options_divider_height:I = 0x7f07040f

.field public static final payment_options_row_height:I = 0x7f070410

.field public static final payment_options_row_text_size:I = 0x7f070411

.field public static final payment_options_scroll_view_shadow_length:I = 0x7f070412

.field public static final payment_options_split_tender_cancel_padding:I = 0x7f070413

.field public static final payment_prompt_card_inserted_offset:I = 0x7f070414

.field public static final payment_prompt_card_removed_offset:I = 0x7f070415

.field public static final payment_prompt_card_slot_padding_top:I = 0x7f070416

.field public static final payment_prompt_charge_text_size:I = 0x7f070417

.field public static final payment_prompt_header_buttons_margin:I = 0x7f070418

.field public static final payment_prompt_nfc_icon_top_offset:I = 0x7f070419

.field public static final payment_prompt_nfc_margin_top:I = 0x7f07041a

.field public static final payment_prompt_payment_language_switcher_textSize:I = 0x7f07041b

.field public static final payment_prompt_payment_type_margin_top:I = 0x7f07041c

.field public static final payment_prompt_payment_type_margin_top_no_transaction_type:I = 0x7f07041d

.field public static final payment_prompt_swipe_insert_tap:I = 0x7f07041e

.field public static final payment_prompt_transaction_total_margin_top:I = 0x7f07041f

.field public static final payment_prompt_transaction_total_text_size:I = 0x7f070420

.field public static final payment_prompt_transaction_type_text_size:I = 0x7f070421

.field public static final payment_request_row_description_start_padding:I = 0x7f070422

.field public static final payment_request_row_number_box_width:I = 0x7f070423

.field public static final payment_settings_preview_margin:I = 0x7f070424

.field public static final payment_settings_preview_padding_horizontal:I = 0x7f070425

.field public static final payment_settings_preview_padding_vertical:I = 0x7f070426

.field public static final payment_type_settings_header_top_padding:I = 0x7f070427

.field public static final payment_type_settings_margin:I = 0x7f070428

.field public static final permission_passcode_group_bottom_margin:I = 0x7f070429

.field public static final permission_passcode_group_height:I = 0x7f07042a

.field public static final permission_passcode_group_top_margin:I = 0x7f07042b

.field public static final permission_passcode_pad_margin:I = 0x7f07042c

.field public static final permission_passcode_star_gap:I = 0x7f07042d

.field public static final permission_passcode_star_radius:I = 0x7f07042e

.field public static final pin_card_info_text_size:I = 0x7f07042f

.field public static final pin_digit_letter_size:I = 0x7f070430

.field public static final pin_digit_text_size:I = 0x7f070431

.field public static final pin_height:I = 0x7f070432

.field public static final pin_panel_padding:I = 0x7f070433

.field public static final pin_radius:I = 0x7f070434

.field public static final pin_star_gap:I = 0x7f070435

.field public static final pin_star_stroke:I = 0x7f070436

.field public static final pin_title_text_size:I = 0x7f070437

.field public static final pin_width:I = 0x7f070438

.field public static final pinpad_height:I = 0x7f070439

.field public static final pinpad_text_size:I = 0x7f07043a

.field public static final popup_actions_view_button_gap:I = 0x7f07043b

.field public static final popup_actions_view_padding:I = 0x7f07043c

.field public static final popup_actions_view_width:I = 0x7f07043d

.field public static final priority_badge_size:I = 0x7f07043e

.field public static final priority_no_text_badge_size:I = 0x7f07043f

.field public static final progress_popup_title_line_spacing_extra:I = 0x7f070440

.field public static final qr_code_size:I = 0x7f070441

.field public static final quantity_button_width:I = 0x7f070442

.field public static final quantity_editext_margin:I = 0x7f070443

.field public static final quick_amount_value_text_size_max:I = 0x7f070444

.field public static final quick_amount_value_text_size_min:I = 0x7f070445

.field public static final quick_amounts_line_width:I = 0x7f070446

.field public static final quick_amounts_list_padding:I = 0x7f070447

.field public static final quick_amounts_list_side_margin:I = 0x7f070448

.field public static final r6_first_time_video_text_margin_top:I = 0x7f070449

.field public static final r6_first_time_video_text_size:I = 0x7f07044a

.field public static final rate_dip_offset:I = 0x7f07044b

.field public static final rate_interac_card_bottom:I = 0x7f07044c

.field public static final rate_interac_card_top:I = 0x7f07044d

.field public static final rate_max_message_width:I = 0x7f07044e

.field public static final rate_tour_help_text_frame_height:I = 0x7f07044f

.field public static final reader_jack_size:I = 0x7f070450

.field public static final reader_message_bar_battery_vertical_offset:I = 0x7f070451

.field public static final receipt_disclaimer_gap:I = 0x7f070452

.field public static final receipt_disclaimer_text_size:I = 0x7f070453

.field public static final receipt_item_image_padding:I = 0x7f070454

.field public static final receipt_item_image_size:I = 0x7f070455

.field public static final responsive_112_128_128_128:I = 0x7f070456

.field public static final responsive_12_16_24:I = 0x7f070457

.field public static final responsive_12_20_32:I = 0x7f070458

.field public static final responsive_160_184_216_240:I = 0x7f070459

.field public static final responsive_16P48L_64_72_96:I = 0x7f07045a

.field public static final responsive_16P96L_64P0L_72P0L_96P0L:I = 0x7f07045b

.field public static final responsive_16_16_24:I = 0x7f07045c

.field public static final responsive_16_20_24:I = 0x7f07045d

.field public static final responsive_16_20_24_32:I = 0x7f07045e

.field public static final responsive_16_20_24_40:I = 0x7f07045f

.field public static final responsive_16_20_96:I = 0x7f070460

.field public static final responsive_16_32:I = 0x7f070461

.field public static final responsive_16_64P0L_72P0L_96P0L:I = 0x7f070462

.field public static final responsive_16_64_72_96:I = 0x7f070463

.field public static final responsive_192_240_384:I = 0x7f070464

.field public static final responsive_24_24_40:I = 0x7f070465

.field public static final responsive_24_32_40_40:I = 0x7f070466

.field public static final responsive_24_32_40_48:I = 0x7f070467

.field public static final responsive_24_32_48:I = 0x7f070468

.field public static final responsive_2_2_4:I = 0x7f070469

.field public static final responsive_320_400_480_560:I = 0x7f07046a

.field public static final responsive_32P48L_64_72_96:I = 0x7f07046b

.field public static final responsive_32_40_48_64:I = 0x7f07046c

.field public static final responsive_32_40_72:I = 0x7f07046d

.field public static final responsive_32_64_72_96:I = 0x7f07046e

.field public static final responsive_36_40:I = 0x7f07046f

.field public static final responsive_40_40_64:I = 0x7f070470

.field public static final responsive_48P96L_60P120L_96P192L:I = 0x7f070471

.field public static final responsive_48_48_60:I = 0x7f070472

.field public static final responsive_48_60_48:I = 0x7f070473

.field public static final responsive_48_60_72:I = 0x7f070474

.field public static final responsive_48_60_96:I = 0x7f070475

.field public static final responsive_4_4_8:I = 0x7f070476

.field public static final responsive_56_64_72_96:I = 0x7f070477

.field public static final responsive_56_64_96:I = 0x7f070478

.field public static final responsive_56_72_80_96:I = 0x7f070479

.field public static final responsive_56_72_96:I = 0x7f07047a

.field public static final responsive_60_60_72:I = 0x7f07047b

.field public static final responsive_60_72_96:I = 0x7f07047c

.field public static final responsive_64_72_104:I = 0x7f07047d

.field public static final responsive_8_12_15:I = 0x7f07047e

.field public static final responsive_96P192L_120P240L_192P384L:I = 0x7f07047f

.field public static final responsive_96_120_192:I = 0x7f070480

.field public static final responsive_96_128_128_128:I = 0x7f070481

.field public static final responsive_square_card_signature_button_margin:I = 0x7f070482

.field public static final responsive_square_card_signature_button_side:I = 0x7f070483

.field public static final responsive_square_card_signature_trash:I = 0x7f070484

.field public static final responsive_square_card_signature_trash_margin:I = 0x7f070485

.field public static final responsive_text_13_14_16_18:I = 0x7f070486

.field public static final responsive_text_13_15_17_20:I = 0x7f070487

.field public static final responsive_text_16_18_21_24:I = 0x7f070488

.field public static final responsive_text_18_21_24_28:I = 0x7f070489

.field public static final responsive_text_24_27_31_36:I = 0x7f07048a

.field public static final responsive_text_32_40_56_56:I = 0x7f07048b

.field public static final sales_report_amount_count_toggle_height:I = 0x7f07048c

.field public static final sales_report_calendar_navigation_padding:I = 0x7f07048d

.field public static final sales_report_caret_size:I = 0x7f07048e

.field public static final sales_report_chart_bar_spacing:I = 0x7f07048f

.field public static final sales_report_chart_dot_size:I = 0x7f070490

.field public static final sales_report_chart_height:I = 0x7f070491

.field public static final sales_report_chart_top_padding:I = 0x7f070492

.field public static final sales_report_horizontal_padding:I = 0x7f070493

.field public static final sales_report_horizontal_padding_responsive_16_64:I = 0x7f070494

.field public static final sales_report_payment_method_bar_width:I = 0x7f070495

.field public static final sales_report_payment_method_text_height:I = 0x7f070496

.field public static final sales_report_payment_method_text_height_responsive_56_57:I = 0x7f070497

.field public static final sales_report_summary_details_row_left_padding:I = 0x7f070498

.field public static final sales_report_summary_details_subrow_left_padding:I = 0x7f070499

.field public static final sales_report_time_selector_edge_prominent:I = 0x7f07049a

.field public static final sales_report_time_selector_height:I = 0x7f07049b

.field public static final sales_report_time_selector_height_responsive_40_56:I = 0x7f07049c

.field public static final sales_report_top_bar_title_padding:I = 0x7f07049d

.field public static final sales_report_top_bar_title_padding_responsive_0_4:I = 0x7f07049e

.field public static final sales_report_vertical_padding:I = 0x7f07049f

.field public static final sales_report_vertical_padding_responsive_32_64:I = 0x7f0704a0

.field public static final sample_image_tile_height:I = 0x7f0704a1

.field public static final sample_text_tile_height:I = 0x7f0704a2

.field public static final sample_text_tile_width:I = 0x7f0704a3

.field public static final sample_tile_container_height:I = 0x7f0704a4

.field public static final sample_tile_gap:I = 0x7f0704a5

.field public static final scales_container_horizontal_padding:I = 0x7f0704a6

.field public static final scales_container_top_padding:I = 0x7f0704a7

.field public static final scales_list_footer_vertical_padding:I = 0x7f0704a8

.field public static final scales_list_header_vertical_padding:I = 0x7f0704a9

.field public static final scaling_text_view_minimum_font_size:I = 0x7f0704aa

.field public static final secure_touch_mode_selection_screen_button_border_width:I = 0x7f0704ab

.field public static final secure_touch_mode_selection_screen_button_gap:I = 0x7f0704ac

.field public static final secure_touch_mode_selection_screen_button_height:I = 0x7f0704ad

.field public static final secure_touch_mode_selection_screen_display_amount_text_size:I = 0x7f0704ae

.field public static final secure_touch_mode_selection_screen_padding:I = 0x7f0704af

.field public static final secure_touch_mode_selection_screen_text_size:I = 0x7f0704b0

.field public static final secure_touch_mode_selection_screen_title_padding:I = 0x7f0704b1

.field public static final select_address_padding:I = 0x7f0704b2

.field public static final select_address_padding_start:I = 0x7f0704b3

.field public static final selected_tab_corner:I = 0x7f0704b4

.field public static final setup_guide_elevation:I = 0x7f0704b5

.field public static final setup_guide_intro_card_icon_side:I = 0x7f0704b6

.field public static final setup_guide_intro_card_subtitle_margin_top:I = 0x7f0704b7

.field public static final setup_guide_rectangle_radius:I = 0x7f0704b8

.field public static final shadow_generator_elevation:I = 0x7f0704b9

.field public static final shadow_generator_margin:I = 0x7f0704ba

.field public static final sign_line_small_x_dimen:I = 0x7f0704bb

.field public static final signature_line_height:I = 0x7f0704bc

.field public static final signature_stroke_width:I = 0x7f0704bd

.field public static final smart_line_row_badge_margin_bottom:I = 0x7f0704be

.field public static final sms_marketing_button_height:I = 0x7f0704bf

.field public static final sms_marketing_button_width:I = 0x7f0704c0

.field public static final sms_marketing_check_margin:I = 0x7f0704c1

.field public static final sms_marketing_check_size:I = 0x7f0704c2

.field public static final sms_marketing_coupon_contents_margin:I = 0x7f0704c3

.field public static final sms_marketing_coupon_height:I = 0x7f0704c4

.field public static final sms_marketing_coupon_terms_size:I = 0x7f0704c5

.field public static final sms_marketing_coupon_title_margin:I = 0x7f0704c6

.field public static final sms_marketing_coupon_title_size:I = 0x7f0704c7

.field public static final sms_marketing_coupon_value_margin:I = 0x7f0704c8

.field public static final sms_marketing_coupon_value_size:I = 0x7f0704c9

.field public static final sms_marketing_coupon_width:I = 0x7f0704ca

.field public static final sms_marketing_disclaimer_size:I = 0x7f0704cb

.field public static final sms_marketing_margin_large:I = 0x7f0704cc

.field public static final sms_marketing_margin_medium:I = 0x7f0704cd

.field public static final sms_marketing_title_size:I = 0x7f0704ce

.field public static final splash_page_button_bar_bottom_margin:I = 0x7f0704cf

.field public static final splash_page_button_bar_button_height:I = 0x7f0704d0

.field public static final splash_page_button_bar_button_text_size:I = 0x7f0704d1

.field public static final splash_page_button_bar_button_top_margin:I = 0x7f0704d2

.field public static final splash_page_button_bar_page_indicator_bottom_margin:I = 0x7f0704d3

.field public static final splash_page_button_bar_side_margin:I = 0x7f0704d4

.field public static final splash_page_button_bar_width:I = 0x7f0704d5

.field public static final splash_page_image_bottom_margin:I = 0x7f0704d6

.field public static final splash_page_logo_size:I = 0x7f0704d7

.field public static final splash_page_logo_top_margin:I = 0x7f0704d8

.field public static final splash_page_text_bottom_margin:I = 0x7f0704d9

.field public static final splash_page_text_line_height_multiplier:I = 0x7f0704da

.field public static final splash_page_text_side_margin:I = 0x7f0704db

.field public static final splash_page_text_size:I = 0x7f0704dc

.field public static final splash_page_text_width:I = 0x7f0704dd

.field public static final split_tender_banner_height:I = 0x7f0704de

.field public static final split_tender_banner_text_size:I = 0x7f0704df

.field public static final split_tender_edit_amount_internal_padding:I = 0x7f0704e0

.field public static final square_card_disabled_icon_size:I = 0x7f0704e1

.field public static final square_card_disabled_message_spacing:I = 0x7f0704e2

.field public static final square_card_max_width_for_signature_screen:I = 0x7f0704e3

.field public static final square_card_max_width_for_status_screens:I = 0x7f0704e4

.field public static final square_card_ordered_phone_image_height:I = 0x7f0704e5

.field public static final square_card_ordered_phone_image_width:I = 0x7f0704e6

.field public static final square_card_outline_corner_radius:I = 0x7f0704e7

.field public static final square_card_outline_dash_gap:I = 0x7f0704e8

.field public static final square_card_outline_dash_width:I = 0x7f0704e9

.field public static final square_card_outline_width:I = 0x7f0704ea

.field public static final square_card_preview_back_name_spacing:I = 0x7f0704eb

.field public static final square_card_preview_background_corner_radius:I = 0x7f0704ec

.field public static final square_card_preview_background_corner_radius_small:I = 0x7f0704ed

.field public static final square_card_preview_elevation:I = 0x7f0704ee

.field public static final square_card_preview_elevation_disabled:I = 0x7f0704ef

.field public static final square_card_preview_elevation_offset_horizontal:I = 0x7f0704f0

.field public static final square_card_preview_elevation_offset_vertical:I = 0x7f0704f1

.field public static final square_card_preview_small_spacing:I = 0x7f0704f2

.field public static final square_card_signature_indicator_top_spacing:I = 0x7f0704f3

.field public static final square_card_signature_spacing:I = 0x7f0704f4

.field public static final square_card_stamp_preview_padding:I = 0x7f0704f5

.field public static final square_card_stamps_peek_height:I = 0x7f0704f6

.field public static final square_card_upsell_elevation:I = 0x7f0704f7

.field public static final square_card_upsell_max_width:I = 0x7f0704f8

.field public static final square_card_upsell_preview_height:I = 0x7f0704f9

.field public static final square_card_upsell_spacing:I = 0x7f0704fa

.field public static final square_card_upsell_spacing_small:I = 0x7f0704fb

.field public static final status_bar_font_size:I = 0x7f0704fc

.field public static final status_bar_glyph_font_size:I = 0x7f0704fd

.field public static final status_bar_height:I = 0x7f0704fe

.field public static final status_bar_icon_height:I = 0x7f0704ff

.field public static final status_bar_icon_padding_horizontal:I = 0x7f070500

.field public static final status_bar_padding_full:I = 0x7f070501

.field public static final status_bar_padding_half:I = 0x7f070502

.field public static final status_bar_widget_width:I = 0x7f070503

.field public static final subsection_header_bottom_padding:I = 0x7f070504

.field public static final subsection_header_top_padding:I = 0x7f070505

.field public static final subtitle_corner_radius:I = 0x7f070506

.field public static final subtitle_outline_width:I = 0x7f070507

.field public static final subtitle_shadow_offset:I = 0x7f070508

.field public static final subtitle_shadow_radius:I = 0x7f070509

.field public static final swipe_failed_background_corner_radius:I = 0x7f07050a

.field public static final swipe_failed_card_corner_radius:I = 0x7f07050b

.field public static final swipe_failed_hgap:I = 0x7f07050c

.field public static final swipe_failed_message:I = 0x7f07050d

.field public static final swipe_failed_title:I = 0x7f07050e

.field public static final switch_employee_tool_tip_dialog_top_nav_offset:I = 0x7f07050f

.field public static final switcher_button_font_size:I = 0x7f070510

.field public static final switcher_button_height:I = 0x7f070511

.field public static final switcher_button_margin_half:I = 0x7f070512

.field public static final switcher_button_vertical_strip_margin:I = 0x7f070513

.field public static final switcher_button_vertical_strip_width:I = 0x7f070514

.field public static final switcher_button_width:I = 0x7f070515

.field public static final t2_image_height:I = 0x7f070516

.field public static final t2_image_left_offset:I = 0x7f070517

.field public static final t2_image_vertical_offset:I = 0x7f070518

.field public static final t2_image_width:I = 0x7f070519

.field public static final t2_screen_image_height:I = 0x7f07051a

.field public static final t2_screen_image_vertical_offset:I = 0x7f07051b

.field public static final t2_screen_image_width:I = 0x7f07051c

.field public static final tab_bar_corner:I = 0x7f07051d

.field public static final tab_bar_height:I = 0x7f07051e

.field public static final tab_bar_padding:I = 0x7f07051f

.field public static final tab_bar_selected_elevation:I = 0x7f070520

.field public static final tab_bar_unselected_elevation:I = 0x7f070521

.field public static final test_mtrl_calendar_day_cornerSize:I = 0x7f070522

.field public static final text_above_image_splash_page_image_top_margin:I = 0x7f070523

.field public static final text_above_image_splash_page_logo_top_margin:I = 0x7f070524

.field public static final text_above_image_splash_page_text_top_margin:I = 0x7f070525

.field public static final text_android_large:I = 0x7f070526

.field public static final text_android_medium:I = 0x7f070527

.field public static final text_android_micro:I = 0x7f070528

.field public static final text_android_mini:I = 0x7f070529

.field public static final text_android_small:I = 0x7f07052a

.field public static final text_android_xlarge:I = 0x7f07052b

.field public static final text_checkout_headline:I = 0x7f07052c

.field public static final text_checkout_subtitle:I = 0x7f07052d

.field public static final text_color_alpha:I = 0x7f07052e

.field public static final text_occluded_color_alpha:I = 0x7f07052f

.field public static final text_receipt_banner_body:I = 0x7f070530

.field public static final text_receipt_banner_title:I = 0x7f070531

.field public static final time_picker_size:I = 0x7f070532

.field public static final tip_option_width:I = 0x7f070533

.field public static final tipping_screen_collect_tips_extra_bottom:I = 0x7f070534

.field public static final tipping_screen_headers_space_after:I = 0x7f070535

.field public static final tipping_screen_heading_extra_top:I = 0x7f070536

.field public static final tipping_screen_section_padding_horizontal:I = 0x7f070537

.field public static final tipping_screen_section_padding_vertical:I = 0x7f070538

.field public static final title_text:I = 0x7f070539

.field public static final title_text_default_bottom_padding:I = 0x7f07053a

.field public static final title_text_default_top_padding:I = 0x7f07053b

.field public static final tooltip_arrow_height:I = 0x7f07053c

.field public static final tooltip_arrow_margin:I = 0x7f07053d

.field public static final tooltip_arrow_width:I = 0x7f07053e

.field public static final tooltip_corner_radius:I = 0x7f07053f

.field public static final tooltip_elevation:I = 0x7f070540

.field public static final tooltip_horizontal_padding:I = 0x7f070541

.field public static final tooltip_margin:I = 0x7f070542

.field public static final tooltip_precise_anchor_extra_offset:I = 0x7f070543

.field public static final tooltip_precise_anchor_threshold:I = 0x7f070544

.field public static final tooltip_tip_margin_horizontal:I = 0x7f070545

.field public static final tooltip_vertical_padding:I = 0x7f070546

.field public static final tooltip_y_offset_non_touch:I = 0x7f070547

.field public static final tooltip_y_offset_touch:I = 0x7f070548

.field public static final tour_help_text_frame_height:I = 0x7f070549

.field public static final transactions_history_after_search_bar_icon_padding:I = 0x7f07054a

.field public static final transactions_history_badge_margin_right:I = 0x7f07054b

.field public static final transactions_history_before_search_bar_icon_margin:I = 0x7f07054c

.field public static final transactions_history_divider:I = 0x7f07054d

.field public static final transactions_history_divider_offset:I = 0x7f07054e

.field public static final transactions_history_header_bottom_padding:I = 0x7f07054f

.field public static final transactions_history_header_height:I = 0x7f070550

.field public static final transactions_history_header_horizontal_padding:I = 0x7f070551

.field public static final transactions_history_header_text_size:I = 0x7f070552

.field public static final transactions_history_header_top_padding:I = 0x7f070553

.field public static final transactions_history_list_row_height:I = 0x7f070554

.field public static final transactions_history_list_row_horizontal_padding:I = 0x7f070555

.field public static final transactions_history_list_row_subtitle_text_size:I = 0x7f070556

.field public static final transactions_history_list_row_title_bottom_padding:I = 0x7f070557

.field public static final transactions_history_list_row_title_text_size:I = 0x7f070558

.field public static final transactions_history_list_row_value_text_size:I = 0x7f070559

.field public static final transactions_history_search_bar_height:I = 0x7f07055a

.field public static final transactions_history_search_bar_magnifying_glass_icon_large_height:I = 0x7f07055b

.field public static final transactions_history_search_bar_magnifying_glass_icon_large_width:I = 0x7f07055c

.field public static final tutorial2_button_corner_radius:I = 0x7f07055d

.field public static final tutorial2_button_height:I = 0x7f07055e

.field public static final tutorial2_button_min_width:I = 0x7f07055f

.field public static final tutorial2_height:I = 0x7f070560

.field public static final tutorial2_tooltip_margin:I = 0x7f070561

.field public static final tutorial2_tooltip_width:I = 0x7f070562

.field public static final tutorial_dialog_icon_size:I = 0x7f070563

.field public static final tutorial_height:I = 0x7f070564

.field public static final tutorial_x_button:I = 0x7f070565

.field public static final two_factor_google_auth_qr_code_size:I = 0x7f070566

.field public static final underline_page_indicator_height:I = 0x7f070567

.field public static final upsell_medium_gap:I = 0x7f070568

.field public static final v2_divider_height:I = 0x7f070569

.field public static final v2_indented_row_margin_start:I = 0x7f07056a

.field public static final v2_row_description_text_size:I = 0x7f07056b

.field public static final v2_row_height:I = 0x7f07056c

.field public static final v2_row_image_size:I = 0x7f07056d

.field public static final v2_row_vertical_padding:I = 0x7f07056e

.field public static final v2_small_row_height:I = 0x7f07056f

.field public static final world_landing_horizontal_padding:I = 0x7f070570

.field public static final world_landing_vertical_padding:I = 0x7f070571

.field public static final wrap_content:I = 0x7f070572

.field public static final wrapping_name_value_row_minimum_font_size:I = 0x7f070573

.field public static final x2_dialog_min_height:I = 0x7f070574

.field public static final x2_dialog_width:I = 0x7f070575

.field public static final x2_gap_large:I = 0x7f070576

.field public static final x2_gap_small:I = 0x7f070577

.field public static final x2_glyph_size:I = 0x7f070578

.field public static final x2_image_title_gap:I = 0x7f070579

.field public static final x2_min_width:I = 0x7f07057a

.field public static final x2_text_size_extra_large:I = 0x7f07057b

.field public static final x2_text_size_extra_small:I = 0x7f07057c

.field public static final x2_text_size_large:I = 0x7f07057d

.field public static final x2_text_size_medium:I = 0x7f07057e

.field public static final x2_text_size_small:I = 0x7f07057f

.field public static final x2_title_message_gap:I = 0x7f070580


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
