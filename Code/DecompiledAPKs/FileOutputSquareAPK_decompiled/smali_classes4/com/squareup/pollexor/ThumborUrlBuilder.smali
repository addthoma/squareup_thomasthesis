.class public final Lcom/squareup/pollexor/ThumborUrlBuilder;
.super Ljava/lang/Object;
.source "ThumborUrlBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;,
        Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;,
        Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;,
        Lcom/squareup/pollexor/ThumborUrlBuilder$VerticalAlign;,
        Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;
    }
.end annotation


# static fields
.field private static final FILTER_BLUR:Ljava/lang/String; = "blur"

.field private static final FILTER_BRIGHTNESS:Ljava/lang/String; = "brightness"

.field private static final FILTER_CONTRAST:Ljava/lang/String; = "contrast"

.field private static final FILTER_EQUALIZE:Ljava/lang/String; = "equalize"

.field private static final FILTER_FILL:Ljava/lang/String; = "fill"

.field private static final FILTER_FORMAT:Ljava/lang/String; = "format"

.field private static final FILTER_FRAME:Ljava/lang/String; = "frame"

.field private static final FILTER_GRAYSCALE:Ljava/lang/String; = "grayscale"

.field private static final FILTER_NOISE:Ljava/lang/String; = "noise"

.field private static final FILTER_NO_UPSCALE:Ljava/lang/String; = "no_upscale"

.field private static final FILTER_QUALITY:Ljava/lang/String; = "quality"

.field private static final FILTER_RGB:Ljava/lang/String; = "rgb"

.field private static final FILTER_ROTATE:Ljava/lang/String; = "rotate"

.field private static final FILTER_ROUND_CORNER:Ljava/lang/String; = "round_corner"

.field private static final FILTER_SHARPEN:Ljava/lang/String; = "sharpen"

.field private static final FILTER_STRIP_ICC:Ljava/lang/String; = "strip_icc"

.field private static final FILTER_WATERMARK:Ljava/lang/String; = "watermark"

.field public static final ORIGINAL_SIZE:I = -0x80000000

.field private static final PART_FILTERS:Ljava/lang/String; = "filters"

.field private static final PART_SMART:Ljava/lang/String; = "smart"

.field private static final PART_TRIM:Ljava/lang/String; = "trim"

.field private static final PREFIX_META:Ljava/lang/String; = "meta/"

.field private static final PREFIX_UNSAFE:Ljava/lang/String; = "unsafe/"


# instance fields
.field cropBottom:I

.field cropHorizontalAlign:Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

.field cropLeft:I

.field cropRight:I

.field cropTop:I

.field cropVerticalAlign:Lcom/squareup/pollexor/ThumborUrlBuilder$VerticalAlign;

.field filters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field fitInStyle:Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

.field flipHorizontally:Z

.field flipVertically:Z

.field hasCrop:Z

.field hasResize:Z

.field final host:Ljava/lang/String;

.field final image:Ljava/lang/String;

.field isLegacy:Z

.field isSmart:Z

.field isTrim:Z

.field final key:Ljava/lang/String;

.field resizeHeight:I

.field resizeWidth:I

.field trimColorTolerance:I

.field trimPixelColor:Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    iput-object p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->host:Ljava/lang/String;

    .line 124
    iput-object p2, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->key:Ljava/lang/String;

    .line 125
    iput-object p3, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->image:Ljava/lang/String;

    return-void
.end method

.method public static blur(I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 792
    invoke-static {p0, v0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->blur(II)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static blur(II)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    if-lt p0, v0, :cond_2

    const/16 v0, 0x96

    if-gt p0, v0, :cond_1

    if-ltz p1, :cond_0

    .line 811
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "blur("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ","

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 809
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Sigma must be greater than zero."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 806
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Radius must be lower or equal than 150."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 803
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Radius must be greater than zero."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static brightness(I)Ljava/lang/String;
    .locals 2

    const/16 v0, -0x64

    if-lt p0, v0, :cond_0

    const/16 v0, 0x64

    if-gt p0, v0, :cond_0

    .line 510
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "brightness("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 508
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Amount must be between -100 and 100, inclusive."

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static contrast(I)Ljava/lang/String;
    .locals 2

    const/16 v0, -0x64

    if-lt p0, v0, :cond_0

    const/16 v0, 0x64

    if-gt p0, v0, :cond_0

    .line 524
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "contrast("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 522
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Amount must be between -100 and 100, inclusive."

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static equalize()Ljava/lang/String;
    .locals 1

    const-string v0, "equalize()"

    return-object v0
.end method

.method public static fill(I)Ljava/lang/String;
    .locals 2

    const v0, 0xffffff

    and-int/2addr p0, v0

    .line 741
    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p0

    .line 742
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fill("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static format(Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_0

    .line 754
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "format("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;->value:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 752
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "You must specify an image format."

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static frame(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_0

    .line 764
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 767
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "frame("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 765
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Image URL must not be blank."

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static grayscale()Ljava/lang/String;
    .locals 1

    const-string v0, "grayscale()"

    return-object v0
.end method

.method public static noUpscale()Ljava/lang/String;
    .locals 1

    const-string v0, "no_upscale()"

    return-object v0
.end method

.method public static noise(I)Ljava/lang/String;
    .locals 2

    if-ltz p0, :cond_0

    const/16 v0, 0x64

    if-gt p0, v0, :cond_0

    .line 537
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "noise("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 535
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Amount must be between 0 and 100, inclusive"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static quality(I)Ljava/lang/String;
    .locals 2

    if-ltz p0, :cond_0

    const/16 v0, 0x64

    if-gt p0, v0, :cond_0

    .line 550
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "quality("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 548
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Amount must be between 0 and 100, inclusive."

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static rgb(III)Ljava/lang/String;
    .locals 2

    const/16 v0, -0x64

    if-lt p0, v0, :cond_2

    const/16 v1, 0x64

    if-gt p0, v1, :cond_2

    if-lt p1, v0, :cond_1

    if-gt p1, v1, :cond_1

    if-lt p2, v0, :cond_0

    if-gt p2, v1, :cond_0

    .line 571
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "rgb("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ","

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 569
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Blue value must be between -100 and 100, inclusive."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 566
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Green value must be between -100 and 100, inclusive."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 563
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Red value must be between -100 and 100, inclusive."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static rotate(I)Ljava/lang/String;
    .locals 2

    .line 826
    rem-int/lit8 v0, p0, 0x5a

    if-nez v0, :cond_0

    .line 829
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "rotate("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 827
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Angle must be multiple of 90\u00b0"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static roundCorner(I)Ljava/lang/String;
    .locals 1

    const v0, 0xffffff

    .line 580
    invoke-static {p0, v0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->roundCorner(II)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static roundCorner(II)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 590
    invoke-static {p0, v0, p1}, Lcom/squareup/pollexor/ThumborUrlBuilder;->roundCorner(III)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static roundCorner(III)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    if-lt p0, v0, :cond_2

    if-ltz p1, :cond_1

    .line 608
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "round_corner"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    if-lez p1, :cond_0

    const-string/jumbo p0, "|"

    .line 610
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    const/high16 p0, 0xff0000

    and-int/2addr p0, p2

    ushr-int/lit8 p0, p0, 0x10

    const p1, 0xff00

    and-int/2addr p1, p2

    ushr-int/lit8 p1, p1, 0x8

    and-int/lit16 p2, p2, 0xff

    const-string v1, ","

    .line 615
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 616
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 617
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 618
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 619
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 606
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Outer radius must be greater than or equal to zero."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 603
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Radius must be greater than zero."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static sharpen(FFZ)Ljava/lang/String;
    .locals 2

    .line 731
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sharpen("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string p0, ","

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static stripicc()Ljava/lang/String;
    .locals 1

    const-string v0, "strip_icc()"

    return-object v0
.end method

.method public static watermark(Lcom/squareup/pollexor/ThumborUrlBuilder;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 641
    invoke-static {p0, v0, v0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->watermark(Lcom/squareup/pollexor/ThumborUrlBuilder;II)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static watermark(Lcom/squareup/pollexor/ThumborUrlBuilder;II)Ljava/lang/String;
    .locals 1

    if-eqz p0, :cond_0

    .line 674
    invoke-virtual {p0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->watermark(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 672
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Image must not be null."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static watermark(Lcom/squareup/pollexor/ThumborUrlBuilder;III)Ljava/lang/String;
    .locals 0

    if-eqz p0, :cond_0

    .line 718
    invoke-virtual {p0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p1, p2, p3}, Lcom/squareup/pollexor/ThumborUrlBuilder;->watermark(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 716
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Image must not be null."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static watermark(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 630
    invoke-static {p0, v0, v0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->watermark(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static watermark(Ljava/lang/String;II)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 656
    invoke-static {p0, p1, p2, v0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->watermark(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static watermark(Ljava/lang/String;III)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_1

    .line 692
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    if-ltz p3, :cond_0

    const/16 v0, 0x64

    if-gt p3, v0, :cond_0

    .line 698
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "watermark("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ","

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 696
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Transparency must be between 0 and 100, inclusive."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 693
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Image URL must not be blank."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public align(Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;)Lcom/squareup/pollexor/ThumborUrlBuilder;
    .locals 1

    .line 239
    iget-boolean v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->hasResize:Z

    if-eqz v0, :cond_0

    .line 242
    iput-object p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->cropHorizontalAlign:Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    return-object p0

    .line 240
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Image must be resized first in order to align."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public align(Lcom/squareup/pollexor/ThumborUrlBuilder$VerticalAlign;)Lcom/squareup/pollexor/ThumborUrlBuilder;
    .locals 1

    .line 253
    iget-boolean v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->hasResize:Z

    if-eqz v0, :cond_0

    .line 256
    iput-object p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->cropVerticalAlign:Lcom/squareup/pollexor/ThumborUrlBuilder$VerticalAlign;

    return-object p0

    .line 254
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Image must be resized first in order to align."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public align(Lcom/squareup/pollexor/ThumborUrlBuilder$VerticalAlign;Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;)Lcom/squareup/pollexor/ThumborUrlBuilder;
    .locals 0

    .line 268
    invoke-virtual {p0, p1}, Lcom/squareup/pollexor/ThumborUrlBuilder;->align(Lcom/squareup/pollexor/ThumborUrlBuilder$VerticalAlign;)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/pollexor/ThumborUrlBuilder;->align(Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object p1

    return-object p1
.end method

.method assembleConfig(Z)Ljava/lang/StringBuilder;
    .locals 7

    .line 428
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p1, :cond_0

    const-string p1, "meta/"

    .line 431
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 434
    :cond_0
    iget-boolean p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->isTrim:Z

    const-string v1, ":"

    const-string v2, "/"

    if-eqz p1, :cond_2

    const-string p1, "trim"

    .line 435
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 436
    iget-object p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->trimPixelColor:Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;

    if-eqz p1, :cond_1

    .line 437
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->trimPixelColor:Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;

    iget-object p1, p1, Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;->value:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 438
    iget p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->trimColorTolerance:I

    if-lez p1, :cond_1

    .line 439
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->trimColorTolerance:I

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 442
    :cond_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    :cond_2
    iget-boolean p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->hasCrop:Z

    const-string/jumbo v3, "x"

    if-eqz p1, :cond_3

    .line 446
    iget p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->cropLeft:I

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->cropTop:I

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 447
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->cropRight:I

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->cropBottom:I

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 449
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 452
    :cond_3
    iget-boolean p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->hasResize:Z

    if-eqz p1, :cond_c

    .line 453
    iget-object p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->fitInStyle:Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    if-eqz p1, :cond_4

    .line 454
    iget-object p1, p1, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;->value:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 456
    :cond_4
    iget-boolean p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->flipHorizontally:Z

    const-string v4, "-"

    if-eqz p1, :cond_5

    .line 457
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 459
    :cond_5
    iget p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->resizeWidth:I

    const-string v5, "orig"

    const/high16 v6, -0x80000000

    if-ne p1, v6, :cond_6

    .line 460
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 462
    :cond_6
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 464
    :goto_0
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 465
    iget-boolean p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->flipVertically:Z

    if-eqz p1, :cond_7

    .line 466
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 468
    :cond_7
    iget p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->resizeHeight:I

    if-ne p1, v6, :cond_8

    .line 469
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 471
    :cond_8
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 473
    :goto_1
    iget-boolean p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->isSmart:Z

    if-eqz p1, :cond_9

    .line 474
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "smart"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 476
    :cond_9
    iget-object p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->cropHorizontalAlign:Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    if-eqz p1, :cond_a

    .line 477
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->cropHorizontalAlign:Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;

    iget-object p1, p1, Lcom/squareup/pollexor/ThumborUrlBuilder$HorizontalAlign;->value:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 479
    :cond_a
    iget-object p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->cropVerticalAlign:Lcom/squareup/pollexor/ThumborUrlBuilder$VerticalAlign;

    if-eqz p1, :cond_b

    .line 480
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->cropVerticalAlign:Lcom/squareup/pollexor/ThumborUrlBuilder$VerticalAlign;

    iget-object p1, p1, Lcom/squareup/pollexor/ThumborUrlBuilder$VerticalAlign;->value:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 483
    :cond_b
    :goto_2
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 486
    :cond_c
    iget-object p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->filters:Ljava/util/List;

    if-eqz p1, :cond_e

    const-string p1, "filters"

    .line 487
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 488
    iget-object p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->filters:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 489
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 491
    :cond_d
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 494
    :cond_e
    iget-boolean p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->isLegacy:Z

    if-eqz p1, :cond_f

    iget-object p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->image:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/pollexor/Utilities;->md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    :cond_f
    iget-object p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->image:Ljava/lang/String;

    :goto_4
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public crop(IIII)Lcom/squareup/pollexor/ThumborUrlBuilder;
    .locals 1

    if-ltz p1, :cond_3

    if-ltz p2, :cond_2

    const/4 v0, 0x1

    if-lt p3, v0, :cond_1

    if-le p3, p1, :cond_1

    if-lt p4, v0, :cond_0

    if-le p4, p2, :cond_0

    .line 224
    iput-boolean v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->hasCrop:Z

    .line 225
    iput p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->cropTop:I

    .line 226
    iput p2, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->cropLeft:I

    .line 227
    iput p3, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->cropBottom:I

    .line 228
    iput p4, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->cropRight:I

    return-object p0

    .line 222
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Right must be greater than zero and left."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 219
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Bottom must be greater than zero and top."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 216
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Left must be greater or equal to zero."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 213
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Top must be greater or equal to zero."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public varargs filter([Ljava/lang/String;)Lcom/squareup/pollexor/ThumborUrlBuilder;
    .locals 4

    .line 352
    array-length v0, p1

    if-eqz v0, :cond_3

    .line 355
    iget-object v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->filters:Ljava/util/List;

    if-nez v0, :cond_0

    .line 356
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->filters:Ljava/util/List;

    .line 358
    :cond_0
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    aget-object v2, p1, v1

    if-eqz v2, :cond_1

    .line 359
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    .line 362
    iget-object v3, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->filters:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 360
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Filter must not be blank."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    return-object p0

    .line 353
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "You must provide at least one filter."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public fitIn()Lcom/squareup/pollexor/ThumborUrlBuilder;
    .locals 1

    .line 184
    sget-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;->NORMAL:Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    invoke-virtual {p0, v0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->fitIn(Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object v0

    return-object v0
.end method

.method public fitIn(Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;)Lcom/squareup/pollexor/ThumborUrlBuilder;
    .locals 1

    .line 193
    iget-boolean v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->hasResize:Z

    if-eqz v0, :cond_0

    .line 196
    iput-object p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->fitInStyle:Lcom/squareup/pollexor/ThumborUrlBuilder$FitInStyle;

    return-object p0

    .line 194
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Image must be resized first in order to apply \'fit-in\'."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public flipHorizontally()Lcom/squareup/pollexor/ThumborUrlBuilder;
    .locals 2

    .line 158
    iget-boolean v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->hasResize:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 161
    iput-boolean v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->flipHorizontally:Z

    return-object p0

    .line 159
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Image must be resized first in order to flip."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public flipVertically()Lcom/squareup/pollexor/ThumborUrlBuilder;
    .locals 2

    .line 171
    iget-boolean v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->hasResize:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 174
    iput-boolean v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->flipVertically:Z

    return-object p0

    .line 172
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Image must be resized first in order to flip."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public legacy()Lcom/squareup/pollexor/ThumborUrlBuilder;
    .locals 1

    const/4 v0, 0x1

    .line 321
    iput-boolean v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->isLegacy:Z

    return-object p0
.end method

.method public resize(II)Lcom/squareup/pollexor/ThumborUrlBuilder;
    .locals 1

    const/high16 v0, -0x80000000

    if-gez p1, :cond_1

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 138
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Width must be a positive number."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    if-gez p2, :cond_3

    if-ne p2, v0, :cond_2

    goto :goto_1

    .line 141
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Height must be a positive number."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    if-nez p1, :cond_5

    if-eqz p2, :cond_4

    goto :goto_2

    .line 144
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Both width and height must not be zero."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    :goto_2
    const/4 v0, 0x1

    .line 146
    iput-boolean v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->hasResize:Z

    .line 147
    iput p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->resizeWidth:I

    .line 148
    iput p2, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->resizeHeight:I

    return-object p0
.end method

.method public smart()Lcom/squareup/pollexor/ThumborUrlBuilder;
    .locals 2

    .line 277
    iget-boolean v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->hasResize:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 280
    iput-boolean v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->isSmart:Z

    return-object p0

    .line 278
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Image must be resized first in order to smart align."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toMeta()Ljava/lang/String;
    .locals 1

    .line 403
    iget-object v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->key:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->toMetaUnsafe()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->toMetaSafe()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public toMetaSafe()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x1

    .line 415
    invoke-virtual {p0, v0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->assembleConfig(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 416
    iget-object v1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->key:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/pollexor/Utilities;->hmacSha1(Ljava/lang/StringBuilder;Ljava/lang/String;)[B

    move-result-object v1

    .line 417
    invoke-static {v1}, Lcom/squareup/pollexor/Utilities;->base64Encode([B)Ljava/lang/String;

    move-result-object v1

    .line 419
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->host:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toMetaUnsafe()Ljava/lang/String;
    .locals 2

    .line 408
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->host:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/squareup/pollexor/ThumborUrlBuilder;->assembleConfig(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 423
    invoke-virtual {p0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->toUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toUrl()Ljava/lang/String;
    .locals 1

    .line 372
    iget-object v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->key:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->toUrlUnsafe()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->toUrlSafe()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public toUrlSafe()Ljava/lang/String;
    .locals 4

    .line 384
    iget-object v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->key:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 388
    iget-boolean v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->isLegacy:Z

    const/4 v1, 0x0

    .line 390
    invoke-virtual {p0, v1}, Lcom/squareup/pollexor/ThumborUrlBuilder;->assembleConfig(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 391
    iget-object v2, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->key:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {v1, v2}, Lcom/squareup/pollexor/Utilities;->aes128Encrypt(Ljava/lang/StringBuilder;Ljava/lang/String;)[B

    move-result-object v2

    goto :goto_0

    :cond_0
    invoke-static {v1, v2}, Lcom/squareup/pollexor/Utilities;->hmacSha1(Ljava/lang/StringBuilder;Ljava/lang/String;)[B

    move-result-object v2

    .line 392
    :goto_0
    invoke-static {v2}, Lcom/squareup/pollexor/Utilities;->base64Encode([B)Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_1

    .line 394
    iget-object v1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->image:Ljava/lang/String;

    :cond_1
    check-cast v1, Ljava/lang/CharSequence;

    .line 395
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->host:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 385
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot build safe URL without a key."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toUrlUnsafe()Ljava/lang/String;
    .locals 2

    .line 377
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->host:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "unsafe/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/squareup/pollexor/ThumborUrlBuilder;->assembleConfig(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public trim()Lcom/squareup/pollexor/ThumborUrlBuilder;
    .locals 1

    const/4 v0, 0x0

    .line 288
    invoke-virtual {p0, v0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->trim(Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object v0

    return-object v0
.end method

.method public trim(Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;)Lcom/squareup/pollexor/ThumborUrlBuilder;
    .locals 1

    const/4 v0, 0x0

    .line 296
    invoke-virtual {p0, p1, v0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->trim(Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;I)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object p1

    return-object p1
.end method

.method public trim(Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;I)Lcom/squareup/pollexor/ThumborUrlBuilder;
    .locals 1

    if-ltz p2, :cond_2

    const/16 v0, 0x1ba

    if-gt p2, v0, :cond_2

    if-lez p2, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 311
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Trim pixel color value must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 313
    iput-boolean v0, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->isTrim:Z

    .line 314
    iput-object p1, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->trimPixelColor:Lcom/squareup/pollexor/ThumborUrlBuilder$TrimPixelColor;

    .line 315
    iput p2, p0, Lcom/squareup/pollexor/ThumborUrlBuilder;->trimColorTolerance:I

    return-object p0

    .line 308
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Color tolerance must be between 0 and 442."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
