.class final Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealQuickAmountsSettingsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->render(Lkotlin/Unit;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
        "+",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
        "it",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;

.field final synthetic this$0:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$3;->this$0:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;

    iput-object p2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$3;->$state:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/quickamounts/QuickAmounts;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmounts;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$3;->$state:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;

    check-cast v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$ItemsTutorialPrompt;

    invoke-virtual {v0}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$ItemsTutorialPrompt;->getAmounts()Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/quickamounts/settings/QuickAmountsFormattingKt;->filterZeroes(Lcom/squareup/quickamounts/QuickAmounts;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 111
    new-instance v0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$3;->this$0:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;

    invoke-static {v1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->access$getCurrency$p(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;)Lcom/squareup/protos/connect/v2/common/Currency;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;-><init>(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/protos/connect/v2/common/Currency;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 113
    :cond_0
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/quickamounts/QuickAmounts;

    invoke-virtual {p0, p1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$3;->invoke(Lcom/squareup/quickamounts/QuickAmounts;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
