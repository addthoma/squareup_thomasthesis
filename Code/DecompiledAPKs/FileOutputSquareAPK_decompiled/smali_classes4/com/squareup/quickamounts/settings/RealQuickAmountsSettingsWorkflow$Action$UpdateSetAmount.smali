.class public final Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;
.super Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action;
.source "RealQuickAmountsSettingsWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UpdateSetAmount"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\tH\u00c6\u0003J1\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u0007H\u00d6\u0001J\t\u0010\u001d\u001a\u00020\tH\u00d6\u0001J\u0018\u0010\u001e\u001a\u00020\u001f*\u000e\u0012\u0004\u0012\u00020!\u0012\u0004\u0012\u00020\"0 H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;",
        "Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action;",
        "currency",
        "Lcom/squareup/protos/connect/v2/common/Currency;",
        "quickAmounts",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "index",
        "",
        "newText",
        "",
        "(Lcom/squareup/protos/connect/v2/common/Currency;Lcom/squareup/quickamounts/QuickAmounts;ILjava/lang/String;)V",
        "getCurrency",
        "()Lcom/squareup/protos/connect/v2/common/Currency;",
        "getIndex",
        "()I",
        "getNewText",
        "()Ljava/lang/String;",
        "getQuickAmounts",
        "()Lcom/squareup/quickamounts/QuickAmounts;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currency:Lcom/squareup/protos/connect/v2/common/Currency;

.field private final index:I

.field private final newText:Ljava/lang/String;

.field private final quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/connect/v2/common/Currency;Lcom/squareup/quickamounts/QuickAmounts;ILjava/lang/String;)V
    .locals 1

    const-string v0, "currency"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "quickAmounts"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newText"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 286
    invoke-direct {p0, v0}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    iput-object p2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    iput p3, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->index:I

    iput-object p4, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->newText:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;Lcom/squareup/protos/connect/v2/common/Currency;Lcom/squareup/quickamounts/QuickAmounts;ILjava/lang/String;ILjava/lang/Object;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget p3, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->index:I

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->newText:Ljava/lang/String;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->copy(Lcom/squareup/protos/connect/v2/common/Currency;Lcom/squareup/quickamounts/QuickAmounts;ILjava/lang/String;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
            "-",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 288
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    invoke-virtual {v0}, Lcom/squareup/quickamounts/QuickAmounts;->getAmounts()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 289
    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->newText:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    invoke-static {v2}, Lcom/squareup/money/v2/MoneysKt;->toCurrencyCode(Lcom/squareup/protos/connect/v2/common/Currency;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/money/MoneyLocaleHelperKt;->extractMoney(Ljava/lang/CharSequence;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV2(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/connect/v2/common/Money;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/squareup/protos/connect/v2/common/Money;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    invoke-direct {v1, v2, v3}, Lcom/squareup/protos/connect/v2/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/connect/v2/common/Currency;)V

    .line 290
    :goto_0
    iget v2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->index:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    xor-int/2addr v2, v3

    if-eqz v2, :cond_1

    .line 291
    iget v2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->index:I

    invoke-interface {v0, v2, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 292
    new-instance v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$Loaded;

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    const/4 v4, 0x0

    invoke-static {v2, v4, v0, v3, v4}, Lcom/squareup/quickamounts/QuickAmounts;->replaceAmounts$default(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$Loaded;-><init>(Lcom/squareup/quickamounts/QuickAmounts;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final component1()Lcom/squareup/protos/connect/v2/common/Currency;
    .locals 1

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object v0
.end method

.method public final component2()Lcom/squareup/quickamounts/QuickAmounts;
    .locals 1

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->index:I

    return v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->newText:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/connect/v2/common/Currency;Lcom/squareup/quickamounts/QuickAmounts;ILjava/lang/String;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;
    .locals 1

    const-string v0, "currency"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "quickAmounts"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newText"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;-><init>(Lcom/squareup/protos/connect/v2/common/Currency;Lcom/squareup/quickamounts/QuickAmounts;ILjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    iget-object v1, p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    iget-object v1, p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->index:I

    iget v1, p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->index:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->newText:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->newText:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCurrency()Lcom/squareup/protos/connect/v2/common/Currency;
    .locals 1

    .line 282
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object v0
.end method

.method public final getIndex()I
    .locals 1

    .line 284
    iget v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->index:I

    return v0
.end method

.method public final getNewText()Ljava/lang/String;
    .locals 1

    .line 285
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->newText:Ljava/lang/String;

    return-object v0
.end method

.method public final getQuickAmounts()Lcom/squareup/quickamounts/QuickAmounts;
    .locals 1

    .line 283
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->index:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->newText:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UpdateSetAmount(currency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", quickAmounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->index:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", newText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateSetAmount;->newText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
