.class final Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealQuickAmountsSettingsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->render(Lkotlin/Unit;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;",
        "it",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$1;->this$0:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/quickamounts/QuickAmounts;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    new-instance v0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$1;->this$0:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;

    invoke-static {v1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->access$getCurrency$p(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;)Lcom/squareup/protos/connect/v2/common/Currency;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;-><init>(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/protos/connect/v2/common/Currency;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/quickamounts/QuickAmounts;

    invoke-virtual {p0, p1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$render$1;->invoke(Lcom/squareup/quickamounts/QuickAmounts;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$LoadedFromQuickAmounts;

    move-result-object p1

    return-object p1
.end method
