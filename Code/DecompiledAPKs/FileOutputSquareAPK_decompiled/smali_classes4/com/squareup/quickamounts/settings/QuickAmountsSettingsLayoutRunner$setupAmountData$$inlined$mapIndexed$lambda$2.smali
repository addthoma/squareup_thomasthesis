.class final Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupAmountData$$inlined$mapIndexed$lambda$2;
.super Lkotlin/jvm/internal/Lambda;
.source "QuickAmountsSettingsLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->setupAmountData(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke",
        "com/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupAmountData$data$1$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $editable$inlined:Z

.field final synthetic $index:I

.field final synthetic $rendering$inlined:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;

.field final synthetic this$0:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;


# direct methods
.method constructor <init>(ILcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;ZLcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;)V
    .locals 0

    iput p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupAmountData$$inlined$mapIndexed$lambda$2;->$index:I

    iput-object p2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupAmountData$$inlined$mapIndexed$lambda$2;->this$0:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;

    iput-boolean p3, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupAmountData$$inlined$mapIndexed$lambda$2;->$editable$inlined:Z

    iput-object p4, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupAmountData$$inlined$mapIndexed$lambda$2;->$rendering$inlined:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 50
    invoke-virtual {p0}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupAmountData$$inlined$mapIndexed$lambda$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 168
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupAmountData$$inlined$mapIndexed$lambda$2;->this$0:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupAmountData$$inlined$mapIndexed$lambda$2;->$rendering$inlined:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;

    iget v2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$setupAmountData$$inlined$mapIndexed$lambda$2;->$index:I

    invoke-static {v0, v1, v2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->access$onAmountCleared(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;I)V

    return-void
.end method
