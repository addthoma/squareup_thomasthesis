.class public abstract Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;
.super Ljava/lang/Object;
.source "RealQuickAmountsSettings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/quickamounts/RealQuickAmountsSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "DataSource"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;,
        Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$CatalogLocal;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u00052\u0010\u0008\u0002\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0008\u0018\u00010\u0007H&\u0082\u0001\u0002\u000b\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;",
        "",
        "()V",
        "onCache",
        "status",
        "Lcom/squareup/quickamounts/QuickAmountsStatus;",
        "newSetAmounts",
        "",
        "Lcom/squareup/protos/connect/v2/common/Money;",
        "Cache",
        "CatalogLocal",
        "Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$Cache;",
        "Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource$CatalogLocal;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 150
    invoke-direct {p0}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;-><init>()V

    return-void
.end method

.method public static synthetic onCache$default(Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;
    .locals 0

    if-nez p4, :cond_1

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 153
    check-cast p2, Ljava/util/List;

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;->onCache(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: onCache"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public abstract onCache(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;)",
            "Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;"
        }
    .end annotation
.end method
