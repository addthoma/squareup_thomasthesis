.class public final Lcom/squareup/quickamounts/SetQuickAmountsTask;
.super Ljava/lang/Object;
.source "SetQuickAmountsTask.kt"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask<",
        "Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSetQuickAmountsTask.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SetQuickAmountsTask.kt\ncom/squareup/quickamounts/SetQuickAmountsTask\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,74:1\n704#2:75\n777#2,2:76\n1370#2:78\n1401#2,4:79\n*E\n*S KotlinDebug\n*F\n+ 1 SetQuickAmountsTask.kt\ncom/squareup/quickamounts/SetQuickAmountsTask\n*L\n46#1:75\n46#1,2:76\n64#1:78\n64#1,4:79\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B1\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0010\u0008\u0002\u0010\u0008\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\t\u0012\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0008\u0010\u0016\u001a\u00020\u0005H\u0007J\u0018\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0002H\u0016J\u0016\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\n\u0010\u001e\u001a\u00020\u001f*\u00020\u0007J\u0016\u0010 \u001a\u0008\u0012\u0004\u0012\u00020!0\t*\u0008\u0012\u0004\u0012\u00020\n0\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0019\u0010\u0008\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/quickamounts/SetQuickAmountsTask;",
        "Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;",
        "Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;",
        "",
        "currentCatalogObject",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;",
        "newStatus",
        "Lcom/squareup/quickamounts/QuickAmountsStatus;",
        "newAmounts",
        "",
        "Lcom/squareup/protos/connect/v2/common/Money;",
        "idempotencyKeyForWriting",
        "",
        "(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;Ljava/lang/String;)V",
        "getCurrentCatalogObject",
        "()Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;",
        "getIdempotencyKeyForWriting",
        "()Ljava/lang/String;",
        "getNewAmounts",
        "()Ljava/util/List;",
        "getNewStatus",
        "()Lcom/squareup/quickamounts/QuickAmountsStatus;",
        "generateNewQuickAmounts",
        "perform",
        "catalogLocal",
        "Lcom/squareup/shared/catalog/Catalog$Local;",
        "successfulOnlineResult",
        "Lcom/squareup/shared/catalog/sync/SyncResult;",
        "catalogOnline",
        "Lcom/squareup/shared/catalog/Catalog$Online;",
        "toProto",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;",
        "toProtos",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentCatalogObject:Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

.field private final idempotencyKeyForWriting:Ljava/lang/String;

.field private final newAmounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final newStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "currentCatalogObject"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newStatus"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "idempotencyKeyForWriting"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/quickamounts/SetQuickAmountsTask;->currentCatalogObject:Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    iput-object p2, p0, Lcom/squareup/quickamounts/SetQuickAmountsTask;->newStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    iput-object p3, p0, Lcom/squareup/quickamounts/SetQuickAmountsTask;->newAmounts:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/quickamounts/SetQuickAmountsTask;->idempotencyKeyForWriting:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_0

    const/4 p3, 0x0

    .line 25
    check-cast p3, Ljava/util/List;

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    .line 26
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p4

    invoke-virtual {p4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p4

    const-string p5, "UUID.randomUUID().toString()"

    invoke-static {p4, p5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/quickamounts/SetQuickAmountsTask;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final generateNewQuickAmounts()Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;
    .locals 7

    .line 44
    iget-object v0, p0, Lcom/squareup/quickamounts/SetQuickAmountsTask;->currentCatalogObject:Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;->newBuilder()Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lcom/squareup/quickamounts/SetQuickAmountsTask;->newStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-virtual {p0, v1}, Lcom/squareup/quickamounts/SetQuickAmountsTask;->toProto(Lcom/squareup/quickamounts/QuickAmountsStatus;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->setCatalogQuickAmountsSettingsOption(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;)Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;

    .line 46
    iget-object v1, p0, Lcom/squareup/quickamounts/SetQuickAmountsTask;->newAmounts:Ljava/util/List;

    if-eqz v1, :cond_3

    .line 48
    iget-object v2, p0, Lcom/squareup/quickamounts/SetQuickAmountsTask;->currentCatalogObject:Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;->getQuickAmounts()Ljava/util/List;

    move-result-object v2

    const-string v3, "currentCatalogObject.quickAmounts"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Iterable;

    .line 75
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 76
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;

    .line 49
    iget-object v5, v5, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    sget-object v6, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->QUICK_AMOUNT_TYPE_MANUAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    if-eq v5, v6, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_0

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 77
    :cond_2
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/util/Collection;

    .line 50
    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v2

    .line 51
    invoke-virtual {p0, v1}, Lcom/squareup/quickamounts/SetQuickAmountsTask;->toProtos(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 52
    invoke-virtual {v0, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->setAmounts(Ljava/util/List;)Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;

    .line 54
    :cond_3
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    move-result-object v0

    const-string v1, "builder.build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getCurrentCatalogObject()Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/quickamounts/SetQuickAmountsTask;->currentCatalogObject:Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    return-object v0
.end method

.method public final getIdempotencyKeyForWriting()Ljava/lang/String;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/quickamounts/SetQuickAmountsTask;->idempotencyKeyForWriting:Ljava/lang/String;

    return-object v0
.end method

.method public final getNewAmounts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/quickamounts/SetQuickAmountsTask;->newAmounts:Ljava/util/List;

    return-object v0
.end method

.method public final getNewStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/quickamounts/SetQuickAmountsTask;->newStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    return-object v0
.end method

.method public perform(Lcom/squareup/shared/catalog/Catalog$Online;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Online;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;",
            ">;"
        }
    .end annotation

    const-string v0, "catalogOnline"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-virtual {p0}, Lcom/squareup/quickamounts/SetQuickAmountsTask;->generateNewQuickAmounts()Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/quickamounts/SetQuickAmountsTask;->idempotencyKeyForWriting:Ljava/lang/String;

    .line 30
    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Online;->batchUpsertCatalogConnectV2Objects(Ljava/util/List;Ljava/lang/String;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    const-string v0, "catalogOnline.batchUpser\u2026otencyKeyForWriting\n    )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p2, Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/quickamounts/SetQuickAmountsTask;->perform(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public perform(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;)V
    .locals 1

    const-string v0, "catalogLocal"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "successfulOnlineResult"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object p2, p2, Lcom/squareup/shared/catalog/BatchUpsertCatalogConnectV2ObjectsResult;->objects:Ljava/util/List;

    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    invoke-interface {p1, p2}, Lcom/squareup/shared/catalog/Catalog$Local;->writeConnectV2Object(Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;)V

    return-void
.end method

.method public final toProto(Lcom/squareup/quickamounts/QuickAmountsStatus;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;
    .locals 1

    const-string v0, "$this$toProto"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    sget-object v0, Lcom/squareup/quickamounts/SetQuickAmountsTask$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/quickamounts/QuickAmountsStatus;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 60
    sget-object p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->MANUAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 59
    :cond_1
    sget-object p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->AUTO:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    goto :goto_0

    .line 58
    :cond_2
    sget-object p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;->DISABLED:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    :goto_0
    return-object p1
.end method

.method public final toProtos(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$toProtos"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    check-cast p1, Ljava/lang/Iterable;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 80
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    if-gez v1, :cond_0

    .line 81
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v2, Lcom/squareup/protos/connect/v2/common/Money;

    .line 65
    new-instance v4, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;-><init>()V

    int-to-long v5, v1

    .line 66
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;->ordinal(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;

    move-result-object v1

    .line 67
    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;->amount(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;

    move-result-object v1

    .line 69
    sget-object v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;->QUICK_AMOUNT_TYPE_MANUAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    .line 68
    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;->type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;

    move-result-object v1

    .line 71
    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v1, v3

    goto :goto_0

    .line 82
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
