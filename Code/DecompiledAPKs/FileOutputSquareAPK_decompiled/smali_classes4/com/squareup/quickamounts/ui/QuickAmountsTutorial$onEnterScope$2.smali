.class final Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$onEnterScope$2;
.super Lkotlin/jvm/internal/Lambda;
.source "QuickAmountsTutorial.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "canShowTutorial",
        "",
        "kotlin.jvm.PlatformType",
        "invoke",
        "(Ljava/lang/Boolean;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;


# direct methods
.method constructor <init>(Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$onEnterScope$2;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$onEnterScope$2;->invoke(Ljava/lang/Boolean;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Boolean;)V
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$onEnterScope$2;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;

    invoke-static {v0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->access$getOutput$p(Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    const-string v1, "canShowTutorial"

    .line 60
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 61
    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$onEnterScope$2;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;

    invoke-static {p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->access$getStartMode$p(Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;)Lcom/squareup/quickamounts/ui/StartMode;

    move-result-object p1

    .line 62
    sget-object v1, Lcom/squareup/quickamounts/ui/StartMode$ManualEligibilityStart;->INSTANCE:Lcom/squareup/quickamounts/ui/StartMode$ManualEligibilityStart;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$onEnterScope$2;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;

    invoke-static {p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->access$manualTutorialFirstPage(Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    goto :goto_0

    .line 63
    :cond_0
    sget-object v1, Lcom/squareup/quickamounts/ui/StartMode$AutomaticEligibilityStart;->INSTANCE:Lcom/squareup/quickamounts/ui/StartMode$AutomaticEligibilityStart;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 64
    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$onEnterScope$2;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;

    invoke-static {p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->access$getAnalytics$p(Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialAnalyticsKt;->onCreatedWalkthrough(Lcom/squareup/analytics/Analytics;)V

    .line 65
    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$onEnterScope$2;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;

    invoke-static {p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->access$autoAmountsTutorial(Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    goto :goto_0

    .line 67
    :cond_1
    sget-object v1, Lcom/squareup/quickamounts/ui/StartMode$AutomaticUpdatedAmountsStart;->INSTANCE:Lcom/squareup/quickamounts/ui/StartMode$AutomaticUpdatedAmountsStart;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$onEnterScope$2;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;

    invoke-static {p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->access$updatedAmountsTutorial(Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    goto :goto_0

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 70
    :cond_3
    sget-object p1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    .line 59
    :goto_0
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
