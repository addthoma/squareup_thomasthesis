.class public final Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator$Seed;
.super Lcom/squareup/tutorialv2/TutorialSeed;
.source "RealQuickAmountsTutorialCreator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "Seed"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator$Seed;",
        "Lcom/squareup/tutorialv2/TutorialSeed;",
        "startMode",
        "Lcom/squareup/quickamounts/ui/StartMode;",
        "(Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;Lcom/squareup/quickamounts/ui/StartMode;)V",
        "doCreate",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final startMode:Lcom/squareup/quickamounts/ui/StartMode;

.field final synthetic this$0:Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;


# direct methods
.method public constructor <init>(Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;Lcom/squareup/quickamounts/ui/StartMode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/ui/StartMode;",
            ")V"
        }
    .end annotation

    const-string v0, "startMode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iput-object p1, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator$Seed;->this$0:Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;

    invoke-static {p1, p2}, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;->access$getPriority$p(Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;Lcom/squareup/quickamounts/ui/StartMode;)Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/tutorialv2/TutorialSeed;-><init>(Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V

    iput-object p2, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator$Seed;->startMode:Lcom/squareup/quickamounts/ui/StartMode;

    return-void
.end method


# virtual methods
.method protected doCreate()Lcom/squareup/tutorialv2/Tutorial;
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator$Seed;->this$0:Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;

    invoke-static {v0}, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;->access$getTutorialFactoryProvider$p(Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;

    iget-object v1, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator$Seed;->startMode:Lcom/squareup/quickamounts/ui/StartMode;

    invoke-virtual {v0, v1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;->forStartMode(Lcom/squareup/quickamounts/ui/StartMode;)Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;

    move-result-object v0

    check-cast v0, Lcom/squareup/tutorialv2/Tutorial;

    return-object v0
.end method
