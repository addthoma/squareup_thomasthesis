.class public final Lcom/squareup/receipt/RealReceiptSender_Factory;
.super Ljava/lang/Object;
.source "RealReceiptSender_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/receipt/RealReceiptSender;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final orderPrintingDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final paperSignatureSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneNumbersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final receiptValidatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptValidator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/receipt/RealReceiptSender_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/receipt/RealReceiptSender_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/receipt/RealReceiptSender_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/receipt/RealReceiptSender_Factory;->phoneNumbersProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/receipt/RealReceiptSender_Factory;->receiptValidatorProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p6, p0, Lcom/squareup/receipt/RealReceiptSender_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/receipt/RealReceiptSender_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;)",
            "Lcom/squareup/receipt/RealReceiptSender_Factory;"
        }
    .end annotation

    .line 59
    new-instance v7, Lcom/squareup/receipt/RealReceiptSender_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/receipt/RealReceiptSender_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/receipt/ReceiptAnalytics;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/receipt/ReceiptValidator;Lcom/squareup/print/PrinterStations;)Lcom/squareup/receipt/RealReceiptSender;
    .locals 8

    .line 66
    new-instance v7, Lcom/squareup/receipt/RealReceiptSender;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/receipt/RealReceiptSender;-><init>(Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/receipt/ReceiptAnalytics;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/receipt/ReceiptValidator;Lcom/squareup/print/PrinterStations;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/receipt/RealReceiptSender;
    .locals 7

    .line 49
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/receipt/ReceiptAnalytics;

    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/papersignature/PaperSignatureSettings;

    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender_Factory;->phoneNumbersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/text/PhoneNumberHelper;

    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender_Factory;->receiptValidatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/receipt/ReceiptValidator;

    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/print/PrinterStations;

    invoke-static/range {v1 .. v6}, Lcom/squareup/receipt/RealReceiptSender_Factory;->newInstance(Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/receipt/ReceiptAnalytics;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/receipt/ReceiptValidator;Lcom/squareup/print/PrinterStations;)Lcom/squareup/receipt/RealReceiptSender;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/receipt/RealReceiptSender_Factory;->get()Lcom/squareup/receipt/RealReceiptSender;

    move-result-object v0

    return-object v0
.end method
