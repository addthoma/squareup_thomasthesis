.class public final Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;
.super Ljava/lang/Object;
.source "StandardResponseCallAdapterFactory.kt"

# interfaces
.implements Lretrofit2/CallAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;->get(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lretrofit2/Retrofit;)Lretrofit2/CallAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit2/CallAdapter<",
        "Ljava/lang/Object;",
        "Lcom/squareup/server/StandardResponse<",
        "*>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000!\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u0014\u0012\u0006\u0012\u0004\u0018\u00010\u0002\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00030\u0001J\u001c\u0010\u0004\u001a\u0006\u0012\u0002\u0008\u00030\u00032\u000e\u0010\u0005\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "com/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1",
        "Lretrofit2/CallAdapter;",
        "",
        "Lcom/squareup/server/StandardResponse;",
        "adapt",
        "call",
        "Lretrofit2/Call;",
        "responseType",
        "Ljava/lang/reflect/Type;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $annotations:[Ljava/lang/annotation/Annotation;

.field final synthetic $constructor:Ljava/lang/reflect/Constructor;

.field final synthetic $responseType:Ljava/lang/reflect/Type;

.field final synthetic $retrofit:Lretrofit2/Retrofit;

.field final synthetic $returnType:Ljava/lang/reflect/Type;

.field final synthetic this$0:Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;


# direct methods
.method constructor <init>(Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;Ljava/lang/reflect/Constructor;Ljava/lang/reflect/Type;Lretrofit2/Retrofit;[Ljava/lang/annotation/Annotation;Ljava/lang/reflect/Type;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Constructor;",
            "Ljava/lang/reflect/Type;",
            "Lretrofit2/Retrofit;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Ljava/lang/reflect/Type;",
            ")V"
        }
    .end annotation

    .line 46
    iput-object p1, p0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;->this$0:Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;

    iput-object p2, p0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;->$constructor:Ljava/lang/reflect/Constructor;

    iput-object p3, p0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;->$responseType:Ljava/lang/reflect/Type;

    iput-object p4, p0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;->$retrofit:Lretrofit2/Retrofit;

    iput-object p5, p0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;->$annotations:[Ljava/lang/annotation/Annotation;

    iput-object p6, p0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;->$returnType:Ljava/lang/reflect/Type;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public adapt(Lretrofit2/Call;)Lcom/squareup/server/StandardResponse;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/Call<",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/squareup/server/StandardResponse<",
            "*>;"
        }
    .end annotation

    move-object/from16 v0, p0

    const-string v1, "call"

    move-object/from16 v10, p1

    invoke-static {v10, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    sget-object v2, Lcom/squareup/server/DefaultStandardResponseFactory;->INSTANCE:Lcom/squareup/server/DefaultStandardResponseFactory;

    .line 50
    iget-object v3, v0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;->$constructor:Ljava/lang/reflect/Constructor;

    .line 51
    iget-object v1, v0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;->this$0:Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;

    invoke-static {v1}, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;->access$getStandardReceiver$p(Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;)Lcom/squareup/receiving/StandardReceiver;

    move-result-object v4

    .line 52
    iget-object v1, v0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;->this$0:Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;

    invoke-static {v1}, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;->access$getMainScheduler$p(Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;)Lio/reactivex/Scheduler;

    move-result-object v5

    .line 53
    sget-object v11, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterExposer;->INSTANCE:Lretrofit2/adapter/rxjava2/RxJava2CallAdapterExposer;

    .line 54
    iget-object v12, v0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;->$responseType:Ljava/lang/reflect/Type;

    .line 55
    iget-object v1, v0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;->this$0:Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;

    invoke-static {v1}, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;->access$getRpcScheduler$p(Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;)Lio/reactivex/Scheduler;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x0

    const/16 v18, 0x1

    const/16 v19, 0x0

    const/16 v20, 0x0

    .line 53
    invoke-virtual/range {v11 .. v20}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterExposer;->createCallAdapter(Ljava/lang/reflect/Type;Lio/reactivex/Scheduler;ZZZZZZZ)Lretrofit2/CallAdapter;

    move-result-object v6

    .line 64
    iget-object v7, v0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;->$responseType:Ljava/lang/reflect/Type;

    .line 65
    iget-object v8, v0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;->$retrofit:Lretrofit2/Retrofit;

    .line 66
    iget-object v9, v0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;->$annotations:[Ljava/lang/annotation/Annotation;

    .line 49
    invoke-virtual/range {v2 .. v10}, Lcom/squareup/server/DefaultStandardResponseFactory;->create(Ljava/lang/reflect/Constructor;Lcom/squareup/receiving/StandardReceiver;Lio/reactivex/Scheduler;Lretrofit2/CallAdapter;Ljava/lang/reflect/Type;Lretrofit2/Retrofit;[Ljava/lang/annotation/Annotation;Lretrofit2/Call;)Lcom/squareup/server/StandardResponse;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic adapt(Lretrofit2/Call;)Ljava/lang/Object;
    .locals 0

    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;->adapt(Lretrofit2/Call;)Lcom/squareup/server/StandardResponse;

    move-result-object p1

    return-object p1
.end method

.method public responseType()Ljava/lang/reflect/Type;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;->$returnType:Ljava/lang/reflect/Type;

    return-object v0
.end method
