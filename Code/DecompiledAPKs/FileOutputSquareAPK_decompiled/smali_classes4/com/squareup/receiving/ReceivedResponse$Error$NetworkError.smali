.class public final Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;
.super Lcom/squareup/receiving/ReceivedResponse$Error;
.source "ReceivedResponse.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/receiving/ReceivedResponse$Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NetworkError"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J+\u0010\u0004\u001a\u0002H\u0005\"\u0004\u0008\u0002\u0010\u00052\u0016\u0010\u0006\u001a\u0012\u0012\u0006\u0008\u0000\u0012\u00020\u0002\u0012\u0006\u0008\u0001\u0012\u0002H\u00050\u0007H\u0016\u00a2\u0006\u0002\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;",
        "Lcom/squareup/receiving/ReceivedResponse$Error;",
        "",
        "()V",
        "map",
        "M",
        "javaMapper",
        "Lcom/squareup/receiving/ReceivedMapper;",
        "(Lcom/squareup/receiving/ReceivedMapper;)Ljava/lang/Object;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 58
    new-instance v0, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    invoke-direct {v0}, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;-><init>()V

    sput-object v0, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;->INSTANCE:Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 58
    invoke-direct {p0, v0, v1}, Lcom/squareup/receiving/ReceivedResponse$Error;-><init>(ZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public map(Lcom/squareup/receiving/ReceivedMapper;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/ReceivedMapper;",
            ")TM;"
        }
    .end annotation

    const-string v0, "javaMapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-interface {p1}, Lcom/squareup/receiving/ReceivedMapper;->isNetworkError()Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
