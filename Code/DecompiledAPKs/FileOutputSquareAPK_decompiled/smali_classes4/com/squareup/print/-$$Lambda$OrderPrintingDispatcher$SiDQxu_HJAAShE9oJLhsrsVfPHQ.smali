.class public final synthetic Lcom/squareup/print/-$$Lambda$OrderPrintingDispatcher$SiDQxu_HJAAShE9oJLhsrsVfPHQ;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/util/SquareCollections$Classifier;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/print/-$$Lambda$OrderPrintingDispatcher$SiDQxu_HJAAShE9oJLhsrsVfPHQ;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/print/-$$Lambda$OrderPrintingDispatcher$SiDQxu_HJAAShE9oJLhsrsVfPHQ;

    invoke-direct {v0}, Lcom/squareup/print/-$$Lambda$OrderPrintingDispatcher$SiDQxu_HJAAShE9oJLhsrsVfPHQ;-><init>()V

    sput-object v0, Lcom/squareup/print/-$$Lambda$OrderPrintingDispatcher$SiDQxu_HJAAShE9oJLhsrsVfPHQ;->INSTANCE:Lcom/squareup/print/-$$Lambda$OrderPrintingDispatcher$SiDQxu_HJAAShE9oJLhsrsVfPHQ;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final classContains(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    check-cast p1, Lcom/squareup/print/PrinterStation;

    check-cast p2, Lcom/squareup/print/PrintableOrderItem;

    invoke-static {p1, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->lambda$static$0(Lcom/squareup/print/PrinterStation;Lcom/squareup/print/PrintableOrderItem;)Z

    move-result p1

    return p1
.end method
