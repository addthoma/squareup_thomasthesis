.class public Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;
.super Lcom/squareup/print/payload/ReceiptPayloadFactory;
.source "TimecardsSummaryPayloadFactory.java"


# instance fields
.field private final clock:Lcom/squareup/util/Clock;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/text/Formatter;Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;Lcom/squareup/util/Clock;)V
    .locals 14
    .param p8    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/ForTaxPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;",
            "Lcom/squareup/util/Clock;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object v9, p1

    move-object/from16 v10, p9

    move-object/from16 v11, p4

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    .line 42
    invoke-direct/range {v0 .. v13}, Lcom/squareup/print/payload/ReceiptPayloadFactory;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/print/papersig/TipSectionFactory;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Ljavax/inject/Provider;Lcom/squareup/text/Formatter;Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;)V

    move-object/from16 v1, p12

    .line 45
    iput-object v1, v0, Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;->clock:Lcom/squareup/util/Clock;

    return-void
.end method


# virtual methods
.method public fromEmployeeAndShiftSummary(Lcom/squareup/permissions/Employee;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;)Lcom/squareup/print/payload/TimecardsSummaryPayload;
    .locals 10

    .line 50
    iget-object v0, p2, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->job_summaries:Ljava/util/List;

    .line 51
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-le v1, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 53
    :goto_0
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 54
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    if-eqz v3, :cond_1

    add-int/lit8 v1, v2, 0x1

    .line 57
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_2

    :cond_1
    const/4 v1, 0x0

    .line 58
    :goto_2
    iget-object v4, p0, Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;->res:Lcom/squareup/util/Res;

    .line 59
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    invoke-static {v4, v5, v1}, Lcom/squareup/print/sections/ShiftSection;->fromSummary(Lcom/squareup/util/Res;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;Ljava/lang/Integer;)Lcom/squareup/print/sections/ShiftSection;

    move-result-object v1

    .line 58
    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 62
    :cond_2
    new-instance v0, Lcom/squareup/print/payload/TimecardsSummaryPayload;

    iget-object v1, p0, Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/print/R$string;->timecards_shift_report_header:I

    .line 63
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v1, p0, Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;->res:Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 64
    invoke-static {v1, p1, p2, v2}, Lcom/squareup/print/sections/EmployeeSummarySection;->fromEmployeeAndSummary(Lcom/squareup/util/Res;Lcom/squareup/permissions/Employee;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/print/sections/EmployeeSummarySection;

    move-result-object v6

    iget-object p1, p0, Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/print/R$string;->timecards_shifts_header:I

    .line 65
    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object p1, p0, Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;->res:Lcom/squareup/util/Res;

    iget-object p2, p0, Lcom/squareup/print/payload/TimecardsSummaryPayloadFactory;->clock:Lcom/squareup/util/Clock;

    .line 67
    invoke-static {p1, p2}, Lcom/squareup/print/sections/PrintedAtSection;->fromClock(Lcom/squareup/util/Res;Lcom/squareup/util/Clock;)Lcom/squareup/print/sections/PrintedAtSection;

    move-result-object v9

    move-object v4, v0

    invoke-direct/range {v4 .. v9}, Lcom/squareup/print/payload/TimecardsSummaryPayload;-><init>(Ljava/lang/String;Lcom/squareup/print/sections/EmployeeSummarySection;Ljava/lang/String;Ljava/util/List;Lcom/squareup/print/sections/PrintedAtSection;)V

    return-object v0
.end method
