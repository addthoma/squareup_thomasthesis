.class public final enum Lcom/squareup/print/payload/ReceiptPayload$RenderType;
.super Ljava/lang/Enum;
.source "ReceiptPayload.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/payload/ReceiptPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RenderType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/print/payload/ReceiptPayload$RenderType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/print/payload/ReceiptPayload$RenderType;",
        "",
        "renderer",
        "Lcom/squareup/print/ReceiptPayloadRenderer;",
        "(Ljava/lang/String;ILcom/squareup/print/ReceiptPayloadRenderer;)V",
        "getRenderer",
        "()Lcom/squareup/print/ReceiptPayloadRenderer;",
        "setRenderer",
        "(Lcom/squareup/print/ReceiptPayloadRenderer;)V",
        "RECEIPT",
        "FORMAL_RECEIPT",
        "GIFT_RECEIPT",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/print/payload/ReceiptPayload$RenderType;

.field public static final enum FORMAL_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

.field public static final enum GIFT_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

.field public static final enum RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;


# instance fields
.field private renderer:Lcom/squareup/print/ReceiptPayloadRenderer;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    new-instance v1, Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    .line 60
    new-instance v2, Lcom/squareup/print/DefaultReceiptPayloadRenderer;

    invoke-direct {v2}, Lcom/squareup/print/DefaultReceiptPayloadRenderer;-><init>()V

    check-cast v2, Lcom/squareup/print/ReceiptPayloadRenderer;

    const/4 v3, 0x0

    const-string v4, "RECEIPT"

    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/print/payload/ReceiptPayload$RenderType;-><init>(Ljava/lang/String;ILcom/squareup/print/ReceiptPayloadRenderer;)V

    sput-object v1, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    .line 61
    new-instance v2, Lcom/squareup/print/FormalReceiptPayloadRenderer;

    invoke-direct {v2}, Lcom/squareup/print/FormalReceiptPayloadRenderer;-><init>()V

    check-cast v2, Lcom/squareup/print/ReceiptPayloadRenderer;

    const/4 v3, 0x1

    const-string v4, "FORMAL_RECEIPT"

    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/print/payload/ReceiptPayload$RenderType;-><init>(Ljava/lang/String;ILcom/squareup/print/ReceiptPayloadRenderer;)V

    sput-object v1, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->FORMAL_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    .line 62
    new-instance v2, Lcom/squareup/print/GiftReceiptPayloadRenderer;

    invoke-direct {v2}, Lcom/squareup/print/GiftReceiptPayloadRenderer;-><init>()V

    check-cast v2, Lcom/squareup/print/ReceiptPayloadRenderer;

    const/4 v3, 0x2

    const-string v4, "GIFT_RECEIPT"

    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/print/payload/ReceiptPayload$RenderType;-><init>(Ljava/lang/String;ILcom/squareup/print/ReceiptPayloadRenderer;)V

    sput-object v1, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->GIFT_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->$VALUES:[Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/print/ReceiptPayloadRenderer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/ReceiptPayloadRenderer;",
            ")V"
        }
    .end annotation

    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->renderer:Lcom/squareup/print/ReceiptPayloadRenderer;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/print/payload/ReceiptPayload$RenderType;
    .locals 1

    const-class v0, Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/print/payload/ReceiptPayload$RenderType;
    .locals 1

    sget-object v0, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->$VALUES:[Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-virtual {v0}, [Lcom/squareup/print/payload/ReceiptPayload$RenderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    return-object v0
.end method


# virtual methods
.method public final getRenderer()Lcom/squareup/print/ReceiptPayloadRenderer;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->renderer:Lcom/squareup/print/ReceiptPayloadRenderer;

    return-object v0
.end method

.method public final setRenderer(Lcom/squareup/print/ReceiptPayloadRenderer;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->renderer:Lcom/squareup/print/ReceiptPayloadRenderer;

    return-void
.end method
