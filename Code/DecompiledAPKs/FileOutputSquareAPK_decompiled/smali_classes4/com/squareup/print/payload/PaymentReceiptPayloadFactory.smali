.class public Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;
.super Lcom/squareup/print/payload/ReceiptPayloadFactory;
.source "PaymentReceiptPayloadFactory.java"


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/print/papersig/TipSectionFactory;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/text/PhoneNumberHelper;Ljavax/inject/Provider;Lcom/squareup/text/Formatter;Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;)V
    .locals 14
    .param p8    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/ForTaxPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/print/papersig/TipSectionFactory;",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 106
    invoke-virtual/range {p9 .. p9}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v9

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    .line 105
    invoke-direct/range {v0 .. v13}, Lcom/squareup/print/payload/ReceiptPayloadFactory;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/print/papersig/TipSectionFactory;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Ljavax/inject/Provider;Lcom/squareup/text/Formatter;Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;)V

    return-void
.end method

.method private createCodesSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/print/sections/CodesSection;
    .locals 7

    .line 331
    invoke-virtual {p0, p1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->buildReceiptFormatter(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/print/ReceiptFormatter;

    move-result-object v1

    if-eqz p3, :cond_0

    .line 334
    invoke-virtual {p3}, Lcom/squareup/payment/tender/BaseTender;->getReceiptNumber()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 336
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/payment/BillPayment;->getReceiptNumber()Ljava/lang/String;

    move-result-object v0

    .line 338
    :goto_0
    invoke-virtual {v1, v0}, Lcom/squareup/print/ReceiptFormatter;->receiptNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 340
    instance-of v0, p3, Lcom/squareup/payment/RequiresCardAgreement;

    if-eqz v0, :cond_1

    .line 341
    check-cast p3, Lcom/squareup/payment/RequiresCardAgreement;

    goto :goto_1

    :cond_1
    const/4 p3, 0x0

    .line 344
    :goto_1
    invoke-direct {p0, p1, p3}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->formatAuthorizationCode(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/RequiresCardAgreement;)Ljava/lang/String;

    move-result-object v4

    .line 346
    iget-object v0, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p2}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/squareup/print/sections/CodesSection;->fromOrder(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/print/sections/CodesSection;

    move-result-object p1

    return-object p1
.end method

.method private createEmvSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/print/sections/EmvSection;
    .locals 2

    .line 354
    invoke-virtual {p0, p1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->buildReceiptFormatter(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/print/ReceiptFormatter;

    move-result-object p1

    .line 356
    instance-of v0, p2, Lcom/squareup/payment/tender/SmartCardTender;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 357
    check-cast p2, Lcom/squareup/payment/tender/SmartCardTender;

    .line 358
    invoke-virtual {p2}, Lcom/squareup/payment/tender/SmartCardTender;->isFallback()Z

    move-result v0

    if-nez v0, :cond_0

    .line 359
    invoke-virtual {p2}, Lcom/squareup/payment/tender/SmartCardTender;->getApplicationPreferredName()Ljava/lang/String;

    move-result-object v1

    .line 360
    invoke-virtual {p2}, Lcom/squareup/payment/tender/SmartCardTender;->getApplicationId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/print/ReceiptFormatter;->applicationIdOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 363
    invoke-virtual {p2}, Lcom/squareup/payment/tender/SmartCardTender;->getCardholderVerificationMethodUsed()Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    move-result-object p2

    .line 362
    invoke-virtual {p1, p2}, Lcom/squareup/print/ReceiptFormatter;->cardholderVerificationMethodUsedOrNull(Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v1

    move-object v0, p1

    .line 367
    :goto_0
    new-instance p2, Lcom/squareup/print/sections/EmvSection;

    invoke-direct {p2, v1, v0, p1}, Lcom/squareup/print/sections/EmvSection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object p2
.end method

.method private createPayload(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;ZZZLcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;)Lcom/squareup/print/payload/ReceiptPayload;
    .locals 34

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    const-string v4, "localeOverride"

    .line 151
    invoke-static {v1, v4}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v4, "payment"

    .line 152
    invoke-static {v2, v4}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v4, "tender"

    .line 153
    invoke-static {v3, v4}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 155
    invoke-virtual/range {p0 .. p1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->buildReceiptFormatter(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/print/ReceiptFormatter;

    move-result-object v4

    if-eqz v1, :cond_0

    .line 156
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v5

    goto :goto_0

    :cond_0
    iget-object v5, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->res:Lcom/squareup/util/Res;

    :goto_0
    move-object v13, v5

    .line 157
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v14

    .line 158
    invoke-static {v14}, Lcom/squareup/util/TaxBreakdown;->fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/util/TaxBreakdown;

    move-result-object v15

    .line 159
    invoke-direct {v0, v3}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->getTimestamp(Lcom/squareup/payment/tender/BaseTender;)Ljava/util/Date;

    move-result-object v7

    .line 162
    iget-object v5, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-virtual/range {p3 .. p3}, Lcom/squareup/payment/tender/BaseTender;->getEmployeeToken()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/squareup/permissions/EmployeeManagement;->getEmployeeByToken(Ljava/lang/String;)Lcom/squareup/permissions/Employee;

    move-result-object v8

    .line 164
    iget-object v5, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v10, 0x0

    const/4 v11, 0x0

    iget-object v12, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    move-object v6, v4

    move-object/from16 v9, p7

    .line 165
    invoke-static/range {v5 .. v12}, Lcom/squareup/print/sections/HeaderSection;->createSection(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Ljava/util/Date;Lcom/squareup/permissions/Employee;Lcom/squareup/print/payload/ReceiptPayload$RenderType;ZZLcom/squareup/settings/server/Features;)Lcom/squareup/print/sections/HeaderSection;

    move-result-object v17

    .line 168
    invoke-direct/range {p0 .. p3}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->createCodesSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/print/sections/CodesSection;

    move-result-object v18

    .line 169
    invoke-direct {v0, v1, v3}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->createEmvSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/print/sections/EmvSection;

    move-result-object v19

    const/16 v16, 0x0

    if-nez p4, :cond_1

    .line 175
    iget-object v5, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v9, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    iget-object v6, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->localeProvider:Ljavax/inject/Provider;

    .line 177
    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    move-object v10, v6

    check-cast v10, Ljava/util/Locale;

    iget-object v6, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v6}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v11

    move-object v6, v4

    move-object v7, v14

    move-object v8, v15

    move-object v12, v13

    .line 176
    invoke-static/range {v5 .. v12}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->fromOrder(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/settings/server/Features;Ljava/util/Locale;ZLcom/squareup/util/Res;)Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    move-result-object v5

    move-object/from16 v20, v5

    goto :goto_1

    :cond_1
    move-object/from16 v20, v16

    .line 181
    :goto_1
    invoke-direct/range {p0 .. p3}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->createSubtotalAndAdjustmentsSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    move-result-object v21

    .line 183
    invoke-direct {v0, v1, v2, v3, v15}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->createTotalsSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/util/TaxBreakdown;)Lcom/squareup/print/sections/TotalSection;

    move-result-object v22

    .line 184
    invoke-direct/range {p0 .. p3}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->createTenderSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/print/sections/TenderSection;

    move-result-object v23

    .line 189
    invoke-virtual {v14}, Lcom/squareup/payment/Order;->getReturnCart()Lcom/squareup/checkout/ReturnCart;

    move-result-object v2

    if-nez p4, :cond_2

    .line 191
    invoke-virtual {v14}, Lcom/squareup/payment/Order;->hasReturn()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 192
    invoke-static {v2}, Lcom/squareup/util/TaxBreakdown;->fromReturnCart(Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/util/TaxBreakdown;

    move-result-object v8

    .line 193
    iget-object v5, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v9, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    iget-object v6, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 200
    invoke-virtual {v6}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v10

    move-object v6, v4

    move-object v7, v2

    move-object v11, v13

    .line 194
    invoke-static/range {v5 .. v11}, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->fromReturnCart(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/settings/server/Features;ZLcom/squareup/util/Res;)Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    move-result-object v5

    move-object/from16 v25, v5

    goto :goto_2

    :cond_2
    move-object/from16 v25, v16

    .line 204
    :goto_2
    invoke-virtual {v14}, Lcom/squareup/payment/Order;->hasReturn()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 206
    invoke-virtual/range {p0 .. p1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->buildReceiptFormatter(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/print/ReceiptFormatter;

    move-result-object v5

    .line 205
    invoke-static {v5, v2}, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->fromReturnCart(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    move-result-object v2

    move-object/from16 v26, v2

    goto :goto_3

    :cond_3
    move-object/from16 v26, v16

    :goto_3
    if-eqz p4, :cond_4

    .line 214
    iget-object v2, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->tipSectionFactory:Lcom/squareup/print/papersig/TipSectionFactory;

    invoke-virtual {v2, v3, v4}, Lcom/squareup/print/papersig/TipSectionFactory;->createTipSectionForTender(Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/ReceiptFormatter;)Lcom/squareup/print/papersig/TipSections;

    move-result-object v2

    .line 215
    iget-object v5, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    new-instance v6, Lcom/squareup/ui/buyer/signature/AgreementBuilder;

    invoke-direct {v6, v13}, Lcom/squareup/ui/buyer/signature/AgreementBuilder;-><init>(Lcom/squareup/util/Res;)V

    .line 216
    invoke-static {v5, v3, v6}, Lcom/squareup/print/papersig/SignatureSection;->fromPayment(Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/ui/buyer/signature/AgreementBuilder;)Lcom/squareup/print/papersig/SignatureSection;

    move-result-object v5

    move-object/from16 v27, v2

    move-object/from16 v28, v5

    goto :goto_4

    :cond_4
    move-object/from16 v27, v16

    move-object/from16 v28, v27

    .line 221
    :goto_4
    iget-object v2, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {v2}, Lcom/squareup/print/sections/SectionUtils;->canShowTaxBreakDownTable(Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 224
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    .line 223
    invoke-static {v4, v15, v1}, Lcom/squareup/print/sections/MultipleTaxBreakdownSection;->fromTaxBreakdown(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/util/Res;)Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    move-result-object v1

    move-object/from16 v29, v1

    goto :goto_5

    :cond_5
    move-object/from16 v29, v16

    .line 227
    :goto_5
    iget-object v5, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v8, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    move-object v6, v4

    move-object v7, v14

    move/from16 v9, p5

    move/from16 v10, p6

    .line 228
    invoke-static/range {v5 .. v10}, Lcom/squareup/print/sections/FooterSection;->fromOrder(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/settings/server/Features;ZZ)Lcom/squareup/print/sections/FooterSection;

    move-result-object v30

    .line 232
    iget-object v1, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->receiptInfoProvider:Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;

    invoke-interface {v1}, Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;->shouldPrintBarcodes()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual/range {p3 .. p3}, Lcom/squareup/payment/tender/BaseTender;->getReceiptNumber()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 233
    iget-object v1, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->receiptInfoProvider:Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;

    .line 234
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/payment/tender/BaseTender;->getReceiptNumber()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;->formatBarcodeForReceiptNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 235
    new-instance v2, Lcom/squareup/print/sections/BarcodeSection;

    invoke-direct {v2, v1}, Lcom/squareup/print/sections/BarcodeSection;-><init>(Ljava/lang/String;)V

    move-object/from16 v31, v2

    goto :goto_6

    :cond_6
    move-object/from16 v31, v16

    .line 238
    :goto_6
    new-instance v1, Lcom/squareup/print/payload/ReceiptPayload;

    move-object/from16 v16, v1

    const/16 v24, 0x0

    move-object/from16 v32, p7

    move-object/from16 v33, p8

    invoke-direct/range {v16 .. v33}, Lcom/squareup/print/payload/ReceiptPayload;-><init>(Lcom/squareup/print/sections/HeaderSection;Lcom/squareup/print/sections/CodesSection;Lcom/squareup/print/sections/EmvSection;Lcom/squareup/print/sections/ItemsAndDiscountsSection;Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;Lcom/squareup/print/sections/TotalSection;Lcom/squareup/print/sections/TenderSection;Lcom/squareup/print/sections/RefundsSection;Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;Lcom/squareup/print/papersig/TipSections;Lcom/squareup/print/papersig/SignatureSection;Lcom/squareup/print/sections/MultipleTaxBreakdownSection;Lcom/squareup/print/sections/FooterSection;Lcom/squareup/print/sections/BarcodeSection;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;)V

    return-object v1
.end method

.method private createSubtotalAndAdjustmentsSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;
    .locals 9

    const-string v0, "tender"

    .line 264
    invoke-static {p3, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 266
    invoke-virtual {p2}, Lcom/squareup/payment/BillPayment;->isSingleTender()Z

    move-result v0

    .line 267
    invoke-virtual {p2}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/OrderSnapshot;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v1

    .line 268
    invoke-direct {p0, p2, p3, v1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->getSwedishRounding(Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v5

    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {p2}, Lcom/squareup/payment/BillPayment;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object p3

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    move-object v4, p3

    .line 274
    invoke-virtual {p0, p1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->buildReceiptFormatter(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/print/ReceiptFormatter;

    move-result-object v2

    .line 275
    invoke-virtual {p2}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v3

    iget-object p1, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object p2, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_AUTO_GRATUITY:Lcom/squareup/settings/server/Features$Feature;

    .line 278
    invoke-interface {p1, p2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 273
    invoke-static/range {v2 .. v8}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->fromOrder(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZZ)Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    move-result-object p1

    return-object p1
.end method

.method private createTenderSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/print/sections/TenderSection;
    .locals 22

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    if-eqz v1, :cond_0

    .line 373
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v3

    goto :goto_0

    :cond_0
    iget-object v3, v0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->res:Lcom/squareup/util/Res;

    :goto_0
    const/4 v4, 0x0

    .line 377
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/BillPayment;->isSingleTender()Z

    move-result v5

    .line 381
    instance-of v6, v2, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;

    const/4 v7, 0x0

    if-eqz v6, :cond_1

    .line 382
    move-object v6, v2

    check-cast v6, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;

    move-object/from16 v19, v7

    goto :goto_1

    .line 383
    :cond_1
    instance-of v6, v2, Lcom/squareup/payment/tender/SmartCardTender;

    if-eqz v6, :cond_2

    .line 384
    move-object v6, v2

    check-cast v6, Lcom/squareup/payment/tender/SmartCardTender;

    .line 385
    invoke-virtual {v6}, Lcom/squareup/payment/tender/SmartCardTender;->isFallback()Z

    move-result v8

    if-nez v8, :cond_2

    .line 386
    invoke-virtual {v6}, Lcom/squareup/payment/tender/SmartCardTender;->getBuyerSelectedAccountName()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v19, v6

    move-object v6, v7

    goto :goto_1

    :cond_2
    move-object v6, v7

    move-object/from16 v19, v6

    :goto_1
    if-eqz v5, :cond_3

    .line 390
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/BillPayment;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v8

    goto :goto_2

    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v8

    invoke-virtual {v8}, Lcom/squareup/payment/OrderSnapshot;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v8

    :goto_2
    if-nez v5, :cond_4

    .line 394
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/payment/tender/BaseTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v9

    move-object/from16 v17, v9

    goto :goto_3

    :cond_4
    move-object/from16 v17, v7

    :goto_3
    if-eqz v2, :cond_9

    .line 404
    invoke-virtual {v2, v3}, Lcom/squareup/payment/tender/BaseTender;->getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v9

    .line 405
    invoke-virtual {v2, v3}, Lcom/squareup/payment/tender/BaseTender;->getReceiptTenderShortName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v10

    .line 407
    instance-of v11, v2, Lcom/squareup/payment/tender/EmoneyTender;

    if-eqz v11, :cond_7

    .line 411
    move-object v10, v2

    check-cast v10, Lcom/squareup/payment/tender/EmoneyTender;

    .line 412
    invoke-virtual {v10}, Lcom/squareup/payment/tender/EmoneyTender;->getBrand()Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    move-result-object v11

    sget-object v12, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->FELICA_TRANSPORTATION:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    if-ne v11, v12, :cond_5

    .line 413
    invoke-virtual {v10}, Lcom/squareup/payment/tender/EmoneyTender;->getRemainingBalanceMinusTip()Lcom/squareup/protos/common/Money;

    move-result-object v11

    .line 414
    new-instance v12, Lcom/squareup/print/payload/LabelAmountPair;

    sget v13, Lcom/squareup/print/R$string;->receipt_felica_suica_terminal_id:I

    .line 415
    invoke-interface {v3, v13}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 416
    invoke-virtual {v10}, Lcom/squareup/payment/tender/EmoneyTender;->getTerminalId()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v12, v3, v10}, Lcom/squareup/print/payload/LabelAmountPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v7

    goto :goto_5

    .line 418
    :cond_5
    invoke-virtual {v10}, Lcom/squareup/payment/tender/EmoneyTender;->getBrand()Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    move-result-object v10

    sget-object v11, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->FELICA_ID:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    if-ne v10, v11, :cond_6

    .line 419
    sget v10, Lcom/squareup/print/R$string;->receipt_tender_felica_id_purchase_location:I

    .line 420
    invoke-interface {v3, v10}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v11, v7

    goto :goto_4

    :cond_6
    move-object v3, v7

    move-object v11, v3

    :goto_4
    move-object v12, v11

    :goto_5
    move-object/from16 v20, v3

    move-object v14, v9

    move-object v15, v14

    move-object v10, v11

    move-object/from16 v21, v12

    move-object/from16 v9, p2

    goto :goto_7

    .line 422
    :cond_7
    instance-of v3, v2, Lcom/squareup/payment/tender/BaseCardTender;

    if-eqz v3, :cond_8

    move-object v3, v2

    check-cast v3, Lcom/squareup/payment/tender/BaseCardTender;

    .line 423
    invoke-virtual {v3}, Lcom/squareup/payment/tender/BaseCardTender;->getCard()Lcom/squareup/Card;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v3

    sget-object v11, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    if-ne v3, v11, :cond_8

    .line 424
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/payment/tender/BaseTender;->getRemainingBalanceMinusTip()Lcom/squareup/protos/common/Money;

    move-result-object v3

    move-object/from16 v20, v7

    move-object/from16 v21, v20

    move-object v14, v9

    move-object v15, v10

    move-object/from16 v9, p2

    move-object v10, v3

    goto :goto_7

    :cond_8
    move-object/from16 v20, v7

    move-object/from16 v21, v20

    move-object v14, v9

    move-object v15, v10

    move-object/from16 v9, p2

    goto :goto_6

    :cond_9
    move-object/from16 v9, p2

    .line 427
    invoke-virtual {v9, v3}, Lcom/squareup/payment/BillPayment;->getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v10

    .line 429
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v11

    invoke-virtual {v11, v3}, Lcom/squareup/payment/tender/BaseTender;->getReceiptTenderShortName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v3

    move-object v15, v3

    move-object/from16 v20, v7

    move-object/from16 v21, v20

    move-object v14, v10

    :goto_6
    move-object/from16 v10, v21

    :goto_7
    if-eqz v6, :cond_a

    .line 435
    invoke-interface {v6}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 437
    invoke-interface {v6}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getChange()Lcom/squareup/protos/common/Money;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v16, v3

    move-object v13, v4

    const/4 v12, 0x1

    goto :goto_9

    :cond_a
    if-nez v5, :cond_b

    .line 441
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/payment/tender/BaseTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v3

    move-object/from16 v16, v3

    move-object v13, v7

    goto :goto_8

    :cond_b
    move-object v13, v7

    move-object/from16 v16, v8

    :goto_8
    const/4 v12, 0x0

    :goto_9
    if-nez v2, :cond_c

    goto :goto_a

    .line 447
    :cond_c
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/payment/tender/BaseTender;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v7

    :goto_a
    move-object v11, v7

    .line 449
    invoke-virtual/range {p0 .. p1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->buildReceiptFormatter(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/print/ReceiptFormatter;

    move-result-object v8

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v9

    .line 451
    invoke-direct {v0, v2, v1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->getCardholderName(Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/locale/LocaleOverrideFactory;)Ljava/lang/String;

    move-result-object v18

    .line 449
    invoke-static/range {v8 .. v21}, Lcom/squareup/print/sections/TenderSection;->fromOrder(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/sections/TenderSection;

    move-result-object v1

    return-object v1
.end method

.method private createTotalsSection(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/util/TaxBreakdown;)Lcom/squareup/print/sections/TotalSection;
    .locals 2

    const-string v0, "tender"

    .line 286
    invoke-static {p3, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 288
    invoke-virtual {p2}, Lcom/squareup/payment/BillPayment;->isSingleTender()Z

    move-result v0

    .line 289
    invoke-virtual {p2}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/OrderSnapshot;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v1

    .line 290
    invoke-direct {p0, p2, p3, v1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->getSwedishRounding(Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p3

    if-eqz v0, :cond_0

    .line 296
    invoke-virtual {p2}, Lcom/squareup/payment/BillPayment;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object p3

    goto :goto_0

    .line 299
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/OrderSnapshot;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p3

    .line 302
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->buildReceiptFormatter(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/print/ReceiptFormatter;

    move-result-object p1

    .line 303
    invoke-virtual {p2}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 302
    invoke-static {p1, p2, p4, p3, v0}, Lcom/squareup/print/sections/TotalSection;->fromOrder(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/protos/common/Money;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/print/sections/TotalSection;

    move-result-object p1

    return-object p1
.end method

.method private formatAuthorizationCode(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/RequiresCardAgreement;)Ljava/lang/String;
    .locals 0

    if-nez p2, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 461
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->buildReceiptFormatter(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/print/ReceiptFormatter;

    move-result-object p1

    .line 462
    invoke-interface {p2}, Lcom/squareup/payment/RequiresCardAgreement;->getAuthorizationCode()Ljava/lang/String;

    move-result-object p2

    .line 461
    invoke-virtual {p1, p2}, Lcom/squareup/print/ReceiptFormatter;->authorizationCodeOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private static getBillPayment(Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/payment/BillPayment;
    .locals 3

    .line 88
    invoke-virtual {p0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object p0

    .line 89
    instance-of v0, p0, Lcom/squareup/payment/BillPayment;

    if-eqz v0, :cond_0

    .line 94
    check-cast p0, Lcom/squareup/payment/BillPayment;

    return-object p0

    .line 90
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 92
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v1, v2

    const-string p0, "paymentReceipt does not contain a BillPayment (has a %s)"

    .line 91
    invoke-static {p0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private getCardholderName(Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/locale/LocaleOverrideFactory;)Ljava/lang/String;
    .locals 1

    .line 470
    instance-of v0, p1, Lcom/squareup/payment/tender/MagStripeTender;

    if-eqz v0, :cond_0

    .line 471
    check-cast p1, Lcom/squareup/payment/tender/MagStripeTender;

    invoke-virtual {p1}, Lcom/squareup/payment/tender/MagStripeTender;->getCard()Lcom/squareup/Card;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 472
    invoke-virtual {p1}, Lcom/squareup/Card;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 473
    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/Card;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/squareup/text/Cards;->formattedCardOwnerName(Lcom/squareup/util/Res;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 475
    :cond_0
    instance-of v0, p1, Lcom/squareup/payment/tender/SmartCardTender;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/payment/tender/SmartCardTender;

    invoke-virtual {p1}, Lcom/squareup/payment/tender/SmartCardTender;->isContactless()Z

    move-result v0

    if-nez v0, :cond_1

    .line 476
    invoke-virtual {p1}, Lcom/squareup/payment/tender/SmartCardTender;->getCardholderName()Ljava/lang/String;

    move-result-object p1

    .line 477
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 478
    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p2

    invoke-static {p2, p1}, Lcom/squareup/text/Cards;->formattedCardOwnerName(Lcom/squareup/util/Res;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private getSwedishRounding(Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;
    .locals 5

    const-wide/16 v0, 0x0

    .line 309
    invoke-static {v0, v1, p3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 311
    instance-of v3, p2, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;

    if-eqz v3, :cond_0

    .line 312
    check-cast p2, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_1

    .line 315
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->isComplete()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 317
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->getUnboundedRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 318
    iget-object p2, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long p2, v3, v0

    if-eqz p2, :cond_1

    .line 319
    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    neg-long p1, p1

    invoke-static {p1, p2, p3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    :cond_1
    return-object v2
.end method

.method public static getTenderForPrinting(Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/payment/tender/BaseTender;
    .locals 0

    .line 84
    invoke-static {p0}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->getBillPayment(Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/payment/BillPayment;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object p0

    return-object p0
.end method

.method private getTimestamp(Lcom/squareup/payment/tender/BaseTender;)Ljava/util/Date;
    .locals 0

    .line 466
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->requireTender()Lcom/squareup/protos/client/bills/Tender;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p1, p1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Times;->requireIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public createAuthSlipPayload(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;ZZ)Lcom/squareup/print/payload/ReceiptPayload;
    .locals 9

    .line 126
    invoke-static {p2}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->getBillPayment(Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/payment/BillPayment;

    move-result-object v2

    sget-object v7, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    if-eqz p5, :cond_0

    sget-object p2, Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;->AUTH_SLIP_COPY:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;->AUTH_SLIP:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    :goto_0
    move-object v8, p2

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v8}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->createPayload(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;ZZZLcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;)Lcom/squareup/print/payload/ReceiptPayload;

    move-result-object p1

    return-object p1
.end method

.method public createItemizedReceiptPayload(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)Lcom/squareup/print/payload/ReceiptPayload;
    .locals 9

    .line 144
    invoke-static {p2}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->getBillPayment(Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/payment/BillPayment;

    move-result-object v2

    sget-object v8, Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;->ITEMIZED_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v8}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->createPayload(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;ZZZLcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;)Lcom/squareup/print/payload/ReceiptPayload;

    move-result-object p1

    return-object p1
.end method
