.class public Lcom/squareup/print/payload/TicketPayload$Factory;
.super Ljava/lang/Object;
.source "TicketPayload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/payload/TicketPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# instance fields
.field private final deviceNameSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final formatter:Lcom/squareup/print/ReceiptFormatter;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/ReceiptFormatter;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    .line 64
    iput-object p2, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->deviceNameSetting:Lcom/squareup/settings/LocalSetting;

    .line 65
    iput-object p3, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->features:Lcom/squareup/settings/server/Features;

    .line 66
    iput-object p4, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->localeProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p5, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 68
    iput-object p6, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 69
    iput-object p7, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private coverCountToString(I)Ljava/lang/String;
    .locals 2

    .line 194
    iget-object v0, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getSubscriptions()Lcom/squareup/settings/server/Subscriptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/Subscriptions;->hasRestaurantSubscription()Z

    move-result v0

    if-eqz v0, :cond_1

    if-lez p1, :cond_1

    const/4 v0, 0x1

    if-le p1, v0, :cond_0

    .line 195
    sget v0, Lcom/squareup/print/R$string;->cover_count_plural:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/print/R$string;->cover_count_singular:I

    .line 197
    :goto_0
    iget-object v1, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->res:Lcom/squareup/util/Res;

    .line 198
    invoke-interface {v1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "count"

    .line 199
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 200
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 201
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return-object p1
.end method

.method private createDiningOptionsItemSections(Ljava/util/List;Lcom/squareup/checkout/DiningOption;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;",
            "Lcom/squareup/checkout/DiningOption;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/TicketItemBlockSection;",
            ">;"
        }
    .end annotation

    .line 158
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    .line 165
    invoke-static {p1, p2, v0}, Lcom/squareup/itemsorter/ItemSorter;->groupItemsByDiningOption(Ljava/util/List;Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/Locale;)Ljava/util/List;

    move-result-object p1

    .line 167
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 168
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;

    .line 169
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 170
    invoke-virtual {v0}, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Lcom/squareup/print/PrintableOrderItem;

    .line 171
    iget-object v4, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    iget-object v3, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->localeProvider:Ljavax/inject/Provider;

    .line 172
    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Ljava/util/Locale;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v9, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_SEATING:Lcom/squareup/settings/server/Features$Feature;

    .line 173
    invoke-interface {v3, v9}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v9

    .line 174
    invoke-virtual {v0}, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->getDiningOption()Lcom/squareup/itemsorter/SortableDiningOption;

    move-result-object v3

    move-object v10, v3

    check-cast v10, Lcom/squareup/checkout/DiningOption;

    .line 172
    invoke-static/range {v4 .. v10}, Lcom/squareup/print/sections/TicketItemSection;->fromOrderItem(Lcom/squareup/print/ReceiptFormatter;Ljava/util/Locale;Lcom/squareup/print/PrintableOrderItem;ZLcom/squareup/util/Res;ZLcom/squareup/checkout/DiningOption;)Lcom/squareup/print/sections/TicketItemSection;

    move-result-object v3

    .line 171
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 176
    :cond_1
    new-instance v0, Lcom/squareup/print/sections/TicketItemBlockSection;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/sections/TicketItemSection;

    iget-object v2, v2, Lcom/squareup/print/sections/TicketItemSection;->diningOptionName:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/print/sections/TicketItemBlockSection;-><init>(Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object p2
.end method

.method private createFulfillmentSection(Lcom/squareup/print/PrintableRecipient;)Lcom/squareup/print/sections/FulfillmentSection;
    .locals 4

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 139
    :cond_0
    new-instance v0, Lcom/squareup/print/sections/FulfillmentSection;

    iget-object v1, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->res:Lcom/squareup/util/Res;

    invoke-interface {p1, v1}, Lcom/squareup/print/PrintableRecipient;->recipientLabel(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lcom/squareup/print/PrintableRecipient;->getRecipientName()Ljava/lang/String;

    move-result-object v2

    .line 140
    invoke-interface {p1}, Lcom/squareup/print/PrintableRecipient;->getRecipientAddress()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/squareup/print/PrintableRecipient;->getRecipientPhoneNumber()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/print/sections/FulfillmentSection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private createItemSection(Ljava/util/List;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/TicketItemBlockSection;",
            ">;"
        }
    .end annotation

    .line 144
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 148
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 149
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/print/PrintableOrderItem;

    .line 150
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    iget-object v1, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Ljava/util/Locale;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v7, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_SEATING:Lcom/squareup/settings/server/Features$Feature;

    .line 151
    invoke-interface {v1, v7}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v7

    invoke-interface {v4}, Lcom/squareup/print/PrintableOrderItem;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v8

    .line 150
    invoke-static/range {v2 .. v8}, Lcom/squareup/print/sections/TicketItemSection;->fromOrderItem(Lcom/squareup/print/ReceiptFormatter;Ljava/util/Locale;Lcom/squareup/print/PrintableOrderItem;ZLcom/squareup/util/Res;ZLcom/squareup/checkout/DiningOption;)Lcom/squareup/print/sections/TicketItemSection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 153
    :cond_1
    new-instance p1, Lcom/squareup/print/sections/TicketItemBlockSection;

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, Lcom/squareup/print/sections/TicketItemBlockSection;-><init>(Ljava/util/List;Ljava/lang/String;)V

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private getEmployeeFirstName(Lcom/squareup/print/PrintableOrder;)Ljava/lang/String;
    .locals 0

    .line 182
    invoke-interface {p1}, Lcom/squareup/print/PrintableOrder;->getEmployeeToken()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/print/payload/TicketPayload$Factory;->getEmployeeFirstNameByToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getEmployeeFirstNameByToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/print/payload/TicketPayload$Factory;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0, p1}, Lcom/squareup/permissions/EmployeeManagement;->getEmployeeByToken(Ljava/lang/String;)Lcom/squareup/permissions/Employee;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 189
    :cond_0
    iget-object p1, p1, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    :goto_0
    return-object p1
.end method


# virtual methods
.method public forOrderTicket(Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZLcom/squareup/checkout/DiningOption;Ljava/lang/String;Ljava/util/List;Lcom/squareup/print/PrintableRecipient;)Lcom/squareup/print/payload/TicketPayload;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;ZZ",
            "Lcom/squareup/checkout/DiningOption;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/print/PrintableRecipient;",
            ")",
            "Lcom/squareup/print/payload/TicketPayload;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move-object/from16 v3, p9

    .line 108
    iget-object v4, v0, Lcom/squareup/print/payload/TicketPayload$Factory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    iget-object v5, v0, Lcom/squareup/print/payload/TicketPayload$Factory;->deviceNameSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v5}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/squareup/print/ReceiptFormatter;->deviceNameOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 109
    iget-object v4, v0, Lcom/squareup/print/payload/TicketPayload$Factory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    invoke-virtual {v4}, Lcom/squareup/print/ReceiptFormatter;->reprint()Ljava/lang/String;

    move-result-object v16

    .line 110
    iget-object v4, v0, Lcom/squareup/print/payload/TicketPayload$Factory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    invoke-virtual {v4, v1}, Lcom/squareup/print/ReceiptFormatter;->getDateDetailString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    .line 111
    iget-object v4, v0, Lcom/squareup/print/payload/TicketPayload$Factory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    invoke-virtual {v4, v1}, Lcom/squareup/print/ReceiptFormatter;->getTimeDetailStringWithSeconds(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    if-eqz p7, :cond_0

    .line 112
    iget-object v1, v0, Lcom/squareup/print/payload/TicketPayload$Factory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    invoke-virtual {v1}, Lcom/squareup/print/ReceiptFormatter;->voidLabel()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    move-object/from16 v17, v1

    .line 113
    iget-object v1, v0, Lcom/squareup/print/payload/TicketPayload$Factory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    move-object/from16 v4, p11

    invoke-virtual {v1, v4}, Lcom/squareup/print/ReceiptFormatter;->receiptNumber(Ljava/util/List;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v1, p12

    .line 116
    invoke-direct {v0, v1}, Lcom/squareup/print/payload/TicketPayload$Factory;->createFulfillmentSection(Lcom/squareup/print/PrintableRecipient;)Lcom/squareup/print/sections/FulfillmentSection;

    move-result-object v12

    .line 119
    iget-object v1, v0, Lcom/squareup/print/payload/TicketPayload$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->DINING_OPTIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v3, :cond_1

    .line 120
    invoke-direct {v0, v2, v3}, Lcom/squareup/print/payload/TicketPayload$Factory;->createDiningOptionsItemSections(Ljava/util/List;Lcom/squareup/checkout/DiningOption;)Ljava/util/List;

    move-result-object v1

    goto :goto_1

    .line 122
    :cond_1
    invoke-direct {v0, v2}, Lcom/squareup/print/payload/TicketPayload$Factory;->createItemSection(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    :goto_1
    move-object v13, v1

    .line 125
    new-instance v1, Lcom/squareup/print/payload/TicketPayload;

    move-object v6, v1

    move-object/from16 v7, p5

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v14, p4

    move-object/from16 v19, p10

    invoke-direct/range {v6 .. v19}, Lcom/squareup/print/payload/TicketPayload;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/sections/FulfillmentSection;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move/from16 v2, p8

    .line 129
    invoke-virtual {v1, v2}, Lcom/squareup/print/payload/TicketPayload;->setIsCompactTicket(Z)V

    return-object v1
.end method

.method public fromOrder(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Ljava/util/List;ZZLjava/lang/String;)Lcom/squareup/print/payload/TicketPayload;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrintableOrder;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;ZZ",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/print/payload/TicketPayload;"
        }
    .end annotation

    move-object v13, p0

    move/from16 v14, p4

    if-eqz v14, :cond_0

    .line 81
    invoke-interface/range {p1 .. p1}, Lcom/squareup/print/PrintableOrder;->getTimestampForReprint()Ljava/util/Date;

    move-result-object v0

    goto :goto_0

    .line 82
    :cond_0
    invoke-interface/range {p1 .. p1}, Lcom/squareup/print/PrintableOrder;->getTimestamp()Ljava/util/Date;

    move-result-object v0

    :goto_0
    move-object v1, v0

    .line 84
    invoke-direct/range {p0 .. p1}, Lcom/squareup/print/payload/TicketPayload$Factory;->getEmployeeFirstName(Lcom/squareup/print/PrintableOrder;)Ljava/lang/String;

    move-result-object v2

    .line 85
    invoke-interface/range {p1 .. p1}, Lcom/squareup/print/PrintableOrder;->getEnabledSeatCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/print/payload/TicketPayload$Factory;->coverCountToString(I)Ljava/lang/String;

    move-result-object v3

    .line 86
    invoke-interface/range {p1 .. p1}, Lcom/squareup/print/PrintableOrder;->getOpenTicketNote()Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x0

    iget-object v0, v13, Lcom/squareup/print/payload/TicketPayload$Factory;->res:Lcom/squareup/util/Res;

    move-object/from16 v5, p1

    .line 87
    invoke-interface {v5, v0}, Lcom/squareup/print/PrintableOrder;->getDiningOption(Lcom/squareup/util/Res;)Lcom/squareup/checkout/DiningOption;

    move-result-object v9

    .line 88
    invoke-interface/range {p1 .. p1}, Lcom/squareup/print/PrintableOrder;->getReceiptNumbers()Ljava/util/List;

    move-result-object v11

    invoke-interface/range {p1 .. p1}, Lcom/squareup/print/PrintableOrder;->getRecipient()Lcom/squareup/print/PrintableRecipient;

    move-result-object v12

    move-object v0, p0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move/from16 v8, p5

    move-object/from16 v10, p6

    .line 84
    invoke-virtual/range {v0 .. v12}, Lcom/squareup/print/payload/TicketPayload$Factory;->forOrderTicket(Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZLcom/squareup/checkout/DiningOption;Ljava/lang/String;Ljava/util/List;Lcom/squareup/print/PrintableRecipient;)Lcom/squareup/print/payload/TicketPayload;

    move-result-object v0

    .line 89
    invoke-virtual {v0, v14}, Lcom/squareup/print/payload/TicketPayload;->setManualReprint(Z)V

    return-object v0
.end method

.method public fromVoidItems(Ljava/util/List;Lcom/squareup/tickets/OpenTicket;Ljava/util/Date;Lcom/squareup/checkout/DiningOption;ZLjava/lang/String;Ljava/util/List;)Lcom/squareup/print/payload/TicketPayload;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Ljava/util/Date;",
            "Lcom/squareup/checkout/DiningOption;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/print/payload/TicketPayload;"
        }
    .end annotation

    .line 96
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tickets/OpenTicket;->getEmployeeFirstName()Ljava/lang/String;

    move-result-object v2

    .line 97
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tickets/OpenTicket;->getEnabledSeatCount()I

    move-result v0

    move-object v13, p0

    invoke-direct {p0, v0}, Lcom/squareup/print/payload/TicketPayload$Factory;->coverCountToString(I)Ljava/lang/String;

    move-result-object v3

    .line 98
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tickets/OpenTicket;->getNote()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x1

    const/4 v12, 0x0

    move-object v0, p0

    move-object/from16 v1, p3

    move-object v6, p1

    move/from16 v8, p5

    move-object/from16 v9, p4

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    .line 96
    invoke-virtual/range {v0 .. v12}, Lcom/squareup/print/payload/TicketPayload$Factory;->forOrderTicket(Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZLcom/squareup/checkout/DiningOption;Ljava/lang/String;Ljava/util/List;Lcom/squareup/print/PrintableRecipient;)Lcom/squareup/print/payload/TicketPayload;

    move-result-object v0

    return-object v0
.end method
