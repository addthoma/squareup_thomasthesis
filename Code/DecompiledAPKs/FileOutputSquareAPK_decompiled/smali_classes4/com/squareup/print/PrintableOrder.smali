.class public interface abstract Lcom/squareup/print/PrintableOrder;
.super Ljava/lang/Object;
.source "PrintableOrder.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0012\u0010!\u001a\u0004\u0018\u00010\"2\u0006\u0010#\u001a\u00020$H&J\u0008\u0010%\u001a\u00020\u001cH&R\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u0018\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR\u0018\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u000eR\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0005R\u0014\u0010\u0013\u001a\u0004\u0018\u00010\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0005R\u0018\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u000bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u000eR\u0014\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\u001aR\u001a\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u00a6\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u001d\u0010\u001e\"\u0004\u0008\u001f\u0010 \u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/print/PrintableOrder;",
        "",
        "employeeToken",
        "",
        "getEmployeeToken",
        "()Ljava/lang/String;",
        "enabledSeatCount",
        "",
        "getEnabledSeatCount",
        "()I",
        "notVoidedLockedItems",
        "",
        "Lcom/squareup/print/PrintableOrderItem;",
        "getNotVoidedLockedItems",
        "()Ljava/util/List;",
        "notVoidedUnlockedItems",
        "getNotVoidedUnlockedItems",
        "openTicketNote",
        "getOpenTicketNote",
        "orderIdentifier",
        "getOrderIdentifier",
        "receiptNumbers",
        "getReceiptNumbers",
        "recipient",
        "Lcom/squareup/print/PrintableRecipient;",
        "getRecipient",
        "()Lcom/squareup/print/PrintableRecipient;",
        "timestampForReprint",
        "Ljava/util/Date;",
        "getTimestampForReprint",
        "()Ljava/util/Date;",
        "setTimestampForReprint",
        "(Ljava/util/Date;)V",
        "getDiningOption",
        "Lcom/squareup/checkout/DiningOption;",
        "res",
        "Lcom/squareup/util/Res;",
        "getTimestamp",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getDiningOption(Lcom/squareup/util/Res;)Lcom/squareup/checkout/DiningOption;
.end method

.method public abstract getEmployeeToken()Ljava/lang/String;
.end method

.method public abstract getEnabledSeatCount()I
.end method

.method public abstract getNotVoidedLockedItems()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getNotVoidedUnlockedItems()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getOpenTicketNote()Ljava/lang/String;
.end method

.method public abstract getOrderIdentifier()Ljava/lang/String;
.end method

.method public abstract getReceiptNumbers()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRecipient()Lcom/squareup/print/PrintableRecipient;
.end method

.method public abstract getTimestamp()Ljava/util/Date;
.end method

.method public abstract getTimestampForReprint()Ljava/util/Date;
.end method

.method public abstract setTimestampForReprint(Ljava/util/Date;)V
.end method
