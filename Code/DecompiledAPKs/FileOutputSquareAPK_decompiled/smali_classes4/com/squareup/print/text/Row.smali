.class public abstract Lcom/squareup/print/text/Row;
.super Ljava/lang/Object;
.source "RenderedRows.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/text/Row$StyledRow;,
        Lcom/squareup/print/text/Row$NewLine;,
        Lcom/squareup/print/text/Row$RawChars;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/print/text/Row;",
        "",
        "()V",
        "NewLine",
        "RawChars",
        "StyledRow",
        "Lcom/squareup/print/text/Row$StyledRow;",
        "Lcom/squareup/print/text/Row$NewLine;",
        "Lcom/squareup/print/text/Row$RawChars;",
        "hardware_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/squareup/print/text/Row;-><init>()V

    return-void
.end method
