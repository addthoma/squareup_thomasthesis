.class public abstract Lcom/squareup/print/PrinterScout;
.super Ljava/lang/Object;
.source "PrinterScout.java"

# interfaces
.implements Lcom/squareup/badbus/BadBusRegistrant;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/PrinterScout$ResultListener;
    }
.end annotation


# instance fields
.field private final backgroundExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private resultListener:Lcom/squareup/print/PrinterScout$ResultListener;

.field private final scoutInBackground:Ljava/lang/Runnable;


# direct methods
.method protected constructor <init>(Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/print/PrinterScout;->backgroundExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    .line 45
    iput-object p2, p0, Lcom/squareup/print/PrinterScout;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 46
    iput-object p3, p0, Lcom/squareup/print/PrinterScout;->features:Lcom/squareup/settings/server/Features;

    .line 47
    new-instance p1, Lcom/squareup/print/-$$Lambda$PrinterScout$kdqlkXUQBDSocwDQ8JmkjUR8OGY;

    invoke-direct {p1, p0}, Lcom/squareup/print/-$$Lambda$PrinterScout$kdqlkXUQBDSocwDQ8JmkjUR8OGY;-><init>(Lcom/squareup/print/PrinterScout;)V

    iput-object p1, p0, Lcom/squareup/print/PrinterScout;->scoutInBackground:Ljava/lang/Runnable;

    return-void
.end method

.method public static synthetic lambda$kdqlkXUQBDSocwDQ8JmkjUR8OGY(Lcom/squareup/print/PrinterScout;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/print/PrinterScout;->scoutInBackground()V

    return-void
.end method

.method private scoutInBackground()V
    .locals 1

    .line 51
    invoke-virtual {p0}, Lcom/squareup/print/PrinterScout;->scout()Ljava/util/List;

    move-result-object v0

    .line 52
    invoke-virtual {p0, v0}, Lcom/squareup/print/PrinterScout;->postResults(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method protected cancelPendingScouting()V
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/squareup/print/PrinterScout;->backgroundExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    iget-object v1, p0, Lcom/squareup/print/PrinterScout;->scoutInBackground:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/StoppableSerialExecutor;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected varargs debug(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .line 18
    iget-object v0, p0, Lcom/squareup/print/PrinterScout;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PRINTER_DEBUGGING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    invoke-static {p1, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method protected abstract getConnectionType()Lcom/squareup/print/ConnectionType;
.end method

.method public synthetic lambda$postResults$0$PrinterScout(Ljava/util/List;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 58
    iget-object v0, p0, Lcom/squareup/print/PrinterScout;->resultListener:Lcom/squareup/print/PrinterScout$ResultListener;

    if-eqz v0, :cond_0

    .line 59
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/print/PrinterScout$ResultListener;->onResult(Ljava/lang/String;Ljava/util/List;)V

    .line 61
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/print/PrinterScout;->onScoutingDone()V

    return-void
.end method

.method public synthetic lambda$shutdown$1$PrinterScout()V
    .locals 1

    .line 72
    invoke-virtual {p0}, Lcom/squareup/print/PrinterScout;->shutdownInBackground()V

    .line 73
    iget-object v0, p0, Lcom/squareup/print/PrinterScout;->backgroundExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    invoke-interface {v0}, Lcom/squareup/thread/executor/StoppableSerialExecutor;->shutdown()V

    return-void
.end method

.method protected onScoutingDone()V
    .locals 0

    return-void
.end method

.method protected postResults(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;)V"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/squareup/print/PrinterScout;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/print/-$$Lambda$PrinterScout$iAcH04xIf89XPsIpFZtoCD4JHtQ;

    invoke-direct {v1, p0, p1}, Lcom/squareup/print/-$$Lambda$PrinterScout$iAcH04xIf89XPsIpFZtoCD4JHtQ;-><init>(Lcom/squareup/print/PrinterScout;Ljava/util/List;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected postScout()V
    .locals 2

    .line 84
    invoke-virtual {p0}, Lcom/squareup/print/PrinterScout;->cancelPendingScouting()V

    .line 85
    iget-object v0, p0, Lcom/squareup/print/PrinterScout;->backgroundExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    iget-object v1, p0, Lcom/squareup/print/PrinterScout;->scoutInBackground:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/StoppableSerialExecutor;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected abstract scout()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;"
        }
    .end annotation
.end method

.method public setResultListener(Lcom/squareup/print/PrinterScout$ResultListener;)V
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/print/PrinterScout;->resultListener:Lcom/squareup/print/PrinterScout$ResultListener;

    return-void
.end method

.method protected shutdown()V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/print/PrinterScout;->backgroundExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    new-instance v1, Lcom/squareup/print/-$$Lambda$PrinterScout$McWUN3TtalhdVrRbpajHzyA-mck;

    invoke-direct {v1, p0}, Lcom/squareup/print/-$$Lambda$PrinterScout$McWUN3TtalhdVrRbpajHzyA-mck;-><init>(Lcom/squareup/print/PrinterScout;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/StoppableSerialExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected shutdownInBackground()V
    .locals 0

    return-void
.end method

.method public abstract start()V
.end method

.method public abstract stop()V
.end method

.method protected varargs verbose(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/print/PrinterScout;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PRINTER_DEBUGGING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    invoke-static {p1, p2}, Ltimber/log/Timber;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
