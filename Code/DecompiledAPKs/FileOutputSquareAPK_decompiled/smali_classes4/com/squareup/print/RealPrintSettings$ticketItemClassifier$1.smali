.class final Lcom/squareup/print/RealPrintSettings$ticketItemClassifier$1;
.super Ljava/lang/Object;
.source "PrintSettings.kt"

# interfaces
.implements Lcom/squareup/util/SquareCollections$Classifier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/print/RealPrintSettings;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/tickets/voidcomp/VoidCompSettings;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/util/SquareCollections$Classifier<",
        "Lcom/squareup/print/PrinterStation;",
        "Lcom/squareup/checkout/CartItem;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0005\u001a\n \u0004*\u0004\u0018\u00010\u00060\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "station",
        "Lcom/squareup/print/PrinterStation;",
        "kotlin.jvm.PlatformType",
        "item",
        "Lcom/squareup/checkout/CartItem;",
        "classContains"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/print/RealPrintSettings$ticketItemClassifier$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/print/RealPrintSettings$ticketItemClassifier$1;

    invoke-direct {v0}, Lcom/squareup/print/RealPrintSettings$ticketItemClassifier$1;-><init>()V

    sput-object v0, Lcom/squareup/print/RealPrintSettings$ticketItemClassifier$1;->INSTANCE:Lcom/squareup/print/RealPrintSettings$ticketItemClassifier$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final classContains(Lcom/squareup/print/PrinterStation;Lcom/squareup/checkout/CartItem;)Z
    .locals 1

    const-string v0, "item"

    .line 152
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->isUncategorized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    invoke-virtual {p1}, Lcom/squareup/print/PrinterStation;->printsUncategorizedItems()Z

    move-result p1

    goto :goto_0

    .line 155
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->categoryId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/print/PrinterStation;->isEnabledForCategoryId(Ljava/lang/String;)Z

    move-result p1

    :goto_0
    return p1
.end method

.method public bridge synthetic classContains(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    .line 144
    check-cast p1, Lcom/squareup/print/PrinterStation;

    check-cast p2, Lcom/squareup/checkout/CartItem;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/print/RealPrintSettings$ticketItemClassifier$1;->classContains(Lcom/squareup/print/PrinterStation;Lcom/squareup/checkout/CartItem;)Z

    move-result p1

    return p1
.end method
