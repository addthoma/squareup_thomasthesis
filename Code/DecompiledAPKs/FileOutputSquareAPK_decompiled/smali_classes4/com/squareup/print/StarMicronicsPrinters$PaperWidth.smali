.class final enum Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;
.super Ljava/lang/Enum;
.source "StarMicronicsPrinters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/StarMicronicsPrinters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "PaperWidth"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;

.field public static final enum THREE_INCH:Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;

.field public static final enum TWO_INCH:Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;


# instance fields
.field private dotsPerInch:I

.field private dotsPerLine:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 34
    new-instance v0, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;

    const/16 v1, 0xcb

    const/4 v2, 0x0

    const-string v3, "TWO_INCH"

    const/16 v4, 0x180

    invoke-direct {v0, v3, v2, v4, v1}, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;->TWO_INCH:Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;

    .line 35
    new-instance v0, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;

    const/4 v3, 0x1

    const-string v4, "THREE_INCH"

    const/16 v5, 0x23b

    invoke-direct {v0, v4, v3, v5, v1}, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;->THREE_INCH:Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;

    .line 33
    sget-object v1, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;->TWO_INCH:Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;->THREE_INCH:Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;->$VALUES:[Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;->dotsPerLine:I

    .line 42
    iput p4, p0, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;->dotsPerInch:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;
    .locals 1

    .line 33
    const-class v0, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;

    return-object p0
.end method

.method public static values()[Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;
    .locals 1

    .line 33
    sget-object v0, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;->$VALUES:[Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;

    invoke-virtual {v0}, [Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;

    return-object v0
.end method


# virtual methods
.method public getDotsPerInch()I
    .locals 1

    .line 50
    iget v0, p0, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;->dotsPerInch:I

    return v0
.end method

.method getDotsPerLine()I
    .locals 1

    .line 46
    iget v0, p0, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;->dotsPerLine:I

    return v0
.end method
