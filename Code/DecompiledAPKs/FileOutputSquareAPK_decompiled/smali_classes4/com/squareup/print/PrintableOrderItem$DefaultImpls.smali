.class public final Lcom/squareup/print/PrintableOrderItem$DefaultImpls;
.super Ljava/lang/Object;
.source "PrintableOrderItem.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PrintableOrderItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static setSelectedDiningOption(Lcom/squareup/print/PrintableOrderItem;Lcom/squareup/checkout/DiningOption;)Lcom/squareup/print/PrintableOrderItem;
    .locals 1

    const-string v0, "selectedDiningOption"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
