.class public Lcom/squareup/print/papersig/SignatureSection;
.super Ljava/lang/Object;
.source "SignatureSection.java"


# instance fields
.field public final cardIssuerAgreement:Ljava/lang/String;

.field public final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    invoke-static {p1}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/papersig/SignatureSection;->name:Ljava/lang/String;

    const-string p1, "cardIssuerAgreement"

    .line 73
    invoke-static {p2, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/print/papersig/SignatureSection;->cardIssuerAgreement:Ljava/lang/String;

    return-void
.end method

.method public static fromPayment(Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/ui/buyer/signature/AgreementBuilder;)Lcom/squareup/print/papersig/SignatureSection;
    .locals 3

    .line 30
    invoke-virtual {p0}, Lcom/squareup/papersignature/PaperSignatureSettings;->isSignOnPrintedReceiptEnabled()Z

    move-result p0

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 34
    :cond_0
    instance-of p0, p1, Lcom/squareup/payment/RequiresCardAgreement;

    const/4 v1, 0x1

    .line 37
    instance-of v2, p1, Lcom/squareup/payment/AcceptsSignature;

    if-eqz v2, :cond_1

    .line 38
    move-object v1, p1

    check-cast v1, Lcom/squareup/payment/AcceptsSignature;

    invoke-interface {v1}, Lcom/squareup/payment/AcceptsSignature;->askForSignature()Z

    move-result v1

    :cond_1
    if-eqz p0, :cond_3

    if-nez v1, :cond_2

    goto :goto_0

    .line 45
    :cond_2
    check-cast p1, Lcom/squareup/payment/RequiresCardAgreement;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/buyer/signature/AgreementBuilder;->getCardholderName(Lcom/squareup/payment/RequiresCardAgreement;)Ljava/lang/String;

    move-result-object p0

    .line 46
    invoke-virtual {p2}, Lcom/squareup/ui/buyer/signature/AgreementBuilder;->buildCardPaymentAgreementForPrinting()Ljava/lang/String;

    move-result-object p1

    .line 47
    new-instance p2, Lcom/squareup/print/papersig/SignatureSection;

    invoke-direct {p2, p0, p1}, Lcom/squareup/print/papersig/SignatureSection;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p2

    :cond_3
    :goto_0
    return-object v0
.end method

.method public static fromTenderHistory(Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/ui/buyer/signature/AgreementBuilder;)Lcom/squareup/print/papersig/SignatureSection;
    .locals 2

    .line 56
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->shouldPrintSignatureLine()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 61
    :cond_0
    instance-of v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    if-eqz v0, :cond_1

    .line 62
    check-cast p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    iget-object v1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->buyerName:Ljava/lang/String;

    .line 64
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/AgreementBuilder;->buildCardPaymentAgreementForPrinting()Ljava/lang/String;

    move-result-object p0

    .line 65
    new-instance p1, Lcom/squareup/print/papersig/SignatureSection;

    invoke-direct {p1, v1, p0}, Lcom/squareup/print/papersig/SignatureSection;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_6

    .line 95
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_2

    .line 97
    :cond_1
    check-cast p1, Lcom/squareup/print/papersig/SignatureSection;

    .line 99
    iget-object v2, p0, Lcom/squareup/print/papersig/SignatureSection;->name:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lcom/squareup/print/papersig/SignatureSection;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/squareup/print/papersig/SignatureSection;->name:Ljava/lang/String;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 100
    :cond_3
    iget-object v2, p0, Lcom/squareup/print/papersig/SignatureSection;->cardIssuerAgreement:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/papersig/SignatureSection;->cardIssuerAgreement:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    goto :goto_1

    :cond_4
    if-eqz p1, :cond_5

    :goto_1
    return v1

    :cond_5
    return v0

    :cond_6
    :goto_2
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 109
    iget-object v0, p0, Lcom/squareup/print/papersig/SignatureSection;->name:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 110
    iget-object v2, p0, Lcom/squareup/print/papersig/SignatureSection;->cardIssuerAgreement:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 1

    .line 78
    sget-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->EXTRA_LARGE:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->space(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 79
    sget-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->LARGE:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->space(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 80
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->signatureLine()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 82
    iget-object v0, p0, Lcom/squareup/print/papersig/SignatureSection;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    sget-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->SMALL:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->space(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 84
    iget-object v0, p0, Lcom/squareup/print/papersig/SignatureSection;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->centeredText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/papersig/SignatureSection;->cardIssuerAgreement:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 88
    sget-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->SMALL:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->space(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 89
    iget-object v0, p0, Lcom/squareup/print/papersig/SignatureSection;->cardIssuerAgreement:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->centeredText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_1
    return-void
.end method
