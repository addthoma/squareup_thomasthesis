.class public abstract Lcom/squareup/print/OrderTicketPrintingDisabledModule;
.super Ljava/lang/Object;
.source "OrderTicketPrintingDisabledModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindOrderTicketPrintingSettings(Lcom/squareup/print/OrderTicketPrintingSettings$OrderTicketPrintingDisabled;)Lcom/squareup/print/OrderTicketPrintingSettings;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
