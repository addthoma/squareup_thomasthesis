.class public Lcom/squareup/print/sections/RefundsSection$RefundSection;
.super Ljava/lang/Object;
.source "RefundsSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/sections/RefundsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RefundSection"
.end annotation


# instance fields
.field public final createdAtDate:Ljava/lang/String;

.field public final negativeRefundAmount:Ljava/lang/String;

.field public final reason:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/print/sections/RefundsSection$RefundSection;->createdAtDate:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/squareup/print/sections/RefundsSection$RefundSection;->negativeRefundAmount:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/squareup/print/sections/RefundsSection$RefundSection;->reason:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 27
    instance-of v0, p1, Lcom/squareup/print/sections/RefundsSection$RefundSection;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 31
    :cond_0
    check-cast p1, Lcom/squareup/print/sections/RefundsSection$RefundSection;

    .line 33
    iget-object v0, p0, Lcom/squareup/print/sections/RefundsSection$RefundSection;->createdAtDate:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/print/sections/RefundsSection$RefundSection;->createdAtDate:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/print/sections/RefundsSection$RefundSection;->negativeRefundAmount:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/print/sections/RefundsSection$RefundSection;->negativeRefundAmount:Ljava/lang/String;

    .line 34
    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/print/sections/RefundsSection$RefundSection;->reason:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/sections/RefundsSection$RefundSection;->reason:Ljava/lang/String;

    .line 35
    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 22
    iget-object v1, p0, Lcom/squareup/print/sections/RefundsSection$RefundSection;->createdAtDate:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/RefundsSection$RefundSection;->negativeRefundAmount:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/RefundsSection$RefundSection;->reason:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
