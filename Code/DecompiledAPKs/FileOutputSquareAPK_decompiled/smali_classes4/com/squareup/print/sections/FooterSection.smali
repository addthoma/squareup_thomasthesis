.class public Lcom/squareup/print/sections/FooterSection;
.super Ljava/lang/Object;
.source "FooterSection.java"


# instance fields
.field public final customReceiptText:Ljava/lang/String;

.field public final dispositionText:Ljava/lang/String;

.field public final formalReceiptItemListConfirmation:Ljava/lang/String;

.field public final formalReceiptItemListHeader:Ljava/lang/String;

.field public final nonTaxableItemLabel:Ljava/lang/String;

.field public final returnPolicy:Ljava/lang/String;

.field public final taxInvoiceLabel:Ljava/lang/String;

.field public final whichCopy:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/squareup/print/sections/FooterSection;->returnPolicy:Ljava/lang/String;

    .line 82
    iput-object p2, p0, Lcom/squareup/print/sections/FooterSection;->taxInvoiceLabel:Ljava/lang/String;

    .line 83
    iput-object p3, p0, Lcom/squareup/print/sections/FooterSection;->nonTaxableItemLabel:Ljava/lang/String;

    .line 84
    iput-object p4, p0, Lcom/squareup/print/sections/FooterSection;->customReceiptText:Ljava/lang/String;

    .line 85
    iput-object p5, p0, Lcom/squareup/print/sections/FooterSection;->whichCopy:Ljava/lang/String;

    .line 86
    iput-object p6, p0, Lcom/squareup/print/sections/FooterSection;->formalReceiptItemListHeader:Ljava/lang/String;

    .line 87
    iput-object p7, p0, Lcom/squareup/print/sections/FooterSection;->formalReceiptItemListConfirmation:Ljava/lang/String;

    .line 88
    iput-object p8, p0, Lcom/squareup/print/sections/FooterSection;->dispositionText:Ljava/lang/String;

    return-void
.end method

.method public static fromOrder(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/settings/server/Features;ZZ)Lcom/squareup/print/sections/FooterSection;
    .locals 13

    move-object/from16 v0, p3

    .line 36
    invoke-static {p0}, Lcom/squareup/print/sections/SectionUtils;->isAustralia(Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 37
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getBusinessAbn()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38
    invoke-virtual {p1}, Lcom/squareup/print/ReceiptFormatter;->invoiceLabel()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 39
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/print/ReceiptFormatter;->taxInvoiceLabel()Ljava/lang/String;

    move-result-object v1

    .line 41
    :goto_0
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/print/sections/SectionUtils;->hasBothTaxedAndUntaxedItems(Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 42
    invoke-virtual {p1}, Lcom/squareup/print/ReceiptFormatter;->nonTaxableItemLabel()Ljava/lang/String;

    move-result-object v3

    move-object v6, v1

    move-object v7, v3

    goto :goto_1

    :cond_1
    move-object v6, v1

    move-object v7, v2

    goto :goto_1

    :cond_2
    move-object v6, v2

    move-object v7, v6

    .line 47
    :goto_1
    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PRINT_MERCHANT_LOGO_ON_RECEIPTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 48
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getCustomReceiptText()Ljava/lang/String;

    move-result-object v1

    move-object v8, v1

    goto :goto_2

    :cond_3
    move-object v8, v2

    .line 51
    :goto_2
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getReturnPolicy()Ljava/lang/String;

    move-result-object v5

    if-eqz p4, :cond_4

    move-object v1, p1

    move/from16 v3, p5

    .line 54
    invoke-virtual {p1, v3}, Lcom/squareup/print/ReceiptFormatter;->whichCopy(Z)Ljava/lang/String;

    move-result-object v3

    move-object v9, v3

    goto :goto_3

    :cond_4
    move-object v1, p1

    move-object v9, v2

    .line 57
    :goto_3
    invoke-virtual {p1}, Lcom/squareup/print/ReceiptFormatter;->getFormalReceiptItemHeader()Ljava/lang/String;

    move-result-object v10

    .line 59
    invoke-virtual {p1}, Lcom/squareup/print/ReceiptFormatter;->getFormalReceiptItemReceivedConfirmation()Ljava/lang/String;

    move-result-object v11

    .line 61
    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->RECEIPTS_PRINT_DISPOSITION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 62
    invoke-virtual {p1}, Lcom/squareup/print/ReceiptFormatter;->getApprovedDispositionText()Ljava/lang/String;

    move-result-object v2

    :cond_5
    move-object v12, v2

    .line 65
    new-instance v0, Lcom/squareup/print/sections/FooterSection;

    move-object v4, v0

    invoke-direct/range {v4 .. v12}, Lcom/squareup/print/sections/FooterSection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_e

    .line 93
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_6

    .line 95
    :cond_1
    check-cast p1, Lcom/squareup/print/sections/FooterSection;

    .line 97
    iget-object v2, p0, Lcom/squareup/print/sections/FooterSection;->returnPolicy:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lcom/squareup/print/sections/FooterSection;->returnPolicy:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/squareup/print/sections/FooterSection;->returnPolicy:Ljava/lang/String;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 101
    :cond_3
    iget-object v2, p0, Lcom/squareup/print/sections/FooterSection;->taxInvoiceLabel:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v3, p1, Lcom/squareup/print/sections/FooterSection;->taxInvoiceLabel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_1

    :cond_4
    iget-object v2, p1, Lcom/squareup/print/sections/FooterSection;->taxInvoiceLabel:Ljava/lang/String;

    if-eqz v2, :cond_5

    :goto_1
    return v1

    .line 105
    :cond_5
    iget-object v2, p0, Lcom/squareup/print/sections/FooterSection;->nonTaxableItemLabel:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v3, p1, Lcom/squareup/print/sections/FooterSection;->nonTaxableItemLabel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    goto :goto_2

    :cond_6
    iget-object v2, p1, Lcom/squareup/print/sections/FooterSection;->nonTaxableItemLabel:Ljava/lang/String;

    if-eqz v2, :cond_7

    :goto_2
    return v1

    .line 109
    :cond_7
    iget-object v2, p0, Lcom/squareup/print/sections/FooterSection;->customReceiptText:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v3, p1, Lcom/squareup/print/sections/FooterSection;->customReceiptText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    goto :goto_3

    :cond_8
    iget-object v2, p1, Lcom/squareup/print/sections/FooterSection;->customReceiptText:Ljava/lang/String;

    if-eqz v2, :cond_9

    :goto_3
    return v1

    .line 113
    :cond_9
    iget-object v2, p0, Lcom/squareup/print/sections/FooterSection;->whichCopy:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v3, p1, Lcom/squareup/print/sections/FooterSection;->whichCopy:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    goto :goto_4

    :cond_a
    iget-object v2, p1, Lcom/squareup/print/sections/FooterSection;->whichCopy:Ljava/lang/String;

    if-eqz v2, :cond_b

    :goto_4
    return v1

    .line 116
    :cond_b
    iget-object v2, p0, Lcom/squareup/print/sections/FooterSection;->dispositionText:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/sections/FooterSection;->dispositionText:Ljava/lang/String;

    if-eqz v2, :cond_c

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_d

    goto :goto_5

    :cond_c
    if-eqz p1, :cond_d

    :goto_5
    return v1

    :cond_d
    return v0

    :cond_e
    :goto_6
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 125
    iget-object v0, p0, Lcom/squareup/print/sections/FooterSection;->returnPolicy:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 126
    iget-object v2, p0, Lcom/squareup/print/sections/FooterSection;->taxInvoiceLabel:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 127
    iget-object v2, p0, Lcom/squareup/print/sections/FooterSection;->nonTaxableItemLabel:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 128
    iget-object v2, p0, Lcom/squareup/print/sections/FooterSection;->customReceiptText:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 129
    iget-object v2, p0, Lcom/squareup/print/sections/FooterSection;->whichCopy:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 130
    iget-object v2, p0, Lcom/squareup/print/sections/FooterSection;->dispositionText:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 5

    .line 135
    iget-object v0, p0, Lcom/squareup/print/sections/FooterSection;->nonTaxableItemLabel:Ljava/lang/String;

    const/4 v1, 0x1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/print/sections/FooterSection;->taxInvoiceLabel:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 136
    :goto_1
    iget-object v2, p0, Lcom/squareup/print/sections/FooterSection;->whichCopy:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/2addr v2, v1

    .line 138
    iget-object v3, p0, Lcom/squareup/print/sections/FooterSection;->returnPolicy:Ljava/lang/String;

    invoke-static {v3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    xor-int/2addr v3, v1

    .line 139
    iget-object v4, p0, Lcom/squareup/print/sections/FooterSection;->customReceiptText:Ljava/lang/String;

    invoke-static {v4}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    xor-int/2addr v1, v4

    if-nez v3, :cond_2

    if-nez v1, :cond_2

    if-nez v0, :cond_2

    if-eqz v2, :cond_3

    .line 141
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->largeSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 144
    :cond_3
    iget-object v1, p0, Lcom/squareup/print/sections/FooterSection;->customReceiptText:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 145
    iget-object v1, p0, Lcom/squareup/print/sections/FooterSection;->customReceiptText:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->centeredText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 148
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_5
    if-eqz v3, :cond_6

    .line 153
    iget-object v1, p0, Lcom/squareup/print/sections/FooterSection;->returnPolicy:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->centeredText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    if-eqz v0, :cond_6

    .line 156
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_6
    if-eqz v0, :cond_8

    .line 161
    iget-object v0, p0, Lcom/squareup/print/sections/FooterSection;->nonTaxableItemLabel:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 162
    iget-object v1, p0, Lcom/squareup/print/sections/FooterSection;->taxInvoiceLabel:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsLeftExpandingText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_2

    .line 164
    :cond_7
    iget-object v0, p0, Lcom/squareup/print/sections/FooterSection;->taxInvoiceLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->centeredText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_8
    :goto_2
    if-eqz v2, :cond_9

    .line 169
    sget-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->SMALL:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->space(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 170
    iget-object v0, p0, Lcom/squareup/print/sections/FooterSection;->whichCopy:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->centeredText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 173
    :cond_9
    iget-object v0, p0, Lcom/squareup/print/sections/FooterSection;->dispositionText:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 174
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 175
    iget-object v0, p0, Lcom/squareup/print/sections/FooterSection;->dispositionText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthCenteredHeadlineText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_a
    return-void
.end method
