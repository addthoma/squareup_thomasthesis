.class final Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory$create$1;
.super Ljava/lang/Object;
.source "ProgressBinder.kt"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/progress/ProgressScreenCoordinator;",
        "it",
        "Landroid/view/View;",
        "provideCoordinator"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;


# direct methods
.method constructor <init>(Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory$create$1;->this$0:Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;

    iput-object p2, p0, Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory$create$1;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 0

    .line 30
    invoke-virtual {p0, p1}, Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory$create$1;->provideCoordinator(Landroid/view/View;)Lcom/squareup/progress/ProgressScreenCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public final provideCoordinator(Landroid/view/View;)Lcom/squareup/progress/ProgressScreenCoordinator;
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object p1, p0, Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory$create$1;->this$0:Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;

    iget-object p1, p1, Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;->this$0:Lcom/squareup/progress/ProgressBinder;

    invoke-static {p1}, Lcom/squareup/progress/ProgressBinder;->access$getCoordinatorProvider$p(Lcom/squareup/progress/ProgressBinder;)Lcom/squareup/caller/ProgressDialogCoordinator$Provider;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory$create$1;->$view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/caller/ProgressDialogCoordinator$Provider;->provideCoordinator(Landroid/view/View;)Lcom/squareup/caller/ProgressDialogCoordinator;

    move-result-object p1

    const-string v0, "coordinatorProvider.provideCoordinator(view)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v0, Lcom/squareup/progress/ProgressScreenCoordinator;

    iget-object v1, p0, Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory$create$1;->this$0:Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;

    invoke-virtual {v1}, Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;->getScreen()Lio/reactivex/Observable;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/progress/ProgressScreenCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/caller/ProgressDialogCoordinator;)V

    return-object v0
.end method
