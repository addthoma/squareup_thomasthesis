.class public abstract Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;
.super Lcom/squareup/salesreport/util/SalesReportRow;
.source "SalesReportRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/util/SalesReportRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SalesDetailsRow"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;,
        Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u001c\u001dBP\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\u0007\u0012\u0006\u0010\n\u001a\u00020\u0007\u0012\u0017\u0010\u000b\u001a\u0013\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000c\u00a2\u0006\u0002\u0008\u000f\u00a2\u0006\u0002\u0010\u0010R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R%\u0010\u000b\u001a\u0013\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000c\u00a2\u0006\u0002\u0008\u000fX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0014\u0010\t\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0012R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0014\u0010\n\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0012R\u0014\u0010\u0008\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0012\u0082\u0001\u0002\u001e\u001f\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow;",
        "name",
        "Lcom/squareup/util/ViewString;",
        "money",
        "",
        "bold",
        "",
        "subRow",
        "hasSubRows",
        "showingSubRows",
        "clickHandler",
        "Lkotlin/Function1;",
        "Landroid/view/View;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;)V",
        "getBold",
        "()Z",
        "getClickHandler",
        "()Lkotlin/jvm/functions/Function1;",
        "getHasSubRows",
        "getMoney",
        "()Ljava/lang/String;",
        "getName",
        "()Lcom/squareup/util/ViewString;",
        "getShowingSubRows",
        "getSubRow",
        "FullSalesDetailsRow",
        "TruncatedSalesDetailsRow",
        "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bold:Z

.field private final clickHandler:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final hasSubRows:Z

.field private final money:Ljava/lang/String;

.field private final name:Lcom/squareup/util/ViewString;

.field private final showingSubRows:Z

.field private final subRow:Z


# direct methods
.method private constructor <init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ViewString;",
            "Ljava/lang/String;",
            "ZZZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 56
    invoke-direct {p0, v0}, Lcom/squareup/salesreport/util/SalesReportRow;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->name:Lcom/squareup/util/ViewString;

    iput-object p2, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->money:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->bold:Z

    iput-boolean p4, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->subRow:Z

    iput-boolean p5, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->hasSubRows:Z

    iput-boolean p6, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->showingSubRows:Z

    iput-object p7, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->clickHandler:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 48
    invoke-direct/range {p0 .. p7}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;)V

    return-void
.end method


# virtual methods
.method public getBold()Z
    .locals 1

    .line 51
    iget-boolean v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->bold:Z

    return v0
.end method

.method public getClickHandler()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->clickHandler:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public getHasSubRows()Z
    .locals 1

    .line 53
    iget-boolean v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->hasSubRows:Z

    return v0
.end method

.method public getMoney()Ljava/lang/String;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->money:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Lcom/squareup/util/ViewString;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->name:Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public getShowingSubRows()Z
    .locals 1

    .line 54
    iget-boolean v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->showingSubRows:Z

    return v0
.end method

.method public getSubRow()Z
    .locals 1

    .line 52
    iget-boolean v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->subRow:Z

    return v0
.end method
