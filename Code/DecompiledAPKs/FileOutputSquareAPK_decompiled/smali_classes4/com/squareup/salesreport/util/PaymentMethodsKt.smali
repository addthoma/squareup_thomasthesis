.class public final Lcom/squareup/salesreport/util/PaymentMethodsKt;
.super Ljava/lang/Object;
.source "PaymentMethods.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028AX\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "title",
        "",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;",
        "getTitle",
        "(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;)I",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getTitle(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;)I
    .locals 1

    const-string v0, "$this$title"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    sget-object v0, Lcom/squareup/salesreport/util/PaymentMethodsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 29
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_method_emoney_title:I

    goto :goto_0

    .line 28
    :pswitch_1
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_method_installment_title:I

    goto :goto_0

    .line 27
    :pswitch_2
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_method_external_title:I

    goto :goto_0

    .line 26
    :pswitch_3
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_method_zero_amount_title:I

    goto :goto_0

    .line 25
    :pswitch_4
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_method_gift_card_title:I

    goto :goto_0

    .line 24
    :pswitch_5
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_method_third_party_card_title:I

    goto :goto_0

    .line 23
    :pswitch_6
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_method_split_tender_title:I

    goto :goto_0

    .line 22
    :pswitch_7
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_method_other_title:I

    goto :goto_0

    .line 21
    :pswitch_8
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_method_card_title:I

    goto :goto_0

    .line 20
    :pswitch_9
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_method_cash_title:I

    :goto_0
    return p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
