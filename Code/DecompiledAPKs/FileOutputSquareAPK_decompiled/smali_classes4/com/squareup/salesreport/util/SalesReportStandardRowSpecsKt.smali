.class public final Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;
.super Ljava/lang/Object;
.source "SalesReportStandardRowSpecs.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSalesReportStandardRowSpecs.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SalesReportStandardRowSpecs.kt\ncom/squareup/salesreport/util/SalesReportStandardRowSpecsKt\n+ 2 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,647:1\n35#2,6:648\n35#2,6:654\n35#2,6:660\n35#2,6:666\n35#2,6:672\n35#2,6:678\n35#2,6:684\n35#2,6:690\n35#2,6:696\n35#2,6:709\n35#2,6:715\n35#2,6:721\n35#2,6:727\n1103#3,7:702\n*E\n*S KotlinDebug\n*F\n+ 1 SalesReportStandardRowSpecs.kt\ncom/squareup/salesreport/util/SalesReportStandardRowSpecsKt\n*L\n72#1,6:648\n86#1,6:654\n101#1,6:660\n114#1,6:666\n149#1,6:672\n412#1,6:678\n428#1,6:684\n446#1,6:690\n470#1,6:696\n522#1,6:709\n550#1,6:715\n578#1,6:721\n593#1,6:727\n489#1,7:702\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a0\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0004\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a8\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002\u001a\"\u0010\u0011\u001a\u00020\u0012*\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0017H\u0002\u001a\u001e\u0010\u0019\u001a\u00020\u0006*\u0014\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001d0\u001aH\u0000\u001a,\u0010\u001e\u001a\u00020\u0006*\u0014\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u001f\u0012\u0004\u0012\u00020 0\u001a2\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u00020#0\"H\u0000\u001a\u001e\u0010$\u001a\u00020\u0006*\u0014\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020%\u0012\u0004\u0012\u00020\u001d0\u001aH\u0000\u001a\u0084\u0001\u0010&\u001a\u00020\u0006*\u0014\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\'\u0012\u0004\u0012\u00020\u001d0\u001a2\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u00172\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\u000c\u0010,\u001a\u0008\u0012\u0004\u0012\u00020-0\"2\u000c\u0010.\u001a\u0008\u0012\u0004\u0012\u00020/0\"2\u000c\u00100\u001a\u0008\u0012\u0004\u0012\u00020-0\"2\u000c\u00101\u001a\u0008\u0012\u0004\u0012\u00020/0\"2\u0006\u00102\u001a\u0002032\u0006\u0010\u0014\u001a\u00020\u0015H\u0000\u001a&\u00104\u001a\u00020\u0006*\u0014\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u000205\u0012\u0004\u0012\u00020\u001d0\u001a2\u0006\u0010\u0007\u001a\u00020\u0008H\u0000\u001a\u001e\u00106\u001a\u00020\u0006*\u0014\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u000207\u0012\u0004\u0012\u00020\u001d0\u001aH\u0000\u001a\u001e\u00108\u001a\u00020\u0006*\u0014\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u000209\u0012\u0004\u0012\u00020\u001d0\u001aH\u0000\u001a\u001e\u0010:\u001a\u00020\u0006*\u0014\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020;\u0012\u0004\u0012\u00020\u001d0\u001aH\u0000\u001a\u001e\u0010<\u001a\u00020\u0006*\u0014\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020=\u0012\u0004\u0012\u00020\u001d0\u001aH\u0000\u001a\u001e\u0010>\u001a\u00020\u0006*\u0014\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020?\u0012\u0004\u0012\u00020\u001d0\u001aH\u0000\u001a\u001e\u0010@\u001a\u00020\u0006*\u0014\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020A\u0012\u0004\u0012\u00020 0\u001aH\u0000\u001a&\u0010B\u001a\u00020\u0006*\u0014\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020C\u0012\u0004\u0012\u00020\u001d0\u001a2\u0006\u0010\u0007\u001a\u00020\u0008H\u0000\u001a&\u0010D\u001a\u00020\u0006*\u0014\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020E\u0012\u0004\u0012\u00020\u001d0\u001a2\u0006\u0010\u0014\u001a\u00020\u0015H\u0000\u001a*\u0010F\u001a\u00020\u0012*\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u00172\u0006\u0010(\u001a\u00020)H\u0002\u001a:\u0010G\u001a\u00020\u0012*\u00020\'2\u0006\u0010\u0014\u001a\u00020\u00152\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u00172\u0006\u0010(\u001a\u00020)2\u0006\u0010H\u001a\u00020I2\u0006\u0010J\u001a\u00020KH\u0002\u001a\u0014\u0010L\u001a\u00020M*\u00020N2\u0006\u0010O\u001a\u00020PH\u0002\u001a\u0014\u0010Q\u001a\u00020\u0006*\u00020R2\u0006\u0010S\u001a\u00020TH\u0002\u001a:\u0010U\u001a\u00020\u0012*\u00020\'2\u0006\u0010\u0014\u001a\u00020\u00152\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u00172\u0006\u0010(\u001a\u00020)2\u0006\u0010H\u001a\u00020I2\u0006\u0010J\u001a\u00020KH\u0002\u001a\u0014\u0010V\u001a\u00020\u0001*\u00020W2\u0006\u0010J\u001a\u00020KH\u0003\u001a\u001c\u0010X\u001a\u00020\u0006*\u00020Y2\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010Z\u001a\u00020KH\u0002\u001a@\u0010[\u001a\n ]*\u0004\u0018\u00010\\0\\*\u00020^2\u0006\u0010_\u001a\u00020`2\u0006\u00102\u001a\u0002032\u000c\u0010a\u001a\u0008\u0012\u0004\u0012\u00020-0\"2\u000c\u0010b\u001a\u0008\u0012\u0004\u0012\u00020/0\"H\u0000\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028CX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006c"
    }
    d2 = {
        "textColor",
        "",
        "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;",
        "getTextColor",
        "(Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;)I",
        "bindSalesDetailRow",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "salesDetailsRow",
        "view",
        "Landroid/view/View;",
        "name",
        "Landroid/widget/TextView;",
        "money",
        "glyph",
        "Lcom/squareup/glyph/SquareGlyphView;",
        "compareLabel",
        "",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "res",
        "Lcom/squareup/util/Res;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "createFeesRow",
        "Lcom/squareup/cycler/StandardRowSpec;",
        "Lcom/squareup/salesreport/util/SalesReportRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow$FeesRow;",
        "Landroid/view/ViewGroup;",
        "createPaymentMethodRow",
        "Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodRow;",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "wholeNumberPercentageFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/util/Percentage;",
        "createPaymentMethodTotalRow",
        "Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodTotalRow;",
        "createSalesChartRow",
        "Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;",
        "localTimeFormatter",
        "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
        "current24HourClockMode",
        "Lcom/squareup/time/Current24HourClockMode;",
        "exactValueMoneyFormatter",
        "Lcom/squareup/protos/common/Money;",
        "exactValueNumberFormatter",
        "",
        "rangeMoneyFormatter",
        "rangeNumberFormatter",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "createSalesDetailsRow",
        "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;",
        "createSalesOverviewRow",
        "Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;",
        "createSectionHeaderRow",
        "Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderRow;",
        "createSectionHeaderWithButtonRow",
        "Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithButtonRow;",
        "createSectionHeaderWithImageButtonRow",
        "Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithImageButtonRow;",
        "createSubsectionHeaderRow",
        "Lcom/squareup/salesreport/util/SalesReportRow$SubsectionHeaderRow;",
        "createTruncatedPaymentMethodRow",
        "Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;",
        "createTruncatedSalesDetailsRow",
        "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;",
        "createTwoTabToggleRow",
        "Lcom/squareup/salesreport/util/SalesReportRow$TwoTabToggleRow;",
        "currentLabel",
        "domainDescription",
        "dateTime",
        "Lorg/threeten/bp/LocalDateTime;",
        "isComparison",
        "",
        "pickChartView",
        "Lcom/squareup/chartography/ChartView;",
        "Lcom/squareup/customreport/data/WithSalesChartReport;",
        "viewAnimator",
        "Lcom/squareup/widgets/SquareViewAnimator;",
        "render",
        "Lcom/squareup/salesreport/widget/AmountCell;",
        "overviewItem",
        "Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;",
        "scrubbingDescription",
        "scrubbingLabelFormat",
        "Lcom/squareup/customreport/data/SalesReportType$Charted;",
        "setSelection",
        "Lcom/squareup/noho/NohoLabel;",
        "selected",
        "textFromChartSalesSelection",
        "",
        "kotlin.jvm.PlatformType",
        "",
        "chartSalesSelection",
        "Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;",
        "moneyFormatter",
        "numberFormatter",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$bindSalesDetailRow(Landroid/content/res/Resources;Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/squareup/glyph/SquareGlyphView;)V
    .locals 0

    .line 1
    invoke-static/range {p0 .. p5}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->bindSalesDetailRow(Landroid/content/res/Resources;Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/squareup/glyph/SquareGlyphView;)V

    return-void
.end method

.method public static final synthetic access$compareLabel(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->compareLabel(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$currentLabel(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->currentLabel(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getTextColor$p(Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;)I
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->getTextColor(Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$pickChartView(Lcom/squareup/customreport/data/WithSalesChartReport;Lcom/squareup/widgets/SquareViewAnimator;)Lcom/squareup/chartography/ChartView;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->pickChartView(Lcom/squareup/customreport/data/WithSalesChartReport;Lcom/squareup/widgets/SquareViewAnimator;)Lcom/squareup/chartography/ChartView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$render(Lcom/squareup/salesreport/widget/AmountCell;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->render(Lcom/squareup/salesreport/widget/AmountCell;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;)V

    return-void
.end method

.method public static final synthetic access$scrubbingDescription(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lorg/threeten/bp/LocalDateTime;Z)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static/range {p0 .. p5}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->scrubbingDescription(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lorg/threeten/bp/LocalDateTime;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setSelection(Lcom/squareup/noho/NohoLabel;Lcom/squareup/util/Res;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->setSelection(Lcom/squareup/noho/NohoLabel;Lcom/squareup/util/Res;Z)V

    return-void
.end method

.method private static final bindSalesDetailRow(Landroid/content/res/Resources;Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/squareup/glyph/SquareGlyphView;)V
    .locals 2

    .line 489
    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->getClickHandler()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    .line 702
    new-instance v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$bindSalesDetailRow$$inlined$onClickDebounced$1;

    invoke-direct {v1, v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$bindSalesDetailRow$$inlined$onClickDebounced$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 490
    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->getBold()Z

    move-result p2

    if-eqz p2, :cond_0

    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 491
    :goto_0
    invoke-static {p1}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->getTextColor(Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 493
    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->getName()Lcom/squareup/util/ViewString;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {p3, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 494
    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 495
    invoke-static {p3, p2}, Lcom/squareup/marketfont/MarketUtils;->setTextViewTypeface(Landroid/widget/TextView;Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 497
    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->getMoney()Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    invoke-virtual {p4, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 498
    invoke-static {p4, p2}, Lcom/squareup/marketfont/MarketUtils;->setTextViewTypeface(Landroid/widget/TextView;Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 499
    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 501
    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->getHasSubRows()Z

    move-result p0

    if-nez p0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->getSubRow()Z

    move-result p0

    if-nez p0, :cond_1

    const/16 p0, 0x8

    goto :goto_1

    .line 503
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->getSubRow()Z

    move-result p0

    if-eqz p0, :cond_2

    const/4 p0, 0x4

    goto :goto_1

    :cond_2
    const/4 p0, 0x0

    .line 501
    :goto_1
    invoke-virtual {p5, p0}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 509
    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->getShowingSubRows()Z

    move-result p0

    if-eqz p0, :cond_3

    .line 510
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->DOWN_CARET:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_2

    .line 512
    :cond_3
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->RIGHT_CARET:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 509
    :goto_2
    invoke-virtual {p5, p0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method private static final compareLabel(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 371
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;

    if-eqz v0, :cond_1

    .line 372
    invoke-static {p0}, Lcom/squareup/customreport/data/util/ReportConfigsKt;->comparisonDateRange(Lcom/squareup/customreport/data/ReportConfig;)Lkotlin/Pair;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalDate;

    .line 374
    sget v2, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_date_range:I

    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 377
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object v3

    .line 379
    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v4

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v5

    if-ne v4, v5, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    .line 378
    :goto_0
    invoke-static {v3, v4, p1, p2}, Lcom/squareup/salesreport/util/RangeSelectionsKt;->startDateFormatter(Lcom/squareup/customreport/data/RangeSelection;ZLcom/squareup/util/Res;Ljavax/inject/Provider;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p2

    .line 383
    check-cast v1, Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-virtual {p2, v1}, Lorg/threeten/bp/format/DateTimeFormatter;->format(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string v1, "start_date"

    .line 375
    invoke-virtual {v2, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 385
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/salesreport/util/RangeSelectionsKt;->endDateFormatter(Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/util/Res;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p0

    check-cast v0, Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/format/DateTimeFormatter;->format(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    const-string p1, "end_date"

    invoke-virtual {p2, p1, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 386
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 387
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 389
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getComparisonRange()Lcom/squareup/customreport/data/ComparisonRange;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/salesreport/util/ComparisonRangesKt;->description(Lcom/squareup/customreport/data/ComparisonRange;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final createFeesRow(Lcom/squareup/cycler/StandardRowSpec;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            "Lcom/squareup/salesreport/util/SalesReportRow$FeesRow;",
            "Landroid/view/ViewGroup;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$createFeesRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 593
    sget v0, Lcom/squareup/salesreport/impl/R$layout;->sales_report_fee_row:I

    .line 727
    new-instance v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createFeesRow$$inlined$create$1;

    invoke-direct {v1, v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createFeesRow$$inlined$create$1;-><init>(I)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public static final createPaymentMethodRow(Lcom/squareup/cycler/StandardRowSpec;Lcom/squareup/text/Formatter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            "Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodRow;",
            "Landroidx/constraintlayout/widget/ConstraintLayout;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$createPaymentMethodRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "wholeNumberPercentageFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 522
    sget v0, Lcom/squareup/salesreport/impl/R$layout;->sales_report_payment_method_row:I

    .line 709
    new-instance v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1;

    invoke-direct {v1, v0, p1}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodRow$$inlined$create$1;-><init>(ILcom/squareup/text/Formatter;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public static final createPaymentMethodTotalRow(Lcom/squareup/cycler/StandardRowSpec;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            "Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodTotalRow;",
            "Landroid/view/ViewGroup;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$createPaymentMethodTotalRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 578
    sget v0, Lcom/squareup/salesreport/impl/R$layout;->sales_report_payment_method_total_row:I

    .line 721
    new-instance v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodTotalRow$$inlined$create$1;

    invoke-direct {v1, v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createPaymentMethodTotalRow$$inlined$create$1;-><init>(I)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public static final createSalesChartRow(Lcom/squareup/cycler/StandardRowSpec;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/time/Current24HourClockMode;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            "Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;",
            "Landroid/view/ViewGroup;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            "Lcom/squareup/time/Current24HourClockMode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    const-string v1, "$this$createSalesChartRow"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "localeProvider"

    move-object v5, p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "localTimeFormatter"

    move-object v6, p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "current24HourClockMode"

    move-object/from16 v12, p3

    invoke-static {v12, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "exactValueMoneyFormatter"

    move-object/from16 v7, p4

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "exactValueNumberFormatter"

    move-object/from16 v8, p5

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "rangeMoneyFormatter"

    move-object/from16 v9, p6

    invoke-static {v9, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "rangeNumberFormatter"

    move-object/from16 v10, p7

    invoke-static {v10, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "currencyCode"

    move-object/from16 v11, p8

    invoke-static {v11, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "res"

    move-object/from16 v4, p9

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    sget v3, Lcom/squareup/salesreport/impl/R$layout;->sales_report_chart_view:I

    .line 672
    new-instance v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;

    move-object v2, v1

    invoke-direct/range {v2 .. v12}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesChartRow$$inlined$create$1;-><init>(ILcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/time/Current24HourClockMode;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public static final createSalesDetailsRow(Lcom/squareup/cycler/StandardRowSpec;Landroid/content/res/Resources;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;",
            "Landroid/view/ViewGroup;",
            ">;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    const-string v0, "$this$createSalesDetailsRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 446
    sget v0, Lcom/squareup/salesreport/impl/R$layout;->sales_report_sales_details_row:I

    .line 690
    new-instance v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1;

    invoke-direct {v1, v0, p1}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesDetailsRow$$inlined$create$1;-><init>(ILandroid/content/res/Resources;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public static final createSalesOverviewRow(Lcom/squareup/cycler/StandardRowSpec;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            "Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;",
            "Landroid/view/ViewGroup;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$createSalesOverviewRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    sget v0, Lcom/squareup/salesreport/impl/R$layout;->sales_report_v2_overview:I

    .line 666
    new-instance v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesOverviewRow$$inlined$create$1;

    invoke-direct {v1, v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSalesOverviewRow$$inlined$create$1;-><init>(I)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public static final createSectionHeaderRow(Lcom/squareup/cycler/StandardRowSpec;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            "Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderRow;",
            "Landroid/view/ViewGroup;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$createSectionHeaderRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    sget v0, Lcom/squareup/salesreport/impl/R$layout;->sales_report_section_header:I

    .line 648
    new-instance v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSectionHeaderRow$$inlined$create$1;

    invoke-direct {v1, v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSectionHeaderRow$$inlined$create$1;-><init>(I)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public static final createSectionHeaderWithButtonRow(Lcom/squareup/cycler/StandardRowSpec;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            "Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithButtonRow;",
            "Landroid/view/ViewGroup;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$createSectionHeaderWithButtonRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    sget v0, Lcom/squareup/salesreport/impl/R$layout;->sales_report_section_header:I

    .line 654
    new-instance v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSectionHeaderWithButtonRow$$inlined$create$1;

    invoke-direct {v1, v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSectionHeaderWithButtonRow$$inlined$create$1;-><init>(I)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public static final createSectionHeaderWithImageButtonRow(Lcom/squareup/cycler/StandardRowSpec;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            "Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithImageButtonRow;",
            "Landroid/view/ViewGroup;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$createSectionHeaderWithImageButtonRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    sget v0, Lcom/squareup/salesreport/impl/R$layout;->sales_report_section_header_with_image:I

    .line 660
    new-instance v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSectionHeaderWithImageButtonRow$$inlined$create$1;

    invoke-direct {v1, v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSectionHeaderWithImageButtonRow$$inlined$create$1;-><init>(I)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public static final createSubsectionHeaderRow(Lcom/squareup/cycler/StandardRowSpec;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            "Lcom/squareup/salesreport/util/SalesReportRow$SubsectionHeaderRow;",
            "Landroid/view/ViewGroup;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$createSubsectionHeaderRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 412
    sget v0, Lcom/squareup/salesreport/impl/R$layout;->sales_report_subsection_header:I

    .line 678
    new-instance v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSubsectionHeaderRow$$inlined$create$1;

    invoke-direct {v1, v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createSubsectionHeaderRow$$inlined$create$1;-><init>(I)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public static final createTruncatedPaymentMethodRow(Lcom/squareup/cycler/StandardRowSpec;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            "Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;",
            "Landroidx/constraintlayout/widget/ConstraintLayout;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$createTruncatedPaymentMethodRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 550
    sget v0, Lcom/squareup/salesreport/impl/R$layout;->sales_report_two_col_payment_method_row:I

    .line 715
    new-instance v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedPaymentMethodRow$$inlined$create$1;

    invoke-direct {v1, v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedPaymentMethodRow$$inlined$create$1;-><init>(I)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public static final createTruncatedSalesDetailsRow(Lcom/squareup/cycler/StandardRowSpec;Landroid/content/res/Resources;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;",
            "Landroid/view/ViewGroup;",
            ">;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    const-string v0, "$this$createTruncatedSalesDetailsRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 470
    sget v0, Lcom/squareup/salesreport/impl/R$layout;->sales_report_two_col_sales_details_row:I

    .line 696
    new-instance v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1;

    invoke-direct {v1, v0, p1}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTruncatedSalesDetailsRow$$inlined$create$1;-><init>(ILandroid/content/res/Resources;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public static final createTwoTabToggleRow(Lcom/squareup/cycler/StandardRowSpec;Lcom/squareup/util/Res;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            "Lcom/squareup/salesreport/util/SalesReportRow$TwoTabToggleRow;",
            "Landroid/view/ViewGroup;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    const-string v0, "$this$createTwoTabToggleRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 428
    sget v0, Lcom/squareup/salesreport/impl/R$layout;->sales_report_two_toggle_row:I

    .line 684
    new-instance v1, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1;

    invoke-direct {v1, v0, p1}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$createTwoTabToggleRow$$inlined$create$1;-><init>(ILcom/squareup/util/Res;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method private static final currentLabel(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 357
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;

    if-nez v0, :cond_1

    .line 358
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;

    check-cast p0, Lcom/squareup/customreport/data/RangeSelection;

    invoke-static {p0}, Lcom/squareup/salesreport/util/RangeSelectionsKt;->getDescription(Lcom/squareup/customreport/data/RangeSelection;)I

    move-result p0

    .line 357
    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 358
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type com.squareup.customreport.data.RangeSelection.PresetRangeSelection"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 360
    :cond_1
    invoke-static {p0, p3, p1, p2}, Lcom/squareup/salesreport/util/ReportConfigsKt;->dateRangeDescription(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private static final domainDescription(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lorg/threeten/bp/LocalDateTime;Z)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            "Lorg/threeten/bp/LocalDateTime;",
            "Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    if-eqz p5, :cond_0

    .line 326
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object p5

    invoke-virtual {p5}, Lcom/squareup/customreport/data/ReportConfig;->getComparisonRange()Lcom/squareup/customreport/data/ComparisonRange;

    move-result-object p5

    invoke-virtual {p4}, Lorg/threeten/bp/LocalDateTime;->toLocalDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-string v1, "dateTime.toLocalDate()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p5, v0}, Lcom/squareup/customreport/data/util/ComparisonRangesKt;->date(Lcom/squareup/customreport/data/ComparisonRange;Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p5

    goto :goto_0

    .line 328
    :cond_0
    invoke-virtual {p4}, Lorg/threeten/bp/LocalDateTime;->toLocalDate()Lorg/threeten/bp/LocalDate;

    move-result-object p5

    .line 331
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getSalesChart()Lcom/squareup/customreport/data/WithSalesChartReport;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/customreport/data/WithSalesChartReport;->getType()Lcom/squareup/customreport/data/SalesReportType$Charted;

    move-result-object p0

    .line 332
    sget-object v0, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p4}, Lorg/threeten/bp/LocalDateTime;->toLocalTime()Lorg/threeten/bp/LocalTime;

    move-result-object p0

    const-string p1, "dateTime.toLocalTime()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3, p0}, Lcom/squareup/salesreport/util/LocalTimeFormatter;->format(Lorg/threeten/bp/LocalTime;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 333
    :cond_1
    sget-object p3, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDayOfWeek;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDayOfWeek;

    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    const-string p4, "DateTimeFormatter\n      \u2026    .format(adjustedDate)"

    if-eqz p3, :cond_2

    .line 334
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_day_of_week:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p0

    .line 335
    invoke-interface {p2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Locale;

    invoke-virtual {p0, p1}, Lorg/threeten/bp/format/DateTimeFormatter;->withLocale(Ljava/util/Locale;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p0

    .line 336
    check-cast p5, Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-virtual {p0, p5}, Lorg/threeten/bp/format/DateTimeFormatter;->format(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 337
    :cond_2
    sget-object p3, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDay;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDay;

    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_3

    .line 338
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_month_day_year:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p0

    .line 339
    invoke-interface {p2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Locale;

    invoke-virtual {p0, p1}, Lorg/threeten/bp/format/DateTimeFormatter;->withLocale(Ljava/util/Locale;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p0

    .line 340
    check-cast p5, Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-virtual {p0, p5}, Lorg/threeten/bp/format/DateTimeFormatter;->format(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 341
    :cond_3
    sget-object p3, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByMonth;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByMonth;

    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 342
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_month_year:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p0

    .line 343
    invoke-interface {p2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Locale;

    invoke-virtual {p0, p1}, Lorg/threeten/bp/format/DateTimeFormatter;->withLocale(Ljava/util/Locale;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p0

    .line 344
    check-cast p5, Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-virtual {p0, p5}, Lorg/threeten/bp/format/DateTimeFormatter;->format(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p0

    :cond_4
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method private static final getTextColor(Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;)I
    .locals 0

    .line 642
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->getSubRow()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 643
    sget p0, Lcom/squareup/salesreport/impl/R$color;->sales_report_summary_details_subrow_text_color:I

    goto :goto_0

    .line 645
    :cond_0
    sget p0, Lcom/squareup/salesreport/impl/R$color;->sales_report_summary_details_row_text_color:I

    :goto_0
    return p0
.end method

.method private static final pickChartView(Lcom/squareup/customreport/data/WithSalesChartReport;Lcom/squareup/widgets/SquareViewAnimator;)Lcom/squareup/chartography/ChartView;
    .locals 1

    .line 400
    invoke-virtual {p0}, Lcom/squareup/customreport/data/WithSalesChartReport;->getType()Lcom/squareup/customreport/data/SalesReportType$Charted;

    move-result-object p0

    .line 401
    sget-object v0, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDayOfWeek;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDayOfWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByMonth;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByMonth;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    :goto_0
    sget p0, Lcom/squareup/salesreport/impl/R$id;->bar_chart:I

    goto :goto_1

    .line 402
    :cond_2
    sget p0, Lcom/squareup/salesreport/impl/R$id;->line_chart:I

    .line 399
    :goto_1
    invoke-virtual {p1, p0}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 405
    invoke-virtual {p1}, Lcom/squareup/widgets/SquareViewAnimator;->getCurrentView()Landroid/view/View;

    move-result-object p0

    if-eqz p0, :cond_3

    check-cast p0, Lcom/squareup/chartography/ChartView;

    return-object p0

    :cond_3
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type com.squareup.chartography.ChartView"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static final render(Lcom/squareup/salesreport/widget/AmountCell;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;)V
    .locals 1

    .line 628
    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;->getTitle()Lcom/squareup/util/ViewString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/salesreport/widget/AmountCell;->setValue(Lcom/squareup/util/ViewString;)V

    .line 629
    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;->getPercentageChange()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 630
    invoke-virtual {p0, v0}, Lcom/squareup/salesreport/widget/AmountCell;->setPercentage(Ljava/lang/String;)V

    .line 632
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;->getPercentageColor()Ljava/lang/Integer;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    .line 633
    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/widget/AmountCell;->setPercentageColorRes(I)V

    :cond_1
    return-void
.end method

.method private static final scrubbingDescription(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lorg/threeten/bp/LocalDateTime;Z)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            "Lorg/threeten/bp/LocalDateTime;",
            "Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 281
    invoke-static/range {p0 .. p5}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->domainDescription(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lorg/threeten/bp/LocalDateTime;Z)Ljava/lang/String;

    move-result-object p4

    .line 283
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getSalesChart()Lcom/squareup/customreport/data/WithSalesChartReport;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/customreport/data/WithSalesChartReport;->getType()Lcom/squareup/customreport/data/SalesReportType$Charted;

    move-result-object v0

    invoke-static {v0, p5}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->scrubbingLabelFormat(Lcom/squareup/customreport/data/SalesReportType$Charted;Z)I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 284
    check-cast p4, Ljava/lang/CharSequence;

    const-string v1, "domain"

    invoke-virtual {v0, v1, p4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p4

    if-eqz p5, :cond_0

    .line 288
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object p0

    invoke-static {p0, p1, p2}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->compareLabel(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 290
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object p0

    invoke-static {p0, p1, p2, p3}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->currentLabel(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;)Ljava/lang/String;

    move-result-object p0

    .line 287
    :goto_0
    check-cast p0, Ljava/lang/CharSequence;

    const-string p1, "label"

    .line 285
    invoke-virtual {p4, p1, p0}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 293
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 294
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static final scrubbingLabelFormat(Lcom/squareup/customreport/data/SalesReportType$Charted;Z)I
    .locals 1

    .line 303
    sget-object v0, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_chart_scrub_label_hour_format:I

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    .line 305
    sget-object p1, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDayOfWeek;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDayOfWeek;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 306
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_chart_scrub_compare_label_weekly_format:I

    goto :goto_0

    .line 308
    :cond_1
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_chart_scrub_label_date_format:I

    :goto_0
    return p0
.end method

.method private static final setSelection(Lcom/squareup/noho/NohoLabel;Lcom/squareup/util/Res;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 610
    sget-object p2, Lcom/squareup/noho/NohoLabel$Type;->LABEL:Lcom/squareup/noho/NohoLabel$Type;

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoLabel;->apply(Lcom/squareup/noho/NohoLabel$Type;)V

    .line 611
    sget p2, Lcom/squareup/salesreport/impl/R$color;->sales_report_amount_count_toggle_text_color_selected:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p2

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoLabel;->setTextColor(I)V

    .line 613
    sget p2, Lcom/squareup/salesreport/impl/R$color;->sales_report_amount_count_toggle_background_color_selected:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p1

    .line 612
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoLabel;->setBackgroundColor(I)V

    goto :goto_0

    .line 616
    :cond_0
    sget-object p2, Lcom/squareup/noho/NohoLabel$Type;->LABEL_2:Lcom/squareup/noho/NohoLabel$Type;

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoLabel;->apply(Lcom/squareup/noho/NohoLabel$Type;)V

    .line 617
    sget p2, Lcom/squareup/salesreport/impl/R$color;->sales_report_amount_count_toggle_text_color_unselected:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p2

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoLabel;->setTextColor(I)V

    .line 619
    sget p2, Lcom/squareup/salesreport/impl/R$color;->sales_report_amount_count_toggle_background_color_unselected:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p1

    .line 618
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoLabel;->setBackgroundColor(I)V

    :goto_0
    return-void
.end method

.method public static final textFromChartSalesSelection(JLcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    const-string v0, "chartSalesSelection"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "numberFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 265
    sget-object v0, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    if-eq p2, v0, :cond_1

    const/4 p3, 0x3

    if-ne p2, p3, :cond_0

    .line 267
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-interface {p5, p0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p0

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 266
    :cond_1
    new-instance p2, Lcom/squareup/protos/common/Money;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-direct {p2, p0, p3}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-interface {p4, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p0

    :goto_0
    return-object p0
.end method
