.class public final synthetic Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$10:[I

.field public static final synthetic $EnumSwitchMapping$11:[I

.field public static final synthetic $EnumSwitchMapping$2:[I

.field public static final synthetic $EnumSwitchMapping$3:[I

.field public static final synthetic $EnumSwitchMapping$4:[I

.field public static final synthetic $EnumSwitchMapping$5:[I

.field public static final synthetic $EnumSwitchMapping$6:[I

.field public static final synthetic $EnumSwitchMapping$7:[I

.field public static final synthetic $EnumSwitchMapping$8:[I

.field public static final synthetic $EnumSwitchMapping$9:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 13

    invoke-static {}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->IMPERIAL_SQUARE_INCH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->IMPERIAL_ACRE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->IMPERIAL_SQUARE_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->IMPERIAL_SQUARE_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->IMPERIAL_SQUARE_MILE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result v1

    const/4 v6, 0x5

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->METRIC_SQUARE_CENTIMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result v1

    const/4 v7, 0x6

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->METRIC_SQUARE_METER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result v1

    const/4 v8, 0x7

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->METRIC_SQUARE_KILOMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result v1

    const/16 v9, 0x8

    aput v9, v0, v1

    invoke-static {}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_INCH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_MILE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_MILLIMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_CENTIMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result v1

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_METER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result v1

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_KILOMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result v1

    aput v9, v0, v1

    invoke-static {}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_DAY:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_MILLISECOND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_SECOND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_MINUTE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_HOUR:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->ordinal()I

    move-result v1

    aput v6, v0, v1

    invoke-static {}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_WEIGHT_OUNCE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_POUND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_STONE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_MILLIGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_GRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_KILOGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ordinal()I

    move-result v1

    aput v7, v0, v1

    invoke-static {}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_FLUID_OUNCE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_SHOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_CUP:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_PINT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_QUART:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_GALLON:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_INCH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v9, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    const/16 v10, 0x9

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->METRIC_MILLILITER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    const/16 v11, 0xa

    aput v11, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->METRIC_LITER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    const/16 v12, 0xb

    aput v12, v0, v1

    invoke-static {}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->ordinal()I

    move-result v1

    aput v2, v0, v1

    invoke-static {}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->IMPERIAL_SQUARE_INCH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->IMPERIAL_ACRE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->IMPERIAL_SQUARE_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->IMPERIAL_SQUARE_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->IMPERIAL_SQUARE_MILE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->METRIC_SQUARE_CENTIMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result v1

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->METRIC_SQUARE_METER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result v1

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->METRIC_SQUARE_KILOMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result v1

    aput v9, v0, v1

    invoke-static {}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$7:[I

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$7:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_INCH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$7:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$7:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$7:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_MILE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$7:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_MILLIMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$7:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_CENTIMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result v1

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$7:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_METER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result v1

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$7:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_KILOMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result v1

    aput v9, v0, v1

    invoke-static {}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$8:[I

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$8:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_DAY:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$8:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_MILLISECOND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$8:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_SECOND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$8:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_MINUTE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$8:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_HOUR:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->ordinal()I

    move-result v1

    aput v6, v0, v1

    invoke-static {}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$9:[I

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$9:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_WEIGHT_OUNCE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$9:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_POUND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$9:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_STONE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$9:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_MILLIGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$9:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_GRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$9:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_KILOGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ordinal()I

    move-result v1

    aput v7, v0, v1

    invoke-static {}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$10:[I

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$10:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_FLUID_OUNCE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$10:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_SHOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$10:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_CUP:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$10:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_PINT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$10:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_QUART:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$10:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_GALLON:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$10:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_INCH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$10:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v9, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$10:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$10:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->METRIC_MILLILITER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v11, v0, v1

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$10:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->METRIC_LITER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result v1

    aput v12, v0, v1

    invoke-static {}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$11:[I

    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$11:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->ordinal()I

    move-result v1

    aput v2, v0, v1

    return-void
.end method
