.class public final Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;
.super Ljava/lang/Object;
.source "StandardMeasurementUnitLocalizationHelper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardMeasurementUnitLocalizationHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardMeasurementUnitLocalizationHelper.kt\ncom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt\n*L\n1#1,302:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00082\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\t2\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\n2\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u000b2\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u000c2\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u0012\u0010\r\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u0012\u0010\r\u001a\u00020\u0001*\u00020\u00072\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u0012\u0010\r\u001a\u00020\u0001*\u00020\u00082\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u0012\u0010\r\u001a\u00020\u0001*\u00020\t2\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u0012\u0010\r\u001a\u00020\u0001*\u00020\n2\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u0012\u0010\r\u001a\u00020\u0001*\u00020\u000b2\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u0012\u0010\r\u001a\u00020\u0001*\u00020\u000c2\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u000e"
    }
    d2 = {
        "localizedAbbreviation",
        "",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
        "resources",
        "Landroid/content/res/Resources;",
        "res",
        "Lcom/squareup/util/Res;",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;",
        "localizedName",
        "proto-utilities_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$localizedAbbreviation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    const-string p0, ""

    goto :goto_0

    .line 99
    :pswitch_0
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_area_metricSquareKilometer:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 97
    :pswitch_1
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_area_metricSquareMeter:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 95
    :pswitch_2
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_area_metricSquareCentimeter:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 93
    :pswitch_3
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_area_imperialSquareMile:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 91
    :pswitch_4
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_area_imperialSquareYard:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 89
    :pswitch_5
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_area_imperialSquareFoot:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 87
    :pswitch_6
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_area_imperialAcre:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 85
    :pswitch_7
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_area_imperialSquareInch:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$localizedAbbreviation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    sget-object p1, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$5:[I

    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->ordinal()I

    move-result p0

    aget p0, p1, p0

    const/4 p1, 0x1

    const-string p0, ""

    return-object p0
.end method

.method public static final localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$localizedAbbreviation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    const-string p0, ""

    goto :goto_0

    .line 121
    :pswitch_0
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_length_metricKilometer:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 119
    :pswitch_1
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_length_metricMeter:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 117
    :pswitch_2
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_length_metricCentimeter:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 115
    :pswitch_3
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_length_metricMillimeter:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 113
    :pswitch_4
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_length_imperialMile:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 111
    :pswitch_5
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_length_imperialYard:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 109
    :pswitch_6
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_length_imperialFoot:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 107
    :pswitch_7
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_length_imperialInch:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$localizedAbbreviation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const-string p0, ""

    goto :goto_0

    .line 133
    :cond_0
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_time_genericHour:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 132
    :cond_1
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_time_genericMinute:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 131
    :cond_2
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_time_genericSecond:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 129
    :cond_3
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_time_genericMillisecond:I

    .line 128
    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 127
    :cond_4
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_time_genericDay:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static final localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$localizedAbbreviation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$4:[I

    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    const-string p0, ""

    goto :goto_0

    .line 178
    :pswitch_0
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_volume_metricLiter:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 176
    :pswitch_1
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_volume_metricMilliliter:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 174
    :pswitch_2
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_volume_imperialCubicYard:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 172
    :pswitch_3
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_volume_imperialCubicFoot:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 170
    :pswitch_4
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_volume_imperialCubicInch:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 168
    :pswitch_5
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_volume_genericGallon:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 166
    :pswitch_6
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_volume_genericQuart:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 164
    :pswitch_7
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_volume_genericPint:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 162
    :pswitch_8
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_volume_genericCup:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 160
    :pswitch_9
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_volume_genericShot:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 158
    :pswitch_a
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_volume_genericFluidOunce:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$localizedAbbreviation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    const-string p0, ""

    goto :goto_0

    .line 150
    :pswitch_0
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_weight_metricKilogram:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 148
    :pswitch_1
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_weight_metricGram:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 146
    :pswitch_2
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_weight_metricMilligram:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 144
    :pswitch_3
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_weight_imperialStone:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 142
    :pswitch_4
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_weight_imperialPound:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 140
    :pswitch_5
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitAbbreviation_weight_imperialWeightOunce:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$localizedAbbreviation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    new-instance v0, Lcom/squareup/util/Res$RealRes;

    invoke-direct {v0, p1}, Lcom/squareup/util/Res$RealRes;-><init>(Landroid/content/res/Resources;)V

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p0, v0}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$localizedAbbreviation"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    const-string v1, ""

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;->abbreviation:Ljava/lang/String;

    if-eqz p0, :cond_6

    move-object v1, p0

    goto :goto_0

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    if-eqz v0, :cond_1

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    const-string v0, "it.area_unit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    if-eqz v0, :cond_2

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    const-string v0, "it.length_unit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 60
    :cond_2
    iget-object v0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    if-eqz v0, :cond_3

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    const-string v0, "it.time_unit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 61
    :cond_3
    iget-object v0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    if-eqz v0, :cond_4

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    const-string v0, "it.weight_unit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 62
    :cond_4
    iget-object v0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    if-eqz v0, :cond_5

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    const-string v0, "it.volume_unit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 63
    :cond_5
    iget-object v0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    if-eqz v0, :cond_6

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    const-string v0, "it.generic_unit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    :cond_6
    :goto_0
    return-object v1
.end method

.method public static final localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$localizedName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    invoke-static {}, Lcom/squareup/quantity/ItemQuantities;->getUNKNOWN_UNIT_NAME_ID()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 192
    sget-object v1, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$6:[I

    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ordinal()I

    move-result p0

    aget p0, v1, p0

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 208
    :pswitch_0
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_area_metricSquareKilometer:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 206
    :pswitch_1
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_area_metricSquareMeter:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 204
    :pswitch_2
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_area_metricSquareCentimeter:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 202
    :pswitch_3
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_area_imperialSquareMile:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 200
    :pswitch_4
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_area_imperialSquareYard:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 198
    :pswitch_5
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_area_imperialSquareFoot:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 196
    :pswitch_6
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_area_imperialAcre:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 194
    :pswitch_7
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_area_imperialSquareInch:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$localizedName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 296
    invoke-static {}, Lcom/squareup/quantity/ItemQuantities;->getGENERIC_UNIT_NAME_ID()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 297
    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$11:[I

    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const-string p1, ""

    :cond_0
    return-object p1
.end method

.method public static final localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$localizedName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    invoke-static {}, Lcom/squareup/quantity/ItemQuantities;->getUNKNOWN_UNIT_NAME_ID()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 215
    sget-object v1, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$7:[I

    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ordinal()I

    move-result p0

    aget p0, v1, p0

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 231
    :pswitch_0
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_length_metricKilometer:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 229
    :pswitch_1
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_length_metricMeter:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 227
    :pswitch_2
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_length_metricCentimeter:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 225
    :pswitch_3
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_length_metricMillimeter:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 223
    :pswitch_4
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_length_imperialMile:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 221
    :pswitch_5
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_length_imperialYard:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 219
    :pswitch_6
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_length_imperialFoot:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 217
    :pswitch_7
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_length_imperialInch:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$localizedName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 236
    sget-object v0, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$8:[I

    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 244
    invoke-static {}, Lcom/squareup/quantity/ItemQuantities;->getUNKNOWN_UNIT_NAME_ID()I

    move-result p0

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 243
    :cond_0
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_time_genericHour:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 242
    :cond_1
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_time_genericMinute:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 241
    :cond_2
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_time_genericSecond:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 239
    :cond_3
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_time_genericMillisecond:I

    .line 238
    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 237
    :cond_4
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_time_genericDay:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static final localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$localizedName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 267
    invoke-static {}, Lcom/squareup/quantity/ItemQuantities;->getUNKNOWN_UNIT_NAME_ID()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 268
    sget-object v1, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$10:[I

    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ordinal()I

    move-result p0

    aget p0, v1, p0

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 290
    :pswitch_0
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_volume_metricLiter:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 288
    :pswitch_1
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_volume_metricMilliliter:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 286
    :pswitch_2
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_volume_imperialCubicYard:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 284
    :pswitch_3
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_volume_imperialCubicFoot:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 282
    :pswitch_4
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_volume_imperialCubicInch:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 280
    :pswitch_5
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_volume_genericGallon:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 278
    :pswitch_6
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_volume_genericQuart:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 276
    :pswitch_7
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_volume_genericPint:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 274
    :pswitch_8
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_volume_genericCup:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 272
    :pswitch_9
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_volume_genericShot:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 270
    :pswitch_a
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_volume_genericFluidOunce:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$localizedName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    invoke-static {}, Lcom/squareup/quantity/ItemQuantities;->getUNKNOWN_UNIT_NAME_ID()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 249
    sget-object v1, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt$WhenMappings;->$EnumSwitchMapping$9:[I

    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ordinal()I

    move-result p0

    aget p0, v1, p0

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 261
    :pswitch_0
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_weight_metricKilogram:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 259
    :pswitch_1
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_weight_metricGram:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 257
    :pswitch_2
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_weight_metricMilligram:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 255
    :pswitch_3
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_weight_imperialStone:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 253
    :pswitch_4
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_weight_imperialPound:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 251
    :pswitch_5
    sget p0, Lcom/squareup/proto_utilities/R$string;->catalogMeasurementUnitName_weight_imperialWeightOunce:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$localizedName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-static {}, Lcom/squareup/quantity/ItemQuantities;->getUNKNOWN_UNIT_NAME_ID()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    if-eqz v1, :cond_1

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    iget-object v0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 72
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    if-eqz v1, :cond_2

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    const-string v0, "it.area_unit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 73
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    if-eqz v1, :cond_3

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    const-string v0, "it.length_unit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 74
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    if-eqz v1, :cond_4

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    const-string v0, "it.weight_unit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 75
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    if-eqz v1, :cond_5

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    const-string v0, "it.volume_unit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 76
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    if-eqz v1, :cond_6

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    const-string v0, "it.generic_unit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 77
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    if-eqz v1, :cond_7

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    const-string v0, "it.time_unit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    :cond_7
    :goto_0
    return-object v0
.end method
