.class final Lcom/squareup/printer/epson/RealEpsonPrinterSdk$setReceiveEventListener$1;
.super Ljava/lang/Object;
.source "RealEpsonPrinterSdk.kt"

# interfaces
.implements Lcom/epson/epos2/printer/ReceiveListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/printer/epson/RealEpsonPrinterSdk;->setReceiveEventListener(Lkotlin/jvm/functions/Function3;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u000e\u0010\u0007\u001a\n \u0004*\u0004\u0018\u00010\u00080\u00082\u000e\u0010\t\u001a\n \u0004*\u0004\u0018\u00010\n0\nH\n\u00a2\u0006\u0002\u0008\u000b"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/epson/epos2/printer/Printer;",
        "kotlin.jvm.PlatformType",
        "code",
        "",
        "status",
        "Lcom/epson/epos2/printer/PrinterStatusInfo;",
        "printJobId",
        "",
        "onPtrReceive"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $receiveEventListener:Lkotlin/jvm/functions/Function3;

.field final synthetic this$0:Lcom/squareup/printer/epson/RealEpsonPrinterSdk;


# direct methods
.method constructor <init>(Lcom/squareup/printer/epson/RealEpsonPrinterSdk;Lkotlin/jvm/functions/Function3;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk$setReceiveEventListener$1;->this$0:Lcom/squareup/printer/epson/RealEpsonPrinterSdk;

    iput-object p2, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk$setReceiveEventListener$1;->$receiveEventListener:Lkotlin/jvm/functions/Function3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPtrReceive(Lcom/epson/epos2/printer/Printer;ILcom/epson/epos2/printer/PrinterStatusInfo;Ljava/lang/String;)V
    .locals 1

    .line 86
    iget-object p1, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk$setReceiveEventListener$1;->$receiveEventListener:Lkotlin/jvm/functions/Function3;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/printer/epson/RealEpsonPrinterSdk$setReceiveEventListener$1;->this$0:Lcom/squareup/printer/epson/RealEpsonPrinterSdk;

    invoke-static {p3}, Lcom/squareup/printer/epson/RealEpsonPrinterSdk;->access$getActualEpsonPrinterSdk$p(Lcom/squareup/printer/epson/RealEpsonPrinterSdk;)Lcom/epson/epos2/printer/Printer;

    move-result-object p3

    invoke-virtual {p3}, Lcom/epson/epos2/printer/Printer;->getStatus()Lcom/epson/epos2/printer/PrinterStatusInfo;

    move-result-object p3

    const-string v0, "actualEpsonPrinterSdk.status"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p3}, Lcom/squareup/printer/epson/RealEpsonPrinterSdkKt;->access$toPrinterStatusInfo(Lcom/epson/epos2/printer/PrinterStatusInfo;)Lcom/squareup/printer/epson/PrinterStatusInfo;

    move-result-object p3

    invoke-interface {p1, p2, p3, p4}, Lkotlin/jvm/functions/Function3;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
