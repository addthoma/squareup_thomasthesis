.class public final Lcom/squareup/printer/epson/EpsonPrinterModule$Companion;
.super Ljava/lang/Object;
.source "EpsonPrinterModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/printer/epson/EpsonPrinterModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0087\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008H\u0007J(\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J(\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/printer/epson/EpsonPrinterModule$Companion;",
        "",
        "()V",
        "provideEpsonPrinterConnection",
        "Lcom/squareup/printer/epson/EpsonPrinterConnection;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "coroutineScope",
        "Lkotlinx/coroutines/CoroutineScope;",
        "provideEpsonTcpPrinterScout",
        "Lcom/squareup/print/EpsonTcpPrinterScout;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "epsonDiscoverer",
        "Lcom/squareup/printer/epson/EpsonDiscoverer;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "provideEpsonUsbPrinterScout",
        "Lcom/squareup/print/EpsonUsbPrinterScout;",
        "usbManager",
        "Lcom/squareup/hardware/usb/UsbManager;",
        "epsonPrinterFactory",
        "Lcom/squareup/printer/epson/EpsonPrinter$Factory;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/printer/epson/EpsonPrinterModule$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideEpsonPrinterConnection(Lcom/squareup/settings/server/Features;Lkotlinx/coroutines/CoroutineScope;)Lcom/squareup/printer/epson/EpsonPrinterConnection;
    .locals 1
    .param p2    # Lkotlinx/coroutines/CoroutineScope;
        .annotation runtime Lcom/squareup/print/PrinterThread;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "coroutineScope"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinterConnection;

    invoke-direct {v0, p1, p2}, Lcom/squareup/printer/epson/EpsonPrinterConnection;-><init>(Lcom/squareup/settings/server/Features;Lkotlinx/coroutines/CoroutineScope;)V

    return-object v0
.end method

.method public final provideEpsonTcpPrinterScout(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/printer/epson/EpsonDiscoverer;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;)Lcom/squareup/print/EpsonTcpPrinterScout;
    .locals 7
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/LoggedInScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "mainThread"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "epsonDiscoverer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    new-instance v0, Lcom/squareup/print/EpsonTcpPrinterScout;

    const-string v1, "Sq-EPSON PRINTER TCP SCOUT"

    const/16 v2, 0xa

    const/4 v3, 0x1

    .line 33
    invoke-static {v1, v2, v3}, Lcom/squareup/thread/executor/Executors;->stoppableNamedThreadExecutor(Ljava/lang/String;IZ)Lcom/squareup/thread/executor/StoppableSerialExecutor;

    move-result-object v2

    const-string v1, "stoppableNamedThreadExec\u2026         true\n          )"

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    .line 32
    invoke-direct/range {v1 .. v6}, Lcom/squareup/print/EpsonTcpPrinterScout;-><init>(Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/printer/epson/EpsonDiscoverer;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method

.method public final provideEpsonUsbPrinterScout(Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/printer/epson/EpsonPrinter$Factory;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;)Lcom/squareup/print/EpsonUsbPrinterScout;
    .locals 7
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/LoggedInScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "usbManager"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "epsonPrinterFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    new-instance v0, Lcom/squareup/print/EpsonUsbPrinterScout;

    const-string v1, "Sq-EPSON PRINTER USB SCOUT"

    const/16 v2, 0xa

    const/4 v3, 0x1

    .line 55
    invoke-static {v1, v2, v3}, Lcom/squareup/thread/executor/Executors;->stoppableNamedThreadExecutor(Ljava/lang/String;IZ)Lcom/squareup/thread/executor/StoppableSerialExecutor;

    move-result-object v4

    const-string v1, "stoppableNamedThreadExec\u2026KGROUND, true\n          )"

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    move-object v6, p4

    .line 52
    invoke-direct/range {v1 .. v6}, Lcom/squareup/print/EpsonUsbPrinterScout;-><init>(Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/printer/epson/EpsonPrinter$Factory;Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method
