.class public final Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeViewFactory;
.super Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeViewFactory;
.source "RealRedeemRewardByCodeViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeViewFactory;",
        "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeViewFactory;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 21
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 10
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 11
    sget-object v2, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeScreen;->Companion:Lcom/squareup/redeemrewards/bycode/EnterRewardCodeScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeScreen$Companion;->getKEY$impl_release()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 12
    sget v3, Lcom/squareup/redeemrewards/bycode/impl/R$layout;->enter_reward_code_screen:I

    .line 13
    sget-object v4, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeViewFactory$1;->INSTANCE:Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeViewFactory$1;

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 10
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 16
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 17
    sget-object v1, Lcom/squareup/redeemrewards/bycode/LoadingRewardsScreen;->Companion:Lcom/squareup/redeemrewards/bycode/LoadingRewardsScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/bycode/LoadingRewardsScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 18
    sget v5, Lcom/squareup/redeemrewards/bycode/impl/R$layout;->loading_rewards_screen:I

    .line 19
    sget-object v1, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeViewFactory$2;->INSTANCE:Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeViewFactory$2;

    move-object v8, v1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 20
    new-instance v1, Lcom/squareup/workflow/ScreenHint;

    sget-object v15, Lcom/squareup/container/spot/Spots;->HERE_CROSS_FADE:Lcom/squareup/container/spot/Spot;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x1df

    const/16 v20, 0x0

    move-object v9, v1

    invoke-direct/range {v9 .. v20}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v7, 0x0

    const/16 v9, 0x8

    move-object v6, v1

    .line 16
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 23
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 24
    sget-object v1, Lcom/squareup/redeemrewards/bycode/NoRewardScreen;->Companion:Lcom/squareup/redeemrewards/bycode/NoRewardScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/bycode/NoRewardScreen$Companion;->getKEY$impl_release()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 25
    sget v5, Lcom/squareup/redeemrewards/bycode/impl/R$layout;->no_reward_screen:I

    .line 26
    sget-object v1, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeViewFactory$3;->INSTANCE:Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeViewFactory$3;

    move-object v8, v1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x0

    const/16 v9, 0xc

    .line 23
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 29
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 30
    sget-object v1, Lcom/squareup/redeemrewards/bycode/DisplayRewardScreen;->Companion:Lcom/squareup/redeemrewards/bycode/DisplayRewardScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/bycode/DisplayRewardScreen$Companion;->getKEY$impl_release()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 31
    sget v5, Lcom/squareup/redeemrewards/bycode/impl/R$layout;->display_reward_screen:I

    .line 32
    sget-object v1, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeViewFactory$4;->INSTANCE:Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeViewFactory$4;

    move-object v8, v1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 29
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    move-object/from16 v1, p0

    .line 9
    invoke-direct {v1, v0}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
