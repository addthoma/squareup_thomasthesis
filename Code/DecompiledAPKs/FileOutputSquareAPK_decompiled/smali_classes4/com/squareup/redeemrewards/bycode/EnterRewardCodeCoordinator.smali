.class public final Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EnterRewardCodeCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEnterRewardCodeCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EnterRewardCodeCoordinator.kt\ncom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator\n*L\n1#1,70:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B)\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0014\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u0005H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/redeemrewards/bycode/EnterRewardCodeScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "codeInput",
        "Lcom/squareup/widgets/DigitInputView;",
        "hintText",
        "Landroid/widget/TextView;",
        "view",
        "Landroid/view/View;",
        "attach",
        "",
        "bindViews",
        "onScreen",
        "screen",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private codeInput:Lcom/squareup/widgets/DigitInputView;

.field private hintText:Landroid/widget/TextView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$onScreen(Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;Lcom/squareup/redeemrewards/bycode/EnterRewardCodeScreen;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;->onScreen(Lcom/squareup/redeemrewards/bycode/EnterRewardCodeScreen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 44
    iput-object p1, p0, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;->view:Landroid/view/View;

    .line 45
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 46
    sget v0, Lcom/squareup/redeemrewards/bycode/impl/R$id;->crm_reward_code_digits:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/DigitInputView;

    iput-object v0, p0, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;->codeInput:Lcom/squareup/widgets/DigitInputView;

    .line 47
    sget v0, Lcom/squareup/redeemrewards/bycode/impl/R$id;->crm_reward_code_description:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;->hintText:Landroid/widget/TextView;

    return-void
.end method

.method private final onScreen(Lcom/squareup/redeemrewards/bycode/EnterRewardCodeScreen;)V
    .locals 4

    .line 56
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 53
    :cond_0
    iget-object v2, p0, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 54
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator$onScreen$1;

    invoke-direct {v3, p1}, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator$onScreen$1;-><init>(Lcom/squareup/redeemrewards/bycode/EnterRewardCodeScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 55
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/redeemrewards/bycode/impl/R$string;->enter_reward_code_title:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 56
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;->view:Landroid/view/View;

    if-nez v0, :cond_2

    const-string/jumbo v1, "view"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v1, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator$onScreen$2;

    invoke-direct {v1, p1}, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator$onScreen$2;-><init>(Lcom/squareup/redeemrewards/bycode/EnterRewardCodeScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 59
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeScreen;->getAlphanumericAllowed()Z

    move-result v0

    const-string v1, "codeInput"

    if-eqz v0, :cond_5

    .line 60
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;->codeInput:Lcom/squareup/widgets/DigitInputView;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0}, Lcom/squareup/widgets/DigitInputView;->setAlphaNumeric()V

    .line 61
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;->hintText:Landroid/widget/TextView;

    if-nez v0, :cond_4

    const-string v2, "hintText"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget v2, Lcom/squareup/redeemrewards/bycode/impl/R$string;->enter_reward_code_hint_alphanumeric:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 64
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeScreen;->getCode()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-nez p1, :cond_6

    const/4 p1, 0x1

    goto :goto_0

    :cond_6
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_9

    .line 65
    iget-object p1, p0, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;->codeInput:Lcom/squareup/widgets/DigitInputView;

    if-nez p1, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p1}, Lcom/squareup/widgets/DigitInputView;->clearDigits()V

    .line 66
    iget-object p1, p0, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;->codeInput:Lcom/squareup/widgets/DigitInputView;

    if-nez p1, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p1}, Lcom/squareup/widgets/DigitInputView;->showKeyboard()V

    :cond_9
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;->bindViews(Landroid/view/View;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;->screens:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator$attach$unwrappedScreens$1;->INSTANCE:Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator$attach$unwrappedScreens$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;->codeInput:Lcom/squareup/widgets/DigitInputView;

    if-nez v1, :cond_0

    const-string v2, "codeInput"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/widgets/DigitInputView;->digits()Lio/reactivex/Observable;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 36
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Observable;->skip(J)Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "codeInput.digits()\n        .skip(1)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "unwrappedScreens"

    .line 37
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, v0

    check-cast v2, Lio/reactivex/ObservableSource;

    invoke-static {v1, v2}, Lcom/squareup/util/rx2/RxKotlinKt;->withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v1

    .line 38
    sget-object v2, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator$attach$1;->INSTANCE:Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator$attach$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, p1, v2}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 40
    new-instance v1, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator$attach$2;

    move-object v2, p0

    check-cast v2, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;

    invoke-direct {v1, v2}, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator$attach$2;-><init>(Lcom/squareup/redeemrewards/bycode/EnterRewardCodeCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
