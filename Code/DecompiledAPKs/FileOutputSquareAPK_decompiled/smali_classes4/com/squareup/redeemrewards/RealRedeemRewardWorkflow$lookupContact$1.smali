.class final Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$lookupContact$1;
.super Ljava/lang/Object;
.source "RealRedeemRewardWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->lookupContact(Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a(\u0012$\u0012\"\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004 \u0005*\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00020\u00020\u00012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/redeemrewards/RedeemRewardState;",
        "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
        "kotlin.jvm.PlatformType",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;

.field final synthetic this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$lookupContact$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    iput-object p2, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$lookupContact$1;->$state:Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
            ">;>;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 224
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_0

    .line 225
    iget-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$lookupContact$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    .line 226
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$lookupContact$1;->$state:Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;

    invoke-virtual {v0}, Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;->getContactToken()Ljava/lang/String;

    move-result-object v0

    .line 227
    iget-object v1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$lookupContact$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    invoke-static {v1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->access$getRes$p(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;)Lcom/squareup/util/Res;

    move-result-object v1

    sget v2, Lcom/squareup/redeemrewards/impl/R$string;->redeem_rewards_contacts_error_message:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 225
    invoke-static {p1, v0, v1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->access$lookupContactFailedAction(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    .line 224
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(\n           \u2026          )\n            )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 230
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$lookupContact$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$lookupContact$1;->$state:Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/GetContactResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/GetContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    const-string v2, "successOrFailure.response.contact"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->access$loyaltyGetStatusForContact(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;Lcom/squareup/protos/client/rolodex/Contact;)Lio/reactivex/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$lookupContact$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
