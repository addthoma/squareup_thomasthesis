.class final Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$12;
.super Lkotlin/jvm/internal/Lambda;
.source "RealRedeemRewardWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->render(Lcom/squareup/redeemrewards/RedeemRewardProps;Lcom/squareup/redeemrewards/RedeemRewardState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/redeemrewards/RedeemRewardState;",
        "+",
        "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0002`\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/redeemrewards/RedeemRewardState;",
        "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
        "Lcom/squareup/redeemrewards/Action;",
        "output",
        "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/redeemrewards/RedeemRewardState;

.field final synthetic this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$12;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    iput-object p2, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$12;->$state:Lcom/squareup/redeemrewards/RedeemRewardState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/redeemrewards/ViewRewardsOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$12;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$12;->$state:Lcom/squareup/redeemrewards/RedeemRewardState;

    check-cast v1, Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    invoke-static {v0, v1, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->access$processViewRewardsOutput(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;Lcom/squareup/redeemrewards/ViewRewardsOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$12;->invoke(Lcom/squareup/redeemrewards/ViewRewardsOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
