.class public final Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner;
.super Ljava/lang/Object;
.source "RedeemRewardsErrorLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/redeemrewards/RedeemRewardsErrorScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0010B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/redeemrewards/RedeemRewardsErrorScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "messageLabel",
        "Landroid/widget/TextView;",
        "kotlin.jvm.PlatformType",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Binding",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final messageLabel:Landroid/widget/TextView;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner;->view:Landroid/view/View;

    .line 19
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 20
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/redeemrewards/impl/R$id;->errorMessageLabel:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner;->messageLabel:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/redeemrewards/RedeemRewardsErrorScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 3

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object p2, p0, Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 26
    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 28
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsErrorScreen;->getBackButtonIsX()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29
    sget-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    goto :goto_0

    .line 31
    :cond_0
    sget-object v1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    .line 33
    :goto_0
    new-instance v2, Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner$showRendering$1;

    invoke-direct {v2, p1}, Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner$showRendering$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsErrorScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 27
    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 34
    new-instance v1, Lcom/squareup/resources/FixedText;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsErrorScreen;->getTitle()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-direct {v1, v2}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 37
    iget-object p2, p0, Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner$showRendering$2;

    invoke-direct {v0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner$showRendering$2;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsErrorScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 39
    iget-object p2, p0, Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner;->messageLabel:Landroid/widget/TextView;

    const-string v0, "messageLabel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsErrorScreen;->getMessage()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/redeemrewards/RedeemRewardsErrorScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/redeemrewards/RedeemRewardsErrorLayoutRunner;->showRendering(Lcom/squareup/redeemrewards/RedeemRewardsErrorScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
