.class public final Lcom/squareup/redeemrewards/ViewRewardsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ViewRewardsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/ViewRewardsCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViewRewardsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ViewRewardsCoordinator.kt\ncom/squareup/redeemrewards/ViewRewardsCoordinator\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 5 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n*L\n1#1,301:1\n49#2:302\n50#2,3:308\n53#2:323\n49#2:324\n50#2,3:330\n53#2:345\n599#3,4:303\n601#3:307\n599#3,4:325\n601#3:329\n310#4,3:311\n313#4,3:320\n310#4,3:333\n313#4,3:342\n35#5,6:314\n35#5,6:336\n*E\n*S KotlinDebug\n*F\n+ 1 ViewRewardsCoordinator.kt\ncom/squareup/redeemrewards/ViewRewardsCoordinator\n*L\n190#1:302\n190#1,3:308\n190#1:323\n218#1:324\n218#1,3:330\n218#1:345\n190#1,4:303\n190#1:307\n218#1,4:325\n218#1:329\n190#1,3:311\n190#1,3:320\n218#1,3:333\n218#1,3:342\n190#1,6:314\n218#1,6:336\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0001:B1\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010#\u001a\u00020\u00102\u0006\u0010\"\u001a\u00020\u0016H\u0016J\u0010\u0010$\u001a\u00020\u00102\u0006\u0010\"\u001a\u00020\u0016H\u0002J\u0018\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020*H\u0002J\u0010\u0010+\u001a\u00020\u00102\u0006\u0010,\u001a\u00020\u0005H\u0002J\u0010\u0010-\u001a\u00020\u00102\u0006\u0010.\u001a\u00020/H\u0002J\u0008\u00100\u001a\u00020\u0010H\u0002J\u0016\u00101\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u00142\u0006\u00102\u001a\u00020\u001bH\u0002J\u0016\u00103\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u00142\u0006\u00102\u001a\u00020\u001bH\u0002J\u000c\u00104\u001a\u00020\u0010*\u00020\u001bH\u0002J\u0014\u00105\u001a\u00020\u0010*\u0002062\u0006\u00107\u001a\u00020\u0019H\u0002J\u0014\u00108\u001a\u00020\u0010*\u00020\u00162\u0006\u00109\u001a\u00020*H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00100\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00100\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006;"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/ViewRewardsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "couponClickedCallback",
        "Lkotlin/Function1;",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
        "",
        "couponEmptyLabel",
        "Landroid/widget/TextView;",
        "couponRecycler",
        "Lcom/squareup/cycler/Recycler;",
        "interactiveContent",
        "Landroid/view/View;",
        "loyaltyEmptyLabel",
        "loyaltyRecycler",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
        "loyaltyRecyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "loyaltySubtitle",
        "loyaltyTitle",
        "onRewardRowRedeemed",
        "onRewardRowRemoved",
        "progressBar",
        "Landroid/widget/ProgressBar;",
        "view",
        "attach",
        "bindViews",
        "getActionTextCopy",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "isApplied",
        "",
        "onScreen",
        "screen",
        "onScreenInteractive",
        "type",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;",
        "onScreenLoading",
        "setupCouponRecycler",
        "recyclerView",
        "setupLoyaltyRecycler",
        "addItemGapDecoration",
        "setColor",
        "Lcom/squareup/ui/crm/rows/InitialCircleView;",
        "row",
        "setRowBackground",
        "selected",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private couponClickedCallback:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private couponEmptyLabel:Landroid/widget/TextView;

.field private couponRecycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
            ">;"
        }
    .end annotation
.end field

.field private interactiveContent:Landroid/view/View;

.field private loyaltyEmptyLabel:Landroid/widget/TextView;

.field private loyaltyRecycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
            ">;"
        }
    .end annotation
.end field

.field private loyaltyRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private loyaltySubtitle:Landroid/widget/TextView;

.field private loyaltyTitle:Landroid/widget/TextView;

.field private onRewardRowRedeemed:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onRewardRowRemoved:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private progressBar:Landroid/widget/ProgressBar;

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    return-void
.end method

.method public static final synthetic access$getActionTextCopy(Lcom/squareup/redeemrewards/ViewRewardsCoordinator;Landroid/content/res/Resources;Z)Ljava/lang/String;
    .locals 0

    .line 40
    invoke-direct {p0, p1, p2}, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->getActionTextCopy(Landroid/content/res/Resources;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getCouponClickedCallback$p(Lcom/squareup/redeemrewards/ViewRewardsCoordinator;)Lkotlin/jvm/functions/Function1;
    .locals 1

    .line 40
    iget-object p0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->couponClickedCallback:Lkotlin/jvm/functions/Function1;

    if-nez p0, :cond_0

    const-string v0, "couponClickedCallback"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getOnRewardRowRedeemed$p(Lcom/squareup/redeemrewards/ViewRewardsCoordinator;)Lkotlin/jvm/functions/Function1;
    .locals 1

    .line 40
    iget-object p0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->onRewardRowRedeemed:Lkotlin/jvm/functions/Function1;

    if-nez p0, :cond_0

    const-string v0, "onRewardRowRedeemed"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getOnRewardRowRemoved$p(Lcom/squareup/redeemrewards/ViewRewardsCoordinator;)Lkotlin/jvm/functions/Function1;
    .locals 1

    .line 40
    iget-object p0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->onRewardRowRemoved:Lkotlin/jvm/functions/Function1;

    if-nez p0, :cond_0

    const-string v0, "onRewardRowRemoved"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$onScreen(Lcom/squareup/redeemrewards/ViewRewardsCoordinator;Lcom/squareup/redeemrewards/ViewRewardsScreen;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->onScreen(Lcom/squareup/redeemrewards/ViewRewardsScreen;)V

    return-void
.end method

.method public static final synthetic access$setColor(Lcom/squareup/redeemrewards/ViewRewardsCoordinator;Lcom/squareup/ui/crm/rows/InitialCircleView;Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1, p2}, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->setColor(Lcom/squareup/ui/crm/rows/InitialCircleView;Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;)V

    return-void
.end method

.method public static final synthetic access$setCouponClickedCallback$p(Lcom/squareup/redeemrewards/ViewRewardsCoordinator;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->couponClickedCallback:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$setOnRewardRowRedeemed$p(Lcom/squareup/redeemrewards/ViewRewardsCoordinator;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->onRewardRowRedeemed:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$setOnRewardRowRemoved$p(Lcom/squareup/redeemrewards/ViewRewardsCoordinator;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->onRewardRowRemoved:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$setRowBackground(Lcom/squareup/redeemrewards/ViewRewardsCoordinator;Landroid/view/View;Z)V
    .locals 0

    .line 40
    invoke-direct {p0, p1, p2}, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->setRowBackground(Landroid/view/View;Z)V

    return-void
.end method

.method private final addItemGapDecoration(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 3

    .line 103
    new-instance v0, Lcom/squareup/ui/DividerDecoration;

    .line 104
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/noho/R$dimen;->noho_spacing_small:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 103
    invoke-direct {v0, v1}, Lcom/squareup/ui/DividerDecoration;-><init>(I)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    .line 102
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 83
    iput-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->view:Landroid/view/View;

    .line 84
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 85
    sget v0, Lcom/squareup/redeemrewards/impl/R$id;->view_rewards_screen_content:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.v\u2026w_rewards_screen_content)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->interactiveContent:Landroid/view/View;

    .line 86
    sget v0, Lcom/squareup/redeemrewards/impl/R$id;->view_rewards_screen_progress_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.v\u2026ards_screen_progress_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->progressBar:Landroid/widget/ProgressBar;

    .line 88
    sget v0, Lcom/squareup/redeemrewards/impl/R$id;->coupon_empty_label:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.coupon_empty_label)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->couponEmptyLabel:Landroid/widget/TextView;

    .line 89
    sget v0, Lcom/squareup/redeemrewards/impl/R$id;->coupon_recycler_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    .line 90
    sget v1, Lcom/squareup/redeemrewards/impl/R$id;->redeem_rewards_loyalty_rewards_title:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyTitle:Landroid/widget/TextView;

    .line 91
    sget v1, Lcom/squareup/redeemrewards/impl/R$id;->redeem_rewards_loyalty_rewards_subtitle:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltySubtitle:Landroid/widget/TextView;

    .line 92
    sget v1, Lcom/squareup/redeemrewards/impl/R$id;->loyalty_rewards_recycler_view:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 94
    invoke-direct {p0, v0}, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->addItemGapDecoration(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 95
    invoke-direct {p0, v0}, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->setupCouponRecycler(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->couponRecycler:Lcom/squareup/cycler/Recycler;

    .line 97
    sget v0, Lcom/squareup/redeemrewards/impl/R$id;->rewards_empty_label:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.rewards_empty_label)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyEmptyLabel:Landroid/widget/TextView;

    .line 98
    iget-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v0, "loyaltyRecyclerView"

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->addItemGapDecoration(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 99
    iget-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p1, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->setupLoyaltyRecycler(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyRecycler:Lcom/squareup/cycler/Recycler;

    return-void
.end method

.method private final getActionTextCopy(Landroid/content/res/Resources;Z)Ljava/lang/String;
    .locals 0

    if-eqz p2, :cond_0

    .line 280
    sget p2, Lcom/squareup/redeemrewards/impl/R$string;->redeem_rewards_undo:I

    goto :goto_0

    .line 282
    :cond_0
    sget p2, Lcom/squareup/redeemrewards/impl/R$string;->redeem_rewards_redeem:I

    .line 278
    :goto_0
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p2, "resources.getString(\n   \u2026ewards_redeem\n      }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onScreen(Lcom/squareup/redeemrewards/ViewRewardsScreen;)V
    .locals 4

    .line 118
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 109
    :cond_0
    iget-object v2, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 111
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen;->getBackButtonIsX()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 112
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    goto :goto_0

    .line 114
    :cond_2
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    .line 116
    :goto_0
    new-instance v3, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$onScreen$1;

    invoke-direct {v3, p1}, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$onScreen$1;-><init>(Lcom/squareup/redeemrewards/ViewRewardsScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 110
    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 117
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen;->getTitle()Lcom/squareup/util/ViewString;

    move-result-object v2

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 118
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->view:Landroid/view/View;

    if-nez v0, :cond_3

    const-string/jumbo v1, "view"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v1, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$onScreen$2;

    invoke-direct {v1, p1}, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$onScreen$2;-><init>(Lcom/squareup/redeemrewards/ViewRewardsScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 121
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen;->getType()Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;

    move-result-object v0

    .line 122
    instance-of v1, v0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen;->getType()Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;

    move-result-object p1

    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;

    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->onScreenInteractive(Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;)V

    goto :goto_1

    .line 123
    :cond_4
    sget-object p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Loading;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Loading;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    invoke-direct {p0}, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->onScreenLoading()V

    :cond_5
    :goto_1
    return-void
.end method

.method private final onScreenInteractive(Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;)V
    .locals 9

    .line 132
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 130
    :cond_0
    iget-object v2, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v2

    .line 131
    sget-object v3, Lcom/squareup/noho/NohoActionButtonStyle;->SECONDARY:Lcom/squareup/noho/NohoActionButtonStyle;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->getActionBarActionText()Lcom/squareup/util/ViewString;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/resources/TextModel;

    const/4 v5, 0x0

    new-instance v1, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$onScreenInteractive$1;

    invoke-direct {v1, p1}, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$onScreenInteractive$1;-><init>(Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;)V

    move-object v6, v1

    check-cast v6, Lkotlin/jvm/functions/Function0;

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 132
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 134
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->interactiveContent:Landroid/view/View;

    if-nez v0, :cond_2

    const-string v1, "interactiveContent"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 135
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->progressBar:Landroid/widget/ProgressBar;

    if-nez v0, :cond_3

    const-string v1, "progressBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 137
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->couponEmptyLabel:Landroid/widget/TextView;

    if-nez v0, :cond_4

    const-string v1, "couponEmptyLabel"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->getShowCouponEmptyLabel()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 138
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->getOnCouponRowClicked()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->couponClickedCallback:Lkotlin/jvm/functions/Function1;

    .line 139
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->getOnRewardRowRedeemed()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->onRewardRowRedeemed:Lkotlin/jvm/functions/Function1;

    .line 140
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->getOnRewardRowRemoved()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->onRewardRowRemoved:Lkotlin/jvm/functions/Function1;

    .line 141
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->couponRecycler:Lcom/squareup/cycler/Recycler;

    if-nez v0, :cond_5

    const-string v1, "couponRecycler"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->getCoupons()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler;->setData(Lcom/squareup/cycler/DataSource;)V

    .line 143
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;->getLoyaltyRewardSection()Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;

    move-result-object p1

    .line 144
    instance-of v0, p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection$Hidden;

    const-string v1, "loyaltyEmptyLabel"

    const-string v2, "loyaltyRecyclerView"

    const-string v3, "loyaltySubtitle"

    const-string v4, "loyaltyTitle"

    if-eqz v0, :cond_a

    .line 145
    iget-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyTitle:Landroid/widget/TextView;

    if-nez p1, :cond_6

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 146
    iget-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltySubtitle:Landroid/widget/TextView;

    if-nez p1, :cond_7

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 148
    iget-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p1, :cond_8

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 150
    iget-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyEmptyLabel:Landroid/widget/TextView;

    if-nez p1, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    goto/16 :goto_0

    .line 152
    :cond_a
    instance-of v0, p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection$ShownNoTiers;

    if-eqz v0, :cond_11

    .line 153
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyTitle:Landroid/widget/TextView;

    if-nez v0, :cond_b

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection$ShownNoTiers;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection$ShownNoTiers;->getLoyaltySectionTitle()Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyTitle:Landroid/widget/TextView;

    if-nez v0, :cond_c

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 155
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltySubtitle:Landroid/widget/TextView;

    if-nez v0, :cond_d

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection$ShownNoTiers;->getLoyaltySectionSubtitle()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    iget-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltySubtitle:Landroid/widget/TextView;

    if-nez p1, :cond_e

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 158
    iget-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p1, :cond_f

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 160
    iget-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyEmptyLabel:Landroid/widget/TextView;

    if-nez p1, :cond_10

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 162
    :cond_11
    instance-of v0, p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection$Shown;

    if-eqz v0, :cond_19

    .line 163
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyTitle:Landroid/widget/TextView;

    if-nez v0, :cond_12

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection$Shown;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection$Shown;->getLoyaltySectionTitle()Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyTitle:Landroid/widget/TextView;

    if-nez v0, :cond_13

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_13
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 165
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltySubtitle:Landroid/widget/TextView;

    if-nez v0, :cond_14

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_14
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection$Shown;->getLoyaltySectionSubtitle()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltySubtitle:Landroid/widget/TextView;

    if-nez v0, :cond_15

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_15
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 168
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyRecycler:Lcom/squareup/cycler/Recycler;

    if-nez v0, :cond_16

    const-string v3, "loyaltyRecycler"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_16
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection$Shown;->getLoyaltyRewardTiers()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cycler/Recycler;->setData(Lcom/squareup/cycler/DataSource;)V

    .line 169
    iget-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p1, :cond_17

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_17
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 171
    iget-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->loyaltyEmptyLabel:Landroid/widget/TextView;

    if-nez p1, :cond_18

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_18
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    :cond_19
    :goto_0
    return-void
.end method

.method private final onScreenLoading()V
    .locals 3

    .line 179
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 177
    :cond_0
    iget-object v2, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 178
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideAction()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 179
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 181
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->interactiveContent:Landroid/view/View;

    if-nez v0, :cond_2

    const-string v1, "interactiveContent"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 182
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->progressBar:Landroid/widget/ProgressBar;

    if-nez v0, :cond_3

    const-string v1, "progressBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 184
    sget-object v0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$onScreenLoading$1;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsCoordinator$onScreenLoading$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    iput-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->couponClickedCallback:Lkotlin/jvm/functions/Function1;

    .line 185
    sget-object v0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$onScreenLoading$2;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsCoordinator$onScreenLoading$2;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    iput-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->onRewardRowRedeemed:Lkotlin/jvm/functions/Function1;

    .line 186
    sget-object v0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$onScreenLoading$3;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsCoordinator$onScreenLoading$3;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    iput-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->onRewardRowRemoved:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method private final setColor(Lcom/squareup/ui/crm/rows/InitialCircleView;Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;)V
    .locals 1

    .line 294
    invoke-virtual {p2}, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;->getAppliedCouponTokens()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    sget p2, Lcom/squareup/redeemrewards/impl/R$color;->reward_row_initial_cirlce_bg_applied:I

    goto :goto_0

    .line 295
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;->getEnabled()Z

    move-result p2

    if-eqz p2, :cond_1

    sget p2, Lcom/squareup/redeemrewards/impl/R$color;->reward_row_initial_cirlce_bg_enabled:I

    goto :goto_0

    .line 296
    :cond_1
    sget p2, Lcom/squareup/redeemrewards/impl/R$color;->reward_row_initial_cirlce_bg_disabled:I

    .line 298
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/InitialCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/rows/InitialCircleView;->setBackgroundColor(I)V

    return-void
.end method

.method private final setRowBackground(Landroid/view/View;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 287
    sget p2, Lcom/squareup/redeemrewards/impl/R$drawable;->reward_row_background_selected:I

    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 289
    :cond_0
    sget p2, Lcom/squareup/redeemrewards/impl/R$drawable;->reward_row_background:I

    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_0
    return-void
.end method

.method private final setupCouponRecycler(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView;",
            ")",
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
            ">;"
        }
    .end annotation

    .line 190
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 302
    sget-object v1, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 303
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 304
    new-instance v1, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v1}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 308
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 309
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 191
    new-instance v0, Lcom/squareup/redeemrewards/CouponRecyclerComparator;

    invoke-direct {v0}, Lcom/squareup/redeemrewards/CouponRecyclerComparator;-><init>()V

    check-cast v0, Lcom/squareup/cycler/ItemComparator;

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setItemComparator(Lcom/squareup/cycler/ItemComparator;)V

    .line 312
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsCoordinator$$special$$inlined$row$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 193
    sget v2, Lcom/squareup/redeemrewards/impl/R$layout;->coupon_reward_row:I

    .line 314
    new-instance v3, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$setupCouponRecycler$$inlined$adopt$lambda$1;

    invoke-direct {v3, v2, p0}, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$setupCouponRecycler$$inlined$adopt$lambda$1;-><init>(ILcom/squareup/redeemrewards/ViewRewardsCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 312
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 311
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 306
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    return-object p1

    .line 303
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final setupLoyaltyRecycler(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView;",
            ")",
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$LoyaltyRewardRow;",
            ">;"
        }
    .end annotation

    .line 218
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 324
    sget-object v1, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 325
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 326
    new-instance v1, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v1}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 330
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 331
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 334
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsCoordinator$$special$$inlined$row$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 220
    sget v2, Lcom/squareup/redeemrewards/impl/R$layout;->loyalty_reward_row:I

    .line 336
    new-instance v3, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$setupLoyaltyRecycler$$inlined$adopt$lambda$1;

    invoke-direct {v3, v2, p0}, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$setupLoyaltyRecycler$$inlined$adopt$lambda$1;-><init>(ILcom/squareup/redeemrewards/ViewRewardsCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 334
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 333
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 328
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    return-object p1

    .line 325
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 75
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->bindViews(Landroid/view/View;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;->screens:Lio/reactivex/Observable;

    .line 78
    sget-object v1, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$attach$1;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsCoordinator$attach$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens\n        .map { it.unwrapV2Screen }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    new-instance v1, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$attach$2;

    move-object v2, p0

    check-cast v2, Lcom/squareup/redeemrewards/ViewRewardsCoordinator;

    invoke-direct {v1, v2}, Lcom/squareup/redeemrewards/ViewRewardsCoordinator$attach$2;-><init>(Lcom/squareup/redeemrewards/ViewRewardsCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
