.class final Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1;
.super Ljava/lang/Object;
.source "ViewRewardsWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->redeemLoyaltyTier(Lcom/squareup/redeemrewards/ViewRewardsState$RedeemingLoyaltyTier;)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/redeemrewards/ViewRewardsState;",
        "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1;->this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/ViewRewardsState;",
            "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1;->this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow;

    .line 245
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;

    iget-object v1, v1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    const-string v2, "successOrFailure.response.coupon"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 246
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->balance:Ljava/lang/Integer;

    .line 244
    invoke-static {v0, v1, p1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->access$applyCouponAction(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/protos/client/coupons/Coupon;Ljava/lang/Integer;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 249
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1;->this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow;

    new-instance v1, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1$1;-><init>(Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v2, v1, p1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$redeemLoyaltyTier$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
