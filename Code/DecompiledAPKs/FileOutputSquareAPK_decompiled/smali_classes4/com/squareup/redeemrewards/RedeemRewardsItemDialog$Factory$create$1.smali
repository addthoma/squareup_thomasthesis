.class final Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory$create$1;
.super Ljava/lang/Object;
.source "RedeemRewardsItemDialog.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/Dialog;",
        "screenData",
        "Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;

.field final synthetic $scopeRunner:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

.field final synthetic this$0:Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory;Landroid/content/Context;Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory$create$1;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory;

    iput-object p2, p0, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory$create$1;->$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory$create$1;->$scopeRunner:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;)Landroid/app/Dialog;
    .locals 3

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory$create$1;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory$create$1;->$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory$create$1;->$scopeRunner:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-static {v0, v1, p1, v2}, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory;->access$createDialog(Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory;Landroid/content/Context;Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory$create$1;->apply(Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method
