.class public final Lcom/squareup/redeemrewards/RealRedeemRewardsFlow;
.super Ljava/lang/Object;
.source "RealRedeemRewardsFlow.kt"

# interfaces
.implements Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\u0008H\u0016J\u0008\u0010\n\u001a\u00020\u000bH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RealRedeemRewardsFlow;",
        "Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;)V",
        "getFirstScreen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "getRedeemPointsScreen",
        "useWorkflow",
        "",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardsFlow;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p2, p0, Lcom/squareup/redeemrewards/RealRedeemRewardsFlow;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private final useWorkflow()Z
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardsFlow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getFirstScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 3

    .line 18
    invoke-direct {p0}, Lcom/squareup/redeemrewards/RealRedeemRewardsFlow;->useWorkflow()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardBootstrapScreen;

    .line 19
    new-instance v1, Lcom/squareup/redeemrewards/RedeemRewardProps;

    .line 22
    iget-object v2, p0, Lcom/squareup/redeemrewards/RealRedeemRewardsFlow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getCustomerId()Ljava/lang/String;

    move-result-object v2

    .line 19
    invoke-direct {v1, v2}, Lcom/squareup/redeemrewards/RedeemRewardProps;-><init>(Ljava/lang/String;)V

    .line 18
    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/RedeemRewardBootstrapScreen;-><init>(Lcom/squareup/redeemrewards/RedeemRewardProps;)V

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    goto :goto_0

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardsFlow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasCustomer()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/redeemrewards/RealRedeemRewardsFlow;->getRedeemPointsScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    goto :goto_0

    .line 28
    :cond_1
    new-instance v0, Lcom/squareup/redeemrewards/ChooseCustomerScreenV2;

    new-instance v1, Lcom/squareup/redeemrewards/RedeemRewardsScope;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/squareup/redeemrewards/RedeemRewardsScope;-><init>(Z)V

    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/ChooseCustomerScreenV2;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    :goto_0
    return-object v0
.end method

.method public getRedeemPointsScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 3

    .line 35
    invoke-direct {p0}, Lcom/squareup/redeemrewards/RealRedeemRewardsFlow;->useWorkflow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/RealRedeemRewardsFlow;->getFirstScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    goto :goto_0

    .line 40
    :cond_0
    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen;

    new-instance v1, Lcom/squareup/redeemrewards/RedeemRewardsScope;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/squareup/redeemrewards/RedeemRewardsScope;-><init>(Z)V

    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    :goto_0
    return-object v0
.end method
