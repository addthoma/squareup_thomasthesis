.class public final Lcom/squareup/redeemrewards/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final choose_reward_item_container:I = 0x7f0a0339

.field public static final coupon_ribbon:I = 0x7f0a03bd

.field public static final crm_bottom_button_row:I = 0x7f0a03f7

.field public static final crm_contact_list:I = 0x7f0a040f

.field public static final crm_create_customer_from_search_term_button:I = 0x7f0a041a

.field public static final crm_progress:I = 0x7f0a04ca

.field public static final crm_progress_bar:I = 0x7f0a04cb

.field public static final crm_progress_text:I = 0x7f0a04cc

.field public static final crm_redeem_reward_button:I = 0x7f0a04d1

.field public static final crm_redeem_reward_layout:I = 0x7f0a04d2

.field public static final crm_redeem_reward_subtitle:I = 0x7f0a04d3

.field public static final crm_redeem_reward_title:I = 0x7f0a04d4

.field public static final crm_reward_code_description:I = 0x7f0a04df

.field public static final crm_reward_code_digits:I = 0x7f0a04e0

.field public static final crm_reward_not_found_layout:I = 0x7f0a04e1

.field public static final crm_search_box:I = 0x7f0a04e7

.field public static final crm_search_message:I = 0x7f0a04ea

.field public static final crm_search_progress_bar:I = 0x7f0a04eb

.field public static final crm_search_reward_again:I = 0x7f0a04ec

.field public static final empty_view:I = 0x7f0a06f6

.field public static final items_recyclerview:I = 0x7f0a08eb

.field public static final redeem_reward_tiers_list:I = 0x7f0a0d22

.field public static final redeem_reward_tiers_list_title:I = 0x7f0a0d23

.field public static final redeem_rewards_cart_points_content:I = 0x7f0a0d24

.field public static final redeem_rewards_cart_points_title:I = 0x7f0a0d25

.field public static final redeem_rewards_cart_points_value:I = 0x7f0a0d26

.field public static final redeem_rewards_contact_points_content:I = 0x7f0a0d27

.field public static final redeem_rewards_contact_points_title:I = 0x7f0a0d28

.field public static final redeem_rewards_contact_points_value:I = 0x7f0a0d29

.field public static final redeem_rewards_container:I = 0x7f0a0d2a

.field public static final redeem_rewards_content:I = 0x7f0a0d2b

.field public static final redeem_rewards_coupons_list:I = 0x7f0a0d2c

.field public static final redeem_rewards_coupons_list_title:I = 0x7f0a0d2d

.field public static final redeem_rewards_points_content:I = 0x7f0a0d30

.field public static final redeem_rewards_points_message:I = 0x7f0a0d31


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
