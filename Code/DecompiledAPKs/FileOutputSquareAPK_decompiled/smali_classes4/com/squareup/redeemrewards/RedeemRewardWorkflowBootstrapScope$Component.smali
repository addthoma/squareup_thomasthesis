.class public interface abstract Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope$Component;
.super Ljava/lang/Object;
.source "RedeemRewardWorkflowBootstrapScope.kt"

# interfaces
.implements Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$Component;
.implements Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope$ParentComponent;


# annotations
.annotation runtime Lcom/squareup/crm/RolodexContactLoader$SharedScope;
.end annotation

.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;
.end annotation

.annotation runtime Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/redeemrewards/RedeemRewardsWorkflowModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u00012\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope$Component;",
        "Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$Component;",
        "Lcom/squareup/redeemrewards/RedeemRewardWorkflowScope$ParentComponent;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
