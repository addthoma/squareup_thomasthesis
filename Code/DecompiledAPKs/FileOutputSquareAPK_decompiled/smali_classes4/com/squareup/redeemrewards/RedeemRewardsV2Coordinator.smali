.class public final Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "RedeemRewardsV2Coordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRedeemRewardsV2Coordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RedeemRewardsV2Coordinator.kt\ncom/squareup/redeemrewards/RedeemRewardsV2Coordinator\n*L\n1#1,183:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020\u000eH\u0016J\u0010\u0010&\u001a\u00020$2\u0006\u0010%\u001a\u00020\u000eH\u0002J\u0008\u0010\'\u001a\u00020(H\u0002J\u0010\u0010)\u001a\u00020$2\u0006\u0010*\u001a\u00020+H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "res",
        "Lcom/squareup/util/Res;",
        "runner",
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$Runner;",
        "errorsBar",
        "Lcom/squareup/ui/ErrorsBarPresenter;",
        "pointsTermsFormatter",
        "Lcom/squareup/loyalty/PointsTermsFormatter;",
        "(Lcom/squareup/util/Res;Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$Runner;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/loyalty/PointsTermsFormatter;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "cartPointsContent",
        "Landroid/view/View;",
        "cartPointsTitle",
        "Landroid/widget/TextView;",
        "cartPointsValue",
        "contactPointsContent",
        "contactPointsTitle",
        "contactPointsValue",
        "customersRewardsAdapter",
        "Lcom/squareup/loyalty/ui/RewardAdapter;",
        "customersRewardsList",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "customersRewardsListTitle",
        "emptyView",
        "Lcom/squareup/ui/EmptyView;",
        "pointsContent",
        "pointsMessage",
        "progressBar",
        "redeemRewardsContent",
        "rewardTiersAdapter",
        "rewardTiersList",
        "rewardTiersListTitle",
        "attach",
        "",
        "view",
        "bindViews",
        "getActionBarIcon",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "updateView",
        "data",
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private cartPointsContent:Landroid/view/View;

.field private cartPointsTitle:Landroid/widget/TextView;

.field private cartPointsValue:Landroid/widget/TextView;

.field private contactPointsContent:Landroid/view/View;

.field private contactPointsTitle:Landroid/widget/TextView;

.field private contactPointsValue:Landroid/widget/TextView;

.field private customersRewardsAdapter:Lcom/squareup/loyalty/ui/RewardAdapter;

.field private customersRewardsList:Landroidx/recyclerview/widget/RecyclerView;

.field private customersRewardsListTitle:Landroid/view/View;

.field private emptyView:Lcom/squareup/ui/EmptyView;

.field private final errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

.field private pointsContent:Landroid/view/View;

.field private pointsMessage:Landroid/widget/TextView;

.field private final pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

.field private progressBar:Landroid/view/View;

.field private redeemRewardsContent:Landroid/view/View;

.field private final res:Lcom/squareup/util/Res;

.field private rewardTiersAdapter:Lcom/squareup/loyalty/ui/RewardAdapter;

.field private rewardTiersList:Landroidx/recyclerview/widget/RecyclerView;

.field private rewardTiersListTitle:Landroid/view/View;

.field private final runner:Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$Runner;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/loyalty/PointsTermsFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "runner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorsBar"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pointsTermsFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->runner:Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$Runner;

    iput-object p3, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    iput-object p4, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    .line 51
    new-instance p1, Lcom/squareup/loyalty/ui/RewardAdapter;

    invoke-direct {p1}, Lcom/squareup/loyalty/ui/RewardAdapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->customersRewardsAdapter:Lcom/squareup/loyalty/ui/RewardAdapter;

    .line 54
    new-instance p1, Lcom/squareup/loyalty/ui/RewardAdapter;

    invoke-direct {p1}, Lcom/squareup/loyalty/ui/RewardAdapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->rewardTiersAdapter:Lcom/squareup/loyalty/ui/RewardAdapter;

    return-void
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;)Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$Runner;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->runner:Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$Runner;

    return-object p0
.end method

.method public static final synthetic access$updateView(Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->updateView(Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 157
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "ActionBarView.findIn(view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 159
    sget v0, Lcom/squareup/redeemrewards/R$id;->crm_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->progressBar:Landroid/view/View;

    .line 160
    sget v0, Lcom/squareup/redeemrewards/R$id;->empty_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/EmptyView;

    iput-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    .line 161
    sget v0, Lcom/squareup/redeemrewards/R$id;->redeem_rewards_content:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->redeemRewardsContent:Landroid/view/View;

    .line 162
    sget v0, Lcom/squareup/redeemrewards/R$id;->redeem_rewards_points_content:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->pointsContent:Landroid/view/View;

    .line 164
    sget v0, Lcom/squareup/redeemrewards/R$id;->redeem_rewards_cart_points_content:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->cartPointsContent:Landroid/view/View;

    .line 165
    sget v0, Lcom/squareup/redeemrewards/R$id;->redeem_rewards_cart_points_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->cartPointsTitle:Landroid/widget/TextView;

    .line 166
    sget v0, Lcom/squareup/redeemrewards/R$id;->redeem_rewards_cart_points_value:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->cartPointsValue:Landroid/widget/TextView;

    .line 168
    sget v0, Lcom/squareup/redeemrewards/R$id;->redeem_rewards_contact_points_content:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->contactPointsContent:Landroid/view/View;

    .line 169
    sget v0, Lcom/squareup/redeemrewards/R$id;->redeem_rewards_contact_points_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->contactPointsTitle:Landroid/widget/TextView;

    .line 170
    sget v0, Lcom/squareup/redeemrewards/R$id;->redeem_rewards_contact_points_value:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->contactPointsValue:Landroid/widget/TextView;

    .line 172
    sget v0, Lcom/squareup/redeemrewards/R$id;->redeem_rewards_points_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->pointsMessage:Landroid/widget/TextView;

    .line 174
    sget v0, Lcom/squareup/redeemrewards/R$id;->redeem_rewards_coupons_list_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->customersRewardsListTitle:Landroid/view/View;

    .line 175
    sget v0, Lcom/squareup/redeemrewards/R$id;->redeem_rewards_coupons_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->customersRewardsList:Landroidx/recyclerview/widget/RecyclerView;

    .line 177
    sget v0, Lcom/squareup/redeemrewards/R$id;->redeem_reward_tiers_list_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->rewardTiersListTitle:Landroid/view/View;

    .line 178
    sget v0, Lcom/squareup/redeemrewards/R$id;->redeem_reward_tiers_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->rewardTiersList:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method private final getActionBarIcon()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->runner:Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$Runner;

    invoke-interface {v0}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$Runner;->actionBarUsesBackArrow()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_0
    return-object v0
.end method

.method private final updateView(Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;)V
    .locals 9

    .line 75
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->progressBar:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "progressBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getLoading()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 76
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const-string v1, "actionBar"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->getActionBarIcon()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getTitle()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 78
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getActionBarAction()Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/Void;

    const-string v2, ""

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v0, :cond_5

    .line 79
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 80
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    move-object v5, v2

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v0, v5}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->hidePrimaryButton()V

    goto :goto_0

    .line 83
    :cond_5
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v0, v4}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 84
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    iget-object v5, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getActionBarAction()Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;

    move-result-object v6

    invoke-virtual {v6}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;->getText()I

    move-result v6

    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v0, v5}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    new-instance v1, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator$updateView$1;

    invoke-direct {v1, p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator$updateView$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 88
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/2addr v0, v4

    if-eqz v0, :cond_9

    .line 89
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getErrorMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 91
    :cond_9
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ErrorsBarPresenter;->clearErrors()V

    .line 95
    :goto_1
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getLoading()Z

    move-result v0

    const-string v1, "redeemRewardsContent"

    if-eqz v0, :cond_b

    .line 96
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->redeemRewardsContent:Landroid/view/View;

    if-nez p1, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-static {p1, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    goto/16 :goto_6

    .line 99
    :cond_b
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->redeemRewardsContent:Landroid/view/View;

    if-nez v0, :cond_c

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->res:Lcom/squareup/util/Res;

    const/high16 v5, 0x10e0000

    invoke-interface {v1, v5}, Lcom/squareup/util/Res;->getInteger(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 104
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getCartPoints()I

    move-result v0

    const-string v1, "pointsContent"

    const/4 v5, -0x1

    if-ne v0, v5, :cond_e

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getContactPoints()I

    move-result v0

    if-ne v0, v5, :cond_e

    .line 105
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->pointsContent:Landroid/view/View;

    if-nez v0, :cond_d

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-static {v0, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    const/4 v0, 0x1

    goto/16 :goto_5

    .line 107
    :cond_e
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->pointsContent:Landroid/view/View;

    if-nez v0, :cond_f

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    invoke-static {v0, v4}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 109
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->cartPointsContent:Landroid/view/View;

    if-nez v0, :cond_10

    const-string v1, "cartPointsContent"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getCartPoints()I

    move-result v1

    if-eq v1, v5, :cond_11

    const/4 v1, 0x1

    goto :goto_2

    :cond_11
    const/4 v1, 0x0

    :goto_2
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 110
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getCartPoints()I

    move-result v0

    if-eq v0, v5, :cond_14

    .line 112
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->cartPointsTitle:Landroid/widget/TextView;

    if-nez v0, :cond_12

    const-string v1, "cartPointsTitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    sget v6, Lcom/squareup/redeemrewards/R$string;->redeem_rewards_cart_points_title:I

    invoke-virtual {v1, v6}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->cartPointsValue:Landroid/widget/TextView;

    if-nez v0, :cond_13

    const-string v1, "cartPointsValue"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_13
    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getCartPoints()I

    move-result v6

    int-to-long v6, v6

    .line 114
    sget v8, Lcom/squareup/redeemrewards/R$string;->redeem_rewards_cart_points_value:I

    .line 113
    invoke-virtual {v1, v6, v7, v8}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(JI)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    goto :goto_3

    :cond_14
    const/4 v0, 0x1

    .line 118
    :goto_3
    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->contactPointsContent:Landroid/view/View;

    if-nez v1, :cond_15

    const-string v6, "contactPointsContent"

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_15
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getContactPoints()I

    move-result v6

    if-eq v6, v5, :cond_16

    const/4 v6, 0x1

    goto :goto_4

    :cond_16
    const/4 v6, 0x0

    :goto_4
    invoke-static {v1, v6}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 119
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getContactPoints()I

    move-result v1

    if-eq v1, v5, :cond_19

    .line 121
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->contactPointsTitle:Landroid/widget/TextView;

    if-nez v0, :cond_17

    const-string v1, "contactPointsTitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_17
    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    sget v5, Lcom/squareup/redeemrewards/R$string;->redeem_rewards_customer_points_title:I

    invoke-virtual {v1, v5}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->contactPointsValue:Landroid/widget/TextView;

    if-nez v0, :cond_18

    const-string v1, "contactPointsValue"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_18
    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getContactPoints()I

    move-result v5

    int-to-long v5, v5

    invoke-virtual {v1, v5, v6}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(J)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    .line 127
    :cond_19
    :goto_5
    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->pointsMessage:Landroid/widget/TextView;

    const-string v5, "pointsMessage"

    if-nez v1, :cond_1a

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1a
    check-cast v1, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getPointsMessage()Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-static {v6}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v6

    xor-int/2addr v6, v4

    invoke-static {v1, v6}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 128
    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->pointsMessage:Landroid/widget/TextView;

    if-nez v1, :cond_1b

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1b
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getPointsMessage()Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->customersRewardsListTitle:Landroid/view/View;

    if-nez v1, :cond_1c

    const-string v5, "customersRewardsListTitle"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1c
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getCoupons()Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    xor-int/2addr v5, v4

    invoke-static {v1, v5}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 132
    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->customersRewardsList:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v1, :cond_1d

    const-string v5, "customersRewardsList"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1d
    check-cast v1, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getCoupons()Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    xor-int/2addr v5, v4

    invoke-static {v1, v5}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 133
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getCoupons()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v4

    if-eqz v1, :cond_1e

    .line 134
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->customersRewardsAdapter:Lcom/squareup/loyalty/ui/RewardAdapter;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getCoupons()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/ui/RewardAdapter;->setItems(Ljava/util/List;)V

    const/4 v0, 0x0

    .line 139
    :cond_1e
    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->rewardTiersListTitle:Landroid/view/View;

    if-nez v1, :cond_1f

    const-string v5, "rewardTiersListTitle"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1f
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getRewardTiers()Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    xor-int/2addr v5, v4

    invoke-static {v1, v5}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 140
    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->rewardTiersList:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v1, :cond_20

    const-string v5, "rewardTiersList"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_20
    check-cast v1, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getRewardTiers()Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    xor-int/2addr v5, v4

    invoke-static {v1, v5}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 141
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getRewardTiers()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v4

    if-eqz v1, :cond_21

    .line 142
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->rewardTiersAdapter:Lcom/squareup/loyalty/ui/RewardAdapter;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->getRewardTiers()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/loyalty/ui/RewardAdapter;->setItems(Ljava/util/List;)V

    const/4 v0, 0x0

    .line 147
    :cond_21
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    const-string v1, "emptyView"

    if-nez p1, :cond_22

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_22
    check-cast p1, Landroid/view/View;

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz v0, :cond_26

    .line 149
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    if-nez p1, :cond_23

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_23
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_REWARDS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/EmptyView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 150
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    if-nez p1, :cond_24

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_24
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/redeemrewards/R$string;->points_empty_state_title:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/EmptyView;->setTitle(Ljava/lang/CharSequence;)V

    .line 151
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    if-nez p1, :cond_25

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_25
    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v2}, Lcom/squareup/ui/EmptyView;->setMessage(Ljava/lang/CharSequence;)V

    :cond_26
    :goto_6
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 6

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->bindViews(Landroid/view/View;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->getActionBarIcon()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v2

    const-string v3, ""

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator$attach$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->rewardTiersList:Landroidx/recyclerview/widget/RecyclerView;

    const-string v1, "rewardTiersList"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v2, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->rewardTiersAdapter:Lcom/squareup/loyalty/ui/RewardAdapter;

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->rewardTiersList:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v2, Lcom/squareup/noho/NohoEdgeDecoration;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string/jumbo v4, "view.resources"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/squareup/noho/NohoEdgeDecoration;-><init>(Landroid/content/res/Resources;)V

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->rewardTiersList:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    .line 66
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->customersRewardsList:Landroidx/recyclerview/widget/RecyclerView;

    const-string v2, "customersRewardsList"

    if-nez v0, :cond_5

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    iget-object v3, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->customersRewardsAdapter:Lcom/squareup/loyalty/ui/RewardAdapter;

    check-cast v3, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {v0, v3}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->customersRewardsList:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    new-instance v3, Lcom/squareup/noho/NohoEdgeDecoration;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v5}, Lcom/squareup/noho/NohoEdgeDecoration;-><init>(Landroid/content/res/Resources;)V

    check-cast v3, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {v0, v3}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->customersRewardsList:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_7

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    .line 70
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;->runner:Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$Runner;

    invoke-interface {v0}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$Runner;->redeemRewardsV2ScreenData()Lrx/Observable;

    move-result-object v0

    .line 71
    new-instance v1, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator$attach$2;

    invoke-direct {v1, p0}, Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator$attach$2;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    return-void
.end method
