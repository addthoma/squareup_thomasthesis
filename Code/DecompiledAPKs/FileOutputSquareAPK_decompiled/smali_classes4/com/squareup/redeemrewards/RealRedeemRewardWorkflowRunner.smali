.class public final Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "RealRedeemRewardWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/redeemrewards/RedeemRewardWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lcom/squareup/redeemrewards/RedeemRewardProps;",
        "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
        ">;",
        "Lcom/squareup/redeemrewards/RedeemRewardWorkflowRunner;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002B/\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0014J\u0010\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u0003H\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\u0008X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;",
        "Lcom/squareup/redeemrewards/RedeemRewardWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "Lcom/squareup/redeemrewards/RedeemRewardProps;",
        "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
        "viewFactory",
        "Lcom/squareup/redeemrewards/RedeemRewardCompositeViewFactory;",
        "workflow",
        "Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "contactLoader",
        "Lcom/squareup/crm/RolodexContactLoader;",
        "addEligibleItemForCouponLegacyOutputs",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;",
        "(Lcom/squareup/redeemrewards/RedeemRewardCompositeViewFactory;Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;)V",
        "getWorkflow",
        "()Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "start",
        "props",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final addEligibleItemForCouponLegacyOutputs:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;

.field private final contactLoader:Lcom/squareup/crm/RolodexContactLoader;

.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final workflow:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardCompositeViewFactory;Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "viewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactLoader"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "addEligibleItemForCouponLegacyOutputs"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget-object v0, Lcom/squareup/redeemrewards/RedeemRewardWorkflowRunner;->Companion:Lcom/squareup/redeemrewards/RedeemRewardWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/redeemrewards/RedeemRewardWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v2

    .line 24
    invoke-interface {p3}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 25
    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 22
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;->workflow:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    iput-object p3, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p4, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    iput-object p5, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;->addEligibleItemForCouponLegacyOutputs:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;

    return-void
.end method

.method public static final synthetic access$getAddEligibleItemForCouponLegacyOutputs$p(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;->addEligibleItemForCouponLegacyOutputs:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;

    return-object p0
.end method

.method public static final synthetic access$getContainer$p(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-object p0
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;->workflow:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;->getWorkflow()Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-super {p0, p1}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 32
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/crm/RolodexContactLoader;->setRestrictToSearchingOnly(Z)V

    .line 33
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 35
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 39
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public start(Lcom/squareup/redeemrewards/RedeemRewardProps;)V
    .locals 1

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;->setProps(Ljava/lang/Object;)V

    .line 55
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflowRunner;->ensureWorkflow()V

    return-void
.end method
