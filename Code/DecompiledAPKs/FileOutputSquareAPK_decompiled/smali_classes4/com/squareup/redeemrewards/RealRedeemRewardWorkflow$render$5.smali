.class final Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$5;
.super Lkotlin/jvm/internal/Lambda;
.source "RealRedeemRewardWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->render(Lcom/squareup/redeemrewards/RedeemRewardProps;Lcom/squareup/redeemrewards/RedeemRewardState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/redeemrewards/RedeemRewardState;",
        "+",
        "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/redeemrewards/RedeemRewardState;",
        "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
        "output",
        "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$5;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    instance-of v0, p1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput$ApplyCoupon;

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$5;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    move-object v1, p1

    check-cast v1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput$ApplyCoupon;

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput$ApplyCoupon;->getCoupon()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->access$shouldLaunchAddEligibleWorkflow(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 119
    iget-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$5;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    .line 120
    new-instance v2, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromCode;

    .line 121
    invoke-virtual {v1}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput$ApplyCoupon;->getCoupon()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v1

    .line 120
    invoke-direct {v2, v1, v0}, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromCode;-><init>(Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)V

    .line 119
    invoke-static {p1, v2}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->access$addingEligibleItemFromCodeAction(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromCode;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$5;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    new-instance v1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$5$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$5$1;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$5;Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v2, v1, p1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 134
    :cond_1
    sget-object v0, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput$Close;->INSTANCE:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput$Close;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$5;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    invoke-static {p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->access$getBackToChooseContactAction$p(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$5;->invoke(Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
