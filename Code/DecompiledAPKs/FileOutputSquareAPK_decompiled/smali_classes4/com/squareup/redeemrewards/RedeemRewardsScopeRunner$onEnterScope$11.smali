.class final Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11;
.super Lkotlin/jvm/internal/Lambda;
.source "RedeemRewardsScopeRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "coupon",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $scope:Lmortar/MortarScope;

.field final synthetic this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Lmortar/MortarScope;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    iput-object p2, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11;->$scope:Lmortar/MortarScope;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11;->invoke(Lcom/squareup/protos/client/coupons/Coupon;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/client/coupons/Coupon;)V
    .locals 3

    .line 302
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    const-string v1, "coupon"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->access$getCouponConstraints(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Lcom/squareup/protos/client/coupons/Coupon;)Ljava/util/List;

    move-result-object v0

    .line 304
    iget-object v1, p1, Lcom/squareup/protos/client/coupons/Coupon;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    sget-object v2, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->VARIATION:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    if-ne v1, v2, :cond_0

    .line 305
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-static {p1, v0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->access$getAllItemsInVariation(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Ljava/util/List;)Lrx/Single;

    move-result-object p1

    .line 306
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p1

    .line 307
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11;->$scope:Lmortar/MortarScope;

    new-instance v1, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11$1;

    invoke-direct {v1, p0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Single;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 310
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->showAddItemDialog()V

    goto :goto_0

    .line 312
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/coupons/Coupon;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    sget-object v1, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->CATEGORY:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    if-ne p1, v1, :cond_1

    .line 313
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-static {p1, v0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->access$getAllItemsInCategory(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Ljava/util/List;)Lrx/Single;

    move-result-object p1

    .line 314
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p1

    .line 315
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11;->$scope:Lmortar/MortarScope;

    new-instance v1, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11$2;

    invoke-direct {v1, p0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11$2;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Single;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 318
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->showAddItemDialog()V

    goto :goto_0

    .line 320
    :cond_1
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-static {p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->access$getFlow$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lflow/Flow;

    move-result-object p1

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    .line 321
    const-class v2, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 322
    const-class v2, Lcom/squareup/redeemrewards/ChooseCustomerScreenV2;

    aput-object v2, v0, v1

    .line 320
    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method
