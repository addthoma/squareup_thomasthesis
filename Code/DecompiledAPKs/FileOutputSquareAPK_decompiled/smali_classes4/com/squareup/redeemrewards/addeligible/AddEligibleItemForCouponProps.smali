.class public final Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;
.super Ljava/lang/Object;
.source "AddEligibleItemForCouponProps.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Creator;,
        Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000c\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0087\u0008\u0018\u00002\u00020\u0001:\u0001 B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0008H\u00c6\u0003J-\u0010\u0013\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008H\u00c6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\u0013\u0010\u0016\u001a\u00020\u00082\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u0006H\u00d6\u0001J\u0019\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0015H\u00d6\u0001R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;",
        "Landroid/os/Parcelable;",
        "type",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;",
        "ids",
        "",
        "",
        "includeConfirmDialog",
        "",
        "(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;Ljava/util/List;Z)V",
        "getIds",
        "()Ljava/util/List;",
        "getIncludeConfirmDialog",
        "()Z",
        "getType",
        "()Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "Type",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final includeConfirmDialog:Z

.field private final type:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Creator;

    invoke-direct {v0}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Creator;-><init>()V

    sput-object v0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ids"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->type:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;

    iput-object p2, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->ids:Ljava/util/List;

    iput-boolean p3, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->includeConfirmDialog:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;Ljava/util/List;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x1

    .line 22
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;-><init>(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;Ljava/util/List;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;Ljava/util/List;ZILjava/lang/Object;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->type:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->ids:Ljava/util/List;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->includeConfirmDialog:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->copy(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;Ljava/util/List;Z)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->type:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->ids:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->includeConfirmDialog:Z

    return v0
.end method

.method public final copy(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;Ljava/util/List;Z)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;"
        }
    .end annotation

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ids"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;-><init>(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;Ljava/util/List;Z)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->type:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;

    iget-object v1, p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->type:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->ids:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->ids:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->includeConfirmDialog:Z

    iget-boolean p1, p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->includeConfirmDialog:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->ids:Ljava/util/List;

    return-object v0
.end method

.method public final getIncludeConfirmDialog()Z
    .locals 1

    .line 22
    iget-boolean v0, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->includeConfirmDialog:Z

    return v0
.end method

.method public final getType()Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->type:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->type:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->ids:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->includeConfirmDialog:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AddEligibleItemForCouponProps(type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->type:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", includeConfirmDialog="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->includeConfirmDialog:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->type:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->ids:Ljava/util/List;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    iget-boolean p2, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->includeConfirmDialog:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
