.class public final Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealAddEligibleItemForCouponWorkflow.kt"

# interfaces
.implements Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAddEligibleItemForCouponWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAddEligibleItemForCouponWorkflow.kt\ncom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 4 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 5 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 6 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,167:1\n149#2,5:168\n149#2,5:173\n149#2,5:178\n149#2,5:183\n149#2,5:192\n1360#3:188\n1429#3,3:189\n85#4:197\n240#5:198\n276#6:199\n*E\n*S KotlinDebug\n*F\n+ 1 RealAddEligibleItemForCouponWorkflow.kt\ncom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow\n*L\n65#1,5:168\n71#1,5:173\n83#1,5:178\n92#1,5:183\n104#1,5:192\n97#1:188\n97#1,3:189\n117#1:197\n117#1:198\n117#1:199\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012@\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u0002:\u0001\u001dB\u0017\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0016\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u00112\u0006\u0010\u0013\u001a\u00020\u0003H\u0002J\u001a\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0013\u001a\u00020\u00032\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0016JR\u0010\u0018\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n2\u0006\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0019\u001a\u00020\u00042\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u0004H\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow;",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "res",
        "Lcom/squareup/util/Res;",
        "cogsHelper",
        "Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;",
        "(Lcom/squareup/util/Res;Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;)V",
        "fetchFromCogs",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action;",
        "props",
        "initialState",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$FetchingFromCogs;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cogsHelper:Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cogsHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow;->cogsHelper:Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;

    return-void
.end method

.method private final fetchFromCogs(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action;",
            ">;"
        }
    .end annotation

    .line 112
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->getType()Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;

    move-result-object v0

    sget-object v1, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps$Type;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 114
    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow;->cogsHelper:Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->getIds()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;->getAllItemsInCategory(Ljava/util/List;)Lio/reactivex/Single;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow;->cogsHelper:Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->getIds()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;->getAllItemsInVariation(Ljava/util/List;)Lio/reactivex/Single;

    move-result-object v0

    .line 116
    :goto_0
    new-instance v1, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$fetchFromCogs$1;

    invoke-direct {v1, p1}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$fetchFromCogs$1;-><init>(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string/jumbo v0, "when (props.type) {\n    \u2026ItemsFetched(props, it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$fetchFromCogs$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$fetchFromCogs$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 198
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 199
    const-class v0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ItemsFetched;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1
.end method


# virtual methods
.method public initialState(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$FetchingFromCogs;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    sget-object p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$FetchingFromCogs;->INSTANCE:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$FetchingFromCogs;

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow;->initialState(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$FetchingFromCogs;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    check-cast p2, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow;->render(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;",
            "-",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 60
    sget-object v1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$FetchingFromCogs;->INSTANCE:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$FetchingFromCogs;

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const-string v2, ""

    if-eqz v1, :cond_0

    .line 61
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow;->fetchFromCogs(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;->toString()Ljava/lang/String;

    move-result-object p2

    sget-object v1, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$1;->INSTANCE:Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p1, p2, v1}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 63
    new-instance p1, Lcom/squareup/redeemrewards/addeligible/LoadingDialogScreen;

    .line 64
    new-instance p2, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$2;

    invoke-direct {p2, v0}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 63
    invoke-direct {p1, p2}, Lcom/squareup/redeemrewards/addeligible/LoadingDialogScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 169
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 170
    const-class p3, Lcom/squareup/redeemrewards/addeligible/LoadingDialogScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 171
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 169
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 65
    sget-object p1, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 68
    :cond_0
    sget-object p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$NoItemsFoundErrorDialog;->INSTANCE:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$NoItemsFoundErrorDialog;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 69
    new-instance p1, Lcom/squareup/redeemrewards/addeligible/DiscountedItemDeletedScreen;

    .line 70
    new-instance p2, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$3;

    invoke-direct {p2, v0}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$3;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 69
    invoke-direct {p1, p2}, Lcom/squareup/redeemrewards/addeligible/DiscountedItemDeletedScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 174
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 175
    const-class p3, Lcom/squareup/redeemrewards/addeligible/DiscountedItemDeletedScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 176
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 174
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 71
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 74
    :cond_1
    instance-of p1, p2, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$ShowingAddItemDialog;

    if-eqz p1, :cond_2

    .line 75
    new-instance p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemDialog;

    .line 76
    iget-object p3, p0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/redeemrewards/addeligible/impl/R$string;->redeem_rewards_item_popup_title:I

    invoke-interface {p3, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 77
    iget-object v1, p0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/redeemrewards/addeligible/impl/R$string;->redeem_rewards_item_popup_body:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 78
    move-object v3, p2

    check-cast v3, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$ShowingAddItemDialog;

    invoke-virtual {v3}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$ShowingAddItemDialog;->getItem()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    const-string v4, "item_name"

    invoke-virtual {v1, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 79
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 80
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 81
    new-instance v3, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$4;

    invoke-direct {v3, v0, p2}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$4;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 82
    new-instance p2, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$5;

    invoke-direct {p2, v0}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$5;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 75
    invoke-direct {p1, p3, v1, v3, p2}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemDialog;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 179
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 180
    const-class p3, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemDialog;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 181
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 179
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 83
    sget-object p1, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 86
    :cond_2
    instance-of p1, p2, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$ShowingChooseEligibleItemDialog;

    if-eqz p1, :cond_3

    .line 87
    new-instance p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemDialog;

    .line 88
    iget-object p3, p0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/redeemrewards/addeligible/impl/R$string;->redeem_rewards_item_popup_title:I

    invoke-interface {p3, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 89
    iget-object v1, p0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/redeemrewards/addeligible/impl/R$string;->redeem_rewards_item_popup_body_category:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 90
    new-instance v3, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$6;

    invoke-direct {v3, v0, p2}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$6;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 91
    new-instance p2, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$7;

    invoke-direct {p2, v0}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$7;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 87
    invoke-direct {p1, p3, v1, v3, p2}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemDialog;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 184
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 185
    const-class p3, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemDialog;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 186
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 184
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 92
    sget-object p1, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 95
    :cond_3
    instance-of p1, p2, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$ShowingChooseEligibleItemScreen;

    if-eqz p1, :cond_5

    const/4 v4, 0x0

    .line 97
    check-cast p2, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$ShowingChooseEligibleItemScreen;

    invoke-virtual {p2}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$ShowingChooseEligibleItemScreen;->getItems()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 188
    new-instance p2, Ljava/util/ArrayList;

    const/16 p3, 0xa

    invoke-static {p1, p3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p3

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 189
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    .line 190
    check-cast p3, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 98
    new-instance v1, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemScreen$ItemRow;

    .line 100
    new-instance v3, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$$inlined$map$lambda$1;

    invoke-direct {v3, p3, v0}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$$inlined$map$lambda$1;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/workflow/Sink;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 98
    invoke-direct {v1, p3, v3}, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemScreen$ItemRow;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lkotlin/jvm/functions/Function0;)V

    .line 101
    invoke-interface {p2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 191
    :cond_4
    move-object v5, p2

    check-cast v5, Ljava/util/List;

    .line 103
    new-instance p1, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$9;

    invoke-direct {p1, v0}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$render$9;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function0;

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 96
    new-instance p1, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemScreen;

    move-object v3, p1

    invoke-direct/range {v3 .. v8}, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemScreen;-><init>(ZLjava/util/List;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 193
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 194
    const-class p3, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 195
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 193
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 104
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow;->snapshotState(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
