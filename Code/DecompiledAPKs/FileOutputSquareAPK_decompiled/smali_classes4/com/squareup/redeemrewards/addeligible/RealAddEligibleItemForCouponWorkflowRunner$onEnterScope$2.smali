.class final Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflowRunner$onEnterScope$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAddEligibleItemForCouponWorkflowRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAddEligibleItemForCouponWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAddEligibleItemForCouponWorkflowRunner.kt\ncom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflowRunner$onEnterScope$2\n+ 2 PosContainer.kt\ncom/squareup/ui/main/PosContainers\n*L\n1#1,42:1\n152#2:43\n*E\n*S KotlinDebug\n*F\n+ 1 RealAddEligibleItemForCouponWorkflowRunner.kt\ncom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflowRunner$onEnterScope$2\n*L\n31#1:43\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "output",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflowRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflowRunner$onEnterScope$2;->invoke(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;)V
    .locals 4

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflowRunner;->access$getContainer$p(Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    .line 43
    const-class v2, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponScope;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/squareup/ui/main/PosContainer;->goBackPast([Ljava/lang/Class;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflowRunner;->access$getLegacyOutputs$p(Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflowRunner;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;->emitOutput(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;)V

    return-void
.end method
