.class final Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$hasCouponApplied$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RedeemRewardsScopeRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->hasCouponApplied(Lcom/squareup/protos/client/coupons/Coupon;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $coupon:Lcom/squareup/protos/client/coupons/Coupon;

.field final synthetic this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Lcom/squareup/protos/client/coupons/Coupon;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$hasCouponApplied$1;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    iput-object p2, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$hasCouponApplied$1;->$coupon:Lcom/squareup/protos/client/coupons/Coupon;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$hasCouponApplied$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    .line 421
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$hasCouponApplied$1;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$hasCouponApplied$1;->$coupon:Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {v0, v1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->applyCustomerCoupon(Lcom/squareup/protos/client/coupons/Coupon;)V

    return-void
.end method
