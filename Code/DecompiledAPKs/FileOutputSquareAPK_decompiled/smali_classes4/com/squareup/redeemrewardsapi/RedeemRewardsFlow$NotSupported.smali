.class public final Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow$NotSupported;
.super Ljava/lang/Object;
.source "RedeemRewardsFlow.kt"

# interfaces
.implements Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NotSupported"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRedeemRewardsFlow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RedeemRewardsFlow.kt\ncom/squareup/redeemrewardsapi/RedeemRewardsFlow$NotSupported\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,27:1\n151#2,2:28\n*E\n*S KotlinDebug\n*F\n+ 1 RedeemRewardsFlow.kt\ncom/squareup/redeemrewardsapi/RedeemRewardsFlow$NotSupported\n*L\n25#1,2:28\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\t\u0010\u0003\u001a\u00020\u0004H\u0096\u0001J\t\u0010\u0005\u001a\u00020\u0004H\u0096\u0001\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow$NotSupported;",
        "Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;",
        "()V",
        "getFirstScreen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "getRedeemPointsScreen",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow$NotSupported;


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow$NotSupported;

    invoke-direct {v0}, Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow$NotSupported;-><init>()V

    sput-object v0, Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow$NotSupported;->INSTANCE:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow$NotSupported;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 28
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 29
    const-class v2, Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

    iput-object v0, p0, Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow$NotSupported;->$$delegate_0:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

    return-void
.end method


# virtual methods
.method public getFirstScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow$NotSupported;->$$delegate_0:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

    invoke-interface {v0}, Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;->getFirstScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getRedeemPointsScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow$NotSupported;->$$delegate_0:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

    invoke-interface {v0}, Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;->getRedeemPointsScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    return-object v0
.end method
