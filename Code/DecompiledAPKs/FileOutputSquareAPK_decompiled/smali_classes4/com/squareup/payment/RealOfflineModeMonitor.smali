.class Lcom/squareup/payment/RealOfflineModeMonitor;
.super Ljava/lang/Object;
.source "RealOfflineModeMonitor.java"

# interfaces
.implements Lcom/squareup/payment/OfflineModeMonitor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/RealOfflineModeMonitor$RecordAvailabilityTask;,
        Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;,
        Lcom/squareup/payment/RealOfflineModeMonitor$WaitingForPaymentToCompleteState;,
        Lcom/squareup/payment/RealOfflineModeMonitor$GoingOnlineState;,
        Lcom/squareup/payment/RealOfflineModeMonitor$OfflineState;,
        Lcom/squareup/payment/RealOfflineModeMonitor$OnlineState;,
        Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;
    }
.end annotation


# static fields
.field private static final CONNECTED_TO_INTERNET:Z = true

.field private static final FIRE_EVENT:Z = true

.field private static final NO_FIRE_EVENT:Z = false

.field static final PING_PERIOD_MS:I = 0x2710


# instance fields
.field private final connectivityCheck:Lcom/squareup/connectivity/check/ConnectivityCheck;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final eventSink:Lcom/squareup/badbus/BadEventSink;

.field private final executor:Ljava/util/concurrent/Executor;

.field private internetUnavailable:Z

.field private internetUnavailableIgnored:Z

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private offlineMode:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private pingTask:Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;

.field private final queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

.field private serviceState:Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final storeAndForwardKeys:Lcom/squareup/payment/offline/StoreAndForwardKeys;

.field private final transactionLazy:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/payment/offline/StoreAndForwardKeys;Ldagger/Lazy;Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/connectivity/check/ConnectivityCheck;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/payment/offline/StoreAndForwardKeys;",
            "Ldagger/Lazy<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Lcom/squareup/queue/QueueServiceStarter;",
            "Lcom/squareup/connectivity/check/ConnectivityCheck;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Lcom/squareup/payment/RealOfflineModeMonitor$OnlineState;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/payment/RealOfflineModeMonitor$OnlineState;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;Lcom/squareup/payment/RealOfflineModeMonitor$1;)V

    iput-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->serviceState:Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;

    const/4 v0, 0x0

    .line 52
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->offlineMode:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 59
    iput-object p1, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->eventSink:Lcom/squareup/badbus/BadEventSink;

    .line 60
    iput-object p2, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 61
    iput-object p3, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 62
    iput-object p4, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->executor:Ljava/util/concurrent/Executor;

    .line 63
    iput-object p5, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 64
    iput-object p6, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->storeAndForwardKeys:Lcom/squareup/payment/offline/StoreAndForwardKeys;

    .line 65
    iput-object p7, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->transactionLazy:Ldagger/Lazy;

    .line 66
    iput-object p8, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    .line 67
    iput-object p9, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->connectivityCheck:Lcom/squareup/connectivity/check/ConnectivityCheck;

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/payment/RealOfflineModeMonitor;Z)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/payment/RealOfflineModeMonitor;->recordServiceAvailable(Z)V

    return-void
.end method

.method static synthetic access$1100(Lcom/squareup/payment/RealOfflineModeMonitor;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->doPing()V

    return-void
.end method

.method static synthetic access$1200(Lcom/squareup/payment/RealOfflineModeMonitor;)Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->pingTask:Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/payment/RealOfflineModeMonitor;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->reschedulePing()V

    return-void
.end method

.method static synthetic access$1400(Lcom/squareup/payment/RealOfflineModeMonitor;Z)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/payment/RealOfflineModeMonitor;->recordServiceUnavailable(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/payment/RealOfflineModeMonitor;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->stopPingingService()V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/payment/RealOfflineModeMonitor;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->startPingingService()V

    return-void
.end method

.method static synthetic access$600(Lcom/squareup/payment/RealOfflineModeMonitor;)Z
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->hasPaymentBlockingOfflineModeExit()Z

    move-result p0

    return p0
.end method

.method static synthetic access$800(Lcom/squareup/payment/RealOfflineModeMonitor;Z)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/payment/RealOfflineModeMonitor;->restartPingingService(Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/squareup/payment/RealOfflineModeMonitor;)Lcom/squareup/thread/executor/MainThread;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-object p0
.end method

.method private doPing()V
    .locals 3

    .line 229
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->pingTask:Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;

    .line 230
    iget-object v1, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->executor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/squareup/payment/-$$Lambda$RealOfflineModeMonitor$kX1U-edAde9oOgbWqZFqD5d1eCw;

    invoke-direct {v2, p0, v0}, Lcom/squareup/payment/-$$Lambda$RealOfflineModeMonitor$kX1U-edAde9oOgbWqZFqD5d1eCw;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private fireOfflineModeEventIfChanged(Z)V
    .locals 1

    .line 246
    invoke-virtual {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    if-eq p1, v0, :cond_0

    .line 247
    invoke-virtual {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/payment/RealOfflineModeMonitor;->postOfflineModeEvent(Z)V

    if-eqz p1, :cond_0

    .line 251
    iget-object p1, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    const-string v0, "offline mode changed"

    invoke-interface {p1, v0}, Lcom/squareup/queue/QueueServiceStarter;->start(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private forceOnlineIfDisabled(Z)Z
    .locals 4

    .line 268
    invoke-direct {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->isDisabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 v0, 0x1

    .line 270
    invoke-direct {p0, v0, p1}, Lcom/squareup/payment/RealOfflineModeMonitor;->setInternetState(ZZ)V

    .line 272
    iget-object v1, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->serviceState:Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;

    instance-of v1, v1, Lcom/squareup/payment/RealOfflineModeMonitor$OnlineState;

    if-nez v1, :cond_1

    .line 273
    invoke-virtual {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->isInOfflineMode()Z

    move-result v1

    .line 274
    new-instance v2, Lcom/squareup/payment/RealOfflineModeMonitor$OnlineState;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/squareup/payment/RealOfflineModeMonitor$OnlineState;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;Lcom/squareup/payment/RealOfflineModeMonitor$1;)V

    invoke-direct {p0, v1, v2, p1}, Lcom/squareup/payment/RealOfflineModeMonitor;->setServiceState(ZLcom/squareup/payment/RealOfflineModeMonitor$ServiceState;Z)V

    :cond_1
    return v0
.end method

.method private hasPaymentBlockingOfflineModeExit()Z
    .locals 1

    .line 399
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->transactionLazy:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 400
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->blockOfflineModeExit()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isDisabled()Z
    .locals 1

    .line 281
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/StoreAndForwardSettings;->isStoreAndForwardEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/connectivity/InternetState;Lkotlin/Unit;)Lcom/squareup/connectivity/InternetState;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-object p0
.end method

.method private postOfflineModeEvent(Z)V
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance v1, Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;

    invoke-direct {v1, p1}, Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;-><init>(Z)V

    invoke-interface {v0, v1}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->offlineMode:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private recordServiceAvailable(Z)V
    .locals 3

    const/4 v0, 0x1

    .line 167
    invoke-direct {p0, v0}, Lcom/squareup/payment/RealOfflineModeMonitor;->forceOnlineIfDisabled(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 169
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->isInOfflineMode()Z

    move-result v1

    .line 170
    iget-object v2, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->serviceState:Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;

    invoke-virtual {v2, p1}, Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;->recordServiceAvailable(Z)Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;

    move-result-object p1

    invoke-direct {p0, v1, p1, v0}, Lcom/squareup/payment/RealOfflineModeMonitor;->setServiceState(ZLcom/squareup/payment/RealOfflineModeMonitor$ServiceState;Z)V

    return-void
.end method

.method private recordServiceUnavailable(Z)V
    .locals 3

    const/4 v0, 0x1

    .line 179
    invoke-direct {p0, v0}, Lcom/squareup/payment/RealOfflineModeMonitor;->forceOnlineIfDisabled(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 181
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->isInOfflineMode()Z

    move-result v1

    .line 182
    iget-object v2, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->serviceState:Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;

    invoke-virtual {v2, p1}, Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;->recordServiceUnavailable(Z)Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;

    move-result-object p1

    invoke-direct {p0, v1, p1, v0}, Lcom/squareup/payment/RealOfflineModeMonitor;->setServiceState(ZLcom/squareup/payment/RealOfflineModeMonitor$ServiceState;Z)V

    return-void
.end method

.method private reschedulePing()V
    .locals 4

    .line 220
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->pingTask:Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    const-wide/16 v2, 0x2710

    invoke-interface {v1, v0, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method private restartPingingService(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 202
    iget-object p1, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->pingTask:Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;

    if-eqz p1, :cond_1

    .line 203
    :cond_0
    invoke-direct {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->stopPingingService()V

    .line 204
    invoke-direct {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->startPingingService()V

    :cond_1
    return-void
.end method

.method private setInternetState(ZZ)V
    .locals 3

    .line 135
    invoke-virtual {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr p1, v1

    .line 136
    iput-boolean p1, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->internetUnavailable:Z

    .line 138
    iget-boolean p1, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->internetUnavailable:Z

    const/4 v2, 0x0

    if-eqz p1, :cond_3

    .line 143
    iget-object p1, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->transactionLazy:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 144
    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->blockOfflineModeEntry()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez v0, :cond_2

    if-nez p1, :cond_1

    .line 146
    iget-object p1, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->storeAndForwardKeys:Lcom/squareup/payment/offline/StoreAndForwardKeys;

    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoreAndForwardKeys;->hasAllKeys()Z

    move-result p1

    if-nez p1, :cond_2

    .line 147
    :cond_1
    iput-boolean v1, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->internetUnavailableIgnored:Z

    return-void

    .line 152
    :cond_2
    new-instance p1, Lcom/squareup/payment/RealOfflineModeMonitor$OfflineState;

    const/4 v1, 0x0

    invoke-direct {p1, p0, v1}, Lcom/squareup/payment/RealOfflineModeMonitor$OfflineState;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;Lcom/squareup/payment/RealOfflineModeMonitor$1;)V

    invoke-direct {p0, v0, p1, v2}, Lcom/squareup/payment/RealOfflineModeMonitor;->setServiceState(ZLcom/squareup/payment/RealOfflineModeMonitor$ServiceState;Z)V

    goto :goto_1

    .line 155
    :cond_3
    invoke-direct {p0, v1}, Lcom/squareup/payment/RealOfflineModeMonitor;->restartPingingService(Z)V

    .line 158
    :goto_1
    iput-boolean v2, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->internetUnavailableIgnored:Z

    if-eqz p2, :cond_4

    .line 159
    invoke-direct {p0, v0}, Lcom/squareup/payment/RealOfflineModeMonitor;->fireOfflineModeEventIfChanged(Z)V

    :cond_4
    return-void
.end method

.method private setInternetStateIfEnabled(ZZ)V
    .locals 1

    .line 130
    invoke-direct {p0, p2}, Lcom/squareup/payment/RealOfflineModeMonitor;->forceOnlineIfDisabled(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 131
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/payment/RealOfflineModeMonitor;->setInternetState(ZZ)V

    return-void
.end method

.method private setServiceState(ZLcom/squareup/payment/RealOfflineModeMonitor$ServiceState;Z)V
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->serviceState:Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;

    if-eq p2, v0, :cond_0

    .line 187
    iput-object p2, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->serviceState:Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;

    .line 188
    iget-object p2, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->serviceState:Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;

    invoke-virtual {p2}, Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;->init()V

    :cond_0
    if-eqz p3, :cond_1

    .line 190
    invoke-direct {p0, p1}, Lcom/squareup/payment/RealOfflineModeMonitor;->fireOfflineModeEventIfChanged(Z)V

    :cond_1
    return-void
.end method

.method private startPingingService()V
    .locals 2

    .line 195
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->pingTask:Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;

    if-eqz v0, :cond_0

    return-void

    .line 196
    :cond_0
    new-instance v0, Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;

    invoke-direct {v0, p0}, Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;)V

    iput-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->pingTask:Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;

    .line 197
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->pingTask:Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private stopPingingService()V
    .locals 2

    .line 213
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->pingTask:Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;

    if-eqz v0, :cond_0

    .line 214
    iget-object v1, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {v1, v0}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    .line 215
    iput-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->pingTask:Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;

    :cond_0
    return-void
.end method


# virtual methods
.method public activityResumed()V
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/payment/RealOfflineModeMonitor;->setInternetStateIfEnabled(ZZ)V

    .line 86
    invoke-virtual {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/payment/RealOfflineModeMonitor;->postOfflineModeEvent(Z)V

    return-void
.end method

.method public checkEncryptor()V
    .locals 4

    .line 115
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->storeAndForwardKeys:Lcom/squareup/payment/offline/StoreAndForwardKeys;

    invoke-virtual {v0}, Lcom/squareup/payment/offline/StoreAndForwardKeys;->hasAllKeys()Z

    move-result v0

    if-nez v0, :cond_1

    .line 116
    invoke-virtual {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    .line 119
    iget-boolean v1, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->internetUnavailable:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    iput-boolean v2, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->internetUnavailableIgnored:Z

    .line 121
    :cond_0
    new-instance v1, Lcom/squareup/payment/RealOfflineModeMonitor$OnlineState;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/squareup/payment/RealOfflineModeMonitor$OnlineState;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;Lcom/squareup/payment/RealOfflineModeMonitor$1;)V

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/payment/RealOfflineModeMonitor;->setServiceState(ZLcom/squareup/payment/RealOfflineModeMonitor$ServiceState;Z)V

    :cond_1
    return-void
.end method

.method public enterOfflineMode()V
    .locals 3

    .line 99
    invoke-virtual {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    new-instance v1, Lcom/squareup/payment/RealOfflineModeMonitor$OfflineState;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/payment/RealOfflineModeMonitor$OfflineState;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;Lcom/squareup/payment/RealOfflineModeMonitor$1;)V

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/payment/RealOfflineModeMonitor;->setServiceState(ZLcom/squareup/payment/RealOfflineModeMonitor$ServiceState;Z)V

    return-void
.end method

.method public isInOfflineMode()Z
    .locals 1

    .line 95
    iget-boolean v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->internetUnavailable:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->internetUnavailableIgnored:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->serviceState:Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;

    invoke-virtual {v0}, Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;->isUnavailable()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$doPing$3$RealOfflineModeMonitor(Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;)V
    .locals 3

    .line 232
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->pingTask:Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;

    if-eq p1, v0, :cond_0

    return-void

    .line 235
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->internetUnavailable:Z

    if-eqz v0, :cond_1

    .line 236
    invoke-direct {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->reschedulePing()V

    return-void

    .line 240
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/payment/RealOfflineModeMonitor$RecordAvailabilityTask;

    iget-object v2, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->connectivityCheck:Lcom/squareup/connectivity/check/ConnectivityCheck;

    .line 241
    invoke-interface {v2}, Lcom/squareup/connectivity/check/ConnectivityCheck;->checkStatusEndpoint()Z

    move-result v2

    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/payment/RealOfflineModeMonitor$RecordAvailabilityTask;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;Z)V

    .line 240
    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$RealOfflineModeMonitor(Lcom/squareup/connectivity/InternetState;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 77
    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {p0, p1, v1}, Lcom/squareup/payment/RealOfflineModeMonitor;->setInternetStateIfEnabled(ZZ)V

    return-void
.end method

.method public synthetic lambda$registerOnBus$2$RealOfflineModeMonitor(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 103
    invoke-virtual {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->onPaymentChanged()V

    return-void
.end method

.method public offlineMode()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 126
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->offlineMode:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 71
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 73
    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 75
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/payment/-$$Lambda$RealOfflineModeMonitor$qIchCHWItivU1r1NVAgtKqZO-3E;->INSTANCE:Lcom/squareup/payment/-$$Lambda$RealOfflineModeMonitor$qIchCHWItivU1r1NVAgtKqZO-3E;

    .line 72
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/payment/-$$Lambda$RealOfflineModeMonitor$G-tYTVJ_llvLCk2B1Q-3UtYWP2A;

    invoke-direct {v1, p0}, Lcom/squareup/payment/-$$Lambda$RealOfflineModeMonitor$G-tYTVJ_llvLCk2B1Q-3UtYWP2A;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;)V

    .line 77
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 71
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    .line 81
    invoke-direct {p0}, Lcom/squareup/payment/RealOfflineModeMonitor;->stopPingingService()V

    return-void
.end method

.method onPaymentChanged()V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->transactionLazy:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/squareup/payment/RealOfflineModeMonitor;->setInternetStateIfEnabled(ZZ)V

    :cond_0
    return-void
.end method

.method public pingImmediatelyIfOffline()V
    .locals 1

    const/4 v0, 0x1

    .line 209
    invoke-direct {p0, v0}, Lcom/squareup/payment/RealOfflineModeMonitor;->restartPingingService(Z)V

    return-void
.end method

.method public registerOnBus(Lcom/squareup/badbus/BadBus;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 103
    const-class v0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {p1, v0}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/payment/-$$Lambda$RealOfflineModeMonitor$y6KlOYQbMZ8EtVQjckK7bP_fbhA;

    invoke-direct {v0, p0}, Lcom/squareup/payment/-$$Lambda$RealOfflineModeMonitor$y6KlOYQbMZ8EtVQjckK7bP_fbhA;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;)V

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method
