.class public Lcom/squareup/payment/VoidMonitor;
.super Ljava/lang/Object;
.source "VoidMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/VoidMonitor$LRIMap;
    }
.end annotation


# static fields
.field private static final MAX_VOIDS_TO_STORE:I = 0x5


# instance fields
.field private final voidedAuths:Lcom/squareup/payment/VoidMonitor$LRIMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/payment/VoidMonitor$LRIMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Lcom/squareup/payment/VoidMonitor$LRIMap;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/squareup/payment/VoidMonitor$LRIMap;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/payment/VoidMonitor;->voidedAuths:Lcom/squareup/payment/VoidMonitor$LRIMap;

    return-void
.end method


# virtual methods
.method onCaptured(Lcom/squareup/protos/client/IdPair;)V
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/squareup/payment/VoidMonitor;->voidedAuths:Lcom/squareup/payment/VoidMonitor$LRIMap;

    iget-object v1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/VoidMonitor$LRIMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    if-eqz v0, :cond_0

    .line 32
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Capture for voided auth! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " reason: "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    .line 33
    invoke-static {p1, v0}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method onDanglingAuthVoided(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/payment/VoidMonitor;->voidedAuths:Lcom/squareup/payment/VoidMonitor$LRIMap;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/payment/VoidMonitor$LRIMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
