.class public final Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting_Factory;
.super Ljava/lang/Object;
.source "AccountStatusStoreAndForwardEnabledSetting_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;)",
            "Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting_Factory;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;)Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;)",
            "Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;

    invoke-direct {v0, p0}, Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting_Factory;->newInstance(Ljavax/inject/Provider;)Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting_Factory;->get()Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;

    move-result-object v0

    return-object v0
.end method
