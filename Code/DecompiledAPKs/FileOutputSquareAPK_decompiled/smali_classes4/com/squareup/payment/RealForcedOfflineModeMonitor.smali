.class public Lcom/squareup/payment/RealForcedOfflineModeMonitor;
.super Ljava/lang/Object;
.source "RealForcedOfflineModeMonitor.java"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/payment/ForcedOfflineModeMonitor;


# static fields
.field static final PING_PERIOD_SECONDS:I = 0x1e


# instance fields
.field private final accountStatus:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private accountStatusSubscription:Lrx/Subscription;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private forceOfflineUrl:Ljava/lang/String;

.field private final httpClient:Lokhttp3/OkHttpClient;

.field private final scheduler:Lrx/Scheduler;

.field private shouldForceOffline:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lrx/Scheduler;Lcom/squareup/connectivity/ConnectivityMonitor;Lokhttp3/OkHttpClient;Lrx/Observable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Scheduler;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lokhttp3/OkHttpClient;",
            "Lrx/Observable<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;)V"
        }
    .end annotation

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->scheduler:Lrx/Scheduler;

    .line 57
    iput-object p2, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 58
    iput-object p3, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->httpClient:Lokhttp3/OkHttpClient;

    .line 59
    iput-object p4, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->accountStatus:Lrx/Observable;

    return-void
.end method

.method public static synthetic lambda$1Q6NN4Y9MGWUjhh5M2tsKceK4hY(Lcom/squareup/payment/RealForcedOfflineModeMonitor;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->updateForceOfflineUrl(Ljava/util/List;)V

    return-void
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/server/account/protos/AccountStatusResponse;)Ljava/util/List;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse;->forced_offline_mode_server_urls:Ljava/util/List;

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$1(Ljava/lang/Long;Lcom/squareup/connectivity/InternetState;)Lcom/squareup/connectivity/InternetState;
    .locals 0

    return-object p1
.end method

.method static synthetic lambda$onEnterScope$2(Lcom/squareup/connectivity/InternetState;)Ljava/lang/Boolean;
    .locals 1

    .line 80
    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private pingForceOffline()Z
    .locals 3

    .line 106
    iget-object v0, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->forceOfflineUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 113
    :cond_0
    :try_start_0
    new-instance v0, Lokhttp3/Request$Builder;

    invoke-direct {v0}, Lokhttp3/Request$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->forceOfflineUrl:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v0

    .line 114
    iget-object v2, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->httpClient:Lokhttp3/OkHttpClient;

    invoke-virtual {v2, v0}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v0

    invoke-interface {v0}, Lokhttp3/Call;->execute()Lokhttp3/Response;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Lokhttp3/Response;->isSuccessful()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 119
    invoke-virtual {v0}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object v0

    .line 120
    new-instance v2, Lcom/google/gson/JsonParser;

    invoke-direct {v2}, Lcom/google/gson/JsonParser;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/gson/JsonParser;->parse(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v0

    const-string v2, "enter_force_offline"

    .line 123
    invoke-virtual {v0, v2}, Lcom/google/gson/JsonObject;->getAsJsonPrimitive(Ljava/lang/String;)Lcom/google/gson/JsonPrimitive;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/JsonPrimitive;->getAsInt()I

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    const/4 v1, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    :goto_0
    const-string v2, "Unexpected failure processing JSON for forced offline mode."

    .line 126
    invoke-static {v0, v2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return v1
.end method

.method private updateForceOfflineUrl(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 146
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 150
    :cond_0
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 151
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->forceOfflineUrl:Ljava/lang/String;

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method protected getForceOfflineUrl()Ljava/lang/String;
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->forceOfflineUrl:Ljava/lang/String;

    return-object v0
.end method

.method public synthetic lambda$onEnterScope$3$RealForcedOfflineModeMonitor(Lcom/squareup/connectivity/InternetState;)Ljava/lang/Boolean;
    .locals 0

    .line 81
    invoke-direct {p0}, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->pingForceOffline()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    .line 65
    iget-object v0, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->accountStatus:Lrx/Observable;

    sget-object v1, Lcom/squareup/payment/-$$Lambda$RealForcedOfflineModeMonitor$aZ9iAu8Ne-8EkheiUjKPvAV748A;->INSTANCE:Lcom/squareup/payment/-$$Lambda$RealForcedOfflineModeMonitor$aZ9iAu8Ne-8EkheiUjKPvAV748A;

    .line 66
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/payment/-$$Lambda$RealForcedOfflineModeMonitor$1Q6NN4Y9MGWUjhh5M2tsKceK4hY;

    invoke-direct {v1, p0}, Lcom/squareup/payment/-$$Lambda$RealForcedOfflineModeMonitor$1Q6NN4Y9MGWUjhh5M2tsKceK4hY;-><init>(Lcom/squareup/payment/RealForcedOfflineModeMonitor;)V

    .line 68
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->accountStatusSubscription:Lrx/Subscription;

    .line 71
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->scheduler:Lrx/Scheduler;

    const-wide/16 v2, 0x1e

    .line 72
    invoke-static {v2, v3, v0, v1}, Lrx/Observable;->interval(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 75
    invoke-interface {v1}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/payment/-$$Lambda$RealForcedOfflineModeMonitor$JAewvnh6kV1HHPJB3NO3_9pQn6M;->INSTANCE:Lcom/squareup/payment/-$$Lambda$RealForcedOfflineModeMonitor$JAewvnh6kV1HHPJB3NO3_9pQn6M;

    .line 74
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 79
    sget-object v1, Lcom/squareup/payment/-$$Lambda$RealForcedOfflineModeMonitor$_q5V5xylmyq5FyIZjYMOgcaYRjU;->INSTANCE:Lcom/squareup/payment/-$$Lambda$RealForcedOfflineModeMonitor$_q5V5xylmyq5FyIZjYMOgcaYRjU;

    .line 80
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/payment/-$$Lambda$RealForcedOfflineModeMonitor$r4i4e-sq8S7VtzZmQAteHP9pw_A;

    invoke-direct {v1, p0}, Lcom/squareup/payment/-$$Lambda$RealForcedOfflineModeMonitor$r4i4e-sq8S7VtzZmQAteHP9pw_A;-><init>(Lcom/squareup/payment/RealForcedOfflineModeMonitor;)V

    .line 81
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->scheduler:Lrx/Scheduler;

    .line 82
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->shouldForceOffline:Lrx/Observable;

    .line 84
    iget-object v0, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->accountStatusSubscription:Lrx/Subscription;

    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    const/4 v0, 0x0

    .line 88
    iput-object v0, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->accountStatusSubscription:Lrx/Subscription;

    .line 89
    iput-object v0, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->shouldForceOffline:Lrx/Observable;

    return-void
.end method

.method public shouldForceOffline()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/squareup/payment/RealForcedOfflineModeMonitor;->shouldForceOffline:Lrx/Observable;

    return-object v0
.end method
