.class public Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;
.super Ljava/lang/Object;
.source "RealDanglingAuth.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/RealDanglingAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AuthorizationInfo"
.end annotation


# instance fields
.field public final amountToReportOnAutoVoid:J

.field private final billId:Lcom/squareup/protos/client/IdPair;

.field private final hasCapturedTenders:Z

.field private final tenderIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;"
        }
    .end annotation
.end field

.field private final type:Lcom/squareup/PaymentType;

.field private final value:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method constructor <init>(JLcom/squareup/protos/client/IdPair;Lcom/squareup/PaymentType;Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/PaymentType;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;Z)V"
        }
    .end annotation

    .line 290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 273
    iput-object v0, p0, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->value:Ljava/lang/String;

    .line 291
    iput-wide p1, p0, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->amountToReportOnAutoVoid:J

    .line 292
    iput-object p3, p0, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->billId:Lcom/squareup/protos/client/IdPair;

    .line 293
    iput-object p4, p0, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->type:Lcom/squareup/PaymentType;

    .line 294
    iput-object p5, p0, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->tenderIds:Ljava/util/List;

    .line 295
    iput-boolean p6, p0, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->hasCapturedTenders:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;)Lcom/squareup/PaymentType;
    .locals 0

    .line 266
    iget-object p0, p0, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->type:Lcom/squareup/PaymentType;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;)Z
    .locals 0

    .line 266
    iget-boolean p0, p0, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->hasCapturedTenders:Z

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;)Ljava/util/List;
    .locals 0

    .line 266
    iget-object p0, p0, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->tenderIds:Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public getBillId()Lcom/squareup/protos/client/IdPair;
    .locals 2

    .line 306
    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->value:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 307
    new-instance v0, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->value:Ljava/lang/String;

    .line 308
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v0

    .line 309
    invoke-virtual {v0}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    return-object v0

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->billId:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public getType()Lcom/squareup/PaymentType;
    .locals 1

    .line 300
    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->type:Lcom/squareup/PaymentType;

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/PaymentType;->CARD:Lcom/squareup/PaymentType;

    :cond_0
    return-object v0
.end method
