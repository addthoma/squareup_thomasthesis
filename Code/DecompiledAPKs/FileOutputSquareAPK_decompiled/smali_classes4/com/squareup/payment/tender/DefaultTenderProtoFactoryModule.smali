.class public abstract Lcom/squareup/payment/tender/DefaultTenderProtoFactoryModule;
.super Ljava/lang/Object;
.source "DefaultTenderProtoFactoryModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindTenderProtoFactory(Lcom/squareup/payment/tender/DefaultTenderProtoFactory;)Lcom/squareup/payment/tender/TenderProtoFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
