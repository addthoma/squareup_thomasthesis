.class synthetic Lcom/squareup/payment/tender/SmartCardTender$2;
.super Ljava/lang/Object;
.source "SmartCardTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/tender/SmartCardTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$cardreader$lcr$CrPaymentAccountType:[I

.field static final synthetic $SwitchMap$com$squareup$protos$client$bills$CardTender$AccountType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 454
    invoke-static {}, Lcom/squareup/protos/client/bills/CardTender$AccountType;->values()[Lcom/squareup/protos/client/bills/CardTender$AccountType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/payment/tender/SmartCardTender$2;->$SwitchMap$com$squareup$protos$client$bills$CardTender$AccountType:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/payment/tender/SmartCardTender$2;->$SwitchMap$com$squareup$protos$client$bills$CardTender$AccountType:[I

    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$AccountType;->UNKNOWN_ACCOUNT_TYPE:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/CardTender$AccountType;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/payment/tender/SmartCardTender$2;->$SwitchMap$com$squareup$protos$client$bills$CardTender$AccountType:[I

    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$AccountType;->SAVINGS:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/CardTender$AccountType;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/payment/tender/SmartCardTender$2;->$SwitchMap$com$squareup$protos$client$bills$CardTender$AccountType:[I

    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$AccountType;->CHECKING:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/CardTender$AccountType;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    const/4 v3, 0x4

    :try_start_3
    sget-object v4, Lcom/squareup/payment/tender/SmartCardTender$2;->$SwitchMap$com$squareup$protos$client$bills$CardTender$AccountType:[I

    sget-object v5, Lcom/squareup/protos/client/bills/CardTender$AccountType;->CREDIT:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    invoke-virtual {v5}, Lcom/squareup/protos/client/bills/CardTender$AccountType;->ordinal()I

    move-result v5

    aput v3, v4, v5
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 435
    :catch_3
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->values()[Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    move-result-object v4

    array-length v4, v4

    new-array v4, v4, [I

    sput-object v4, Lcom/squareup/payment/tender/SmartCardTender$2;->$SwitchMap$com$squareup$cardreader$lcr$CrPaymentAccountType:[I

    :try_start_4
    sget-object v4, Lcom/squareup/payment/tender/SmartCardTender$2;->$SwitchMap$com$squareup$cardreader$lcr$CrPaymentAccountType:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_DEFAULT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->ordinal()I

    move-result v5

    aput v0, v4, v5
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v0, Lcom/squareup/payment/tender/SmartCardTender$2;->$SwitchMap$com$squareup$cardreader$lcr$CrPaymentAccountType:[I

    sget-object v4, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_SAVINGS:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    invoke-virtual {v4}, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->ordinal()I

    move-result v4

    aput v1, v0, v4
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v0, Lcom/squareup/payment/tender/SmartCardTender$2;->$SwitchMap$com$squareup$cardreader$lcr$CrPaymentAccountType:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_DEBIT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v0, Lcom/squareup/payment/tender/SmartCardTender$2;->$SwitchMap$com$squareup$cardreader$lcr$CrPaymentAccountType:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_CREDIT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->ordinal()I

    move-result v1

    aput v3, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    return-void
.end method
