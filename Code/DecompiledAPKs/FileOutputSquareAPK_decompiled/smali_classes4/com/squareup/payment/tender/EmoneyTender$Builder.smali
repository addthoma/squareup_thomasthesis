.class public final Lcom/squareup/payment/tender/EmoneyTender$Builder;
.super Lcom/squareup/payment/tender/BaseCardTender$Builder;
.source "EmoneyTender.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/tender/EmoneyTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\"\u001a\u00020#H\u0016R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR\"\u0010\r\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0013\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012R\u001c\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\"\u0004\u0008\u0018\u0010\u0019R\u001c\u0010\u001a\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001b\u0010\u001c\"\u0004\u0008\u001d\u0010\u001eR\u001c\u0010\u001f\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008 \u0010\u001c\"\u0004\u0008!\u0010\u001e\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/payment/tender/EmoneyTender$Builder;",
        "Lcom/squareup/payment/tender/BaseCardTender$Builder;",
        "factory",
        "Lcom/squareup/payment/tender/TenderFactory;",
        "clientId",
        "",
        "(Lcom/squareup/payment/tender/TenderFactory;Ljava/lang/String;)V",
        "brand",
        "Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;",
        "getBrand",
        "()Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;",
        "setBrand",
        "(Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;)V",
        "brandDisplay",
        "",
        "getBrandDisplay",
        "()Ljava/lang/Integer;",
        "setBrandDisplay",
        "(Ljava/lang/Integer;)V",
        "Ljava/lang/Integer;",
        "cardData",
        "Lokio/ByteString;",
        "getCardData",
        "()Lokio/ByteString;",
        "setCardData",
        "(Lokio/ByteString;)V",
        "maskedCardNumber",
        "getMaskedCardNumber",
        "()Ljava/lang/String;",
        "setMaskedCardNumber",
        "(Ljava/lang/String;)V",
        "terminalId",
        "getTerminalId",
        "setTerminalId",
        "build",
        "Lcom/squareup/payment/tender/EmoneyTender;",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

.field private brandDisplay:Ljava/lang/Integer;

.field private cardData:Lokio/ByteString;

.field private maskedCardNumber:Ljava/lang/String;

.field private terminalId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/tender/TenderFactory;Ljava/lang/String;)V
    .locals 1

    const-string v0, "factory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    invoke-direct {p0, p1, p2}, Lcom/squareup/payment/tender/BaseCardTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/payment/tender/BaseTender;
    .locals 1

    .line 107
    invoke-virtual {p0}, Lcom/squareup/payment/tender/EmoneyTender$Builder;->build()Lcom/squareup/payment/tender/EmoneyTender;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/BaseTender;

    return-object v0
.end method

.method public build()Lcom/squareup/payment/tender/EmoneyTender;
    .locals 1

    .line 118
    new-instance v0, Lcom/squareup/payment/tender/EmoneyTender;

    invoke-direct {v0, p0}, Lcom/squareup/payment/tender/EmoneyTender;-><init>(Lcom/squareup/payment/tender/EmoneyTender$Builder;)V

    return-object v0
.end method

.method public final getBrand()Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/payment/tender/EmoneyTender$Builder;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    return-object v0
.end method

.method public final getBrandDisplay()Ljava/lang/Integer;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/payment/tender/EmoneyTender$Builder;->brandDisplay:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getCardData()Lokio/ByteString;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/payment/tender/EmoneyTender$Builder;->cardData:Lokio/ByteString;

    return-object v0
.end method

.method public final getMaskedCardNumber()Ljava/lang/String;
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/payment/tender/EmoneyTender$Builder;->maskedCardNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final getTerminalId()Ljava/lang/String;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/payment/tender/EmoneyTender$Builder;->terminalId:Ljava/lang/String;

    return-object v0
.end method

.method public final setBrand(Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;)V
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/payment/tender/EmoneyTender$Builder;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    return-void
.end method

.method public final setBrandDisplay(Ljava/lang/Integer;)V
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/payment/tender/EmoneyTender$Builder;->brandDisplay:Ljava/lang/Integer;

    return-void
.end method

.method public final setCardData(Lokio/ByteString;)V
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/payment/tender/EmoneyTender$Builder;->cardData:Lokio/ByteString;

    return-void
.end method

.method public final setMaskedCardNumber(Ljava/lang/String;)V
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/payment/tender/EmoneyTender$Builder;->maskedCardNumber:Ljava/lang/String;

    return-void
.end method

.method public final setTerminalId(Ljava/lang/String;)V
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/payment/tender/EmoneyTender$Builder;->terminalId:Ljava/lang/String;

    return-void
.end method
