.class public abstract Lcom/squareup/payment/tender/BaseCardTender;
.super Lcom/squareup/payment/tender/BaseTender;
.source "BaseCardTender.java"

# interfaces
.implements Lcom/squareup/payment/AcceptsTips;
.implements Lcom/squareup/payment/AcceptsSignature;
.implements Lcom/squareup/payment/RequiresCardAgreement;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/tender/BaseCardTender$Builder;
    }
.end annotation


# instance fields
.field private authorizationSuccessful:Z

.field protected final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final delayCapture:Z

.field private final hasAutoGratuity:Z

.field protected final isEmvCapableReaderPresent:Z

.field private final isQuickTipEnabledPref:Z

.field private final isTakingPaymentForInvoice:Z

.field protected final merchantAlwaysSkipSignature:Z

.field protected final merchantIsAllowedToNSR:Z

.field protected final merchantOptedInToNSR:Z

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private originalAmountInPartialAuth:Lcom/squareup/protos/common/Money;

.field protected responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

.field private final shouldBeReopenable:Z

.field protected final showSignatureForCardNotPresent:Z

.field protected final showSignatureForCardOnFile:Z

.field private final signOnPaperReceiptPref:Z

.field private signature:Lcom/squareup/signature/SignatureAsJson;

.field protected signatureOptionalMaxMoney:Lcom/squareup/protos/common/Money;

.field private tip:Lcom/squareup/protos/common/Money;

.field private tipPercentage:Lcom/squareup/util/Percentage;

.field protected final tipSettings:Lcom/squareup/settings/server/TipSettings;

.field private final tippingCalculator:Lcom/squareup/tipping/TippingCalculator;

.field private final usePreAuthTipping:Z

.field protected final useSeparateTippingScreen:Z


# direct methods
.method protected constructor <init>(Lcom/squareup/payment/tender/BaseCardTender$Builder;)V
    .locals 2

    .line 103
    invoke-direct {p0, p1}, Lcom/squareup/payment/tender/BaseTender;-><init>(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    .line 104
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 105
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v0}, Lcom/squareup/papersignature/PaperSignatureSettings;->shouldPrintReceiptToSign()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->signOnPaperReceiptPref:Z

    .line 106
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 107
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 108
    iget-boolean v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->isEmvCapableReaderPresent:Z

    iput-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->isEmvCapableReaderPresent:Z

    .line 109
    new-instance v0, Lcom/squareup/tipping/TippingCalculator;

    iget-object v1, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-direct {v0, v1}, Lcom/squareup/tipping/TippingCalculator;-><init>(Lcom/squareup/settings/server/TipSettings;)V

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->tippingCalculator:Lcom/squareup/tipping/TippingCalculator;

    .line 110
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v0}, Lcom/squareup/papersignature/PaperSignatureSettings;->isQuickTipEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->isQuickTipEnabledPref:Z

    .line 111
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->merchantAlwaysSkipSignature()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->merchantAlwaysSkipSignature:Z

    .line 112
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->transaction:Lcom/squareup/payment/Transaction;

    .line 113
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->merchantIsAllowedToSkipSignaturesForSmallPayments()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->merchantIsAllowedToNSR:Z

    .line 114
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->transaction:Lcom/squareup/payment/Transaction;

    .line 115
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->merchantOptedInToSkipSignaturesForSmallPayments()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->merchantOptedInToNSR:Z

    .line 116
    iget-boolean v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->showSignatureForCardNotPresent:Z

    iput-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->showSignatureForCardNotPresent:Z

    .line 117
    iget-boolean v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->showSignatureForCardOnFile:Z

    iput-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->showSignatureForCardOnFile:Z

    .line 118
    iget-boolean v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->usePreAuthTipping:Z

    iput-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->usePreAuthTipping:Z

    .line 119
    iget-boolean v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->useSeparateTippingScreen:Z

    iput-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->useSeparateTippingScreen:Z

    .line 120
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    .line 121
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->tipAmount:Lcom/squareup/protos/common/Money;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->tip:Lcom/squareup/protos/common/Money;

    .line 122
    iget-boolean v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->isTakingPaymentFromInvoice:Z

    iput-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->isTakingPaymentForInvoice:Z

    .line 123
    iget-object v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasAutoGratuity()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->hasAutoGratuity:Z

    .line 124
    iget-boolean v0, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->delayCapture:Z

    iput-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->delayCapture:Z

    .line 125
    iget-boolean p1, p1, Lcom/squareup/payment/tender/BaseCardTender$Builder;->shouldBeReopenable:Z

    iput-boolean p1, p0, Lcom/squareup/payment/tender/BaseCardTender;->shouldBeReopenable:Z

    return-void
.end method

.method private getRemainingBalanceText(Lcom/squareup/util/Res;I)Ljava/lang/String;
    .locals 2

    .line 281
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getRemainingBalanceMinusTip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 284
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getCardBrandResources()Lcom/squareup/text/CardBrandResources;

    move-result-object v1

    iget v1, v1, Lcom/squareup/text/CardBrandResources;->buyerBrandNameId:I

    .line 285
    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 286
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "brand"

    invoke-virtual {p2, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/payment/tender/BaseCardTender;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 287
    invoke-interface {p2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v0, "amount"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 288
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 289
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getShortBrandName()Ljava/lang/String;
    .locals 2

    .line 505
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getCardBrandResources()Lcom/squareup/text/CardBrandResources;

    move-result-object v0

    iget v0, v0, Lcom/squareup/text/CardBrandResources;->shortBrandNameId:I

    .line 506
    iget-object v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected appendEmvSection()Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;
    .locals 2

    .line 416
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;-><init>()V

    iget-boolean v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->isEmvCapableReaderPresent:Z

    .line 417
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->is_emv_capable_reader_present(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;

    move-result-object v0

    .line 419
    iget-object v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    if-nez v1, :cond_0

    return-object v0

    .line 424
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->getFirmwareVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->write_only_reader_firmware_version(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 425
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->getHardwareSerialNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->write_only_hardware_serial_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;

    move-result-object v0

    return-object v0
.end method

.method public abstract askForSignature()Z
.end method

.method public askForTip()Z
    .locals 3

    .line 393
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->tippingCalculator:Lcom/squareup/tipping/TippingCalculator;

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getRemainingBalance()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/tipping/TippingCalculator;->withRemainingBalance(Lcom/squareup/protos/common/Money;)Lcom/squareup/tipping/TippingCalculator;

    move-result-object v0

    .line 394
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getAmountForTipping()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-boolean v2, p0, Lcom/squareup/payment/tender/BaseCardTender;->hasAutoGratuity:Z

    invoke-virtual {v0, v1, v2}, Lcom/squareup/tipping/TippingCalculator;->askForTip(Lcom/squareup/protos/common/Money;Z)Z

    move-result v0

    return v0
.end method

.method public abstract canQuickCapture()Z
.end method

.method public captureAndCreateReceipt(Lcom/squareup/payment/PaymentReceipt$Factory;)Lcom/squareup/payment/PaymentReceipt;
    .locals 2

    .line 264
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getPayer()Lcom/squareup/payment/Payer;

    move-result-object v0

    .line 265
    invoke-virtual {v0}, Lcom/squareup/payment/Payer;->getEmail()Lcom/squareup/payment/Obfuscated;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/payment/Payer;->getPhone()Lcom/squareup/payment/Obfuscated;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Lcom/squareup/payment/PaymentReceipt$Factory;->createTenderReceipt(Lcom/squareup/payment/Obfuscated;Lcom/squareup/payment/Obfuscated;)Lcom/squareup/payment/PaymentReceipt;

    move-result-object p1

    return-object p1
.end method

.method public createEncodedSignatureData()Ljava/lang/String;
    .locals 1

    .line 382
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->signature:Lcom/squareup/signature/SignatureAsJson;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 385
    :cond_0
    invoke-interface {v0}, Lcom/squareup/signature/SignatureAsJson;->encode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public enqueueSignature()V
    .locals 3

    .line 375
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->createEncodedSignatureData()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 377
    iget-object v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getServerId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/squareup/queue/Sign;->payment(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/queue/Sign;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public getAmountForTipping()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 535
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->isUsingTipPreTax()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getAmountWithoutTax()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0

    .line 538
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getAuthorizationCode()Ljava/lang/String;
    .locals 1

    .line 412
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->requireCardTender()Lcom/squareup/protos/client/bills/CardTender;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->read_only_authorization:Lcom/squareup/protos/client/bills/CardTender$Authorization;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorization_code:Ljava/lang/String;

    return-object v0
.end method

.method protected final getAutoCaptureSignatureThreshold()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->signatureOptionalMaxMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getCardBrandResources()Lcom/squareup/text/CardBrandResources;
    .locals 1

    .line 512
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->requireTender()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-static {v0}, Lcom/squareup/card/CardBrands;->toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object v0

    .line 513
    invoke-static {v0}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object v0

    return-object v0
.end method

.method public getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;
    .locals 1

    .line 517
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-object v0
.end method

.method protected getCompleteCardTender()Lcom/squareup/protos/client/bills/CompleteCardTender;
    .locals 3

    .line 445
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;-><init>()V

    .line 447
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->tipOnPrintedReceipt()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->shouldBeReopenable:Z

    if-eqz v1, :cond_0

    goto :goto_0

    .line 451
    :cond_0
    iget-boolean v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->delayCapture:Z

    if-eqz v1, :cond_3

    .line 452
    new-instance v1, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;->FULFILLMENT:Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;

    .line 453
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;->reason(Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;)Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;

    move-result-object v1

    .line 454
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

    move-result-object v1

    .line 452
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;->delay_capture(Lcom/squareup/protos/client/bills/CardTender$DelayCapture;)Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;

    goto :goto_2

    .line 448
    :cond_1
    :goto_0
    new-instance v1, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;-><init>()V

    iget-boolean v2, p0, Lcom/squareup/payment/tender/BaseCardTender;->shouldBeReopenable:Z

    if-eqz v2, :cond_2

    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;->REOPENABLE:Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;

    goto :goto_1

    :cond_2
    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;->TIP_ON_PAPER_RECEIPT:Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;

    .line 449
    :goto_1
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;->reason(Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;)Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;

    move-result-object v1

    .line 450
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

    move-result-object v1

    .line 448
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;->delay_capture(Lcom/squareup/protos/client/bills/CardTender$DelayCapture;)Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;

    .line 457
    :cond_3
    :goto_2
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;->build()Lcom/squareup/protos/client/bills/CompleteCardTender;

    move-result-object v0

    return-object v0
.end method

.method public getCompleteDetails()Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;
    .locals 2

    .line 434
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails$Builder;-><init>()V

    .line 435
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getCompleteCardTender()Lcom/squareup/protos/client/bills/CompleteCardTender;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails$Builder;->card_details(Lcom/squareup/protos/client/bills/CompleteCardTender;)Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails$Builder;

    move-result-object v0

    .line 436
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails$Builder;->build()Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    move-result-object v0

    return-object v0
.end method

.method public getCompleteTenderDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;
    .locals 4

    .line 490
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;-><init>()V

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 491
    invoke-virtual {p0, v1}, Lcom/squareup/payment/tender/BaseCardTender;->getBuyerLocaleString(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->buyer_selected_locale(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    move-result-object v0

    .line 492
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->signOnPrintedReceipt()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_signature_line_on_paper_receipt(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    move-result-object v0

    .line 493
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->tipOnPrintedReceipt()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_tip_options_on_paper_receipt(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->isQuickTipEnabledPref:Z

    .line 494
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_quick_tip_options(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    move-result-object v0

    .line 495
    iget-object v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->tippingCalculator:Lcom/squareup/tipping/TippingCalculator;

    .line 496
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getRemainingBalanceMinusTip()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/tipping/TippingCalculator;->withRemainingBalance(Lcom/squareup/protos/common/Money;)Lcom/squareup/tipping/TippingCalculator;

    move-result-object v1

    .line 497
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getAmountForTipping()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-boolean v3, p0, Lcom/squareup/payment/tender/BaseCardTender;->hasAutoGratuity:Z

    invoke-virtual {v1, v0, v2, v3}, Lcom/squareup/tipping/TippingCalculator;->fillReceiptDisplayDetails(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;Lcom/squareup/protos/common/Money;Z)V

    .line 499
    new-instance v1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;-><init>()V

    .line 500
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;->receipt_display_details(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;

    move-result-object v0

    .line 501
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;->build()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    move-result-object v0

    return-object v0
.end method

.method public getCustomTipMaxMoney()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 403
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->tippingCalculator:Lcom/squareup/tipping/TippingCalculator;

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getRemainingBalance()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/tipping/TippingCalculator;->withRemainingBalance(Lcom/squareup/protos/common/Money;)Lcom/squareup/tipping/TippingCalculator;

    move-result-object v0

    .line 404
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getAmountForTipping()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/tipping/TippingCalculator;->customTipMaxMoney(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getDeclinedMessage()Ljava/lang/String;
    .locals 3

    .line 314
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object v0

    .line 315
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object v0

    .line 318
    iget-object v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/transaction/R$string;->split_tender_card_declined_no_digits:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/tender/BaseCardTender;->res:Lcom/squareup/util/Res;

    iget v0, v0, Lcom/squareup/text/CardBrandResources;->buyerCardPhrase:I

    .line 319
    invoke-interface {v2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "card_phrase"

    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 320
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/transaction/R$string;->split_tender_card_declined:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 324
    invoke-direct {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getShortBrandName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "brand"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 325
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getCard()Lcom/squareup/Card;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object v1

    const-string v2, "digits"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 326
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method abstract getMethod()Lcom/squareup/protos/client/bills/Tender$Method;
.end method

.method public getOriginalAmountInPartialAuth()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 339
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->originalAmountInPartialAuth:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getPayer()Lcom/squareup/payment/Payer;
    .locals 2

    .line 408
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->requireTender()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getReceiptDetails()Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/payment/Payer;->buildFrom(Lcom/squareup/protos/client/Buyer;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;)Lcom/squareup/payment/Payer;

    move-result-object v0

    return-object v0
.end method

.method public abstract getPaymentInstrument()Lcom/squareup/protos/client/bills/PaymentInstrument;
.end method

.method public getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 3

    .line 352
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object v0

    .line 353
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getCardBrandResources()Lcom/squareup/text/CardBrandResources;

    move-result-object v1

    iget v1, v1, Lcom/squareup/text/CardBrandResources;->buyerBrandNameId:I

    .line 354
    iget-object v2, p0, Lcom/squareup/payment/tender/BaseCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/CardTender$Card;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-static {p1, v1, v0, v2}, Lcom/squareup/util/ReceiptTenderNameUtils;->getReceiptCardTenderName(Lcom/squareup/util/Res;ILjava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getReceiptTenderShortName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 3

    .line 359
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object v0

    .line 360
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getCardBrandResources()Lcom/squareup/text/CardBrandResources;

    move-result-object v1

    iget v1, v1, Lcom/squareup/text/CardBrandResources;->buyerBrandNameId:I

    const/4 v2, 0x0

    .line 361
    invoke-static {p1, v1, v0, v2}, Lcom/squareup/util/ReceiptTenderNameUtils;->getReceiptCardTenderName(Lcom/squareup/util/Res;ILjava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getRemainingBalanceMinusTip()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 294
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getRemainingBalance()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 296
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 297
    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->subtractNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getRemainingBalanceTextForHud()Ljava/lang/String;
    .locals 2

    .line 277
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/transaction/R$string;->buyer_remaining_card_balance_for_hud:I

    invoke-direct {p0, v0, v1}, Lcom/squareup/payment/tender/BaseCardTender;->getRemainingBalanceText(Lcom/squareup/util/Res;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRemainingBalanceTextForReceipt(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 273
    sget v0, Lcom/squareup/transaction/R$string;->buyer_remaining_card_balance:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/payment/tender/BaseCardTender;->getRemainingBalanceText(Lcom/squareup/util/Res;I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getShortTenderMessage()Ljava/lang/String;
    .locals 3

    .line 303
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/transaction/R$string;->split_tender_card_brand_and_digits:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 304
    invoke-direct {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getShortBrandName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "brand"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 305
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getCard()Lcom/squareup/Card;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object v1

    const-string v2, "digits"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 306
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSignatureRequiredThreshold()Lcom/squareup/protos/common/Money;
    .locals 4

    .line 158
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->requireCardTender()Lcom/squareup/protos/client/bills/CardTender;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 164
    :goto_0
    iget-boolean v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->merchantAlwaysSkipSignature:Z

    if-eqz v1, :cond_1

    const-wide v0, 0x7fffffffffffffffL

    .line 165
    iget-object v2, p0, Lcom/squareup/payment/tender/BaseCardTender;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0

    .line 172
    :cond_1
    iget-boolean v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->merchantIsAllowedToNSR:Z

    const-wide/16 v2, 0x0

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->merchantOptedInToNSR:Z

    if-nez v1, :cond_3

    :cond_2
    if-nez v0, :cond_3

    .line 173
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v2, v3, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0

    .line 178
    :cond_3
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->signatureOptionalMaxMoney:Lcom/squareup/protos/common/Money;

    if-nez v0, :cond_4

    .line 179
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v2, v3, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    :cond_4
    return-object v0
.end method

.method public getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;
    .locals 1

    .line 366
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/Card;->isManual()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->CARD_NP:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->CARD:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    :goto_0
    return-object v0
.end method

.method public getTenderTypeGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 2

    .line 310
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1}, Lcom/squareup/util/ProtoGlyphs;->card(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    return-object v0
.end method

.method public getTip()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 236
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->tip:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getTipOptions()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation

    .line 398
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->tippingCalculator:Lcom/squareup/tipping/TippingCalculator;

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getRemainingBalance()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/tipping/TippingCalculator;->withRemainingBalance(Lcom/squareup/protos/common/Money;)Lcom/squareup/tipping/TippingCalculator;

    move-result-object v0

    .line 399
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getAmountForTipping()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-boolean v2, p0, Lcom/squareup/payment/tender/BaseCardTender;->hasAutoGratuity:Z

    invoke-virtual {v0, v1, v2}, Lcom/squareup/tipping/TippingCalculator;->tipOptions(Lcom/squareup/protos/common/Money;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTipPercentage()Lcom/squareup/util/Percentage;
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->tipPercentage:Lcom/squareup/util/Percentage;

    return-object v0
.end method

.method public final hasIssuerRequestForSignature()Z
    .locals 2

    .line 230
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->hasResponseCardTender()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->read_only_authorization:Lcom/squareup/protos/client/bills/CardTender$Authorization;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->signature_behavior:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;->ISSUER_REQUEST_FOR_SIGNATURE:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasResponseCardTender()Z
    .locals 1

    .line 220
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final hasSignature()Z
    .locals 1

    .line 389
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->signature:Lcom/squareup/signature/SignatureAsJson;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isAuthorizationSuccessful()Z
    .locals 1

    .line 331
    iget-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->authorizationSuccessful:Z

    return v0
.end method

.method public isEmvCapableReaderPresent()Z
    .locals 1

    .line 521
    iget-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->isEmvCapableReaderPresent:Z

    return v0
.end method

.method public isLocalTender()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isPartialAuth()Z
    .locals 1

    .line 335
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->originalAmountInPartialAuth:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected requireCardTender()Lcom/squareup/protos/client/bills/CardTender;
    .locals 2

    .line 429
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->requireTender()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    const-string v1, "cardTender"

    .line 430
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardTender;

    return-object v0
.end method

.method public requireTenderForHistory()Lcom/squareup/protos/client/bills/Tender;
    .locals 5

    .line 461
    invoke-super {p0}, Lcom/squareup/payment/tender/BaseTender;->requireTenderForHistory()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v0

    .line 463
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->tipOnPrintedReceipt()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 465
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender;->newBuilder()Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    .line 466
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Tender$Method;->newBuilder()Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v2

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    .line 467
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardTender;->newBuilder()Lcom/squareup/protos/client/bills/CardTender$Builder;

    move-result-object v0

    new-instance v3, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;-><init>()V

    iget-boolean v4, p0, Lcom/squareup/payment/tender/BaseCardTender;->shouldBeReopenable:Z

    if-eqz v4, :cond_0

    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;->REOPENABLE:Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;

    goto :goto_0

    :cond_0
    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;->TIP_ON_PAPER_RECEIPT:Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;

    .line 469
    :goto_0
    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;->reason(Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;)Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;

    move-result-object v3

    .line 470
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

    move-result-object v3

    .line 468
    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardTender$Builder;->read_only_delay_capture(Lcom/squareup/protos/client/bills/CardTender$DelayCapture;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    move-result-object v0

    .line 471
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardTender$Builder;->build()Lcom/squareup/protos/client/bills/CardTender;

    move-result-object v0

    .line 467
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->card_tender(Lcom/squareup/protos/client/bills/CardTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v0

    .line 472
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v0

    .line 466
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/Tender$Builder;->method(Lcom/squareup/protos/client/bills/Tender$Method;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v0

    .line 473
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Builder;->build()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v0

    goto :goto_1

    .line 474
    :cond_1
    iget-boolean v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->delayCapture:Z

    if-eqz v1, :cond_2

    .line 475
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender;->newBuilder()Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    .line 476
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Tender$Method;->newBuilder()Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v2

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    .line 477
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardTender;->newBuilder()Lcom/squareup/protos/client/bills/CardTender$Builder;

    move-result-object v0

    new-instance v3, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;-><init>()V

    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;->FULFILLMENT:Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;

    .line 479
    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;->reason(Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Reason;)Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;

    move-result-object v3

    .line 480
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/CardTender$DelayCapture$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$DelayCapture;

    move-result-object v3

    .line 478
    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardTender$Builder;->read_only_delay_capture(Lcom/squareup/protos/client/bills/CardTender$DelayCapture;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    move-result-object v0

    .line 481
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardTender$Builder;->build()Lcom/squareup/protos/client/bills/CardTender;

    move-result-object v0

    .line 477
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->card_tender(Lcom/squareup/protos/client/bills/CardTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v0

    .line 482
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v0

    .line 476
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/Tender$Builder;->method(Lcom/squareup/protos/client/bills/Tender$Method;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v0

    .line 483
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Builder;->build()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v0

    :cond_2
    :goto_1
    return-object v0
.end method

.method public setTenderOnSuccess(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;Lcom/squareup/protos/common/Money;)V
    .locals 3

    const/4 v0, 0x1

    .line 191
    iput-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->authorizationSuccessful:Z

    .line 192
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    .line 193
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->read_only_authorization:Lcom/squareup/protos/client/bills/CardTender$Authorization;

    .line 194
    iget-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->signature_optional_max_money:Lcom/squareup/protos/common/Money;

    iput-object v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->signatureOptionalMaxMoney:Lcom/squareup/protos/common/Money;

    .line 197
    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorized_money:Lcom/squareup/protos/common/Money;

    .line 198
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->isEqual(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 200
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->originalAmountInPartialAuth:Lcom/squareup/protos/common/Money;

    if-nez p3, :cond_0

    const-wide/16 v1, 0x0

    .line 202
    iget-object p3, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1, v2, p3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p3

    .line 203
    :cond_0
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->subtractNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 204
    invoke-virtual {p0, v0}, Lcom/squareup/payment/tender/BaseCardTender;->setAmount(Lcom/squareup/protos/common/Money;)V

    .line 206
    iget-object v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->originalAmountInPartialAuth:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getAmountWithoutTax()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 207
    invoke-static {v1}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 208
    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 210
    :cond_1
    invoke-virtual {p0, v0}, Lcom/squareup/payment/tender/BaseCardTender;->setAmountWithoutTax(Lcom/squareup/protos/common/Money;)V

    .line 213
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/squareup/payment/tender/BaseTender;->setTenderOnSuccess(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method public setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V
    .locals 0

    .line 244
    iput-object p1, p0, Lcom/squareup/payment/tender/BaseCardTender;->tip:Lcom/squareup/protos/common/Money;

    .line 245
    iput-object p2, p0, Lcom/squareup/payment/tender/BaseCardTender;->tipPercentage:Lcom/squareup/util/Percentage;

    return-void
.end method

.method public setVectorSignature(Lcom/squareup/signature/SignatureAsJson;)V
    .locals 0

    .line 371
    iput-object p1, p0, Lcom/squareup/payment/tender/BaseCardTender;->signature:Lcom/squareup/signature/SignatureAsJson;

    return-void
.end method

.method protected final shouldAskForSignature(Lcom/squareup/protos/common/Money;)Z
    .locals 4

    .line 131
    new-instance v0, Lcom/squareup/signature/SignatureDeterminer$Builder;

    invoke-direct {v0}, Lcom/squareup/signature/SignatureDeterminer$Builder;-><init>()V

    .line 132
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->hasIssuerRequestForSignature()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->hasIssuerRequestForSignature(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->merchantAlwaysSkipSignature:Z

    .line 133
    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->merchantAlwaysSkipSignature(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->merchantIsAllowedToNSR:Z

    .line 134
    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->merchantIsAllowedToNSR(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->merchantOptedInToNSR:Z

    .line 135
    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->merchantOptedInToNSR(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->signatureOptionalMaxMoney:Lcom/squareup/protos/common/Money;

    .line 136
    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->signatureRequiredThreshold(Lcom/squareup/protos/common/Money;)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    .line 137
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->requireCardTender()Lcom/squareup/protos/client/bills/CardTender;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v3, 0x0

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->isGiftCard(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    .line 138
    invoke-virtual {v0, v3}, Lcom/squareup/signature/SignatureDeterminer$Builder;->isFallbackSwipe(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    .line 139
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->requireCardTender()Lcom/squareup/protos/client/bills/CardTender;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender$Card;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->cardEntryMethod(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->showSignatureForCardNotPresent:Z

    .line 140
    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->showSignatureForCardNotPresent(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/payment/tender/BaseCardTender;->showSignatureForCardOnFile:Z

    .line 141
    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->showSignatureForCardOnFile(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    .line 142
    invoke-virtual {v0, v3}, Lcom/squareup/signature/SignatureDeterminer$Builder;->smartReaderPresentAndRequestsSignature(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    .line 144
    invoke-virtual {v0}, Lcom/squareup/signature/SignatureDeterminer$Builder;->build()Lcom/squareup/signature/SignatureDeterminer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/signature/SignatureDeterminer;->shouldAskForSignature(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    return p1
.end method

.method public shouldAutoSendReceipt()Z
    .locals 1

    .line 269
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getPayer()Lcom/squareup/payment/Payer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payer;->shouldAutoSendReceipt()Z

    move-result v0

    return v0
.end method

.method public signOnPrintedReceipt()Z
    .locals 2

    .line 148
    iget-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->isTakingPaymentForInvoice:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 151
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->askForSignature()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->signOnPaperReceiptPref:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->supportsPaperSigAndTip()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public tipOnPrintedReceipt()Z
    .locals 2

    .line 249
    iget-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->isTakingPaymentForInvoice:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 253
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->askForTip()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->usePreAuthTipping:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->signOnPaperReceiptPref:Z

    if-eqz v0, :cond_1

    .line 256
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->supportsPaperSigAndTip()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public useSeparateTippingScreen()Z
    .locals 1

    .line 531
    iget-boolean v0, p0, Lcom/squareup/payment/tender/BaseCardTender;->useSeparateTippingScreen:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/payment/tender/BaseCardTender;->shouldAskForSignature(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
