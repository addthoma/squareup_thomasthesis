.class public Lcom/squareup/payment/tender/MagStripeTender;
.super Lcom/squareup/payment/tender/BaseCardTender;
.source "MagStripeTender.java"


# instance fields
.field private final canSwipeChipCard:Z

.field private final card:Lcom/squareup/Card;

.field private final cardConverter:Lcom/squareup/payment/CardConverter;

.field private cardVisitor:Lcom/squareup/Card$InputType$InputTypeHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/Card$InputType$InputTypeHandler<",
            "Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/MagStripeTenderBuilder;)V
    .locals 1

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/payment/tender/BaseCardTender;-><init>(Lcom/squareup/payment/tender/BaseCardTender$Builder;)V

    .line 91
    new-instance v0, Lcom/squareup/payment/tender/MagStripeTender$1;

    invoke-direct {v0, p0}, Lcom/squareup/payment/tender/MagStripeTender$1;-><init>(Lcom/squareup/payment/tender/MagStripeTender;)V

    iput-object v0, p0, Lcom/squareup/payment/tender/MagStripeTender;->cardVisitor:Lcom/squareup/Card$InputType$InputTypeHandler;

    .line 27
    iget-boolean v0, p1, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->canSwipeChipCard:Z

    iput-boolean v0, p0, Lcom/squareup/payment/tender/MagStripeTender;->canSwipeChipCard:Z

    .line 28
    iget-object v0, p1, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->cardConverter:Lcom/squareup/payment/CardConverter;

    iput-object v0, p0, Lcom/squareup/payment/tender/MagStripeTender;->cardConverter:Lcom/squareup/payment/CardConverter;

    .line 29
    iget-object p1, p1, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->card:Lcom/squareup/Card;

    iput-object p1, p0, Lcom/squareup/payment/tender/MagStripeTender;->card:Lcom/squareup/Card;

    return-void
.end method


# virtual methods
.method public askForSignature()Z
    .locals 1

    .line 37
    invoke-virtual {p0}, Lcom/squareup/payment/tender/MagStripeTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/payment/tender/MagStripeTender;->shouldAskForSignature(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    return v0
.end method

.method public canQuickCapture()Z
    .locals 5

    .line 63
    invoke-virtual {p0}, Lcom/squareup/payment/tender/MagStripeTender;->isAuthorizationSuccessful()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/tender/MagStripeTender;->card:Lcom/squareup/Card;

    invoke-virtual {v0}, Lcom/squareup/Card;->isManual()Z

    move-result v0

    .line 69
    invoke-virtual {p0}, Lcom/squareup/payment/tender/MagStripeTender;->getAutoCaptureSignatureThreshold()Lcom/squareup/protos/common/Money;

    move-result-object v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    .line 70
    invoke-virtual {p0}, Lcom/squareup/payment/tender/MagStripeTender;->getAutoCaptureSignatureThreshold()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {p0}, Lcom/squareup/payment/tender/MagStripeTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-nez v0, :cond_2

    if-nez v2, :cond_2

    .line 72
    invoke-virtual {p0}, Lcom/squareup/payment/tender/MagStripeTender;->hasSignature()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v1, 0x1

    :cond_3
    return v1
.end method

.method public getCard()Lcom/squareup/Card;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/payment/tender/MagStripeTender;->card:Lcom/squareup/Card;

    return-object v0
.end method

.method protected getCompleteCardTender()Lcom/squareup/protos/client/bills/CompleteCardTender;
    .locals 2

    .line 76
    invoke-virtual {p0}, Lcom/squareup/payment/tender/MagStripeTender;->hasSignature()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/payment/tender/MagStripeTender;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/Card;->isManual()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 78
    :goto_1
    invoke-super {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getCompleteCardTender()Lcom/squareup/protos/client/bills/CompleteCardTender;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CompleteCardTender;->newBuilder()Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;

    move-result-object v1

    .line 79
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;->was_customer_present(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;->build()Lcom/squareup/protos/client/bills/CompleteCardTender;

    move-result-object v0

    return-object v0
.end method

.method getMethod()Lcom/squareup/protos/client/bills/Tender$Method;
    .locals 5

    .line 41
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/bills/CardTender$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CardTender$Builder;-><init>()V

    new-instance v2, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/payment/tender/MagStripeTender;->card:Lcom/squareup/Card;

    iget-object v4, p0, Lcom/squareup/payment/tender/MagStripeTender;->cardVisitor:Lcom/squareup/Card$InputType$InputTypeHandler;

    .line 44
    invoke-virtual {v3, v4}, Lcom/squareup/Card;->handleInputType(Lcom/squareup/Card$InputType$InputTypeHandler;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/payment/tender/MagStripeTender;->card:Lcom/squareup/Card;

    .line 45
    invoke-virtual {v3}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/card/CardBrands;->toBillsBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->brand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/payment/tender/MagStripeTender;->card:Lcom/squareup/Card;

    .line 46
    invoke-virtual {v3}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->pan_suffix(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/squareup/payment/tender/MagStripeTender;->canSwipeChipCard:Z

    .line 47
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->write_only_can_swipe_chip_card(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    move-result-object v2

    .line 48
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$Card;

    move-result-object v2

    .line 43
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CardTender$Builder;->card(Lcom/squareup/protos/client/bills/CardTender$Card;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    move-result-object v1

    .line 49
    invoke-virtual {p0}, Lcom/squareup/payment/tender/MagStripeTender;->appendEmvSection()Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;

    move-result-object v2

    .line 50
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$Emv;

    move-result-object v2

    .line 49
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CardTender$Builder;->emv(Lcom/squareup/protos/client/bills/CardTender$Emv;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    move-result-object v1

    .line 51
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Builder;->build()Lcom/squareup/protos/client/bills/CardTender;

    move-result-object v1

    .line 42
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->card_tender(Lcom/squareup/protos/client/bills/CardTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentInstrument()Lcom/squareup/protos/client/bills/PaymentInstrument;
    .locals 3

    .line 56
    new-instance v0, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/payment/tender/MagStripeTender;->cardConverter:Lcom/squareup/payment/CardConverter;

    iget-object v2, p0, Lcom/squareup/payment/tender/MagStripeTender;->card:Lcom/squareup/Card;

    .line 57
    invoke-virtual {v1, v2}, Lcom/squareup/payment/CardConverter;->getCardData(Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->build()Lcom/squareup/protos/client/bills/PaymentInstrument;

    move-result-object v0

    return-object v0
.end method

.method public supportsPaperSigAndTip()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
