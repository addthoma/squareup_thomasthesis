.class public interface abstract Lcom/squareup/payment/OfflineModeMonitor;
.super Ljava/lang/Object;
.source "OfflineModeMonitor.java"

# interfaces
.implements Lcom/squareup/badbus/BadBusRegistrant;
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;
    }
.end annotation


# virtual methods
.method public abstract activityResumed()V
.end method

.method public abstract checkEncryptor()V
.end method

.method public abstract enterOfflineMode()V
.end method

.method public abstract isInOfflineMode()Z
.end method

.method public abstract offlineMode()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract pingImmediatelyIfOffline()V
.end method
