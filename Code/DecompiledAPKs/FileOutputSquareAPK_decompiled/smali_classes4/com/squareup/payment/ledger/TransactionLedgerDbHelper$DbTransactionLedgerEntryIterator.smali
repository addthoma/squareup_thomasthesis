.class Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;
.super Ljava/lang/Object;
.source "TransactionLedgerDbHelper.java"

# interfaces
.implements Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DbTransactionLedgerEntryIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;",
        "Ljava/util/Iterator<",
        "Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;",
        ">;"
    }
.end annotation


# instance fields
.field private cursor:Landroid/database/Cursor;

.field private db:Landroid/database/sqlite/SQLiteDatabase;

.field private final dbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

.field private endTime:J

.field private final firstElementOffset:I

.field private startTime:J


# direct methods
.method private constructor <init>(Landroid/database/sqlite/SQLiteOpenHelper;IJJ)V
    .locals 0

    .line 318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 319
    iput-object p1, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->dbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 320
    iput p2, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->firstElementOffset:I

    .line 321
    iput-wide p3, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->startTime:J

    .line 322
    iput-wide p5, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->endTime:J

    return-void
.end method

.method synthetic constructor <init>(Landroid/database/sqlite/SQLiteOpenHelper;IJJLcom/squareup/payment/ledger/TransactionLedgerDbHelper$1;)V
    .locals 0

    .line 306
    invoke-direct/range {p0 .. p6}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;-><init>(Landroid/database/sqlite/SQLiteOpenHelper;IJJ)V

    return-void
.end method

.method private ledgerQuery()Ljava/lang/String;
    .locals 3

    .line 361
    invoke-static {}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper;->access$100()Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget v1, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->firstElementOffset:I

    const-string v2, "offset"

    .line 362
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-wide v1, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->startTime:J

    .line 363
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "start_time"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-wide v1, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->endTime:J

    .line 364
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "end_time"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 365
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private myCursor()Landroid/database/Cursor;
    .locals 3

    .line 351
    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->cursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->dbHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 353
    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->ledgerQuery()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->cursor:Landroid/database/Cursor;

    .line 355
    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 357
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->cursor:Landroid/database/Cursor;

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 339
    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 340
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 341
    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_0
    return-void
.end method

.method public hasNext()Z
    .locals 1

    .line 326
    invoke-direct {p0}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->myCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->myCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;",
            ">;"
        }
    .end annotation

    return-object p0
.end method

.method public next()Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;
    .locals 5

    .line 330
    new-instance v0, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/ISO8601Date$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/ISO8601Date$Builder;-><init>()V

    new-instance v2, Ljava/util/Date;

    iget-object v3, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->cursor:Landroid/database/Cursor;

    const/4 v4, 0x0

    .line 332
    invoke-interface {v3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/ISO8601Date$Builder;->date_string(Ljava/lang/String;)Lcom/squareup/protos/client/ISO8601Date$Builder;

    move-result-object v1

    .line 333
    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date$Builder;->build()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v1

    .line 331
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;->timestamp(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->cursor:Landroid/database/Cursor;

    const/4 v2, 0x1

    .line 334
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;->encrypted_data(Lokio/ByteString;)Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;

    move-result-object v0

    .line 335
    invoke-virtual {v0}, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry$Builder;->build()Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .line 306
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$DbTransactionLedgerEntryIterator;->next()Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;

    move-result-object v0

    return-object v0
.end method
