.class public final Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;
.super Ljava/lang/Object;
.source "TransactionLedgerModule_LoggedOut_ProvideFactoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final dataDirectoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final fileExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerUploaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerUploader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerUploader;",
            ">;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->applicationProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p2, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->clockProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p3, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->dataDirectoryProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p4, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->fileExecutorProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p5, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->gsonProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p6, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->featuresProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p7, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->transactionLedgerUploaderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerUploader;",
            ">;)",
            "Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;"
        }
    .end annotation

    .line 61
    new-instance v8, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static provideFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Ljava/io/File;Ljava/util/concurrent/Executor;Lcom/google/gson/Gson;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/ledger/TransactionLedgerUploader;)Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;
    .locals 0

    .line 67
    invoke-static/range {p0 .. p6}, Lcom/squareup/payment/ledger/TransactionLedgerModule$LoggedOut;->provideFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Ljava/io/File;Ljava/util/concurrent/Executor;Lcom/google/gson/Gson;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/ledger/TransactionLedgerUploader;)Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;
    .locals 8

    .line 53
    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->dataDirectoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/io/File;

    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->fileExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/gson/Gson;

    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->transactionLedgerUploaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/payment/ledger/TransactionLedgerUploader;

    invoke-static/range {v1 .. v7}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->provideFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Ljava/io/File;Ljava/util/concurrent/Executor;Lcom/google/gson/Gson;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/ledger/TransactionLedgerUploader;)Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOut_ProvideFactoryFactory;->get()Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;

    move-result-object v0

    return-object v0
.end method
