.class public Lcom/squareup/payment/ledger/UploadTransactionLedgerRequestBody;
.super Lokhttp3/RequestBody;
.source "UploadTransactionLedgerRequestBody.java"


# instance fields
.field private final idPair:Lcom/squareup/protos/client/IdPair;

.field private final ledgerEntryIterator:Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Lokhttp3/RequestBody;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/payment/ledger/UploadTransactionLedgerRequestBody;->idPair:Lcom/squareup/protos/client/IdPair;

    .line 21
    iput-object p2, p0, Lcom/squareup/payment/ledger/UploadTransactionLedgerRequestBody;->ledgerEntryIterator:Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;

    return-void
.end method


# virtual methods
.method public contentType()Lokhttp3/MediaType;
    .locals 1

    const-string v0, "application/x-protobuf"

    .line 25
    invoke-static {v0}, Lokhttp3/MediaType;->parse(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lokio/BufferedSink;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 30
    :try_start_0
    new-instance v0, Lcom/squareup/wire/ProtoWriter;

    invoke-direct {v0, p1}, Lcom/squareup/wire/ProtoWriter;-><init>(Lokio/BufferedSink;)V

    .line 32
    sget-object v1, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/squareup/payment/ledger/UploadTransactionLedgerRequestBody;->idPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v1, v0, v2, v3}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 33
    iget-object v1, p0, Lcom/squareup/payment/ledger/UploadTransactionLedgerRequestBody;->ledgerEntryIterator:Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;

    invoke-interface {v1}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;

    .line 34
    sget-object v3, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v4, 0x2

    invoke-virtual {v3, v0, v4, v2}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/ledger/UploadTransactionLedgerRequestBody;->ledgerEntryIterator:Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;

    invoke-interface {v0}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;->close()V

    .line 38
    invoke-interface {p1}, Lokio/BufferedSink;->close()V

    return-void

    :catchall_0
    move-exception v0

    .line 37
    iget-object v1, p0, Lcom/squareup/payment/ledger/UploadTransactionLedgerRequestBody;->ledgerEntryIterator:Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;

    invoke-interface {v1}, Lcom/squareup/payment/ledger/TransactionLedgerDbHelper$TransactionLedgerEntryIterable;->close()V

    .line 38
    invoke-interface {p1}, Lokio/BufferedSink;->close()V

    .line 39
    throw v0
.end method
