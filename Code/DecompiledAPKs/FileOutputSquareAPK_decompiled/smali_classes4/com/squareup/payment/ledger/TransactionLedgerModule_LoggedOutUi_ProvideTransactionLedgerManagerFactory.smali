.class public final Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOutUi_ProvideTransactionLedgerManagerFactory;
.super Ljava/lang/Object;
.source "TransactionLedgerModule_LoggedOutUi_ProvideTransactionLedgerManagerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOutUi_ProvideTransactionLedgerManagerFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOutUi_ProvideTransactionLedgerManagerFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOutUi_ProvideTransactionLedgerManagerFactory$InstanceHolder;->access$000()Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOutUi_ProvideTransactionLedgerManagerFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideTransactionLedgerManager()Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/payment/ledger/TransactionLedgerModule$LoggedOutUi;->provideTransactionLedgerManager()Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOutUi_ProvideTransactionLedgerManagerFactory;->provideTransactionLedgerManager()Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOutUi_ProvideTransactionLedgerManagerFactory;->get()Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;

    move-result-object v0

    return-object v0
.end method
