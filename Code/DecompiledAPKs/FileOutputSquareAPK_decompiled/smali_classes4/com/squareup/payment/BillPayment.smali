.class public Lcom/squareup/payment/BillPayment;
.super Lcom/squareup/payment/Payment;
.source "BillPayment.java"

# interfaces
.implements Lcom/squareup/payment/AcceptsTips;
.implements Lcom/squareup/payment/AcceptsSignature;
.implements Lcom/squareup/payment/RequiresAuthorization;
.implements Lcom/squareup/payment/RequiresCardAgreement;
.implements Lcom/squareup/payment/CompletedPayment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/BillPayment$LastAddedTender;,
        Lcom/squareup/payment/BillPayment$Factory;,
        Lcom/squareup/payment/BillPayment$BillPaymentStrategy;
    }
.end annotation


# instance fields
.field private addTendersDisposable:Lio/reactivex/disposables/CompositeDisposable;

.field private final addTendersRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final addTendersRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest;",
            ">;"
        }
    .end annotation
.end field

.field private addTendersResponseObservable:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final addTendersResponseObservableCompleteTrigger:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final advancedModifierLogger:Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;

.field private final apiClientId:Ljava/lang/String;

.field private authPrepared:Z

.field private final backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

.field private billId:Lcom/squareup/protos/client/IdPair;

.field private final billPaymentEvents:Lcom/squareup/payment/BillPaymentEvents;

.field private final canceledTenders:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation
.end field

.field private captured:Z

.field private final capturedTenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation
.end field

.field private final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

.field private createdAt:Ljava/util/Date;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final delayCapture:Z

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flushedTenders:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation
.end field

.field private final gson:Lcom/google/gson/Gson;

.field private lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

.field private lastDeclinedTender:Lcom/squareup/payment/tender/BaseTender;

.field private final localStrategyFactory:Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;

.field private final mainScheduler:Lrx/Scheduler;

.field private networkRequestsModifier:Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private final offlineStrategyFactory:Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;

.field private final onAddTendersResponse:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/protos/client/bills/AddedTender;",
            ">;"
        }
    .end annotation
.end field

.field private final onlineStrategyFactory:Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;

.field private final paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

.field private final paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

.field private final paymentFlowTaskProvider:Lcom/squareup/payment/PaymentFlowTaskProvider;

.field private postAuthCouponAppliedToPayment:Z

.field private final receiptFactory:Lcom/squareup/payment/PaymentReceipt$Factory;

.field private final rpcScheduler:Lrx/Scheduler;

.field private final serverClock:Lcom/squareup/account/ServerClock;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final singleTender:Z

.field private final skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

.field private final stagedTenders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation
.end field

.field private final standardReceiver:Lcom/squareup/receiving/StandardReceiver;

.field private strategy:Lcom/squareup/payment/BillPayment$BillPaymentStrategy;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final tipSettings:Lcom/squareup/settings/server/TipSettings;

.field private final transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

.field private final voidMonitor:Lcom/squareup/payment/VoidMonitor;


# direct methods
.method private constructor <init>(Lcom/squareup/payment/BillPayment$Factory;Lcom/squareup/payment/Order;ZLcom/squareup/settings/server/TipSettings;Ljava/lang/String;ZLcom/squareup/checkoutflow/services/NetworkRequestModifier;Lcom/squareup/payment/PaymentFlowTaskProvider;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/settings/server/Features;)V
    .locals 1

    .line 261
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$000(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/squareup/payment/Payment;-><init>(Lcom/squareup/payment/Order;Lcom/squareup/analytics/Analytics;)V

    const/4 p2, 0x0

    .line 127
    iput-boolean p2, p0, Lcom/squareup/payment/BillPayment;->postAuthCouponAppliedToPayment:Z

    .line 194
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->stagedTenders:Ljava/util/Map;

    .line 197
    new-instance p2, Ljava/util/LinkedHashSet;

    invoke-direct {p2}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->flushedTenders:Ljava/util/Set;

    .line 203
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->capturedTenders:Ljava/util/List;

    .line 206
    new-instance p2, Ljava/util/LinkedHashSet;

    invoke-direct {p2}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->canceledTenders:Ljava/util/Set;

    .line 210
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->addTendersRequests:Ljava/util/List;

    .line 212
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->addTendersRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 221
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->addTendersResponseObservableCompleteTrigger:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 263
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$100(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    .line 264
    iput-boolean p3, p0, Lcom/squareup/payment/BillPayment;->singleTender:Z

    const-string p2, "tipSettings"

    .line 265
    invoke-static {p4, p2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/settings/server/TipSettings;

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    .line 266
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$200(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/BackgroundCaptor;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    .line 267
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$300(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 268
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$400(Lcom/squareup/payment/BillPayment$Factory;)Lrx/Scheduler;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->mainScheduler:Lrx/Scheduler;

    .line 269
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$500(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/receiving/StandardReceiver;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    .line 270
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$600(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/PaymentReceipt$Factory;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->receiptFactory:Lcom/squareup/payment/PaymentReceipt$Factory;

    .line 271
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$700(Lcom/squareup/payment/BillPayment$Factory;)Lrx/Scheduler;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->rpcScheduler:Lrx/Scheduler;

    .line 272
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$800(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 273
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$900(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/account/ServerClock;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->serverClock:Lcom/squareup/account/ServerClock;

    .line 274
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$1000(Lcom/squareup/payment/BillPayment$Factory;)Lcom/google/gson/Gson;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->gson:Lcom/google/gson/Gson;

    .line 275
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$1100(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/OfflineModeMonitor;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    .line 276
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$1200(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    .line 277
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$1300(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/PaymentAccuracyLogger;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

    .line 278
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$1400(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->advancedModifierLogger:Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;

    .line 279
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$1500(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->onlineStrategyFactory:Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;

    .line 280
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$1600(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->offlineStrategyFactory:Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;

    .line 281
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$1700(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->localStrategyFactory:Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;

    .line 282
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$1800(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/TenderInEdit;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 283
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$1900(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    const-string p2, "apiClientId"

    .line 284
    invoke-static {p5, p2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->apiClientId:Ljava/lang/String;

    .line 285
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$2000(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/VoidMonitor;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->voidMonitor:Lcom/squareup/payment/VoidMonitor;

    .line 286
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$2100(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/BillPaymentEvents;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/BillPayment;->billPaymentEvents:Lcom/squareup/payment/BillPaymentEvents;

    .line 287
    iput-boolean p6, p0, Lcom/squareup/payment/BillPayment;->delayCapture:Z

    .line 288
    iput-object p7, p0, Lcom/squareup/payment/BillPayment;->networkRequestsModifier:Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

    .line 289
    iput-object p8, p0, Lcom/squareup/payment/BillPayment;->paymentFlowTaskProvider:Lcom/squareup/payment/PaymentFlowTaskProvider;

    .line 290
    iput-object p9, p0, Lcom/squareup/payment/BillPayment;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    .line 291
    iput-object p10, p0, Lcom/squareup/payment/BillPayment;->features:Lcom/squareup/settings/server/Features;

    .line 293
    invoke-static {p1}, Lcom/squareup/payment/BillPayment$Factory;->access$1500(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;

    move-result-object p1

    invoke-virtual {p1, p5}, Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;->create(Ljava/lang/String;)Lcom/squareup/payment/BillPaymentOnlineStrategy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/BillPayment;->strategy:Lcom/squareup/payment/BillPayment$BillPaymentStrategy;

    .line 295
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getUniqueClientId()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    if-eqz p9, :cond_1

    .line 298
    invoke-virtual {p9}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object p3

    if-eqz p3, :cond_1

    .line 299
    invoke-virtual {p9}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object p3

    iget-object p3, p3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    if-eqz p3, :cond_0

    .line 300
    invoke-virtual {p9}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    .line 302
    :cond_0
    invoke-virtual {p9}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object p3

    iget-object p3, p3, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    if-eqz p3, :cond_1

    .line 303
    invoke-virtual {p9}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 306
    :cond_1
    new-instance p3, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {p3}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 307
    invoke-virtual {p3, p1}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object p1

    .line 308
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/IdPair$Builder;->server_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object p1

    .line 309
    invoke-virtual {p1}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/BillPayment;->billId:Lcom/squareup/protos/client/IdPair;

    .line 311
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/BillPayment;->onAddTendersResponse:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/BillPayment$Factory;Lcom/squareup/payment/Order;ZLcom/squareup/settings/server/TipSettings;Ljava/lang/String;ZLcom/squareup/checkoutflow/services/NetworkRequestModifier;Lcom/squareup/payment/PaymentFlowTaskProvider;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/BillPayment$1;)V
    .locals 0

    .line 119
    invoke-direct/range {p0 .. p10}, Lcom/squareup/payment/BillPayment;-><init>(Lcom/squareup/payment/BillPayment$Factory;Lcom/squareup/payment/Order;ZLcom/squareup/settings/server/TipSettings;Ljava/lang/String;ZLcom/squareup/checkoutflow/services/NetworkRequestModifier;Lcom/squareup/payment/PaymentFlowTaskProvider;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/settings/server/Features;)V

    return-void
.end method

.method private addStagedTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/tender/BaseTender;
    .locals 3

    .line 453
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->assertNoTenderInFlight()V

    .line 455
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPayment;->isIllegalZeroTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 460
    iput-object v0, p0, Lcom/squareup/payment/BillPayment;->lastDeclinedTender:Lcom/squareup/payment/tender/BaseTender;

    .line 463
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->stagedTenders:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->flushedTenders:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 464
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->serverClock:Lcom/squareup/account/ServerClock;

    invoke-virtual {v0}, Lcom/squareup/account/ServerClock;->getCurrentTime()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/BillPayment;->createdAt:Ljava/util/Date;

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    goto :goto_0

    .line 469
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v0

    .line 472
    :goto_0
    invoke-interface {v0}, Lcom/squareup/payment/crm/HoldsCustomer;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    .line 473
    invoke-interface {v0}, Lcom/squareup/payment/crm/HoldsCustomer;->getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object v2

    .line 474
    invoke-interface {v0}, Lcom/squareup/payment/crm/HoldsCustomer;->getCustomerInstrumentDetails()Ljava/util/List;

    move-result-object v0

    .line 471
    invoke-virtual {p1, v1, v2, v0}, Lcom/squareup/payment/tender/BaseTender$Builder;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 477
    new-instance v0, Lcom/squareup/payment/BillPayment$LastAddedTender;

    iget-boolean v1, p0, Lcom/squareup/payment/BillPayment;->delayCapture:Z

    invoke-direct {v0, p1, v1}, Lcom/squareup/payment/BillPayment$LastAddedTender;-><init>(Lcom/squareup/payment/tender/BaseTender$Builder;Z)V

    iput-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    .line 478
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    .line 479
    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->startTender()V

    .line 480
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender$Builder;->buyerSelectedLocale()Ljava/util/Locale;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 481
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender$Builder;->buyerSelectedLocale()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/BaseTender;->setBuyerSelectedLanguage(Ljava/util/Locale;)V

    .line 485
    :cond_2
    iget-object p1, p0, Lcom/squareup/payment/BillPayment;->stagedTenders:Ljava/util/Map;

    iget-object v1, v0, Lcom/squareup/payment/tender/BaseTender;->clientId:Ljava/lang/String;

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486
    iget-object p1, p0, Lcom/squareup/payment/BillPayment;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-interface {p1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logAddTenderBeforeAuth(Lcom/squareup/payment/tender/BaseTender;)V

    return-object v0

    .line 456
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Tender amount must be positive"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private addTenderInfo(Ljava/util/Collection;Ljava/util/Collection;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;",
            ">;",
            "Ljava/util/Collection<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;Z)V"
        }
    .end annotation

    .line 650
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/BaseTender;

    .line 651
    new-instance v1, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;-><init>()V

    .line 652
    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->requireTender()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;

    move-result-object v1

    .line 653
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;->is_canceled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;

    move-result-object v1

    .line 654
    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;->total_charged_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;

    move-result-object v0

    .line 651
    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method private applyStrategy(Lcom/squareup/payment/BillPayment$BillPaymentStrategy;)V
    .locals 2

    .line 603
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->strategy:Lcom/squareup/payment/BillPayment$BillPaymentStrategy;

    invoke-interface {v0}, Lcom/squareup/payment/BillPayment$BillPaymentStrategy;->canExitStrategy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 606
    iput-object p1, p0, Lcom/squareup/payment/BillPayment;->strategy:Lcom/squareup/payment/BillPayment$BillPaymentStrategy;

    return-void

    .line 604
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot exit strategy: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->strategy:Lcom/squareup/payment/BillPayment$BillPaymentStrategy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private asTippingTender()Lcom/squareup/payment/AcceptsTips;
    .locals 1

    .line 1118
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->asAcceptsTips()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1119
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->asAcceptsTips()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    return-object v0

    .line 1120
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    instance-of v0, v0, Lcom/squareup/payment/AcceptsTips;

    if-eqz v0, :cond_1

    .line 1121
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    check-cast v0, Lcom/squareup/payment/AcceptsTips;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private assertNoTenderInFlight()V
    .locals 2

    .line 659
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->captured:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 660
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot start a tender while another tender is in flight."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return-void
.end method

.method private buildServerCall()V
    .locals 4

    .line 1162
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/BillPayment;->addTendersDisposable:Lio/reactivex/disposables/CompositeDisposable;

    .line 1164
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->addTendersRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->addTendersResponseObservableCompleteTrigger:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 1165
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->takeUntil(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/payment/-$$Lambda$BillPayment$6W74C8J3N0im6FGHPRnIA-Nx0uQ;

    invoke-direct {v1, p0}, Lcom/squareup/payment/-$$Lambda$BillPayment$6W74C8J3N0im6FGHPRnIA-Nx0uQ;-><init>(Lcom/squareup/payment/BillPayment;)V

    .line 1166
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const/4 v1, 0x1

    .line 1175
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/payment/BillPayment;->addTendersDisposable:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/payment/-$$Lambda$4UJYT9T8LRhDU0_KLqBZWMrZXZE;

    invoke-direct {v3, v2}, Lcom/squareup/payment/-$$Lambda$4UJYT9T8LRhDU0_KLqBZWMrZXZE;-><init>(Lio/reactivex/disposables/CompositeDisposable;)V

    .line 1176
    invoke-virtual {v0, v1, v3}, Lio/reactivex/observables/ConnectableObservable;->autoConnect(ILio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/BillPayment;->addTendersResponseObservable:Lio/reactivex/Observable;

    .line 1178
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->addTendersDisposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->addTendersResponseObservable:Lio/reactivex/Observable;

    new-instance v2, Lcom/squareup/payment/-$$Lambda$BillPayment$w_XHxMHLTw3tggHmxwSFieQMVW8;

    invoke-direct {v2, p0}, Lcom/squareup/payment/-$$Lambda$BillPayment$w_XHxMHLTw3tggHmxwSFieQMVW8;-><init>(Lcom/squareup/payment/BillPayment;)V

    .line 1179
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 1178
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private cancelTenderInDrawerShift(Lcom/squareup/payment/tender/BaseTender;)V
    .locals 3

    .line 710
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->getTenderType()Lcom/squareup/protos/client/bills/Tender$Type;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->CASH:Lcom/squareup/protos/client/bills/Tender$Type;

    if-ne v0, v1, :cond_0

    .line 711
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->CASH_TENDER_CANCELLED_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    iget-object p1, p1, Lcom/squareup/payment/tender/BaseTender;->clientId:Ljava/lang/String;

    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;->addTenderWithId(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Ljava/lang/String;)V

    goto :goto_0

    .line 713
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->getTenderType()Lcom/squareup/protos/client/bills/Tender$Type;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->OTHER:Lcom/squareup/protos/client/bills/Tender$Type;

    if-ne v0, v1, :cond_1

    .line 714
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->OTHER_TENDER_CANCELLED_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    iget-object p1, p1, Lcom/squareup/payment/tender/BaseTender;->clientId:Ljava/lang/String;

    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;->addTenderWithId(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private createAndRecordAddTendersRequest()Lcom/squareup/protos/client/bills/AddTendersRequest;
    .locals 8

    .line 524
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 526
    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    .line 527
    invoke-virtual {v1}, Lcom/squareup/settings/server/TipSettings;->isPreAuthTippingRequired()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/TipSettings;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 528
    :goto_0
    iget-object v4, p0, Lcom/squareup/payment/BillPayment;->stagedTenders:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/payment/tender/BaseTender;

    .line 529
    invoke-virtual {v5}, Lcom/squareup/payment/tender/BaseTender;->requireTender()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v6

    .line 530
    new-instance v7, Lcom/squareup/protos/client/bills/AddTender$Builder;

    invoke-direct {v7}, Lcom/squareup/protos/client/bills/AddTender$Builder;-><init>()V

    .line 531
    invoke-virtual {v7, v6}, Lcom/squareup/protos/client/bills/AddTender$Builder;->tender(Lcom/squareup/protos/client/bills/Tender;)Lcom/squareup/protos/client/bills/AddTender$Builder;

    move-result-object v6

    .line 532
    invoke-virtual {v5}, Lcom/squareup/payment/tender/BaseTender;->getPaymentInstrument()Lcom/squareup/protos/client/bills/PaymentInstrument;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/squareup/protos/client/bills/AddTender$Builder;->payment_instrument(Lcom/squareup/protos/client/bills/PaymentInstrument;)Lcom/squareup/protos/client/bills/AddTender$Builder;

    move-result-object v6

    .line 533
    invoke-virtual {v5}, Lcom/squareup/payment/tender/BaseTender;->getLogging()Lcom/squareup/protos/client/bills/AddTender$Logging;

    move-result-object v5

    invoke-virtual {v6, v5}, Lcom/squareup/protos/client/bills/AddTender$Builder;->logging(Lcom/squareup/protos/client/bills/AddTender$Logging;)Lcom/squareup/protos/client/bills/AddTender$Builder;

    move-result-object v5

    .line 534
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/squareup/protos/client/bills/AddTender$Builder;->variable_capture_possible(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/AddTender$Builder;

    move-result-object v5

    .line 535
    invoke-virtual {v5}, Lcom/squareup/protos/client/bills/AddTender$Builder;->build()Lcom/squareup/protos/client/bills/AddTender;

    move-result-object v5

    .line 530
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 538
    :cond_1
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->isCovered()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    invoke-static {v1}, Lcom/squareup/payment/BillPayment;->mayRequireSwedishRounding(Lcom/squareup/payment/BillPayment$LastAddedTender;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    .line 540
    :goto_2
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->getAmountDueForThisPaymentFlow()Lcom/squareup/protos/common/Money;

    move-result-object v1

    if-eqz v2, :cond_3

    .line 541
    invoke-static {v1}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    goto :goto_3

    :cond_3
    move-object v2, v1

    .line 542
    :goto_3
    invoke-static {v2, v1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 544
    iget-object v3, p0, Lcom/squareup/payment/BillPayment;->flushedTenders:Ljava/util/Set;

    .line 545
    invoke-direct {p0, v3}, Lcom/squareup/payment/BillPayment;->getTipMoney(Ljava/util/Collection;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/payment/BillPayment;->stagedTenders:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/squareup/payment/BillPayment;->getTipMoney(Ljava/util/Collection;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/squareup/money/MoneyMath;->sumNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 546
    invoke-static {v2, v3}, Lcom/squareup/money/MoneyMath;->sumNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 549
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v1}, Lcom/squareup/payment/OrderSnapshot;->getCartProtoForBill(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart;

    move-result-object v1

    .line 551
    new-instance v2, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/payment/BillPayment;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 552
    invoke-virtual {v3}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/payment/BillPayment;->billId:Lcom/squareup/protos/client/IdPair;

    .line 553
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object v2

    .line 554
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->add_tender(Ljava/util/List;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object v0

    .line 555
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->getTenderInfo()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_info(Ljava/util/List;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object v0

    .line 556
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object v0

    .line 557
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersRequest;

    move-result-object v0

    .line 559
    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->networkRequestsModifier:Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

    if-eqz v1, :cond_4

    .line 560
    invoke-interface {v1, v0}, Lcom/squareup/checkoutflow/services/NetworkRequestModifier;->addTenderRequestParams(Lcom/squareup/protos/client/bills/AddTendersRequest;)Lcom/squareup/checkoutflow/services/AddTendersParams;

    move-result-object v1

    .line 561
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/AddTendersRequest;->newBuilder()Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object v0

    .line 562
    invoke-virtual {v1}, Lcom/squareup/checkoutflow/services/AddTendersParams;->getTenderGroupToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_group_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object v0

    .line 563
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersRequest;

    move-result-object v0

    .line 567
    :cond_4
    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->addTendersRequests:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 569
    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-interface {v1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logAddTendersRequest(Lcom/squareup/protos/client/bills/AddTendersRequest;)V

    return-object v0
.end method

.method private dropTender(Lcom/squareup/payment/tender/BaseTender;)V
    .locals 2

    .line 698
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->stagedTenders:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/payment/tender/BaseTender;->clientId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 699
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->flushedTenders:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 700
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->capturedTenders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 701
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->canceledTenders:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 702
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->strategy:Lcom/squareup/payment/BillPayment$BillPaymentStrategy;

    invoke-interface {v0, p1}, Lcom/squareup/payment/BillPayment$BillPaymentStrategy;->onDropTender(Lcom/squareup/payment/tender/BaseTender;)V

    .line 704
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;->cashManagementEnabledAndIsOpenShift()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 705
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPayment;->cancelTenderInDrawerShift(Lcom/squareup/payment/tender/BaseTender;)V

    :cond_0
    return-void
.end method

.method private getAmountDueForThisPaymentFlow()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 1318
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PAYMENT_FLOW_USE_PAYMENT_CONFIG:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/payment/BillPayment;->postAuthCouponAppliedToPayment:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1322
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->getAmountToCollect()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0

    .line 1320
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/OrderSnapshot;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method private getTenderInfo()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;",
            ">;"
        }
    .end annotation

    .line 641
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 642
    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->flushedTenders:Ljava/util/Set;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/payment/BillPayment;->addTenderInfo(Ljava/util/Collection;Ljava/util/Collection;Z)V

    .line 643
    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->stagedTenders:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/payment/BillPayment;->addTenderInfo(Ljava/util/Collection;Ljava/util/Collection;Z)V

    .line 644
    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->canceledTenders:Ljava/util/Set;

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/payment/BillPayment;->addTenderInfo(Ljava/util/Collection;Ljava/util/Collection;Z)V

    return-object v0
.end method

.method private getTipMoney(Ljava/util/Collection;)Lcom/squareup/protos/common/Money;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    .line 632
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 633
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/tender/BaseTender;

    .line 634
    invoke-virtual {v1}, Lcom/squareup/payment/tender/BaseTender;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 635
    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->sumNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private getTotalMoney(Ljava/util/Collection;)Lcom/squareup/protos/common/Money;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    .line 621
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 622
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/tender/BaseTender;

    .line 623
    invoke-virtual {v1}, Lcom/squareup/payment/tender/BaseTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 625
    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private hasCardTenderInFlight()Z
    .locals 1

    .line 799
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    instance-of v0, v0, Lcom/squareup/payment/tender/BaseCardTender;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private hasTippingTender()Z
    .locals 1

    .line 1114
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->asTippingTender()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isCovered()Z
    .locals 3

    .line 610
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->getAmountDueForThisPaymentFlow()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 612
    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    invoke-static {v1}, Lcom/squareup/payment/BillPayment;->mayRequireSwedishRounding(Lcom/squareup/payment/BillPayment$LastAddedTender;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 613
    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 616
    :cond_0
    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->flushedTenders:Ljava/util/Set;

    invoke-direct {p0, v1}, Lcom/squareup/payment/BillPayment;->getTotalMoney(Ljava/util/Collection;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/BillPayment;->stagedTenders:Ljava/util/Map;

    .line 617
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/payment/BillPayment;->getTotalMoney(Ljava/util/Collection;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 616
    invoke-static {v1, v2}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/squareup/money/MoneyMath;->greaterThanOrEqualTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v0

    return v0
.end method

.method private isIllegalZeroTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Z
    .locals 1

    .line 379
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender$Builder;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender$Builder;->canBeZeroAmount()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private killServerCall()V
    .locals 2

    .line 1199
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->addTendersDisposable:Lio/reactivex/disposables/CompositeDisposable;

    if-eqz v0, :cond_0

    .line 1201
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->addTendersResponseObservableCompleteTrigger:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 1204
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->addTendersDisposable:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->dispose()V

    const/4 v0, 0x0

    .line 1205
    iput-object v0, p0, Lcom/squareup/payment/BillPayment;->addTendersDisposable:Lio/reactivex/disposables/CompositeDisposable;

    :cond_0
    return-void
.end method

.method public static synthetic lambda$P2Ca3rLod6dwJJOkDqjaq6GoEOU(Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/client/bills/AddTendersResponse;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPayment;->onAddTendersResponse(Lcom/squareup/protos/client/bills/AddTendersResponse;)V

    return-void
.end method

.method static synthetic lambda$null$1(Lcom/squareup/protos/client/bills/AddTendersResponse;)Ljava/lang/Boolean;
    .locals 1

    .line 1171
    iget-object v0, p0, Lcom/squareup/protos/client/bills/AddTendersResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/client/bills/AddTendersResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/bills/AddTendersResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    .line 1173
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 1171
    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private static log(Ljava/lang/String;)V
    .locals 7

    .line 1064
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    const-string v2, "CompleteBill"

    const/4 v3, 0x2

    const-string v4, "%s: %s"

    const/4 v5, 0x0

    const/16 v6, 0x800

    if-le v0, v6, :cond_0

    new-array v0, v3, [Ljava/lang/Object;

    aput-object v2, v0, v5

    .line 1065
    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v4, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1066
    invoke-virtual {p0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    new-array v0, v3, [Ljava/lang/Object;

    aput-object v2, v0, v5

    aput-object p0, v0, v1

    .line 1068
    invoke-static {v4, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private logAdvancedModifiers()V
    .locals 2

    .line 1039
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/OrderSnapshot;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->interestingItems(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1040
    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->advancedModifierLogger:Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;

    invoke-virtual {v1, v0}, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;->logModifiers(Ljava/util/List;)V

    return-void
.end method

.method private static mayRequireSwedishRounding(Lcom/squareup/payment/BillPayment$LastAddedTender;)Z
    .locals 0

    if-eqz p0, :cond_0

    .line 1457
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$LastAddedTender;->builder:Lcom/squareup/payment/tender/BaseTender$Builder;

    invoke-static {p0}, Lcom/squareup/payment/BillPayment;->mayRequireSwedishRounding(Lcom/squareup/payment/tender/BaseTender$Builder;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static mayRequireSwedishRounding(Lcom/squareup/payment/tender/BaseTender$Builder;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 1465
    :cond_0
    instance-of v1, p0, Lcom/squareup/payment/tender/CashTender$Builder;

    if-nez v1, :cond_1

    instance-of p0, p0, Lcom/squareup/payment/tender/ZeroTender$Builder;

    if-eqz p0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method private static mayRequireSwedishRounding(Lcom/squareup/payment/tender/BaseTender;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 1484
    :cond_0
    instance-of v1, p0, Lcom/squareup/payment/tender/CashTender;

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    return v2

    .line 1490
    :cond_1
    instance-of p0, p0, Lcom/squareup/payment/tender/ZeroTender;

    if-eqz p0, :cond_2

    return v2

    :cond_2
    return v0
.end method

.method private onAddTendersResponse(Lcom/squareup/protos/client/bills/AddTendersResponse;)V
    .locals 10

    .line 1222
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-interface {v0, p1}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logAddTendersResponse(Lcom/squareup/protos/client/bills/AddTendersResponse;)V

    .line 1225
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 1226
    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->billId:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iget-object v2, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1229
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v0, p0, Lcom/squareup/payment/BillPayment;->billId:Lcom/squareup/protos/client/IdPair;

    .line 1231
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->tender:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->stagedTenders:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x0

    .line 1238
    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v4, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/client/bills/AddedTender;

    .line 1239
    iget-object v6, v5, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    .line 1240
    iget-object v7, p0, Lcom/squareup/payment/BillPayment;->stagedTenders:Ljava/util/Map;

    iget-object v8, v6, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v8, v8, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/payment/tender/BaseTender;

    .line 1245
    iget-object v8, v5, Lcom/squareup/protos/client/bills/AddedTender;->status:Lcom/squareup/protos/client/Status;

    iget-object v8, v8, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1246
    iget-object v8, v5, Lcom/squareup/protos/client/bills/AddedTender;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    iget-object v9, v5, Lcom/squareup/protos/client/bills/AddedTender;->remaining_balance_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v7, v6, v8, v9}, Lcom/squareup/payment/tender/BaseTender;->setTenderOnSuccess(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;Lcom/squareup/protos/common/Money;)V

    .line 1248
    iget-object v8, p0, Lcom/squareup/payment/BillPayment;->flushedTenders:Ljava/util/Set;

    invoke-interface {v8, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1249
    instance-of v7, v7, Lcom/squareup/payment/tender/BaseCardTender;

    if-eqz v7, :cond_1

    .line 1250
    iget-object v7, p0, Lcom/squareup/payment/BillPayment;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    invoke-virtual {v7, p0}, Lcom/squareup/payment/BackgroundCaptor;->startAutoCaptureOnAuth(Lcom/squareup/payment/RequiresAuthorization;)V

    :cond_1
    move v7, v4

    move-object v4, v0

    const/4 v0, 0x1

    goto :goto_1

    .line 1253
    :cond_2
    iget-object v8, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v9, v5, Lcom/squareup/protos/client/bills/AddedTender;->remaining_balance_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v7, v8, v6, v9}, Lcom/squareup/payment/tender/BaseTender;->setTenderOnFailure(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/common/Money;)V

    .line 1254
    iget-object v8, p0, Lcom/squareup/payment/BillPayment;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    invoke-virtual {v8, p0}, Lcom/squareup/payment/BackgroundCaptor;->onFailedAuth(Lcom/squareup/payment/RequiresAuthorization;)V

    .line 1255
    invoke-virtual {v7}, Lcom/squareup/payment/tender/BaseTender;->isLocalTender()Z

    move-result v7

    if-eqz v7, :cond_3

    move v7, v4

    move-object v4, v6

    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move-object v4, v0

    const/4 v0, 0x1

    const/4 v7, 0x1

    .line 1268
    :goto_1
    iget-object v8, v6, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v8, v8, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    if-eqz v8, :cond_4

    .line 1269
    iget-object v8, v6, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v5, v5, Lcom/squareup/protos/client/bills/AddedTender;->status:Lcom/squareup/protos/client/Status;

    iget-object v5, v5, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    xor-int/2addr v5, v3

    invoke-direct {p0, v8, v5}, Lcom/squareup/payment/BillPayment;->setAddTendersRequestsServerIds(Lcom/squareup/protos/client/IdPair;Z)V

    :cond_4
    if-eqz v0, :cond_5

    .line 1272
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->stagedTenders:Ljava/util/Map;

    iget-object v5, v6, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v5, v5, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    move-object v0, v4

    move v4, v7

    goto :goto_0

    .line 1277
    :cond_6
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/OrderSnapshot;->requiresSynchronousAuthorization()Z

    move-result v1

    if-nez v1, :cond_8

    if-eqz v0, :cond_8

    if-eqz v4, :cond_7

    goto :goto_2

    .line 1280
    :cond_7
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Local tender should not have been declined: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 1281
    invoke-virtual {v0}, Lcom/squareup/protos/client/IdPair;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 1284
    :cond_8
    :goto_2
    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->tender:Ljava/util/List;

    .line 1285
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v0, v3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/AddedTender;

    .line 1286
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->onAddTendersResponse:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void

    .line 1232
    :cond_9
    new-instance v0, Ljava/lang/AssertionError;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/squareup/payment/BillPayment;->stagedTenders:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTendersResponse;->tender:Ljava/util/List;

    .line 1233
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v3

    const-string p1, "Expected %s tender responses, got %s."

    .line 1232
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1227
    :cond_a
    new-instance p1, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BillId does not match client token: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method private requireSignedTender()Lcom/squareup/payment/AcceptsSignature;
    .locals 1

    .line 1157
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/AcceptsSignature;

    return-object v0
.end method

.method private requireTippingTender()Lcom/squareup/payment/AcceptsTips;
    .locals 2

    .line 1147
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->asTippingTender()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    const-string v1, "tenderInEdit or lastAddedTender"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/AcceptsTips;

    return-object v0
.end method

.method private setAddTendersRequestsServerIds(Lcom/squareup/protos/client/IdPair;Z)V
    .locals 9

    .line 1333
    iget-object v0, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1335
    :goto_0
    iget-object v3, p0, Lcom/squareup/payment/BillPayment;->addTendersRequests:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_a

    .line 1336
    iget-object v3, p0, Lcom/squareup/payment/BillPayment;->addTendersRequests:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/AddTendersRequest;

    const/4 v4, 0x0

    .line 1340
    iget-object v5, v3, Lcom/squareup/protos/client/bills/AddTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v5, v5, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    if-nez v5, :cond_0

    .line 1341
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/AddTendersRequest;->newBuilder()Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object v4

    iget-object v5, v3, Lcom/squareup/protos/client/bills/AddTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 1342
    invoke-virtual {v5}, Lcom/squareup/protos/client/IdPair;->newBuilder()Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/payment/BillPayment;->billId:Lcom/squareup/protos/client/IdPair;

    iget-object v6, v6, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 1343
    invoke-virtual {v5, v6}, Lcom/squareup/protos/client/IdPair$Builder;->server_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v5

    .line 1344
    invoke-virtual {v5}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v5

    .line 1342
    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object v4

    :cond_0
    if-nez v4, :cond_1

    .line 1348
    iget-object v5, v3, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    goto :goto_1

    :cond_1
    iget-object v5, v4, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->add_tender:Ljava/util/List;

    :goto_1
    const/4 v6, 0x0

    .line 1349
    :goto_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    if-ge v6, v7, :cond_4

    .line 1350
    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/protos/client/bills/AddTender;

    .line 1351
    iget-object v8, v7, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v8, v8, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v8, v8, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    iget-object v8, v7, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v8, v8, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v8, v8, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    if-nez v8, :cond_3

    if-nez v4, :cond_2

    .line 1354
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/AddTendersRequest;->newBuilder()Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object v4

    .line 1355
    iget-object v5, v4, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->add_tender:Ljava/util/List;

    .line 1357
    :cond_2
    invoke-virtual {v7}, Lcom/squareup/protos/client/bills/AddTender;->newBuilder()Lcom/squareup/protos/client/bills/AddTender$Builder;

    move-result-object v8

    iget-object v7, v7, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    .line 1358
    invoke-virtual {v7}, Lcom/squareup/protos/client/bills/Tender;->newBuilder()Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v7

    .line 1359
    invoke-virtual {v7, p1}, Lcom/squareup/protos/client/bills/Tender$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v7

    .line 1360
    invoke-virtual {v7}, Lcom/squareup/protos/client/bills/Tender$Builder;->build()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v7

    .line 1358
    invoke-virtual {v8, v7}, Lcom/squareup/protos/client/bills/AddTender$Builder;->tender(Lcom/squareup/protos/client/bills/Tender;)Lcom/squareup/protos/client/bills/AddTender$Builder;

    move-result-object v7

    .line 1361
    invoke-virtual {v7}, Lcom/squareup/protos/client/bills/AddTender$Builder;->build()Lcom/squareup/protos/client/bills/AddTender;

    move-result-object v7

    .line 1357
    invoke-interface {v5, v6, v7}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_4
    :goto_3
    if-nez v4, :cond_5

    .line 1367
    iget-object v5, v3, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info:Ljava/util/List;

    goto :goto_4

    :cond_5
    iget-object v5, v4, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_info:Ljava/util/List;

    :goto_4
    const/4 v6, 0x0

    .line 1368
    :goto_5
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    if-ge v6, v7, :cond_8

    .line 1369
    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;

    .line 1370
    iget-object v8, v7, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v8, v8, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    iget-object v8, v7, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v8, v8, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    if-nez v8, :cond_7

    if-nez v4, :cond_6

    .line 1373
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/AddTendersRequest;->newBuilder()Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object v3

    .line 1374
    iget-object v5, v3, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_info:Ljava/util/List;

    move-object v4, v3

    .line 1376
    :cond_6
    invoke-virtual {v7}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;->newBuilder()Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;

    move-result-object v3

    .line 1377
    invoke-virtual {v3, p1}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;

    move-result-object v3

    .line 1378
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;->is_canceled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;

    move-result-object v3

    .line 1379
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;

    move-result-object v3

    .line 1376
    invoke-interface {v5, v6, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    :cond_8
    :goto_6
    if-eqz v4, :cond_9

    .line 1385
    iget-object v3, p0, Lcom/squareup/payment/BillPayment;->addTendersRequests:Ljava/util/List;

    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersRequest;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_a
    return-void
.end method

.method private shouldAddTenderOffline()Z
    .locals 3

    .line 1407
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->stagedTenders:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/tender/BaseTender;

    .line 1408
    invoke-virtual {v1}, Lcom/squareup/payment/tender/BaseTender;->offlineCompatible()Z

    move-result v1

    if-nez v1, :cond_0

    return v2

    .line 1412
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->strategy:Lcom/squareup/payment/BillPayment$BillPaymentStrategy;

    invoke-interface {v0}, Lcom/squareup/payment/BillPayment$BillPaymentStrategy;->hasOfflineTenders()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->useStoreAndForward()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v2, 0x1

    :cond_3
    return v2
.end method

.method private updateStrategy()V
    .locals 2

    .line 593
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->shouldAddTenderOffline()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594
    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->strategy:Lcom/squareup/payment/BillPayment$BillPaymentStrategy;

    invoke-interface {v1}, Lcom/squareup/payment/BillPayment$BillPaymentStrategy;->hasOfflineTenders()Z

    move-result v1

    if-nez v1, :cond_0

    .line 595
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->offlineStrategyFactory:Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;

    invoke-virtual {v0}, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->create()Lcom/squareup/payment/BillPaymentOfflineStrategy;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/payment/BillPayment;->applyStrategy(Lcom/squareup/payment/BillPayment$BillPaymentStrategy;)V

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    .line 598
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->onlineStrategyFactory:Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;

    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->apiClientId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;->create(Ljava/lang/String;)Lcom/squareup/payment/BillPaymentOnlineStrategy;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/payment/BillPayment;->applyStrategy(Lcom/squareup/payment/BillPayment$BillPaymentStrategy;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private useStoreAndForward()Z
    .locals 1

    .line 1398
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/OrderSnapshot;->hasGiftCardItem()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1399
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/OrderSnapshot;->hasInvoice()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    .line 1400
    invoke-interface {v0}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object v0

    .line 1401
    invoke-virtual {v0}, Lcom/squareup/settings/server/StoreAndForwardSettings;->isStoreAndForwardEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private verifyReadyForCapture(Z)V
    .locals 1

    const/4 v0, 0x1

    if-eqz p1, :cond_2

    .line 1028
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->isComplete()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/payment/BillPayment;->stagedTenders:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_1

    .line 1030
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Asked to capture before all tenders added."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 1033
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->isComplete()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 1035
    :goto_1
    iput-boolean v0, p0, Lcom/squareup/payment/BillPayment;->captured:Z

    return-void

    .line 1033
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Asked to capture without enough tenders"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public addLocalTender(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)V
    .locals 1

    .line 392
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->isSingleTender()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 395
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->hasTendered()Z

    move-result v0

    if-nez v0, :cond_0

    .line 400
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPayment;->addStagedTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/tender/BaseTender;

    .line 403
    iget-object p1, p0, Lcom/squareup/payment/BillPayment;->localStrategyFactory:Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;

    invoke-virtual {p1}, Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;->create()Lcom/squareup/payment/BillPaymentLocalStrategy;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPayment;->applyStrategy(Lcom/squareup/payment/BillPayment$BillPaymentStrategy;)V

    .line 404
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->createAndRecordAddTendersRequest()Lcom/squareup/protos/client/bills/AddTendersRequest;

    move-result-object p1

    .line 405
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->strategy:Lcom/squareup/payment/BillPayment$BillPaymentStrategy;

    invoke-interface {v0, p1}, Lcom/squareup/payment/BillPayment$BillPaymentStrategy;->addTenders(Lcom/squareup/protos/client/bills/AddTendersRequest;)Lcom/squareup/protos/client/bills/AddTendersResponse;

    move-result-object p1

    .line 406
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPayment;->onAddTendersResponse(Lcom/squareup/protos/client/bills/AddTendersResponse;)V

    return-void

    .line 396
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Attempt to add a local tender after a tender has already been added"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 393
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Only single tenders can be processed in the background."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public addTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/tender/BaseTender;
    .locals 1

    .line 410
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPayment;->addStagedTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object p1

    .line 412
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->shouldAddTenderOffline()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

    invoke-virtual {v0, p0}, Lcom/squareup/payment/PaymentAccuracyLogger;->logOfflineCheckmarkForAuthorization(Lcom/squareup/payment/Payment;)V

    goto :goto_0

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

    invoke-virtual {v0, p0}, Lcom/squareup/payment/PaymentAccuracyLogger;->logOnlineCheckmarkForAuthorization(Lcom/squareup/payment/Payment;)V

    :goto_0
    return-object p1
.end method

.method public applyCoupon(Lcom/squareup/protos/client/coupons/Coupon;Z)Lcom/squareup/payment/Order;
    .locals 1

    .line 1290
    invoke-super {p0, p1, p2}, Lcom/squareup/payment/Payment;->applyCoupon(Lcom/squareup/protos/client/coupons/Coupon;Z)Lcom/squareup/payment/Order;

    const/4 p1, 0x1

    .line 1291
    iput-boolean p1, p0, Lcom/squareup/payment/BillPayment;->postAuthCouponAppliedToPayment:Z

    .line 1292
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 1293
    iget-object p2, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    iget-object p2, p2, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {p2, p1}, Lcom/squareup/payment/tender/BaseTender;->setAmount(Lcom/squareup/protos/common/Money;)V

    .line 1301
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->getAmountDueForThisPaymentFlow()Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 1302
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/OrderSnapshot;->getAllTaxes()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 1301
    invoke-static {p2, p1, v0}, Lcom/squareup/money/MoneyMath;->prorateAmountNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 1307
    invoke-static {p2}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1, p2}, Lcom/squareup/money/MoneyMath;->greaterThanNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1309
    invoke-static {p1, p2}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 1311
    :cond_0
    iget-object p2, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    iget-object p2, p2, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {p2, p1}, Lcom/squareup/payment/tender/BaseTender;->setAmountWithoutTax(Lcom/squareup/protos/common/Money;)V

    .line 1312
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object p1

    return-object p1
.end method

.method public askForSignature()Z
    .locals 1

    .line 912
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->requireSignedTender()Lcom/squareup/payment/AcceptsSignature;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsSignature;->askForSignature()Z

    move-result v0

    return v0
.end method

.method public askForTip()Z
    .locals 1

    .line 929
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->requireTippingTender()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsTips;->askForTip()Z

    move-result v0

    return v0
.end method

.method public authorize()V
    .locals 4

    .line 509
    iget-boolean v0, p0, Lcom/squareup/payment/BillPayment;->authPrepared:Z

    const-string v1, "Must call prepareToAuthorize() before each authorize() call!"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    const/4 v0, 0x0

    .line 510
    iput-boolean v0, p0, Lcom/squareup/payment/BillPayment;->authPrepared:Z

    .line 512
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->createAndRecordAddTendersRequest()Lcom/squareup/protos/client/bills/AddTendersRequest;

    move-result-object v0

    .line 513
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 514
    iget-object v2, v0, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/AddTender;

    .line 515
    iget-object v3, v3, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 517
    :cond_0
    iget-object v2, p0, Lcom/squareup/payment/BillPayment;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    iget-object v3, p0, Lcom/squareup/payment/BillPayment;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->hasCapturedTenders()Z

    move-result v3

    invoke-virtual {v2, p0, v1, v3}, Lcom/squareup/payment/BackgroundCaptor;->writeLastAuth(Lcom/squareup/payment/RequiresAuthorization;Ljava/util/List;Z)V

    .line 519
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->updateStrategy()V

    .line 520
    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->addTendersRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public blockOfflineModeEntry()Z
    .locals 1

    .line 976
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->hasCardTenderInFlight()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    iget-boolean v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->captured:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public blockOfflineModeExit()Z
    .locals 1

    .line 980
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->strategy:Lcom/squareup/payment/BillPayment$BillPaymentStrategy;

    invoke-interface {v0}, Lcom/squareup/payment/BillPayment$BillPaymentStrategy;->hasOfflineTenders()Z

    move-result v0

    return v0
.end method

.method public canAutoCapture()Z
    .locals 1

    .line 1094
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->isCovered()Z

    move-result v0

    return v0
.end method

.method public canQuickCapture()Z
    .locals 1

    .line 1087
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    instance-of v0, v0, Lcom/squareup/payment/tender/BaseCardTender;

    if-eqz v0, :cond_0

    .line 1089
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->isCovered()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1090
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireBaseCardTenderInFlight()Lcom/squareup/payment/tender/BaseCardTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseCardTender;->canQuickCapture()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public cancel(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 1

    .line 1009
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->killServerCall()V

    .line 1010
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/payment/BackgroundCaptor;->cancelAuthorization(Lcom/squareup/payment/RequiresAuthorization;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    return-void
.end method

.method public capture(Z)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 984
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->receiptFactory:Lcom/squareup/payment/PaymentReceipt$Factory;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/BillPayment$LastAddedTender;->captureAndCreateReceipt(Lcom/squareup/payment/PaymentReceipt$Factory;)V

    .line 985
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->capturedTenders:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    iget-object v1, v1, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 987
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 988
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->voidMonitor:Lcom/squareup/payment/VoidMonitor;

    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->billId:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/VoidMonitor;->onCaptured(Lcom/squareup/protos/client/IdPair;)V

    .line 989
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->strategy:Lcom/squareup/payment/BillPayment$BillPaymentStrategy;

    invoke-interface {v0, p0, p1}, Lcom/squareup/payment/BillPayment$BillPaymentStrategy;->doCapture(Lcom/squareup/payment/BillPayment;Z)V

    .line 990
    iget-object p1, p0, Lcom/squareup/payment/BillPayment;->billPaymentEvents:Lcom/squareup/payment/BillPaymentEvents;

    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->billId:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {p1, v0}, Lcom/squareup/payment/BillPaymentEvents;->notifyBillCaptured(Lcom/squareup/protos/client/IdPair;)V

    .line 992
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/OrderSnapshot;->createTaxRuleAppliedInTransactionEvent()Lcom/squareup/log/cart/TaxRuleAppliedInTransactionEvent;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 994
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 996
    :cond_0
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->logAdvancedModifiers()V

    .line 997
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->isSingleTender()Z

    move-result p1

    if-nez p1, :cond_1

    .line 999
    iget-object p1, p0, Lcom/squareup/payment/BillPayment;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->SPLIT:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    invoke-virtual {v0}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->fullNameForLogging()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logPaymentFinished(Ljava/lang/String;)V

    :cond_1
    const/4 p1, 0x1

    return p1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public createCancelTask(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/queue/CancelTask;
    .locals 8

    .line 1072
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->hasRequestedAuthorization()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1075
    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->paymentFlowTaskProvider:Lcom/squareup/payment/PaymentFlowTaskProvider;

    iget-object v2, p0, Lcom/squareup/payment/BillPayment;->billId:Lcom/squareup/protos/client/IdPair;

    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 1076
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/payment/BillPayment;->capturedTenders:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/payment/BillPayment;->canceledTenders:Ljava/util/Set;

    iget-object v7, p0, Lcom/squareup/payment/BillPayment;->flushedTenders:Ljava/util/Set;

    move-object v3, p1

    .line 1075
    invoke-virtual/range {v1 .. v7}, Lcom/squareup/payment/PaymentFlowTaskProvider;->cancelTenders(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/String;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;)Lcom/squareup/queue/CancelTask;

    move-result-object p1

    return-object p1

    .line 1073
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Attempt to cancel a Bill that is not authorized."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public createCaptureTask(Z)Lcom/squareup/queue/CaptureTask;
    .locals 8

    .line 1044
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPayment;->verifyReadyForCapture(Z)V

    .line 1045
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->paymentFlowTaskProvider:Lcom/squareup/payment/PaymentFlowTaskProvider;

    iget-object v2, p0, Lcom/squareup/payment/BillPayment;->flushedTenders:Ljava/util/Set;

    iget-object v3, p0, Lcom/squareup/payment/BillPayment;->createdAt:Ljava/util/Date;

    .line 1046
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/payment/BillPayment;->billId:Lcom/squareup/protos/client/IdPair;

    iget-object v6, p0, Lcom/squareup/payment/BillPayment;->apiClientId:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/payment/BillPayment;->capturedTenders:Ljava/util/List;

    move v1, p1

    .line 1045
    invoke-virtual/range {v0 .. v7}, Lcom/squareup/payment/PaymentFlowTaskProvider;->createCaptureTask(ZLjava/util/Set;Ljava/util/Date;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/queue/CaptureTask;

    move-result-object p1

    return-object p1
.end method

.method createCompleteBillWithKeyException(Z)Lcom/squareup/queue/bills/CompleteBill;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 1052
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPayment;->verifyReadyForCapture(Z)V

    .line 1053
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->paymentFlowTaskProvider:Lcom/squareup/payment/PaymentFlowTaskProvider;

    iget-object v2, p0, Lcom/squareup/payment/BillPayment;->flushedTenders:Ljava/util/Set;

    iget-object v3, p0, Lcom/squareup/payment/BillPayment;->createdAt:Ljava/util/Date;

    .line 1055
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/payment/BillPayment;->billId:Lcom/squareup/protos/client/IdPair;

    iget-object v6, p0, Lcom/squareup/payment/BillPayment;->apiClientId:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/payment/BillPayment;->capturedTenders:Ljava/util/List;

    move v1, p1

    .line 1054
    invoke-virtual/range {v0 .. v7}, Lcom/squareup/payment/PaymentFlowTaskProvider;->createCompleteBill(ZLjava/util/Set;Ljava/util/Date;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/queue/bills/CompleteBill;

    move-result-object p1

    return-object p1
.end method

.method public dropCapturedTender(Lcom/squareup/payment/tender/BaseTender;)V
    .locals 0

    .line 694
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPayment;->dropTender(Lcom/squareup/payment/tender/BaseTender;)V

    return-void
.end method

.method public dropLastAddedTender(Z)Lcom/squareup/payment/tender/BaseTender$Builder;
    .locals 2

    .line 677
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    const-string v1, "lastAddedTender"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/BillPayment$LastAddedTender;

    const/4 v1, 0x0

    .line 678
    iput-object v1, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    .line 679
    iget-object v1, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-direct {p0, v1}, Lcom/squareup/payment/BillPayment;->dropTender(Lcom/squareup/payment/tender/BaseTender;)V

    .line 680
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->killServerCall()V

    if-eqz p1, :cond_0

    .line 684
    iget-object p1, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    iput-object p1, p0, Lcom/squareup/payment/BillPayment;->lastDeclinedTender:Lcom/squareup/payment/tender/BaseTender;

    .line 687
    :cond_0
    iget-object p1, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->builder:Lcom/squareup/payment/tender/BaseTender$Builder;

    return-object p1
.end method

.method endCurrentTenderAndCreateReceipt()Lcom/squareup/payment/PaymentReceipt;
    .locals 1

    .line 343
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment$LastAddedTender;->consumeReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    return-object v0
.end method

.method public enqueueAttachContactTask()V
    .locals 2

    .line 843
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 844
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->strategy:Lcom/squareup/payment/BillPayment$BillPaymentStrategy;

    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/payment/BillPayment$BillPaymentStrategy;->enqueueAttachContactTask(Lcom/squareup/payment/tender/BaseTender;)V

    :cond_0
    return-void
.end method

.method public enqueueReceiptIfComplete(Lcom/squareup/payment/tender/BaseTender;)V
    .locals 1

    .line 824
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 825
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->strategy:Lcom/squareup/payment/BillPayment$BillPaymentStrategy;

    invoke-interface {v0, p0, p1}, Lcom/squareup/payment/BillPayment$BillPaymentStrategy;->enqueueLastReceipt(Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;)V

    :cond_0
    return-void
.end method

.method public getAddTendersRequests()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest;",
            ">;"
        }
    .end annotation

    .line 1416
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->addTendersRequests:Ljava/util/List;

    return-object v0
.end method

.method public getAmountForTipping()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 964
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->requireTippingTender()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsTips;->getAmountForTipping()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getAuthorizationCode()Ljava/lang/String;
    .locals 1

    .line 908
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireBaseCardTenderInFlight()Lcom/squareup/payment/tender/BaseCardTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseCardTender;->getAuthorizationCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBillId()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 1080
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->billId:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public getCapturedTenders()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    .line 366
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->capturedTenders:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCard()Lcom/squareup/Card;
    .locals 1

    .line 896
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireBaseCardTenderInFlight()Lcom/squareup/payment/tender/BaseCardTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseCardTender;->getCard()Lcom/squareup/Card;

    move-result-object v0

    return-object v0
.end method

.method public getCardData()Lcom/squareup/protos/client/bills/CardData;
    .locals 1

    .line 900
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireBaseCardTenderInFlight()Lcom/squareup/payment/tender/BaseCardTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseCardTender;->getPaymentInstrument()Lcom/squareup/protos/client/bills/PaymentInstrument;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/PaymentInstrument;->card_data:Lcom/squareup/protos/client/bills/CardData;

    return-object v0
.end method

.method public getCompleteBill()Lcom/squareup/queue/bills/CompleteBill;
    .locals 1

    .line 1424
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->paymentFlowTaskProvider:Lcom/squareup/payment/PaymentFlowTaskProvider;

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentFlowTaskProvider;->completeBill()Lcom/squareup/queue/bills/CompleteBill;

    move-result-object v0

    return-object v0
.end method

.method public getCustomTipMaxMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 937
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->requireTippingTender()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsTips;->getCustomTipMaxMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 1441
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    return-object v0
.end method

.method public getCustomerId()Ljava/lang/String;
    .locals 1

    .line 1437
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getCustomerId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCustomerInstrumentDetails()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;"
        }
    .end annotation

    .line 1449
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getCustomerInstrumentDetails()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;
    .locals 1

    .line 1445
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object v0

    return-object v0
.end method

.method public getFirstAddTenderRequestCart()Lcom/squareup/protos/client/bills/Cart;
    .locals 2

    .line 581
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->addTendersRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 584
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->addTendersRequests:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/AddTendersRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/AddTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object v0
.end method

.method public getFlushedTenders()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    .line 1420
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->flushedTenders:Ljava/util/Set;

    return-object v0
.end method

.method getFlushedTendersForTest()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    .line 375
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->flushedTenders:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLastDeclinedTender()Lcom/squareup/payment/tender/BaseTender;
    .locals 1

    .line 803
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastDeclinedTender:Lcom/squareup/payment/tender/BaseTender;

    return-object v0
.end method

.method public getLocalAddAndCaptureTask(ZLcom/squareup/protos/client/bills/AddTendersRequest;)Lcom/squareup/queue/LocalPaymentsQueueTask;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 1020
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPayment;->verifyReadyForCapture(Z)V

    .line 1021
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->paymentFlowTaskProvider:Lcom/squareup/payment/PaymentFlowTaskProvider;

    iget-object v3, p0, Lcom/squareup/payment/BillPayment;->flushedTenders:Ljava/util/Set;

    iget-object v4, p0, Lcom/squareup/payment/BillPayment;->createdAt:Ljava/util/Date;

    .line 1022
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/payment/BillPayment;->billId:Lcom/squareup/protos/client/IdPair;

    iget-object v7, p0, Lcom/squareup/payment/BillPayment;->apiClientId:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/payment/BillPayment;->capturedTenders:Ljava/util/List;

    move-object v1, p2

    move v2, p1

    .line 1021
    invoke-virtual/range {v0 .. v8}, Lcom/squareup/payment/PaymentFlowTaskProvider;->getLocalQueueTask(Lcom/squareup/protos/client/bills/AddTendersRequest;ZLjava/util/Set;Ljava/util/Date;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/queue/LocalPaymentsQueueTask;

    move-result-object p1

    return-object p1
.end method

.method public getPayer()Lcom/squareup/payment/Payer;
    .locals 1

    .line 904
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireBaseCardTenderInFlight()Lcom/squareup/payment/tender/BaseCardTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseCardTender;->getPayer()Lcom/squareup/payment/Payer;

    move-result-object v0

    return-object v0
.end method

.method public getReceiptNumber()Ljava/lang/String;
    .locals 1

    .line 358
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getReceiptNumber()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 1102
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/BaseTender;->getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getRemainingAmountDue()Lcom/squareup/protos/common/Money;
    .locals 4

    .line 867
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getUnboundedRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->max(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getRemainingAmountOnTotalBill()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 859
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PAYMENT_FLOW_USE_PAYMENT_CONFIG:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 860
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->getAmountRemainingOnBill()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0

    .line 862
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getSignatureRequiredThreshold()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 916
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->requireSignedTender()Lcom/squareup/payment/AcceptsSignature;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsSignature;->getSignatureRequiredThreshold()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getTenderAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 889
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 890
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0

    .line 892
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;
    .locals 1

    .line 1106
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->isSingleTender()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1107
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->hasLastAddedTender()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    move-result-object v0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->NONE:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    :goto_0
    return-object v0

    .line 1109
    :cond_1
    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->SPLIT:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    return-object v0
.end method

.method public getTendersForLogging()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    .line 383
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->stagedTenders:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 384
    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->flushedTenders:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method public getTip()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 941
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->hasTippingTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 942
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->asTippingTender()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsTips;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getTipOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation

    .line 933
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->requireTippingTender()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsTips;->getTipOptions()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTipSettings()Lcom/squareup/settings/server/TipSettings;
    .locals 1

    .line 1470
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    return-object v0
.end method

.method public getTotal()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 884
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    iget-object v2, p0, Lcom/squareup/payment/BillPayment;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0

    .line 885
    :cond_0
    invoke-super {p0}, Lcom/squareup/payment/Payment;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getUnboundedRemainingAmountDue()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 876
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->getAmountDueForThisPaymentFlow()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 877
    iget-object v1, p0, Lcom/squareup/payment/BillPayment;->capturedTenders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/payment/tender/BaseTender;

    .line 878
    invoke-virtual {v2}, Lcom/squareup/payment/tender/BaseTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public hasCustomer()Z
    .locals 1

    .line 1428
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->hasCustomer()Z

    move-result v0

    return v0
.end method

.method public hasEmvTenderInFlight()Z
    .locals 1

    .line 740
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->hasSmartCardTenderInFlight()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireSmartCardTenderInFlight()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTender;->isDip()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasLastAddedTender()Z
    .locals 1

    .line 728
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasMagStripeTenderInFlight()Z
    .locals 1

    .line 781
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    instance-of v0, v0, Lcom/squareup/payment/tender/MagStripeTender;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasPartialAuthCardTenderInFlight()Z
    .locals 1

    .line 793
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    instance-of v0, v0, Lcom/squareup/payment/tender/BaseCardTender;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    check-cast v0, Lcom/squareup/payment/tender/BaseCardTender;

    .line 795
    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseCardTender;->isPartialAuth()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasRequestedAuthorization()Z
    .locals 1

    .line 972
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    invoke-virtual {v0}, Lcom/squareup/payment/BackgroundCaptor;->hasRequestedAuthorization()Z

    move-result v0

    return v0
.end method

.method public hasSmartCardTenderInFlight()Z
    .locals 1

    .line 736
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    instance-of v0, v0, Lcom/squareup/payment/tender/SmartCardTender;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasSmartCardTenderWithCaptureArgs()Z
    .locals 1

    .line 775
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    if-nez v0, :cond_0

    .line 776
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->hasSmartCardTenderInFlight()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 777
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireSmartCardTenderInFlight()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTender;->hasSmartCardCaptureArgs()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasTenderInFlight()Z
    .locals 1

    .line 666
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->captured:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasTendered()Z
    .locals 1

    .line 371
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->stagedTenders:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->flushedTenders:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isCaptured()Z
    .locals 1

    .line 1014
    iget-boolean v0, p0, Lcom/squareup/payment/BillPayment;->captured:Z

    return v0
.end method

.method public isComplete()Z
    .locals 6

    .line 808
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->capturedTenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 812
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 816
    iget-object v2, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    invoke-static {v2}, Lcom/squareup/payment/BillPayment;->mayRequireSwedishRounding(Lcom/squareup/payment/BillPayment$LastAddedTender;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 817
    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 820
    :cond_1
    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method isLastAddedTender(Lcom/squareup/payment/tender/BaseTender;)Z
    .locals 1

    .line 1128
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isLocalPayment()Z
    .locals 3

    .line 333
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 334
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isMagStripeTender()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 335
    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isInstrumentTender()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 337
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    if-eqz v0, :cond_2

    .line 338
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->isLocalTender()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public isSingleTender()Z
    .locals 2

    .line 319
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->capturedTenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 321
    iget-boolean v0, p0, Lcom/squareup/payment/BillPayment;->singleTender:Z

    return v0

    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 323
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isStoreAndForward()Z
    .locals 1

    .line 1393
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->strategy:Lcom/squareup/payment/BillPayment$BillPaymentStrategy;

    invoke-interface {v0}, Lcom/squareup/payment/BillPayment$BillPaymentStrategy;->hasOfflineTenders()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$buildServerCall$2$BillPayment(Lcom/squareup/protos/client/bills/AddTendersRequest;)Lio/reactivex/ObservableSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1166
    new-instance v0, Lcom/squareup/payment/-$$Lambda$BillPayment$su2_uZ5xY0OmerTm92j3t5xcw68;

    invoke-direct {v0, p0, p1}, Lcom/squareup/payment/-$$Lambda$BillPayment$su2_uZ5xY0OmerTm92j3t5xcw68;-><init>(Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/client/bills/AddTendersRequest;)V

    .line 1167
    invoke-static {v0}, Lrx/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->rpcScheduler:Lrx/Scheduler;

    .line 1168
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->mainScheduler:Lrx/Scheduler;

    .line 1169
    invoke-virtual {p1, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v1, Lcom/squareup/payment/-$$Lambda$BillPayment$QsM_JrGx2zLAbRbOLQ1rvDfeTV0;->INSTANCE:Lcom/squareup/payment/-$$Lambda$BillPayment$QsM_JrGx2zLAbRbOLQ1rvDfeTV0;

    .line 1170
    invoke-static {v0, v1}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    .line 1166
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$buildServerCall$4$BillPayment(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1179
    new-instance v0, Lcom/squareup/payment/-$$Lambda$BillPayment$P2Ca3rLod6dwJJOkDqjaq6GoEOU;

    invoke-direct {v0, p0}, Lcom/squareup/payment/-$$Lambda$BillPayment$P2Ca3rLod6dwJJOkDqjaq6GoEOU;-><init>(Lcom/squareup/payment/BillPayment;)V

    new-instance v1, Lcom/squareup/payment/-$$Lambda$BillPayment$a3xtw4HimJzkbG0pxMTTgr8BfGc;

    invoke-direct {v1, p0}, Lcom/squareup/payment/-$$Lambda$BillPayment$a3xtw4HimJzkbG0pxMTTgr8BfGc;-><init>(Lcom/squareup/payment/BillPayment;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$null$0$BillPayment(Lcom/squareup/protos/client/bills/AddTendersRequest;)Lcom/squareup/protos/client/bills/AddTendersResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1167
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->strategy:Lcom/squareup/payment/BillPayment$BillPaymentStrategy;

    invoke-interface {v0, p1}, Lcom/squareup/payment/BillPayment$BillPaymentStrategy;->addTenders(Lcom/squareup/protos/client/bills/AddTendersRequest;)Lcom/squareup/protos/client/bills/AddTendersResponse;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$3$BillPayment(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1184
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 1185
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_0

    .line 1186
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    .line 1187
    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/AddTendersResponse;

    goto :goto_0

    .line 1188
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_1

    .line 1189
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    .line 1190
    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/AddTendersResponse;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_2

    .line 1193
    invoke-direct {p0, p1}, Lcom/squareup/payment/BillPayment;->onAddTendersResponse(Lcom/squareup/protos/client/bills/AddTendersResponse;)V

    :cond_2
    return-void
.end method

.method public onAddTendersResponse()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/bills/AddedTender;",
            ">;"
        }
    .end annotation

    .line 835
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->onAddTendersResponse:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onCustomerChanged()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 1453
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->onCustomerChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onPaymentDropped()V
    .locals 2

    .line 720
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;->cashManagementEnabledAndIsOpenShift()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 721
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->capturedTenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/tender/BaseTender;

    .line 722
    invoke-direct {p0, v1}, Lcom/squareup/payment/BillPayment;->cancelTenderInDrawerShift(Lcom/squareup/payment/tender/BaseTender;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public prepareToAuthorize()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;>;"
        }
    .end annotation

    .line 498
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->killServerCall()V

    .line 499
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->buildServerCall()V

    const/4 v0, 0x1

    .line 500
    iput-boolean v0, p0, Lcom/squareup/payment/BillPayment;->authPrepared:Z

    .line 501
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->addTendersResponseObservable:Lio/reactivex/Observable;

    return-object v0
.end method

.method public promoteEmoneyTender(Lokio/ByteString;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;I)V
    .locals 1

    .line 749
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearEmoney()Lcom/squareup/payment/tender/EmoneyTender$Builder;

    move-result-object v0

    .line 751
    invoke-virtual {v0, p2}, Lcom/squareup/payment/tender/EmoneyTender$Builder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 752
    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/EmoneyTender$Builder;->setCardData(Lokio/ByteString;)V

    .line 753
    invoke-virtual {v0, p3}, Lcom/squareup/payment/tender/EmoneyTender$Builder;->setBrand(Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;)V

    .line 754
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/EmoneyTender$Builder;->setBrandDisplay(Ljava/lang/Integer;)V

    .line 756
    invoke-virtual {p0, v0}, Lcom/squareup/payment/BillPayment;->addTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/tender/BaseTender;

    return-void
.end method

.method public promoteInstrumentTender()Lcom/squareup/payment/tender/InstrumentTender;
    .locals 1

    .line 770
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearInstrumentTender()Lcom/squareup/payment/tender/InstrumentTender$Builder;

    move-result-object v0

    .line 771
    invoke-virtual {p0, v0}, Lcom/squareup/payment/BillPayment;->addTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/InstrumentTender;

    return-object v0
.end method

.method public promoteMagStripeCardTender()Lcom/squareup/payment/tender/MagStripeTender;
    .locals 1

    .line 765
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearMagStripeTender()Lcom/squareup/payment/tender/MagStripeTenderBuilder;

    move-result-object v0

    .line 766
    invoke-virtual {p0, v0}, Lcom/squareup/payment/BillPayment;->addTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/MagStripeTender;

    return-object v0
.end method

.method public promoteSmartCardTender()V
    .locals 1

    .line 760
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    .line 761
    invoke-virtual {p0, v0}, Lcom/squareup/payment/BillPayment;->addTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/tender/BaseTender;

    return-void
.end method

.method public replaceCardTenderWithZeroTender(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)V
    .locals 1

    const/4 v0, 0x0

    .line 1136
    invoke-virtual {p0, v0}, Lcom/squareup/payment/BillPayment;->dropLastAddedTender(Z)Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 1137
    invoke-virtual {p0, p1}, Lcom/squareup/payment/BillPayment;->addTender(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/tender/BaseTender;

    return-void
.end method

.method public requireBaseCardTenderInFlight()Lcom/squareup/payment/tender/BaseCardTender;
    .locals 1

    .line 789
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/BaseCardTender;

    return-object v0
.end method

.method public requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;
    .locals 2

    .line 732
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    const-string v1, "lastAddedTender"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/BillPayment$LastAddedTender;

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    return-object v0
.end method

.method public bridge synthetic requireLastAddedTender()Ljava/lang/Object;
    .locals 1

    .line 119
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    return-object v0
.end method

.method public requireMagStripeTenderInFlight()Lcom/squareup/payment/tender/MagStripeTender;
    .locals 1

    .line 785
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/MagStripeTender;

    return-object v0
.end method

.method public requireSmartCardTenderInFlight()Lcom/squareup/payment/tender/SmartCardTender;
    .locals 1

    .line 744
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/SmartCardTender;

    return-object v0
.end method

.method public restartAutoCaptureTimer()V
    .locals 1

    .line 1098
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    invoke-virtual {v0, p0}, Lcom/squareup/payment/BackgroundCaptor;->restartAutoCaptureOnActivity(Lcom/squareup/payment/RequiresAuthorization;)V

    return-void
.end method

.method public setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;)V"
        }
    .end annotation

    .line 1433
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->lastAddedTender:Lcom/squareup/payment/BillPayment$LastAddedTender;

    iget-object v0, v0, Lcom/squareup/payment/BillPayment$LastAddedTender;->tender:Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/payment/tender/BaseTender;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    return-void
.end method

.method public setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V
    .locals 1

    .line 947
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->asAcceptsTips()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 948
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->asAcceptsTips()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/payment/AcceptsTips;->setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V

    goto :goto_0

    .line 950
    :cond_0
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->requireTippingTender()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/squareup/payment/AcceptsTips;->setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V

    .line 952
    :goto_0
    iget-object p1, p0, Lcom/squareup/payment/BillPayment;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    invoke-virtual {p1, p0}, Lcom/squareup/payment/BackgroundCaptor;->restartAutoCaptureOnActivity(Lcom/squareup/payment/RequiresAuthorization;)V

    return-void
.end method

.method public setVectorSignature(Lcom/squareup/signature/SignatureAsJson;)V
    .locals 1

    .line 924
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->requireSignedTender()Lcom/squareup/payment/AcceptsSignature;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/payment/AcceptsSignature;->setVectorSignature(Lcom/squareup/signature/SignatureAsJson;)V

    .line 925
    iget-object p1, p0, Lcom/squareup/payment/BillPayment;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    invoke-virtual {p1, p0}, Lcom/squareup/payment/BackgroundCaptor;->restartAutoCaptureOnActivity(Lcom/squareup/payment/RequiresAuthorization;)V

    return-void
.end method

.method public shouldAutoSendReceipt()Z
    .locals 1

    .line 352
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->shouldAutoSendReceipt()Z

    move-result v0

    return v0
.end method

.method public shouldCallAuthOnTender(Lcom/squareup/payment/tender/BaseTender;)Z
    .locals 4

    .line 422
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->canAutoFlush()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 427
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 428
    invoke-static {p1}, Lcom/squareup/payment/BillPayment;->mayRequireSwedishRounding(Lcom/squareup/payment/tender/BaseTender;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 429
    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 433
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 434
    invoke-static {v2, v0}, Lcom/squareup/money/MoneyMath;->greaterThanOrEqualTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v0

    .line 435
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/payment/OrderSnapshot;->requiresSynchronousAuthorization()Z

    move-result v2

    if-nez v2, :cond_3

    .line 436
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender;->isLocalTender()Z

    move-result p1

    if-eqz p1, :cond_3

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 p1, 0x1

    :goto_1
    if-nez p1, :cond_5

    .line 441
    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->stagedTenders:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/payment/tender/BaseTender;

    .line 442
    invoke-virtual {v2}, Lcom/squareup/payment/tender/BaseTender;->isLocalTender()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v2}, Lcom/squareup/payment/tender/BaseTender;->canAutoFlush()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 p1, 0x1

    :cond_5
    return p1
.end method

.method public shouldSkipReceipt()Z
    .locals 1

    .line 347
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->shouldAutoSendReceipt()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/payment/BillPayment;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    .line 348
    invoke-interface {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;->skipReceiptScreenForFastCheckout()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public signOnPrintedReceipt()Z
    .locals 1

    .line 920
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->requireSignedTender()Lcom/squareup/payment/AcceptsSignature;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsSignature;->signOnPrintedReceipt()Z

    move-result v0

    return v0
.end method

.method public tipOnPrintedReceipt()Z
    .locals 1

    .line 960
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->requireTippingTender()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsTips;->tipOnPrintedReceipt()Z

    move-result v0

    return v0
.end method

.method public useSeparateTippingScreen()Z
    .locals 1

    .line 956
    invoke-direct {p0}, Lcom/squareup/payment/BillPayment;->requireTippingTender()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsTips;->useSeparateTippingScreen()Z

    move-result v0

    return v0
.end method
