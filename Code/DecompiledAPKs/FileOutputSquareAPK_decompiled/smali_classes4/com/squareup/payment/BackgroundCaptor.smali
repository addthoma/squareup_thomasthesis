.class Lcom/squareup/payment/BackgroundCaptor;
.super Ljava/lang/Object;
.source "BackgroundCaptor.java"


# instance fields
.field private final autoCaptureControl:Lcom/squareup/autocapture/AutoCaptureControl;

.field private final danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

.field private final pendingCaptures:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private final taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private final transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

.field private final voidMonitor:Lcom/squareup/payment/VoidMonitor;


# direct methods
.method constructor <init>(Lcom/squareup/autocapture/AutoCaptureControl;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/payment/VoidMonitor;)V
    .locals 0
    .param p3    # Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;
        .annotation runtime Lcom/squareup/payment/DanglingPayment;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/payment/BackgroundCaptor;->autoCaptureControl:Lcom/squareup/autocapture/AutoCaptureControl;

    .line 32
    iput-object p3, p0, Lcom/squareup/payment/BackgroundCaptor;->danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    .line 33
    iput-object p4, p0, Lcom/squareup/payment/BackgroundCaptor;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 34
    iput-object p2, p0, Lcom/squareup/payment/BackgroundCaptor;->pendingCaptures:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 35
    iput-object p5, p0, Lcom/squareup/payment/BackgroundCaptor;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    .line 36
    iput-object p6, p0, Lcom/squareup/payment/BackgroundCaptor;->voidMonitor:Lcom/squareup/payment/VoidMonitor;

    return-void
.end method


# virtual methods
.method addCapture(Lcom/squareup/payment/RequiresAuthorization;Z)V
    .locals 0

    .line 43
    invoke-interface {p1, p2}, Lcom/squareup/payment/RequiresAuthorization;->createCaptureTask(Z)Lcom/squareup/queue/CaptureTask;

    move-result-object p1

    .line 44
    iget-object p2, p0, Lcom/squareup/payment/BackgroundCaptor;->pendingCaptures:Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-interface {p2, p1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    .line 45
    iget-object p2, p0, Lcom/squareup/payment/BackgroundCaptor;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-interface {p1, p2}, Lcom/squareup/queue/CaptureTask;->logEnqueued(Lcom/squareup/payment/ledger/TransactionLedgerManager;)V

    .line 46
    invoke-virtual {p0}, Lcom/squareup/payment/BackgroundCaptor;->stopAutoCapture()V

    return-void
.end method

.method cancelAuthorization(Lcom/squareup/payment/RequiresAuthorization;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/payment/BackgroundCaptor;->voidMonitor:Lcom/squareup/payment/VoidMonitor;

    invoke-interface {p1}, Lcom/squareup/payment/RequiresAuthorization;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/squareup/payment/VoidMonitor;->onDanglingAuthVoided(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 51
    invoke-interface {p1, p2}, Lcom/squareup/payment/RequiresAuthorization;->createCancelTask(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/queue/CancelTask;

    move-result-object p1

    .line 52
    iget-object p2, p0, Lcom/squareup/payment/BackgroundCaptor;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-interface {p2, p1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    .line 53
    iget-object p2, p0, Lcom/squareup/payment/BackgroundCaptor;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-interface {p1, p2}, Lcom/squareup/queue/CancelTask;->logEnqueued(Lcom/squareup/payment/ledger/TransactionLedgerManager;)V

    .line 54
    invoke-virtual {p0}, Lcom/squareup/payment/BackgroundCaptor;->stopAutoCapture()V

    return-void
.end method

.method public hasRequestedAuthorization()Z
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/payment/BackgroundCaptor;->danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;->hasDanglingAuth()Z

    move-result v0

    return v0
.end method

.method onFailedAuth(Lcom/squareup/payment/RequiresAuthorization;)V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/payment/BackgroundCaptor;->danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    invoke-interface {p1}, Lcom/squareup/payment/RequiresAuthorization;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;->onFailedAuth(Lcom/squareup/protos/client/IdPair;)V

    return-void
.end method

.method restartAutoCaptureOnActivity(Lcom/squareup/payment/RequiresAuthorization;)V
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/payment/BackgroundCaptor;->autoCaptureControl:Lcom/squareup/autocapture/AutoCaptureControl;

    invoke-virtual {v0, p1}, Lcom/squareup/autocapture/AutoCaptureControl;->restartQuickTimer(Lcom/squareup/payment/RequiresAuthorization;)V

    return-void
.end method

.method startAutoCaptureOnAuth(Lcom/squareup/payment/RequiresAuthorization;)V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/payment/BackgroundCaptor;->autoCaptureControl:Lcom/squareup/autocapture/AutoCaptureControl;

    invoke-virtual {v0, p1}, Lcom/squareup/autocapture/AutoCaptureControl;->startQuickAndSlowTimers(Lcom/squareup/payment/RequiresAuthorization;)V

    return-void
.end method

.method stopAutoCapture()V
    .locals 2

    .line 90
    :try_start_0
    iget-object v0, p0, Lcom/squareup/payment/BackgroundCaptor;->danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;->clearLastAuth()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    :goto_0
    iget-object v0, p0, Lcom/squareup/payment/BackgroundCaptor;->autoCaptureControl:Lcom/squareup/autocapture/AutoCaptureControl;

    invoke-virtual {v0}, Lcom/squareup/autocapture/AutoCaptureControl;->stop()V

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "Error clearing last authorization key."

    .line 98
    invoke-static {v0, v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :goto_1
    return-void

    .line 100
    :goto_2
    iget-object v1, p0, Lcom/squareup/payment/BackgroundCaptor;->autoCaptureControl:Lcom/squareup/autocapture/AutoCaptureControl;

    invoke-virtual {v1}, Lcom/squareup/autocapture/AutoCaptureControl;->stop()V

    .line 101
    throw v0
.end method

.method writeLastAuth(Lcom/squareup/payment/RequiresAuthorization;Ljava/util/List;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/RequiresAuthorization;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;Z)V"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/squareup/payment/BackgroundCaptor;->danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    invoke-interface {p1}, Lcom/squareup/payment/RequiresAuthorization;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {p1}, Lcom/squareup/payment/RequiresAuthorization;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    sget-object v4, Lcom/squareup/PaymentType;->BILL2:Lcom/squareup/PaymentType;

    move-object v5, p2

    move v6, p3

    invoke-interface/range {v0 .. v6}, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;->writeLastAuth(JLcom/squareup/protos/client/IdPair;Lcom/squareup/PaymentType;Ljava/util/List;Z)V

    return-void
.end method
