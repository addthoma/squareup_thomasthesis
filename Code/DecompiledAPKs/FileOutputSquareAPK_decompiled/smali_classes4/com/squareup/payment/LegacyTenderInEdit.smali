.class public Lcom/squareup/payment/LegacyTenderInEdit;
.super Ljava/lang/Object;
.source "LegacyTenderInEdit.java"

# interfaces
.implements Lcom/squareup/payment/TenderInEdit;


# instance fields
.field private contact:Lcom/squareup/protos/client/rolodex/Contact;

.field private final mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private tender:Lcom/squareup/payment/tender/BaseTender$Builder;


# direct methods
.method public constructor <init>(Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 0
    .param p1    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/payment/LegacyTenderInEdit;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-void
.end method

.method private clearTender()Lcom/squareup/payment/tender/BaseTender$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/payment/tender/BaseTender$Builder;",
            ">()TT;"
        }
    .end annotation

    .line 149
    invoke-direct {p0}, Lcom/squareup/payment/LegacyTenderInEdit;->requireTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lcom/squareup/payment/LegacyTenderInEdit;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-static {v1}, Lcom/squareup/util/ThreadEnforcerExtensionsKt;->logIfNotThread(Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    const/4 v1, 0x0

    .line 151
    iput-object v1, p0, Lcom/squareup/payment/LegacyTenderInEdit;->tender:Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 152
    iput-object v1, p0, Lcom/squareup/payment/LegacyTenderInEdit;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method private requireTender()Lcom/squareup/payment/tender/BaseTender$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/payment/tender/BaseTender$Builder;",
            ">()TT;"
        }
    .end annotation

    .line 145
    iget-object v0, p0, Lcom/squareup/payment/LegacyTenderInEdit;->tender:Lcom/squareup/payment/tender/BaseTender$Builder;

    const-string v1, "tender"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/BaseTender$Builder;

    return-object v0
.end method


# virtual methods
.method public asAcceptsTips()Lcom/squareup/payment/AcceptsTips;
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/payment/LegacyTenderInEdit;->tender:Lcom/squareup/payment/tender/BaseTender$Builder;

    instance-of v1, v0, Lcom/squareup/payment/AcceptsTips;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/squareup/payment/AcceptsTips;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public assertNoTenderInEdit()V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/payment/LegacyTenderInEdit;->tender:Lcom/squareup/payment/tender/BaseTender$Builder;

    if-nez v0, :cond_0

    return-void

    .line 46
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "tenderInEdit when not expected!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public clearBaseTender()Lcom/squareup/payment/tender/BaseTender$Builder;
    .locals 1

    .line 89
    invoke-direct {p0}, Lcom/squareup/payment/LegacyTenderInEdit;->clearTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clearCashTender()Lcom/squareup/payment/tender/CashTender$Builder;
    .locals 1

    .line 93
    invoke-direct {p0}, Lcom/squareup/payment/LegacyTenderInEdit;->clearTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/CashTender$Builder;

    return-object v0
.end method

.method public clearEmoney()Lcom/squareup/payment/tender/EmoneyTender$Builder;
    .locals 1

    .line 137
    invoke-direct {p0}, Lcom/squareup/payment/LegacyTenderInEdit;->clearTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/EmoneyTender$Builder;

    return-object v0
.end method

.method public clearInstrumentTender()Lcom/squareup/payment/tender/InstrumentTender$Builder;
    .locals 1

    .line 117
    invoke-direct {p0}, Lcom/squareup/payment/LegacyTenderInEdit;->clearTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/InstrumentTender$Builder;

    return-object v0
.end method

.method public clearMagStripeTender()Lcom/squareup/payment/tender/MagStripeTenderBuilder;
    .locals 1

    .line 109
    invoke-direct {p0}, Lcom/squareup/payment/LegacyTenderInEdit;->clearTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/MagStripeTenderBuilder;

    return-object v0
.end method

.method public clearOtherTender()Lcom/squareup/payment/tender/OtherTender$Builder;
    .locals 1

    .line 101
    invoke-direct {p0}, Lcom/squareup/payment/LegacyTenderInEdit;->clearTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/OtherTender$Builder;

    return-object v0
.end method

.method public clearSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;
    .locals 1

    .line 125
    invoke-direct {p0}, Lcom/squareup/payment/LegacyTenderInEdit;->clearTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    return-object v0
.end method

.method public editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V
    .locals 1

    const-string v0, "tender"

    .line 51
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 52
    invoke-virtual {p0}, Lcom/squareup/payment/LegacyTenderInEdit;->assertNoTenderInEdit()V

    .line 53
    iput-object p1, p0, Lcom/squareup/payment/LegacyTenderInEdit;->tender:Lcom/squareup/payment/tender/BaseTender$Builder;

    return-void
.end method

.method public getAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/payment/LegacyTenderInEdit;->tender:Lcom/squareup/payment/tender/BaseTender$Builder;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender$Builder;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getContact()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/payment/LegacyTenderInEdit;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public isEditingTender()Z
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/payment/LegacyTenderInEdit;->tender:Lcom/squareup/payment/tender/BaseTender$Builder;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isInstrumentTender()Z
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/payment/LegacyTenderInEdit;->tender:Lcom/squareup/payment/tender/BaseTender$Builder;

    instance-of v0, v0, Lcom/squareup/payment/tender/InstrumentTender$Builder;

    return v0
.end method

.method public isMagStripeTender()Z
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/payment/LegacyTenderInEdit;->tender:Lcom/squareup/payment/tender/BaseTender$Builder;

    instance-of v0, v0, Lcom/squareup/payment/tender/MagStripeTenderBuilder;

    return v0
.end method

.method public isSmartCardTender()Z
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/payment/LegacyTenderInEdit;->tender:Lcom/squareup/payment/tender/BaseTender$Builder;

    instance-of v0, v0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    return v0
.end method

.method public requireBaseTender()Lcom/squareup/payment/tender/BaseTender$Builder;
    .locals 1

    .line 133
    invoke-direct {p0}, Lcom/squareup/payment/LegacyTenderInEdit;->requireTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    return-object v0
.end method

.method public requireCashTender()Lcom/squareup/payment/tender/CashTender$Builder;
    .locals 1

    .line 97
    invoke-direct {p0}, Lcom/squareup/payment/LegacyTenderInEdit;->requireTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/CashTender$Builder;

    return-object v0
.end method

.method public requireEmoney()Lcom/squareup/payment/tender/EmoneyTender$Builder;
    .locals 1

    .line 141
    invoke-direct {p0}, Lcom/squareup/payment/LegacyTenderInEdit;->requireTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/EmoneyTender$Builder;

    return-object v0
.end method

.method public requireInstrumentTender()Lcom/squareup/payment/tender/InstrumentTender$Builder;
    .locals 1

    .line 121
    invoke-direct {p0}, Lcom/squareup/payment/LegacyTenderInEdit;->requireTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/InstrumentTender$Builder;

    return-object v0
.end method

.method public requireMagStripeTender()Lcom/squareup/payment/tender/MagStripeTenderBuilder;
    .locals 1

    .line 113
    invoke-direct {p0}, Lcom/squareup/payment/LegacyTenderInEdit;->requireTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/MagStripeTenderBuilder;

    return-object v0
.end method

.method public requireOtherTender()Lcom/squareup/payment/tender/OtherTender$Builder;
    .locals 1

    .line 105
    invoke-direct {p0}, Lcom/squareup/payment/LegacyTenderInEdit;->requireTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/OtherTender$Builder;

    return-object v0
.end method

.method public requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;
    .locals 1

    .line 129
    invoke-direct {p0}, Lcom/squareup/payment/LegacyTenderInEdit;->requireTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    return-object v0
.end method

.method public reset()V
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/payment/LegacyTenderInEdit;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-static {v0}, Lcom/squareup/util/ThreadEnforcerExtensionsKt;->logIfNotThread(Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    const/4 v0, 0x0

    .line 40
    iput-object v0, p0, Lcom/squareup/payment/LegacyTenderInEdit;->tender:Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 41
    iput-object v0, p0, Lcom/squareup/payment/LegacyTenderInEdit;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-void
.end method

.method public setContact(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/squareup/payment/LegacyTenderInEdit;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-void
.end method
