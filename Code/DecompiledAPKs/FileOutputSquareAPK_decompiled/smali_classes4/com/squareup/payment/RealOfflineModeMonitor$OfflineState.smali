.class Lcom/squareup/payment/RealOfflineModeMonitor$OfflineState;
.super Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;
.source "RealOfflineModeMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/RealOfflineModeMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OfflineState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/RealOfflineModeMonitor;


# direct methods
.method private constructor <init>(Lcom/squareup/payment/RealOfflineModeMonitor;)V
    .locals 0

    .line 326
    iput-object p1, p0, Lcom/squareup/payment/RealOfflineModeMonitor$OfflineState;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/RealOfflineModeMonitor;Lcom/squareup/payment/RealOfflineModeMonitor$1;)V
    .locals 0

    .line 326
    invoke-direct {p0, p1}, Lcom/squareup/payment/RealOfflineModeMonitor$OfflineState;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;)V

    return-void
.end method


# virtual methods
.method public init()V
    .locals 1

    .line 336
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor$OfflineState;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    invoke-static {v0}, Lcom/squareup/payment/RealOfflineModeMonitor;->access$500(Lcom/squareup/payment/RealOfflineModeMonitor;)V

    return-void
.end method

.method public isUnavailable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public recordServiceAvailable(Z)Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;
    .locals 2

    .line 328
    new-instance p1, Lcom/squareup/payment/RealOfflineModeMonitor$GoingOnlineState;

    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor$OfflineState;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, Lcom/squareup/payment/RealOfflineModeMonitor$GoingOnlineState;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;Lcom/squareup/payment/RealOfflineModeMonitor$1;)V

    return-object p1
.end method
