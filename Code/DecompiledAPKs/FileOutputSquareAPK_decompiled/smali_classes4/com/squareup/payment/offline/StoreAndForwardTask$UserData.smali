.class interface abstract Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;
.super Ljava/lang/Object;
.source "StoreAndForwardTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/offline/StoreAndForwardTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "UserData"
.end annotation


# virtual methods
.method public abstract getStoredPayments()Lcom/squareup/queue/StoredPaymentsQueue;
.end method

.method public abstract getTransactionLedgerManager()Lcom/squareup/payment/ledger/TransactionLedgerManager;
.end method
