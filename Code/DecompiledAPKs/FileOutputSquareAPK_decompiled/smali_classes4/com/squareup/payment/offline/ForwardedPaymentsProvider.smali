.class public Lcom/squareup/payment/offline/ForwardedPaymentsProvider;
.super Ljava/lang/Object;
.source "ForwardedPaymentsProvider.java"


# instance fields
.field private final application:Landroid/app/Application;

.field private final forwardedPaymentsByUserId:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final gson:Lcom/google/gson/Gson;


# direct methods
.method constructor <init>(Landroid/app/Application;Lcom/google/gson/Gson;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentsProvider;->application:Landroid/app/Application;

    .line 28
    iput-object p2, p0, Lcom/squareup/payment/offline/ForwardedPaymentsProvider;->gson:Lcom/google/gson/Gson;

    .line 29
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentsProvider;->forwardedPaymentsByUserId:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method getForwardedPayments(Ljava/lang/String;)Lcom/squareup/settings/LocalSetting;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;>;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentsProvider;->forwardedPaymentsByUserId:Ljava/util/Map;

    .line 39
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    if-nez v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentsProvider;->application:Landroid/app/Application;

    invoke-static {v0, p1}, Lcom/squareup/user/Users;->getUserPreferences(Landroid/app/Application;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 42
    new-instance v1, Lcom/squareup/payment/offline/ForwardedPaymentsProvider$1;

    invoke-direct {v1, p0}, Lcom/squareup/payment/offline/ForwardedPaymentsProvider$1;-><init>(Lcom/squareup/payment/offline/ForwardedPaymentsProvider;)V

    invoke-virtual {v1}, Lcom/squareup/payment/offline/ForwardedPaymentsProvider$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 43
    iget-object v2, p0, Lcom/squareup/payment/offline/ForwardedPaymentsProvider;->gson:Lcom/google/gson/Gson;

    const-string v3, "store-and-forward-forwarded-payments.json"

    .line 44
    invoke-static {v0, v3, v2, v1}, Lcom/squareup/settings/GsonLocalSetting;->forType(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/reflect/Type;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lcom/squareup/payment/offline/ForwardedPaymentsProvider;->forwardedPaymentsByUserId:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method
