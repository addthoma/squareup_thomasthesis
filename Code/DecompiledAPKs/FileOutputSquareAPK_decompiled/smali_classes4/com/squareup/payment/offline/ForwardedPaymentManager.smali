.class public Lcom/squareup/payment/offline/ForwardedPaymentManager;
.super Ljava/lang/Object;
.source "ForwardedPaymentManager.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;,
        Lcom/squareup/payment/offline/ForwardedPaymentManager$OnPaymentsForwarded;
    }
.end annotation


# static fields
.field static final EMPTY_FORWARDED_PAYMENTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;"
        }
    .end annotation
.end field

.field static final FIRST_UPDATE_TIMEOUT_MILLIS:J = 0x1388L

.field static final UPDATE_INTERVAL_MILLIS:J = 0x2710L


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private destroyed:Z

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final forwardedPayments:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;>;"
        }
    .end annotation
.end field

.field private final forwardedPaymentsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/util/Collection<",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final onProcessedForwardedPaymentsRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/util/Collection<",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;>;"
        }
    .end annotation
.end field

.field private requestingStatus:Z

.field private final storeAndForwardBillService:Lcom/squareup/server/bills/StoreAndForwardBillService;

.field private final updateStatusTask:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 63
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->EMPTY_FORWARDED_PAYMENTS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/badbus/BadBus;Lcom/squareup/server/bills/StoreAndForwardBillService;Lcom/squareup/payment/offline/ForwardedPaymentsProvider;Ljava/lang/String;)V
    .locals 6
    .param p1    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 92
    invoke-virtual {p5, p6}, Lcom/squareup/payment/offline/ForwardedPaymentsProvider;->getForwardedPayments(Ljava/lang/String;)Lcom/squareup/settings/LocalSetting;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 91
    invoke-direct/range {v0 .. v5}, Lcom/squareup/payment/offline/ForwardedPaymentManager;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/badbus/BadBus;Lcom/squareup/server/bills/StoreAndForwardBillService;Lcom/squareup/settings/LocalSetting;)V

    return-void
.end method

.method public constructor <init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/badbus/BadBus;Lcom/squareup/server/bills/StoreAndForwardBillService;Lcom/squareup/settings/LocalSetting;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/server/bills/StoreAndForwardBillService;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;>;)V"
        }
    .end annotation

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 99
    iput-object p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->mainScheduler:Lio/reactivex/Scheduler;

    .line 100
    iput-object p2, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 101
    iput-object p3, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->badBus:Lcom/squareup/badbus/BadBus;

    .line 102
    iput-object p4, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->storeAndForwardBillService:Lcom/squareup/server/bills/StoreAndForwardBillService;

    .line 103
    iput-object p5, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->forwardedPayments:Lcom/squareup/settings/LocalSetting;

    .line 106
    new-instance p1, Lcom/squareup/payment/offline/ForwardedPaymentManager$1;

    invoke-direct {p1, p0, p2}, Lcom/squareup/payment/offline/ForwardedPaymentManager$1;-><init>(Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/thread/executor/MainThread;)V

    iput-object p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->updateStatusTask:Ljava/lang/Runnable;

    .line 112
    sget-object p1, Lcom/squareup/payment/offline/ForwardedPaymentManager;->EMPTY_FORWARDED_PAYMENTS:Ljava/util/List;

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->forwardedPaymentsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 113
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->onProcessedForwardedPaymentsRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Ljava/lang/Runnable;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->updateStatusTask:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Lio/reactivex/disposables/CompositeDisposable;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Z
    .locals 0

    .line 53
    iget-boolean p0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->destroyed:Z

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Ljava/util/Map;
    .locals 0

    .line 53
    invoke-direct {p0}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->getMutableMap()Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse;Ljava/util/List;)V
    .locals 0

    .line 53
    invoke-direct {p0, p1, p2}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->getResults(Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse;Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/payment/offline/ForwardedPaymentManager;Ljava/util/Map;Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;Ljava/util/List;)V
    .locals 0

    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->update(Ljava/util/Map;Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$600(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->onProcessedForwardedPaymentsRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Lcom/squareup/settings/LocalSetting;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->forwardedPayments:Lcom/squareup/settings/LocalSetting;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->forwardedPaymentsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method static synthetic access$902(Lcom/squareup/payment/offline/ForwardedPaymentManager;Z)Z
    .locals 0

    .line 53
    iput-boolean p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->requestingStatus:Z

    return p1
.end method

.method private getMutableMap()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;"
        }
    .end annotation

    .line 327
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->forwardedPayments:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-nez v0, :cond_0

    .line 329
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    return-object v0

    .line 332
    :cond_0
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1, v0}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    return-object v1
.end method

.method private getResults(Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;",
            ">;)V"
        }
    .end annotation

    .line 256
    iget-object p1, p1, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse;->bill_processing_result:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    .line 257
    new-instance v1, Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;

    invoke-direct {v1, v0}, Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;-><init>(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;)V

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method private keysToQuery()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 298
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->forwardedPayments:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_5

    .line 299
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 303
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 304
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/payment/offline/ForwardedPayment;

    .line 305
    invoke-virtual {v3}, Lcom/squareup/payment/offline/ForwardedPayment;->getUniqueKey()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    goto :goto_0

    .line 309
    :cond_1
    invoke-virtual {v3}, Lcom/squareup/payment/offline/ForwardedPayment;->isPending()Z

    move-result v4

    if-nez v4, :cond_3

    .line 310
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Non-pending payment found in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 310
    invoke-static {v4}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 312
    invoke-virtual {v3}, Lcom/squareup/payment/offline/ForwardedPayment;->getUniqueKey()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/payment/offline/ForwardedPayment;

    if-eqz v4, :cond_2

    .line 316
    iget-object v5, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->onProcessedForwardedPaymentsRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 318
    :cond_2
    iget-object v4, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->forwardedPayments:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v4, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 319
    iget-object v4, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->forwardedPaymentsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 321
    :cond_3
    invoke-virtual {v3}, Lcom/squareup/payment/offline/ForwardedPayment;->getUniqueKey()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    return-object v1

    :cond_5
    :goto_1
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic lambda$allForwardedPaymentIds$0(Ljava/util/Collection;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 137
    invoke-static {p0}, Lio/reactivex/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/Observable;

    move-result-object p0

    sget-object v0, Lcom/squareup/payment/offline/-$$Lambda$pOiNUAtP4IubfCf_cdupZ4FPoXo;->INSTANCE:Lcom/squareup/payment/offline/-$$Lambda$pOiNUAtP4IubfCf_cdupZ4FPoXo;

    .line 138
    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    .line 139
    invoke-virtual {p0}, Lio/reactivex/Observable;->toList()Lio/reactivex/Single;

    move-result-object p0

    .line 140
    invoke-virtual {p0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onProcessedForwardedPaymentIds$2(Ljava/util/Collection;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 163
    invoke-static {p0}, Lio/reactivex/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/Observable;

    move-result-object p0

    sget-object v0, Lcom/squareup/payment/offline/-$$Lambda$pOiNUAtP4IubfCf_cdupZ4FPoXo;->INSTANCE:Lcom/squareup/payment/offline/-$$Lambda$pOiNUAtP4IubfCf_cdupZ4FPoXo;

    .line 164
    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    .line 165
    invoke-virtual {p0}, Lio/reactivex/Observable;->toList()Lio/reactivex/Single;

    move-result-object p0

    .line 166
    invoke-virtual {p0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onProcessedForwardedPayments$1(Ljava/util/Collection;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 151
    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method private replaceBillIdIfChanged(Lcom/squareup/payment/offline/ForwardedPayment;Lcom/squareup/payment/offline/ForwardedPayment;)V
    .locals 2

    .line 289
    invoke-virtual {p1}, Lcom/squareup/payment/offline/ForwardedPayment;->getBillId()Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object p1

    .line 290
    invoke-virtual {p2}, Lcom/squareup/payment/offline/ForwardedPayment;->getBillId()Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object p2

    .line 291
    invoke-virtual {p2, p1}, Lcom/squareup/billhistory/model/BillHistoryId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->badBus:Lcom/squareup/badbus/BadBus;

    new-instance v1, Lcom/squareup/billhistory/Bills$BillIdChanged;

    invoke-direct {v1, p1, p2}, Lcom/squareup/billhistory/Bills$BillIdChanged;-><init>(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/billhistory/model/BillHistoryId;)V

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->post(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private update(Ljava/util/Map;Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;",
            "Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;)V"
        }
    .end annotation

    .line 263
    invoke-virtual {p2}, Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;->getUniqueKey()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const-string v2, "Payment/Bill status update for %s: %s"

    .line 264
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 266
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/offline/ForwardedPayment;

    if-eqz v1, :cond_1

    .line 268
    invoke-virtual {p2, v1}, Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;->addTo(Lcom/squareup/payment/offline/ForwardedPayment;)Lcom/squareup/payment/offline/ForwardedPayment;

    move-result-object p2

    .line 269
    invoke-direct {p0, v1, p2}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->replaceBillIdIfChanged(Lcom/squareup/payment/offline/ForwardedPayment;Lcom/squareup/payment/offline/ForwardedPayment;)V

    .line 271
    invoke-virtual {p2}, Lcom/squareup/payment/offline/ForwardedPayment;->isPending()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 272
    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 274
    :cond_0
    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/payment/offline/ForwardedPayment;

    if-eqz p1, :cond_1

    .line 276
    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public allForwardedPaymentIds()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 134
    invoke-virtual {p0}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->allForwardedPayments()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/payment/offline/-$$Lambda$ForwardedPaymentManager$gL_kQ_51UsxBW92tEEpMEF9uyog;->INSTANCE:Lcom/squareup/payment/offline/-$$Lambda$ForwardedPaymentManager$gL_kQ_51UsxBW92tEEpMEF9uyog;

    .line 135
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public allForwardedPayments()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/Collection<",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;>;"
        }
    .end annotation

    .line 126
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->forwardedPaymentsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->mainScheduler:Lio/reactivex/Scheduler;

    .line 127
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public forwardedPaymentWithId(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;>;"
        }
    .end annotation

    .line 174
    invoke-virtual {p0}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->allForwardedPayments()Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 175
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 176
    invoke-virtual {v0}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 178
    invoke-static {v0, p1}, Lcom/squareup/payment/offline/ForwardedPaymentsKt;->maybeForwardedPaymentWithId(Lio/reactivex/Single;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public forwardedPaymentsCount()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 118
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->forwardedPaymentsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->mainScheduler:Lio/reactivex/Scheduler;

    .line 119
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/payment/offline/-$$Lambda$OPVJx0T8Y_f7YZX1BazDEOyQjC0;->INSTANCE:Lcom/squareup/payment/offline/-$$Lambda$OPVJx0T8Y_f7YZX1BazDEOyQjC0;

    .line 120
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onEnterScope$3$ForwardedPaymentManager(Lcom/squareup/payment/offline/ForwardedPaymentManager$OnPaymentsForwarded;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 183
    invoke-virtual {p0}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->onPaymentsForwarded()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 182
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/offline/ForwardedPaymentManager$OnPaymentsForwarded;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/payment/offline/-$$Lambda$ForwardedPaymentManager$J3yHSLR-SbSD3u25p61hD0thvEQ;

    invoke-direct {v1, p0}, Lcom/squareup/payment/offline/-$$Lambda$ForwardedPaymentManager$J3yHSLR-SbSD3u25p61hD0thvEQ;-><init>(Lcom/squareup/payment/offline/ForwardedPaymentManager;)V

    .line 183
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 182
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 184
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 185
    iget-object p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->updateStatusTask:Ljava/lang/Runnable;

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    const/4 v0, 0x1

    .line 189
    iput-boolean v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->destroyed:Z

    .line 190
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->updateStatusTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method

.method onPaymentsForwarded()V
    .locals 4

    .line 339
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/payment/offline/-$$Lambda$jTvwJbX4J9bU9Cwe0gqPeZCyndQ;

    invoke-direct {v1, p0}, Lcom/squareup/payment/offline/-$$Lambda$jTvwJbX4J9bU9Cwe0gqPeZCyndQ;-><init>(Lcom/squareup/payment/offline/ForwardedPaymentManager;)V

    const-wide/16 v2, 0x1388

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public onProcessedForwardedPaymentIds()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 160
    invoke-virtual {p0}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->onProcessedForwardedPayments()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/payment/offline/-$$Lambda$ForwardedPaymentManager$Qt-pGuFxgTexJw8nUWplU741mIU;->INSTANCE:Lcom/squareup/payment/offline/-$$Lambda$ForwardedPaymentManager$Qt-pGuFxgTexJw8nUWplU741mIU;

    .line 161
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onProcessedForwardedPayments()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/Collection<",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;>;"
        }
    .end annotation

    .line 149
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->onProcessedForwardedPaymentsRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->mainScheduler:Lio/reactivex/Scheduler;

    .line 150
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/payment/offline/-$$Lambda$ForwardedPaymentManager$Db3_YgJO50KDR2k68xTpSW4dTMc;->INSTANCE:Lcom/squareup/payment/offline/-$$Lambda$ForwardedPaymentManager$Db3_YgJO50KDR2k68xTpSW4dTMc;

    .line 151
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public updateStatus()V
    .locals 2

    .line 194
    iget-boolean v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->requestingStatus:Z

    if-eqz v0, :cond_0

    return-void

    .line 198
    :cond_0
    invoke-direct {p0}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->keysToQuery()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 203
    :cond_1
    new-instance v1, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest$Builder;-><init>()V

    .line 204
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest$Builder;->client_id(Ljava/util/List;)Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest$Builder;

    move-result-object v0

    .line 205
    invoke-virtual {v0}, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;

    move-result-object v0

    .line 206
    iget-object v1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->storeAndForwardBillService:Lcom/squareup/server/bills/StoreAndForwardBillService;

    .line 207
    invoke-interface {v1, v0}, Lcom/squareup/server/bills/StoreAndForwardBillService;->getBillsStatus(Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;)Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    const/4 v1, 0x1

    .line 209
    iput-boolean v1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->requestingStatus:Z

    .line 211
    iget-object v1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;

    invoke-direct {v1, p0}, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;-><init>(Lcom/squareup/payment/offline/ForwardedPaymentManager;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/Observer;)V

    return-void
.end method
