.class public final Lcom/squareup/payment/offline/QueueBertPublicKeyManager_Factory;
.super Ljava/lang/Object;
.source "QueueBertPublicKeyManager_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/offline/QueueBertPublicKeyManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorLazyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager_Factory;->offlineModeMonitorLazyProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p5, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager_Factory;->clockProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/offline/QueueBertPublicKeyManager_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager_Factory;"
        }
    .end annotation

    .line 48
    new-instance v6, Lcom/squareup/payment/offline/QueueBertPublicKeyManager_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/payment/offline/QueueBertPublicKeyManager_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/thread/executor/MainThread;Ldagger/Lazy;Lcom/squareup/util/Clock;)Lcom/squareup/payment/offline/QueueBertPublicKeyManager;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Ldagger/Lazy<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Lcom/squareup/util/Clock;",
            ")",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager;"
        }
    .end annotation

    .line 54
    new-instance v6, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/thread/executor/MainThread;Ldagger/Lazy;Lcom/squareup/util/Clock;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/payment/offline/QueueBertPublicKeyManager;
    .locals 5

    .line 41
    iget-object v0, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/analytics/StoreAndForwardAnalytics;

    iget-object v2, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/thread/executor/MainThread;

    iget-object v3, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager_Factory;->offlineModeMonitorLazyProvider:Ljavax/inject/Provider;

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/payment/offline/QueueBertPublicKeyManager_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/util/Clock;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/payment/offline/QueueBertPublicKeyManager_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/thread/executor/MainThread;Ldagger/Lazy;Lcom/squareup/util/Clock;)Lcom/squareup/payment/offline/QueueBertPublicKeyManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/payment/offline/QueueBertPublicKeyManager_Factory;->get()Lcom/squareup/payment/offline/QueueBertPublicKeyManager;

    move-result-object v0

    return-object v0
.end method
