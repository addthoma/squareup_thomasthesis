.class public final Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;
.super Ljava/lang/Object;
.source "StoreAndForwardTask_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/payment/offline/StoreAndForwardTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final crossSessionQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final currentUserIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final dataDirectoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final fileExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final fileThreadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/FileThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final forwardedPaymentsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/ForwardedPaymentsProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final jobManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;"
        }
    .end annotation
.end field

.field private final queueCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardBillServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/StoreAndForwardBillService;",
            ">;"
        }
    .end annotation
.end field

.field private final storedPaymentNotifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notifications/StoredPaymentNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/FileThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/ForwardedPaymentsProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/StoreAndForwardBillService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notifications/StoredPaymentNotifier;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 93
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->applicationProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 94
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->busProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 95
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->clockProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 96
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->currentUserIdProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 97
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->dataDirectoryProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 98
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->fileExecutorProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 99
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->fileThreadEnforcerProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 100
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->forwardedPaymentsProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 101
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->connectivityMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 102
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 103
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->mainThreadProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 104
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->jobManagerProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 105
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->notificationManagerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 106
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->resProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 107
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->storeAndForwardBillServiceProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 108
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->crossSessionQueueProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 109
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->queueCacheProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 110
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->transactionLedgerManagerFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 111
    iput-object v1, v0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->storedPaymentNotifierProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/FileThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/ForwardedPaymentsProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/StoreAndForwardBillService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notifications/StoredPaymentNotifier;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/payment/offline/StoreAndForwardTask;",
            ">;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    .line 129
    new-instance v20, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;

    move-object/from16 v0, v20

    invoke-direct/range {v0 .. v19}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v20
.end method

.method public static injectApplication(Lcom/squareup/payment/offline/StoreAndForwardTask;Landroid/app/Application;)V
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->application:Landroid/app/Application;

    return-void
.end method

.method public static injectBus(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/badbus/BadEventSink;)V
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->bus:Lcom/squareup/badbus/BadEventSink;

    return-void
.end method

.method public static injectClock(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/util/Clock;)V
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method public static injectConnectivityMonitor(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/connectivity/ConnectivityMonitor;)V
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    return-void
.end method

.method public static injectCrossSessionQueue(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/queue/retrofit/RetrofitQueue;)V
    .locals 0

    .line 244
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->crossSessionQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-void
.end method

.method public static injectCurrentUserId(Lcom/squareup/payment/offline/StoreAndForwardTask;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/offline/StoreAndForwardTask;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 173
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->currentUserId:Ljavax/inject/Provider;

    return-void
.end method

.method public static injectDataDirectory(Lcom/squareup/payment/offline/StoreAndForwardTask;Ljava/io/File;)V
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->dataDirectory:Ljava/io/File;

    return-void
.end method

.method public static injectFileExecutor(Lcom/squareup/payment/offline/StoreAndForwardTask;Ljava/util/concurrent/Executor;)V
    .locals 0
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .line 185
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->fileExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public static injectFileThreadEnforcer(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/FileThreadEnforcer;)V
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->fileThreadEnforcer:Lcom/squareup/FileThreadEnforcer;

    return-void
.end method

.method public static injectForwardedPaymentsProvider(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/payment/offline/ForwardedPaymentsProvider;)V
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->forwardedPaymentsProvider:Lcom/squareup/payment/offline/ForwardedPaymentsProvider;

    return-void
.end method

.method public static injectJobManager(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/backgroundjob/BackgroundJobManager;)V
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    return-void
.end method

.method public static injectMainScheduler(Lcom/squareup/payment/offline/StoreAndForwardTask;Lrx/Scheduler;)V
    .locals 0
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation

    .line 209
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->mainScheduler:Lrx/Scheduler;

    return-void
.end method

.method public static injectMainThread(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/thread/executor/MainThread;)V
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-void
.end method

.method public static injectNotificationManager(Lcom/squareup/payment/offline/StoreAndForwardTask;Landroid/app/NotificationManager;)V
    .locals 0

    .line 226
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->notificationManager:Landroid/app/NotificationManager;

    return-void
.end method

.method public static injectQueueCache(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/queue/retrofit/QueueCache;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/offline/StoreAndForwardTask;",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;)V"
        }
    .end annotation

    .line 250
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->queueCache:Lcom/squareup/queue/retrofit/QueueCache;

    return-void
.end method

.method public static injectRes(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/util/Res;)V
    .locals 0

    .line 231
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static injectStoreAndForwardBillService(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/bills/StoreAndForwardBillService;)V
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->storeAndForwardBillService:Lcom/squareup/server/bills/StoreAndForwardBillService;

    return-void
.end method

.method public static injectStoredPaymentNotifier(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/notifications/StoredPaymentNotifier;)V
    .locals 0

    .line 262
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->storedPaymentNotifier:Lcom/squareup/notifications/StoredPaymentNotifier;

    return-void
.end method

.method public static injectTransactionLedgerManagerFactory(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;)V
    .locals 0

    .line 256
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->transactionLedgerManagerFactory:Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/payment/offline/StoreAndForwardTask;)V
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectApplication(Lcom/squareup/payment/offline/StoreAndForwardTask;Landroid/app/Application;)V

    .line 134
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/badbus/BadEventSink;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectBus(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/badbus/BadEventSink;)V

    .line 135
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Clock;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectClock(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/util/Clock;)V

    .line 136
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->currentUserIdProvider:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectCurrentUserId(Lcom/squareup/payment/offline/StoreAndForwardTask;Ljavax/inject/Provider;)V

    .line 137
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->dataDirectoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectDataDirectory(Lcom/squareup/payment/offline/StoreAndForwardTask;Ljava/io/File;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->fileExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectFileExecutor(Lcom/squareup/payment/offline/StoreAndForwardTask;Ljava/util/concurrent/Executor;)V

    .line 139
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->fileThreadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/FileThreadEnforcer;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectFileThreadEnforcer(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/FileThreadEnforcer;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->forwardedPaymentsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/offline/ForwardedPaymentsProvider;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectForwardedPaymentsProvider(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/payment/offline/ForwardedPaymentsProvider;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectConnectivityMonitor(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/connectivity/ConnectivityMonitor;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectMainScheduler(Lcom/squareup/payment/offline/StoreAndForwardTask;Lrx/Scheduler;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/executor/MainThread;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectMainThread(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/thread/executor/MainThread;)V

    .line 144
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->jobManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/backgroundjob/BackgroundJobManager;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectJobManager(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/backgroundjob/BackgroundJobManager;)V

    .line 145
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->notificationManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectNotificationManager(Lcom/squareup/payment/offline/StoreAndForwardTask;Landroid/app/NotificationManager;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectRes(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/util/Res;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->storeAndForwardBillServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/bills/StoreAndForwardBillService;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectStoreAndForwardBillService(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/bills/StoreAndForwardBillService;)V

    .line 148
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->crossSessionQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectCrossSessionQueue(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/queue/retrofit/RetrofitQueue;)V

    .line 149
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->queueCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/retrofit/QueueCache;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectQueueCache(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/queue/retrofit/QueueCache;)V

    .line 150
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->transactionLedgerManagerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectTransactionLedgerManagerFactory(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;)V

    .line 151
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->storedPaymentNotifierProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/notifications/StoredPaymentNotifier;

    invoke-static {p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectStoredPaymentNotifier(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/notifications/StoredPaymentNotifier;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/payment/offline/StoreAndForwardTask;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/offline/StoreAndForwardTask_MembersInjector;->injectMembers(Lcom/squareup/payment/offline/StoreAndForwardTask;)V

    return-void
.end method
