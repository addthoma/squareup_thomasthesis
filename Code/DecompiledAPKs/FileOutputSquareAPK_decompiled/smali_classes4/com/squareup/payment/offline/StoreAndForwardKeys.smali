.class public Lcom/squareup/payment/offline/StoreAndForwardKeys;
.super Ljava/lang/Object;
.source "StoreAndForwardKeys.java"


# instance fields
.field private final merchantKeyManager:Lcom/squareup/payment/offline/MerchantKeyManager;

.field private final queueBertPublicKeyManager:Lcom/squareup/payment/offline/QueueBertPublicKeyManager;


# direct methods
.method constructor <init>(Lcom/squareup/payment/offline/QueueBertPublicKeyManager;Lcom/squareup/payment/offline/MerchantKeyManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardKeys;->queueBertPublicKeyManager:Lcom/squareup/payment/offline/QueueBertPublicKeyManager;

    .line 15
    iput-object p2, p0, Lcom/squareup/payment/offline/StoreAndForwardKeys;->merchantKeyManager:Lcom/squareup/payment/offline/MerchantKeyManager;

    return-void
.end method


# virtual methods
.method public hasAllKeys()Z
    .locals 2

    const/4 v0, 0x0

    .line 20
    :try_start_0
    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardKeys;->queueBertPublicKeyManager:Lcom/squareup/payment/offline/QueueBertPublicKeyManager;

    invoke-virtual {v1}, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->getEncryptor()Lcom/squareup/encryption/JweEncryptor;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardKeys;->queueBertPublicKeyManager:Lcom/squareup/payment/offline/QueueBertPublicKeyManager;

    .line 21
    invoke-virtual {v1}, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->getBillEncryptor()Lcom/squareup/encryption/JweEncryptor;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardKeys;->merchantKeyManager:Lcom/squareup/payment/offline/MerchantKeyManager;

    .line 22
    invoke-virtual {v1}, Lcom/squareup/payment/offline/MerchantKeyManager;->getAuthCodeGenerator()Lcom/squareup/encryption/CryptoPrimitive;

    move-result-object v1
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :catch_0
    :cond_0
    return v0
.end method
