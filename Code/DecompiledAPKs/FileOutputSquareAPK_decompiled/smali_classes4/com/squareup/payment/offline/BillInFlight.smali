.class public Lcom/squareup/payment/offline/BillInFlight;
.super Ljava/lang/Object;
.source "BillInFlight.java"


# static fields
.field private static final unescapedGson:Lcom/google/gson/Gson;


# instance fields
.field public final addTendersRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest;",
            ">;"
        }
    .end annotation
.end field

.field public final clientId:Ljava/lang/String;

.field public final completeBill:Lcom/squareup/queue/bills/CompleteBill;

.field public final extraTenderDetails:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 55
    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    .line 56
    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->disableHtmlEscaping()Lcom/google/gson/GsonBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/offline/BillInFlight;->unescapedGson:Lcom/google/gson/Gson;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/payment/BillPayment;)V
    .locals 3

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->getAddTendersRequests()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 86
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->getCompleteBill()Lcom/squareup/queue/bills/CompleteBill;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->getFlushedTenders()Ljava/util/Set;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/payment/offline/BillInFlight;->getExtraTenderDetails(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v2

    .line 87
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->getCompleteBill()Lcom/squareup/queue/bills/CompleteBill;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/queue/bills/CompleteBill;->clientId:Ljava/lang/String;

    .line 85
    invoke-direct {p0, v0, v1, v2, p1}, Lcom/squareup/payment/offline/BillInFlight;-><init>(Ljava/util/List;Lcom/squareup/queue/bills/CompleteBill;Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/util/List;Lcom/squareup/queue/bills/CompleteBill;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest;",
            ">;",
            "Lcom/squareup/queue/bills/CompleteBill;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/squareup/payment/offline/BillInFlight;->addTendersRequests:Ljava/util/List;

    .line 61
    iput-object p2, p0, Lcom/squareup/payment/offline/BillInFlight;->completeBill:Lcom/squareup/queue/bills/CompleteBill;

    .line 62
    iput-object p3, p0, Lcom/squareup/payment/offline/BillInFlight;->extraTenderDetails:Ljava/util/List;

    .line 63
    iput-object p4, p0, Lcom/squareup/payment/offline/BillInFlight;->clientId:Ljava/lang/String;

    return-void
.end method

.method private encrypt(Lcom/squareup/encryption/CryptoPrimitive;Lcom/squareup/protos/client/bills/AddTendersRequest;)Lcom/squareup/protos/client/bills/AddTendersRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/encryption/CryptoPrimitive<",
            "Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;",
            ">;",
            "Lcom/squareup/protos/client/bills/AddTendersRequest;",
            ")",
            "Lcom/squareup/protos/client/bills/AddTendersRequest;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    const-string v0, "encryptor"

    .line 237
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "addTendersRequest"

    .line 238
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 239
    iget-object v0, p2, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 243
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 244
    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/AddTender;

    .line 245
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/AddTender;->newBuilder()Lcom/squareup/protos/client/bills/AddTender$Builder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/bills/AddTender$Builder;->payment_instrument(Lcom/squareup/protos/client/bills/PaymentInstrument;)Lcom/squareup/protos/client/bills/AddTender$Builder;

    move-result-object v3

    .line 246
    iget-object v4, v2, Lcom/squareup/protos/client/bills/AddTender;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    if-eqz v4, :cond_1

    .line 247
    iget-object v2, v2, Lcom/squareup/protos/client/bills/AddTender;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    .line 248
    invoke-direct {p0, p1, v2}, Lcom/squareup/payment/offline/BillInFlight;->encrypt(Lcom/squareup/encryption/CryptoPrimitive;Lcom/squareup/protos/client/bills/PaymentInstrument;)Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    move-result-object v2

    .line 247
    invoke-virtual {v3, v2}, Lcom/squareup/protos/client/bills/AddTender$Builder;->store_and_forward_payment_instrument(Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;)Lcom/squareup/protos/client/bills/AddTender$Builder;

    .line 250
    :cond_1
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/AddTender$Builder;->build()Lcom/squareup/protos/client/bills/AddTender;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 252
    :cond_2
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/AddTendersRequest;->newBuilder()Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object p1

    .line 253
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->add_tender(Ljava/util/List;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object p1

    .line 254
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersRequest;

    move-result-object p1

    return-object p1

    :cond_3
    :goto_1
    return-object p2
.end method

.method private encrypt(Lcom/squareup/encryption/CryptoPrimitive;Lcom/squareup/protos/client/bills/PaymentInstrument;)Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/encryption/CryptoPrimitive<",
            "Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;",
            ">;",
            "Lcom/squareup/protos/client/bills/PaymentInstrument;",
            ")",
            "Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    const-string v0, "encryptor"

    .line 260
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "paymentInstrument"

    .line 261
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 262
    sget-object v0, Lcom/squareup/protos/client/bills/PaymentInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 263
    invoke-virtual {v0, p2}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/encryption/CryptoPrimitive;->compute([B)Lcom/squareup/encryption/CryptoResult;

    move-result-object p1

    .line 265
    new-instance p2, Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument$Builder;-><init>()V

    .line 266
    invoke-virtual {p1}, Lcom/squareup/encryption/CryptoResult;->getValue()[B

    move-result-object p1

    invoke-static {p1}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument$Builder;->encrypted_payment_instrument(Lokio/ByteString;)Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument$Builder;

    move-result-object p1

    .line 267
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument$Builder;->build()Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    move-result-object p1

    return-object p1
.end method

.method private encrypt(Lcom/squareup/encryption/CryptoPrimitive;Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/encryption/CryptoPrimitive<",
            "Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    const-string v0, "encryptor"

    .line 226
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "addTendersRequests"

    .line 227
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 228
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 229
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/AddTendersRequest;

    .line 230
    invoke-direct {p0, p1, v1}, Lcom/squareup/payment/offline/BillInFlight;->encrypt(Lcom/squareup/encryption/CryptoPrimitive;Lcom/squareup/protos/client/bills/AddTendersRequest;)Lcom/squareup/protos/client/bills/AddTendersRequest;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static forTesting(Ljava/util/List;Lcom/squareup/queue/bills/CompleteBill;Ljava/util/List;)Lcom/squareup/payment/offline/BillInFlight;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest;",
            ">;",
            "Lcom/squareup/queue/bills/CompleteBill;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;",
            ">;)",
            "Lcom/squareup/payment/offline/BillInFlight;"
        }
    .end annotation

    .line 69
    new-instance v0, Lcom/squareup/payment/offline/BillInFlight;

    const-string v1, "NO_CLIENT_ID"

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/squareup/payment/offline/BillInFlight;-><init>(Ljava/util/List;Lcom/squareup/queue/bills/CompleteBill;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.method private static getExtraTenderDetails(Ljava/util/Collection;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;",
            ">;"
        }
    .end annotation

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 98
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/tender/BaseTender;

    .line 99
    invoke-static {v1}, Lcom/squareup/payment/offline/BillInFlight;->getSignature(Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/protos/client/Signature;

    move-result-object v2

    .line 100
    invoke-static {v1}, Lcom/squareup/payment/offline/BillInFlight;->getReceiptOption(Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/protos/client/ReceiptOption;

    move-result-object v3

    if-nez v3, :cond_1

    if-eqz v2, :cond_0

    .line 103
    :cond_1
    new-instance v4, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;-><init>()V

    .line 104
    invoke-virtual {v1}, Lcom/squareup/payment/tender/BaseTender;->getIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->tender_id(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;

    move-result-object v1

    .line 105
    invoke-virtual {v1, v3}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->receipt_option(Lcom/squareup/protos/client/ReceiptOption;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;

    move-result-object v1

    .line 106
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->signature(Lcom/squareup/protos/client/Signature;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;

    move-result-object v1

    .line 107
    invoke-virtual {v1}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;

    move-result-object v1

    .line 103
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 110
    :cond_2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static getMerchantAuthCode(Lcom/squareup/encryption/CryptoPrimitive;[B)Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/encryption/CryptoPrimitive<",
            "Lcom/squareup/server/account/protos/User$MerchantKey;",
            ">;[B)",
            "Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 74
    invoke-interface {p0, p1}, Lcom/squareup/encryption/CryptoPrimitive;->compute([B)Lcom/squareup/encryption/CryptoResult;

    move-result-object p0

    .line 75
    invoke-virtual {p0}, Lcom/squareup/encryption/CryptoResult;->getKey()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/account/protos/User$MerchantKey;

    .line 76
    new-instance v0, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;

    invoke-direct {v0}, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;-><init>()V

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$MerchantKey;->master_key_id:Ljava/lang/Integer;

    .line 77
    invoke-virtual {v0, v1}, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;->master_key_id(Ljava/lang/Integer;)Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/User$MerchantKey;->timestamp:Ljava/lang/Long;

    .line 78
    invoke-virtual {v0, p1}, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;->keygen_timestamp(Ljava/lang/Long;)Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;

    move-result-object p1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/encryption/CryptoResult;->getValue()[B

    move-result-object p0

    invoke-static {p0}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;->payload_mac(Lokio/ByteString;)Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;

    move-result-object p0

    .line 80
    invoke-virtual {p0}, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode$Builder;->build()Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    move-result-object p0

    return-object p0
.end method

.method private static getReceiptOption(Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/protos/client/ReceiptOption;
    .locals 4

    .line 127
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->getReceiptDestinationType()Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 132
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->getReceiptDestination()Ljava/lang/String;

    move-result-object p0

    .line 133
    new-instance v2, Lcom/squareup/protos/client/ReceiptOption$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/ReceiptOption$Builder;-><init>()V

    .line 134
    sget-object v3, Lcom/squareup/payment/offline/BillInFlight$1;->$SwitchMap$com$squareup$payment$tender$BaseTender$ReceiptDestinationType:[I

    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->ordinal()I

    move-result v0

    aget v0, v3, v0

    const/4 v3, 0x1

    if-eq v0, v3, :cond_4

    const/4 v3, 0x2

    if-eq v0, v3, :cond_3

    const/4 v3, 0x3

    if-eq v0, v3, :cond_2

    const/4 v3, 0x4

    if-eq v0, v3, :cond_1

    move-object v2, v1

    goto :goto_0

    .line 145
    :cond_1
    invoke-virtual {v2, p0}, Lcom/squareup/protos/client/ReceiptOption$Builder;->email_id(Ljava/lang/String;)Lcom/squareup/protos/client/ReceiptOption$Builder;

    goto :goto_0

    .line 142
    :cond_2
    invoke-virtual {v2, p0}, Lcom/squareup/protos/client/ReceiptOption$Builder;->phone_number_id(Ljava/lang/String;)Lcom/squareup/protos/client/ReceiptOption$Builder;

    goto :goto_0

    .line 139
    :cond_3
    invoke-virtual {v2, p0}, Lcom/squareup/protos/client/ReceiptOption$Builder;->email(Ljava/lang/String;)Lcom/squareup/protos/client/ReceiptOption$Builder;

    goto :goto_0

    .line 136
    :cond_4
    invoke-virtual {v2, p0}, Lcom/squareup/protos/client/ReceiptOption$Builder;->phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/ReceiptOption$Builder;

    :goto_0
    if-nez v2, :cond_5

    goto :goto_1

    .line 152
    :cond_5
    invoke-virtual {v2}, Lcom/squareup/protos/client/ReceiptOption$Builder;->build()Lcom/squareup/protos/client/ReceiptOption;

    move-result-object v1

    :goto_1
    return-object v1
.end method

.method private static getSignature(Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/protos/client/Signature;
    .locals 2

    .line 114
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender;->createEncodedSignatureData()Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 119
    :cond_0
    new-instance v0, Lcom/squareup/protos/client/Signature$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/Signature$Builder;-><init>()V

    sget-object v1, Lcom/squareup/protos/client/Signature$DataType;->VECTOR_V1:Lcom/squareup/protos/client/Signature$DataType;

    .line 120
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/Signature$Builder;->type(Lcom/squareup/protos/client/Signature$DataType;)Lcom/squareup/protos/client/Signature$Builder;

    move-result-object v0

    .line 121
    invoke-static {p0}, Lokio/ByteString;->encodeUtf8(Ljava/lang/String;)Lokio/ByteString;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/Signature$Builder;->data(Lokio/ByteString;)Lcom/squareup/protos/client/Signature$Builder;

    move-result-object p0

    .line 122
    invoke-virtual {p0}, Lcom/squareup/protos/client/Signature$Builder;->build()Lcom/squareup/protos/client/Signature;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public createStoredPayment(Lcom/squareup/payment/offline/QueueBertPublicKeyManager;Lcom/squareup/payment/offline/MerchantKeyManager;Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;ZLcom/squareup/http/SquareHeaders;)Lcom/squareup/payment/offline/StoredPayment;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager;",
            "Lcom/squareup/payment/offline/MerchantKeyManager;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/squareup/http/SquareHeaders;",
            ")",
            "Lcom/squareup/payment/offline/StoredPayment;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 161
    invoke-virtual {p1}, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->getBillEncryptor()Lcom/squareup/encryption/JweEncryptor;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 167
    invoke-virtual {p1}, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->getEncryptor()Lcom/squareup/encryption/JweEncryptor;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 172
    invoke-virtual {p2}, Lcom/squareup/payment/offline/MerchantKeyManager;->getAuthCodeGenerator()Lcom/squareup/encryption/CryptoPrimitive;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 178
    invoke-virtual {p3}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/settings/server/StoreAndForwardSettings;->getUserCredential()Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    move-result-object p3

    if-eqz p3, :cond_0

    .line 183
    invoke-interface {p6}, Lcom/squareup/http/SquareHeaders;->createMutableHeaderList()Ljava/util/List;

    move-result-object p6

    .line 184
    new-instance v1, Lcom/squareup/protos/common/Header$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/common/Header$Builder;-><init>()V

    const-string v2, "X-Connect-App-ID"

    invoke-virtual {v1, v2}, Lcom/squareup/protos/common/Header$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/common/Header$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/offline/BillInFlight;->clientId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/common/Header$Builder;->value(Ljava/lang/String;)Lcom/squareup/protos/common/Header$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Header$Builder;->build()Lcom/squareup/protos/common/Header;

    move-result-object v1

    invoke-interface {p6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    new-instance v1, Lcom/squareup/protos/common/Headers$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/common/Headers$Builder;-><init>()V

    invoke-virtual {v1, p6}, Lcom/squareup/protos/common/Headers$Builder;->header(Ljava/util/List;)Lcom/squareup/protos/common/Headers$Builder;

    move-result-object p6

    invoke-virtual {p6}, Lcom/squareup/protos/common/Headers$Builder;->build()Lcom/squareup/protos/common/Headers;

    move-result-object p6

    .line 187
    new-instance v1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;-><init>()V

    .line 188
    invoke-interface {p4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/lang/String;

    invoke-virtual {v1, p4}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->session_token(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;

    move-result-object p4

    iget-object v1, p0, Lcom/squareup/payment/offline/BillInFlight;->addTendersRequests:Ljava/util/List;

    .line 189
    invoke-direct {p0, p1, v1}, Lcom/squareup/payment/offline/BillInFlight;->encrypt(Lcom/squareup/encryption/CryptoPrimitive;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->add_tenders_request(Ljava/util/List;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;

    move-result-object p4

    iget-object v1, p0, Lcom/squareup/payment/offline/BillInFlight;->completeBill:Lcom/squareup/queue/bills/CompleteBill;

    iget-object v1, v1, Lcom/squareup/queue/bills/CompleteBill;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    .line 190
    invoke-virtual {p4, v1}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->complete_bill_request(Lcom/squareup/protos/client/bills/CompleteBillRequest;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;

    move-result-object p4

    .line 191
    invoke-virtual {p0}, Lcom/squareup/payment/offline/BillInFlight;->extraTenderDetailsForStoredPayment()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->extra_tender_detail(Ljava/util/List;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;

    move-result-object p4

    .line 192
    invoke-virtual {p4, p6}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->headers(Lcom/squareup/protos/common/Headers;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;

    move-result-object p4

    sget-object p6, Lcom/squareup/payment/offline/BillInFlight;->unescapedGson:Lcom/google/gson/Gson;

    .line 193
    invoke-virtual {p6, p3}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p4, p3}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->creator_credential(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;

    move-result-object p3

    .line 194
    invoke-virtual {p3}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;

    move-result-object p3

    .line 196
    sget-object p4, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 197
    invoke-virtual {p4, p3}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object p3

    invoke-interface {v0, p3}, Lcom/squareup/encryption/CryptoPrimitive;->compute([B)Lcom/squareup/encryption/CryptoResult;

    move-result-object p3

    .line 199
    invoke-interface {p1}, Lcom/squareup/encryption/CryptoPrimitive;->getKey()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;

    iget-object p1, p1, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;->bletchleyKeyId:Ljava/lang/String;

    .line 200
    invoke-interface {v0}, Lcom/squareup/encryption/CryptoPrimitive;->getKey()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;

    iget-object p4, p4, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;->bletchleyKeyId:Ljava/lang/String;

    .line 202
    invoke-virtual {p3}, Lcom/squareup/encryption/CryptoResult;->getValue()[B

    move-result-object p3

    .line 203
    new-instance p6, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;

    invoke-direct {p6}, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;-><init>()V

    iget-object v0, p0, Lcom/squareup/payment/offline/BillInFlight;->completeBill:Lcom/squareup/queue/bills/CompleteBill;

    iget-object v0, v0, Lcom/squareup/queue/bills/CompleteBill;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 204
    invoke-virtual {p6, v0}, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;->bill_id(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;

    move-result-object p6

    .line 205
    invoke-virtual {p6, p1}, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;->payment_instrument_encryption_key_token(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;

    move-result-object p1

    .line 206
    invoke-virtual {p1, p4}, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;->bill_payload_encryption_key_token(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;

    move-result-object p1

    .line 207
    invoke-static {p3}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p4

    invoke-virtual {p1, p4}, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;->encrypted_payload(Lokio/ByteString;)Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;

    move-result-object p1

    .line 208
    invoke-static {p2, p3}, Lcom/squareup/payment/offline/BillInFlight;->getMerchantAuthCode(Lcom/squareup/encryption/CryptoPrimitive;[B)Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;->auth_code(Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;)Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;

    move-result-object p1

    .line 209
    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;

    move-result-object p1

    .line 211
    new-instance p2, Lcom/squareup/payment/offline/StoredPayment;

    iget-object p3, p0, Lcom/squareup/payment/offline/BillInFlight;->completeBill:Lcom/squareup/queue/bills/CompleteBill;

    invoke-direct {p2, p3, p1, p5}, Lcom/squareup/payment/offline/StoredPayment;-><init>(Lcom/squareup/queue/bills/CompleteBill;Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;Z)V

    return-object p2

    .line 180
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "User credential is null!"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 174
    :cond_1
    new-instance p1, Ljava/security/InvalidKeyException;

    const-string p2, "No encoder available to generate merchant auth code"

    invoke-direct {p1, p2}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 169
    :cond_2
    new-instance p1, Ljava/security/InvalidKeyException;

    const-string p2, "No encoder available for offline payment instrument"

    invoke-direct {p1, p2}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 163
    :cond_3
    new-instance p1, Ljava/security/InvalidKeyException;

    const-string p2, "No encoder available for offline bill"

    invoke-direct {p1, p2}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method extraTenderDetailsForStoredPayment()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;",
            ">;"
        }
    .end annotation

    .line 220
    iget-object v0, p0, Lcom/squareup/payment/offline/BillInFlight;->extraTenderDetails:Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 221
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 279
    iget-object v0, p0, Lcom/squareup/payment/offline/BillInFlight;->completeBill:Lcom/squareup/queue/bills/CompleteBill;

    iget-object v0, v0, Lcom/squareup/queue/bills/CompleteBill;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getBillClientId()Ljava/lang/String;
    .locals 1

    .line 271
    iget-object v0, p0, Lcom/squareup/payment/offline/BillInFlight;->completeBill:Lcom/squareup/queue/bills/CompleteBill;

    iget-object v0, v0, Lcom/squareup/queue/bills/CompleteBill;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    return-object v0
.end method

.method public getTimestamp()Ljava/lang/String;
    .locals 1

    .line 275
    iget-object v0, p0, Lcom/squareup/payment/offline/BillInFlight;->completeBill:Lcom/squareup/queue/bills/CompleteBill;

    iget-object v0, v0, Lcom/squareup/queue/bills/CompleteBill;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Bill$Dates;->completed_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v0, v0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    return-object v0
.end method

.method public logFailed(Lcom/squareup/payment/ledger/TransactionLedgerManager;Ljava/lang/String;)V
    .locals 0

    .line 287
    invoke-interface {p1, p0, p2}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logStoreAndForwardBillFailed(Lcom/squareup/payment/offline/BillInFlight;Ljava/lang/String;)V

    return-void
.end method

.method public logReady(Lcom/squareup/payment/ledger/TransactionLedgerManager;Z)V
    .locals 0

    .line 283
    invoke-interface {p1, p0, p2}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logStoreAndForwardBillReady(Lcom/squareup/payment/offline/BillInFlight;Z)V

    return-void
.end method

.method public withLastOfflineReceipt(Lcom/squareup/payment/BillPayment;)Lcom/squareup/payment/offline/BillInFlight;
    .locals 4

    .line 91
    new-instance v0, Lcom/squareup/payment/offline/BillInFlight;

    iget-object v1, p0, Lcom/squareup/payment/offline/BillInFlight;->addTendersRequests:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/payment/offline/BillInFlight;->completeBill:Lcom/squareup/queue/bills/CompleteBill;

    .line 92
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->getFlushedTenders()Ljava/util/Set;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/payment/offline/BillInFlight;->getExtraTenderDetails(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    iget-object v3, p0, Lcom/squareup/payment/offline/BillInFlight;->clientId:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/squareup/payment/offline/BillInFlight;-><init>(Ljava/util/List;Lcom/squareup/queue/bills/CompleteBill;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method
