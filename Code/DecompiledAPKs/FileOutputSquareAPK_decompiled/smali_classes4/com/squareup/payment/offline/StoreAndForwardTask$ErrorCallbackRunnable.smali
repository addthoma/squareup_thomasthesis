.class abstract Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;
.super Ljava/lang/Object;
.source "StoreAndForwardTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/offline/StoreAndForwardTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "ErrorCallbackRunnable"
.end annotation


# instance fields
.field private final callback:Lcom/squareup/server/SquareCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;


# direct methods
.method constructor <init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/SquareCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 556
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 557
    iput-object p2, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;->callback:Lcom/squareup/server/SquareCallback;

    return-void
.end method


# virtual methods
.method protected abstract doRun(Lcom/squareup/server/SquareCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation
.end method

.method public synthetic lambda$run$0$StoreAndForwardTask$ErrorCallbackRunnable(Ljava/lang/Throwable;)V
    .locals 1

    .line 565
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;->callback:Lcom/squareup/server/SquareCallback;

    invoke-static {v0, p1}, Lcom/squareup/payment/offline/StoreAndForwardTask;->access$700(Lcom/squareup/server/SquareCallback;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final run()V
    .locals 3

    .line 562
    :try_start_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;->callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;->doRun(Lcom/squareup/server/SquareCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 565
    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v1, v1, Lcom/squareup/payment/offline/StoreAndForwardTask;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v2, Lcom/squareup/payment/offline/-$$Lambda$StoreAndForwardTask$ErrorCallbackRunnable$3iQwcL_6lQ7ju0EWTJEQTSHem_Q;

    invoke-direct {v2, p0, v0}, Lcom/squareup/payment/offline/-$$Lambda$StoreAndForwardTask$ErrorCallbackRunnable$3iQwcL_6lQ7ju0EWTJEQTSHem_Q;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;Ljava/lang/Throwable;)V

    invoke-interface {v1, v2}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method
