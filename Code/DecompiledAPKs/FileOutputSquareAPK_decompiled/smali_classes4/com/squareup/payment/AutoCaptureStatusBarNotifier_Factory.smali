.class public final Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;
.super Ljava/lang/Object;
.source "AutoCaptureStatusBarNotifier_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/AutoCaptureStatusBarNotifier;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationContextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final notificationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationWrapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;->applicationContextProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;->notificationManagerProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;->resProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p5, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;->notificationWrapperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;)",
            "Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;"
        }
    .end annotation

    .line 49
    new-instance v6, Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/text/Formatter;Landroid/app/NotificationManager;Lcom/squareup/util/Res;Lcom/squareup/notification/NotificationWrapper;)Lcom/squareup/payment/AutoCaptureStatusBarNotifier;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Landroid/app/NotificationManager;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/notification/NotificationWrapper;",
            ")",
            "Lcom/squareup/payment/AutoCaptureStatusBarNotifier;"
        }
    .end annotation

    .line 55
    new-instance v6, Lcom/squareup/payment/AutoCaptureStatusBarNotifier;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/payment/AutoCaptureStatusBarNotifier;-><init>(Landroid/app/Application;Lcom/squareup/text/Formatter;Landroid/app/NotificationManager;Lcom/squareup/util/Res;Lcom/squareup/notification/NotificationWrapper;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/payment/AutoCaptureStatusBarNotifier;
    .locals 5

    .line 41
    iget-object v0, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;->applicationContextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;->notificationManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    iget-object v3, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v4, p0, Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;->notificationWrapperProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/notification/NotificationWrapper;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/text/Formatter;Landroid/app/NotificationManager;Lcom/squareup/util/Res;Lcom/squareup/notification/NotificationWrapper;)Lcom/squareup/payment/AutoCaptureStatusBarNotifier;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/payment/AutoCaptureStatusBarNotifier_Factory;->get()Lcom/squareup/payment/AutoCaptureStatusBarNotifier;

    move-result-object v0

    return-object v0
.end method
