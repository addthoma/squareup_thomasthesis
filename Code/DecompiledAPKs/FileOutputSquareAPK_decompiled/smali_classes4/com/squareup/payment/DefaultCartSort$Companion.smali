.class public final Lcom/squareup/payment/DefaultCartSort$Companion;
.super Ljava/lang/Object;
.source "DefaultCartSort.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/DefaultCartSort;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J@\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\n2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u000c2\u0006\u0010\u0011\u001a\u00020\u000eH\u0002R\u001e\u0010\u0003\u001a\u0010\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/payment/DefaultCartSort$Companion;",
        "",
        "()V",
        "Comparator",
        "Ljava/util/Comparator;",
        "Lcom/squareup/checkout/CartItem;",
        "kotlin.jvm.PlatformType",
        "sortCompare",
        "",
        "lhsDate",
        "Ljava/util/Date;",
        "lhsName",
        "",
        "lhsVoided",
        "",
        "rhsDate",
        "rhsName",
        "rhsVoided",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/squareup/payment/DefaultCartSort$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$sortCompare(Lcom/squareup/payment/DefaultCartSort$Companion;Ljava/util/Date;Ljava/lang/String;ZLjava/util/Date;Ljava/lang/String;Z)I
    .locals 0

    .line 8
    invoke-direct/range {p0 .. p6}, Lcom/squareup/payment/DefaultCartSort$Companion;->sortCompare(Ljava/util/Date;Ljava/lang/String;ZLjava/util/Date;Ljava/lang/String;Z)I

    move-result p0

    return p0
.end method

.method private final sortCompare(Ljava/util/Date;Ljava/lang/String;ZLjava/util/Date;Ljava/lang/String;Z)I
    .locals 1

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    if-eqz p4, :cond_0

    .line 32
    invoke-virtual {p1, p4}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_3

    if-eqz p2, :cond_3

    if-eqz p5, :cond_2

    .line 40
    invoke-virtual {p2, p5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    goto :goto_1

    :cond_2
    const/4 p1, 0x1

    :cond_3
    :goto_1
    if-nez p1, :cond_5

    if-eq p3, p6, :cond_5

    if-eqz p3, :cond_4

    const/4 v0, -0x1

    const/4 p1, -0x1

    goto :goto_2

    :cond_4
    const/4 p1, 0x1

    :cond_5
    :goto_2
    return p1
.end method
