.class public Lcom/squareup/payment/pending/PaymentNotifier;
.super Ljava/lang/Object;
.source "PaymentNotifier.java"

# interfaces
.implements Lcom/squareup/badbus/BadBusRegistrant;
.implements Lmortar/Scoped;


# static fields
.field public static final PAYMENT_EXPIRATION_THRESHOLD_MS:J = 0xa4cb800L


# instance fields
.field private final appContext:Landroid/app/Application;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

.field private final jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private nonForwardedPendingPaymentsCount:I

.field private final pendingPaymentNotifier:Lcom/squareup/notifications/PendingPaymentNotifier;

.field private final pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

.field private final queueConformerWatcher:Lcom/squareup/queue/redundant/QueueConformerWatcher;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/queue/redundant/QueueConformerWatcher;Lcom/squareup/notifications/PendingPaymentNotifier;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/payment/pending/PaymentNotifier;->appContext:Landroid/app/Application;

    .line 53
    iput-object p2, p0, Lcom/squareup/payment/pending/PaymentNotifier;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 54
    iput-object p3, p0, Lcom/squareup/payment/pending/PaymentNotifier;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 55
    iput-object p4, p0, Lcom/squareup/payment/pending/PaymentNotifier;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    .line 56
    iput-object p5, p0, Lcom/squareup/payment/pending/PaymentNotifier;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    .line 57
    iput-object p6, p0, Lcom/squareup/payment/pending/PaymentNotifier;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    .line 58
    iput-object p7, p0, Lcom/squareup/payment/pending/PaymentNotifier;->queueConformerWatcher:Lcom/squareup/queue/redundant/QueueConformerWatcher;

    .line 59
    iput-object p8, p0, Lcom/squareup/payment/pending/PaymentNotifier;->pendingPaymentNotifier:Lcom/squareup/notifications/PendingPaymentNotifier;

    return-void
.end method

.method private onTaskRequiresRetry()V
    .locals 0

    .line 68
    invoke-virtual {p0}, Lcom/squareup/payment/pending/PaymentNotifier;->updateNotification()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$1$PaymentNotifier(Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/connectivity/InternetState;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 85
    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne p3, v0, :cond_0

    move-object p1, p2

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iput p1, p0, Lcom/squareup/payment/pending/PaymentNotifier;->nonForwardedPendingPaymentsCount:I

    .line 90
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 91
    invoke-virtual {p0}, Lcom/squareup/payment/pending/PaymentNotifier;->updateNotification()V

    .line 93
    :cond_1
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public synthetic lambda$registerOnBus$0$PaymentNotifier(Lcom/squareup/queue/QueueService$TaskRequiresRetry;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 64
    invoke-direct {p0}, Lcom/squareup/payment/pending/PaymentNotifier;->onTaskRequiresRetry()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 7

    .line 72
    new-instance v6, Lcom/squareup/payment/pending/PaymentNotifierJobCreator;

    iget-object v1, p0, Lcom/squareup/payment/pending/PaymentNotifier;->appContext:Landroid/app/Application;

    iget-object v2, p0, Lcom/squareup/payment/pending/PaymentNotifier;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v3, p0, Lcom/squareup/payment/pending/PaymentNotifier;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    iget-object v4, p0, Lcom/squareup/payment/pending/PaymentNotifier;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    move-object v0, v6

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/payment/pending/PaymentNotifierJobCreator;-><init>(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/payment/pending/PaymentNotifier;)V

    invoke-virtual {p1, v6}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/payment/pending/PaymentNotifier;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    .line 78
    invoke-interface {v0}, Lcom/squareup/payment/pending/PendingTransactionsStore;->smoothedAllPendingTransactionsCount()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/pending/PaymentNotifier;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    .line 79
    invoke-interface {v1}, Lcom/squareup/payment/pending/PendingTransactionsStore;->smoothedRipenedAllPendingTransactionsCount()Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/pending/PaymentNotifier;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 80
    invoke-interface {v2}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/payment/pending/PaymentNotifier;->queueConformerWatcher:Lcom/squareup/queue/redundant/QueueConformerWatcher;

    .line 81
    invoke-virtual {v3}, Lcom/squareup/queue/redundant/QueueConformerWatcher;->queuesConformed()Lrx/Observable;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v3

    .line 82
    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toQuartet()Lio/reactivex/functions/Function4;

    move-result-object v4

    .line 77
    invoke-static {v0, v1, v2, v3, v4}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/payment/pending/-$$Lambda$PaymentNotifier$OIrspFg_aUU7Kj0kPqAub0u0kYc;

    invoke-direct {v1, p0}, Lcom/squareup/payment/pending/-$$Lambda$PaymentNotifier$OIrspFg_aUU7Kj0kPqAub0u0kYc;-><init>(Lcom/squareup/payment/pending/PaymentNotifier;)V

    .line 83
    invoke-static {v1}, Lcom/squareup/util/Rx2Tuples;->expandQuartet(Lio/reactivex/functions/Function4;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 76
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public registerOnBus(Lcom/squareup/badbus/BadBus;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 63
    const-class v0, Lcom/squareup/queue/QueueService$TaskRequiresRetry;

    invoke-virtual {p1, v0}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/payment/pending/-$$Lambda$PaymentNotifier$vvvZFUDObgjhK7dBHkgdmS-esNY;

    invoke-direct {v0, p0}, Lcom/squareup/payment/pending/-$$Lambda$PaymentNotifier$vvvZFUDObgjhK7dBHkgdmS-esNY;-><init>(Lcom/squareup/payment/pending/PaymentNotifier;)V

    .line 64
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public updateNotification()V
    .locals 2

    .line 102
    iget v0, p0, Lcom/squareup/payment/pending/PaymentNotifier;->nonForwardedPendingPaymentsCount:I

    if-lez v0, :cond_0

    .line 104
    iget-object v1, p0, Lcom/squareup/payment/pending/PaymentNotifier;->pendingPaymentNotifier:Lcom/squareup/notifications/PendingPaymentNotifier;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/squareup/notifications/PendingPaymentNotifier;->showNotification(Ljava/lang/Integer;)V

    goto :goto_0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/pending/PaymentNotifier;->pendingPaymentNotifier:Lcom/squareup/notifications/PendingPaymentNotifier;

    invoke-interface {v0}, Lcom/squareup/notifications/PendingPaymentNotifier;->hideNotification()V

    :goto_0
    return-void
.end method
