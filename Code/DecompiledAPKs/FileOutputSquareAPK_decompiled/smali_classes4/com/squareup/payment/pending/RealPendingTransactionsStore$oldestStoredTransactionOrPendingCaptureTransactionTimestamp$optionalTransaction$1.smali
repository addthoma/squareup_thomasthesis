.class final Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransactionTimestamp$optionalTransaction$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealPendingTransactionsStore.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/pending/RealPendingTransactionsStore;->oldestStoredTransactionOrPendingCaptureTransactionTimestamp()J
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/squareup/util/Optional<",
        "+",
        "Lcom/squareup/queue/CaptureTask;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/queue/CaptureTask;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransactionTimestamp$optionalTransaction$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransactionTimestamp$optionalTransaction$1;

    invoke-direct {v0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransactionTimestamp$optionalTransaction$1;-><init>()V

    sput-object v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransactionTimestamp$optionalTransaction$1;->INSTANCE:Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransactionTimestamp$optionalTransaction$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/queue/CaptureTask;",
            ">;"
        }
    .end annotation

    .line 315
    sget-object v0, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v0}, Lcom/squareup/util/Optional$Companion;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 54
    invoke-virtual {p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransactionTimestamp$optionalTransaction$1;->invoke()Lcom/squareup/util/Optional;

    move-result-object v0

    return-object v0
.end method
