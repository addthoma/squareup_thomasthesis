.class public final Lcom/squareup/payment/pending/RealPendingTransactionsStore_CountSmoother_Factory;
.super Ljava/lang/Object;
.source "RealPendingTransactionsStore_CountSmoother_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;",
        ">;"
    }
.end annotation


# instance fields
.field private final forwardedPaymentManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/ForwardedPaymentManager;",
            ">;"
        }
    .end annotation
.end field

.field private final localPaymentsMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingCapturesMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/PendingCapturesMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final storedPaymentsMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/ForwardedPaymentManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/PendingCapturesMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_CountSmoother_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_CountSmoother_Factory;->forwardedPaymentManagerProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_CountSmoother_Factory;->storedPaymentsMonitorProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_CountSmoother_Factory;->pendingCapturesMonitorProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_CountSmoother_Factory;->localPaymentsMonitorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/pending/RealPendingTransactionsStore_CountSmoother_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/ForwardedPaymentManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/PendingCapturesMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;",
            ">;)",
            "Lcom/squareup/payment/pending/RealPendingTransactionsStore_CountSmoother_Factory;"
        }
    .end annotation

    .line 55
    new-instance v6, Lcom/squareup/payment/pending/RealPendingTransactionsStore_CountSmoother_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/payment/pending/RealPendingTransactionsStore_CountSmoother_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lio/reactivex/Scheduler;Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;Lcom/squareup/queue/sqlite/PendingCapturesMonitor;Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;)Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;
    .locals 7

    .line 61
    new-instance v6, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;Lcom/squareup/queue/sqlite/PendingCapturesMonitor;Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;
    .locals 5

    .line 46
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_CountSmoother_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/Scheduler;

    iget-object v1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_CountSmoother_Factory;->forwardedPaymentManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/offline/ForwardedPaymentManager;

    iget-object v2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_CountSmoother_Factory;->storedPaymentsMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;

    iget-object v3, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_CountSmoother_Factory;->pendingCapturesMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

    iget-object v4, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore_CountSmoother_Factory;->localPaymentsMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/payment/pending/RealPendingTransactionsStore_CountSmoother_Factory;->newInstance(Lio/reactivex/Scheduler;Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;Lcom/squareup/queue/sqlite/PendingCapturesMonitor;Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;)Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore_CountSmoother_Factory;->get()Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;

    move-result-object v0

    return-object v0
.end method
