.class public Lcom/squareup/payment/TipDeterminer;
.super Ljava/lang/Object;
.source "TipDeterminer.java"


# instance fields
.field private final featureFlagPreAuthPreferred:Z

.field private final featureFlagPreAuthRequired:Z

.field private final separateTipAndSigScreenSetting:Z

.field private final tenderExistsAndSupportsPreAuthPreferred:Z

.field private final tenderExistsAndWantsTipPrompt:Z

.field private final tipOnDeviceSetting:Z

.field private final tipOnReceipt:Z


# direct methods
.method public constructor <init>(ZZZZZZZ)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p1, p0, Lcom/squareup/payment/TipDeterminer;->featureFlagPreAuthPreferred:Z

    .line 17
    iput-boolean p2, p0, Lcom/squareup/payment/TipDeterminer;->featureFlagPreAuthRequired:Z

    .line 18
    iput-boolean p3, p0, Lcom/squareup/payment/TipDeterminer;->separateTipAndSigScreenSetting:Z

    .line 19
    iput-boolean p4, p0, Lcom/squareup/payment/TipDeterminer;->tenderExistsAndWantsTipPrompt:Z

    .line 20
    iput-boolean p5, p0, Lcom/squareup/payment/TipDeterminer;->tenderExistsAndSupportsPreAuthPreferred:Z

    .line 21
    iput-boolean p6, p0, Lcom/squareup/payment/TipDeterminer;->tipOnDeviceSetting:Z

    .line 22
    iput-boolean p7, p0, Lcom/squareup/payment/TipDeterminer;->tipOnReceipt:Z

    return-void
.end method


# virtual methods
.method public showPostAuthTipScreen()Z
    .locals 2

    .line 26
    iget-boolean v0, p0, Lcom/squareup/payment/TipDeterminer;->tipOnDeviceSetting:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 30
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/payment/TipDeterminer;->featureFlagPreAuthRequired:Z

    if-eqz v0, :cond_1

    return v1

    .line 34
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/payment/TipDeterminer;->featureFlagPreAuthPreferred:Z

    if-eqz v0, :cond_4

    .line 35
    iget-boolean v0, p0, Lcom/squareup/payment/TipDeterminer;->separateTipAndSigScreenSetting:Z

    if-nez v0, :cond_2

    .line 36
    iget-boolean v0, p0, Lcom/squareup/payment/TipDeterminer;->tenderExistsAndWantsTipPrompt:Z

    return v0

    .line 39
    :cond_2
    iget-boolean v0, p0, Lcom/squareup/payment/TipDeterminer;->tipOnReceipt:Z

    if-eqz v0, :cond_3

    return v1

    .line 43
    :cond_3
    iget-boolean v0, p0, Lcom/squareup/payment/TipDeterminer;->tenderExistsAndSupportsPreAuthPreferred:Z

    if-eqz v0, :cond_4

    return v1

    .line 48
    :cond_4
    iget-boolean v0, p0, Lcom/squareup/payment/TipDeterminer;->tenderExistsAndWantsTipPrompt:Z

    return v0
.end method

.method public showPreAuthTipScreen()Z
    .locals 2

    .line 52
    iget-boolean v0, p0, Lcom/squareup/payment/TipDeterminer;->tipOnDeviceSetting:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 56
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/payment/TipDeterminer;->featureFlagPreAuthRequired:Z

    if-eqz v0, :cond_1

    .line 57
    iget-boolean v0, p0, Lcom/squareup/payment/TipDeterminer;->tenderExistsAndWantsTipPrompt:Z

    return v0

    .line 60
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/payment/TipDeterminer;->featureFlagPreAuthPreferred:Z

    if-eqz v0, :cond_4

    .line 61
    iget-boolean v0, p0, Lcom/squareup/payment/TipDeterminer;->separateTipAndSigScreenSetting:Z

    if-nez v0, :cond_2

    return v1

    .line 65
    :cond_2
    iget-boolean v0, p0, Lcom/squareup/payment/TipDeterminer;->tipOnReceipt:Z

    if-eqz v0, :cond_3

    return v1

    .line 69
    :cond_3
    iget-boolean v0, p0, Lcom/squareup/payment/TipDeterminer;->tenderExistsAndSupportsPreAuthPreferred:Z

    if-eqz v0, :cond_4

    .line 70
    iget-boolean v0, p0, Lcom/squareup/payment/TipDeterminer;->tenderExistsAndWantsTipPrompt:Z

    return v0

    :cond_4
    return v1
.end method
