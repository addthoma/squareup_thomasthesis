.class public interface abstract Lcom/squareup/payment/RequiresAuthorization;
.super Ljava/lang/Object;
.source "RequiresAuthorization.java"


# virtual methods
.method public abstract authorize()V
.end method

.method public abstract canAutoCapture()Z
.end method

.method public abstract canQuickCapture()Z
.end method

.method public abstract cancel(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
.end method

.method public abstract capture(Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation
.end method

.method public abstract createCancelTask(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/queue/CancelTask;
.end method

.method public abstract createCaptureTask(Z)Lcom/squareup/queue/CaptureTask;
.end method

.method public abstract getBillId()Lcom/squareup/protos/client/IdPair;
.end method

.method public abstract getTotal()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getUniqueClientId()Ljava/lang/String;
.end method

.method public abstract hasRequestedAuthorization()Z
.end method

.method public abstract isCaptured()Z
.end method

.method public abstract restartAutoCaptureTimer()V
.end method
