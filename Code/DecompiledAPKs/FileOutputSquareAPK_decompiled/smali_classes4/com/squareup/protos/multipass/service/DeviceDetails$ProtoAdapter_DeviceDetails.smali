.class final Lcom/squareup/protos/multipass/service/DeviceDetails$ProtoAdapter_DeviceDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "DeviceDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/service/DeviceDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_DeviceDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/multipass/service/DeviceDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 339
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/multipass/service/DeviceDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/service/DeviceDetails;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 366
    new-instance v0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;-><init>()V

    .line 367
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 368
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 384
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 382
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->is_squid(Ljava/lang/Boolean;)Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;

    goto :goto_0

    .line 381
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->serial(Ljava/lang/String;)Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;

    goto :goto_0

    .line 380
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->secondary_device_id(Ljava/lang/String;)Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;

    goto :goto_0

    .line 379
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->ip_address(Ljava/lang/String;)Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;

    goto :goto_0

    .line 378
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->device_id(Ljava/lang/String;)Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;

    goto :goto_0

    .line 372
    :pswitch_5
    :try_start_0
    sget-object v4, Lcom/squareup/protos/multipass/service/DeviceDetails$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/multipass/service/DeviceDetails$Type;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->type(Lcom/squareup/protos/multipass/service/DeviceDetails$Type;)Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 374
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 388
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 389
    invoke-virtual {v0}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->build()Lcom/squareup/protos/multipass/service/DeviceDetails;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 337
    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/service/DeviceDetails$ProtoAdapter_DeviceDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/service/DeviceDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/service/DeviceDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 355
    sget-object v0, Lcom/squareup/protos/multipass/service/DeviceDetails$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/service/DeviceDetails;->type:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 356
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/service/DeviceDetails;->device_id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 357
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/service/DeviceDetails;->ip_address:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 358
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/service/DeviceDetails;->secondary_device_id:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 359
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/service/DeviceDetails;->serial:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 360
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/service/DeviceDetails;->is_squid:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 361
    invoke-virtual {p2}, Lcom/squareup/protos/multipass/service/DeviceDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 337
    check-cast p2, Lcom/squareup/protos/multipass/service/DeviceDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/multipass/service/DeviceDetails$ProtoAdapter_DeviceDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/service/DeviceDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/multipass/service/DeviceDetails;)I
    .locals 4

    .line 344
    sget-object v0, Lcom/squareup/protos/multipass/service/DeviceDetails$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/service/DeviceDetails;->type:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/service/DeviceDetails;->device_id:Ljava/lang/String;

    const/4 v3, 0x2

    .line 345
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/service/DeviceDetails;->ip_address:Ljava/lang/String;

    const/4 v3, 0x3

    .line 346
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/service/DeviceDetails;->secondary_device_id:Ljava/lang/String;

    const/4 v3, 0x4

    .line 347
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/service/DeviceDetails;->serial:Ljava/lang/String;

    const/4 v3, 0x5

    .line 348
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/service/DeviceDetails;->is_squid:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 349
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 350
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/DeviceDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 337
    check-cast p1, Lcom/squareup/protos/multipass/service/DeviceDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/service/DeviceDetails$ProtoAdapter_DeviceDetails;->encodedSize(Lcom/squareup/protos/multipass/service/DeviceDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/multipass/service/DeviceDetails;)Lcom/squareup/protos/multipass/service/DeviceDetails;
    .locals 1

    .line 394
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/DeviceDetails;->newBuilder()Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 395
    iput-object v0, p1, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->device_id:Ljava/lang/String;

    .line 396
    iput-object v0, p1, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->ip_address:Ljava/lang/String;

    .line 397
    iput-object v0, p1, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->secondary_device_id:Ljava/lang/String;

    .line 398
    iput-object v0, p1, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->serial:Ljava/lang/String;

    .line 399
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 400
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->build()Lcom/squareup/protos/multipass/service/DeviceDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 337
    check-cast p1, Lcom/squareup/protos/multipass/service/DeviceDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/service/DeviceDetails$ProtoAdapter_DeviceDetails;->redact(Lcom/squareup/protos/multipass/service/DeviceDetails;)Lcom/squareup/protos/multipass/service/DeviceDetails;

    move-result-object p1

    return-object p1
.end method
