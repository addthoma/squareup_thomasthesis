.class public final enum Lcom/squareup/protos/multipass/common/ScopedSession;
.super Ljava/lang/Enum;
.source "ScopedSession.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/multipass/common/ScopedSession$ProtoAdapter_ScopedSession;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/multipass/common/ScopedSession;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/multipass/common/ScopedSession;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/multipass/common/ScopedSession;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BUYER:Lcom/squareup/protos/multipass/common/ScopedSession;

.field public static final enum CAPITAL_CONSUMER:Lcom/squareup/protos/multipass/common/ScopedSession;

.field public static final enum CAPITAL_PARTNERSHIPS:Lcom/squareup/protos/multipass/common/ScopedSession;

.field public static final enum HIRING_WEB:Lcom/squareup/protos/multipass/common/ScopedSession;

.field public static final enum SQUARE:Lcom/squareup/protos/multipass/common/ScopedSession;

.field public static final enum SQUARE_BUYER:Lcom/squareup/protos/multipass/common/ScopedSession;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 18
    new-instance v0, Lcom/squareup/protos/multipass/common/ScopedSession;

    const/4 v1, 0x0

    const-string v2, "SQUARE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/multipass/common/ScopedSession;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/common/ScopedSession;->SQUARE:Lcom/squareup/protos/multipass/common/ScopedSession;

    .line 23
    new-instance v0, Lcom/squareup/protos/multipass/common/ScopedSession;

    const/4 v2, 0x1

    const-string v3, "CAPITAL_PARTNERSHIPS"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/multipass/common/ScopedSession;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/common/ScopedSession;->CAPITAL_PARTNERSHIPS:Lcom/squareup/protos/multipass/common/ScopedSession;

    .line 28
    new-instance v0, Lcom/squareup/protos/multipass/common/ScopedSession;

    const/4 v3, 0x2

    const-string v4, "BUYER"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/multipass/common/ScopedSession;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/common/ScopedSession;->BUYER:Lcom/squareup/protos/multipass/common/ScopedSession;

    .line 33
    new-instance v0, Lcom/squareup/protos/multipass/common/ScopedSession;

    const/4 v4, 0x3

    const-string v5, "CAPITAL_CONSUMER"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/multipass/common/ScopedSession;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/common/ScopedSession;->CAPITAL_CONSUMER:Lcom/squareup/protos/multipass/common/ScopedSession;

    .line 38
    new-instance v0, Lcom/squareup/protos/multipass/common/ScopedSession;

    const/4 v5, 0x4

    const-string v6, "HIRING_WEB"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/multipass/common/ScopedSession;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/common/ScopedSession;->HIRING_WEB:Lcom/squareup/protos/multipass/common/ScopedSession;

    .line 43
    new-instance v0, Lcom/squareup/protos/multipass/common/ScopedSession;

    const/4 v6, 0x5

    const-string v7, "SQUARE_BUYER"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/multipass/common/ScopedSession;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/multipass/common/ScopedSession;->SQUARE_BUYER:Lcom/squareup/protos/multipass/common/ScopedSession;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/multipass/common/ScopedSession;

    .line 13
    sget-object v7, Lcom/squareup/protos/multipass/common/ScopedSession;->SQUARE:Lcom/squareup/protos/multipass/common/ScopedSession;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/multipass/common/ScopedSession;->CAPITAL_PARTNERSHIPS:Lcom/squareup/protos/multipass/common/ScopedSession;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/multipass/common/ScopedSession;->BUYER:Lcom/squareup/protos/multipass/common/ScopedSession;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/multipass/common/ScopedSession;->CAPITAL_CONSUMER:Lcom/squareup/protos/multipass/common/ScopedSession;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/multipass/common/ScopedSession;->HIRING_WEB:Lcom/squareup/protos/multipass/common/ScopedSession;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/multipass/common/ScopedSession;->SQUARE_BUYER:Lcom/squareup/protos/multipass/common/ScopedSession;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/multipass/common/ScopedSession;->$VALUES:[Lcom/squareup/protos/multipass/common/ScopedSession;

    .line 45
    new-instance v0, Lcom/squareup/protos/multipass/common/ScopedSession$ProtoAdapter_ScopedSession;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/common/ScopedSession$ProtoAdapter_ScopedSession;-><init>()V

    sput-object v0, Lcom/squareup/protos/multipass/common/ScopedSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 50
    iput p3, p0, Lcom/squareup/protos/multipass/common/ScopedSession;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/multipass/common/ScopedSession;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 63
    :cond_0
    sget-object p0, Lcom/squareup/protos/multipass/common/ScopedSession;->SQUARE_BUYER:Lcom/squareup/protos/multipass/common/ScopedSession;

    return-object p0

    .line 62
    :cond_1
    sget-object p0, Lcom/squareup/protos/multipass/common/ScopedSession;->HIRING_WEB:Lcom/squareup/protos/multipass/common/ScopedSession;

    return-object p0

    .line 61
    :cond_2
    sget-object p0, Lcom/squareup/protos/multipass/common/ScopedSession;->CAPITAL_CONSUMER:Lcom/squareup/protos/multipass/common/ScopedSession;

    return-object p0

    .line 60
    :cond_3
    sget-object p0, Lcom/squareup/protos/multipass/common/ScopedSession;->BUYER:Lcom/squareup/protos/multipass/common/ScopedSession;

    return-object p0

    .line 59
    :cond_4
    sget-object p0, Lcom/squareup/protos/multipass/common/ScopedSession;->CAPITAL_PARTNERSHIPS:Lcom/squareup/protos/multipass/common/ScopedSession;

    return-object p0

    .line 58
    :cond_5
    sget-object p0, Lcom/squareup/protos/multipass/common/ScopedSession;->SQUARE:Lcom/squareup/protos/multipass/common/ScopedSession;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/ScopedSession;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/multipass/common/ScopedSession;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/multipass/common/ScopedSession;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/multipass/common/ScopedSession;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/multipass/common/ScopedSession;->$VALUES:[Lcom/squareup/protos/multipass/common/ScopedSession;

    invoke-virtual {v0}, [Lcom/squareup/protos/multipass/common/ScopedSession;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/multipass/common/ScopedSession;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 70
    iget v0, p0, Lcom/squareup/protos/multipass/common/ScopedSession;->value:I

    return v0
.end method
