.class public final Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SmsTwoFactor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/common/SmsTwoFactor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/multipass/common/SmsTwoFactor;",
        "Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public phone:Ljava/lang/String;

.field public verification_code:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 103
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/multipass/common/SmsTwoFactor;
    .locals 4

    .line 125
    new-instance v0, Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;->phone:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;->verification_code:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/multipass/common/SmsTwoFactor;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;->build()Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    move-result-object v0

    return-object v0
.end method

.method public phone(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;->phone:Ljava/lang/String;

    return-object p0
.end method

.method public verification_code(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;->verification_code:Ljava/lang/String;

    return-object p0
.end method
