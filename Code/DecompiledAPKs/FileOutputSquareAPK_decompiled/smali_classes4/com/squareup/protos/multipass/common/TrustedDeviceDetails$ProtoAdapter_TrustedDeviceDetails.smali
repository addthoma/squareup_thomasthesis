.class final Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$ProtoAdapter_TrustedDeviceDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "TrustedDeviceDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_TrustedDeviceDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 164
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 185
    new-instance v0, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;-><init>()V

    .line 186
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 187
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 193
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 191
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;->person_token(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;

    goto :goto_0

    .line 190
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;->expiry(Ljava/lang/Long;)Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;

    goto :goto_0

    .line 189
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;

    goto :goto_0

    .line 197
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 198
    invoke-virtual {v0}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;->build()Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 162
    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$ProtoAdapter_TrustedDeviceDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 177
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;->token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 178
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;->expiry:Ljava/lang/Long;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 179
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;->person_token:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 180
    invoke-virtual {p2}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 162
    check-cast p2, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$ProtoAdapter_TrustedDeviceDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;)I
    .locals 4

    .line 169
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;->token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;->expiry:Ljava/lang/Long;

    const/4 v3, 0x2

    .line 170
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;->person_token:Ljava/lang/String;

    const/4 v3, 0x3

    .line 171
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 162
    check-cast p1, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$ProtoAdapter_TrustedDeviceDetails;->encodedSize(Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;)Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;
    .locals 1

    .line 203
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;->newBuilder()Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 204
    iput-object v0, p1, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;->token:Ljava/lang/String;

    .line 205
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 206
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$Builder;->build()Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 162
    check-cast p1, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails$ProtoAdapter_TrustedDeviceDetails;->redact(Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;)Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    move-result-object p1

    return-object p1
.end method
