.class public final enum Lcom/squareup/protos/checklist/signal/ActivationStatus;
.super Ljava/lang/Enum;
.source "ActivationStatus.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/checklist/signal/ActivationStatus$ProtoAdapter_ActivationStatus;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/checklist/signal/ActivationStatus;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/checklist/signal/ActivationStatus;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/checklist/signal/ActivationStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum APPROVED:Lcom/squareup/protos/checklist/signal/ActivationStatus;

.field public static final enum DECLINED:Lcom/squareup/protos/checklist/signal/ActivationStatus;

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/checklist/signal/ActivationStatus;

.field public static final enum MANUAL_REVIEW:Lcom/squareup/protos/checklist/signal/ActivationStatus;

.field public static final enum OPTIMISTICALLY_APPROVED:Lcom/squareup/protos/checklist/signal/ActivationStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 11
    new-instance v0, Lcom/squareup/protos/checklist/signal/ActivationStatus;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/checklist/signal/ActivationStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/ActivationStatus;->DO_NOT_USE:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    .line 13
    new-instance v0, Lcom/squareup/protos/checklist/signal/ActivationStatus;

    const/4 v2, 0x1

    const-string v3, "DECLINED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/checklist/signal/ActivationStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/ActivationStatus;->DECLINED:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    .line 15
    new-instance v0, Lcom/squareup/protos/checklist/signal/ActivationStatus;

    const/4 v3, 0x2

    const-string v4, "MANUAL_REVIEW"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/checklist/signal/ActivationStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/ActivationStatus;->MANUAL_REVIEW:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    .line 17
    new-instance v0, Lcom/squareup/protos/checklist/signal/ActivationStatus;

    const/4 v4, 0x3

    const-string v5, "OPTIMISTICALLY_APPROVED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/checklist/signal/ActivationStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/ActivationStatus;->OPTIMISTICALLY_APPROVED:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    .line 19
    new-instance v0, Lcom/squareup/protos/checklist/signal/ActivationStatus;

    const/4 v5, 0x4

    const-string v6, "APPROVED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/checklist/signal/ActivationStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/ActivationStatus;->APPROVED:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/checklist/signal/ActivationStatus;

    .line 10
    sget-object v6, Lcom/squareup/protos/checklist/signal/ActivationStatus;->DO_NOT_USE:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/checklist/signal/ActivationStatus;->DECLINED:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/ActivationStatus;->MANUAL_REVIEW:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/checklist/signal/ActivationStatus;->OPTIMISTICALLY_APPROVED:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/checklist/signal/ActivationStatus;->APPROVED:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/checklist/signal/ActivationStatus;->$VALUES:[Lcom/squareup/protos/checklist/signal/ActivationStatus;

    .line 21
    new-instance v0, Lcom/squareup/protos/checklist/signal/ActivationStatus$ProtoAdapter_ActivationStatus;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/signal/ActivationStatus$ProtoAdapter_ActivationStatus;-><init>()V

    sput-object v0, Lcom/squareup/protos/checklist/signal/ActivationStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    iput p3, p0, Lcom/squareup/protos/checklist/signal/ActivationStatus;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/checklist/signal/ActivationStatus;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 38
    :cond_0
    sget-object p0, Lcom/squareup/protos/checklist/signal/ActivationStatus;->APPROVED:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    return-object p0

    .line 37
    :cond_1
    sget-object p0, Lcom/squareup/protos/checklist/signal/ActivationStatus;->OPTIMISTICALLY_APPROVED:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    return-object p0

    .line 36
    :cond_2
    sget-object p0, Lcom/squareup/protos/checklist/signal/ActivationStatus;->MANUAL_REVIEW:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    return-object p0

    .line 35
    :cond_3
    sget-object p0, Lcom/squareup/protos/checklist/signal/ActivationStatus;->DECLINED:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    return-object p0

    .line 34
    :cond_4
    sget-object p0, Lcom/squareup/protos/checklist/signal/ActivationStatus;->DO_NOT_USE:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/checklist/signal/ActivationStatus;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/checklist/signal/ActivationStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/checklist/signal/ActivationStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/checklist/signal/ActivationStatus;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/checklist/signal/ActivationStatus;->$VALUES:[Lcom/squareup/protos/checklist/signal/ActivationStatus;

    invoke-virtual {v0}, [Lcom/squareup/protos/checklist/signal/ActivationStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/checklist/signal/ActivationStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 45
    iget v0, p0, Lcom/squareup/protos/checklist/signal/ActivationStatus;->value:I

    return v0
.end method
