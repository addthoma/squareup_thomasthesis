.class final Lcom/squareup/protos/checklist/service/GetChecklistResponse$ProtoAdapter_GetChecklistResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetChecklistResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/checklist/service/GetChecklistResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetChecklistResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/checklist/service/GetChecklistResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 115
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/checklist/service/GetChecklistResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/checklist/service/GetChecklistResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 134
    new-instance v0, Lcom/squareup/protos/checklist/service/GetChecklistResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/service/GetChecklistResponse$Builder;-><init>()V

    .line 135
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 136
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 141
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 139
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/checklist/service/GetChecklistResponse$Builder;->visible(Ljava/lang/Boolean;)Lcom/squareup/protos/checklist/service/GetChecklistResponse$Builder;

    goto :goto_0

    .line 138
    :cond_1
    sget-object v3, Lcom/squareup/protos/checklist/common/Checklist;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/checklist/common/Checklist;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/checklist/service/GetChecklistResponse$Builder;->checklist(Lcom/squareup/protos/checklist/common/Checklist;)Lcom/squareup/protos/checklist/service/GetChecklistResponse$Builder;

    goto :goto_0

    .line 145
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/checklist/service/GetChecklistResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 146
    invoke-virtual {v0}, Lcom/squareup/protos/checklist/service/GetChecklistResponse$Builder;->build()Lcom/squareup/protos/checklist/service/GetChecklistResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 113
    invoke-virtual {p0, p1}, Lcom/squareup/protos/checklist/service/GetChecklistResponse$ProtoAdapter_GetChecklistResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/checklist/service/GetChecklistResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/checklist/service/GetChecklistResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 127
    sget-object v0, Lcom/squareup/protos/checklist/common/Checklist;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/checklist/service/GetChecklistResponse;->checklist:Lcom/squareup/protos/checklist/common/Checklist;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 128
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/checklist/service/GetChecklistResponse;->visible:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 129
    invoke-virtual {p2}, Lcom/squareup/protos/checklist/service/GetChecklistResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 113
    check-cast p2, Lcom/squareup/protos/checklist/service/GetChecklistResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/checklist/service/GetChecklistResponse$ProtoAdapter_GetChecklistResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/checklist/service/GetChecklistResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/checklist/service/GetChecklistResponse;)I
    .locals 4

    .line 120
    sget-object v0, Lcom/squareup/protos/checklist/common/Checklist;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/checklist/service/GetChecklistResponse;->checklist:Lcom/squareup/protos/checklist/common/Checklist;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/checklist/service/GetChecklistResponse;->visible:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 121
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 122
    invoke-virtual {p1}, Lcom/squareup/protos/checklist/service/GetChecklistResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 113
    check-cast p1, Lcom/squareup/protos/checklist/service/GetChecklistResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/checklist/service/GetChecklistResponse$ProtoAdapter_GetChecklistResponse;->encodedSize(Lcom/squareup/protos/checklist/service/GetChecklistResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/checklist/service/GetChecklistResponse;)Lcom/squareup/protos/checklist/service/GetChecklistResponse;
    .locals 2

    .line 151
    invoke-virtual {p1}, Lcom/squareup/protos/checklist/service/GetChecklistResponse;->newBuilder()Lcom/squareup/protos/checklist/service/GetChecklistResponse$Builder;

    move-result-object p1

    .line 152
    iget-object v0, p1, Lcom/squareup/protos/checklist/service/GetChecklistResponse$Builder;->checklist:Lcom/squareup/protos/checklist/common/Checklist;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/checklist/common/Checklist;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/checklist/service/GetChecklistResponse$Builder;->checklist:Lcom/squareup/protos/checklist/common/Checklist;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/checklist/common/Checklist;

    iput-object v0, p1, Lcom/squareup/protos/checklist/service/GetChecklistResponse$Builder;->checklist:Lcom/squareup/protos/checklist/common/Checklist;

    .line 153
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/checklist/service/GetChecklistResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 154
    invoke-virtual {p1}, Lcom/squareup/protos/checklist/service/GetChecklistResponse$Builder;->build()Lcom/squareup/protos/checklist/service/GetChecklistResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 113
    check-cast p1, Lcom/squareup/protos/checklist/service/GetChecklistResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/checklist/service/GetChecklistResponse$ProtoAdapter_GetChecklistResponse;->redact(Lcom/squareup/protos/checklist/service/GetChecklistResponse;)Lcom/squareup/protos/checklist/service/GetChecklistResponse;

    move-result-object p1

    return-object p1
.end method
