.class public final enum Lcom/squareup/protos/checklist/common/NextStep;
.super Ljava/lang/Enum;
.source "NextStep.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/checklist/common/NextStep$ProtoAdapter_NextStep;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/checklist/common/NextStep;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/checklist/common/NextStep;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/checklist/common/NextStep;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BUY_AN_R12:Lcom/squareup/protos/checklist/common/NextStep;

.field public static final enum CORE:Lcom/squareup/protos/checklist/common/NextStep;

.field public static final enum INVOICING:Lcom/squareup/protos/checklist/common/NextStep;

.field public static final enum ONLINE:Lcom/squareup/protos/checklist/common/NextStep;

.field public static final enum RECOMMENDED_PRODUCTS:Lcom/squareup/protos/checklist/common/NextStep;

.field public static final enum REFER_A_FRIEND:Lcom/squareup/protos/checklist/common/NextStep;

.field public static final enum UNUSED:Lcom/squareup/protos/checklist/common/NextStep;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 20
    new-instance v0, Lcom/squareup/protos/checklist/common/NextStep;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "UNUSED"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/checklist/common/NextStep;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/NextStep;->UNUSED:Lcom/squareup/protos/checklist/common/NextStep;

    .line 22
    new-instance v0, Lcom/squareup/protos/checklist/common/NextStep;

    const/4 v3, 0x2

    const-string v4, "CORE"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/checklist/common/NextStep;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/NextStep;->CORE:Lcom/squareup/protos/checklist/common/NextStep;

    .line 24
    new-instance v0, Lcom/squareup/protos/checklist/common/NextStep;

    const/4 v4, 0x3

    const-string v5, "ONLINE"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/checklist/common/NextStep;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/NextStep;->ONLINE:Lcom/squareup/protos/checklist/common/NextStep;

    .line 26
    new-instance v0, Lcom/squareup/protos/checklist/common/NextStep;

    const/4 v5, 0x4

    const-string v6, "INVOICING"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/checklist/common/NextStep;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/NextStep;->INVOICING:Lcom/squareup/protos/checklist/common/NextStep;

    .line 31
    new-instance v0, Lcom/squareup/protos/checklist/common/NextStep;

    const/4 v6, 0x5

    const-string v7, "REFER_A_FRIEND"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/protos/checklist/common/NextStep;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/NextStep;->REFER_A_FRIEND:Lcom/squareup/protos/checklist/common/NextStep;

    .line 33
    new-instance v0, Lcom/squareup/protos/checklist/common/NextStep;

    const/4 v7, 0x6

    const-string v8, "RECOMMENDED_PRODUCTS"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/protos/checklist/common/NextStep;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/NextStep;->RECOMMENDED_PRODUCTS:Lcom/squareup/protos/checklist/common/NextStep;

    .line 35
    new-instance v0, Lcom/squareup/protos/checklist/common/NextStep;

    const/4 v8, 0x7

    const-string v9, "BUY_AN_R12"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/protos/checklist/common/NextStep;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/NextStep;->BUY_AN_R12:Lcom/squareup/protos/checklist/common/NextStep;

    new-array v0, v8, [Lcom/squareup/protos/checklist/common/NextStep;

    .line 19
    sget-object v8, Lcom/squareup/protos/checklist/common/NextStep;->UNUSED:Lcom/squareup/protos/checklist/common/NextStep;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/checklist/common/NextStep;->CORE:Lcom/squareup/protos/checklist/common/NextStep;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/NextStep;->ONLINE:Lcom/squareup/protos/checklist/common/NextStep;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/checklist/common/NextStep;->INVOICING:Lcom/squareup/protos/checklist/common/NextStep;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/checklist/common/NextStep;->REFER_A_FRIEND:Lcom/squareup/protos/checklist/common/NextStep;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/checklist/common/NextStep;->RECOMMENDED_PRODUCTS:Lcom/squareup/protos/checklist/common/NextStep;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/checklist/common/NextStep;->BUY_AN_R12:Lcom/squareup/protos/checklist/common/NextStep;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/checklist/common/NextStep;->$VALUES:[Lcom/squareup/protos/checklist/common/NextStep;

    .line 37
    new-instance v0, Lcom/squareup/protos/checklist/common/NextStep$ProtoAdapter_NextStep;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/common/NextStep$ProtoAdapter_NextStep;-><init>()V

    sput-object v0, Lcom/squareup/protos/checklist/common/NextStep;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 42
    iput p3, p0, Lcom/squareup/protos/checklist/common/NextStep;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/checklist/common/NextStep;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 56
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/checklist/common/NextStep;->BUY_AN_R12:Lcom/squareup/protos/checklist/common/NextStep;

    return-object p0

    .line 55
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/checklist/common/NextStep;->RECOMMENDED_PRODUCTS:Lcom/squareup/protos/checklist/common/NextStep;

    return-object p0

    .line 54
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/checklist/common/NextStep;->REFER_A_FRIEND:Lcom/squareup/protos/checklist/common/NextStep;

    return-object p0

    .line 53
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/checklist/common/NextStep;->INVOICING:Lcom/squareup/protos/checklist/common/NextStep;

    return-object p0

    .line 52
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/checklist/common/NextStep;->ONLINE:Lcom/squareup/protos/checklist/common/NextStep;

    return-object p0

    .line 51
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/checklist/common/NextStep;->CORE:Lcom/squareup/protos/checklist/common/NextStep;

    return-object p0

    .line 50
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/checklist/common/NextStep;->UNUSED:Lcom/squareup/protos/checklist/common/NextStep;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/checklist/common/NextStep;
    .locals 1

    .line 19
    const-class v0, Lcom/squareup/protos/checklist/common/NextStep;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/checklist/common/NextStep;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/checklist/common/NextStep;
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/protos/checklist/common/NextStep;->$VALUES:[Lcom/squareup/protos/checklist/common/NextStep;

    invoke-virtual {v0}, [Lcom/squareup/protos/checklist/common/NextStep;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/checklist/common/NextStep;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 63
    iget v0, p0, Lcom/squareup/protos/checklist/common/NextStep;->value:I

    return v0
.end method
