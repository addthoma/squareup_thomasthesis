.class public final enum Lcom/squareup/protos/riskevaluation/external/Level;
.super Ljava/lang/Enum;
.source "Level.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/riskevaluation/external/Level$ProtoAdapter_Level;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/riskevaluation/external/Level;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/riskevaluation/external/Level;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/riskevaluation/external/Level;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum HIGH:Lcom/squareup/protos/riskevaluation/external/Level;

.field public static final enum MODERATE:Lcom/squareup/protos/riskevaluation/external/Level;

.field public static final enum NORMAL:Lcom/squareup/protos/riskevaluation/external/Level;

.field public static final enum UNKNOWN_LEVEL:Lcom/squareup/protos/riskevaluation/external/Level;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 18
    new-instance v0, Lcom/squareup/protos/riskevaluation/external/Level;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_LEVEL"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/riskevaluation/external/Level;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/riskevaluation/external/Level;->UNKNOWN_LEVEL:Lcom/squareup/protos/riskevaluation/external/Level;

    .line 20
    new-instance v0, Lcom/squareup/protos/riskevaluation/external/Level;

    const/4 v2, 0x1

    const-string v3, "NORMAL"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/riskevaluation/external/Level;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/riskevaluation/external/Level;->NORMAL:Lcom/squareup/protos/riskevaluation/external/Level;

    .line 22
    new-instance v0, Lcom/squareup/protos/riskevaluation/external/Level;

    const/4 v3, 0x2

    const-string v4, "MODERATE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/riskevaluation/external/Level;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/riskevaluation/external/Level;->MODERATE:Lcom/squareup/protos/riskevaluation/external/Level;

    .line 24
    new-instance v0, Lcom/squareup/protos/riskevaluation/external/Level;

    const/4 v4, 0x3

    const-string v5, "HIGH"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/riskevaluation/external/Level;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/riskevaluation/external/Level;->HIGH:Lcom/squareup/protos/riskevaluation/external/Level;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/riskevaluation/external/Level;

    .line 17
    sget-object v5, Lcom/squareup/protos/riskevaluation/external/Level;->UNKNOWN_LEVEL:Lcom/squareup/protos/riskevaluation/external/Level;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/riskevaluation/external/Level;->NORMAL:Lcom/squareup/protos/riskevaluation/external/Level;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/riskevaluation/external/Level;->MODERATE:Lcom/squareup/protos/riskevaluation/external/Level;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/riskevaluation/external/Level;->HIGH:Lcom/squareup/protos/riskevaluation/external/Level;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/riskevaluation/external/Level;->$VALUES:[Lcom/squareup/protos/riskevaluation/external/Level;

    .line 26
    new-instance v0, Lcom/squareup/protos/riskevaluation/external/Level$ProtoAdapter_Level;

    invoke-direct {v0}, Lcom/squareup/protos/riskevaluation/external/Level$ProtoAdapter_Level;-><init>()V

    sput-object v0, Lcom/squareup/protos/riskevaluation/external/Level;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput p3, p0, Lcom/squareup/protos/riskevaluation/external/Level;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/riskevaluation/external/Level;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 42
    :cond_0
    sget-object p0, Lcom/squareup/protos/riskevaluation/external/Level;->HIGH:Lcom/squareup/protos/riskevaluation/external/Level;

    return-object p0

    .line 41
    :cond_1
    sget-object p0, Lcom/squareup/protos/riskevaluation/external/Level;->MODERATE:Lcom/squareup/protos/riskevaluation/external/Level;

    return-object p0

    .line 40
    :cond_2
    sget-object p0, Lcom/squareup/protos/riskevaluation/external/Level;->NORMAL:Lcom/squareup/protos/riskevaluation/external/Level;

    return-object p0

    .line 39
    :cond_3
    sget-object p0, Lcom/squareup/protos/riskevaluation/external/Level;->UNKNOWN_LEVEL:Lcom/squareup/protos/riskevaluation/external/Level;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/riskevaluation/external/Level;
    .locals 1

    .line 17
    const-class v0, Lcom/squareup/protos/riskevaluation/external/Level;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/riskevaluation/external/Level;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/riskevaluation/external/Level;
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/protos/riskevaluation/external/Level;->$VALUES:[Lcom/squareup/protos/riskevaluation/external/Level;

    invoke-virtual {v0}, [Lcom/squareup/protos/riskevaluation/external/Level;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/riskevaluation/external/Level;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 49
    iget v0, p0, Lcom/squareup/protos/riskevaluation/external/Level;->value:I

    return v0
.end method
