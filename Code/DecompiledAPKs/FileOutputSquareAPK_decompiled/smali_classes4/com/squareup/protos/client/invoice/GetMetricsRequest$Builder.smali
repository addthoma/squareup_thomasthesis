.class public final Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetMetricsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/GetMetricsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/GetMetricsRequest;",
        "Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact_token:Ljava/lang/String;

.field public metric_query:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 102
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 103
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;->metric_query:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/GetMetricsRequest;
    .locals 4

    .line 119
    new-instance v0, Lcom/squareup/protos/client/invoice/GetMetricsRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;->contact_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;->metric_query:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/invoice/GetMetricsRequest;-><init>(Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsRequest;

    move-result-object v0

    return-object v0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method public metric_query(Ljava/util/List;)Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;"
        }
    .end annotation

    .line 112
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetMetricsRequest$Builder;->metric_query:Ljava/util/List;

    return-object p0
.end method
