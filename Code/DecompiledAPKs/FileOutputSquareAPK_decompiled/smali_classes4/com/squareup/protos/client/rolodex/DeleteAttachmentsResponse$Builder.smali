.class public final Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeleteAttachmentsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;",
        "Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attachment_responses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttachmentResponse;",
            ">;"
        }
    .end annotation
.end field

.field public failed_attachments:Ljava/lang/Integer;

.field public status:Lcom/squareup/protos/client/Status;

.field public succeeded_attachments:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 126
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 127
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;->attachment_responses:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public attachment_responses(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttachmentResponse;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;"
        }
    .end annotation

    .line 141
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;->attachment_responses:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;
    .locals 7

    .line 153
    new-instance v6, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;->succeeded_attachments:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;->failed_attachments:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;->attachment_responses:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Lcom/squareup/protos/client/Status;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;

    move-result-object v0

    return-object v0
.end method

.method public failed_attachments(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;->failed_attachments:Ljava/lang/Integer;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public succeeded_attachments(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;->succeeded_attachments:Ljava/lang/Integer;

    return-object p0
.end method
