.class public final Lcom/squareup/protos/client/rolodex/Contact;
.super Lcom/squareup/wire/Message;
.source "Contact.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/Contact$ProtoAdapter_Contact;,
        Lcom/squareup/protos/client/rolodex/Contact$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "Lcom/squareup/protos/client/rolodex/Contact$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CONTACT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_CREATED_AT_MS:Ljava/lang/Long;

.field public static final DEFAULT_CREATION_SOURCE:Ljava/lang/String; = ""

.field public static final DEFAULT_DISPLAY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_EMAIL_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_MERCHANT_PROVIDED_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_PHONE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_SCHEMA_VERSION:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final app_fields:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.AppField#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x10
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AppField;",
            ">;"
        }
    .end annotation
.end field

.field public final application_fields:Lcom/squareup/protos/client/rolodex/ApplicationFields;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.ApplicationFields#ADAPTER"
        tag = 0x13
    .end annotation
.end field

.field public final attachments:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Attachment#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xe
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Attachment;",
            ">;"
        }
    .end annotation
.end field

.field public final attributes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.AttributeSchema$Attribute#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
            ">;"
        }
    .end annotation
.end field

.field public final buyer_summary:Lcom/squareup/protos/client/rolodex/BuyerSummary;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.BuyerSummary#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final contact_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final created_at_ms:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x9
    .end annotation
.end field

.field public final creation_source:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xf
    .end annotation
.end field

.field public final display_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final email_subscriptions:Lcom/squareup/protos/client/rolodex/EmailSubscriptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.EmailSubscriptions#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final email_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final frequent_items:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.FrequentItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x11
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/FrequentItem;",
            ">;"
        }
    .end annotation
.end field

.field public final group:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Group#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation
.end field

.field public final instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.InstrumentsOnFile#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final merchant_provided_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x12
    .end annotation
.end field

.field public final note:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Note#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        redacted = true
        tag = 0xa
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Note;",
            ">;"
        }
    .end annotation
.end field

.field public final phone_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.CustomerProfile#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final schema_version:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 23
    new-instance v0, Lcom/squareup/protos/client/rolodex/Contact$ProtoAdapter_Contact;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Contact$ProtoAdapter_Contact;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 35
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/rolodex/Contact;->DEFAULT_CREATED_AT_MS:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/rolodex/Contact$Builder;Lokio/ByteString;)V
    .locals 1

    .line 231
    sget-object v0, Lcom/squareup/protos/client/rolodex/Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 232
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->contact_token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    .line 233
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->display_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    .line 234
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    .line 235
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->group:Ljava/util/List;

    const-string v0, "group"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->group:Ljava/util/List;

    .line 236
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->buyer_summary:Lcom/squareup/protos/client/rolodex/BuyerSummary;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->buyer_summary:Lcom/squareup/protos/client/rolodex/BuyerSummary;

    .line 237
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    .line 238
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->email_token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->email_token:Ljava/lang/String;

    .line 239
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->phone_token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->phone_token:Ljava/lang/String;

    .line 240
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->created_at_ms:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->created_at_ms:Ljava/lang/Long;

    .line 241
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->note:Ljava/util/List;

    const-string v0, "note"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->note:Ljava/util/List;

    .line 242
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->attributes:Ljava/util/List;

    const-string v0, "attributes"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    .line 243
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->schema_version:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->schema_version:Ljava/lang/String;

    .line 244
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->email_subscriptions:Lcom/squareup/protos/client/rolodex/EmailSubscriptions;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->email_subscriptions:Lcom/squareup/protos/client/rolodex/EmailSubscriptions;

    .line 245
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->attachments:Ljava/util/List;

    const-string v0, "attachments"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->attachments:Ljava/util/List;

    .line 246
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->creation_source:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->creation_source:Ljava/lang/String;

    .line 247
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->app_fields:Ljava/util/List;

    const-string v0, "app_fields"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->app_fields:Ljava/util/List;

    .line 248
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->frequent_items:Ljava/util/List;

    const-string v0, "frequent_items"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->frequent_items:Ljava/util/List;

    .line 249
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->merchant_provided_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/Contact;->merchant_provided_id:Ljava/lang/String;

    .line 250
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact$Builder;->application_fields:Lcom/squareup/protos/client/rolodex/ApplicationFields;

    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact;->application_fields:Lcom/squareup/protos/client/rolodex/ApplicationFields;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 282
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/Contact;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 283
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/Contact;

    .line 284
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Contact;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    .line 285
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    .line 286
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    .line 287
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->group:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->group:Ljava/util/List;

    .line 288
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->buyer_summary:Lcom/squareup/protos/client/rolodex/BuyerSummary;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->buyer_summary:Lcom/squareup/protos/client/rolodex/BuyerSummary;

    .line 289
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    .line 290
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->email_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->email_token:Ljava/lang/String;

    .line 291
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->phone_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->phone_token:Ljava/lang/String;

    .line 292
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->created_at_ms:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->created_at_ms:Ljava/lang/Long;

    .line 293
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->note:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->note:Ljava/util/List;

    .line 294
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    .line 295
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->schema_version:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->schema_version:Ljava/lang/String;

    .line 296
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->email_subscriptions:Lcom/squareup/protos/client/rolodex/EmailSubscriptions;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->email_subscriptions:Lcom/squareup/protos/client/rolodex/EmailSubscriptions;

    .line 297
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->attachments:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->attachments:Ljava/util/List;

    .line 298
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->creation_source:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->creation_source:Ljava/lang/String;

    .line 299
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->app_fields:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->app_fields:Ljava/util/List;

    .line 300
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->frequent_items:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->frequent_items:Ljava/util/List;

    .line 301
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->merchant_provided_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/Contact;->merchant_provided_id:Ljava/lang/String;

    .line 302
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->application_fields:Lcom/squareup/protos/client/rolodex/ApplicationFields;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->application_fields:Lcom/squareup/protos/client/rolodex/ApplicationFields;

    .line 303
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 308
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_d

    .line 310
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 311
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 312
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 313
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/CustomerProfile;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 314
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->group:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 315
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->buyer_summary:Lcom/squareup/protos/client/rolodex/BuyerSummary;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/BuyerSummary;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 316
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 317
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->email_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 318
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->phone_token:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 319
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->created_at_ms:Ljava/lang/Long;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 320
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->note:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 321
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 322
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->schema_version:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 323
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->email_subscriptions:Lcom/squareup/protos/client/rolodex/EmailSubscriptions;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/EmailSubscriptions;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 324
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->attachments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 325
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->creation_source:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 326
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->app_fields:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 327
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->frequent_items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 328
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->merchant_provided_id:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 329
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->application_fields:Lcom/squareup/protos/client/rolodex/ApplicationFields;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/ApplicationFields;->hashCode()I

    move-result v2

    :cond_c
    add-int/2addr v0, v2

    .line 330
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_d
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 2

    .line 255
    new-instance v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;-><init>()V

    .line 256
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->contact_token:Ljava/lang/String;

    .line 257
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->display_name:Ljava/lang/String;

    .line 258
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    .line 259
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->group:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->group:Ljava/util/List;

    .line 260
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->buyer_summary:Lcom/squareup/protos/client/rolodex/BuyerSummary;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->buyer_summary:Lcom/squareup/protos/client/rolodex/BuyerSummary;

    .line 261
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    .line 262
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->email_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->email_token:Ljava/lang/String;

    .line 263
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->phone_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->phone_token:Ljava/lang/String;

    .line 264
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->created_at_ms:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->created_at_ms:Ljava/lang/Long;

    .line 265
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->note:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->note:Ljava/util/List;

    .line 266
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->attributes:Ljava/util/List;

    .line 267
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->schema_version:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->schema_version:Ljava/lang/String;

    .line 268
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->email_subscriptions:Lcom/squareup/protos/client/rolodex/EmailSubscriptions;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->email_subscriptions:Lcom/squareup/protos/client/rolodex/EmailSubscriptions;

    .line 269
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->attachments:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->attachments:Ljava/util/List;

    .line 270
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->creation_source:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->creation_source:Ljava/lang/String;

    .line 271
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->app_fields:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->app_fields:Ljava/util/List;

    .line 272
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->frequent_items:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->frequent_items:Ljava/util/List;

    .line 273
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->merchant_provided_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->merchant_provided_id:Ljava/lang/String;

    .line 274
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->application_fields:Lcom/squareup/protos/client/rolodex/ApplicationFields;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->application_fields:Lcom/squareup/protos/client/rolodex/ApplicationFields;

    .line 275
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact;->newBuilder()Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 337
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 338
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", contact_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", display_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz v1, :cond_2

    const-string v1, ", profile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 341
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->group:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", group="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->group:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 342
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->buyer_summary:Lcom/squareup/protos/client/rolodex/BuyerSummary;

    if-eqz v1, :cond_4

    const-string v1, ", buyer_summary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->buyer_summary:Lcom/squareup/protos/client/rolodex/BuyerSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 343
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    if-eqz v1, :cond_5

    const-string v1, ", instruments_on_file="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 344
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->email_token:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", email_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->email_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->phone_token:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", phone_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->phone_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->created_at_ms:Ljava/lang/Long;

    if-eqz v1, :cond_8

    const-string v1, ", created_at_ms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->created_at_ms:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 347
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->note:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, ", note=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 348
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, ", attributes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->attributes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 349
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->schema_version:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", schema_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->schema_version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 350
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->email_subscriptions:Lcom/squareup/protos/client/rolodex/EmailSubscriptions;

    if-eqz v1, :cond_c

    const-string v1, ", email_subscriptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->email_subscriptions:Lcom/squareup/protos/client/rolodex/EmailSubscriptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 351
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->attachments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, ", attachments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->attachments:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 352
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->creation_source:Ljava/lang/String;

    if-eqz v1, :cond_e

    const-string v1, ", creation_source="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->creation_source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 353
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->app_fields:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, ", app_fields="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->app_fields:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 354
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->frequent_items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_10

    const-string v1, ", frequent_items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->frequent_items:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 355
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->merchant_provided_id:Ljava/lang/String;

    if-eqz v1, :cond_11

    const-string v1, ", merchant_provided_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->merchant_provided_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->application_fields:Lcom/squareup/protos/client/rolodex/ApplicationFields;

    if-eqz v1, :cond_12

    const-string v1, ", application_fields="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Contact;->application_fields:Lcom/squareup/protos/client/rolodex/ApplicationFields;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_12
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Contact{"

    .line 357
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
