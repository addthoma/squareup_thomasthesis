.class final Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours$ProtoAdapter_ViewTutorialsAndTours;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ClientAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ViewTutorialsAndTours"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 3601
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3616
    new-instance v0, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours$Builder;-><init>()V

    .line 3617
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 3618
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 3621
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 3625
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 3626
    invoke-virtual {v0}, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3599
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours$ProtoAdapter_ViewTutorialsAndTours;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3611
    invoke-virtual {p2}, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3599
    check-cast p2, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours$ProtoAdapter_ViewTutorialsAndTours;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;)I
    .locals 0

    .line 3606
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 3599
    check-cast p1, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours$ProtoAdapter_ViewTutorialsAndTours;->encodedSize(Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;)Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;
    .locals 0

    .line 3631
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;->newBuilder()Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours$Builder;

    move-result-object p1

    .line 3632
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 3633
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 3599
    check-cast p1, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours$ProtoAdapter_ViewTutorialsAndTours;->redact(Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;)Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    move-result-object p1

    return-object p1
.end method
