.class public final Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LoyaltyAccountMapping.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
        "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public loyalty_account_mapping_token:Ljava/lang/String;

.field public raw_id:Ljava/lang/String;

.field public type:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 148
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;
    .locals 7

    .line 185
    new-instance v6, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->loyalty_account_mapping_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->type:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->raw_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 139
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public loyalty_account_mapping_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->loyalty_account_mapping_token:Ljava/lang/String;

    return-object p0
.end method

.method public raw_id(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->raw_id:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;)Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->type:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    return-object p0
.end method
