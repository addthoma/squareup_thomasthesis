.class final Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$ProtoAdapter_ReturnItemization;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReturnItemization"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 8283
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8302
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;-><init>()V

    .line 8303
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 8304
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 8309
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 8307
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/Itemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Itemization;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->itemization(Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;

    goto :goto_0

    .line 8306
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->source_itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;

    goto :goto_0

    .line 8313
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 8314
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->build()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8281
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$ProtoAdapter_ReturnItemization;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8295
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8296
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8297
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8281
    check-cast p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$ProtoAdapter_ReturnItemization;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;)I
    .locals 4

    .line 8288
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    const/4 v3, 0x2

    .line 8289
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8290
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 8281
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$ProtoAdapter_ReturnItemization;->encodedSize(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;
    .locals 2

    .line 8319
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->newBuilder()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;

    move-result-object p1

    .line 8320
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 8321
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/Itemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Itemization;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    .line 8322
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 8323
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->build()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 8281
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$ProtoAdapter_ReturnItemization;->redact(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    move-result-object p1

    return-object p1
.end method
