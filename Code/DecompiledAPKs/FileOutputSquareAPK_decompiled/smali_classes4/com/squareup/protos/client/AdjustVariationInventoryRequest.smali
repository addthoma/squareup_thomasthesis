.class public final Lcom/squareup/protos/client/AdjustVariationInventoryRequest;
.super Lcom/squareup/wire/Message;
.source "AdjustVariationInventoryRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/AdjustVariationInventoryRequest$ProtoAdapter_AdjustVariationInventoryRequest;,
        Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/AdjustVariationInventoryRequest;",
        "Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/AdjustVariationInventoryRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_IDEMPOTENCY_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final adjustment:Lcom/squareup/protos/client/InventoryAdjustment;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.InventoryAdjustment#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final count:Lcom/squareup/protos/client/InventoryCount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.InventoryCount#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final idempotency_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$ProtoAdapter_AdjustVariationInventoryRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$ProtoAdapter_AdjustVariationInventoryRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/InventoryCount;Lcom/squareup/protos/client/InventoryAdjustment;)V
    .locals 1

    .line 49
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/InventoryCount;Lcom/squareup/protos/client/InventoryAdjustment;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/InventoryCount;Lcom/squareup/protos/client/InventoryAdjustment;Lokio/ByteString;)V
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 55
    invoke-static {p2, p3}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p4

    const/4 v0, 0x1

    if-gt p4, v0, :cond_0

    .line 58
    iput-object p1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->idempotency_token:Ljava/lang/String;

    .line 59
    iput-object p2, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->count:Lcom/squareup/protos/client/InventoryCount;

    .line 60
    iput-object p3, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    return-void

    .line 56
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of count, adjustment may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 76
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 77
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->idempotency_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->idempotency_token:Ljava/lang/String;

    .line 79
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->count:Lcom/squareup/protos/client/InventoryCount;

    iget-object v3, p1, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->count:Lcom/squareup/protos/client/InventoryCount;

    .line 80
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    iget-object p1, p1, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    .line 81
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 86
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->idempotency_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->count:Lcom/squareup/protos/client/InventoryCount;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/InventoryCount;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/InventoryAdjustment;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 92
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;
    .locals 2

    .line 65
    new-instance v0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;-><init>()V

    .line 66
    iget-object v1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->idempotency_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->idempotency_token:Ljava/lang/String;

    .line 67
    iget-object v1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->count:Lcom/squareup/protos/client/InventoryCount;

    iput-object v1, v0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->count:Lcom/squareup/protos/client/InventoryCount;

    .line 68
    iget-object v1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    iput-object v1, v0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    .line 69
    invoke-virtual {p0}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->newBuilder()Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->idempotency_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", idempotency_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->idempotency_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->count:Lcom/squareup/protos/client/InventoryCount;

    if-eqz v1, :cond_1

    const-string v1, ", count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->count:Lcom/squareup/protos/client/InventoryCount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 102
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    if-eqz v1, :cond_2

    const-string v1, ", adjustment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AdjustVariationInventoryRequest{"

    .line 103
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
