.class final Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ProtoAdapter_LookupFailureReason;
.super Lcom/squareup/wire/ProtoAdapter;
.source "LookupCouponsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_LookupFailureReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 314
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 335
    new-instance v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;-><init>()V

    .line 336
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 337
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 350
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 348
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->redeemed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;

    goto :goto_0

    .line 347
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->expired_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;

    goto :goto_0

    .line 341
    :cond_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->type(Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;)Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 343
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 354
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 355
    invoke-virtual {v0}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->build()Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 312
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ProtoAdapter_LookupFailureReason;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 327
    sget-object v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->type:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 328
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->expired_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 329
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 330
    invoke-virtual {p2}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 312
    check-cast p2, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ProtoAdapter_LookupFailureReason;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;)I
    .locals 4

    .line 319
    sget-object v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->type:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->expired_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x2

    .line 320
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x3

    .line 321
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 322
    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 312
    check-cast p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ProtoAdapter_LookupFailureReason;->encodedSize(Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;)Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;
    .locals 2

    .line 360
    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->newBuilder()Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;

    move-result-object p1

    .line 361
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->expired_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->expired_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->expired_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 362
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 363
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 364
    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->build()Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 312
    check-cast p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ProtoAdapter_LookupFailureReason;->redact(Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;)Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    move-result-object p1

    return-object p1
.end method
