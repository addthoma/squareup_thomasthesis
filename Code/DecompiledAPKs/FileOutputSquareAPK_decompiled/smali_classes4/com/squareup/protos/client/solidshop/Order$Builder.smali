.class public final Lcom/squareup/protos/client/solidshop/Order$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/solidshop/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/solidshop/Order;",
        "Lcom/squareup/protos/client/solidshop/Order$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public carton:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/solidshop/Carton;",
            ">;"
        }
    .end annotation
.end field

.field public item_count:Ljava/lang/Integer;

.field public number:Ljava/lang/String;

.field public placed_at:Lcom/squareup/protos/common/time/DateTime;

.field public total:Lcom/squareup/protos/common/Money;

.field public user_facing_status:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 174
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 175
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/solidshop/Order$Builder;->carton:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/solidshop/Order;
    .locals 9

    .line 228
    new-instance v8, Lcom/squareup/protos/client/solidshop/Order;

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order$Builder;->number:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/solidshop/Order$Builder;->placed_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p0, Lcom/squareup/protos/client/solidshop/Order$Builder;->total:Lcom/squareup/protos/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/client/solidshop/Order$Builder;->item_count:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/client/solidshop/Order$Builder;->user_facing_status:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    iget-object v6, p0, Lcom/squareup/protos/client/solidshop/Order$Builder;->carton:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/solidshop/Order;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/Money;Ljava/lang/Integer;Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;Ljava/util/List;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 161
    invoke-virtual {p0}, Lcom/squareup/protos/client/solidshop/Order$Builder;->build()Lcom/squareup/protos/client/solidshop/Order;

    move-result-object v0

    return-object v0
.end method

.method public carton(Ljava/util/List;)Lcom/squareup/protos/client/solidshop/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/solidshop/Carton;",
            ">;)",
            "Lcom/squareup/protos/client/solidshop/Order$Builder;"
        }
    .end annotation

    .line 221
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 222
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/Order$Builder;->carton:Ljava/util/List;

    return-object p0
.end method

.method public item_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/solidshop/Order$Builder;
    .locals 0

    .line 208
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/Order$Builder;->item_count:Ljava/lang/Integer;

    return-object p0
.end method

.method public number(Ljava/lang/String;)Lcom/squareup/protos/client/solidshop/Order$Builder;
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/Order$Builder;->number:Ljava/lang/String;

    return-object p0
.end method

.method public placed_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/solidshop/Order$Builder;
    .locals 0

    .line 192
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/Order$Builder;->placed_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public total(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/solidshop/Order$Builder;
    .locals 0

    .line 200
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/Order$Builder;->total:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public user_facing_status(Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;)Lcom/squareup/protos/client/solidshop/Order$Builder;
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/Order$Builder;->user_facing_status:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-object p0
.end method
