.class public final Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;",
        "Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public item:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2445
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;
    .locals 3

    .line 2455
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Builder;->item:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;-><init>(Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2442
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    move-result-object v0

    return-object v0
.end method

.method public item(Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;)Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Builder;
    .locals 0

    .line 2449
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Builder;->item:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    return-object p0
.end method
