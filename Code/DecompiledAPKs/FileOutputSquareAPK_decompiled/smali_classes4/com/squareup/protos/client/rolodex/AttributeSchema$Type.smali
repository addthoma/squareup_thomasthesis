.class public final enum Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;
.super Ljava/lang/Enum;
.source "AttributeSchema.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/AttributeSchema;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/AttributeSchema$Type$ProtoAdapter_Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ADDRESS:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

.field public static final enum BOOLEAN:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

.field public static final enum DATE:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

.field public static final enum EMAIL:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

.field public static final enum ENUM:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

.field public static final enum NUMBER:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

.field public static final enum PHONE:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

.field public static final enum TEXT:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 155
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->UNKNOWN:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    .line 157
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    const/4 v2, 0x1

    const-string v3, "NUMBER"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->NUMBER:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    .line 159
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    const/4 v3, 0x2

    const-string v4, "BOOLEAN"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->BOOLEAN:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    .line 161
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    const/4 v4, 0x3

    const-string v5, "TEXT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->TEXT:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    .line 163
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    const/4 v5, 0x4

    const-string v6, "ENUM"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->ENUM:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    .line 165
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    const/4 v6, 0x5

    const-string v7, "PHONE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->PHONE:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    .line 167
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    const/4 v7, 0x6

    const-string v8, "EMAIL"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->EMAIL:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    .line 169
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    const/4 v8, 0x7

    const-string v9, "ADDRESS"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->ADDRESS:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    .line 171
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    const/16 v9, 0x8

    const-string v10, "DATE"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->DATE:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    .line 154
    sget-object v10, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->UNKNOWN:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->NUMBER:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->BOOLEAN:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->TEXT:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->ENUM:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->PHONE:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->EMAIL:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->ADDRESS:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->DATE:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->$VALUES:[Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    .line 173
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type$ProtoAdapter_Type;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type$ProtoAdapter_Type;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 177
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 178
    iput p3, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 194
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->DATE:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    return-object p0

    .line 193
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->ADDRESS:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    return-object p0

    .line 192
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->EMAIL:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    return-object p0

    .line 191
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->PHONE:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    return-object p0

    .line 190
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->ENUM:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    return-object p0

    .line 189
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->TEXT:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    return-object p0

    .line 188
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->BOOLEAN:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    return-object p0

    .line 187
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->NUMBER:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    return-object p0

    .line 186
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->UNKNOWN:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;
    .locals 1

    .line 154
    const-class v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;
    .locals 1

    .line 154
    sget-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->$VALUES:[Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 201
    iget v0, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->value:I

    return v0
.end method
