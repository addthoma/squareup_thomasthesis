.class final Lcom/squareup/protos/client/bfd/cart/DisplayCart$ProtoAdapter_DisplayCart;
.super Lcom/squareup/wire/ProtoAdapter;
.source "DisplayCart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bfd/cart/DisplayCart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_DisplayCart"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bfd/cart/DisplayCart;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 179
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bfd/cart/DisplayCart;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bfd/cart/DisplayCart;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 204
    new-instance v0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;-><init>()V

    .line 205
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 206
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 214
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 212
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->is_card_still_inserted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;

    goto :goto_0

    .line 211
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->offline_mode(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;

    goto :goto_0

    .line 210
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->total_amount(Ljava/lang/Long;)Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;

    goto :goto_0

    .line 209
    :cond_3
    sget-object v3, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->banner(Lcom/squareup/protos/client/bfd/cart/DisplayBanner;)Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;

    goto :goto_0

    .line 208
    :cond_4
    iget-object v3, v0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->items:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 218
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 219
    invoke-virtual {v0}, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->build()Lcom/squareup/protos/client/bfd/cart/DisplayCart;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 177
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bfd/cart/DisplayCart$ProtoAdapter_DisplayCart;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bfd/cart/DisplayCart;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bfd/cart/DisplayCart;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 194
    sget-object v0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->items:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 195
    sget-object v0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->banner:Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 196
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->total_amount:Ljava/lang/Long;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 197
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->offline_mode:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 198
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->is_card_still_inserted:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 199
    invoke-virtual {p2}, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 177
    check-cast p2, Lcom/squareup/protos/client/bfd/cart/DisplayCart;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bfd/cart/DisplayCart$ProtoAdapter_DisplayCart;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bfd/cart/DisplayCart;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bfd/cart/DisplayCart;)I
    .locals 4

    .line 184
    sget-object v0, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->items:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->banner:Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    const/4 v3, 0x2

    .line 185
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->total_amount:Ljava/lang/Long;

    const/4 v3, 0x3

    .line 186
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->offline_mode:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 187
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->is_card_still_inserted:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 188
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 189
    invoke-virtual {p1}, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 177
    check-cast p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bfd/cart/DisplayCart$ProtoAdapter_DisplayCart;->encodedSize(Lcom/squareup/protos/client/bfd/cart/DisplayCart;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bfd/cart/DisplayCart;)Lcom/squareup/protos/client/bfd/cart/DisplayCart;
    .locals 2

    .line 224
    invoke-virtual {p1}, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->newBuilder()Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;

    move-result-object p1

    .line 225
    iget-object v0, p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->items:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bfd/cart/DisplayItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 226
    iget-object v0, p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->banner:Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->banner:Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    iput-object v0, p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->banner:Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    .line 227
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 228
    invoke-virtual {p1}, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->build()Lcom/squareup/protos/client/bfd/cart/DisplayCart;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 177
    check-cast p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bfd/cart/DisplayCart$ProtoAdapter_DisplayCart;->redact(Lcom/squareup/protos/client/bfd/cart/DisplayCart;)Lcom/squareup/protos/client/bfd/cart/DisplayCart;

    move-result-object p1

    return-object p1
.end method
