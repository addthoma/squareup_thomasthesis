.class public final Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListConversationsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/dialogue/ListConversationsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/dialogue/ListConversationsResponse;",
        "Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/dialogue/ConversationListItem;",
            ">;"
        }
    .end annotation
.end field

.field public list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

.field public paging_key:Ljava/lang/String;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 131
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 132
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;->items:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/dialogue/ListConversationsResponse;
    .locals 7

    .line 162
    new-instance v6, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;->items:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    iget-object v4, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;->paging_key:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/dialogue/ListOptions;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 122
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;->build()Lcom/squareup/protos/client/dialogue/ListConversationsResponse;

    move-result-object v0

    return-object v0
.end method

.method public items(Ljava/util/List;)Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/dialogue/ConversationListItem;",
            ">;)",
            "Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;"
        }
    .end annotation

    .line 141
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;->items:Ljava/util/List;

    return-object p0
.end method

.method public list_options(Lcom/squareup/protos/client/dialogue/ListOptions;)Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    return-object p0
.end method

.method public paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;->paging_key:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
