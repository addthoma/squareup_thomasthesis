.class public final Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeleteAttachmentsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest;",
        "Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attachment_tokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public merchant_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 100
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 101
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest$Builder;->attachment_tokens:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public attachment_tokens(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest$Builder;"
        }
    .end annotation

    .line 114
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest$Builder;->attachment_tokens:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest;
    .locals 4

    .line 121
    new-instance v0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest$Builder;->attachment_tokens:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest;-><init>(Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest;

    move-result-object v0

    return-object v0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 109
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method
