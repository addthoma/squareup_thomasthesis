.class public final Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "VerifyCardActivationTokenResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;",
        "Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public result:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 81
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;
    .locals 3

    .line 91
    new-instance v0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Builder;->result:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;-><init>(Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;

    move-result-object v0

    return-object v0
.end method

.method public result(Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;)Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Builder;
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Builder;->result:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    return-object p0
.end method
