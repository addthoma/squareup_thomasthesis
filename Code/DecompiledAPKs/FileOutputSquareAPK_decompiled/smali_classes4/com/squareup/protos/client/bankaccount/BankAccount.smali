.class public final Lcom/squareup/protos/client/bankaccount/BankAccount;
.super Lcom/squareup/wire/Message;
.source "BankAccount.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bankaccount/BankAccount$ProtoAdapter_BankAccount;,
        Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bankaccount/BankAccount;",
        "Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bankaccount/BankAccount;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACCOUNT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_ACCOUNT_NUMBER_SUFFIX:Ljava/lang/String; = ""

.field public static final DEFAULT_ACCOUNT_TYPE:Lcom/squareup/protos/client/bankaccount/BankAccountType;

.field public static final DEFAULT_BANK_ACCOUNT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_BANK_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PRIMARY_INSTITUTION_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_ROUTING_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_SECONDARY_INSTITUTION_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_SUPPORTS_INSTANT_DEPOSIT:Ljava/lang/Boolean;

.field public static final DEFAULT_VERIFICATION_STATE:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

.field private static final serialVersionUID:J


# instance fields
.field public final account_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final account_number_suffix:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bankaccount.BankAccountType#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final bank_account_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final bank_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final primary_institution_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final routing_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final secondary_institution_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final supports_instant_deposit:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bankaccount.BankAccountVerificationState#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final verified_by:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x7
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccount$ProtoAdapter_BankAccount;

    invoke-direct {v0}, Lcom/squareup/protos/client/bankaccount/BankAccount$ProtoAdapter_BankAccount;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->BANK_ACCOUNT_VERIFICATION_STATE_DO_NOT_USE:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->DEFAULT_VERIFICATION_STATE:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    .line 34
    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->BANK_ACCOUNT_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->DEFAULT_ACCOUNT_TYPE:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    const/4 v0, 0x0

    .line 46
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->DEFAULT_SUPPORTS_INSTANT_DEPOSIT:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;Ljava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 13

    .line 157
    sget-object v12, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/client/bankaccount/BankAccount;-><init>(Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;Ljava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;Ljava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 165
    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p12}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    .line 167
    iput-object p2, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_name:Ljava/lang/String;

    .line 168
    iput-object p3, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    .line 169
    iput-object p4, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_name:Ljava/lang/String;

    .line 170
    iput-object p5, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_number_suffix:Ljava/lang/String;

    .line 171
    iput-object p6, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->routing_number:Ljava/lang/String;

    .line 172
    iput-object p7, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->verified_by:Lcom/squareup/protos/common/time/DateTime;

    .line 173
    iput-object p8, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->primary_institution_number:Ljava/lang/String;

    .line 174
    iput-object p9, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->secondary_institution_number:Ljava/lang/String;

    .line 175
    iput-object p10, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->supports_instant_deposit:Ljava/lang/Boolean;

    .line 176
    iput-object p11, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_account_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 200
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 201
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bankaccount/BankAccount;

    .line 202
    invoke-virtual {p0}, Lcom/squareup/protos/client/bankaccount/BankAccount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/BankAccount;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    iget-object v3, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    .line 203
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_name:Ljava/lang/String;

    .line 204
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    iget-object v3, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    .line 205
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_name:Ljava/lang/String;

    .line 206
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_number_suffix:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_number_suffix:Ljava/lang/String;

    .line 207
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->routing_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->routing_number:Ljava/lang/String;

    .line 208
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->verified_by:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->verified_by:Lcom/squareup/protos/common/time/DateTime;

    .line 209
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->primary_institution_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->primary_institution_number:Ljava/lang/String;

    .line 210
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->secondary_institution_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->secondary_institution_number:Ljava/lang/String;

    .line 211
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->supports_instant_deposit:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->supports_instant_deposit:Ljava/lang/Boolean;

    .line 212
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_account_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_account_token:Ljava/lang/String;

    .line 213
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 218
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_b

    .line 220
    invoke-virtual {p0}, Lcom/squareup/protos/client/bankaccount/BankAccount;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 222
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 223
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bankaccount/BankAccountType;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 224
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_name:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 225
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_number_suffix:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 226
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->routing_number:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 227
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->verified_by:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 228
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->primary_institution_number:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 229
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->secondary_institution_number:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 230
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->supports_instant_deposit:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 231
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_account_token:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_a
    add-int/2addr v0, v2

    .line 232
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_b
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;
    .locals 2

    .line 181
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;-><init>()V

    .line 182
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    iput-object v1, v0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    .line 183
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->bank_name:Ljava/lang/String;

    .line 184
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    iput-object v1, v0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    .line 185
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->account_name:Ljava/lang/String;

    .line 186
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_number_suffix:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->account_number_suffix:Ljava/lang/String;

    .line 187
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->routing_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->routing_number:Ljava/lang/String;

    .line 188
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->verified_by:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->verified_by:Lcom/squareup/protos/common/time/DateTime;

    .line 189
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->primary_institution_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->primary_institution_number:Ljava/lang/String;

    .line 190
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->secondary_institution_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->secondary_institution_number:Ljava/lang/String;

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->supports_instant_deposit:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->supports_instant_deposit:Ljava/lang/Boolean;

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_account_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->bank_account_token:Ljava/lang/String;

    .line 193
    invoke-virtual {p0}, Lcom/squareup/protos/client/bankaccount/BankAccount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/bankaccount/BankAccount;->newBuilder()Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 240
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    if-eqz v1, :cond_0

    const-string v1, ", verification_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 241
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", bank_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    if-eqz v1, :cond_2

    const-string v1, ", account_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 243
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_name:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", account_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_number_suffix:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", account_number_suffix="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_number_suffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->routing_number:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", routing_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->routing_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->verified_by:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_6

    const-string v1, ", verified_by="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->verified_by:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 247
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->primary_institution_number:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", primary_institution_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->primary_institution_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->secondary_institution_number:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", secondary_institution_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->secondary_institution_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->supports_instant_deposit:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", supports_instant_deposit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->supports_instant_deposit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 250
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_account_token:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", bank_account_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_account_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BankAccount{"

    .line 251
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
