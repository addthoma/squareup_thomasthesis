.class public final Lcom/squareup/protos/client/Buyer$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Buyer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/Buyer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/Buyer;",
        "Lcom/squareup/protos/client/Buyer$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public buyer_id:Ljava/lang/String;

.field public full_name:Ljava/lang/String;

.field public photo_url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 114
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/Buyer;
    .locals 5

    .line 134
    new-instance v0, Lcom/squareup/protos/client/Buyer;

    iget-object v1, p0, Lcom/squareup/protos/client/Buyer$Builder;->buyer_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/Buyer$Builder;->full_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/Buyer$Builder;->photo_url:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/Buyer;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/client/Buyer$Builder;->build()Lcom/squareup/protos/client/Buyer;

    move-result-object v0

    return-object v0
.end method

.method public buyer_id(Ljava/lang/String;)Lcom/squareup/protos/client/Buyer$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/client/Buyer$Builder;->buyer_id:Ljava/lang/String;

    return-object p0
.end method

.method public full_name(Ljava/lang/String;)Lcom/squareup/protos/client/Buyer$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/Buyer$Builder;->full_name:Ljava/lang/String;

    return-object p0
.end method

.method public photo_url(Ljava/lang/String;)Lcom/squareup/protos/client/Buyer$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/client/Buyer$Builder;->photo_url:Ljava/lang/String;

    return-object p0
.end method
