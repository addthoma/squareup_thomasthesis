.class public final Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;
.super Lcom/squareup/wire/Message;
.source "ModifierOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ModifierOptionLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BackingDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$ProtoAdapter_BackingDetails;,
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemModifierOption#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final modifier_list:Lcom/squareup/api/items/ItemModifierList;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemModifierList#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 584
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$ProtoAdapter_BackingDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$ProtoAdapter_BackingDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/ItemModifierOption;Lcom/squareup/api/items/ItemModifierList;)V
    .locals 1

    .line 608
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;-><init>(Lcom/squareup/api/items/ItemModifierOption;Lcom/squareup/api/items/ItemModifierList;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/ItemModifierOption;Lcom/squareup/api/items/ItemModifierList;Lokio/ByteString;)V
    .locals 1

    .line 613
    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 614
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    .line 615
    iput-object p2, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 630
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 631
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    .line 632
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    .line 633
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    .line 634
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 639
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 641
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 642
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemModifierOption;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 643
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemModifierList;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 644
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;
    .locals 2

    .line 620
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;-><init>()V

    .line 621
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    .line 622
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    .line 623
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 583
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 651
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 652
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    if-eqz v1, :cond_0

    const-string v1, ", backing_modifier_option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 653
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    if-eqz v1, :cond_1

    const-string v1, ", modifier_list="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BackingDetails{"

    .line 654
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
