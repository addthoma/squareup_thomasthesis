.class public final Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SurchargeLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;",
        "Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public backing_type:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

.field public surcharge:Lcom/squareup/api/items/Surcharge;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 257
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public backing_type(Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;)Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;
    .locals 0

    .line 266
    iput-object p1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;->backing_type:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;
    .locals 4

    .line 272
    new-instance v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;->surcharge:Lcom/squareup/api/items/Surcharge;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;->backing_type:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;-><init>(Lcom/squareup/api/items/Surcharge;Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 252
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    move-result-object v0

    return-object v0
.end method

.method public surcharge(Lcom/squareup/api/items/Surcharge;)Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;
    .locals 0

    .line 261
    iput-object p1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;->surcharge:Lcom/squareup/api/items/Surcharge;

    return-object p0
.end method
