.class public final Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListEventsForContactResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;",
        "Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public event:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Event;",
            ">;"
        }
    .end annotation
.end field

.field public list_option:Lcom/squareup/protos/client/rolodex/ListOption;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 113
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 114
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;->event:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;
    .locals 5

    .line 141
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;->event:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;->list_option:Lcom/squareup/protos/client/rolodex/ListOption;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/rolodex/ListOption;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;

    move-result-object v0

    return-object v0
.end method

.method public event(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Event;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;"
        }
    .end annotation

    .line 126
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;->event:Ljava/util/List;

    return-object p0
.end method

.method public list_option(Lcom/squareup/protos/client/rolodex/ListOption;)Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;->list_option:Lcom/squareup/protos/client/rolodex/ListOption;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
