.class public final Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;
.super Lcom/squareup/wire/Message;
.source "GetBillFamiliesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InstrumentSearch"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$ProtoAdapter_InstrumentSearch;,
        Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field private static final serialVersionUID:J


# instance fields
.field public final entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Card$EntryMethod#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.PaymentInstrument#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 293
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$ProtoAdapter_InstrumentSearch;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$ProtoAdapter_InstrumentSearch;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 297
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->UNKNOWN_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sput-object v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/PaymentInstrument;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V
    .locals 1

    .line 319
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;-><init>(Lcom/squareup/protos/client/bills/PaymentInstrument;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/PaymentInstrument;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lokio/ByteString;)V
    .locals 1

    .line 324
    sget-object v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 325
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    .line 326
    iput-object p2, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 341
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 342
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;

    .line 343
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    .line 344
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 345
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 350
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 352
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 353
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/PaymentInstrument;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 354
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 355
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;
    .locals 2

    .line 331
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;-><init>()V

    .line 332
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    .line 333
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 334
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 292
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->newBuilder()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 362
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 363
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    if-eqz v1, :cond_0

    const-string v1, ", payment_instrument="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 364
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eqz v1, :cond_1

    const-string v1, ", entry_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$InstrumentSearch;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InstrumentSearch{"

    .line 365
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
