.class public final Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AddTendersRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddTendersRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/AddTendersRequest;",
        "Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public add_tender:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTender;",
            ">;"
        }
    .end annotation
.end field

.field public bill_id_pair:Lcom/squareup/protos/client/IdPair;

.field public cart:Lcom/squareup/protos/client/bills/Cart;

.field public merchant_token:Ljava/lang/String;

.field public square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

.field public tender_group_token:Ljava/lang/String;

.field public tender_info:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;",
            ">;"
        }
    .end annotation
.end field

.field public tender_info_loyalty_options:Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 216
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 217
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->add_tender:Ljava/util/List;

    .line 218
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_info:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public add_tender(Ljava/util/List;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTender;",
            ">;)",
            "Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;"
        }
    .end annotation

    .line 242
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 243
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->add_tender:Ljava/util/List;

    return-object p0
.end method

.method public bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;
    .locals 0

    .line 234
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/AddTendersRequest;
    .locals 11

    .line 298
    new-instance v10, Lcom/squareup/protos/client/bills/AddTendersRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->add_tender:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_info:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_info_loyalty_options:Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    iget-object v8, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_group_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/bills/AddTendersRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/bills/SquareProductAttributes;Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;Ljava/lang/String;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 199
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersRequest;

    move-result-object v0

    return-object v0
.end method

.method public cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;
    .locals 0

    .line 267
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;
    .locals 0

    .line 225
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public square_product_attributes(Lcom/squareup/protos/client/bills/SquareProductAttributes;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;
    .locals 0

    .line 277
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    return-object p0
.end method

.method public tender_group_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;
    .locals 0

    .line 292
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_group_token:Ljava/lang/String;

    return-object p0
.end method

.method public tender_info(Ljava/util/List;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;",
            ">;)",
            "Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;"
        }
    .end annotation

    .line 254
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 255
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_info:Ljava/util/List;

    return-object p0
.end method

.method public tender_info_loyalty_options(Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;
    .locals 0

    .line 283
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_info_loyalty_options:Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    return-object p0
.end method
