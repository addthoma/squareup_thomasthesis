.class public final Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CancelInvoiceRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;",
        "Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id_pair:Lcom/squareup/protos/client/IdPair;

.field public send_email_to_recipients:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;
    .locals 4

    .line 116
    new-instance v0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;->send_email_to_recipients:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;->build()Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;

    move-result-object v0

    return-object v0
.end method

.method public id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public send_email_to_recipients(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/CancelInvoiceRequest$Builder;->send_email_to_recipients:Ljava/lang/Boolean;

    return-object p0
.end method
