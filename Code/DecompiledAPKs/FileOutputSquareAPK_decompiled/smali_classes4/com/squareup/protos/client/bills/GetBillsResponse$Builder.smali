.class public final Lcom/squareup/protos/client/bills/GetBillsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetBillsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/GetBillsResponse;",
        "Lcom/squareup/protos/client/bills/GetBillsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Bill;",
            ">;"
        }
    .end annotation
.end field

.field public pagination_token:Ljava/lang/String;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 114
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 115
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/GetBillsResponse$Builder;->bill:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bill(Ljava/util/List;)Lcom/squareup/protos/client/bills/GetBillsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Bill;",
            ">;)",
            "Lcom/squareup/protos/client/bills/GetBillsResponse$Builder;"
        }
    .end annotation

    .line 124
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillsResponse$Builder;->bill:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/GetBillsResponse;
    .locals 5

    .line 141
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/GetBillsResponse$Builder;->bill:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/GetBillsResponse$Builder;->pagination_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/GetBillsResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillsResponse$Builder;->build()Lcom/squareup/protos/client/bills/GetBillsResponse;

    move-result-object v0

    return-object v0
.end method

.method public pagination_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillsResponse$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillsResponse$Builder;->pagination_token:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bills/GetBillsResponse$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
