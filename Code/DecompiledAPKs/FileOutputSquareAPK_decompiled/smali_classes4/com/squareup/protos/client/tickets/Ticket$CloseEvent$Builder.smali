.class public final Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Ticket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;",
        "Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public close_reason:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

.field public closed_at:Lcom/squareup/protos/client/ISO8601Date;

.field public terminal_event_id_pair:Lcom/squareup/protos/client/IdPair;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 512
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;
    .locals 5

    .line 555
    new-instance v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v2, p0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;->close_reason:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    iget-object v3, p0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;->terminal_event_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;-><init>(Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;Lcom/squareup/protos/client/IdPair;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 505
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    move-result-object v0

    return-object v0
.end method

.method public close_reason(Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;)Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;
    .locals 0

    .line 527
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;->close_reason:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    return-object p0
.end method

.method public closed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;
    .locals 0

    .line 519
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public terminal_event_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;
    .locals 0

    .line 549
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$Builder;->terminal_event_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method
