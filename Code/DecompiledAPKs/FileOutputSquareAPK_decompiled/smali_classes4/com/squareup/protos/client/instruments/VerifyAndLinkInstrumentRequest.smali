.class public final Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;
.super Lcom/squareup/wire/Message;
.source "VerifyAndLinkInstrumentRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$ProtoAdapter_VerifyAndLinkInstrumentRequest;,
        Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;",
        "Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CARDHOLDER_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_CONTACT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public static final DEFAULT_UNIQUE_KEY:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final cardholder_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final contact_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final creator_details:Lcom/squareup/protos/client/CreatorDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.CreatorDetails#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Card$EntryMethod#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final linked_instrument:Lcom/squareup/protos/client/instruments/LinkedInstrument;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.instruments.LinkedInstrument#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final unique_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final validation_information:Lcom/squareup/protos/client/instruments/ValidationInformation;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.instruments.ValidationInformation#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$ProtoAdapter_VerifyAndLinkInstrumentRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$ProtoAdapter_VerifyAndLinkInstrumentRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->UNKNOWN_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sput-object v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/instruments/LinkedInstrument;Lcom/squareup/protos/client/instruments/ValidationInformation;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Ljava/lang/String;Lcom/squareup/protos/client/CreatorDetails;)V
    .locals 9

    .line 102
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/instruments/LinkedInstrument;Lcom/squareup/protos/client/instruments/ValidationInformation;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Ljava/lang/String;Lcom/squareup/protos/client/CreatorDetails;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/instruments/LinkedInstrument;Lcom/squareup/protos/client/instruments/ValidationInformation;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Ljava/lang/String;Lcom/squareup/protos/client/CreatorDetails;Lokio/ByteString;)V
    .locals 1

    .line 109
    sget-object v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 110
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->unique_key:Ljava/lang/String;

    .line 111
    iput-object p2, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->cardholder_name:Ljava/lang/String;

    .line 112
    iput-object p3, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->linked_instrument:Lcom/squareup/protos/client/instruments/LinkedInstrument;

    .line 113
    iput-object p4, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->validation_information:Lcom/squareup/protos/client/instruments/ValidationInformation;

    .line 114
    iput-object p5, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 115
    iput-object p6, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->contact_token:Ljava/lang/String;

    .line 116
    iput-object p7, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 136
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 137
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;

    .line 138
    invoke-virtual {p0}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->unique_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->unique_key:Ljava/lang/String;

    .line 139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->cardholder_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->cardholder_name:Ljava/lang/String;

    .line 140
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->linked_instrument:Lcom/squareup/protos/client/instruments/LinkedInstrument;

    iget-object v3, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->linked_instrument:Lcom/squareup/protos/client/instruments/LinkedInstrument;

    .line 141
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->validation_information:Lcom/squareup/protos/client/instruments/ValidationInformation;

    iget-object v3, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->validation_information:Lcom/squareup/protos/client/instruments/ValidationInformation;

    .line 142
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iget-object v3, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 143
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->contact_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->contact_token:Ljava/lang/String;

    .line 144
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 145
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 150
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 152
    invoke-virtual {p0}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->unique_key:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->cardholder_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->linked_instrument:Lcom/squareup/protos/client/instruments/LinkedInstrument;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/instruments/LinkedInstrument;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->validation_information:Lcom/squareup/protos/client/instruments/ValidationInformation;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/instruments/ValidationInformation;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->contact_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 160
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;
    .locals 2

    .line 121
    new-instance v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;-><init>()V

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->unique_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->unique_key:Ljava/lang/String;

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->cardholder_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->cardholder_name:Ljava/lang/String;

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->linked_instrument:Lcom/squareup/protos/client/instruments/LinkedInstrument;

    iput-object v1, v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->linked_instrument:Lcom/squareup/protos/client/instruments/LinkedInstrument;

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->validation_information:Lcom/squareup/protos/client/instruments/ValidationInformation;

    iput-object v1, v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->validation_information:Lcom/squareup/protos/client/instruments/ValidationInformation;

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iput-object v1, v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->contact_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->contact_token:Ljava/lang/String;

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->newBuilder()Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 167
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->unique_key:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", unique_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->unique_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->cardholder_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", cardholder_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->linked_instrument:Lcom/squareup/protos/client/instruments/LinkedInstrument;

    if-eqz v1, :cond_2

    const-string v1, ", linked_instrument="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->linked_instrument:Lcom/squareup/protos/client/instruments/LinkedInstrument;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 171
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->validation_information:Lcom/squareup/protos/client/instruments/ValidationInformation;

    if-eqz v1, :cond_3

    const-string v1, ", validation_information="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->validation_information:Lcom/squareup/protos/client/instruments/ValidationInformation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 172
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eqz v1, :cond_4

    const-string v1, ", entry_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 173
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->contact_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", contact_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->contact_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_6

    const-string v1, ", creator_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "VerifyAndLinkInstrumentRequest{"

    .line 175
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
