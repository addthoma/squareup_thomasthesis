.class public final Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpdateUnitMetadataRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest;",
        "Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 77
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest;
    .locals 3

    .line 87
    new-instance v0, Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest$Builder;->metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest;-><init>(Lcom/squareup/protos/client/invoice/UnitMetadata;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 74
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest$Builder;->build()Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest;

    move-result-object v0

    return-object v0
.end method

.method public metadata(Lcom/squareup/protos/client/invoice/UnitMetadata;)Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest$Builder;
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest$Builder;->metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    return-object p0
.end method
