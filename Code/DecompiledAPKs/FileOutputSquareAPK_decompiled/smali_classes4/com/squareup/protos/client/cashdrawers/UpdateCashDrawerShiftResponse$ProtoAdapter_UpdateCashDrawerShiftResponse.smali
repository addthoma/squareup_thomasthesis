.class final Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$ProtoAdapter_UpdateCashDrawerShiftResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "UpdateCashDrawerShiftResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_UpdateCashDrawerShiftResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 145
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 162
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Builder;-><init>()V

    .line 163
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 164
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 175
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 168
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Status;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Builder;->status(Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Status;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 170
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 179
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 180
    invoke-virtual {v0}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Builder;->build()Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 143
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$ProtoAdapter_UpdateCashDrawerShiftResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 156
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;->status:Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Status;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 157
    invoke-virtual {p2}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 143
    check-cast p2, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$ProtoAdapter_UpdateCashDrawerShiftResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;)I
    .locals 3

    .line 150
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;->status:Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Status;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 151
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 143
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$ProtoAdapter_UpdateCashDrawerShiftResponse;->encodedSize(Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;
    .locals 0

    .line 185
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;->newBuilder()Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Builder;

    move-result-object p1

    .line 186
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 187
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Builder;->build()Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 143
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$ProtoAdapter_UpdateCashDrawerShiftResponse;->redact(Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;

    move-result-object p1

    return-object p1
.end method
