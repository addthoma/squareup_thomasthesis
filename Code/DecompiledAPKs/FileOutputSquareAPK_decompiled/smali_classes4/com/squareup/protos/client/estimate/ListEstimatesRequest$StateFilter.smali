.class public final enum Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;
.super Ljava/lang/Enum;
.source "ListEstimatesRequest.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/ListEstimatesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "StateFilter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter$ProtoAdapter_StateFilter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

.field public static final enum ACCEPTED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ALL:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

.field public static final enum ARCHIVED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

.field public static final enum DRAFT:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

.field public static final enum EXPIRED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

.field public static final enum INVOICED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

.field public static final enum PENDING:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

.field public static final enum SCHEDULED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

.field public static final enum UNSUCCESSFUL:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 301
    new-instance v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->UNKNOWN:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    .line 306
    new-instance v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    const/4 v2, 0x1

    const-string v3, "DRAFT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->DRAFT:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    .line 311
    new-instance v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    const/4 v3, 0x2

    const-string v4, "PENDING"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->PENDING:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    .line 316
    new-instance v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    const/4 v4, 0x3

    const-string v5, "SCHEDULED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->SCHEDULED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    .line 321
    new-instance v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    const/4 v5, 0x4

    const-string v6, "EXPIRED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->EXPIRED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    .line 326
    new-instance v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    const/4 v6, 0x5

    const-string v7, "ACCEPTED"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->ACCEPTED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    .line 331
    new-instance v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    const/4 v7, 0x6

    const-string v8, "INVOICED"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->INVOICED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    .line 336
    new-instance v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    const/4 v8, 0x7

    const-string v9, "UNSUCCESSFUL"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->UNSUCCESSFUL:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    .line 341
    new-instance v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    const/16 v9, 0x8

    const-string v10, "ALL"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->ALL:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    .line 346
    new-instance v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    const/16 v10, 0x9

    const-string v11, "ARCHIVED"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->ARCHIVED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    .line 300
    sget-object v11, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->UNKNOWN:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    aput-object v11, v0, v1

    sget-object v1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->DRAFT:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->PENDING:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->SCHEDULED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->EXPIRED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->ACCEPTED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->INVOICED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->UNSUCCESSFUL:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->ALL:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->ARCHIVED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    aput-object v1, v0, v10

    sput-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->$VALUES:[Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    .line 348
    new-instance v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter$ProtoAdapter_StateFilter;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter$ProtoAdapter_StateFilter;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 352
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 353
    iput p3, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 370
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->ARCHIVED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    return-object p0

    .line 369
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->ALL:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    return-object p0

    .line 368
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->UNSUCCESSFUL:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    return-object p0

    .line 367
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->INVOICED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    return-object p0

    .line 366
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->ACCEPTED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    return-object p0

    .line 365
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->EXPIRED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    return-object p0

    .line 364
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->SCHEDULED:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    return-object p0

    .line 363
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->PENDING:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    return-object p0

    .line 362
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->DRAFT:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    return-object p0

    .line 361
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->UNKNOWN:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;
    .locals 1

    .line 300
    const-class v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;
    .locals 1

    .line 300
    sget-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->$VALUES:[Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 377
    iget v0, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->value:I

    return v0
.end method
