.class final Lcom/squareup/protos/client/tickets/Ticket$ProtoAdapter_Ticket;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Ticket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tickets/Ticket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Ticket"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/tickets/Ticket;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 689
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/tickets/Ticket;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/tickets/Ticket;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 726
    new-instance v0, Lcom/squareup/protos/client/tickets/Ticket$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tickets/Ticket$Builder;-><init>()V

    .line 727
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 728
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 742
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 740
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/tickets/Transient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/tickets/Transient;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->transient_(Lcom/squareup/protos/client/tickets/Transient;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    goto :goto_0

    .line 739
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    goto :goto_0

    .line 738
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->close_event(Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    goto :goto_0

    .line 737
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->note(Ljava/lang/String;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    goto :goto_0

    .line 736
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->client_updated_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    goto :goto_0

    .line 735
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    goto :goto_0

    .line 734
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    goto :goto_0

    .line 733
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->closed_at_deprecated(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    goto :goto_0

    .line 732
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/client/tickets/VectorClock;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/tickets/VectorClock;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->vector_clock(Lcom/squareup/protos/client/tickets/VectorClock;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    goto :goto_0

    .line 731
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->tender_id_deprecated(Ljava/lang/String;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    goto/16 :goto_0

    .line 730
    :pswitch_b
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->ticket_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    goto/16 :goto_0

    .line 746
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 747
    invoke-virtual {v0}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 687
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/tickets/Ticket$ProtoAdapter_Ticket;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/tickets/Ticket;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 710
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tickets/Ticket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 711
    sget-object v0, Lcom/squareup/protos/client/tickets/VectorClock;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tickets/Ticket;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 712
    sget-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 713
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tickets/Ticket;->name:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 714
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tickets/Ticket;->client_updated_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 715
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tickets/Ticket;->note:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 716
    sget-object v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tickets/Ticket;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 717
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tickets/Ticket;->total_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 718
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tickets/Ticket;->tender_id_deprecated:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 719
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tickets/Ticket;->closed_at_deprecated:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 720
    sget-object v0, Lcom/squareup/protos/client/tickets/Transient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/tickets/Ticket;->transient_:Lcom/squareup/protos/client/tickets/Transient;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 721
    invoke-virtual {p2}, Lcom/squareup/protos/client/tickets/Ticket;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 687
    check-cast p2, Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/tickets/Ticket$ProtoAdapter_Ticket;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/tickets/Ticket;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/tickets/Ticket;)I
    .locals 4

    .line 694
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/tickets/Ticket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/tickets/VectorClock;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tickets/Ticket;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    const/4 v3, 0x3

    .line 695
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v3, 0x5

    .line 696
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tickets/Ticket;->name:Ljava/lang/String;

    const/4 v3, 0x6

    .line 697
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tickets/Ticket;->client_updated_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x7

    .line 698
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tickets/Ticket;->note:Ljava/lang/String;

    const/16 v3, 0x8

    .line 699
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tickets/Ticket;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    const/16 v3, 0x9

    .line 700
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tickets/Ticket;->total_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xa

    .line 701
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tickets/Ticket;->tender_id_deprecated:Ljava/lang/String;

    const/4 v3, 0x2

    .line 702
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tickets/Ticket;->closed_at_deprecated:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x4

    .line 703
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/tickets/Transient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/tickets/Ticket;->transient_:Lcom/squareup/protos/client/tickets/Transient;

    const/16 v3, 0xc

    .line 704
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 705
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 687
    check-cast p1, Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/tickets/Ticket$ProtoAdapter_Ticket;->encodedSize(Lcom/squareup/protos/client/tickets/Ticket;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/protos/client/tickets/Ticket;
    .locals 2

    .line 752
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p1

    .line 753
    iget-object v0, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 754
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/tickets/VectorClock;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/tickets/VectorClock;

    iput-object v0, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    .line 755
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart;

    iput-object v0, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 756
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->client_updated_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->client_updated_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->client_updated_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 757
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    iput-object v0, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    .line 758
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->total_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->total_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->total_money:Lcom/squareup/protos/common/Money;

    .line 759
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->closed_at_deprecated:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->closed_at_deprecated:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->closed_at_deprecated:Lcom/squareup/protos/client/ISO8601Date;

    .line 760
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->transient_:Lcom/squareup/protos/client/tickets/Transient;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/client/tickets/Transient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->transient_:Lcom/squareup/protos/client/tickets/Transient;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/tickets/Transient;

    iput-object v0, p1, Lcom/squareup/protos/client/tickets/Ticket$Builder;->transient_:Lcom/squareup/protos/client/tickets/Transient;

    .line 761
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 762
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 687
    check-cast p1, Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/tickets/Ticket$ProtoAdapter_Ticket;->redact(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    return-object p1
.end method
