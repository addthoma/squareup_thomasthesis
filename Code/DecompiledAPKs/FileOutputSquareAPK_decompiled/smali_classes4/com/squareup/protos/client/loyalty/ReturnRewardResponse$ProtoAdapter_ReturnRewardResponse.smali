.class final Lcom/squareup/protos/client/loyalty/ReturnRewardResponse$ProtoAdapter_ReturnRewardResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReturnRewardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReturnRewardResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 94
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 111
    new-instance v0, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse$Builder;-><init>()V

    .line 112
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 113
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 117
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 115
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/loyalty/ReturnRewardResponse$Builder;

    goto :goto_0

    .line 121
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 122
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 92
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse$ProtoAdapter_ReturnRewardResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 105
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 106
    invoke-virtual {p2}, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 92
    check-cast p2, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse$ProtoAdapter_ReturnRewardResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;)I
    .locals 3

    .line 99
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 100
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 92
    check-cast p1, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse$ProtoAdapter_ReturnRewardResponse;->encodedSize(Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;)Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;
    .locals 2

    .line 127
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;->newBuilder()Lcom/squareup/protos/client/loyalty/ReturnRewardResponse$Builder;

    move-result-object p1

    .line 128
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Status;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 129
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 130
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 92
    check-cast p1, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/ReturnRewardResponse$ProtoAdapter_ReturnRewardResponse;->redact(Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;)Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;

    move-result-object p1

    return-object p1
.end method
