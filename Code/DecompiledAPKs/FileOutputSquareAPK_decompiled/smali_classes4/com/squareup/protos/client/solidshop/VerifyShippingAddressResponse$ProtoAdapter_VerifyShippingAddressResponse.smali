.class final Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$ProtoAdapter_VerifyShippingAddressResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "VerifyShippingAddressResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_VerifyShippingAddressResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 344
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 369
    new-instance v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;-><init>()V

    .line 370
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 371
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 393
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 386
    :cond_0
    :try_start_0
    iget-object v4, v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->corrected_field:Ljava/util/List;

    sget-object v5, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 388
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 383
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->corrected_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;

    goto :goto_0

    .line 382
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;

    goto :goto_0

    .line 376
    :cond_3
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->verification_status(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 378
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 373
    :cond_4
    sget-object v3, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;

    goto :goto_0

    .line 397
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 398
    invoke-virtual {v0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->build()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 342
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$ProtoAdapter_VerifyShippingAddressResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 359
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 360
    sget-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->verification_status:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 361
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->token:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 362
    sget-object v0, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->corrected_address:Lcom/squareup/protos/common/location/GlobalAddress;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 363
    sget-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->corrected_field:Ljava/util/List;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 364
    invoke-virtual {p2}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 342
    check-cast p2, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$ProtoAdapter_VerifyShippingAddressResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;)I
    .locals 4

    .line 349
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->verification_status:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    const/4 v3, 0x2

    .line 350
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->token:Ljava/lang/String;

    const/4 v3, 0x3

    .line 351
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->corrected_address:Lcom/squareup/protos/common/location/GlobalAddress;

    const/4 v3, 0x4

    .line 352
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 353
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->corrected_field:Ljava/util/List;

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 354
    invoke-virtual {p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 342
    check-cast p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$ProtoAdapter_VerifyShippingAddressResponse;->encodedSize(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;
    .locals 2

    .line 403
    invoke-virtual {p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->newBuilder()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;

    move-result-object p1

    .line 404
    iget-object v0, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Status;

    iput-object v0, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    :cond_0
    const/4 v0, 0x0

    .line 405
    iput-object v0, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->corrected_address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 406
    invoke-virtual {p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 407
    invoke-virtual {p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->build()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 342
    check-cast p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$ProtoAdapter_VerifyShippingAddressResponse;->redact(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;

    move-result-object p1

    return-object p1
.end method
