.class public final Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StartIdentityVerificationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;",
        "Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

.field public owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

.field public owner_social_security_number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 113
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;
    .locals 5

    .line 133
    new-instance v0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;->owner_social_security_number:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;-><init>(Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;

    move-result-object v0

    return-object v0
.end method

.method public owner_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object p0
.end method

.method public owner_birth_date(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object p0
.end method

.method public owner_social_security_number(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest$Builder;->owner_social_security_number:Ljava/lang/String;

    return-object p0
.end method
