.class public final enum Lcom/squareup/protos/client/InventoryAdjustmentReason;
.super Ljava/lang/Enum;
.source "InventoryAdjustmentReason.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/InventoryAdjustmentReason$ProtoAdapter_InventoryAdjustmentReason;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/InventoryAdjustmentReason;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/InventoryAdjustmentReason;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/InventoryAdjustmentReason;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CANCELED_SALE:Lcom/squareup/protos/client/InventoryAdjustmentReason;

.field public static final enum DAMAGED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

.field public static final enum DO_NOT_USE_REASON:Lcom/squareup/protos/client/InventoryAdjustmentReason;

.field public static final enum LOST:Lcom/squareup/protos/client/InventoryAdjustmentReason;

.field public static final enum RECEIVED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

.field public static final enum RETURNED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

.field public static final enum THEFT:Lcom/squareup/protos/client/InventoryAdjustmentReason;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 14
    new-instance v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE_REASON"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/InventoryAdjustmentReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->DO_NOT_USE_REASON:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    .line 19
    new-instance v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;

    const/4 v2, 0x1

    const-string v3, "RECEIVED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/InventoryAdjustmentReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->RECEIVED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    .line 24
    new-instance v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;

    const/4 v3, 0x2

    const-string v4, "DAMAGED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/InventoryAdjustmentReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->DAMAGED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    .line 29
    new-instance v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;

    const/4 v4, 0x3

    const-string v5, "THEFT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/InventoryAdjustmentReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->THEFT:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    .line 34
    new-instance v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;

    const/4 v5, 0x4

    const-string v6, "LOST"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/InventoryAdjustmentReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->LOST:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    .line 39
    new-instance v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;

    const/4 v6, 0x5

    const-string v7, "RETURNED"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/InventoryAdjustmentReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->RETURNED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    .line 44
    new-instance v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;

    const/4 v7, 0x6

    const-string v8, "CANCELED_SALE"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/InventoryAdjustmentReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->CANCELED_SALE:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/protos/client/InventoryAdjustmentReason;

    .line 13
    sget-object v8, Lcom/squareup/protos/client/InventoryAdjustmentReason;->DO_NOT_USE_REASON:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/client/InventoryAdjustmentReason;->RECEIVED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/InventoryAdjustmentReason;->DAMAGED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/InventoryAdjustmentReason;->THEFT:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/InventoryAdjustmentReason;->LOST:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/InventoryAdjustmentReason;->RETURNED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/InventoryAdjustmentReason;->CANCELED_SALE:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->$VALUES:[Lcom/squareup/protos/client/InventoryAdjustmentReason;

    .line 46
    new-instance v0, Lcom/squareup/protos/client/InventoryAdjustmentReason$ProtoAdapter_InventoryAdjustmentReason;

    invoke-direct {v0}, Lcom/squareup/protos/client/InventoryAdjustmentReason$ProtoAdapter_InventoryAdjustmentReason;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 51
    iput p3, p0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/InventoryAdjustmentReason;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 65
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->CANCELED_SALE:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    return-object p0

    .line 64
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->RETURNED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    return-object p0

    .line 63
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->LOST:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    return-object p0

    .line 62
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->THEFT:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    return-object p0

    .line 61
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->DAMAGED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    return-object p0

    .line 60
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->RECEIVED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    return-object p0

    .line 59
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->DO_NOT_USE_REASON:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustmentReason;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/InventoryAdjustmentReason;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/InventoryAdjustmentReason;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->$VALUES:[Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/InventoryAdjustmentReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/InventoryAdjustmentReason;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 72
    iget v0, p0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->value:I

    return v0
.end method
