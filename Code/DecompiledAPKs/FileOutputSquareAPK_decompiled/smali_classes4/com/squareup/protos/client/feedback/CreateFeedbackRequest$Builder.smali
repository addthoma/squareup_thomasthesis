.class public final Lcom/squareup/protos/client/feedback/CreateFeedbackRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateFeedbackRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/feedback/CreateFeedbackRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/feedback/CreateFeedbackRequest;",
        "Lcom/squareup/protos/client/feedback/CreateFeedbackRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public feedback:Ljava/lang/String;

.field public rating:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 101
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/feedback/CreateFeedbackRequest;
    .locals 4

    .line 122
    new-instance v0, Lcom/squareup/protos/client/feedback/CreateFeedbackRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/feedback/CreateFeedbackRequest$Builder;->rating:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/feedback/CreateFeedbackRequest$Builder;->feedback:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/feedback/CreateFeedbackRequest;-><init>(Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/feedback/CreateFeedbackRequest$Builder;->build()Lcom/squareup/protos/client/feedback/CreateFeedbackRequest;

    move-result-object v0

    return-object v0
.end method

.method public feedback(Ljava/lang/String;)Lcom/squareup/protos/client/feedback/CreateFeedbackRequest$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/feedback/CreateFeedbackRequest$Builder;->feedback:Ljava/lang/String;

    return-object p0
.end method

.method public rating(Ljava/lang/Integer;)Lcom/squareup/protos/client/feedback/CreateFeedbackRequest$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/client/feedback/CreateFeedbackRequest$Builder;->rating:Ljava/lang/Integer;

    return-object p0
.end method
