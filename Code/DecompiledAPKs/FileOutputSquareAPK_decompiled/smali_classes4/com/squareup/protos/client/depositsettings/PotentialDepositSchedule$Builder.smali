.class public final Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PotentialDepositSchedule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;",
        "Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public deposit_schedule:Lcom/squareup/protos/client/depositsettings/DepositSchedule;

.field public requested_cutoff_at:Lcom/squareup/protos/common/time/LocalTime;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 101
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;
    .locals 4

    .line 122
    new-instance v0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;->requested_cutoff_at:Lcom/squareup/protos/common/time/LocalTime;

    iget-object v2, p0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;->deposit_schedule:Lcom/squareup/protos/client/depositsettings/DepositSchedule;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;-><init>(Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/protos/client/depositsettings/DepositSchedule;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;->build()Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;

    move-result-object v0

    return-object v0
.end method

.method public deposit_schedule(Lcom/squareup/protos/client/depositsettings/DepositSchedule;)Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;->deposit_schedule:Lcom/squareup/protos/client/depositsettings/DepositSchedule;

    return-object p0
.end method

.method public requested_cutoff_at(Lcom/squareup/protos/common/time/LocalTime;)Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;->requested_cutoff_at:Lcom/squareup/protos/common/time/LocalTime;

    return-object p0
.end method
