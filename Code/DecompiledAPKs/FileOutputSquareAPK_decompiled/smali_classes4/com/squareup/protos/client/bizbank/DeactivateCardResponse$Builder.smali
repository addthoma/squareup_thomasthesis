.class public final Lcom/squareup/protos/client/bizbank/DeactivateCardResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeactivateCardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/DeactivateCardResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/DeactivateCardResponse;",
        "Lcom/squareup/protos/client/bizbank/DeactivateCardResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bizbank_enabled:Ljava/lang/Boolean;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 94
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bizbank_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/DeactivateCardResponse$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardResponse$Builder;->bizbank_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bizbank/DeactivateCardResponse;
    .locals 4

    .line 109
    new-instance v0, Lcom/squareup/protos/client/bizbank/DeactivateCardResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardResponse$Builder;->bizbank_enabled:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bizbank/DeactivateCardResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/DeactivateCardResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/DeactivateCardResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bizbank/DeactivateCardResponse$Builder;
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
