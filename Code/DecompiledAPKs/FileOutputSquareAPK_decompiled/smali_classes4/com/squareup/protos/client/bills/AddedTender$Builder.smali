.class public final Lcom/squareup/protos/client/bills/AddedTender$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AddedTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddedTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/AddedTender;",
        "Lcom/squareup/protos/client/bills/AddedTender$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cart_punch_status:Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;

.field public coupon:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;"
        }
    .end annotation
.end field

.field public loyalty_status:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;",
            ">;"
        }
    .end annotation
.end field

.field public receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

.field public remaining_balance_money:Lcom/squareup/protos/common/Money;

.field public status:Lcom/squareup/protos/client/Status;

.field public tender:Lcom/squareup/protos/client/bills/Tender;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 198
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 199
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->coupon:Ljava/util/List;

    .line 200
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->loyalty_status:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/AddedTender;
    .locals 10

    .line 270
    new-instance v9, Lcom/squareup/protos/client/bills/AddedTender;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->remaining_balance_money:Lcom/squareup/protos/common/Money;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->coupon:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->cart_punch_status:Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->loyalty_status:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/AddedTender;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;Lcom/squareup/protos/common/Money;Ljava/util/List;Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;Ljava/util/List;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 183
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$Builder;->build()Lcom/squareup/protos/client/bills/AddedTender;

    move-result-object v0

    return-object v0
.end method

.method public cart_punch_status(Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;)Lcom/squareup/protos/client/bills/AddedTender$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 254
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->cart_punch_status:Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;

    return-object p0
.end method

.method public coupon(Ljava/util/List;)Lcom/squareup/protos/client/bills/AddedTender$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;)",
            "Lcom/squareup/protos/client/bills/AddedTender$Builder;"
        }
    .end annotation

    .line 244
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 245
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->coupon:Ljava/util/List;

    return-object p0
.end method

.method public loyalty_status(Ljava/util/List;)Lcom/squareup/protos/client/bills/AddedTender$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;",
            ">;)",
            "Lcom/squareup/protos/client/bills/AddedTender$Builder;"
        }
    .end annotation

    .line 263
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 264
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->loyalty_status:Ljava/util/List;

    return-object p0
.end method

.method public receipt_details(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;)Lcom/squareup/protos/client/bills/AddedTender$Builder;
    .locals 0

    .line 224
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    return-object p0
.end method

.method public remaining_balance_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/AddedTender$Builder;
    .locals 0

    .line 236
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->remaining_balance_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bills/AddedTender$Builder;
    .locals 0

    .line 204
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public tender(Lcom/squareup/protos/client/bills/Tender;)Lcom/squareup/protos/client/bills/AddedTender$Builder;
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->tender:Lcom/squareup/protos/client/bills/Tender;

    return-object p0
.end method
