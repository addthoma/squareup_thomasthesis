.class public final Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetBillFamiliesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public exclude_related_bills:Ljava/lang/Boolean;

.field public limit:Ljava/lang/Integer;

.field public merchant_token:Ljava/lang/String;

.field public pagination_token:Ljava/lang/String;

.field public populate_tender_contact_data:Ljava/lang/Boolean;

.field public query:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

.field public unit_token:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 217
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 218
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->unit_token:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;
    .locals 10

    .line 288
    new-instance v9, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->query:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->limit:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->pagination_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->exclude_related_bills:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->unit_token:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->populate_tender_contact_data:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;-><init>(Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 202
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;

    move-result-object v0

    return-object v0
.end method

.method public exclude_related_bills(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;
    .locals 0

    .line 262
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->exclude_related_bills:Ljava/lang/Boolean;

    return-object p0
.end method

.method public limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;
    .locals 0

    .line 234
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->limit:Ljava/lang/Integer;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;
    .locals 0

    .line 253
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public pagination_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->pagination_token:Ljava/lang/String;

    return-object p0
.end method

.method public populate_tender_contact_data(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;
    .locals 0

    .line 282
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->populate_tender_contact_data:Ljava/lang/Boolean;

    return-object p0
.end method

.method public query(Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;
    .locals 0

    .line 222
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->query:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    return-object p0
.end method

.method public unit_token(Ljava/util/List;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;"
        }
    .end annotation

    .line 272
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 273
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->unit_token:Ljava/util/List;

    return-object p0
.end method
