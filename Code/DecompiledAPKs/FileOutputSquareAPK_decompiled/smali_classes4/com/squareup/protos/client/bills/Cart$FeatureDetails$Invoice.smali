.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Invoice"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$ProtoAdapter_Invoice;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_IS_FINAL_PAYMENT:Ljava/lang/Boolean;

.field public static final DEFAULT_TIPPING_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_VERSION:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final invoice_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final is_final_payment:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final tipping_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final version:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1997
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$ProtoAdapter_Invoice;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$ProtoAdapter_Invoice;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 2001
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->DEFAULT_TIPPING_ENABLED:Ljava/lang/Boolean;

    const/4 v0, 0x1

    .line 2005
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->DEFAULT_IS_FINAL_PAYMENT:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 6

    .line 2044
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 2049
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 2050
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 2051
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->tipping_enabled:Ljava/lang/Boolean;

    .line 2052
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->version:Ljava/lang/String;

    .line 2053
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->is_final_payment:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 2070
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2071
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    .line 2072
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 2073
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->tipping_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->tipping_enabled:Ljava/lang/Boolean;

    .line 2074
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->version:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->version:Ljava/lang/String;

    .line 2075
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->is_final_payment:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->is_final_payment:Ljava/lang/Boolean;

    .line 2076
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 2081
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 2083
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 2084
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2085
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->tipping_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2086
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->version:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2087
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->is_final_payment:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 2088
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;
    .locals 2

    .line 2058
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;-><init>()V

    .line 2059
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 2060
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->tipping_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->tipping_enabled:Ljava/lang/Boolean;

    .line 2061
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->version:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->version:Ljava/lang/String;

    .line 2062
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->is_final_payment:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->is_final_payment:Ljava/lang/Boolean;

    .line 2063
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1996
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 2095
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2096
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", invoice_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2097
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->tipping_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", tipping_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->tipping_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2098
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->version:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2099
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->is_final_payment:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", is_final_payment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->is_final_payment:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Invoice{"

    .line 2100
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
