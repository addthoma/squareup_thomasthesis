.class final Lcom/squareup/protos/client/rolodex/GroupV2$ProtoAdapter_GroupV2;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GroupV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/GroupV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GroupV2"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/rolodex/GroupV2;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 310
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/rolodex/GroupV2;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/GroupV2;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 337
    new-instance v0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;-><init>()V

    .line 338
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 339
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 355
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 353
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/client/rolodex/GroupV2Counts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->counts(Lcom/squareup/protos/client/rolodex/GroupV2Counts;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    goto :goto_0

    .line 352
    :pswitch_1
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->filters:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 351
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    goto :goto_0

    .line 350
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    goto :goto_0

    .line 349
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->group_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    goto :goto_0

    .line 343
    :pswitch_5
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/rolodex/GroupV2$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->type(Lcom/squareup/protos/client/rolodex/GroupV2$Type;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 345
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 359
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 360
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->build()Lcom/squareup/protos/client/rolodex/GroupV2;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 308
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/GroupV2$ProtoAdapter_GroupV2;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/GroupV2;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/GroupV2;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 326
    sget-object v0, Lcom/squareup/protos/client/rolodex/GroupV2$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/GroupV2;->type:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 327
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/GroupV2;->group_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 328
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/GroupV2;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 329
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/GroupV2;->name:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 330
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/GroupV2;->filters:Ljava/util/List;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 331
    sget-object v0, Lcom/squareup/protos/client/rolodex/GroupV2Counts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/GroupV2;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 332
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/GroupV2;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 308
    check-cast p2, Lcom/squareup/protos/client/rolodex/GroupV2;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/rolodex/GroupV2$ProtoAdapter_GroupV2;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/GroupV2;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/rolodex/GroupV2;)I
    .locals 4

    .line 315
    sget-object v0, Lcom/squareup/protos/client/rolodex/GroupV2$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/GroupV2;->type:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/GroupV2;->group_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v3, 0x2

    .line 316
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/GroupV2;->merchant_token:Ljava/lang/String;

    const/4 v3, 0x3

    .line 317
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/GroupV2;->name:Ljava/lang/String;

    const/4 v3, 0x4

    .line 318
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 319
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/GroupV2;->filters:Ljava/util/List;

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/GroupV2Counts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/GroupV2;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    const/4 v3, 0x6

    .line 320
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 321
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GroupV2;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 308
    check-cast p1, Lcom/squareup/protos/client/rolodex/GroupV2;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/GroupV2$ProtoAdapter_GroupV2;->encodedSize(Lcom/squareup/protos/client/rolodex/GroupV2;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/rolodex/GroupV2;)Lcom/squareup/protos/client/rolodex/GroupV2;
    .locals 2

    .line 365
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GroupV2;->newBuilder()Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    move-result-object p1

    .line 366
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->group_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->group_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->group_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 367
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->filters:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 368
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/rolodex/GroupV2Counts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    .line 369
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 370
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->build()Lcom/squareup/protos/client/rolodex/GroupV2;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 308
    check-cast p1, Lcom/squareup/protos/client/rolodex/GroupV2;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/GroupV2$ProtoAdapter_GroupV2;->redact(Lcom/squareup/protos/client/rolodex/GroupV2;)Lcom/squareup/protos/client/rolodex/GroupV2;

    move-result-object p1

    return-object p1
.end method
