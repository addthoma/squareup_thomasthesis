.class public final Lcom/squareup/protos/client/solidshop/Order;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/solidshop/Order$ProtoAdapter_Order;,
        Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;,
        Lcom/squareup/protos/client/solidshop/Order$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/solidshop/Order;",
        "Lcom/squareup/protos/client/solidshop/Order$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/solidshop/Order;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ITEM_COUNT:Ljava/lang/Integer;

.field public static final DEFAULT_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_USER_FACING_STATUS:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

.field private static final serialVersionUID:J


# instance fields
.field public final carton:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.solidshop.Carton#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/solidshop/Carton;",
            ">;"
        }
    .end annotation
.end field

.field public final item_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final placed_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final total:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final user_facing_status:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.solidshop.Order$UserFacingStatus#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$ProtoAdapter_Order;

    invoke-direct {v0}, Lcom/squareup/protos/client/solidshop/Order$ProtoAdapter_Order;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 32
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order;->DEFAULT_ITEM_COUNT:Ljava/lang/Integer;

    .line 34
    sget-object v0, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->CANCELED:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    sput-object v0, Lcom/squareup/protos/client/solidshop/Order;->DEFAULT_USER_FACING_STATUS:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/Money;Ljava/lang/Integer;Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/solidshop/Carton;",
            ">;)V"
        }
    .end annotation

    .line 92
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/solidshop/Order;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/Money;Ljava/lang/Integer;Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/Money;Ljava/lang/Integer;Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/solidshop/Carton;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 97
    sget-object v0, Lcom/squareup/protos/client/solidshop/Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 98
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/Order;->number:Ljava/lang/String;

    .line 99
    iput-object p2, p0, Lcom/squareup/protos/client/solidshop/Order;->placed_at:Lcom/squareup/protos/common/time/DateTime;

    .line 100
    iput-object p3, p0, Lcom/squareup/protos/client/solidshop/Order;->total:Lcom/squareup/protos/common/Money;

    .line 101
    iput-object p4, p0, Lcom/squareup/protos/client/solidshop/Order;->item_count:Ljava/lang/Integer;

    .line 102
    iput-object p5, p0, Lcom/squareup/protos/client/solidshop/Order;->user_facing_status:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    const-string p1, "carton"

    .line 103
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/Order;->carton:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 122
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/solidshop/Order;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 123
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/solidshop/Order;

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/client/solidshop/Order;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/solidshop/Order;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/solidshop/Order;->number:Ljava/lang/String;

    .line 125
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->placed_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/solidshop/Order;->placed_at:Lcom/squareup/protos/common/time/DateTime;

    .line 126
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->total:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/solidshop/Order;->total:Lcom/squareup/protos/common/Money;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->item_count:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/solidshop/Order;->item_count:Ljava/lang/Integer;

    .line 128
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->user_facing_status:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    iget-object v3, p1, Lcom/squareup/protos/client/solidshop/Order;->user_facing_status:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 129
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->carton:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/solidshop/Order;->carton:Ljava/util/List;

    .line 130
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 135
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 137
    invoke-virtual {p0}, Lcom/squareup/protos/client/solidshop/Order;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->number:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->placed_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->total:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->item_count:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->user_facing_status:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->carton:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 144
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/solidshop/Order$Builder;
    .locals 2

    .line 108
    new-instance v0, Lcom/squareup/protos/client/solidshop/Order$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/solidshop/Order$Builder;-><init>()V

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/solidshop/Order$Builder;->number:Ljava/lang/String;

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->placed_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/solidshop/Order$Builder;->placed_at:Lcom/squareup/protos/common/time/DateTime;

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->total:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/solidshop/Order$Builder;->total:Lcom/squareup/protos/common/Money;

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->item_count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/solidshop/Order$Builder;->item_count:Ljava/lang/Integer;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->user_facing_status:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    iput-object v1, v0, Lcom/squareup/protos/client/solidshop/Order$Builder;->user_facing_status:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->carton:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/solidshop/Order$Builder;->carton:Ljava/util/List;

    .line 115
    invoke-virtual {p0}, Lcom/squareup/protos/client/solidshop/Order;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/solidshop/Order$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/solidshop/Order;->newBuilder()Lcom/squareup/protos/client/solidshop/Order$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->number:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->placed_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    const-string v1, ", placed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->placed_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 154
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->total:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", total="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->total:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 155
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->item_count:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", item_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->item_count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 156
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->user_facing_status:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    if-eqz v1, :cond_4

    const-string v1, ", user_facing_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->user_facing_status:Lcom/squareup/protos/client/solidshop/Order$UserFacingStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 157
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->carton:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", carton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/Order;->carton:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Order{"

    .line 158
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
