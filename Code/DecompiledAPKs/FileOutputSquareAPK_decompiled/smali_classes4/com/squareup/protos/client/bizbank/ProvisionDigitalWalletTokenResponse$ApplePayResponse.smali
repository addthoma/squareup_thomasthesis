.class public final Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;
.super Lcom/squareup/wire/Message;
.source "ProvisionDigitalWalletTokenResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ApplePayResponse"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$ProtoAdapter_ApplePayResponse;,
        Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACTIVATION_DATA:Lokio/ByteString;

.field public static final DEFAULT_ENCRYPTED_PASS_DATA:Lokio/ByteString;

.field public static final DEFAULT_EPHEMERAL_PUBLIC_KEY:Lokio/ByteString;

.field private static final serialVersionUID:J


# instance fields
.field public final activation_data:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final encrypted_pass_data:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final ephemeral_public_key:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        redacted = true
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 136
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$ProtoAdapter_ApplePayResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$ProtoAdapter_ApplePayResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 140
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->DEFAULT_ENCRYPTED_PASS_DATA:Lokio/ByteString;

    .line 142
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->DEFAULT_ACTIVATION_DATA:Lokio/ByteString;

    .line 144
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->DEFAULT_EPHEMERAL_PUBLIC_KEY:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V
    .locals 1

    .line 178
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;-><init>(Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V
    .locals 1

    .line 183
    sget-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->encrypted_pass_data:Lokio/ByteString;

    .line 185
    iput-object p2, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->activation_data:Lokio/ByteString;

    .line 186
    iput-object p3, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->ephemeral_public_key:Lokio/ByteString;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 202
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 203
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    .line 204
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->encrypted_pass_data:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->encrypted_pass_data:Lokio/ByteString;

    .line 205
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->activation_data:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->activation_data:Lokio/ByteString;

    .line 206
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->ephemeral_public_key:Lokio/ByteString;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->ephemeral_public_key:Lokio/ByteString;

    .line 207
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 212
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 214
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 215
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->encrypted_pass_data:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->activation_data:Lokio/ByteString;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 217
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->ephemeral_public_key:Lokio/ByteString;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 218
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;
    .locals 2

    .line 191
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;-><init>()V

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->encrypted_pass_data:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->encrypted_pass_data:Lokio/ByteString;

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->activation_data:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->activation_data:Lokio/ByteString;

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->ephemeral_public_key:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->ephemeral_public_key:Lokio/ByteString;

    .line 195
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 135
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->newBuilder()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 225
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 226
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->encrypted_pass_data:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", encrypted_pass_data=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->activation_data:Lokio/ByteString;

    if-eqz v1, :cond_1

    const-string v1, ", activation_data=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->ephemeral_public_key:Lokio/ByteString;

    if-eqz v1, :cond_2

    const-string v1, ", ephemeral_public_key=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ApplePayResponse{"

    .line 229
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
