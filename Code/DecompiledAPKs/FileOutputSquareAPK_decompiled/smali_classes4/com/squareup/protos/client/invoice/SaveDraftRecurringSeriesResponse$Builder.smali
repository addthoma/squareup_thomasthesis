.class public final Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SaveDraftRecurringSeriesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;",
        "Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public recurring_invoice:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 93
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;
    .locals 4

    .line 108
    new-instance v0, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse$Builder;->recurring_invoice:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse$Builder;->build()Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;

    move-result-object v0

    return-object v0
.end method

.method public recurring_invoice(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse$Builder;
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse$Builder;->recurring_invoice:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse$Builder;
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
