.class public final Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EstimateDisplayDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;",
        "Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public accepted_at:Lcom/squareup/protos/client/ISO8601Date;

.field public canceled_at:Lcom/squareup/protos/client/ISO8601Date;

.field public created_at:Lcom/squareup/protos/client/ISO8601Date;

.field public delivered_at:Lcom/squareup/protos/client/ISO8601Date;

.field public display_state:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

.field public estimate:Lcom/squareup/protos/client/estimate/Estimate;

.field public estimate_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

.field public event:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceEvent;",
            ">;"
        }
    .end annotation
.end field

.field public is_archived:Ljava/lang/Boolean;

.field public linked_invoice_token:Ljava/lang/String;

.field public selected_package_token:Ljava/lang/String;

.field public sort_date:Lcom/squareup/protos/client/ISO8601Date;

.field public updated_at:Lcom/squareup/protos/client/ISO8601Date;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 300
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 301
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->event:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public accepted_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;
    .locals 0

    .line 348
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->accepted_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;
    .locals 17

    move-object/from16 v0, p0

    .line 411
    new-instance v16, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    iget-object v2, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->estimate:Lcom/squareup/protos/client/estimate/Estimate;

    iget-object v3, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v4, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v5, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v6, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->delivered_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v7, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->accepted_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v8, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->display_state:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    iget-object v9, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->linked_invoice_token:Ljava/lang/String;

    iget-object v10, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v11, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->estimate_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    iget-object v12, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->event:Ljava/util/List;

    iget-object v13, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->is_archived:Ljava/lang/Boolean;

    iget-object v14, v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->selected_package_token:Ljava/lang/String;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v15

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;-><init>(Lcom/squareup/protos/client/estimate/Estimate;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/invoice/InvoiceTimeline;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-object v16
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 273
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->build()Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;

    move-result-object v0

    return-object v0
.end method

.method public canceled_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;
    .locals 0

    .line 372
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;
    .locals 0

    .line 324
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public delivered_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;
    .locals 0

    .line 340
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->delivered_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public display_state(Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;
    .locals 0

    .line 356
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->display_state:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    return-object p0
.end method

.method public estimate(Lcom/squareup/protos/client/estimate/Estimate;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;
    .locals 0

    .line 308
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->estimate:Lcom/squareup/protos/client/estimate/Estimate;

    return-object p0
.end method

.method public estimate_timeline(Lcom/squareup/protos/client/invoice/InvoiceTimeline;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;
    .locals 0

    .line 380
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->estimate_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    return-object p0
.end method

.method public event(Ljava/util/List;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceEvent;",
            ">;)",
            "Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;"
        }
    .end annotation

    .line 388
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 389
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->event:Ljava/util/List;

    return-object p0
.end method

.method public is_archived(Ljava/lang/Boolean;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;
    .locals 0

    .line 397
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->is_archived:Ljava/lang/Boolean;

    return-object p0
.end method

.method public linked_invoice_token(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;
    .locals 0

    .line 364
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->linked_invoice_token:Ljava/lang/String;

    return-object p0
.end method

.method public selected_package_token(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;
    .locals 0

    .line 405
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->selected_package_token:Ljava/lang/String;

    return-object p0
.end method

.method public sort_date(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;
    .locals 0

    .line 316
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public updated_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;
    .locals 0

    .line 332
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method
