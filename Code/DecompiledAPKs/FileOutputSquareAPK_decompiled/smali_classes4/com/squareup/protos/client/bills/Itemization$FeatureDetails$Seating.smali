.class public final Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;
.super Lcom/squareup/wire/Message;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Seating"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$ProtoAdapter_Seating;,
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;,
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$FeatureDetails$Seating$Destination#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 4300
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$ProtoAdapter_Seating;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$ProtoAdapter_Seating;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;)V
    .locals 1

    .line 4311
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;-><init>(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;Lokio/ByteString;)V
    .locals 1

    .line 4315
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 4316
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 4330
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 4331
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    .line 4332
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    .line 4333
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 4338
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 4340
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 4341
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 4342
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;
    .locals 2

    .line 4321
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;-><init>()V

    .line 4322
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;->destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    .line 4323
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 4299
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 4349
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4350
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    if-eqz v1, :cond_0

    const-string v1, ", destination="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->destination:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Seating{"

    .line 4351
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
