.class public final Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;
.super Lcom/squareup/wire/Message;
.source "ListRecurringSeriesRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$ProtoAdapter_ListRecurringSeriesRequest;,
        Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;,
        Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;",
        "Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LIMIT:Ljava/lang/Integer;

.field public static final DEFAULT_PAGING_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_QUERY:Ljava/lang/String; = ""

.field public static final DEFAULT_STATE_FILTER:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

.field private static final serialVersionUID:J


# instance fields
.field public final date_range:Lcom/squareup/protos/common/time/DateTimeInterval;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTimeInterval#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final limit:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final paging_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final query:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final state_filter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.ListRecurringSeriesRequest$StateFilter#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$ProtoAdapter_ListRecurringSeriesRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$ProtoAdapter_ListRecurringSeriesRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/16 v0, 0xa

    .line 30
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->DEFAULT_LIMIT:Ljava/lang/Integer;

    .line 32
    sget-object v0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;->UNKNOWN:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    sput-object v0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->DEFAULT_STATE_FILTER:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;Lcom/squareup/protos/common/time/DateTimeInterval;Ljava/lang/String;)V
    .locals 7

    .line 81
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;-><init>(Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;Lcom/squareup/protos/common/time/DateTimeInterval;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;Lcom/squareup/protos/common/time/DateTimeInterval;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 86
    sget-object v0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 87
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->paging_key:Ljava/lang/String;

    .line 88
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->limit:Ljava/lang/Integer;

    .line 89
    iput-object p3, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->state_filter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    .line 90
    iput-object p4, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 91
    iput-object p5, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->query:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 109
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 110
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->paging_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->paging_key:Ljava/lang/String;

    .line 112
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->limit:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->limit:Ljava/lang/Integer;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->state_filter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->state_filter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->query:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->query:Ljava/lang/String;

    .line 116
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 121
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 123
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->paging_key:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->limit:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->state_filter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTimeInterval;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->query:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 129
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;
    .locals 2

    .line 96
    new-instance v0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;-><init>()V

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->paging_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->paging_key:Ljava/lang/String;

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->limit:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->limit:Ljava/lang/Integer;

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->state_filter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->state_filter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->query:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->query:Ljava/lang/String;

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->newBuilder()Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->paging_key:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", paging_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->paging_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->limit:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->limit:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 139
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->state_filter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    if-eqz v1, :cond_2

    const-string v1, ", state_filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->state_filter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 140
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v1, :cond_3

    const-string v1, ", date_range="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 141
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->query:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ListRecurringSeriesRequest{"

    .line 142
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
