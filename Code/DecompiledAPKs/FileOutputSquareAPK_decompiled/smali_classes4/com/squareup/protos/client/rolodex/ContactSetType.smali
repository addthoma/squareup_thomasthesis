.class public final enum Lcom/squareup/protos/client/rolodex/ContactSetType;
.super Ljava/lang/Enum;
.source "ContactSetType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/ContactSetType$ProtoAdapter_ContactSetType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/rolodex/ContactSetType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/rolodex/ContactSetType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/ContactSetType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ALL_CONTACTS:Lcom/squareup/protos/client/rolodex/ContactSetType;

.field public static final enum GROUP:Lcom/squareup/protos/client/rolodex/ContactSetType;

.field public static final enum INCLUDED_CONTACTS:Lcom/squareup/protos/client/rolodex/ContactSetType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 11
    new-instance v0, Lcom/squareup/protos/client/rolodex/ContactSetType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "ALL_CONTACTS"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/rolodex/ContactSetType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/ContactSetType;->ALL_CONTACTS:Lcom/squareup/protos/client/rolodex/ContactSetType;

    .line 13
    new-instance v0, Lcom/squareup/protos/client/rolodex/ContactSetType;

    const/4 v3, 0x2

    const-string v4, "INCLUDED_CONTACTS"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/rolodex/ContactSetType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/ContactSetType;->INCLUDED_CONTACTS:Lcom/squareup/protos/client/rolodex/ContactSetType;

    .line 15
    new-instance v0, Lcom/squareup/protos/client/rolodex/ContactSetType;

    const/4 v4, 0x3

    const-string v5, "GROUP"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/client/rolodex/ContactSetType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/ContactSetType;->GROUP:Lcom/squareup/protos/client/rolodex/ContactSetType;

    new-array v0, v4, [Lcom/squareup/protos/client/rolodex/ContactSetType;

    .line 10
    sget-object v4, Lcom/squareup/protos/client/rolodex/ContactSetType;->ALL_CONTACTS:Lcom/squareup/protos/client/rolodex/ContactSetType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/ContactSetType;->INCLUDED_CONTACTS:Lcom/squareup/protos/client/rolodex/ContactSetType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/rolodex/ContactSetType;->GROUP:Lcom/squareup/protos/client/rolodex/ContactSetType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/rolodex/ContactSetType;->$VALUES:[Lcom/squareup/protos/client/rolodex/ContactSetType;

    .line 17
    new-instance v0, Lcom/squareup/protos/client/rolodex/ContactSetType$ProtoAdapter_ContactSetType;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ContactSetType$ProtoAdapter_ContactSetType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/ContactSetType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput p3, p0, Lcom/squareup/protos/client/rolodex/ContactSetType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/rolodex/ContactSetType;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 32
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/rolodex/ContactSetType;->GROUP:Lcom/squareup/protos/client/rolodex/ContactSetType;

    return-object p0

    .line 31
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/rolodex/ContactSetType;->INCLUDED_CONTACTS:Lcom/squareup/protos/client/rolodex/ContactSetType;

    return-object p0

    .line 30
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/rolodex/ContactSetType;->ALL_CONTACTS:Lcom/squareup/protos/client/rolodex/ContactSetType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/ContactSetType;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/client/rolodex/ContactSetType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/rolodex/ContactSetType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/rolodex/ContactSetType;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/client/rolodex/ContactSetType;->$VALUES:[Lcom/squareup/protos/client/rolodex/ContactSetType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/rolodex/ContactSetType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/rolodex/ContactSetType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/squareup/protos/client/rolodex/ContactSetType;->value:I

    return v0
.end method
