.class public final Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
        "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public discount_line_item:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public fee_line_item:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public return_itemization:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
            ">;"
        }
    .end annotation
.end field

.field public return_surcharge_line_item:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ReturnSurchargeLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public return_tip_line_item:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ReturnTipLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

.field public source_bill_server_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 8088
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 8089
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_itemization:Ljava/util/List;

    .line 8090
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->discount_line_item:Ljava/util/List;

    .line 8091
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->fee_line_item:Ljava/util/List;

    .line 8092
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_tip_line_item:Ljava/util/List;

    .line 8093
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_surcharge_line_item:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;
    .locals 10

    .line 8160
    new-instance v9, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->source_bill_server_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_itemization:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->discount_line_item:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->fee_line_item:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_tip_line_item:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_surcharge_line_item:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 8073
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->build()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    move-result-object v0

    return-object v0
.end method

.method public discount_line_item(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;"
        }
    .end annotation

    .line 8117
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 8118
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->discount_line_item:Ljava/util/List;

    return-object p0
.end method

.method public fee_line_item(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;"
        }
    .end annotation

    .line 8126
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 8127
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->fee_line_item:Ljava/util/List;

    return-object p0
.end method

.method public return_itemization(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;"
        }
    .end annotation

    .line 8108
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 8109
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_itemization:Ljava/util/List;

    return-object p0
.end method

.method public return_surcharge_line_item(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ReturnSurchargeLineItem;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;"
        }
    .end annotation

    .line 8153
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 8154
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_surcharge_line_item:Ljava/util/List;

    return-object p0
.end method

.method public return_tip_line_item(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ReturnTipLineItem;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;"
        }
    .end annotation

    .line 8143
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 8144
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_tip_line_item:Ljava/util/List;

    return-object p0
.end method

.method public rounding_adjustment(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;
    .locals 0

    .line 8135
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    return-object p0
.end method

.method public source_bill_server_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;
    .locals 0

    .line 8100
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->source_bill_server_id:Ljava/lang/String;

    return-object p0
.end method
