.class public final Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FeeLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;",
        "Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public calculation_phase:Lcom/squareup/api/items/CalculationPhase;

.field public cogs_object_id:Ljava/lang/String;

.field public inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

.field public name:Ljava/lang/String;

.field public percentage:Ljava/lang/String;

.field public tax_label_order:Ljava/lang/Integer;

.field public tax_type_id:Ljava/lang/String;

.field public tax_type_name:Ljava/lang/String;

.field public tax_type_number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 664
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;
    .locals 12

    .line 730
    new-instance v11, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->percentage:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->tax_type_name:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->tax_type_number:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->tax_type_id:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->cogs_object_id:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->tax_label_order:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/CalculationPhase;Lcom/squareup/api/items/Fee$InclusionType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 645
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    move-result-object v0

    return-object v0
.end method

.method public calculation_phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;
    .locals 0

    .line 678
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    return-object p0
.end method

.method public cogs_object_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;
    .locals 0

    .line 715
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->cogs_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public inclusion_type(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;
    .locals 0

    .line 683
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;
    .locals 0

    .line 668
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/String;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;
    .locals 0

    .line 673
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->percentage:Ljava/lang/String;

    return-object p0
.end method

.method public tax_label_order(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;
    .locals 0

    .line 724
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->tax_label_order:Ljava/lang/Integer;

    return-object p0
.end method

.method public tax_type_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;
    .locals 0

    .line 707
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->tax_type_id:Ljava/lang/String;

    return-object p0
.end method

.method public tax_type_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;
    .locals 0

    .line 691
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->tax_type_name:Ljava/lang/String;

    return-object p0
.end method

.method public tax_type_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;
    .locals 0

    .line 699
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails$Builder;->tax_type_number:Ljava/lang/String;

    return-object p0
.end method
