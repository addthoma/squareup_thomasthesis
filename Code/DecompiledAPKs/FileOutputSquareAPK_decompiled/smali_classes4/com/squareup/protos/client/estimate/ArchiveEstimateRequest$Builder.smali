.class public final Lcom/squareup/protos/client/estimate/ArchiveEstimateRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ArchiveEstimateRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/ArchiveEstimateRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/estimate/ArchiveEstimateRequest;",
        "Lcom/squareup/protos/client/estimate/ArchiveEstimateRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id_pair:Lcom/squareup/protos/client/IdPair;

.field public version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 94
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/estimate/ArchiveEstimateRequest;
    .locals 4

    .line 109
    new-instance v0, Lcom/squareup/protos/client/estimate/ArchiveEstimateRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ArchiveEstimateRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/estimate/ArchiveEstimateRequest$Builder;->version:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/estimate/ArchiveEstimateRequest;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/ArchiveEstimateRequest$Builder;->build()Lcom/squareup/protos/client/estimate/ArchiveEstimateRequest;

    move-result-object v0

    return-object v0
.end method

.method public id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/estimate/ArchiveEstimateRequest$Builder;
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/ArchiveEstimateRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public version(Ljava/lang/Integer;)Lcom/squareup/protos/client/estimate/ArchiveEstimateRequest$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/ArchiveEstimateRequest$Builder;->version:Ljava/lang/Integer;

    return-object p0
.end method
