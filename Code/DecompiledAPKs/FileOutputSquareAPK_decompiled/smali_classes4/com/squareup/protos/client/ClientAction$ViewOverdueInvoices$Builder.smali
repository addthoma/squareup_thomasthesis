.class public final Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ClientAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;",
        "Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public invoice_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 3788
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 3789
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices$Builder;->invoice_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;
    .locals 3

    .line 3803
    new-instance v0, Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices$Builder;->invoice_ids:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 3785
    invoke-virtual {p0}, Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    move-result-object v0

    return-object v0
.end method

.method public invoice_ids(Ljava/util/List;)Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices$Builder;"
        }
    .end annotation

    .line 3796
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 3797
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices$Builder;->invoice_ids:Ljava/util/List;

    return-object p0
.end method
