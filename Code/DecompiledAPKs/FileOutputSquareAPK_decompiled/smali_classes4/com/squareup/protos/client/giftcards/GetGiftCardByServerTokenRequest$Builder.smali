.class public final Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetGiftCardByServerTokenRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;",
        "Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public gift_card_server_token:Ljava/lang/String;

.field public merchant_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;
    .locals 4

    .line 118
    new-instance v0, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;->gift_card_server_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;

    move-result-object v0

    return-object v0
.end method

.method public gift_card_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;->gift_card_server_token:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method
