.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplayDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$ProtoAdapter_DisplayDetails;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DISPLAY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_EMAIL:Ljava/lang/String; = ""

.field public static final DEFAULT_LOYALTY_ACCOUNT_PHONE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_PHONE_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_PHONE_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final display_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final email:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final last_visit:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final loyalty_account_phone_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final phone_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final phone_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 2805
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$ProtoAdapter_DisplayDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$ProtoAdapter_DisplayDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 2880
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 2886
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 2887
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->display_name:Ljava/lang/String;

    .line 2888
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->last_visit:Lcom/squareup/protos/client/ISO8601Date;

    .line 2889
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->email:Ljava/lang/String;

    .line 2890
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_number:Ljava/lang/String;

    .line 2891
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_token:Ljava/lang/String;

    .line 2892
    iput-object p6, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->loyalty_account_phone_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 2911
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2912
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    .line 2913
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->display_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->display_name:Ljava/lang/String;

    .line 2914
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->last_visit:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->last_visit:Lcom/squareup/protos/client/ISO8601Date;

    .line 2915
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->email:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->email:Ljava/lang/String;

    .line 2916
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_number:Ljava/lang/String;

    .line 2917
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_token:Ljava/lang/String;

    .line 2918
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->loyalty_account_phone_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->loyalty_account_phone_token:Ljava/lang/String;

    .line 2919
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 2924
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 2926
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 2927
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->display_name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2928
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->last_visit:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2929
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->email:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2930
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_number:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2931
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2932
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->loyalty_account_phone_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 2933
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;
    .locals 2

    .line 2897
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;-><init>()V

    .line 2898
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->display_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->display_name:Ljava/lang/String;

    .line 2899
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->last_visit:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->last_visit:Lcom/squareup/protos/client/ISO8601Date;

    .line 2900
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->email:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->email:Ljava/lang/String;

    .line 2901
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->phone_number:Ljava/lang/String;

    .line 2902
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->phone_token:Ljava/lang/String;

    .line 2903
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->loyalty_account_phone_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->loyalty_account_phone_token:Ljava/lang/String;

    .line 2904
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 2804
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 2940
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2941
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", display_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2942
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->last_visit:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    const-string v1, ", last_visit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->last_visit:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2943
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->email:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", email=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2944
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_number:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", phone_number=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2945
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", phone_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->phone_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2946
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->loyalty_account_phone_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", loyalty_account_phone_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;->loyalty_account_phone_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DisplayDetails{"

    .line 2947
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
