.class final Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$ProtoAdapter_GetRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 96
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 113
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$Builder;-><init>()V

    .line 114
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 115
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 119
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 117
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$Builder;->applied_server_version(Ljava/lang/Long;)Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$Builder;

    goto :goto_0

    .line 123
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 124
    invoke-virtual {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 94
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$ProtoAdapter_GetRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 107
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;->applied_server_version:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 108
    invoke-virtual {p2}, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 94
    check-cast p2, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$ProtoAdapter_GetRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;)I
    .locals 3

    .line 101
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;->applied_server_version:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 102
    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 94
    check-cast p1, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$ProtoAdapter_GetRequest;->encodedSize(Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;)Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;
    .locals 0

    .line 129
    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;->newBuilder()Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$Builder;

    move-result-object p1

    .line 130
    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 131
    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 94
    check-cast p1, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$ProtoAdapter_GetRequest;->redact(Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;)Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    move-result-object p1

    return-object p1
.end method
