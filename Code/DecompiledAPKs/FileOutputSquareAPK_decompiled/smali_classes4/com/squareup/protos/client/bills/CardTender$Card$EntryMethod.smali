.class public final enum Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
.super Ljava/lang/Enum;
.source "CardTender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardTender$Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EntryMethod"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod$ProtoAdapter_EntryMethod;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public static final enum EMV:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public static final enum KEYED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public static final enum ON_FILE:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public static final enum SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public static final enum UNKNOWN_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 559
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_ENTRY_METHOD"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->UNKNOWN_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 564
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v2, 0x1

    const-string v3, "SWIPED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 569
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v3, 0x2

    const-string v4, "KEYED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->KEYED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 574
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v4, 0x3

    const-string v5, "EMV"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->EMV:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 579
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v5, 0x4

    const-string v6, "CONTACTLESS"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 584
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v6, 0x5

    const-string v7, "ON_FILE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ON_FILE:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 558
    sget-object v7, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->UNKNOWN_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->KEYED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->EMV:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ON_FILE:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->$VALUES:[Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 586
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod$ProtoAdapter_EntryMethod;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod$ProtoAdapter_EntryMethod;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 590
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 591
    iput p3, p0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 604
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ON_FILE:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0

    .line 603
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0

    .line 602
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->EMV:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0

    .line 601
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->KEYED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0

    .line 600
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0

    .line 599
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->UNKNOWN_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 1

    .line 558
    const-class v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 1

    .line 558
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->$VALUES:[Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 611
    iget v0, p0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->value:I

    return v0
.end method
