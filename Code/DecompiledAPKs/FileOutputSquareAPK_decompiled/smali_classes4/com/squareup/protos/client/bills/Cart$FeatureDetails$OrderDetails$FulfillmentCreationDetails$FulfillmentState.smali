.class public final enum Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;
.super Ljava/lang/Enum;
.source "Cart.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FulfillmentState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState$ProtoAdapter_FulfillmentState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum COMPLETED:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

.field public static final enum FULFILLMENT_STATE_DO_NOT_USE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

.field public static final enum NEW:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 7514
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    const/4 v1, 0x0

    const-string v2, "FULFILLMENT_STATE_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->FULFILLMENT_STATE_DO_NOT_USE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    .line 7516
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    const/4 v2, 0x1

    const-string v3, "NEW"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->NEW:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    .line 7518
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    const/4 v3, 0x2

    const-string v4, "COMPLETED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->COMPLETED:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    .line 7513
    sget-object v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->FULFILLMENT_STATE_DO_NOT_USE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->NEW:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->COMPLETED:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->$VALUES:[Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    .line 7520
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState$ProtoAdapter_FulfillmentState;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState$ProtoAdapter_FulfillmentState;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 7524
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 7525
    iput p3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 7535
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->COMPLETED:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    return-object p0

    .line 7534
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->NEW:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    return-object p0

    .line 7533
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->FULFILLMENT_STATE_DO_NOT_USE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;
    .locals 1

    .line 7513
    const-class v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;
    .locals 1

    .line 7513
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->$VALUES:[Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 7542
    iget v0, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails$FulfillmentCreationDetails$FulfillmentState;->value:I

    return v0
.end method
