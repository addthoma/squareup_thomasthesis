.class public final Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AccumulateLoyaltyStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;",
        "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_id_pair:Lcom/squareup/protos/client/IdPair;

.field public cart:Lcom/squareup/protos/client/bills/Cart;

.field public email_address:Ljava/lang/String;

.field public email_address_token:Ljava/lang/String;

.field public enroll_intent:Ljava/lang/Boolean;

.field public expected_point_accrual:Ljava/lang/Long;

.field public newly_accepted_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

.field public phone_number:Ljava/lang/String;

.field public phone_number_token:Ljava/lang/String;

.field public reason_for_point_accrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

.field public tender_id_pair:Lcom/squareup/protos/client/IdPair;

.field public tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

.field public transfer_from_email_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 312
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
    .locals 0

    .line 382
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;
    .locals 17

    move-object/from16 v0, p0

    .line 425
    new-instance v16, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;

    iget-object v2, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v4, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->transfer_from_email_token:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->enroll_intent:Ljava/lang/Boolean;

    iget-object v6, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    iget-object v7, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->expected_point_accrual:Ljava/lang/Long;

    iget-object v8, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->reason_for_point_accrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    iget-object v9, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v10, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->newly_accepted_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    iget-object v11, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->phone_number:Ljava/lang/String;

    iget-object v12, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->phone_number_token:Ljava/lang/String;

    iget-object v13, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->email_address:Ljava/lang/String;

    iget-object v14, v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->email_address_token:Ljava/lang/String;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v15

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/Tender$Type;Ljava/lang/Long;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v16
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 285
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;

    move-result-object v0

    return-object v0
.end method

.method public cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
    .locals 0

    .line 329
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object p0
.end method

.method public email_address(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
    .locals 0

    .line 412
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->email_address:Ljava/lang/String;

    const/4 p1, 0x0

    .line 413
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->email_address_token:Ljava/lang/String;

    return-object p0
.end method

.method public email_address_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
    .locals 0

    .line 418
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->email_address_token:Ljava/lang/String;

    const/4 p1, 0x0

    .line 419
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->email_address:Ljava/lang/String;

    return-object p0
.end method

.method public enroll_intent(Ljava/lang/Boolean;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
    .locals 0

    .line 347
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->enroll_intent:Ljava/lang/Boolean;

    return-object p0
.end method

.method public expected_point_accrual(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
    .locals 0

    .line 364
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->expected_point_accrual:Ljava/lang/Long;

    return-object p0
.end method

.method public newly_accepted_tos(Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
    .locals 0

    .line 392
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->newly_accepted_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    return-object p0
.end method

.method public phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
    .locals 0

    .line 397
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->phone_number:Ljava/lang/String;

    const/4 p1, 0x0

    .line 398
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->phone_number_token:Ljava/lang/String;

    return-object p0
.end method

.method public phone_number_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
    .locals 0

    .line 403
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->phone_number_token:Ljava/lang/String;

    const/4 p1, 0x0

    .line 404
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public reason_for_point_accrual(Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
    .locals 0

    .line 373
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->reason_for_point_accrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    return-object p0
.end method

.method public tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
    .locals 0

    .line 320
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public tender_type(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
    .locals 0

    .line 356
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object p0
.end method

.method public transfer_from_email_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 338
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->transfer_from_email_token:Ljava/lang/String;

    return-object p0
.end method
