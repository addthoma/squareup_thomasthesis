.class final Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$ProtoAdapter_UpdateLoyaltyAccountMappingResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "UpdateLoyaltyAccountMappingResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_UpdateLoyaltyAccountMappingResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 192
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 216
    new-instance v0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;-><init>()V

    .line 217
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 218
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 232
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 230
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->conflicting_contacts:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 229
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->errors:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 223
    :cond_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->status(Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;)Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 225
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 220
    :cond_3
    sget-object v3, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->loyalty_account_mapping(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;

    goto :goto_0

    .line 236
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 237
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 190
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$ProtoAdapter_UpdateLoyaltyAccountMappingResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 207
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 208
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 209
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->errors:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 210
    sget-object v0, Lcom/squareup/protos/client/rolodex/Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->conflicting_contacts:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 211
    invoke-virtual {p2}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 190
    check-cast p2, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$ProtoAdapter_UpdateLoyaltyAccountMappingResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;)I
    .locals 4

    .line 197
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    const/4 v3, 0x2

    .line 198
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 199
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->errors:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 200
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->conflicting_contacts:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 201
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 190
    check-cast p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$ProtoAdapter_UpdateLoyaltyAccountMappingResponse;->encodedSize(Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;)Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;
    .locals 2

    .line 242
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->newBuilder()Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;

    move-result-object p1

    .line 243
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    .line 244
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->errors:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 245
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->conflicting_contacts:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 246
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 247
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 190
    check-cast p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$ProtoAdapter_UpdateLoyaltyAccountMappingResponse;->redact(Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;)Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;

    move-result-object p1

    return-object p1
.end method
