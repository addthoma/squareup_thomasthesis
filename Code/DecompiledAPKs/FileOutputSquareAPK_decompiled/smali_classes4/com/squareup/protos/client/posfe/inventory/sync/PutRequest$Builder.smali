.class public final Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PutRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;",
        "Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public employee_token:Ljava/lang/String;

.field public idempotency_token:Ljava/lang/String;

.field public object:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 118
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 119
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest$Builder;->object:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;
    .locals 5

    .line 148
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest$Builder;->idempotency_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest$Builder;->object:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest$Builder;->employee_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    move-result-object v0

    return-object v0
.end method

.method public employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest$Builder;->employee_token:Ljava/lang/String;

    return-object p0
.end method

.method public idempotency_token(Ljava/lang/String;)Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest$Builder;->idempotency_token:Ljava/lang/String;

    return-object p0
.end method

.method public object(Ljava/util/List;)Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;",
            ">;)",
            "Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest$Builder;"
        }
    .end annotation

    .line 131
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest$Builder;->object:Ljava/util/List;

    return-object p0
.end method
