.class public final Lcom/squareup/protos/client/rolodex/GroupV2Counts$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupV2Counts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/GroupV2Counts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/GroupV2Counts;",
        "Lcom/squareup/protos/client/rolodex/GroupV2Counts$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public num_customers:Ljava/lang/Integer;

.field public num_emailable:Ljava/lang/Integer;

.field public num_manual:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 120
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/GroupV2Counts;
    .locals 5

    .line 149
    new-instance v0, Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2Counts$Builder;->num_customers:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/GroupV2Counts$Builder;->num_emailable:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/GroupV2Counts$Builder;->num_manual:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/rolodex/GroupV2Counts;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 113
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GroupV2Counts$Builder;->build()Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    move-result-object v0

    return-object v0
.end method

.method public num_customers(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/GroupV2Counts$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GroupV2Counts$Builder;->num_customers:Ljava/lang/Integer;

    return-object p0
.end method

.method public num_emailable(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/GroupV2Counts$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GroupV2Counts$Builder;->num_emailable:Ljava/lang/Integer;

    return-object p0
.end method

.method public num_manual(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/GroupV2Counts$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GroupV2Counts$Builder;->num_manual:Ljava/lang/Integer;

    return-object p0
.end method
