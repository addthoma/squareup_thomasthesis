.class public final Lcom/squareup/protos/client/invoice/InvoiceTimeline$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InvoiceTimeline.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceTimeline;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/InvoiceTimeline;",
        "Lcom/squareup/protos/client/invoice/InvoiceTimeline$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public call_to_action:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

.field public primary_status:Ljava/lang/String;

.field public secondary_status:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/InvoiceTimeline;
    .locals 5

    .line 131
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$Builder;->primary_status:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$Builder;->secondary_status:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$Builder;->call_to_action:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/invoice/InvoiceTimeline;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    move-result-object v0

    return-object v0
.end method

.method public call_to_action(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$Builder;->call_to_action:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    return-object p0
.end method

.method public primary_status(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$Builder;->primary_status:Ljava/lang/String;

    return-object p0
.end method

.method public secondary_status(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$Builder;->secondary_status:Ljava/lang/String;

    return-object p0
.end method
