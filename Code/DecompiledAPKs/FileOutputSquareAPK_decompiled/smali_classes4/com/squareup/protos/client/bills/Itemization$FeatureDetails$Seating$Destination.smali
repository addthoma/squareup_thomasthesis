.class public final Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;
.super Lcom/squareup/wire/Message;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Destination"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$ProtoAdapter_Destination;,
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;,
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final created_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final seat_id_pair:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;"
        }
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$FeatureDetails$Seating$Destination$Type#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 4372
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$ProtoAdapter_Destination;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$ProtoAdapter_Destination;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 4376
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;->UNKNOWN:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->DEFAULT_TYPE:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;Ljava/util/List;Lcom/squareup/protos/client/ISO8601Date;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            ")V"
        }
    .end annotation

    .line 4405
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;-><init>(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;Ljava/util/List;Lcom/squareup/protos/client/ISO8601Date;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;Ljava/util/List;Lcom/squareup/protos/client/ISO8601Date;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 4410
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 4411
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->type:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;

    const-string p1, "seat_id_pair"

    .line 4412
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->seat_id_pair:Ljava/util/List;

    .line 4413
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 4429
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 4430
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;

    .line 4431
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->type:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->type:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;

    .line 4432
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->seat_id_pair:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->seat_id_pair:Ljava/util/List;

    .line 4433
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 4434
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 4439
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 4441
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 4442
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->type:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4443
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->seat_id_pair:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4444
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 4445
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;
    .locals 2

    .line 4418
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;-><init>()V

    .line 4419
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->type:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->type:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;

    .line 4420
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->seat_id_pair:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->seat_id_pair:Ljava/util/List;

    .line 4421
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 4422
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 4371
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 4452
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4453
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->type:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->type:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4454
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->seat_id_pair:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", seat_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->seat_id_pair:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4455
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating$Destination;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Destination{"

    .line 4456
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
