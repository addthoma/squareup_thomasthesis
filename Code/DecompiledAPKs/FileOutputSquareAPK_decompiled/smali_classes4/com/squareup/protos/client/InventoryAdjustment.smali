.class public final Lcom/squareup/protos/client/InventoryAdjustment;
.super Lcom/squareup/wire/Message;
.source "InventoryAdjustment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/InventoryAdjustment$ProtoAdapter_InventoryAdjustment;,
        Lcom/squareup/protos/client/InventoryAdjustment$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/InventoryAdjustment;",
        "Lcom/squareup/protos/client/InventoryAdjustment$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/InventoryAdjustment;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ADJUST_COUNT:Ljava/lang/Long;

.field public static final DEFAULT_ADJUST_QUANTITY_DECIMAL:Ljava/lang/String; = ""

.field public static final DEFAULT_BILL_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_CATALOG_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_REASON:Lcom/squareup/protos/client/InventoryAdjustmentReason;

.field public static final DEFAULT_REFUND_BILL_SERVER_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_UNIT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_VARIATION_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final adjust_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x3
    .end annotation
.end field

.field public final adjust_quantity_decimal:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xe
    .end annotation
.end field

.field public final bill_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final catalog_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0xc
    .end annotation
.end field

.field public final cost_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final reason:Lcom/squareup/protos/client/InventoryAdjustmentReason;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.InventoryAdjustmentReason#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final refund_bill_server_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final variation_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Lcom/squareup/protos/client/InventoryAdjustment$ProtoAdapter_InventoryAdjustment;

    invoke-direct {v0}, Lcom/squareup/protos/client/InventoryAdjustment$ProtoAdapter_InventoryAdjustment;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/InventoryAdjustment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 33
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/InventoryAdjustment;->DEFAULT_ADJUST_COUNT:Ljava/lang/Long;

    .line 35
    sget-object v1, Lcom/squareup/protos/client/InventoryAdjustmentReason;->DO_NOT_USE_REASON:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    sput-object v1, Lcom/squareup/protos/client/InventoryAdjustment;->DEFAULT_REASON:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    .line 41
    sput-object v0, Lcom/squareup/protos/client/InventoryAdjustment;->DEFAULT_CATALOG_VERSION:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lcom/squareup/protos/client/InventoryAdjustmentReason;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 11

    .line 134
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/InventoryAdjustment;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lcom/squareup/protos/client/InventoryAdjustmentReason;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lcom/squareup/protos/client/InventoryAdjustmentReason;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 141
    sget-object v0, Lcom/squareup/protos/client/InventoryAdjustment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->variation_token:Ljava/lang/String;

    .line 143
    iput-object p2, p0, Lcom/squareup/protos/client/InventoryAdjustment;->unit_token:Ljava/lang/String;

    .line 144
    iput-object p3, p0, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_count:Ljava/lang/Long;

    .line 145
    iput-object p4, p0, Lcom/squareup/protos/client/InventoryAdjustment;->reason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    .line 146
    iput-object p5, p0, Lcom/squareup/protos/client/InventoryAdjustment;->cost_money:Lcom/squareup/protos/common/Money;

    .line 147
    iput-object p6, p0, Lcom/squareup/protos/client/InventoryAdjustment;->refund_bill_server_token:Ljava/lang/String;

    .line 148
    iput-object p7, p0, Lcom/squareup/protos/client/InventoryAdjustment;->bill_token:Ljava/lang/String;

    .line 149
    iput-object p8, p0, Lcom/squareup/protos/client/InventoryAdjustment;->catalog_version:Ljava/lang/Long;

    .line 150
    iput-object p9, p0, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_quantity_decimal:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 172
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/InventoryAdjustment;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 173
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/InventoryAdjustment;

    .line 174
    invoke-virtual {p0}, Lcom/squareup/protos/client/InventoryAdjustment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/InventoryAdjustment;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->variation_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/InventoryAdjustment;->variation_token:Ljava/lang/String;

    .line 175
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/InventoryAdjustment;->unit_token:Ljava/lang/String;

    .line 176
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_count:Ljava/lang/Long;

    .line 177
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->reason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    iget-object v3, p1, Lcom/squareup/protos/client/InventoryAdjustment;->reason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    .line 178
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->cost_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/InventoryAdjustment;->cost_money:Lcom/squareup/protos/common/Money;

    .line 179
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->refund_bill_server_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/InventoryAdjustment;->refund_bill_server_token:Ljava/lang/String;

    .line 180
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->bill_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/InventoryAdjustment;->bill_token:Ljava/lang/String;

    .line 181
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->catalog_version:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/InventoryAdjustment;->catalog_version:Ljava/lang/Long;

    .line 182
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_quantity_decimal:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_quantity_decimal:Ljava/lang/String;

    .line 183
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 188
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 190
    invoke-virtual {p0}, Lcom/squareup/protos/client/InventoryAdjustment;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->variation_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_count:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->reason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/InventoryAdjustmentReason;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->cost_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->refund_bill_server_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->bill_token:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->catalog_version:Ljava/lang/Long;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_quantity_decimal:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 200
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/InventoryAdjustment$Builder;
    .locals 2

    .line 155
    new-instance v0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;-><init>()V

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->variation_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->variation_token:Ljava/lang/String;

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->unit_token:Ljava/lang/String;

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->adjust_count:Ljava/lang/Long;

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->reason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    iput-object v1, v0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->reason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->cost_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->cost_money:Lcom/squareup/protos/common/Money;

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->refund_bill_server_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->refund_bill_server_token:Ljava/lang/String;

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->bill_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->bill_token:Ljava/lang/String;

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->catalog_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->catalog_version:Ljava/lang/Long;

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_quantity_decimal:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->adjust_quantity_decimal:Ljava/lang/String;

    .line 165
    invoke-virtual {p0}, Lcom/squareup/protos/client/InventoryAdjustment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/InventoryAdjustment;->newBuilder()Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->variation_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", variation_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->variation_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_count:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", adjust_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 211
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->reason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    if-eqz v1, :cond_3

    const-string v1, ", reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->reason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 212
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->cost_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", cost_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->cost_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 213
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->refund_bill_server_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", refund_bill_server_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->refund_bill_server_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->bill_token:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", bill_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->bill_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->catalog_version:Ljava/lang/Long;

    if-eqz v1, :cond_7

    const-string v1, ", catalog_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->catalog_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 216
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_quantity_decimal:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", adjust_quantity_decimal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_quantity_decimal:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InventoryAdjustment{"

    .line 217
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
