.class final Lcom/squareup/protos/client/estimate/EstimateDefaults$ProtoAdapter_EstimateDefaults;
.super Lcom/squareup/wire/ProtoAdapter;
.source "EstimateDefaults.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/EstimateDefaults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_EstimateDefaults"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/estimate/EstimateDefaults;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 178
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/estimate/EstimateDefaults;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/estimate/EstimateDefaults;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 203
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;-><init>()V

    .line 204
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 205
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 220
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 218
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->relative_expiry_on(Ljava/lang/Integer;)Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;

    goto :goto_0

    .line 217
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->relative_send_on(Ljava/lang/Integer;)Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;

    goto :goto_0

    .line 211
    :cond_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->delivery_method(Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;)Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 213
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 208
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->message(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;

    goto :goto_0

    .line 207
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->title(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;

    goto :goto_0

    .line 224
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 225
    invoke-virtual {v0}, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->build()Lcom/squareup/protos/client/estimate/EstimateDefaults;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 176
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/EstimateDefaults$ProtoAdapter_EstimateDefaults;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/estimate/EstimateDefaults;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/estimate/EstimateDefaults;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 193
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDefaults;->title:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 194
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDefaults;->message:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 195
    sget-object v0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDefaults;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 196
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_send_on:Ljava/lang/Integer;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 197
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_expiry_on:Ljava/lang/Integer;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 198
    invoke-virtual {p2}, Lcom/squareup/protos/client/estimate/EstimateDefaults;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 176
    check-cast p2, Lcom/squareup/protos/client/estimate/EstimateDefaults;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/estimate/EstimateDefaults$ProtoAdapter_EstimateDefaults;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/estimate/EstimateDefaults;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/estimate/EstimateDefaults;)I
    .locals 4

    .line 183
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/estimate/EstimateDefaults;->title:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimateDefaults;->message:Ljava/lang/String;

    const/4 v3, 0x2

    .line 184
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimateDefaults;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    const/4 v3, 0x3

    .line 185
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_send_on:Ljava/lang/Integer;

    const/4 v3, 0x4

    .line 186
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_expiry_on:Ljava/lang/Integer;

    const/4 v3, 0x5

    .line 187
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 188
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/EstimateDefaults;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 176
    check-cast p1, Lcom/squareup/protos/client/estimate/EstimateDefaults;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/EstimateDefaults$ProtoAdapter_EstimateDefaults;->encodedSize(Lcom/squareup/protos/client/estimate/EstimateDefaults;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/estimate/EstimateDefaults;)Lcom/squareup/protos/client/estimate/EstimateDefaults;
    .locals 0

    .line 230
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/EstimateDefaults;->newBuilder()Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;

    move-result-object p1

    .line 231
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 232
    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->build()Lcom/squareup/protos/client/estimate/EstimateDefaults;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 176
    check-cast p1, Lcom/squareup/protos/client/estimate/EstimateDefaults;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/estimate/EstimateDefaults$ProtoAdapter_EstimateDefaults;->redact(Lcom/squareup/protos/client/estimate/EstimateDefaults;)Lcom/squareup/protos/client/estimate/EstimateDefaults;

    move-result-object p1

    return-object p1
.end method
