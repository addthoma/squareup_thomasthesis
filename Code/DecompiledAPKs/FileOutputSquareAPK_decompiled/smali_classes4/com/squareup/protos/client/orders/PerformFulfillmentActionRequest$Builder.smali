.class public final Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PerformFulfillmentActionRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;",
        "Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public action:Lcom/squareup/protos/client/orders/FulfillmentAction;

.field public client_support:Lcom/squareup/protos/client/orders/ClientSupport;

.field public order_id:Ljava/lang/String;

.field public order_version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 123
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public action(Lcom/squareup/protos/client/orders/FulfillmentAction;)Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->action:Lcom/squareup/protos/client/orders/FulfillmentAction;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;
    .locals 7

    .line 148
    new-instance v6, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    iget-object v2, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->order_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->order_version:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->action:Lcom/squareup/protos/client/orders/FulfillmentAction;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;-><init>(Lcom/squareup/protos/client/orders/ClientSupport;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/client/orders/FulfillmentAction;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->build()Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest;

    move-result-object v0

    return-object v0
.end method

.method public client_support(Lcom/squareup/protos/client/orders/ClientSupport;)Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    return-object p0
.end method

.method public order_id(Ljava/lang/String;)Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->order_id:Ljava/lang/String;

    return-object p0
.end method

.method public order_version(Ljava/lang/Integer;)Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/orders/PerformFulfillmentActionRequest$Builder;->order_version:Ljava/lang/Integer;

    return-object p0
.end method
