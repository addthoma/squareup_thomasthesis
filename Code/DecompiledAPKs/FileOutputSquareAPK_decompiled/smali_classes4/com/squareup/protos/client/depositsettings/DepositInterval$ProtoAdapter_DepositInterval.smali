.class final Lcom/squareup/protos/client/depositsettings/DepositInterval$ProtoAdapter_DepositInterval;
.super Lcom/squareup/wire/ProtoAdapter;
.source "DepositInterval.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/depositsettings/DepositInterval;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_DepositInterval"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/depositsettings/DepositInterval;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 156
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/depositsettings/DepositInterval;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/depositsettings/DepositInterval;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 177
    new-instance v0, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;-><init>()V

    .line 178
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 179
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 185
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 183
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->same_day_settlement_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;

    goto :goto_0

    .line 182
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/time/DayTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DayTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->initiate_at(Lcom/squareup/protos/common/time/DayTime;)Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;

    goto :goto_0

    .line 181
    :cond_2
    sget-object v3, Lcom/squareup/protos/common/time/DayTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DayTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->cutoff_at(Lcom/squareup/protos/common/time/DayTime;)Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;

    goto :goto_0

    .line 189
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 190
    invoke-virtual {v0}, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->build()Lcom/squareup/protos/client/depositsettings/DepositInterval;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 154
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/depositsettings/DepositInterval$ProtoAdapter_DepositInterval;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/depositsettings/DepositInterval;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/depositsettings/DepositInterval;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 169
    sget-object v0, Lcom/squareup/protos/common/time/DayTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/depositsettings/DepositInterval;->cutoff_at:Lcom/squareup/protos/common/time/DayTime;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 170
    sget-object v0, Lcom/squareup/protos/common/time/DayTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/depositsettings/DepositInterval;->initiate_at:Lcom/squareup/protos/common/time/DayTime;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 171
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/depositsettings/DepositInterval;->same_day_settlement_enabled:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 172
    invoke-virtual {p2}, Lcom/squareup/protos/client/depositsettings/DepositInterval;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 154
    check-cast p2, Lcom/squareup/protos/client/depositsettings/DepositInterval;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/depositsettings/DepositInterval$ProtoAdapter_DepositInterval;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/depositsettings/DepositInterval;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/depositsettings/DepositInterval;)I
    .locals 4

    .line 161
    sget-object v0, Lcom/squareup/protos/common/time/DayTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/depositsettings/DepositInterval;->cutoff_at:Lcom/squareup/protos/common/time/DayTime;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/time/DayTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/depositsettings/DepositInterval;->initiate_at:Lcom/squareup/protos/common/time/DayTime;

    const/4 v3, 0x2

    .line 162
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/depositsettings/DepositInterval;->same_day_settlement_enabled:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 163
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 164
    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/DepositInterval;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 154
    check-cast p1, Lcom/squareup/protos/client/depositsettings/DepositInterval;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/depositsettings/DepositInterval$ProtoAdapter_DepositInterval;->encodedSize(Lcom/squareup/protos/client/depositsettings/DepositInterval;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/depositsettings/DepositInterval;)Lcom/squareup/protos/client/depositsettings/DepositInterval;
    .locals 2

    .line 195
    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/DepositInterval;->newBuilder()Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;

    move-result-object p1

    .line 196
    iget-object v0, p1, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->cutoff_at:Lcom/squareup/protos/common/time/DayTime;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/time/DayTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->cutoff_at:Lcom/squareup/protos/common/time/DayTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DayTime;

    iput-object v0, p1, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->cutoff_at:Lcom/squareup/protos/common/time/DayTime;

    .line 197
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->initiate_at:Lcom/squareup/protos/common/time/DayTime;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/time/DayTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->initiate_at:Lcom/squareup/protos/common/time/DayTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DayTime;

    iput-object v0, p1, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->initiate_at:Lcom/squareup/protos/common/time/DayTime;

    .line 198
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 199
    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->build()Lcom/squareup/protos/client/depositsettings/DepositInterval;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 154
    check-cast p1, Lcom/squareup/protos/client/depositsettings/DepositInterval;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/depositsettings/DepositInterval$ProtoAdapter_DepositInterval;->redact(Lcom/squareup/protos/client/depositsettings/DepositInterval;)Lcom/squareup/protos/client/depositsettings/DepositInterval;

    move-result-object p1

    return-object p1
.end method
