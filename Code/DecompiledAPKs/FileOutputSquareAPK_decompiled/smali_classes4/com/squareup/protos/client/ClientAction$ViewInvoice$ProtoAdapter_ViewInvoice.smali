.class final Lcom/squareup/protos/client/ClientAction$ViewInvoice$ProtoAdapter_ViewInvoice;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ClientAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/ClientAction$ViewInvoice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ViewInvoice"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/ClientAction$ViewInvoice;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2034
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$ViewInvoice;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2051
    new-instance v0, Lcom/squareup/protos/client/ClientAction$ViewInvoice$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$ViewInvoice$Builder;-><init>()V

    .line 2052
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2053
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 2057
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2055
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/ClientAction$ViewInvoice$Builder;->invoice_id(Ljava/lang/String;)Lcom/squareup/protos/client/ClientAction$ViewInvoice$Builder;

    goto :goto_0

    .line 2061
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/ClientAction$ViewInvoice$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2062
    invoke-virtual {v0}, Lcom/squareup/protos/client/ClientAction$ViewInvoice$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2032
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewInvoice$ProtoAdapter_ViewInvoice;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$ViewInvoice;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2045
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/ClientAction$ViewInvoice;->invoice_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2046
    invoke-virtual {p2}, Lcom/squareup/protos/client/ClientAction$ViewInvoice;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2032
    check-cast p2, Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/ClientAction$ViewInvoice$ProtoAdapter_ViewInvoice;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$ViewInvoice;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/ClientAction$ViewInvoice;)I
    .locals 3

    .line 2039
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/ClientAction$ViewInvoice;->invoice_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 2040
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewInvoice;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2032
    check-cast p1, Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewInvoice$ProtoAdapter_ViewInvoice;->encodedSize(Lcom/squareup/protos/client/ClientAction$ViewInvoice;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/ClientAction$ViewInvoice;)Lcom/squareup/protos/client/ClientAction$ViewInvoice;
    .locals 0

    .line 2067
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewInvoice;->newBuilder()Lcom/squareup/protos/client/ClientAction$ViewInvoice$Builder;

    move-result-object p1

    .line 2068
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewInvoice$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2069
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewInvoice$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2032
    check-cast p1, Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewInvoice$ProtoAdapter_ViewInvoice;->redact(Lcom/squareup/protos/client/ClientAction$ViewInvoice;)Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    move-result-object p1

    return-object p1
.end method
