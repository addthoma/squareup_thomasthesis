.class public final Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;",
        "Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public applied_server_version:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public applied_server_version(Ljava/lang/Long;)Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$Builder;
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$Builder;->applied_server_version:Ljava/lang/Long;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;
    .locals 3

    .line 90
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$Builder;->applied_server_version:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;-><init>(Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    move-result-object v0

    return-object v0
.end method
