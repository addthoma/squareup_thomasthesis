.class public final Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateFirstDeviceCredentialRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialRequest;",
        "Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public idempotency_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 82
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialRequest;
    .locals 3

    .line 95
    new-instance v0, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialRequest$Builder;->idempotency_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialRequest;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialRequest$Builder;->build()Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialRequest;

    move-result-object v0

    return-object v0
.end method

.method public idempotency_token(Ljava/lang/String;)Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialRequest$Builder;
    .locals 0

    .line 89
    iput-object p1, p0, Lcom/squareup/protos/client/retail/onboarding/CreateFirstDeviceCredentialRequest$Builder;->idempotency_token:Ljava/lang/String;

    return-object p0
.end method
