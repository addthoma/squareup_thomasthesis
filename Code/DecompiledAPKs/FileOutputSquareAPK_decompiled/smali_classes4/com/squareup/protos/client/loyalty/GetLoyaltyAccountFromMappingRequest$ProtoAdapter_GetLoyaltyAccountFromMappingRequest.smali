.class final Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$ProtoAdapter_GetLoyaltyAccountFromMappingRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetLoyaltyAccountFromMappingRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetLoyaltyAccountFromMappingRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 136
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 156
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;-><init>()V

    .line 157
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 158
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 163
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 161
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/loyalty/LoyaltyOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->options(Lcom/squareup/protos/client/loyalty/LoyaltyOptions;)Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;

    goto :goto_0

    .line 160
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->loyalty_account_mapping(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;

    goto :goto_0

    .line 167
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 168
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 134
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$ProtoAdapter_GetLoyaltyAccountFromMappingRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 149
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 150
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 151
    invoke-virtual {p2}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 134
    check-cast p2, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$ProtoAdapter_GetLoyaltyAccountFromMappingRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;)I
    .locals 4

    .line 141
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    const/4 v3, 0x2

    .line 142
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 143
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 134
    check-cast p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$ProtoAdapter_GetLoyaltyAccountFromMappingRequest;->encodedSize(Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;)Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;
    .locals 2

    .line 173
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;->newBuilder()Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;

    move-result-object p1

    .line 174
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    .line 175
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->options:Lcom/squareup/protos/client/loyalty/LoyaltyOptions;

    .line 176
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 177
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 134
    check-cast p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest$ProtoAdapter_GetLoyaltyAccountFromMappingRequest;->redact(Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;)Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;

    move-result-object p1

    return-object p1
.end method
