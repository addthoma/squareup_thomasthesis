.class public final Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;
.super Lcom/squareup/wire/Message;
.source "LoyaltyRequestError.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$ProtoAdapter_LoyaltyRequestError;,
        Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;,
        Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;,
        Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
        "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CODE:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

.field public static final DEFAULT_DETAILS:Ljava/lang/String; = ""

.field public static final DEFAULT_FIELD:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;

.field public static final DEFAULT_RAW_VALUE:Ljava/lang/String; = ""

.field public static final DEFAULT_VALUE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final code:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyRequestError$Code#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final details:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final field:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyRequestError$Field#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final raw_value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$ProtoAdapter_LoyaltyRequestError;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$ProtoAdapter_LoyaltyRequestError;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 29
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;->FIELD_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->DEFAULT_FIELD:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;

    .line 35
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->DEFAULT_CODE:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;Ljava/lang/String;)V
    .locals 7

    .line 89
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;-><init>(Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 94
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 95
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->field:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;

    .line 96
    iput-object p2, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->value:Ljava/lang/String;

    .line 97
    iput-object p3, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->raw_value:Ljava/lang/String;

    .line 98
    iput-object p4, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->code:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    .line 99
    iput-object p5, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->details:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 117
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 118
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;

    .line 119
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->field:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->field:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;

    .line 120
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->value:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->value:Ljava/lang/String;

    .line 121
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->raw_value:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->raw_value:Ljava/lang/String;

    .line 122
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->code:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->code:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    .line 123
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->details:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->details:Ljava/lang/String;

    .line 124
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 129
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->field:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->value:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->raw_value:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->code:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->details:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 137
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;
    .locals 2

    .line 104
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;-><init>()V

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->field:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;->field:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;->value:Ljava/lang/String;

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->raw_value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;->raw_value:Ljava/lang/String;

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->code:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;->code:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->details:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;->details:Ljava/lang/String;

    .line 110
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->newBuilder()Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->field:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;

    if-eqz v1, :cond_0

    const-string v1, ", field="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->field:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Field;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 146
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->value:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->raw_value:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", raw_value=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->code:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    if-eqz v1, :cond_3

    const-string v1, ", code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->code:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 149
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->details:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->details:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LoyaltyRequestError{"

    .line 150
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
