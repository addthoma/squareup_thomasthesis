.class public final Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "OrderClientDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/OrderClientDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/orders/OrderClientDetails;",
        "Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public channel:Lcom/squareup/protos/client/orders/Channel;

.field public external_link:Ljava/lang/String;

.field public fulfillments_display_name:Ljava/lang/String;

.field public groups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/OrderGroup;",
            ">;"
        }
    .end annotation
.end field

.field public localized_prompt_description:Ljava/lang/String;

.field public localized_prompt_title:Ljava/lang/String;

.field public order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 191
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 192
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->groups:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/orders/OrderClientDetails;
    .locals 10

    .line 250
    new-instance v9, Lcom/squareup/protos/client/orders/OrderClientDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->groups:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->channel:Lcom/squareup/protos/client/orders/Channel;

    iget-object v3, p0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->fulfillments_display_name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->localized_prompt_title:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->localized_prompt_description:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    iget-object v7, p0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->external_link:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/orders/OrderClientDetails;-><init>(Ljava/util/List;Lcom/squareup/protos/client/orders/Channel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/orders/OrderDisplayStateData;Ljava/lang/String;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 176
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->build()Lcom/squareup/protos/client/orders/OrderClientDetails;

    move-result-object v0

    return-object v0
.end method

.method public channel(Lcom/squareup/protos/client/orders/Channel;)Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->channel:Lcom/squareup/protos/client/orders/Channel;

    return-object p0
.end method

.method public external_link(Ljava/lang/String;)Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;
    .locals 0

    .line 244
    iput-object p1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->external_link:Ljava/lang/String;

    return-object p0
.end method

.method public fulfillments_display_name(Ljava/lang/String;)Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;
    .locals 0

    .line 213
    iput-object p1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->fulfillments_display_name:Ljava/lang/String;

    return-object p0
.end method

.method public groups(Ljava/util/List;)Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/OrderGroup;",
            ">;)",
            "Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;"
        }
    .end annotation

    .line 199
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 200
    iput-object p1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->groups:Ljava/util/List;

    return-object p0
.end method

.method public localized_prompt_description(Ljava/lang/String;)Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->localized_prompt_description:Ljava/lang/String;

    return-object p0
.end method

.method public localized_prompt_title(Ljava/lang/String;)Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->localized_prompt_title:Ljava/lang/String;

    return-object p0
.end method

.method public order_display_state_data(Lcom/squareup/protos/client/orders/OrderDisplayStateData;)Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;
    .locals 0

    .line 236
    iput-object p1, p0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    return-object p0
.end method
