.class public final Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StartTwoFactorAuthenticationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest;",
        "Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card_token:Ljava/lang/String;

.field public request_type:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 104
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest;
    .locals 4

    .line 125
    new-instance v0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$Builder;->card_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$Builder;->request_type:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest;

    move-result-object v0

    return-object v0
.end method

.method public card_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$Builder;->card_token:Ljava/lang/String;

    return-object p0
.end method

.method public request_type(Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;)Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$Builder;->request_type:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    return-object p0
.end method
