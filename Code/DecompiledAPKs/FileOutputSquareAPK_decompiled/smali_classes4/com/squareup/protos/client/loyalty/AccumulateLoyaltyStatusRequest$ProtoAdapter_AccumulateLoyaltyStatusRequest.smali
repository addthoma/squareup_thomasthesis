.class final Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$ProtoAdapter_AccumulateLoyaltyStatusRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AccumulateLoyaltyStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AccumulateLoyaltyStatusRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 431
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 473
    new-instance v0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;-><init>()V

    .line 474
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 475
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 505
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 503
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->newly_accepted_tos(Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    goto :goto_0

    .line 502
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    goto :goto_0

    .line 496
    :pswitch_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->reason_for_point_accrual(Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 498
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 493
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->expected_point_accrual(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    goto :goto_0

    .line 492
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->email_address_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    goto :goto_0

    .line 491
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->email_address(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    goto :goto_0

    .line 485
    :pswitch_6
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/bills/Tender$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->tender_type(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 487
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 482
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->enroll_intent(Ljava/lang/Boolean;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    goto/16 :goto_0

    .line 481
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->transfer_from_email_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    goto/16 :goto_0

    .line 480
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    goto/16 :goto_0

    .line 479
    :pswitch_a
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    goto/16 :goto_0

    .line 478
    :pswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->phone_number_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    goto/16 :goto_0

    .line 477
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    goto/16 :goto_0

    .line 509
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 510
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 429
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$ProtoAdapter_AccumulateLoyaltyStatusRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 455
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 456
    sget-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 457
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->transfer_from_email_token:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 458
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->enroll_intent:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 459
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 460
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->expected_point_accrual:Ljava/lang/Long;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 461
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->reason_for_point_accrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 462
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 463
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->newly_accepted_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 464
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->phone_number:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 465
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->phone_number_token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 466
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->email_address:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 467
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->email_address_token:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 468
    invoke-virtual {p2}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 429
    check-cast p2, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$ProtoAdapter_AccumulateLoyaltyStatusRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;)I
    .locals 4

    .line 436
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v3, 0x4

    .line 437
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->transfer_from_email_token:Ljava/lang/String;

    const/4 v3, 0x5

    .line 438
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->enroll_intent:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 439
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v3, 0x7

    .line 440
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->expected_point_accrual:Ljava/lang/Long;

    const/16 v3, 0xa

    .line 441
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->reason_for_point_accrual:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    const/16 v3, 0xb

    .line 442
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/16 v3, 0xc

    .line 443
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->newly_accepted_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    const/16 v3, 0xd

    .line 444
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->phone_number:Ljava/lang/String;

    const/4 v3, 0x1

    .line 445
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->phone_number_token:Ljava/lang/String;

    const/4 v3, 0x2

    .line 446
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->email_address:Ljava/lang/String;

    const/16 v3, 0x8

    .line 447
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->email_address_token:Ljava/lang/String;

    const/16 v3, 0x9

    .line 448
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 449
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 429
    check-cast p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$ProtoAdapter_AccumulateLoyaltyStatusRequest;->encodedSize(Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;
    .locals 2

    .line 515
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;->newBuilder()Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;

    move-result-object p1

    .line 516
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 517
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 518
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 519
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->newly_accepted_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->newly_accepted_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->newly_accepted_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    :cond_3
    const/4 v0, 0x0

    .line 520
    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->phone_number:Ljava/lang/String;

    .line 521
    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->email_address:Ljava/lang/String;

    .line 522
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 523
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 429
    check-cast p1, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest$ProtoAdapter_AccumulateLoyaltyStatusRequest;->redact(Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;

    move-result-object p1

    return-object p1
.end method
