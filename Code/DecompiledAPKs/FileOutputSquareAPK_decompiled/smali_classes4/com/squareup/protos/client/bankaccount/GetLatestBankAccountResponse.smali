.class public final Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;
.super Lcom/squareup/wire/Message;
.source "GetLatestBankAccountResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$ProtoAdapter_GetLatestBankAccountResponse;,
        Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;",
        "Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_REQUIRES_PASSWORD:Ljava/lang/Boolean;

.field public static final DEFAULT_SUCCESS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final latest_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bankaccount.BankAccount#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final requires_password:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final success:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$ProtoAdapter_GetLatestBankAccountResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$ProtoAdapter_GetLatestBankAccountResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 25
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    .line 27
    sput-object v0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->DEFAULT_REQUIRES_PASSWORD:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/squareup/protos/client/bankaccount/BankAccount;Ljava/lang/Boolean;)V
    .locals 1

    .line 58
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;-><init>(Ljava/lang/Boolean;Lcom/squareup/protos/client/bankaccount/BankAccount;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/squareup/protos/client/bankaccount/BankAccount;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 63
    sget-object v0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 64
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->success:Ljava/lang/Boolean;

    .line 65
    iput-object p2, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->latest_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    .line 66
    iput-object p3, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->requires_password:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 82
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 83
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->success:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->success:Ljava/lang/Boolean;

    .line 85
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->latest_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    iget-object v3, p1, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->latest_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    .line 86
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->requires_password:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->requires_password:Ljava/lang/Boolean;

    .line 87
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 92
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->latest_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bankaccount/BankAccount;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->requires_password:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 98
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;
    .locals 2

    .line 71
    new-instance v0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;-><init>()V

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->success:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;->success:Ljava/lang/Boolean;

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->latest_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    iput-object v1, v0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;->latest_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->requires_password:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;->requires_password:Ljava/lang/Boolean;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->newBuilder()Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->success:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", success="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->latest_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    if-eqz v1, :cond_1

    const-string v1, ", latest_bank_account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->latest_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 108
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->requires_password:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", requires_password="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->requires_password:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetLatestBankAccountResponse{"

    .line 109
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
