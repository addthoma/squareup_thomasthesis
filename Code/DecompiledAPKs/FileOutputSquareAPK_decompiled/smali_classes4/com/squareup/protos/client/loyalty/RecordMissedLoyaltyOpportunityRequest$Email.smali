.class public final Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;
.super Lcom/squareup/wire/Message;
.source "RecordMissedLoyaltyOpportunityRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Email"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email$ProtoAdapter_Email;,
        Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;",
        "Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_RAW_VALUE:Ljava/lang/String; = ""

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final raw_value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 367
    new-instance v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email$ProtoAdapter_Email;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email$ProtoAdapter_Email;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 395
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 399
    sget-object v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 400
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p3

    const/4 v0, 0x1

    if-gt p3, v0, :cond_0

    .line 403
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->token:Ljava/lang/String;

    .line 404
    iput-object p2, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->raw_value:Ljava/lang/String;

    return-void

    .line 401
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of token, raw_value may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 419
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 420
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    .line 421
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->token:Ljava/lang/String;

    .line 422
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->raw_value:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->raw_value:Ljava/lang/String;

    .line 423
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 428
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 430
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 431
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 432
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->raw_value:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 433
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email$Builder;
    .locals 2

    .line 409
    new-instance v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email$Builder;-><init>()V

    .line 410
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email$Builder;->token:Ljava/lang/String;

    .line 411
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->raw_value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email$Builder;->raw_value:Ljava/lang/String;

    .line 412
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 366
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->newBuilder()Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 440
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 441
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 442
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->raw_value:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", raw_value=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Email{"

    .line 443
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
