.class public final Lcom/squareup/protos/client/timecards/StopTimecardResponse;
.super Lcom/squareup/wire/Message;
.source "StopTimecardResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/timecards/StopTimecardResponse$ProtoAdapter_StopTimecardResponse;,
        Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;,
        Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/timecards/StopTimecardResponse;",
        "Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/timecards/StopTimecardResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_VALID:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final timecard:Lcom/squareup/protos/client/timecards/Timecard;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.Timecard#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.TimecardBreak#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final valid:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final workday_shift_summary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.StopTimecardResponse$WorkdayShiftSummary#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$ProtoAdapter_StopTimecardResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$ProtoAdapter_StopTimecardResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->DEFAULT_VALID:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/lang/Boolean;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;)V
    .locals 6

    .line 57
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/timecards/StopTimecardResponse;-><init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/lang/Boolean;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/lang/Boolean;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;Lokio/ByteString;)V
    .locals 1

    .line 62
    sget-object v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 63
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 64
    iput-object p2, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    .line 65
    iput-object p3, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->valid:Ljava/lang/Boolean;

    .line 66
    iput-object p4, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->workday_shift_summary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 83
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 84
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 86
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    .line 87
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->valid:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->valid:Ljava/lang/Boolean;

    .line 88
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->workday_shift_summary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->workday_shift_summary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    .line 89
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 94
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/Timecard;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/TimecardBreak;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->valid:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->workday_shift_summary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 101
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;
    .locals 2

    .line 71
    new-instance v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;-><init>()V

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->valid:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;->valid:Ljava/lang/Boolean;

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->workday_shift_summary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;->workday_shift_summary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->newBuilder()Lcom/squareup/protos/client/timecards/StopTimecardResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-eqz v1, :cond_0

    const-string v1, ", timecard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    if-eqz v1, :cond_1

    const-string v1, ", timecard_break="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 111
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->valid:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", valid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->valid:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 112
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->workday_shift_summary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    if-eqz v1, :cond_3

    const-string v1, ", workday_shift_summary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse;->workday_shift_summary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "StopTimecardResponse{"

    .line 113
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
