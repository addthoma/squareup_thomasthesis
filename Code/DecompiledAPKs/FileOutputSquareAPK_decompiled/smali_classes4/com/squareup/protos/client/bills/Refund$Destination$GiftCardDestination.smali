.class public final Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;
.super Lcom/squareup/wire/Message;
.source "Refund.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Refund$Destination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GiftCardDestination"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$ProtoAdapter_GiftCardDestination;,
        Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;,
        Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;",
        "Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_GIFT_CARD_PAN_SUFFIX:Ljava/lang/String; = ""

.field public static final DEFAULT_GIFT_CARD_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final gift_card_balance:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final gift_card_pan_suffix:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final gift_card_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Refund$Destination$GiftCardDestination$Type#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 649
    new-instance v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$ProtoAdapter_GiftCardDestination;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$ProtoAdapter_GiftCardDestination;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 657
    sget-object v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;->PHYSICAL:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    sput-object v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->DEFAULT_TYPE:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;Lcom/squareup/protos/common/Money;)V
    .locals 6

    .line 691
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 696
    sget-object v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 697
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_token:Ljava/lang/String;

    .line 698
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_pan_suffix:Ljava/lang/String;

    .line 699
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->type:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    .line 700
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_balance:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 717
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 718
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;

    .line 719
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_token:Ljava/lang/String;

    .line 720
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_pan_suffix:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_pan_suffix:Ljava/lang/String;

    .line 721
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->type:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->type:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    .line 722
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_balance:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_balance:Lcom/squareup/protos/common/Money;

    .line 723
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 728
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 730
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 731
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 732
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_pan_suffix:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 733
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->type:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 734
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_balance:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 735
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;
    .locals 2

    .line 705
    new-instance v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;-><init>()V

    .line 706
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;->gift_card_token:Ljava/lang/String;

    .line 707
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_pan_suffix:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;->gift_card_pan_suffix:Ljava/lang/String;

    .line 708
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->type:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;->type:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    .line 709
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_balance:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;->gift_card_balance:Lcom/squareup/protos/common/Money;

    .line 710
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 648
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->newBuilder()Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 742
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 743
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", gift_card_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 744
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_pan_suffix:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", gift_card_pan_suffix="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_pan_suffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 745
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->type:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    if-eqz v1, :cond_2

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->type:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 746
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_balance:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", gift_card_balance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_balance:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GiftCardDestination{"

    .line 747
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
