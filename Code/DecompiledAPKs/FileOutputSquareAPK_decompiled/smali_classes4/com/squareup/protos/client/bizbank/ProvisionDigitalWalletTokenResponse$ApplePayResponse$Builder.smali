.class public final Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ProvisionDigitalWalletTokenResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public activation_data:Lokio/ByteString;

.field public encrypted_pass_data:Lokio/ByteString;

.field public ephemeral_public_key:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 239
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public activation_data(Lokio/ByteString;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;
    .locals 0

    .line 254
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->activation_data:Lokio/ByteString;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;
    .locals 5

    .line 268
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->encrypted_pass_data:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->activation_data:Lokio/ByteString;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->ephemeral_public_key:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;-><init>(Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 232
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    move-result-object v0

    return-object v0
.end method

.method public encrypted_pass_data(Lokio/ByteString;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;
    .locals 0

    .line 246
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->encrypted_pass_data:Lokio/ByteString;

    return-object p0
.end method

.method public ephemeral_public_key(Lokio/ByteString;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;
    .locals 0

    .line 262
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse$Builder;->ephemeral_public_key:Lokio/ByteString;

    return-object p0
.end method
