.class public final Lcom/squareup/protos/client/CreatorDetails;
.super Lcom/squareup/wire/Message;
.source "CreatorDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/CreatorDetails$ProtoAdapter_CreatorDetails;,
        Lcom/squareup/protos/client/CreatorDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/CreatorDetails;",
        "Lcom/squareup/protos/client/CreatorDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/CreatorDetails;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final device_credential:Lcom/squareup/protos/client/DeviceCredential;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.DeviceCredential#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final employee:Lcom/squareup/protos/client/Employee;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Employee#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final read_only_mobile_staff:Lcom/squareup/protos/client/MobileStaff;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.MobileStaff#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/CreatorDetails$ProtoAdapter_CreatorDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/CreatorDetails$ProtoAdapter_CreatorDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/MobileStaff;Lcom/squareup/protos/client/Employee;Lcom/squareup/protos/client/DeviceCredential;)V
    .locals 1

    .line 60
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/CreatorDetails;-><init>(Lcom/squareup/protos/client/MobileStaff;Lcom/squareup/protos/client/Employee;Lcom/squareup/protos/client/DeviceCredential;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/MobileStaff;Lcom/squareup/protos/client/Employee;Lcom/squareup/protos/client/DeviceCredential;Lokio/ByteString;)V
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 66
    iput-object p1, p0, Lcom/squareup/protos/client/CreatorDetails;->read_only_mobile_staff:Lcom/squareup/protos/client/MobileStaff;

    .line 67
    iput-object p2, p0, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    .line 68
    iput-object p3, p0, Lcom/squareup/protos/client/CreatorDetails;->device_credential:Lcom/squareup/protos/client/DeviceCredential;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 84
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/CreatorDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 85
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/CreatorDetails;

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/CreatorDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/CreatorDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/CreatorDetails;->read_only_mobile_staff:Lcom/squareup/protos/client/MobileStaff;

    iget-object v3, p1, Lcom/squareup/protos/client/CreatorDetails;->read_only_mobile_staff:Lcom/squareup/protos/client/MobileStaff;

    .line 87
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    iget-object v3, p1, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    .line 88
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/CreatorDetails;->device_credential:Lcom/squareup/protos/client/DeviceCredential;

    iget-object p1, p1, Lcom/squareup/protos/client/CreatorDetails;->device_credential:Lcom/squareup/protos/client/DeviceCredential;

    .line 89
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 94
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/CreatorDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/client/CreatorDetails;->read_only_mobile_staff:Lcom/squareup/protos/client/MobileStaff;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/MobileStaff;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/Employee;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/CreatorDetails;->device_credential:Lcom/squareup/protos/client/DeviceCredential;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/DeviceCredential;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 100
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/CreatorDetails$Builder;
    .locals 2

    .line 73
    new-instance v0, Lcom/squareup/protos/client/CreatorDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/CreatorDetails$Builder;-><init>()V

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/client/CreatorDetails;->read_only_mobile_staff:Lcom/squareup/protos/client/MobileStaff;

    iput-object v1, v0, Lcom/squareup/protos/client/CreatorDetails$Builder;->read_only_mobile_staff:Lcom/squareup/protos/client/MobileStaff;

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    iput-object v1, v0, Lcom/squareup/protos/client/CreatorDetails$Builder;->employee:Lcom/squareup/protos/client/Employee;

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/CreatorDetails;->device_credential:Lcom/squareup/protos/client/DeviceCredential;

    iput-object v1, v0, Lcom/squareup/protos/client/CreatorDetails$Builder;->device_credential:Lcom/squareup/protos/client/DeviceCredential;

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/client/CreatorDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/CreatorDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/CreatorDetails;->newBuilder()Lcom/squareup/protos/client/CreatorDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/CreatorDetails;->read_only_mobile_staff:Lcom/squareup/protos/client/MobileStaff;

    if-eqz v1, :cond_0

    const-string v1, ", read_only_mobile_staff="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/CreatorDetails;->read_only_mobile_staff:Lcom/squareup/protos/client/MobileStaff;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 109
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    if-eqz v1, :cond_1

    const-string v1, ", employee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 110
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/CreatorDetails;->device_credential:Lcom/squareup/protos/client/DeviceCredential;

    if-eqz v1, :cond_2

    const-string v1, ", device_credential="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/CreatorDetails;->device_credential:Lcom/squareup/protos/client/DeviceCredential;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CreatorDetails{"

    .line 111
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
