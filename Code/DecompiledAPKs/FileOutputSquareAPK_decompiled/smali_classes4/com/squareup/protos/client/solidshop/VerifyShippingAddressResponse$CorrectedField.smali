.class public final enum Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;
.super Ljava/lang/Enum;
.source "VerifyShippingAddressResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CorrectedField"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField$ProtoAdapter_CorrectedField;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ADDRESS_LINE_1:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

.field public static final enum ADDRESS_LINE_2:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

.field public static final enum ADMINISTRATIVE_DISTRICT_LEVEL_1:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

.field public static final enum LOCALITY:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

.field public static final enum POSTAL_CODE:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

.field public static final enum UNKNOWN_FIELD:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 275
    new-instance v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_FIELD"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->UNKNOWN_FIELD:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    .line 280
    new-instance v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    const/4 v2, 0x1

    const-string v3, "POSTAL_CODE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->POSTAL_CODE:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    .line 285
    new-instance v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    const/4 v3, 0x2

    const-string v4, "LOCALITY"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->LOCALITY:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    .line 290
    new-instance v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    const/4 v4, 0x3

    const-string v5, "ADMINISTRATIVE_DISTRICT_LEVEL_1"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->ADMINISTRATIVE_DISTRICT_LEVEL_1:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    .line 295
    new-instance v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    const/4 v5, 0x4

    const-string v6, "ADDRESS_LINE_1"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->ADDRESS_LINE_1:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    .line 300
    new-instance v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    const/4 v6, 0x5

    const-string v7, "ADDRESS_LINE_2"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->ADDRESS_LINE_2:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    .line 274
    sget-object v7, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->UNKNOWN_FIELD:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->POSTAL_CODE:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->LOCALITY:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->ADMINISTRATIVE_DISTRICT_LEVEL_1:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->ADDRESS_LINE_1:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->ADDRESS_LINE_2:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->$VALUES:[Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    .line 302
    new-instance v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField$ProtoAdapter_CorrectedField;

    invoke-direct {v0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField$ProtoAdapter_CorrectedField;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 306
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 307
    iput p3, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 320
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->ADDRESS_LINE_2:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    return-object p0

    .line 319
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->ADDRESS_LINE_1:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    return-object p0

    .line 318
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->ADMINISTRATIVE_DISTRICT_LEVEL_1:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    return-object p0

    .line 317
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->LOCALITY:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    return-object p0

    .line 316
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->POSTAL_CODE:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    return-object p0

    .line 315
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->UNKNOWN_FIELD:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;
    .locals 1

    .line 274
    const-class v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;
    .locals 1

    .line 274
    sget-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->$VALUES:[Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 327
    iget v0, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;->value:I

    return v0
.end method
