.class public final Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LoyaltyAccount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/LoyaltyAccount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyAccount;",
        "Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact:Lcom/squareup/protos/client/rolodex/Contact;

.field public created_at:Lcom/squareup/protos/client/ISO8601Date;

.field public enrolled_at:Lcom/squareup/protos/common/time/DateTime;

.field public loyalty_account_token:Ljava/lang/String;

.field public mappings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 168
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 169
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->mappings:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/LoyaltyAccount;
    .locals 8

    .line 222
    new-instance v7, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->loyalty_account_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->mappings:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v5, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->enrolled_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/util/List;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 157
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    move-result-object v0

    return-object v0
.end method

.method public contact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;
    .locals 0

    .line 208
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object p0
.end method

.method public created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public enrolled_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->enrolled_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public loyalty_account_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->loyalty_account_token:Ljava/lang/String;

    return-object p0
.end method

.method public mappings(Ljava/util/List;)Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            ">;)",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;"
        }
    .end annotation

    .line 198
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 199
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->mappings:Ljava/util/List;

    return-object p0
.end method
