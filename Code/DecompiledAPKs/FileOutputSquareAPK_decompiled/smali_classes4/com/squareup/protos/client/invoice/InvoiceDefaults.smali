.class public final Lcom/squareup/protos/client/invoice/InvoiceDefaults;
.super Lcom/squareup/wire/Message;
.source "InvoiceDefaults.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/InvoiceDefaults$ProtoAdapter_InvoiceDefaults;,
        Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
        "Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AUTOMATIC_REMINDERS_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_BUYER_ENTERED_INSTRUMENT_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_RELATIVE_SEND_ON:Ljava/lang/Integer;

.field public static final DEFAULT_SHIPPING_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_TITLE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final automatic_reminder_config:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceReminderConfig#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;"
        }
    .end annotation
.end field

.field public final automatic_reminders_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final buyer_entered_instrument_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final payment_request_defaults:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.PaymentRequestDefaults#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;",
            ">;"
        }
    .end annotation
.end field

.field public final relative_send_on:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final shipping_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 23
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$ProtoAdapter_InvoiceDefaults;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceDefaults$ProtoAdapter_InvoiceDefaults;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->DEFAULT_BUYER_ENTERED_INSTRUMENT_ENABLED:Ljava/lang/Boolean;

    .line 29
    sput-object v1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->DEFAULT_SHIPPING_ENABLED:Ljava/lang/Boolean;

    .line 31
    sput-object v1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->DEFAULT_AUTOMATIC_REMINDERS_ENABLED:Ljava/lang/Boolean;

    .line 33
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->DEFAULT_RELATIVE_SEND_ON:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;)V"
        }
    .end annotation

    .line 117
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/invoice/InvoiceDefaults;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 124
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    .line 126
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->shipping_enabled:Ljava/lang/Boolean;

    .line 127
    iput-object p3, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminders_enabled:Ljava/lang/Boolean;

    .line 128
    iput-object p4, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->relative_send_on:Ljava/lang/Integer;

    .line 129
    iput-object p5, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->title:Ljava/lang/String;

    .line 130
    iput-object p6, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->message:Ljava/lang/String;

    const-string p1, "payment_request_defaults"

    .line 131
    invoke-static {p1, p7}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->payment_request_defaults:Ljava/util/List;

    const-string p1, "automatic_reminder_config"

    .line 132
    invoke-static {p1, p8}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminder_config:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 153
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 154
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;

    .line 155
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    .line 156
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->shipping_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->shipping_enabled:Ljava/lang/Boolean;

    .line 157
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminders_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminders_enabled:Ljava/lang/Boolean;

    .line 158
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->relative_send_on:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->relative_send_on:Ljava/lang/Integer;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->title:Ljava/lang/String;

    .line 160
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->message:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->message:Ljava/lang/String;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->payment_request_defaults:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->payment_request_defaults:Ljava/util/List;

    .line 162
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminder_config:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminder_config:Ljava/util/List;

    .line 163
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 168
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 170
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->shipping_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminders_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->relative_send_on:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->title:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->message:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->payment_request_defaults:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminder_config:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;
    .locals 2

    .line 137
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;-><init>()V

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->shipping_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->shipping_enabled:Ljava/lang/Boolean;

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminders_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->automatic_reminders_enabled:Ljava/lang/Boolean;

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->relative_send_on:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->relative_send_on:Ljava/lang/Integer;

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->title:Ljava/lang/String;

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->message:Ljava/lang/String;

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->payment_request_defaults:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->payment_request_defaults:Ljava/util/List;

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminder_config:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->automatic_reminder_config:Ljava/util/List;

    .line 146
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->newBuilder()Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 187
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", buyer_entered_instrument_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->shipping_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", shipping_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->shipping_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 189
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminders_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", automatic_reminders_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminders_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 190
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->relative_send_on:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", relative_send_on="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->relative_send_on:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 191
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->title:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->message:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->payment_request_defaults:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", payment_request_defaults="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->payment_request_defaults:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 194
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminder_config:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, ", automatic_reminder_config="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminder_config:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InvoiceDefaults{"

    .line 195
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
