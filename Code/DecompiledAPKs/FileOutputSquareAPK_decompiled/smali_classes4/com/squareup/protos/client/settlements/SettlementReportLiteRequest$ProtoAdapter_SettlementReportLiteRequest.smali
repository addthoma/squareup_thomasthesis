.class final Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$ProtoAdapter_SettlementReportLiteRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SettlementReportLiteRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SettlementReportLiteRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 266
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 285
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;-><init>()V

    .line 286
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 287
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x6

    if-eq v3, v4, :cond_0

    .line 292
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 290
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->batch_request(Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;

    goto :goto_0

    .line 289
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/settlements/RequestParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/settlements/RequestParams;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->request_params(Lcom/squareup/protos/client/settlements/RequestParams;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;

    goto :goto_0

    .line 296
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 297
    invoke-virtual {v0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 264
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$ProtoAdapter_SettlementReportLiteRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 278
    sget-object v0, Lcom/squareup/protos/client/settlements/RequestParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;->request_params:Lcom/squareup/protos/client/settlements/RequestParams;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 279
    sget-object v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;->batch_request:Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 280
    invoke-virtual {p2}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 264
    check-cast p2, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$ProtoAdapter_SettlementReportLiteRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;)I
    .locals 4

    .line 271
    sget-object v0, Lcom/squareup/protos/client/settlements/RequestParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;->request_params:Lcom/squareup/protos/client/settlements/RequestParams;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;->batch_request:Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;

    const/4 v3, 0x6

    .line 272
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 273
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 264
    check-cast p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$ProtoAdapter_SettlementReportLiteRequest;->encodedSize(Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;
    .locals 2

    .line 302
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;->newBuilder()Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;

    move-result-object p1

    .line 303
    iget-object v0, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->request_params:Lcom/squareup/protos/client/settlements/RequestParams;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/settlements/RequestParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->request_params:Lcom/squareup/protos/client/settlements/RequestParams;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/settlements/RequestParams;

    iput-object v0, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->request_params:Lcom/squareup/protos/client/settlements/RequestParams;

    .line 304
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->batch_request:Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->batch_request:Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;

    iput-object v0, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->batch_request:Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;

    .line 305
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 306
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 264
    check-cast p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$ProtoAdapter_SettlementReportLiteRequest;->redact(Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;

    move-result-object p1

    return-object p1
.end method
