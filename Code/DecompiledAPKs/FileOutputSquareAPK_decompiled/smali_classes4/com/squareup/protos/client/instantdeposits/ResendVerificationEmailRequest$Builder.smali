.class public final Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ResendVerificationEmailRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailRequest;",
        "Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailRequest$Builder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailRequest;
    .locals 2

    .line 66
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailRequest;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailRequest;-><init>(Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 60
    invoke-virtual {p0}, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailRequest$Builder;->build()Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailRequest;

    move-result-object v0

    return-object v0
.end method
