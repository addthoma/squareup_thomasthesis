.class public final Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DayOfWeekDepositSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;",
        "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public day_of_week:Lcom/squareup/protos/common/time/DayOfWeek;

.field public deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 105
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;
    .locals 4

    .line 128
    new-instance v0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->day_of_week:Lcom/squareup/protos/common/time/DayOfWeek;

    iget-object v2, p0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;-><init>(Lcom/squareup/protos/common/time/DayOfWeek;Lcom/squareup/protos/client/depositsettings/DepositInterval;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->build()Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    move-result-object v0

    return-object v0
.end method

.method public day_of_week(Lcom/squareup/protos/common/time/DayOfWeek;)Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->day_of_week:Lcom/squareup/protos/common/time/DayOfWeek;

    return-object p0
.end method

.method public deposit_interval(Lcom/squareup/protos/client/depositsettings/DepositInterval;)Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    return-object p0
.end method
