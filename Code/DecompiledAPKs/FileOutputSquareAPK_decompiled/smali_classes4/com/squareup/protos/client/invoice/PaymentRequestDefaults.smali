.class public final Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;
.super Lcom/squareup/wire/Message;
.source "PaymentRequestDefaults.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$ProtoAdapter_PaymentRequestDefaults;,
        Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;",
        "Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PAYMENT_METHOD:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

.field public static final DEFAULT_RELATIVE_DUE_ON:Ljava/lang/Integer;

.field public static final DEFAULT_TIPPING_ENABLED:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.Invoice$PaymentMethod#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final relative_due_on:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field

.field public final tipping_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 22
    new-instance v0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$ProtoAdapter_PaymentRequestDefaults;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$ProtoAdapter_PaymentRequestDefaults;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 26
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->DEFAULT_TIPPING_ENABLED:Ljava/lang/Boolean;

    .line 28
    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->UNKNOWN_METHOD:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    sput-object v1, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->DEFAULT_PAYMENT_METHOD:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 30
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->DEFAULT_RELATIVE_DUE_ON:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/Integer;)V
    .locals 1

    .line 61
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;-><init>(Ljava/lang/Boolean;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 66
    sget-object v0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 67
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->tipping_enabled:Ljava/lang/Boolean;

    .line 68
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 69
    iput-object p3, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->relative_due_on:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 85
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 86
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->tipping_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->tipping_enabled:Ljava/lang/Boolean;

    .line 88
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 89
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->relative_due_on:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->relative_due_on:Ljava/lang/Integer;

    .line 90
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 95
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->tipping_enabled:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->relative_due_on:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 101
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;
    .locals 2

    .line 74
    new-instance v0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;-><init>()V

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->tipping_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;->tipping_enabled:Ljava/lang/Boolean;

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->relative_due_on:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;->relative_due_on:Ljava/lang/Integer;

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->newBuilder()Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->tipping_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", tipping_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->tipping_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eqz v1, :cond_1

    const-string v1, ", payment_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 111
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->relative_due_on:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", relative_due_on="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;->relative_due_on:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PaymentRequestDefaults{"

    .line 112
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
