.class final Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$ProtoAdapter_TenderInfoLoyaltyOptions;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AddTendersRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_TenderInfoLoyaltyOptions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 611
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 630
    new-instance v0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;-><init>()V

    .line 631
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 632
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 637
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 635
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;->max_supported_tos(Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;

    goto :goto_0

    .line 634
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;->can_accrue_loyalty(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;

    goto :goto_0

    .line 641
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 642
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 609
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$ProtoAdapter_TenderInfoLoyaltyOptions;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 623
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->can_accrue_loyalty:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 624
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->max_supported_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 625
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 609
    check-cast p2, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$ProtoAdapter_TenderInfoLoyaltyOptions;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;)I
    .locals 4

    .line 616
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->can_accrue_loyalty:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->max_supported_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    const/4 v3, 0x2

    .line 617
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 618
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 609
    check-cast p1, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$ProtoAdapter_TenderInfoLoyaltyOptions;->encodedSize(Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;
    .locals 2

    .line 647
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->newBuilder()Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;

    move-result-object p1

    .line 648
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;->max_supported_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;->max_supported_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;->max_supported_tos:Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    .line 649
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 650
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 609
    check-cast p1, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions$ProtoAdapter_TenderInfoLoyaltyOptions;->redact(Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;)Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    move-result-object p1

    return-object p1
.end method
