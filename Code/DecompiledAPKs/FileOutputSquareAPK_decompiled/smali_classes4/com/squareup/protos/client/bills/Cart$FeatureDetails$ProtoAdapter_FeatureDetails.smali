.class final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ProtoAdapter_FeatureDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FeatureDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 7811
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7860
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;-><init>()V

    .line 7861
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 7862
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 7882
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 7880
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->pricing_engine_state(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    goto :goto_0

    .line 7879
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->order_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    goto :goto_0

    .line 7878
    :pswitch_3
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->lifecycle_event:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 7877
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->rst_order_metadata(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    goto :goto_0

    .line 7876
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->auth_and_delayed_capture(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    goto :goto_0

    .line 7875
    :pswitch_6
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->cover_count_change_log:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 7874
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->reopen_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    goto :goto_0

    .line 7873
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->bill_print_state(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    goto :goto_0

    .line 7872
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->seating(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    goto :goto_0

    .line 7871
    :pswitch_a
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->coursing_options(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    goto/16 :goto_0

    .line 7870
    :pswitch_b
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->available_instrument_details:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 7869
    :pswitch_c
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->appointments_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    goto/16 :goto_0

    .line 7868
    :pswitch_d
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->buyer_info(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    goto/16 :goto_0

    .line 7867
    :pswitch_e
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->kitchen_printing(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    goto/16 :goto_0

    .line 7866
    :pswitch_f
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->invoice(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    goto/16 :goto_0

    .line 7865
    :pswitch_10
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    goto/16 :goto_0

    .line 7864
    :pswitch_11
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->order_deprecated(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    goto/16 :goto_0

    .line 7886
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 7887
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7809
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ProtoAdapter_FeatureDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7838
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_deprecated:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7839
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7840
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7841
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7842
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7843
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7844
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->available_instrument_details:Ljava/util/List;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7845
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7846
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7847
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->bill_print_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7848
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->reopen_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7849
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->cover_count_change_log:Ljava/util/List;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7850
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->auth_and_delayed_capture:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7851
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->rst_order_metadata:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7852
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->lifecycle_event:Ljava/util/List;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7853
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7854
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7855
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7809
    check-cast p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ProtoAdapter_FeatureDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)I
    .locals 4

    .line 7816
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_deprecated:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    const/4 v3, 0x2

    .line 7817
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    const/4 v3, 0x3

    .line 7818
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    const/4 v3, 0x4

    .line 7819
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    const/4 v3, 0x5

    .line 7820
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    const/4 v3, 0x6

    .line 7821
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 7822
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->available_instrument_details:Ljava/util/List;

    const/4 v3, 0x7

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    const/16 v3, 0x8

    .line 7823
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    const/16 v3, 0x9

    .line 7824
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->bill_print_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    const/16 v3, 0xa

    .line 7825
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->reopen_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    const/16 v3, 0xb

    .line 7826
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 7827
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->cover_count_change_log:Ljava/util/List;

    const/16 v3, 0xc

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->auth_and_delayed_capture:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    const/16 v3, 0xe

    .line 7828
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->rst_order_metadata:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

    const/16 v3, 0xf

    .line 7829
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 7830
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->lifecycle_event:Ljava/util/List;

    const/16 v3, 0x10

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    const/16 v3, 0x11

    .line 7831
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    const/16 v3, 0x12

    .line 7832
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7833
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 7809
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ProtoAdapter_FeatureDetails;->encodedSize(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails;
    .locals 2

    .line 7892
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object p1

    .line 7893
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->order_deprecated:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->order_deprecated:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->order_deprecated:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    .line 7894
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    .line 7895
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    .line 7896
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    .line 7897
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    .line 7898
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    .line 7899
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->available_instrument_details:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 7900
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    .line 7901
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    .line 7902
    :cond_7
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->bill_print_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->bill_print_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->bill_print_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    .line 7903
    :cond_8
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->reopen_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->reopen_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->reopen_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    .line 7904
    :cond_9
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->cover_count_change_log:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 7905
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->auth_and_delayed_capture:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->auth_and_delayed_capture:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->auth_and_delayed_capture:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    .line 7906
    :cond_a
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->rst_order_metadata:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->rst_order_metadata:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->rst_order_metadata:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

    .line 7907
    :cond_b
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->lifecycle_event:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 7908
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->order_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->order_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->order_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    .line 7909
    :cond_c
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    .line 7910
    :cond_d
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 7911
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 7809
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ProtoAdapter_FeatureDetails;->redact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object p1

    return-object p1
.end method
