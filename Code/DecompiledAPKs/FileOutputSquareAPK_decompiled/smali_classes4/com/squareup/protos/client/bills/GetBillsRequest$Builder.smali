.class public final Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetBillsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/GetBillsRequest;",
        "Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public limit:Ljava/lang/Integer;

.field public merchant_token:Ljava/lang/String;

.field public pagination_token:Ljava/lang/String;

.field public query:Lcom/squareup/protos/client/bills/GetBillsRequest$Query;

.field public query_params:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 170
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/GetBillsRequest;
    .locals 8

    .line 226
    new-instance v7, Lcom/squareup/protos/client/bills/GetBillsRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->query:Lcom/squareup/protos/client/bills/GetBillsRequest$Query;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->limit:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->pagination_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->query_params:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/GetBillsRequest;-><init>(Lcom/squareup/protos/client/bills/GetBillsRequest$Query;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 159
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->build()Lcom/squareup/protos/client/bills/GetBillsRequest;

    move-result-object v0

    return-object v0
.end method

.method public limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->limit:Ljava/lang/Integer;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;
    .locals 0

    .line 212
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public pagination_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;
    .locals 0

    .line 202
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->pagination_token:Ljava/lang/String;

    return-object p0
.end method

.method public query(Lcom/squareup/protos/client/bills/GetBillsRequest$Query;)Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->query:Lcom/squareup/protos/client/bills/GetBillsRequest$Query;

    return-object p0
.end method

.method public query_params(Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;)Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->query_params:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    return-object p0
.end method
