.class public final Lcom/squareup/protos/client/rolodex/UpsertGroupRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpsertGroupRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/UpsertGroupRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/UpsertGroupRequest;",
        "Lcom/squareup/protos/client/rolodex/UpsertGroupRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public group:Lcom/squareup/protos/client/rolodex/Group;

.field public request_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/UpsertGroupRequest;
    .locals 4

    .line 119
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpsertGroupRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpsertGroupRequest$Builder;->group:Lcom/squareup/protos/client/rolodex/Group;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/UpsertGroupRequest$Builder;->request_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/UpsertGroupRequest;-><init>(Lcom/squareup/protos/client/rolodex/Group;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/UpsertGroupRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UpsertGroupRequest;

    move-result-object v0

    return-object v0
.end method

.method public group(Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/protos/client/rolodex/UpsertGroupRequest$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpsertGroupRequest$Builder;->group:Lcom/squareup/protos/client/rolodex/Group;

    return-object p0
.end method

.method public request_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UpsertGroupRequest$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpsertGroupRequest$Builder;->request_token:Ljava/lang/String;

    return-object p0
.end method
