.class public final Lcom/squareup/protos/client/tickets/v2/ListResponse;
.super Lcom/squareup/wire/Message;
.source "ListResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tickets/v2/ListResponse$ProtoAdapter_ListResponse;,
        Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/tickets/v2/ListResponse;",
        "Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tickets/v2/ListResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_INCOMPATIBLE_TICKET_COUNT_FIXABLE_VIA_UPGRADE:Ljava/lang/Long;

.field public static final DEFAULT_INCOMPATIBLE_TICKET_COUNT_UNFIXABLE:Ljava/lang/Long;

.field public static final DEFAULT_MORE_TICKETS_AVAILABLE:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final incompatible_ticket_count_fixable_via_upgrade:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x3
    .end annotation
.end field

.field public final incompatible_ticket_count_unfixable:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x4
    .end annotation
.end field

.field public final more_tickets_available:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final ticket:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tickets.Ticket#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tickets/Ticket;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Lcom/squareup/protos/client/tickets/v2/ListResponse$ProtoAdapter_ListResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/tickets/v2/ListResponse$ProtoAdapter_ListResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 29
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->DEFAULT_INCOMPATIBLE_TICKET_COUNT_FIXABLE_VIA_UPGRADE:Ljava/lang/Long;

    .line 31
    sput-object v0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->DEFAULT_INCOMPATIBLE_TICKET_COUNT_UNFIXABLE:Ljava/lang/Long;

    const/4 v0, 0x0

    .line 33
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->DEFAULT_MORE_TICKETS_AVAILABLE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tickets/Ticket;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 87
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/tickets/v2/ListResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tickets/Ticket;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 93
    sget-object v0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 94
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->status:Lcom/squareup/protos/client/Status;

    const-string p1, "ticket"

    .line 95
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->ticket:Ljava/util/List;

    .line 96
    iput-object p3, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->incompatible_ticket_count_fixable_via_upgrade:Ljava/lang/Long;

    .line 97
    iput-object p4, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->incompatible_ticket_count_unfixable:Ljava/lang/Long;

    .line 98
    iput-object p5, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->more_tickets_available:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 116
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/tickets/v2/ListResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 117
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/tickets/v2/ListResponse;

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/v2/ListResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/v2/ListResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/tickets/v2/ListResponse;->status:Lcom/squareup/protos/client/Status;

    .line 119
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->ticket:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/tickets/v2/ListResponse;->ticket:Ljava/util/List;

    .line 120
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->incompatible_ticket_count_fixable_via_upgrade:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/tickets/v2/ListResponse;->incompatible_ticket_count_fixable_via_upgrade:Ljava/lang/Long;

    .line 121
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->incompatible_ticket_count_unfixable:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/tickets/v2/ListResponse;->incompatible_ticket_count_unfixable:Ljava/lang/Long;

    .line 122
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->more_tickets_available:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/tickets/v2/ListResponse;->more_tickets_available:Ljava/lang/Boolean;

    .line 123
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 128
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/v2/ListResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->ticket:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->incompatible_ticket_count_fixable_via_upgrade:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->incompatible_ticket_count_unfixable:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->more_tickets_available:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 136
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;
    .locals 2

    .line 103
    new-instance v0, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;-><init>()V

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->ticket:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->ticket:Ljava/util/List;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->incompatible_ticket_count_fixable_via_upgrade:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->incompatible_ticket_count_fixable_via_upgrade:Ljava/lang/Long;

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->incompatible_ticket_count_unfixable:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->incompatible_ticket_count_unfixable:Ljava/lang/Long;

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->more_tickets_available:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->more_tickets_available:Ljava/lang/Boolean;

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/v2/ListResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/v2/ListResponse;->newBuilder()Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 145
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->ticket:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", ticket="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->ticket:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 146
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->incompatible_ticket_count_fixable_via_upgrade:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", incompatible_ticket_count_fixable_via_upgrade="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->incompatible_ticket_count_fixable_via_upgrade:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 147
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->incompatible_ticket_count_unfixable:Ljava/lang/Long;

    if-eqz v1, :cond_3

    const-string v1, ", incompatible_ticket_count_unfixable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->incompatible_ticket_count_unfixable:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 148
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->more_tickets_available:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", more_tickets_available="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse;->more_tickets_available:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ListResponse{"

    .line 149
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
