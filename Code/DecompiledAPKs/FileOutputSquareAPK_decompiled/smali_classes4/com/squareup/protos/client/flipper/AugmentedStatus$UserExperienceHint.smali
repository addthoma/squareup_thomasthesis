.class public final enum Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;
.super Ljava/lang/Enum;
.source "AugmentedStatus.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/flipper/AugmentedStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UserExperienceHint"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint$ProtoAdapter_UserExperienceHint;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DEFAULT_UX_HINT_DO_NOT_USE:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

.field public static final enum OBSOLETE_NO_SUGGESTED_ACTION:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

.field public static final enum SUGGEST_ACTIVATION:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

.field public static final enum SUGGEST_CONTACT_SUPPORT:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

.field public static final enum SUGGEST_RETRY:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 176
    new-instance v0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    const/4 v1, 0x0

    const-string v2, "DEFAULT_UX_HINT_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->DEFAULT_UX_HINT_DO_NOT_USE:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    .line 182
    new-instance v0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    const/4 v2, 0x1

    const-string v3, "OBSOLETE_NO_SUGGESTED_ACTION"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->OBSOLETE_NO_SUGGESTED_ACTION:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    .line 189
    new-instance v0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    const/4 v3, 0x2

    const-string v4, "SUGGEST_RETRY"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->SUGGEST_RETRY:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    .line 194
    new-instance v0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    const/4 v4, 0x3

    const-string v5, "SUGGEST_ACTIVATION"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->SUGGEST_ACTIVATION:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    .line 200
    new-instance v0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    const/4 v5, 0x4

    const-string v6, "SUGGEST_CONTACT_SUPPORT"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->SUGGEST_CONTACT_SUPPORT:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    .line 171
    sget-object v6, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->DEFAULT_UX_HINT_DO_NOT_USE:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->OBSOLETE_NO_SUGGESTED_ACTION:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->SUGGEST_RETRY:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->SUGGEST_ACTIVATION:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->SUGGEST_CONTACT_SUPPORT:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->$VALUES:[Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    .line 202
    new-instance v0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint$ProtoAdapter_UserExperienceHint;

    invoke-direct {v0}, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint$ProtoAdapter_UserExperienceHint;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 206
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 207
    iput p3, p0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 219
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->SUGGEST_CONTACT_SUPPORT:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    return-object p0

    .line 218
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->SUGGEST_ACTIVATION:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    return-object p0

    .line 217
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->SUGGEST_RETRY:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    return-object p0

    .line 216
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->OBSOLETE_NO_SUGGESTED_ACTION:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    return-object p0

    .line 215
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->DEFAULT_UX_HINT_DO_NOT_USE:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;
    .locals 1

    .line 171
    const-class v0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;
    .locals 1

    .line 171
    sget-object v0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->$VALUES:[Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 226
    iget v0, p0, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->value:I

    return v0
.end method
