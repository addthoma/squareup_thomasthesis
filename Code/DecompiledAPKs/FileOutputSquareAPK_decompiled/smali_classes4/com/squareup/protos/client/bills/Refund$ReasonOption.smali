.class public final enum Lcom/squareup/protos/client/bills/Refund$ReasonOption;
.super Ljava/lang/Enum;
.source "Refund.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Refund;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReasonOption"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Refund$ReasonOption$ProtoAdapter_ReasonOption;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/Refund$ReasonOption;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/Refund$ReasonOption;

.field public static final enum ACCIDENTAL_CHARGE:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Refund$ReasonOption;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CANCELED_ORDER:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

.field public static final enum FRAUDULENT_CHARGE:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

.field public static final enum OTHER_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

.field public static final enum RETURNED_GOODS:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

.field public static final enum UNKNOWN_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 514
    new-instance v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_REASON"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/Refund$ReasonOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->UNKNOWN_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    .line 516
    new-instance v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    const/4 v2, 0x1

    const-string v3, "RETURNED_GOODS"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/Refund$ReasonOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->RETURNED_GOODS:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    .line 518
    new-instance v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    const/4 v3, 0x2

    const-string v4, "ACCIDENTAL_CHARGE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/Refund$ReasonOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->ACCIDENTAL_CHARGE:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    .line 520
    new-instance v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    const/4 v4, 0x3

    const-string v5, "CANCELED_ORDER"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/Refund$ReasonOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->CANCELED_ORDER:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    .line 522
    new-instance v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    const/4 v5, 0x4

    const-string v6, "OTHER_REASON"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bills/Refund$ReasonOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->OTHER_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    .line 524
    new-instance v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    const/4 v6, 0x5

    const-string v7, "FRAUDULENT_CHARGE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/bills/Refund$ReasonOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->FRAUDULENT_CHARGE:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    .line 513
    sget-object v7, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->UNKNOWN_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->RETURNED_GOODS:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->ACCIDENTAL_CHARGE:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->CANCELED_ORDER:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->OTHER_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->FRAUDULENT_CHARGE:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->$VALUES:[Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    .line 526
    new-instance v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption$ProtoAdapter_ReasonOption;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Refund$ReasonOption$ProtoAdapter_ReasonOption;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 530
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 531
    iput p3, p0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/Refund$ReasonOption;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 544
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->FRAUDULENT_CHARGE:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    return-object p0

    .line 543
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->OTHER_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    return-object p0

    .line 542
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->CANCELED_ORDER:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    return-object p0

    .line 541
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->ACCIDENTAL_CHARGE:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    return-object p0

    .line 540
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->RETURNED_GOODS:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    return-object p0

    .line 539
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->UNKNOWN_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Refund$ReasonOption;
    .locals 1

    .line 513
    const-class v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/Refund$ReasonOption;
    .locals 1

    .line 513
    sget-object v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->$VALUES:[Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/Refund$ReasonOption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 551
    iget v0, p0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->value:I

    return v0
.end method
