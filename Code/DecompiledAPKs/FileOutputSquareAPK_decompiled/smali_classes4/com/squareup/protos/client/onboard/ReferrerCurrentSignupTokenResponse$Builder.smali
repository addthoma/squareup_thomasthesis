.class public final Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReferrerCurrentSignupTokenResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;",
        "Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public current_signup_token:Lcom/squareup/protos/client/onboard/SignupToken;

.field public free_processing_balance:Lcom/squareup/protos/client/onboard/FreeProcessingBalance;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 114
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;
    .locals 5

    .line 142
    new-instance v0, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse$Builder;->current_signup_token:Lcom/squareup/protos/client/onboard/SignupToken;

    iget-object v3, p0, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse$Builder;->free_processing_balance:Lcom/squareup/protos/client/onboard/FreeProcessingBalance;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/onboard/SignupToken;Lcom/squareup/protos/client/onboard/FreeProcessingBalance;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse$Builder;->build()Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;

    move-result-object v0

    return-object v0
.end method

.method public current_signup_token(Lcom/squareup/protos/client/onboard/SignupToken;)Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse$Builder;->current_signup_token:Lcom/squareup/protos/client/onboard/SignupToken;

    return-object p0
.end method

.method public free_processing_balance(Lcom/squareup/protos/client/onboard/FreeProcessingBalance;)Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse$Builder;->free_processing_balance:Lcom/squareup/protos/client/onboard/FreeProcessingBalance;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
