.class public final Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EGiftOrderConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
        "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public all_egift_themes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation
.end field

.field public custom_policy:Ljava/lang/String;

.field public denomination_amounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field public disabled_in_store:Ljava/lang/Boolean;

.field public disabled_online:Ljava/lang/Boolean;

.field public enabled_theme_tokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public host_unit_token:Ljava/lang/String;

.field public max_load_amount:Lcom/squareup/protos/common/Money;

.field public min_load_amount:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 239
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 240
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->denomination_amounts:Ljava/util/List;

    .line 241
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->all_egift_themes:Ljava/util/List;

    .line 242
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->enabled_theme_tokens:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public all_egift_themes(Ljava/util/List;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;)",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 271
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 272
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->all_egift_themes:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;
    .locals 12

    .line 329
    new-instance v11, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->host_unit_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->denomination_amounts:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->all_egift_themes:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->enabled_theme_tokens:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->custom_policy:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->min_load_amount:Lcom/squareup/protos/common/Money;

    iget-object v7, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->max_load_amount:Lcom/squareup/protos/common/Money;

    iget-object v8, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->disabled_online:Ljava/lang/Boolean;

    iget-object v9, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->disabled_in_store:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 220
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->build()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public custom_policy(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;
    .locals 0

    .line 291
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->custom_policy:Ljava/lang/String;

    return-object p0
.end method

.method public denomination_amounts(Ljava/util/List;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;"
        }
    .end annotation

    .line 259
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 260
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->denomination_amounts:Ljava/util/List;

    return-object p0
.end method

.method public disabled_in_store(Ljava/lang/Boolean;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;
    .locals 0

    .line 323
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->disabled_in_store:Ljava/lang/Boolean;

    return-object p0
.end method

.method public disabled_online(Ljava/lang/Boolean;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;
    .locals 0

    .line 315
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->disabled_online:Ljava/lang/Boolean;

    return-object p0
.end method

.method public enabled_theme_tokens(Ljava/util/List;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;"
        }
    .end annotation

    .line 282
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 283
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->enabled_theme_tokens:Ljava/util/List;

    return-object p0
.end method

.method public host_unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;
    .locals 0

    .line 249
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->host_unit_token:Ljava/lang/String;

    return-object p0
.end method

.method public max_load_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;
    .locals 0

    .line 307
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->max_load_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public min_load_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;
    .locals 0

    .line 299
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->min_load_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
