.class public final Lcom/squareup/protos/client/DeviceCredential$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeviceCredential.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/DeviceCredential;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/DeviceCredential;",
        "Lcom/squareup/protos/client/DeviceCredential$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public name:Ljava/lang/String;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 100
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/DeviceCredential;
    .locals 4

    .line 121
    new-instance v0, Lcom/squareup/protos/client/DeviceCredential;

    iget-object v1, p0, Lcom/squareup/protos/client/DeviceCredential$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/DeviceCredential$Builder;->name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/DeviceCredential;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/protos/client/DeviceCredential$Builder;->build()Lcom/squareup/protos/client/DeviceCredential;

    move-result-object v0

    return-object v0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/DeviceCredential$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/DeviceCredential$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/DeviceCredential$Builder;
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/protos/client/DeviceCredential$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
