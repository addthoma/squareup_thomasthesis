.class public final enum Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;
.super Ljava/lang/Enum;
.source "DeactivateCardRequest.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DeactivationReason"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason$ProtoAdapter_DeactivationReason;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CANCEL:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

.field public static final enum CARD_NOT_RECEIVED:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

.field public static final enum LOST:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

.field public static final enum STOLEN:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 117
    new-instance v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    .line 119
    new-instance v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    const/4 v2, 0x1

    const-string v3, "LOST"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->LOST:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    .line 121
    new-instance v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    const/4 v3, 0x2

    const-string v4, "STOLEN"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->STOLEN:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    .line 123
    new-instance v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    const/4 v4, 0x3

    const-string v5, "CARD_NOT_RECEIVED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->CARD_NOT_RECEIVED:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    .line 125
    new-instance v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    const/4 v5, 0x4

    const-string v6, "CANCEL"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->CANCEL:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    .line 116
    sget-object v6, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->LOST:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->STOLEN:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->CARD_NOT_RECEIVED:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->CANCEL:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->$VALUES:[Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    .line 127
    new-instance v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason$ProtoAdapter_DeactivationReason;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason$ProtoAdapter_DeactivationReason;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 131
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 132
    iput p3, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 144
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->CANCEL:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    return-object p0

    .line 143
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->CARD_NOT_RECEIVED:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    return-object p0

    .line 142
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->STOLEN:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    return-object p0

    .line 141
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->LOST:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    return-object p0

    .line 140
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;
    .locals 1

    .line 116
    const-class v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;
    .locals 1

    .line 116
    sget-object v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->$VALUES:[Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 151
    iget v0, p0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->value:I

    return v0
.end method
