.class public final Lcom/squareup/protos/client/irf/SaveFormResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SaveFormResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/irf/SaveFormResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/irf/SaveFormResponse;",
        "Lcom/squareup/protos/client/irf/SaveFormResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 93
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 94
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/irf/SaveFormResponse$Builder;->error:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/irf/SaveFormResponse;
    .locals 4

    .line 110
    new-instance v0, Lcom/squareup/protos/client/irf/SaveFormResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/SaveFormResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/irf/SaveFormResponse$Builder;->error:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/irf/SaveFormResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/SaveFormResponse$Builder;->build()Lcom/squareup/protos/client/irf/SaveFormResponse;

    move-result-object v0

    return-object v0
.end method

.method public error(Ljava/util/List;)Lcom/squareup/protos/client/irf/SaveFormResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/irf/SaveFormResponse$Builder;"
        }
    .end annotation

    .line 103
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 104
    iput-object p1, p0, Lcom/squareup/protos/client/irf/SaveFormResponse$Builder;->error:Ljava/util/List;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/irf/SaveFormResponse$Builder;
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/protos/client/irf/SaveFormResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
