.class public final Lcom/squareup/protos/client/timecards/DateRange$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DateRange.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/DateRange;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/DateRange;",
        "Lcom/squareup/protos/client/timecards/DateRange$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public start_date:Lcom/squareup/protos/client/timecards/ISO8601DateOnly;

.field public stop_date:Lcom/squareup/protos/client/timecards/ISO8601DateOnly;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/DateRange;
    .locals 4

    .line 118
    new-instance v0, Lcom/squareup/protos/client/timecards/DateRange;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/DateRange$Builder;->start_date:Lcom/squareup/protos/client/timecards/ISO8601DateOnly;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/DateRange$Builder;->stop_date:Lcom/squareup/protos/client/timecards/ISO8601DateOnly;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/timecards/DateRange;-><init>(Lcom/squareup/protos/client/timecards/ISO8601DateOnly;Lcom/squareup/protos/client/timecards/ISO8601DateOnly;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/DateRange$Builder;->build()Lcom/squareup/protos/client/timecards/DateRange;

    move-result-object v0

    return-object v0
.end method

.method public start_date(Lcom/squareup/protos/client/timecards/ISO8601DateOnly;)Lcom/squareup/protos/client/timecards/DateRange$Builder;
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/DateRange$Builder;->start_date:Lcom/squareup/protos/client/timecards/ISO8601DateOnly;

    return-object p0
.end method

.method public stop_date(Lcom/squareup/protos/client/timecards/ISO8601DateOnly;)Lcom/squareup/protos/client/timecards/DateRange$Builder;
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/DateRange$Builder;->stop_date:Lcom/squareup/protos/client/timecards/ISO8601DateOnly;

    return-object p0
.end method
