.class public final Lcom/squareup/protos/client/giftcards/GiftCard$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GiftCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/GiftCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        "Lcom/squareup/protos/client/giftcards/GiftCard$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public activity:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field public balance_money:Lcom/squareup/protos/common/Money;

.field public card_type:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

.field public pan_suffix:Ljava/lang/String;

.field public server_id:Ljava/lang/String;

.field public state:Lcom/squareup/protos/client/giftcards/GiftCard$State;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 166
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 167
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->activity:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public activity(Ljava/util/List;)Lcom/squareup/protos/client/giftcards/GiftCard$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/Transaction;",
            ">;)",
            "Lcom/squareup/protos/client/giftcards/GiftCard$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 199
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 200
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->activity:Ljava/util/List;

    return-object p0
.end method

.method public balance_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/giftcards/GiftCard$Builder;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->balance_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/giftcards/GiftCard;
    .locals 9

    .line 211
    new-instance v8, Lcom/squareup/protos/client/giftcards/GiftCard;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->server_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->state:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    iget-object v3, p0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->balance_money:Lcom/squareup/protos/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->pan_suffix:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->activity:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->card_type:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/giftcards/GiftCard;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/giftcards/GiftCard$State;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/giftcards/GiftCard$CardType;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 153
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->build()Lcom/squareup/protos/client/giftcards/GiftCard;

    move-result-object v0

    return-object v0
.end method

.method public card_type(Lcom/squareup/protos/client/giftcards/GiftCard$CardType;)Lcom/squareup/protos/client/giftcards/GiftCard$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->card_type:Lcom/squareup/protos/client/giftcards/GiftCard$CardType;

    return-object p0
.end method

.method public pan_suffix(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/GiftCard$Builder;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->pan_suffix:Ljava/lang/String;

    return-object p0
.end method

.method public server_id(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/GiftCard$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->server_id:Ljava/lang/String;

    return-object p0
.end method

.method public state(Lcom/squareup/protos/client/giftcards/GiftCard$State;)Lcom/squareup/protos/client/giftcards/GiftCard$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/GiftCard$Builder;->state:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    return-object p0
.end method
