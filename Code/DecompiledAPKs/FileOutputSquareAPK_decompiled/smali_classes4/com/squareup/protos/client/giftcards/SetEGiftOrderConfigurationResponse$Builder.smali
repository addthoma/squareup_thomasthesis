.class public final Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SetEGiftOrderConfigurationResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;",
        "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public all_egift_themes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation
.end field

.field public order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 115
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 116
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->all_egift_themes:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public all_egift_themes(Ljava/util/List;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;)",
            "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;"
        }
    .end annotation

    .line 128
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->all_egift_themes:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;
    .locals 5

    .line 144
    new-instance v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->all_egift_themes:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 108
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->build()Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;

    move-result-object v0

    return-object v0
.end method

.method public order_configuration(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
