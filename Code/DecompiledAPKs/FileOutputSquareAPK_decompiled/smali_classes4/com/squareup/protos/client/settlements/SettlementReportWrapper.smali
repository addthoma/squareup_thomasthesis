.class public final Lcom/squareup/protos/client/settlements/SettlementReportWrapper;
.super Lcom/squareup/wire/Message;
.source "SettlementReportWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/settlements/SettlementReportWrapper$ProtoAdapter_SettlementReportWrapper;,
        Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
        "Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final send_date:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final settlement:Lcom/squareup/protos/client/settlements/SettlementReport;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.settlements.SettlementReport#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$ProtoAdapter_SettlementReportWrapper;

    invoke-direct {v0}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$ProtoAdapter_SettlementReportWrapper;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/settlements/SettlementReport;Lcom/squareup/protos/common/time/DateTime;)V
    .locals 1

    .line 49
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;-><init>(Lcom/squareup/protos/client/settlements/SettlementReport;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/settlements/SettlementReport;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 55
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    .line 56
    iput-object p2, p0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 71
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 72
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    .line 73
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    .line 74
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    iget-object p1, p1, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    .line 75
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 80
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/settlements/SettlementReport;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 85
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;
    .locals 2

    .line 61
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;-><init>()V

    .line 62
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    .line 63
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->send_date:Lcom/squareup/protos/common/time/DateTime;

    .line 64
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->newBuilder()Lcom/squareup/protos/client/settlements/SettlementReportWrapper$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    if-eqz v1, :cond_0

    const-string v1, ", settlement="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->settlement:Lcom/squareup/protos/client/settlements/SettlementReport;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 94
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    const-string v1, ", send_date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportWrapper;->send_date:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SettlementReportWrapper{"

    .line 95
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
