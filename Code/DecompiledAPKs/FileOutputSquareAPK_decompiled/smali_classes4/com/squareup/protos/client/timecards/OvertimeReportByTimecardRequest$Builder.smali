.class public final Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "OvertimeReportByTimecardRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardRequest;",
        "Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public request:Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 78
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardRequest;
    .locals 3

    .line 88
    new-instance v0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardRequest$Builder;->request:Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardRequest;-><init>(Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardRequest$Builder;->build()Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardRequest;

    move-result-object v0

    return-object v0
.end method

.method public request(Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;)Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardRequest$Builder;
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardRequest$Builder;->request:Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;

    return-object p0
.end method
