.class public final Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListEstimatesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/ListEstimatesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/estimate/ListEstimatesRequest;",
        "Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public check_archive_for_matches:Ljava/lang/Boolean;

.field public contact_token:Ljava/lang/String;

.field public date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

.field public include_archived:Ljava/lang/Boolean;

.field public limit:Ljava/lang/Integer;

.field public paging_key:Ljava/lang/String;

.field public query:Ljava/lang/String;

.field public state_filter:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 221
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/estimate/ListEstimatesRequest;
    .locals 11

    .line 296
    new-instance v10, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->paging_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->limit:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->query:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iget-object v5, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->state_filter:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    iget-object v6, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->contact_token:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->check_archive_for_matches:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->include_archived:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 204
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->build()Lcom/squareup/protos/client/estimate/ListEstimatesRequest;

    move-result-object v0

    return-object v0
.end method

.method public check_archive_for_matches(Ljava/lang/Boolean;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;
    .locals 0

    .line 280
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->check_archive_for_matches:Ljava/lang/Boolean;

    return-object p0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;
    .locals 0

    .line 271
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method public date_range(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;
    .locals 0

    .line 255
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    return-object p0
.end method

.method public include_archived(Ljava/lang/Boolean;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;
    .locals 0

    .line 290
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->include_archived:Ljava/lang/Boolean;

    return-object p0
.end method

.method public limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->limit:Ljava/lang/Integer;

    return-object p0
.end method

.method public paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->paging_key:Ljava/lang/String;

    return-object p0
.end method

.method public query(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->query:Ljava/lang/String;

    return-object p0
.end method

.method public state_filter(Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;)Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;
    .locals 0

    .line 263
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->state_filter:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    return-object p0
.end method
