.class public final Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;
.super Lcom/squareup/wire/Message;
.source "GetOpenTimecardAndBreakForEmployeeResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$ProtoAdapter_GetOpenTimecardAndBreakForEmployeeResponse;,
        Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;",
        "Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CLOCKED_IN_CURRENT_WORKDAY:Ljava/lang/Boolean;

.field public static final DEFAULT_TOTAL_SECONDS_CLOCKED_IN_FOR_CURRENT_WORKDAY:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final clocked_in_current_workday:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final timecard:Lcom/squareup/protos/client/timecards/Timecard;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.Timecard#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.TimecardBreak#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 22
    new-instance v0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$ProtoAdapter_GetOpenTimecardAndBreakForEmployeeResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$ProtoAdapter_GetOpenTimecardAndBreakForEmployeeResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 26
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->DEFAULT_TOTAL_SECONDS_CLOCKED_IN_FOR_CURRENT_WORKDAY:Ljava/lang/Long;

    const/4 v0, 0x0

    .line 28
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->DEFAULT_CLOCKED_IN_CURRENT_WORKDAY:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/lang/Long;Ljava/lang/Boolean;)V
    .locals 6

    .line 67
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;-><init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/lang/Long;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/lang/Long;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 73
    sget-object v0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 74
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 75
    iput-object p2, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    .line 76
    iput-object p3, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;

    .line 77
    iput-object p4, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->clocked_in_current_workday:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 94
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 95
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 97
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    .line 98
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;

    .line 99
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->clocked_in_current_workday:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->clocked_in_current_workday:Ljava/lang/Boolean;

    .line 100
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 105
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/Timecard;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/TimecardBreak;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->clocked_in_current_workday:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 112
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;
    .locals 2

    .line 82
    new-instance v0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;-><init>()V

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->clocked_in_current_workday:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->clocked_in_current_workday:Ljava/lang/Boolean;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->newBuilder()Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-eqz v1, :cond_0

    const-string v1, ", timecard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 121
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    if-eqz v1, :cond_1

    const-string v1, ", timecard_break="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 122
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", total_seconds_clocked_in_for_current_workday="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->total_seconds_clocked_in_for_current_workday:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 123
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->clocked_in_current_workday:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", clocked_in_current_workday="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeResponse;->clocked_in_current_workday:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetOpenTimecardAndBreakForEmployeeResponse{"

    .line 124
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
