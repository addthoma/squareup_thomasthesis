.class public final Lcom/squareup/protos/client/bills/Itemization$BackingDetails;
.super Lcom/squareup/wire/Message;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BackingDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Itemization$BackingDetails$ProtoAdapter_BackingDetails;,
        Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;,
        Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Itemization$BackingDetails;",
        "Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Itemization$BackingDetails;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$BackingDetails$AvailableOptions#ADAPTER"
        tag = 0x4
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final category:Lcom/squareup/api/items/MenuCategory;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.MenuCategory#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final item:Lcom/squareup/api/items/Item;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Item#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final item_image:Lcom/squareup/api/items/ItemImage;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemImage#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final menu:Lcom/squareup/api/items/Menu;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Menu#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1844
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$ProtoAdapter_BackingDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$ProtoAdapter_BackingDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/Item;Lcom/squareup/api/items/ItemImage;Lcom/squareup/api/items/MenuCategory;Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;Lcom/squareup/api/items/Menu;)V
    .locals 7

    .line 1900
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;-><init>(Lcom/squareup/api/items/Item;Lcom/squareup/api/items/ItemImage;Lcom/squareup/api/items/MenuCategory;Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;Lcom/squareup/api/items/Menu;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/Item;Lcom/squareup/api/items/ItemImage;Lcom/squareup/api/items/MenuCategory;Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;Lcom/squareup/api/items/Menu;Lokio/ByteString;)V
    .locals 1

    .line 1905
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1906
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    .line 1907
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item_image:Lcom/squareup/api/items/ItemImage;

    .line 1908
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->category:Lcom/squareup/api/items/MenuCategory;

    .line 1909
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    .line 1910
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->menu:Lcom/squareup/api/items/Menu;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1928
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1929
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    .line 1930
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    .line 1931
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item_image:Lcom/squareup/api/items/ItemImage;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item_image:Lcom/squareup/api/items/ItemImage;

    .line 1932
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->category:Lcom/squareup/api/items/MenuCategory;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->category:Lcom/squareup/api/items/MenuCategory;

    .line 1933
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    .line 1934
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->menu:Lcom/squareup/api/items/Menu;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->menu:Lcom/squareup/api/items/Menu;

    .line 1935
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1940
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 1942
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1943
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/api/items/Item;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1944
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item_image:Lcom/squareup/api/items/ItemImage;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemImage;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1945
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->category:Lcom/squareup/api/items/MenuCategory;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/api/items/MenuCategory;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1946
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1947
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->menu:Lcom/squareup/api/items/Menu;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/api/items/Menu;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 1948
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;
    .locals 2

    .line 1915
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;-><init>()V

    .line 1916
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item:Lcom/squareup/api/items/Item;

    .line 1917
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item_image:Lcom/squareup/api/items/ItemImage;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item_image:Lcom/squareup/api/items/ItemImage;

    .line 1918
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->category:Lcom/squareup/api/items/MenuCategory;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->category:Lcom/squareup/api/items/MenuCategory;

    .line 1919
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    .line 1920
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->menu:Lcom/squareup/api/items/Menu;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->menu:Lcom/squareup/api/items/Menu;

    .line 1921
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1843
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1955
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1956
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    if-eqz v1, :cond_0

    const-string v1, ", item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1957
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item_image:Lcom/squareup/api/items/ItemImage;

    if-eqz v1, :cond_1

    const-string v1, ", item_image="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item_image:Lcom/squareup/api/items/ItemImage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1958
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->category:Lcom/squareup/api/items/MenuCategory;

    if-eqz v1, :cond_2

    const-string v1, ", category="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->category:Lcom/squareup/api/items/MenuCategory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1959
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    if-eqz v1, :cond_3

    const-string v1, ", available_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1960
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->menu:Lcom/squareup/api/items/Menu;

    if-eqz v1, :cond_4

    const-string v1, ", menu="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->menu:Lcom/squareup/api/items/Menu;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BackingDetails{"

    .line 1961
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
