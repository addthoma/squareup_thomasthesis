.class public final Lcom/squareup/protos/client/onboard/NextRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NextRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/NextRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/onboard/NextRequest;",
        "Lcom/squareup/protos/client/onboard/NextRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public action:Ljava/lang/String;

.field public client_request_uuid:Ljava/lang/String;

.field public outputs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Output;",
            ">;"
        }
    .end annotation
.end field

.field public panel_name:Ljava/lang/String;

.field public session_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 141
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 142
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->outputs:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public action(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/NextRequest$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->action:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/onboard/NextRequest;
    .locals 8

    .line 173
    new-instance v7, Lcom/squareup/protos/client/onboard/NextRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->session_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->panel_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->outputs:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->action:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->client_request_uuid:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/onboard/NextRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->build()Lcom/squareup/protos/client/onboard/NextRequest;

    move-result-object v0

    return-object v0
.end method

.method public client_request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/NextRequest$Builder;
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->client_request_uuid:Ljava/lang/String;

    return-object p0
.end method

.method public outputs(Ljava/util/List;)Lcom/squareup/protos/client/onboard/NextRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Output;",
            ">;)",
            "Lcom/squareup/protos/client/onboard/NextRequest$Builder;"
        }
    .end annotation

    .line 156
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 157
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->outputs:Ljava/util/List;

    return-object p0
.end method

.method public panel_name(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/NextRequest$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->panel_name:Ljava/lang/String;

    return-object p0
.end method

.method public session_token(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/NextRequest$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/NextRequest$Builder;->session_token:Ljava/lang/String;

    return-object p0
.end method
