.class public final Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TipLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/TipLineItem$Amounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/TipLineItem$Amounts;",
        "Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public applied_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 206
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;->applied_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/TipLineItem$Amounts;
    .locals 3

    .line 216
    new-instance v0, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;-><init>(Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 203
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    move-result-object v0

    return-object v0
.end method
