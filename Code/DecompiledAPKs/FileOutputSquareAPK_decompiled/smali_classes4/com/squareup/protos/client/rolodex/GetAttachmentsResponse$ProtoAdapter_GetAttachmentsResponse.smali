.class final Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$ProtoAdapter_GetAttachmentsResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetAttachmentsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetAttachmentsResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 181
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 206
    new-instance v0, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;-><init>()V

    .line 207
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 208
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 216
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 214
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;

    goto :goto_0

    .line 213
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->attachments:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/Attachment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 212
    :cond_2
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->attachment_responses:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/rolodex/AttachmentResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 211
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->failed_attachments(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;

    goto :goto_0

    .line 210
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->succeeded_attachments(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;

    goto :goto_0

    .line 220
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 221
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 179
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$ProtoAdapter_GetAttachmentsResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 196
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;->succeeded_attachments:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 197
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;->failed_attachments:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 198
    sget-object v0, Lcom/squareup/protos/client/rolodex/AttachmentResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;->attachment_responses:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 199
    sget-object v0, Lcom/squareup/protos/client/rolodex/Attachment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;->attachments:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 200
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 201
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 179
    check-cast p2, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$ProtoAdapter_GetAttachmentsResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;)I
    .locals 4

    .line 186
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;->succeeded_attachments:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;->failed_attachments:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 187
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/AttachmentResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 188
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;->attachment_responses:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Attachment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 189
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;->attachments:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v3, 0x5

    .line 190
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 191
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 179
    check-cast p1, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$ProtoAdapter_GetAttachmentsResponse;->encodedSize(Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;)Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;
    .locals 2

    .line 226
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;->newBuilder()Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;

    move-result-object p1

    .line 227
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->attachment_responses:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/AttachmentResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 228
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->attachments:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/rolodex/Attachment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 229
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Status;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 230
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 231
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 179
    check-cast p1, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse$ProtoAdapter_GetAttachmentsResponse;->redact(Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;)Lcom/squareup/protos/client/rolodex/GetAttachmentsResponse;

    move-result-object p1

    return-object p1
.end method
