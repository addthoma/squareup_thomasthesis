.class public final Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;
.super Lcom/squareup/wire/Message;
.source "CardSummary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/instruments/CardSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GiftCardSummary"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary$ProtoAdapter_GiftCardSummary;,
        Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;",
        "Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final remaining_balance:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 180
    new-instance v0, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary$ProtoAdapter_GiftCardSummary;

    invoke-direct {v0}, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary$ProtoAdapter_GiftCardSummary;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 194
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;-><init>(Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 198
    sget-object v0, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 199
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;->remaining_balance:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 213
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 214
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;

    .line 215
    invoke-virtual {p0}, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;->remaining_balance:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;->remaining_balance:Lcom/squareup/protos/common/Money;

    .line 216
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 221
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 223
    invoke-virtual {p0}, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 224
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;->remaining_balance:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 225
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary$Builder;
    .locals 2

    .line 204
    new-instance v0, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary$Builder;-><init>()V

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;->remaining_balance:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary$Builder;->remaining_balance:Lcom/squareup/protos/common/Money;

    .line 206
    invoke-virtual {p0}, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 179
    invoke-virtual {p0}, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;->newBuilder()Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 232
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 233
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;->remaining_balance:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    const-string v1, ", remaining_balance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;->remaining_balance:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GiftCardSummary{"

    .line 234
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
