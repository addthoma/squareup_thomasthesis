.class public final Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;
.super Lcom/squareup/wire/Message;
.source "RedeemPointsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$ProtoAdapter_RedeemPointsRequest;,
        Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;",
        "Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COUPON_DEFINITION_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_IDEMPOTENCE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_LOYALTY_ACCOUNT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_PHONE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_RETURN_COUPON_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final coupon_definition_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final idempotence_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final loyalty_account_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final phone_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final return_coupon_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$ProtoAdapter_RedeemPointsRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$ProtoAdapter_RedeemPointsRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 76
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 82
    sget-object v0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 83
    invoke-static {p4, p5}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p6

    const/4 v0, 0x1

    if-gt p6, v0, :cond_0

    .line 86
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->coupon_definition_token:Ljava/lang/String;

    .line 87
    iput-object p2, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->return_coupon_token:Ljava/lang/String;

    .line 88
    iput-object p3, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->idempotence_token:Ljava/lang/String;

    .line 89
    iput-object p4, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->phone_token:Ljava/lang/String;

    .line 90
    iput-object p5, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->loyalty_account_token:Ljava/lang/String;

    return-void

    .line 84
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of phone_token, loyalty_account_token may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 108
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 109
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;

    .line 110
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->coupon_definition_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->coupon_definition_token:Ljava/lang/String;

    .line 111
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->return_coupon_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->return_coupon_token:Ljava/lang/String;

    .line 112
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->idempotence_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->idempotence_token:Ljava/lang/String;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->phone_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->phone_token:Ljava/lang/String;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->loyalty_account_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->loyalty_account_token:Ljava/lang/String;

    .line 115
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 120
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 122
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->coupon_definition_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->return_coupon_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->idempotence_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->phone_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->loyalty_account_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 128
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;
    .locals 2

    .line 95
    new-instance v0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;-><init>()V

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->coupon_definition_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->coupon_definition_token:Ljava/lang/String;

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->return_coupon_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->return_coupon_token:Ljava/lang/String;

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->idempotence_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->idempotence_token:Ljava/lang/String;

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->phone_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->phone_token:Ljava/lang/String;

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->loyalty_account_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->loyalty_account_token:Ljava/lang/String;

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->newBuilder()Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->coupon_definition_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", coupon_definition_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->coupon_definition_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->return_coupon_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", return_coupon_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->return_coupon_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->idempotence_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", idempotence_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->idempotence_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->phone_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", phone_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->phone_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->loyalty_account_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", loyalty_account_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->loyalty_account_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RedeemPointsRequest{"

    .line 141
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
