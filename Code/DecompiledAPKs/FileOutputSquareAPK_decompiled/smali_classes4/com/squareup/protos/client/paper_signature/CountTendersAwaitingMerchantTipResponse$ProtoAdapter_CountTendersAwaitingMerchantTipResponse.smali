.class final Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$ProtoAdapter_CountTendersAwaitingMerchantTipResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CountTendersAwaitingMerchantTipResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CountTendersAwaitingMerchantTipResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 283
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 313
    new-instance v0, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;-><init>()V

    .line 314
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 315
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 325
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 323
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_expiring_soon_age_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;

    goto :goto_0

    .line 322
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_expiring_soon_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;

    goto :goto_0

    .line 321
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->next_request_backoff_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;

    goto :goto_0

    .line 320
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;

    goto :goto_0

    .line 319
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->error_title(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;

    goto :goto_0

    .line 318
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->error_message(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;

    goto :goto_0

    .line 317
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;

    goto :goto_0

    .line 329
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 330
    invoke-virtual {v0}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->build()Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 281
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$ProtoAdapter_CountTendersAwaitingMerchantTipResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 301
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 302
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 303
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 304
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_count:Ljava/lang/Integer;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 305
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 306
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_count:Ljava/lang/Integer;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 307
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_age_seconds:Ljava/lang/Integer;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 308
    invoke-virtual {p2}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 281
    check-cast p2, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$ProtoAdapter_CountTendersAwaitingMerchantTipResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)I
    .locals 4

    .line 288
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    const/4 v3, 0x2

    .line 289
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    const/4 v3, 0x3

    .line 290
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_count:Ljava/lang/Integer;

    const/4 v3, 0x4

    .line 291
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    const/4 v3, 0x5

    .line 292
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_count:Ljava/lang/Integer;

    const/4 v3, 0x6

    .line 293
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_expiring_soon_age_seconds:Ljava/lang/Integer;

    const/4 v3, 0x7

    .line 294
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 295
    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 281
    check-cast p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$ProtoAdapter_CountTendersAwaitingMerchantTipResponse;->encodedSize(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;
    .locals 0

    .line 336
    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;->newBuilder()Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;

    move-result-object p1

    .line 337
    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 338
    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$Builder;->build()Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 281
    check-cast p1, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse$ProtoAdapter_CountTendersAwaitingMerchantTipResponse;->redact(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;)Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;

    move-result-object p1

    return-object p1
.end method
