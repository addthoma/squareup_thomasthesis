.class public final Lcom/squareup/protos/client/estimate/EstimateDefaults;
.super Lcom/squareup/wire/Message;
.source "EstimateDefaults.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/estimate/EstimateDefaults$ProtoAdapter_EstimateDefaults;,
        Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/estimate/EstimateDefaults;",
        "Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/estimate/EstimateDefaults;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DELIVERY_METHOD:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

.field public static final DEFAULT_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_RELATIVE_EXPIRY_ON:Ljava/lang/Integer;

.field public static final DEFAULT_RELATIVE_SEND_ON:Ljava/lang/Integer;

.field public static final DEFAULT_TITLE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.estimate.Estimate$DeliveryMethod#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final relative_expiry_on:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x5
    .end annotation
.end field

.field public final relative_send_on:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimateDefaults$ProtoAdapter_EstimateDefaults;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/EstimateDefaults$ProtoAdapter_EstimateDefaults;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 29
    sget-object v0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->UNKNOWN_METHOD:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->DEFAULT_DELIVERY_METHOD:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->DEFAULT_RELATIVE_SEND_ON:Ljava/lang/Integer;

    .line 33
    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->DEFAULT_RELATIVE_EXPIRY_ON:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 7

    .line 67
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/estimate/EstimateDefaults;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 72
    sget-object v0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 73
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->title:Ljava/lang/String;

    .line 74
    iput-object p2, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->message:Ljava/lang/String;

    .line 75
    iput-object p3, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    .line 76
    iput-object p4, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_send_on:Ljava/lang/Integer;

    .line 77
    iput-object p5, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_expiry_on:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 95
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/estimate/EstimateDefaults;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 96
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/estimate/EstimateDefaults;

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/EstimateDefaults;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/EstimateDefaults;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimateDefaults;->title:Ljava/lang/String;

    .line 98
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->message:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimateDefaults;->message:Ljava/lang/String;

    .line 99
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimateDefaults;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    .line 100
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_send_on:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_send_on:Ljava/lang/Integer;

    .line 101
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_expiry_on:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_expiry_on:Ljava/lang/Integer;

    .line 102
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 107
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/EstimateDefaults;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->title:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->message:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_send_on:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_expiry_on:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 115
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;
    .locals 2

    .line 82
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;-><init>()V

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->title:Ljava/lang/String;

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->message:Ljava/lang/String;

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_send_on:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->relative_send_on:Ljava/lang/Integer;

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_expiry_on:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->relative_expiry_on:Ljava/lang/Integer;

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/EstimateDefaults;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/EstimateDefaults;->newBuilder()Lcom/squareup/protos/client/estimate/EstimateDefaults$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->title:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->message:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    if-eqz v1, :cond_2

    const-string v1, ", delivery_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_send_on:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", relative_send_on="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_send_on:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 127
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_expiry_on:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", relative_expiry_on="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/EstimateDefaults;->relative_expiry_on:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "EstimateDefaults{"

    .line 128
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
