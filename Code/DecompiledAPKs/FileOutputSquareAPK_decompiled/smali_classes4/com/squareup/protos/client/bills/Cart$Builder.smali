.class public final Lcom/squareup/protos/client/bills/Cart$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart;",
        "Lcom/squareup/protos/client/bills/Cart$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

.field public amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

.field public created_by_unit_token:Ljava/lang/String;

.field public feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

.field public line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

.field public read_only_custom_note:Ljava/lang/String;

.field public return_line_items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 210
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 211
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$Builder;->return_line_items:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public amount_details(Lcom/squareup/protos/client/bills/Cart$AmountDetails;)Lcom/squareup/protos/client/bills/Cart$Builder;
    .locals 0

    .line 261
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$Builder;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    return-object p0
.end method

.method public amounts(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$Builder;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/Cart;
    .locals 10

    .line 278
    new-instance v9, Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$Builder;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$Builder;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$Builder;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Cart$Builder;->read_only_custom_note:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Cart$Builder;->return_line_items:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/Cart$Builder;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/Cart$Builder;->created_by_unit_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/Cart;-><init>(Lcom/squareup/protos/client/bills/Cart$LineItems;Lcom/squareup/protos/client/bills/Cart$Amounts;Lcom/squareup/protos/client/bills/Cart$FeatureDetails;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart$AmountDetails;Ljava/lang/String;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 195
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$Builder;->build()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v0

    return-object v0
.end method

.method public created_by_unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$Builder;
    .locals 0

    .line 272
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$Builder;->created_by_unit_token:Ljava/lang/String;

    return-object p0
.end method

.method public feature_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Lcom/squareup/protos/client/bills/Cart$Builder;
    .locals 0

    .line 236
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$Builder;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    return-object p0
.end method

.method public line_items(Lcom/squareup/protos/client/bills/Cart$LineItems;)Lcom/squareup/protos/client/bills/Cart$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$Builder;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    return-object p0
.end method

.method public read_only_custom_note(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$Builder;
    .locals 0

    .line 244
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$Builder;->read_only_custom_note:Ljava/lang/String;

    return-object p0
.end method

.method public return_line_items(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$Builder;"
        }
    .end annotation

    .line 252
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 253
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$Builder;->return_line_items:Ljava/util/List;

    return-object p0
.end method
