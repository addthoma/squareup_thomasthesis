.class public final Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetBillFamiliesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_family:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;",
            ">;"
        }
    .end annotation
.end field

.field public contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;",
            ">;"
        }
    .end annotation
.end field

.field public pagination_token:Ljava/lang/String;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 164
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 165
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;->bill_family:Ljava/util/List;

    .line 166
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;->contacts:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bill_family(Ljava/util/List;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;",
            ">;)",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;"
        }
    .end annotation

    .line 180
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;->bill_family:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;
    .locals 7

    .line 206
    new-instance v6, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;->bill_family:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;->pagination_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;->contacts:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 155
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;

    move-result-object v0

    return-object v0
.end method

.method public contacts(Ljava/util/List;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;",
            ">;)",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;"
        }
    .end annotation

    .line 199
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 200
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;->contacts:Ljava/util/List;

    return-object p0
.end method

.method public pagination_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;->pagination_token:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
