.class public final Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SquareProductAttributes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;",
        "Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public is_legacy_payment_transaction:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 179
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;
    .locals 3

    .line 192
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$Builder;->is_legacy_payment_transaction:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 176
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$Builder;->build()Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    move-result-object v0

    return-object v0
.end method

.method public is_legacy_payment_transaction(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register$Builder;->is_legacy_payment_transaction:Ljava/lang/Boolean;

    return-object p0
.end method
