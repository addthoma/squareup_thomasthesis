.class final Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$ProtoAdapter_ListManualMergeProposalRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ListManualMergeProposalRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ListManualMergeProposalRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 126
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 146
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;-><init>()V

    .line 147
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 148
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 153
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 151
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/rolodex/ContactSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/ContactSet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->contact_set(Lcom/squareup/protos/client/rolodex/ContactSet;)Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;

    goto :goto_0

    .line 150
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->duplicate_contact_token_set(Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;)Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;

    goto :goto_0

    .line 157
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 158
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 124
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$ProtoAdapter_ListManualMergeProposalRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 139
    sget-object v0, Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 140
    sget-object v0, Lcom/squareup/protos/client/rolodex/ContactSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 141
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 124
    check-cast p2, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$ProtoAdapter_ListManualMergeProposalRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;)I
    .locals 4

    .line 131
    sget-object v0, Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/rolodex/ContactSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    const/4 v3, 0x2

    .line 132
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 124
    check-cast p1, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$ProtoAdapter_ListManualMergeProposalRequest;->encodedSize(Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;)Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;
    .locals 2

    .line 163
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;->newBuilder()Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;

    move-result-object p1

    .line 164
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->duplicate_contact_token_set:Lcom/squareup/protos/client/rolodex/DuplicateContactTokenSet;

    .line 165
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/rolodex/ContactSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/ContactSet;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    .line 166
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 167
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 124
    check-cast p1, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest$ProtoAdapter_ListManualMergeProposalRequest;->redact(Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;)Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;

    move-result-object p1

    return-object p1
.end method
