.class final Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ProtoAdapter_LoyaltyDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_LoyaltyDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2518
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2545
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;-><init>()V

    .line 2546
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2547
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 2563
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2561
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->account_contact(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;

    goto :goto_0

    .line 2555
    :pswitch_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->reason(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 2557
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 2552
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->reward_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;

    goto :goto_0

    .line 2551
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->new_enrollment(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;

    goto :goto_0

    .line 2550
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->rewards_earned(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;

    goto :goto_0

    .line 2549
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->stars_earned(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;

    goto :goto_0

    .line 2567
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2568
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->build()Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2516
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ProtoAdapter_LoyaltyDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2534
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->stars_earned:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2535
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->rewards_earned:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2536
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->new_enrollment:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2537
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reward_name:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2538
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2539
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2540
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2516
    check-cast p2, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ProtoAdapter_LoyaltyDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;)I
    .locals 4

    .line 2523
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->stars_earned:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->rewards_earned:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 2524
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->new_enrollment:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 2525
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reward_name:Ljava/lang/String;

    const/4 v3, 0x4

    .line 2526
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    const/4 v3, 0x5

    .line 2527
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    const/4 v3, 0x6

    .line 2528
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2529
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2516
    check-cast p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ProtoAdapter_LoyaltyDetails;->encodedSize(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;
    .locals 2

    .line 2573
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->newBuilder()Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;

    move-result-object p1

    .line 2574
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    .line 2575
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2576
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->build()Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2516
    check-cast p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ProtoAdapter_LoyaltyDetails;->redact(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    move-result-object p1

    return-object p1
.end method
