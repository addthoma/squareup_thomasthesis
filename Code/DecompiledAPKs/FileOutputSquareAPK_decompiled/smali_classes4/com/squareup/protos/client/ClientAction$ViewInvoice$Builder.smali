.class public final Lcom/squareup/protos/client/ClientAction$ViewInvoice$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ClientAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/ClientAction$ViewInvoice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/ClientAction$ViewInvoice;",
        "Lcom/squareup/protos/client/ClientAction$ViewInvoice$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public invoice_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2015
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/ClientAction$ViewInvoice;
    .locals 3

    .line 2028
    new-instance v0, Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    iget-object v1, p0, Lcom/squareup/protos/client/ClientAction$ViewInvoice$Builder;->invoice_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/ClientAction$ViewInvoice;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2012
    invoke-virtual {p0}, Lcom/squareup/protos/client/ClientAction$ViewInvoice$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    move-result-object v0

    return-object v0
.end method

.method public invoice_id(Ljava/lang/String;)Lcom/squareup/protos/client/ClientAction$ViewInvoice$Builder;
    .locals 0

    .line 2022
    iput-object p1, p0, Lcom/squareup/protos/client/ClientAction$ViewInvoice$Builder;->invoice_id:Ljava/lang/String;

    return-object p0
.end method
