.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public note_deprecated:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1388
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;
    .locals 3

    .line 1402
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order$Builder;->note_deprecated:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1385
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    move-result-object v0

    return-object v0
.end method

.method public note_deprecated(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1396
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order$Builder;->note_deprecated:Ljava/lang/String;

    return-object p0
.end method
