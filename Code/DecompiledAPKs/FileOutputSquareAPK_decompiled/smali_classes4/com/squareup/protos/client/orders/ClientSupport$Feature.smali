.class public final enum Lcom/squareup/protos/client/orders/ClientSupport$Feature;
.super Ljava/lang/Enum;
.source "ClientSupport.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/ClientSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Feature"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/orders/ClientSupport$Feature$ProtoAdapter_Feature;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/orders/ClientSupport$Feature;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/orders/ClientSupport$Feature;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/orders/ClientSupport$Feature;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CAVIAR_MANAGED_DELIVERY:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

.field public static final enum CAVIAR_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

.field public static final enum FRACTIONAL_QUANTITIES:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

.field public static final enum SQUARE_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

.field public static final enum SQUARE_SHIPMENT:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

.field public static final enum WEEBLY_DELIVERY:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

.field public static final enum WEEBLY_DIGITAL:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

.field public static final enum WEEBLY_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

.field public static final enum WEEBLY_SHIPMENT:Lcom/squareup/protos/client/orders/ClientSupport$Feature;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 162
    new-instance v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/orders/ClientSupport$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->UNKNOWN:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    .line 167
    new-instance v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    const/4 v2, 0x1

    const-string v3, "CAVIAR_PICKUP"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/orders/ClientSupport$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->CAVIAR_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    .line 172
    new-instance v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    const/4 v3, 0x2

    const-string v4, "CAVIAR_MANAGED_DELIVERY"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/orders/ClientSupport$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->CAVIAR_MANAGED_DELIVERY:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    .line 177
    new-instance v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    const/4 v4, 0x3

    const-string v5, "WEEBLY_SHIPMENT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/orders/ClientSupport$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_SHIPMENT:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    .line 182
    new-instance v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    const/4 v5, 0x4

    const-string v6, "WEEBLY_PICKUP"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/orders/ClientSupport$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    .line 187
    new-instance v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    const/4 v6, 0x5

    const-string v7, "SQUARE_PICKUP"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/orders/ClientSupport$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->SQUARE_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    .line 192
    new-instance v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    const/4 v7, 0x6

    const-string v8, "WEEBLY_DIGITAL"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/orders/ClientSupport$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_DIGITAL:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    .line 197
    new-instance v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    const/4 v8, 0x7

    const-string v9, "FRACTIONAL_QUANTITIES"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/orders/ClientSupport$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->FRACTIONAL_QUANTITIES:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    .line 202
    new-instance v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    const/16 v9, 0x8

    const/16 v10, 0x9

    const-string v11, "SQUARE_SHIPMENT"

    invoke-direct {v0, v11, v9, v10}, Lcom/squareup/protos/client/orders/ClientSupport$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->SQUARE_SHIPMENT:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    .line 207
    new-instance v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    const/16 v11, 0xa

    const-string v12, "WEEBLY_DELIVERY"

    invoke-direct {v0, v12, v10, v11}, Lcom/squareup/protos/client/orders/ClientSupport$Feature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_DELIVERY:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    new-array v0, v11, [Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    .line 161
    sget-object v11, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->UNKNOWN:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v11, v0, v1

    sget-object v1, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->CAVIAR_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->CAVIAR_MANAGED_DELIVERY:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_SHIPMENT:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->SQUARE_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_DIGITAL:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->FRACTIONAL_QUANTITIES:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->SQUARE_SHIPMENT:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_DELIVERY:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    aput-object v1, v0, v10

    sput-object v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->$VALUES:[Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    .line 209
    new-instance v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature$ProtoAdapter_Feature;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/ClientSupport$Feature$ProtoAdapter_Feature;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 213
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 214
    iput p3, p0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/orders/ClientSupport$Feature;
    .locals 0

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/4 p0, 0x0

    return-object p0

    .line 231
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_DELIVERY:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    return-object p0

    .line 230
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->SQUARE_SHIPMENT:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    return-object p0

    .line 229
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->FRACTIONAL_QUANTITIES:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    return-object p0

    .line 228
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_DIGITAL:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    return-object p0

    .line 227
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->SQUARE_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    return-object p0

    .line 226
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    return-object p0

    .line 225
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->WEEBLY_SHIPMENT:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    return-object p0

    .line 224
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->CAVIAR_MANAGED_DELIVERY:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    return-object p0

    .line 223
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->CAVIAR_PICKUP:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    return-object p0

    .line 222
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->UNKNOWN:Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/orders/ClientSupport$Feature;
    .locals 1

    .line 161
    const-class v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/orders/ClientSupport$Feature;
    .locals 1

    .line 161
    sget-object v0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->$VALUES:[Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/orders/ClientSupport$Feature;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/orders/ClientSupport$Feature;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 238
    iget v0, p0, Lcom/squareup/protos/client/orders/ClientSupport$Feature;->value:I

    return v0
.end method
