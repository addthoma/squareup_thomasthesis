.class public final Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetConversationResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/dialogue/GetConversationResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/dialogue/GetConversationResponse;",
        "Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public conversation:Lcom/squareup/protos/client/dialogue/Conversation;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/dialogue/GetConversationResponse;
    .locals 4

    .line 110
    new-instance v0, Lcom/squareup/protos/client/dialogue/GetConversationResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/dialogue/GetConversationResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/dialogue/Conversation;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;->build()Lcom/squareup/protos/client/dialogue/GetConversationResponse;

    move-result-object v0

    return-object v0
.end method

.method public conversation(Lcom/squareup/protos/client/dialogue/Conversation;)Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/GetConversationResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
