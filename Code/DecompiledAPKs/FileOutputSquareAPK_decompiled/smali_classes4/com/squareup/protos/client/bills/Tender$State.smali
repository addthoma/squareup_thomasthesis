.class public final enum Lcom/squareup/protos/client/bills/Tender$State;
.super Ljava/lang/Enum;
.source "Tender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Tender$State$ProtoAdapter_State;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/Tender$State;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/Tender$State;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Tender$State;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AWAITING_CUSTOMER_TIP:Lcom/squareup/protos/client/bills/Tender$State;

.field public static final enum AWAITING_MERCHANT_TIP:Lcom/squareup/protos/client/bills/Tender$State;

.field public static final enum LOST:Lcom/squareup/protos/client/bills/Tender$State;

.field public static final enum SETTLED:Lcom/squareup/protos/client/bills/Tender$State;

.field public static final enum UNKNOWN_STATE:Lcom/squareup/protos/client/bills/Tender$State;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 801
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$State;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_STATE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/Tender$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$State;->UNKNOWN_STATE:Lcom/squareup/protos/client/bills/Tender$State;

    .line 808
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$State;

    const/4 v2, 0x1

    const-string v3, "LOST"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/Tender$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$State;->LOST:Lcom/squareup/protos/client/bills/Tender$State;

    .line 813
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$State;

    const/4 v3, 0x2

    const-string v4, "AWAITING_MERCHANT_TIP"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/Tender$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$State;->AWAITING_MERCHANT_TIP:Lcom/squareup/protos/client/bills/Tender$State;

    .line 818
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$State;

    const/4 v4, 0x3

    const-string v5, "AWAITING_CUSTOMER_TIP"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/Tender$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$State;->AWAITING_CUSTOMER_TIP:Lcom/squareup/protos/client/bills/Tender$State;

    .line 823
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$State;

    const/4 v5, 0x4

    const-string v6, "SETTLED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bills/Tender$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$State;->SETTLED:Lcom/squareup/protos/client/bills/Tender$State;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/client/bills/Tender$State;

    .line 800
    sget-object v6, Lcom/squareup/protos/client/bills/Tender$State;->UNKNOWN_STATE:Lcom/squareup/protos/client/bills/Tender$State;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$State;->LOST:Lcom/squareup/protos/client/bills/Tender$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$State;->AWAITING_MERCHANT_TIP:Lcom/squareup/protos/client/bills/Tender$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$State;->AWAITING_CUSTOMER_TIP:Lcom/squareup/protos/client/bills/Tender$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$State;->SETTLED:Lcom/squareup/protos/client/bills/Tender$State;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$State;->$VALUES:[Lcom/squareup/protos/client/bills/Tender$State;

    .line 825
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$State$ProtoAdapter_State;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$State$ProtoAdapter_State;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 829
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 830
    iput p3, p0, Lcom/squareup/protos/client/bills/Tender$State;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/Tender$State;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 842
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$State;->SETTLED:Lcom/squareup/protos/client/bills/Tender$State;

    return-object p0

    .line 841
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$State;->AWAITING_CUSTOMER_TIP:Lcom/squareup/protos/client/bills/Tender$State;

    return-object p0

    .line 840
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$State;->AWAITING_MERCHANT_TIP:Lcom/squareup/protos/client/bills/Tender$State;

    return-object p0

    .line 839
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$State;->LOST:Lcom/squareup/protos/client/bills/Tender$State;

    return-object p0

    .line 838
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$State;->UNKNOWN_STATE:Lcom/squareup/protos/client/bills/Tender$State;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$State;
    .locals 1

    .line 800
    const-class v0, Lcom/squareup/protos/client/bills/Tender$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/Tender$State;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/Tender$State;
    .locals 1

    .line 800
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$State;->$VALUES:[Lcom/squareup/protos/client/bills/Tender$State;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/Tender$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/Tender$State;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 849
    iget v0, p0, Lcom/squareup/protos/client/bills/Tender$State;->value:I

    return v0
.end method
