.class final Lcom/squareup/protos/client/bills/CardData$X2Card$ProtoAdapter_X2Card;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData$X2Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_X2Card"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/CardData$X2Card;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2030
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/CardData$X2Card;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardData$X2Card;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2049
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;-><init>()V

    .line 2050
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2051
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 2056
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2054
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;->encrypted_swipe_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;

    goto :goto_0

    .line 2053
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;->encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;

    goto :goto_0

    .line 2060
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2061
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$X2Card;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2028
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$X2Card$ProtoAdapter_X2Card;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardData$X2Card;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardData$X2Card;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2042
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData$X2Card;->encrypted_reader_data:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2043
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData$X2Card;->encrypted_swipe_data:Lokio/ByteString;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2044
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CardData$X2Card;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2028
    check-cast p2, Lcom/squareup/protos/client/bills/CardData$X2Card;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/CardData$X2Card$ProtoAdapter_X2Card;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardData$X2Card;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/CardData$X2Card;)I
    .locals 4

    .line 2035
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$X2Card;->encrypted_reader_data:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData$X2Card;->encrypted_swipe_data:Lokio/ByteString;

    const/4 v3, 0x2

    .line 2036
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2037
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$X2Card;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2028
    check-cast p1, Lcom/squareup/protos/client/bills/CardData$X2Card;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$X2Card$ProtoAdapter_X2Card;->encodedSize(Lcom/squareup/protos/client/bills/CardData$X2Card;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/CardData$X2Card;)Lcom/squareup/protos/client/bills/CardData$X2Card;
    .locals 1

    .line 2066
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$X2Card;->newBuilder()Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 2067
    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;->encrypted_reader_data:Lokio/ByteString;

    .line 2068
    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;->encrypted_swipe_data:Lokio/ByteString;

    .line 2069
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2070
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$X2Card;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2028
    check-cast p1, Lcom/squareup/protos/client/bills/CardData$X2Card;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$X2Card$ProtoAdapter_X2Card;->redact(Lcom/squareup/protos/client/bills/CardData$X2Card;)Lcom/squareup/protos/client/bills/CardData$X2Card;

    move-result-object p1

    return-object p1
.end method
