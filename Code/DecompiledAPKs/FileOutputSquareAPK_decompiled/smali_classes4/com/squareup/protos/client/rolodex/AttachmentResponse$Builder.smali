.class public final Lcom/squareup/protos/client/rolodex/AttachmentResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AttachmentResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/AttachmentResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/AttachmentResponse;",
        "Lcom/squareup/protos/client/rolodex/AttachmentResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attachment_token:Ljava/lang/String;

.field public error:Lcom/squareup/protos/client/rolodex/Error;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public attachment_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttachmentResponse$Builder;
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttachmentResponse$Builder;->attachment_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/AttachmentResponse;
    .locals 4

    .line 110
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttachmentResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttachmentResponse$Builder;->attachment_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/AttachmentResponse$Builder;->error:Lcom/squareup/protos/client/rolodex/Error;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/AttachmentResponse;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Error;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttachmentResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/AttachmentResponse;

    move-result-object v0

    return-object v0
.end method

.method public error(Lcom/squareup/protos/client/rolodex/Error;)Lcom/squareup/protos/client/rolodex/AttachmentResponse$Builder;
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttachmentResponse$Builder;->error:Lcom/squareup/protos/client/rolodex/Error;

    return-object p0
.end method
