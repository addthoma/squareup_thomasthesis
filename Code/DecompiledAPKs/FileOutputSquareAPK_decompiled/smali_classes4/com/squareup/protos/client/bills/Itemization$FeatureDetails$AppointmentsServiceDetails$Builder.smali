.class public final Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public intermissions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;"
        }
    .end annotation
.end field

.field public no_show_fee_money:Lcom/squareup/protos/common/Money;

.field public ordinal:Ljava/lang/Integer;

.field public pricing_type:Lcom/squareup/api/items/PricingType;

.field public resource_tokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public service_duration:Ljava/lang/Long;

.field public service_id:Ljava/lang/String;

.field public transition_time_duration:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 4839
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 4840
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->intermissions:Ljava/util/List;

    .line 4841
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->resource_tokens:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;
    .locals 11

    .line 4912
    new-instance v10, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->service_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->service_duration:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->ordinal:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->transition_time_duration:Ljava/lang/Long;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->pricing_type:Lcom/squareup/api/items/PricingType;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->no_show_fee_money:Lcom/squareup/protos/common/Money;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->intermissions:Ljava/util/List;

    iget-object v8, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->resource_tokens:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Lcom/squareup/api/items/PricingType;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4822
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    move-result-object v0

    return-object v0
.end method

.method public intermissions(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;"
        }
    .end annotation

    .line 4896
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 4897
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->intermissions:Ljava/util/List;

    return-object p0
.end method

.method public no_show_fee_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;
    .locals 0

    .line 4888
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->no_show_fee_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;
    .locals 0

    .line 4864
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public pricing_type(Lcom/squareup/api/items/PricingType;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;
    .locals 0

    .line 4880
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->pricing_type:Lcom/squareup/api/items/PricingType;

    return-object p0
.end method

.method public resource_tokens(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;"
        }
    .end annotation

    .line 4905
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 4906
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->resource_tokens:Ljava/util/List;

    return-object p0
.end method

.method public service_duration(Ljava/lang/Long;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;
    .locals 0

    .line 4856
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->service_duration:Ljava/lang/Long;

    return-object p0
.end method

.method public service_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;
    .locals 0

    .line 4848
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->service_id:Ljava/lang/String;

    return-object p0
.end method

.method public transition_time_duration(Ljava/lang/Long;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;
    .locals 0

    .line 4872
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->transition_time_duration:Ljava/lang/Long;

    return-object p0
.end method
