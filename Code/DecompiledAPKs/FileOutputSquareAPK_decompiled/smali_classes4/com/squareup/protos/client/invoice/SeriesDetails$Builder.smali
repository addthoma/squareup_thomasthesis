.class public final Lcom/squareup/protos/client/invoice/SeriesDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SeriesDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/SeriesDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/SeriesDetails;",
        "Lcom/squareup/protos/client/invoice/SeriesDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public recurrence_rule:Ljava/lang/String;

.field public recurrence_rule_start_date:Lcom/squareup/protos/common/time/YearMonthDay;

.field public recurrence_rule_text:Lcom/squareup/protos/client/invoice/RecurrenceRuleText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 120
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/SeriesDetails;
    .locals 5

    .line 149
    new-instance v0, Lcom/squareup/protos/client/invoice/SeriesDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/SeriesDetails$Builder;->recurrence_rule_start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/SeriesDetails$Builder;->recurrence_rule:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/SeriesDetails$Builder;->recurrence_rule_text:Lcom/squareup/protos/client/invoice/RecurrenceRuleText;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/invoice/SeriesDetails;-><init>(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/lang/String;Lcom/squareup/protos/client/invoice/RecurrenceRuleText;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 113
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/SeriesDetails$Builder;->build()Lcom/squareup/protos/client/invoice/SeriesDetails;

    move-result-object v0

    return-object v0
.end method

.method public recurrence_rule(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/SeriesDetails$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/SeriesDetails$Builder;->recurrence_rule:Ljava/lang/String;

    return-object p0
.end method

.method public recurrence_rule_start_date(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/SeriesDetails$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/SeriesDetails$Builder;->recurrence_rule_start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object p0
.end method

.method public recurrence_rule_text(Lcom/squareup/protos/client/invoice/RecurrenceRuleText;)Lcom/squareup/protos/client/invoice/SeriesDetails$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/SeriesDetails$Builder;->recurrence_rule_text:Lcom/squareup/protos/client/invoice/RecurrenceRuleText;

    return-object p0
.end method
