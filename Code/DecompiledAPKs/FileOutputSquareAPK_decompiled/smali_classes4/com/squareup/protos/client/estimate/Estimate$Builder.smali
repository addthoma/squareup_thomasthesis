.class public final Lcom/squareup/protos/client/estimate/Estimate$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Estimate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/Estimate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/estimate/Estimate;",
        "Lcom/squareup/protos/client/estimate/Estimate$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public additional_recipient_email:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public attachment:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;"
        }
    .end annotation
.end field

.field public cart:Lcom/squareup/protos/client/bills/Cart;

.field public creator_details:Lcom/squareup/protos/client/CreatorDetails;

.field public delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

.field public description:Ljava/lang/String;

.field public estimate_name:Ljava/lang/String;

.field public expires_on:Lcom/squareup/protos/common/time/YearMonthDay;

.field public id_pair:Lcom/squareup/protos/client/IdPair;

.field public merchant_estimate_number:Ljava/lang/String;

.field public packages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/estimate/EstimatePackage;",
            ">;"
        }
    .end annotation
.end field

.field public payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

.field public scheduled_on:Lcom/squareup/protos/common/time/YearMonthDay;

.field public version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 328
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 329
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->additional_recipient_email:Ljava/util/List;

    .line 330
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->attachment:Ljava/util/List;

    .line 331
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->packages:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public additional_recipient_email(Ljava/util/List;)Lcom/squareup/protos/client/estimate/Estimate$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/estimate/Estimate$Builder;"
        }
    .end annotation

    .line 386
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 387
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->additional_recipient_email:Ljava/util/List;

    return-object p0
.end method

.method public attachment(Ljava/util/List;)Lcom/squareup/protos/client/estimate/Estimate$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;)",
            "Lcom/squareup/protos/client/estimate/Estimate$Builder;"
        }
    .end annotation

    .line 436
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 437
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->attachment:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/estimate/Estimate;
    .locals 18

    move-object/from16 v0, p0

    .line 452
    new-instance v17, Lcom/squareup/protos/client/estimate/Estimate;

    iget-object v2, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->description:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->merchant_estimate_number:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->estimate_name:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->expires_on:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v7, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v8, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->additional_recipient_email:Ljava/util/List;

    iget-object v9, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    iget-object v10, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v11, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v12, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->version:Ljava/lang/Integer;

    iget-object v13, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->scheduled_on:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v14, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->attachment:Ljava/util/List;

    iget-object v15, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->packages:Ljava/util/List;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v16

    move-object/from16 v1, v17

    invoke-direct/range {v1 .. v16}, Lcom/squareup/protos/client/estimate/Estimate;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/client/invoice/InvoiceContact;Ljava/util/List;Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/CreatorDetails;Ljava/lang/Integer;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v17
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 299
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/Estimate$Builder;->build()Lcom/squareup/protos/client/estimate/Estimate;

    move-result-object v0

    return-object v0
.end method

.method public cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/estimate/Estimate$Builder;
    .locals 0

    .line 403
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object p0
.end method

.method public creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/estimate/Estimate$Builder;
    .locals 0

    .line 411
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public delivery_method(Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;)Lcom/squareup/protos/client/estimate/Estimate$Builder;
    .locals 0

    .line 395
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/Estimate$Builder;
    .locals 0

    .line 346
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public estimate_name(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/Estimate$Builder;
    .locals 0

    .line 362
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->estimate_name:Ljava/lang/String;

    return-object p0
.end method

.method public expires_on(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/estimate/Estimate$Builder;
    .locals 0

    .line 370
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->expires_on:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object p0
.end method

.method public id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/estimate/Estimate$Builder;
    .locals 0

    .line 338
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public merchant_estimate_number(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/Estimate$Builder;
    .locals 0

    .line 354
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->merchant_estimate_number:Ljava/lang/String;

    return-object p0
.end method

.method public packages(Ljava/util/List;)Lcom/squareup/protos/client/estimate/Estimate$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/estimate/EstimatePackage;",
            ">;)",
            "Lcom/squareup/protos/client/estimate/Estimate$Builder;"
        }
    .end annotation

    .line 445
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 446
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->packages:Ljava/util/List;

    return-object p0
.end method

.method public payer(Lcom/squareup/protos/client/invoice/InvoiceContact;)Lcom/squareup/protos/client/estimate/Estimate$Builder;
    .locals 0

    .line 378
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    return-object p0
.end method

.method public scheduled_on(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/estimate/Estimate$Builder;
    .locals 0

    .line 428
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->scheduled_on:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object p0
.end method

.method public version(Ljava/lang/Integer;)Lcom/squareup/protos/client/estimate/Estimate$Builder;
    .locals 0

    .line 420
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->version:Ljava/lang/Integer;

    return-object p0
.end method
