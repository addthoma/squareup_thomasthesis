.class public final enum Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;
.super Ljava/lang/Enum;
.source "StartTwoFactorAuthenticationRequest.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TwoFactorAuthenticationRequestType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType$ProtoAdapter_TwoFactorAuthenticationRequestType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CARD_ACTIVATION:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

.field public static final enum RESET_PASSCODE:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

.field public static final enum SHOW_PRIVATE_CARD_DATA:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 133
    new-instance v0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    .line 135
    new-instance v0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    const/4 v2, 0x1

    const-string v3, "CARD_ACTIVATION"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->CARD_ACTIVATION:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    .line 137
    new-instance v0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    const/4 v3, 0x2

    const-string v4, "RESET_PASSCODE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->RESET_PASSCODE:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    .line 139
    new-instance v0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    const/4 v4, 0x3

    const-string v5, "SHOW_PRIVATE_CARD_DATA"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->SHOW_PRIVATE_CARD_DATA:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    .line 132
    sget-object v5, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->CARD_ACTIVATION:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->RESET_PASSCODE:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->SHOW_PRIVATE_CARD_DATA:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->$VALUES:[Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    .line 141
    new-instance v0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType$ProtoAdapter_TwoFactorAuthenticationRequestType;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType$ProtoAdapter_TwoFactorAuthenticationRequestType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 145
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 146
    iput p3, p0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 157
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->SHOW_PRIVATE_CARD_DATA:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    return-object p0

    .line 156
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->RESET_PASSCODE:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    return-object p0

    .line 155
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->CARD_ACTIVATION:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    return-object p0

    .line 154
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;
    .locals 1

    .line 132
    const-class v0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;
    .locals 1

    .line 132
    sget-object v0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->$VALUES:[Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 164
    iget v0, p0, Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest$TwoFactorAuthenticationRequestType;->value:I

    return v0
.end method
