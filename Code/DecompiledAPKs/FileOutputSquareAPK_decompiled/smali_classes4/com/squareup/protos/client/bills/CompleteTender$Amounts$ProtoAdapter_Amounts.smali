.class final Lcom/squareup/protos/client/bills/CompleteTender$Amounts$ProtoAdapter_Amounts;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CompleteTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CompleteTender$Amounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Amounts"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/CompleteTender$Amounts;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 302
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 323
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;-><init>()V

    .line 324
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 325
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 331
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 329
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->tip_percentage(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    goto :goto_0

    .line 328
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->total_charged_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    goto :goto_0

    .line 327
    :cond_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    goto :goto_0

    .line 335
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 336
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 300
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$ProtoAdapter_Amounts;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CompleteTender$Amounts;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 315
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 316
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->total_charged_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 317
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 318
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 300
    check-cast p2, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$ProtoAdapter_Amounts;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CompleteTender$Amounts;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/CompleteTender$Amounts;)I
    .locals 4

    .line 307
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->total_charged_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x2

    .line 308
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    const/4 v3, 0x3

    .line 309
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 310
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 300
    check-cast p1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$ProtoAdapter_Amounts;->encodedSize(Lcom/squareup/protos/client/bills/CompleteTender$Amounts;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/CompleteTender$Amounts;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts;
    .locals 2

    .line 341
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->newBuilder()Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    move-result-object p1

    .line 342
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    .line 343
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->total_charged_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->total_charged_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->total_charged_money:Lcom/squareup/protos/common/Money;

    .line 344
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 345
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 300
    check-cast p1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$ProtoAdapter_Amounts;->redact(Lcom/squareup/protos/client/bills/CompleteTender$Amounts;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    move-result-object p1

    return-object p1
.end method
