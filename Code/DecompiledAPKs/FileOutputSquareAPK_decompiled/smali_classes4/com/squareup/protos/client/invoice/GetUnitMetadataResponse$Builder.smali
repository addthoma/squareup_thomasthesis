.class public final Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetUnitMetadataResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;",
        "Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

.field public status:Lcom/squareup/protos/client/Status;

.field public unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;
    .locals 5

    .line 135
    new-instance v0, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/invoice/UnitMetadata;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->build()Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;

    move-result-object v0

    return-object v0
.end method

.method public metadata(Lcom/squareup/protos/client/invoice/UnitMetadata;)Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public unit_metadata(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;)Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    return-object p0
.end method
