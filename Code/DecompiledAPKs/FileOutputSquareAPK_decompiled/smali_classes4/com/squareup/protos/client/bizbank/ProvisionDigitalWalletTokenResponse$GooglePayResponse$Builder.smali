.class public final Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ProvisionDigitalWalletTokenResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public opaque_payment_card:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 387
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;
    .locals 3

    .line 400
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse$Builder;->opaque_payment_card:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 384
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;

    move-result-object v0

    return-object v0
.end method

.method public opaque_payment_card(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse$Builder;
    .locals 0

    .line 394
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse$Builder;->opaque_payment_card:Ljava/lang/String;

    return-object p0
.end method
