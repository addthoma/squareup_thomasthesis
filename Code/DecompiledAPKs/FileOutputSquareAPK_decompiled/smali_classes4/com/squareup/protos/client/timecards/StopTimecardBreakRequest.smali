.class public final Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;
.super Lcom/squareup/wire/Message;
.source "StopTimecardBreakRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$ProtoAdapter_StopTimecardBreakRequest;,
        Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;",
        "Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SHOULD_BLOCK_EARLY_CLOCKIN:Ljava/lang/Boolean;

.field public static final DEFAULT_TIMECARD_BREAK_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final authorization:Lcom/squareup/protos/client/timecards/Authorization;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.Authorization#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final should_block_early_clockin:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final timecard_break_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$ProtoAdapter_StopTimecardBreakRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$ProtoAdapter_StopTimecardBreakRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->DEFAULT_SHOULD_BLOCK_EARLY_CLOCKIN:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/timecards/Authorization;Ljava/lang/Boolean;)V
    .locals 1

    .line 60
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/timecards/Authorization;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/timecards/Authorization;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 66
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->timecard_break_token:Ljava/lang/String;

    .line 67
    iput-object p2, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->authorization:Lcom/squareup/protos/client/timecards/Authorization;

    .line 68
    iput-object p3, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->should_block_early_clockin:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 84
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 85
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->timecard_break_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->timecard_break_token:Ljava/lang/String;

    .line 87
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->authorization:Lcom/squareup/protos/client/timecards/Authorization;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->authorization:Lcom/squareup/protos/client/timecards/Authorization;

    .line 88
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->should_block_early_clockin:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->should_block_early_clockin:Ljava/lang/Boolean;

    .line 89
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 94
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->timecard_break_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->authorization:Lcom/squareup/protos/client/timecards/Authorization;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/Authorization;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->should_block_early_clockin:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 100
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;
    .locals 2

    .line 73
    new-instance v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;-><init>()V

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->timecard_break_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;->timecard_break_token:Ljava/lang/String;

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->authorization:Lcom/squareup/protos/client/timecards/Authorization;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;->authorization:Lcom/squareup/protos/client/timecards/Authorization;

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->should_block_early_clockin:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;->should_block_early_clockin:Ljava/lang/Boolean;

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->newBuilder()Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->timecard_break_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", timecard_break_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->timecard_break_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->authorization:Lcom/squareup/protos/client/timecards/Authorization;

    if-eqz v1, :cond_1

    const-string v1, ", authorization="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->authorization:Lcom/squareup/protos/client/timecards/Authorization;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 110
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->should_block_early_clockin:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", should_block_early_clockin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakRequest;->should_block_early_clockin:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "StopTimecardBreakRequest{"

    .line 111
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
