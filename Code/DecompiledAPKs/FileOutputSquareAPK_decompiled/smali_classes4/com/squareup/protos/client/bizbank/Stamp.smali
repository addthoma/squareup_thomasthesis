.class public final Lcom/squareup/protos/client/bizbank/Stamp;
.super Lcom/squareup/wire/Message;
.source "Stamp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/Stamp$ProtoAdapter_Stamp;,
        Lcom/squareup/protos/client/bizbank/Stamp$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bizbank/Stamp;",
        "Lcom/squareup/protos/client/bizbank/Stamp$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/Stamp;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACCESSIBILITY_TEXT:Ljava/lang/String; = ""

.field public static final DEFAULT_MIN_SCALE:Ljava/lang/Integer;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_SVG:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final accessibility_text:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final min_scale:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final svg:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/bizbank/Stamp$ProtoAdapter_Stamp;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/Stamp$ProtoAdapter_Stamp;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/Stamp;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 34
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bizbank/Stamp;->DEFAULT_MIN_SCALE:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 6

    .line 75
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bizbank/Stamp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 80
    sget-object v0, Lcom/squareup/protos/client/bizbank/Stamp;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 81
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->name:Ljava/lang/String;

    .line 82
    iput-object p2, p0, Lcom/squareup/protos/client/bizbank/Stamp;->accessibility_text:Ljava/lang/String;

    .line 83
    iput-object p3, p0, Lcom/squareup/protos/client/bizbank/Stamp;->svg:Ljava/lang/String;

    .line 84
    iput-object p4, p0, Lcom/squareup/protos/client/bizbank/Stamp;->min_scale:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 101
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bizbank/Stamp;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 102
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bizbank/Stamp;

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/Stamp;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/Stamp;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/Stamp;->name:Ljava/lang/String;

    .line 104
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->accessibility_text:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/Stamp;->accessibility_text:Ljava/lang/String;

    .line 105
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->svg:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/Stamp;->svg:Ljava/lang/String;

    .line 106
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->min_scale:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/Stamp;->min_scale:Ljava/lang/Integer;

    .line 107
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 112
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/Stamp;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->accessibility_text:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->svg:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->min_scale:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 119
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bizbank/Stamp$Builder;
    .locals 2

    .line 89
    new-instance v0, Lcom/squareup/protos/client/bizbank/Stamp$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/Stamp$Builder;-><init>()V

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/Stamp$Builder;->name:Ljava/lang/String;

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->accessibility_text:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/Stamp$Builder;->accessibility_text:Ljava/lang/String;

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->svg:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/Stamp$Builder;->svg:Ljava/lang/String;

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->min_scale:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/Stamp$Builder;->min_scale:Ljava/lang/Integer;

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/Stamp;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/Stamp$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/Stamp;->newBuilder()Lcom/squareup/protos/client/bizbank/Stamp$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->accessibility_text:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", accessibility_text=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->svg:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", svg=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->min_scale:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", min_scale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/Stamp;->min_scale:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Stamp{"

    .line 131
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
