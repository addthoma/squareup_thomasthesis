.class public final Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DepositInterval.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/depositsettings/DepositInterval;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/depositsettings/DepositInterval;",
        "Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cutoff_at:Lcom/squareup/protos/common/time/DayTime;

.field public initiate_at:Lcom/squareup/protos/common/time/DayTime;

.field public same_day_settlement_enabled:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 121
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/depositsettings/DepositInterval;
    .locals 5

    .line 150
    new-instance v0, Lcom/squareup/protos/client/depositsettings/DepositInterval;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->cutoff_at:Lcom/squareup/protos/common/time/DayTime;

    iget-object v2, p0, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->initiate_at:Lcom/squareup/protos/common/time/DayTime;

    iget-object v3, p0, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->same_day_settlement_enabled:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/depositsettings/DepositInterval;-><init>(Lcom/squareup/protos/common/time/DayTime;Lcom/squareup/protos/common/time/DayTime;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->build()Lcom/squareup/protos/client/depositsettings/DepositInterval;

    move-result-object v0

    return-object v0
.end method

.method public cutoff_at(Lcom/squareup/protos/common/time/DayTime;)Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->cutoff_at:Lcom/squareup/protos/common/time/DayTime;

    return-object p0
.end method

.method public initiate_at(Lcom/squareup/protos/common/time/DayTime;)Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->initiate_at:Lcom/squareup/protos/common/time/DayTime;

    return-object p0
.end method

.method public same_day_settlement_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/DepositInterval$Builder;->same_day_settlement_enabled:Ljava/lang/Boolean;

    return-object p0
.end method
