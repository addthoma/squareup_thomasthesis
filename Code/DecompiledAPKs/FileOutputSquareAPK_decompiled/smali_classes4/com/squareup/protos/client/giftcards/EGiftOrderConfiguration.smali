.class public final Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;
.super Lcom/squareup/wire/Message;
.source "EGiftOrderConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$ProtoAdapter_EGiftOrderConfiguration;,
        Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
        "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CUSTOM_POLICY:Ljava/lang/String; = ""

.field public static final DEFAULT_DISABLED_IN_STORE:Ljava/lang/Boolean;

.field public static final DEFAULT_DISABLED_ONLINE:Ljava/lang/Boolean;

.field public static final DEFAULT_HOST_UNIT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final all_egift_themes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.giftcards.EGiftTheme#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final custom_policy:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final denomination_amounts:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field public final disabled_in_store:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final disabled_online:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final enabled_theme_tokens:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final host_unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final max_load_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final min_load_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$ProtoAdapter_EGiftOrderConfiguration;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$ProtoAdapter_EGiftOrderConfiguration;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 35
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->DEFAULT_DISABLED_ONLINE:Ljava/lang/Boolean;

    .line 37
    sput-object v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->DEFAULT_DISABLED_IN_STORE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 134
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 141
    sget-object v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->host_unit_token:Ljava/lang/String;

    const-string p1, "denomination_amounts"

    .line 143
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->denomination_amounts:Ljava/util/List;

    const-string p1, "all_egift_themes"

    .line 144
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->all_egift_themes:Ljava/util/List;

    const-string p1, "enabled_theme_tokens"

    .line 145
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->enabled_theme_tokens:Ljava/util/List;

    .line 146
    iput-object p5, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->custom_policy:Ljava/lang/String;

    .line 147
    iput-object p6, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->min_load_amount:Lcom/squareup/protos/common/Money;

    .line 148
    iput-object p7, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->max_load_amount:Lcom/squareup/protos/common/Money;

    .line 149
    iput-object p8, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->disabled_online:Ljava/lang/Boolean;

    .line 150
    iput-object p9, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->disabled_in_store:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 172
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 173
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    .line 174
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->host_unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->host_unit_token:Ljava/lang/String;

    .line 175
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->denomination_amounts:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->denomination_amounts:Ljava/util/List;

    .line 176
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->all_egift_themes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->all_egift_themes:Ljava/util/List;

    .line 177
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->enabled_theme_tokens:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->enabled_theme_tokens:Ljava/util/List;

    .line 178
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->custom_policy:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->custom_policy:Ljava/lang/String;

    .line 179
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->min_load_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->min_load_amount:Lcom/squareup/protos/common/Money;

    .line 180
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->max_load_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->max_load_amount:Lcom/squareup/protos/common/Money;

    .line 181
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->disabled_online:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->disabled_online:Ljava/lang/Boolean;

    .line 182
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->disabled_in_store:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->disabled_in_store:Ljava/lang/Boolean;

    .line 183
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 188
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 190
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->host_unit_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->denomination_amounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->all_egift_themes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->enabled_theme_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->custom_policy:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->min_load_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->max_load_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->disabled_online:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->disabled_in_store:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 200
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;
    .locals 2

    .line 155
    new-instance v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;-><init>()V

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->host_unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->host_unit_token:Ljava/lang/String;

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->denomination_amounts:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->denomination_amounts:Ljava/util/List;

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->all_egift_themes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->all_egift_themes:Ljava/util/List;

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->enabled_theme_tokens:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->enabled_theme_tokens:Ljava/util/List;

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->custom_policy:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->custom_policy:Ljava/lang/String;

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->min_load_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->min_load_amount:Lcom/squareup/protos/common/Money;

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->max_load_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->max_load_amount:Lcom/squareup/protos/common/Money;

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->disabled_online:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->disabled_online:Ljava/lang/Boolean;

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->disabled_in_store:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->disabled_in_store:Ljava/lang/Boolean;

    .line 165
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->newBuilder()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->host_unit_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", host_unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->host_unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->denomination_amounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", denomination_amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->denomination_amounts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 210
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->all_egift_themes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", all_egift_themes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->all_egift_themes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 211
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->enabled_theme_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", enabled_theme_tokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->enabled_theme_tokens:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 212
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->custom_policy:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", custom_policy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->custom_policy:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->min_load_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", min_load_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->min_load_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 214
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->max_load_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", max_load_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->max_load_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 215
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->disabled_online:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", disabled_online="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->disabled_online:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 216
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->disabled_in_store:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", disabled_in_store="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->disabled_in_store:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "EGiftOrderConfiguration{"

    .line 217
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
