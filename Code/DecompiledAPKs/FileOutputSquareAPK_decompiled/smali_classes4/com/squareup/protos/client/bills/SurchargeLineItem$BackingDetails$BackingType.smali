.class public final enum Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;
.super Ljava/lang/Enum;
.source "SurchargeLineItem.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BackingType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType$ProtoAdapter_BackingType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CUSTOM_SURCHARGE:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

.field public static final enum ITEMS_SERVICE_SURCHARGE:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 283
    new-instance v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    const/4 v1, 0x0

    const-string v2, "ITEMS_SERVICE_SURCHARGE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;->ITEMS_SERVICE_SURCHARGE:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    .line 288
    new-instance v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    const/4 v2, 0x1

    const-string v3, "CUSTOM_SURCHARGE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;->CUSTOM_SURCHARGE:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    .line 279
    sget-object v3, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;->ITEMS_SERVICE_SURCHARGE:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;->CUSTOM_SURCHARGE:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;->$VALUES:[Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    .line 290
    new-instance v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType$ProtoAdapter_BackingType;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType$ProtoAdapter_BackingType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 294
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 295
    iput p3, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 304
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;->CUSTOM_SURCHARGE:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    return-object p0

    .line 303
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;->ITEMS_SERVICE_SURCHARGE:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;
    .locals 1

    .line 279
    const-class v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;
    .locals 1

    .line 279
    sget-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;->$VALUES:[Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 311
    iget v0, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;->value:I

    return v0
.end method
