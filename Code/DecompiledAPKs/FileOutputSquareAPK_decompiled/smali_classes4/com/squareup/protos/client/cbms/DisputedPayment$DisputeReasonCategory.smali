.class public final enum Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;
.super Ljava/lang/Enum;
.source "DisputedPayment.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cbms/DisputedPayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DisputeReasonCategory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory$ProtoAdapter_DisputeReasonCategory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AMOUNT_DIFFERS:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

.field public static final enum CANCELLED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

.field public static final enum COMPLIANCE:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

.field public static final enum CUSTOMER_REQUESTS_CREDIT:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

.field public static final enum DISSATISFIED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

.field public static final enum DUPLICATE:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

.field public static final enum EMV_LIABILITY_SHIFT:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

.field public static final enum FRAUD:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

.field public static final enum NOT_AS_DESCRIBED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

.field public static final enum NOT_RECEIVED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

.field public static final enum NO_KNOWLEDGE:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

.field public static final enum PAID_BY_OTHER_MEANS:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

.field public static final enum UNAUTHORIZED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 434
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->UNKNOWN:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 436
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    const/4 v2, 0x1

    const-string v3, "AMOUNT_DIFFERS"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->AMOUNT_DIFFERS:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 438
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    const/4 v3, 0x2

    const-string v4, "CANCELLED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->CANCELLED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 440
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    const/4 v4, 0x3

    const-string v5, "COMPLIANCE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->COMPLIANCE:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 442
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    const/4 v5, 0x4

    const-string v6, "DISSATISFIED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->DISSATISFIED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 444
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    const/4 v6, 0x5

    const-string v7, "DUPLICATE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->DUPLICATE:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 446
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    const/4 v7, 0x6

    const-string v8, "FRAUD"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->FRAUD:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 448
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    const/4 v8, 0x7

    const-string v9, "NO_KNOWLEDGE"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->NO_KNOWLEDGE:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 450
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    const/16 v9, 0x8

    const-string v10, "NOT_AS_DESCRIBED"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->NOT_AS_DESCRIBED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 452
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    const/16 v10, 0x9

    const-string v11, "NOT_RECEIVED"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->NOT_RECEIVED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 454
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    const/16 v11, 0xa

    const-string v12, "PAID_BY_OTHER_MEANS"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->PAID_BY_OTHER_MEANS:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 456
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    const/16 v12, 0xb

    const-string v13, "CUSTOMER_REQUESTS_CREDIT"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->CUSTOMER_REQUESTS_CREDIT:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 458
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    const/16 v13, 0xc

    const-string v14, "UNAUTHORIZED"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->UNAUTHORIZED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 460
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    const/16 v14, 0xd

    const-string v15, "EMV_LIABILITY_SHIFT"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->EMV_LIABILITY_SHIFT:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    const/16 v0, 0xe

    new-array v0, v0, [Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 433
    sget-object v15, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->UNKNOWN:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    aput-object v15, v0, v1

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->AMOUNT_DIFFERS:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->CANCELLED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->COMPLIANCE:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->DISSATISFIED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->DUPLICATE:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->FRAUD:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->NO_KNOWLEDGE:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->NOT_AS_DESCRIBED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->NOT_RECEIVED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->PAID_BY_OTHER_MEANS:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->CUSTOMER_REQUESTS_CREDIT:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->UNAUTHORIZED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->EMV_LIABILITY_SHIFT:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    aput-object v1, v0, v14

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->$VALUES:[Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 462
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory$ProtoAdapter_DisputeReasonCategory;

    invoke-direct {v0}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory$ProtoAdapter_DisputeReasonCategory;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 466
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 467
    iput p3, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 488
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->EMV_LIABILITY_SHIFT:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    return-object p0

    .line 487
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->UNAUTHORIZED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    return-object p0

    .line 486
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->CUSTOMER_REQUESTS_CREDIT:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    return-object p0

    .line 485
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->PAID_BY_OTHER_MEANS:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    return-object p0

    .line 484
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->NOT_RECEIVED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    return-object p0

    .line 483
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->NOT_AS_DESCRIBED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    return-object p0

    .line 482
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->NO_KNOWLEDGE:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    return-object p0

    .line 481
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->FRAUD:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    return-object p0

    .line 480
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->DUPLICATE:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    return-object p0

    .line 479
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->DISSATISFIED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    return-object p0

    .line 478
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->COMPLIANCE:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    return-object p0

    .line 477
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->CANCELLED:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    return-object p0

    .line 476
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->AMOUNT_DIFFERS:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    return-object p0

    .line 475
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->UNKNOWN:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;
    .locals 1

    .line 433
    const-class v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;
    .locals 1

    .line 433
    sget-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->$VALUES:[Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 495
    iget v0, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->value:I

    return v0
.end method
