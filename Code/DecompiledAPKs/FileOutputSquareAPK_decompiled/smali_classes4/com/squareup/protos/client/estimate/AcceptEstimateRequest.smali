.class public final Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;
.super Lcom/squareup/wire/Message;
.source "AcceptEstimateRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$ProtoAdapter_AcceptEstimateRequest;,
        Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;",
        "Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PACKAGE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_SEND_EMAIL_TO_RECIPIENTS:Ljava/lang/Boolean;

.field public static final DEFAULT_VERSION:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final package_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final send_email_to_recipients:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final token_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$ProtoAdapter_AcceptEstimateRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$ProtoAdapter_AcceptEstimateRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->DEFAULT_VERSION:Ljava/lang/Integer;

    const/4 v0, 0x1

    .line 29
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->DEFAULT_SEND_EMAIL_TO_RECIPIENTS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 6

    .line 65
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 70
    sget-object v0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 71
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->token_pair:Lcom/squareup/protos/client/IdPair;

    .line 72
    iput-object p2, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->version:Ljava/lang/Integer;

    .line 73
    iput-object p3, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    .line 74
    iput-object p4, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->package_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 91
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 92
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->token_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->token_pair:Lcom/squareup/protos/client/IdPair;

    .line 94
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->version:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->version:Ljava/lang/Integer;

    .line 95
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    .line 96
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->package_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->package_token:Ljava/lang/String;

    .line 97
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 102
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->token_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->version:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->package_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 109
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;
    .locals 2

    .line 79
    new-instance v0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;-><init>()V

    .line 80
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->token_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;->token_pair:Lcom/squareup/protos/client/IdPair;

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->version:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;->version:Ljava/lang/Integer;

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;->send_email_to_recipients:Ljava/lang/Boolean;

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->package_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;->package_token:Ljava/lang/String;

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->newBuilder()Lcom/squareup/protos/client/estimate/AcceptEstimateRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->token_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", token_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->token_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->version:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 119
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", send_email_to_recipients="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 120
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->package_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", package_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/AcceptEstimateRequest;->package_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AcceptEstimateRequest{"

    .line 121
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
