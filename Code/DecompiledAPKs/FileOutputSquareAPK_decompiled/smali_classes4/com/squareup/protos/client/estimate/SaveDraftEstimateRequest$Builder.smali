.class public final Lcom/squareup/protos/client/estimate/SaveDraftEstimateRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SaveDraftEstimateRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/SaveDraftEstimateRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/estimate/SaveDraftEstimateRequest;",
        "Lcom/squareup/protos/client/estimate/SaveDraftEstimateRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public estimate:Lcom/squareup/protos/client/estimate/Estimate;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 77
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/estimate/SaveDraftEstimateRequest;
    .locals 3

    .line 87
    new-instance v0, Lcom/squareup/protos/client/estimate/SaveDraftEstimateRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/SaveDraftEstimateRequest$Builder;->estimate:Lcom/squareup/protos/client/estimate/Estimate;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/estimate/SaveDraftEstimateRequest;-><init>(Lcom/squareup/protos/client/estimate/Estimate;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 74
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/SaveDraftEstimateRequest$Builder;->build()Lcom/squareup/protos/client/estimate/SaveDraftEstimateRequest;

    move-result-object v0

    return-object v0
.end method

.method public estimate(Lcom/squareup/protos/client/estimate/Estimate;)Lcom/squareup/protos/client/estimate/SaveDraftEstimateRequest$Builder;
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/SaveDraftEstimateRequest$Builder;->estimate:Lcom/squareup/protos/client/estimate/Estimate;

    return-object p0
.end method
