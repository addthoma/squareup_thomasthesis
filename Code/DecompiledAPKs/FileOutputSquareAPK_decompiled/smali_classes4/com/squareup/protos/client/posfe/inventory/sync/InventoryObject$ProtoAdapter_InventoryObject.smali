.class final Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$ProtoAdapter_InventoryObject;
.super Lcom/squareup/wire/ProtoAdapter;
.source "InventoryObject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_InventoryObject"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 145
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 166
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;-><init>()V

    .line 167
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 168
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 174
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 172
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->product_state(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;

    goto :goto_0

    .line 171
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->count(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;

    goto :goto_0

    .line 170
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->deleted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;

    goto :goto_0

    .line 178
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 179
    invoke-virtual {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 143
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$ProtoAdapter_InventoryObject;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 158
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;->deleted:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 159
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;->count:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 160
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;->product_state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 161
    invoke-virtual {p2}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 143
    check-cast p2, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$ProtoAdapter_InventoryObject;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;)I
    .locals 4

    .line 150
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;->deleted:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;->count:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    const/4 v3, 0x2

    .line 151
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;->product_state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;

    const/4 v3, 0x3

    .line 152
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 143
    check-cast p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$ProtoAdapter_InventoryObject;->encodedSize(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;
    .locals 2

    .line 184
    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;->newBuilder()Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;

    move-result-object p1

    .line 185
    iget-object v0, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->count:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->count:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    iput-object v0, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->count:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryCount;

    .line 186
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->product_state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->product_state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;

    iput-object v0, p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->product_state:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductState;

    .line 187
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 188
    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 143
    check-cast p1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject$ProtoAdapter_InventoryObject;->redact(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;

    move-result-object p1

    return-object p1
.end method
