.class public final Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;
.super Lcom/squareup/wire/Message;
.source "AssetUpdateRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$ProtoAdapter_AssetUpdateRequest;,
        Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;,
        Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;",
        "Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MANIFEST:Lokio/ByteString;

.field public static final DEFAULT_READER_TYPE:Lcom/squareup/protos/client/tarkin/ReaderType;

.field private static final serialVersionUID:J


# instance fields
.field public final libcardreader_comms_version:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tarkin.AssetUpdateRequest$CommsProtocolVersion#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final manifest:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x1
    .end annotation
.end field

.field public final reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tarkin.ReaderType#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$ProtoAdapter_AssetUpdateRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$ProtoAdapter_AssetUpdateRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 25
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->DEFAULT_MANIFEST:Lokio/ByteString;

    .line 27
    sget-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->UNKNOWN:Lcom/squareup/protos/client/tarkin/ReaderType;

    sput-object v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->DEFAULT_READER_TYPE:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/ReaderType;)V
    .locals 1

    .line 53
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;-><init>(Lokio/ByteString;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/ReaderType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/ReaderType;Lokio/ByteString;)V
    .locals 1

    .line 58
    sget-object v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 59
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->manifest:Lokio/ByteString;

    .line 60
    iput-object p2, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->libcardreader_comms_version:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    .line 61
    iput-object p3, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 77
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 78
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->manifest:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->manifest:Lokio/ByteString;

    .line 80
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->libcardreader_comms_version:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->libcardreader_comms_version:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    .line 81
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    iget-object p1, p1, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 82
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 87
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->manifest:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->libcardreader_comms_version:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/tarkin/ReaderType;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 93
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;
    .locals 2

    .line 66
    new-instance v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;-><init>()V

    .line 67
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->manifest:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;->manifest:Lokio/ByteString;

    .line 68
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->libcardreader_comms_version:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;->libcardreader_comms_version:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    .line 69
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 70
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->newBuilder()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->manifest:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", manifest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->manifest:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 102
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->libcardreader_comms_version:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    if-eqz v1, :cond_1

    const-string v1, ", libcardreader_comms_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->libcardreader_comms_version:Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 103
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    if-eqz v1, :cond_2

    const-string v1, ", reader_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AssetUpdateRequest{"

    .line 104
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
