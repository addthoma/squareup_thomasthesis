.class public final Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AccumulateLoyaltyStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;",
        "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error:Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

.field public is_first_star:Ljava/lang/Boolean;

.field public loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 139
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;
    .locals 7

    .line 177
    new-instance v6, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$Builder;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$Builder;->is_first_star:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$Builder;->error:Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;-><init>(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;Lcom/squareup/protos/client/Status;Ljava/lang/Boolean;Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public error(Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$Builder;->error:Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$AccumulationError;

    return-object p0
.end method

.method public is_first_star(Ljava/lang/Boolean;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$Builder;->is_first_star:Ljava/lang/Boolean;

    return-object p0
.end method

.method public loyalty_status(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$Builder;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$Builder;
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
