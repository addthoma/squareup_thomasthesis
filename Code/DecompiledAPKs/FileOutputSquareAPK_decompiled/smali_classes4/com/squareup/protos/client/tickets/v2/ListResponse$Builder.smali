.class public final Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tickets/v2/ListResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tickets/v2/ListResponse;",
        "Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public incompatible_ticket_count_fixable_via_upgrade:Ljava/lang/Long;

.field public incompatible_ticket_count_unfixable:Ljava/lang/Long;

.field public more_tickets_available:Ljava/lang/Boolean;

.field public status:Lcom/squareup/protos/client/Status;

.field public ticket:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tickets/Ticket;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 163
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 164
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->ticket:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/tickets/v2/ListResponse;
    .locals 8

    .line 214
    new-instance v7, Lcom/squareup/protos/client/tickets/v2/ListResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->ticket:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->incompatible_ticket_count_fixable_via_upgrade:Ljava/lang/Long;

    iget-object v4, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->incompatible_ticket_count_unfixable:Ljava/lang/Long;

    iget-object v5, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->more_tickets_available:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/tickets/v2/ListResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 152
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->build()Lcom/squareup/protos/client/tickets/v2/ListResponse;

    move-result-object v0

    return-object v0
.end method

.method public incompatible_ticket_count_fixable_via_upgrade(Ljava/lang/Long;)Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->incompatible_ticket_count_fixable_via_upgrade:Ljava/lang/Long;

    return-object p0
.end method

.method public incompatible_ticket_count_unfixable(Ljava/lang/Long;)Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->incompatible_ticket_count_unfixable:Ljava/lang/Long;

    return-object p0
.end method

.method public more_tickets_available(Ljava/lang/Boolean;)Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;
    .locals 0

    .line 208
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->more_tickets_available:Ljava/lang/Boolean;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public ticket(Ljava/util/List;)Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tickets/Ticket;",
            ">;)",
            "Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;"
        }
    .end annotation

    .line 173
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 174
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/v2/ListResponse$Builder;->ticket:Ljava/util/List;

    return-object p0
.end method
