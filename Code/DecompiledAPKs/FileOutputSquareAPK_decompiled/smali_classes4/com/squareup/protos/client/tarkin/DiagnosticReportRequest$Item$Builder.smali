.class public final Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DiagnosticReportRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;",
        "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public URI:Ljava/lang/String;

.field public type:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 547
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public URI(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;
    .locals 0

    .line 559
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;->URI:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;
    .locals 4

    .line 565
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;->type:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    iget-object v2, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;->URI:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;-><init>(Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 542
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;->build()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;

    move-result-object v0

    return-object v0
.end method

.method public type(Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;
    .locals 0

    .line 551
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;->type:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    return-object p0
.end method
