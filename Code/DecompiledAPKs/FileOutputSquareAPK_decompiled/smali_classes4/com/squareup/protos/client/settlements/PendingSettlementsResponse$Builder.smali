.class public final Lcom/squareup/protos/client/settlements/PendingSettlementsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PendingSettlementsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;",
        "Lcom/squareup/protos/client/settlements/PendingSettlementsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public active_sales_report:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

.field public pending_settlement_report:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 103
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 104
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/settlements/PendingSettlementsResponse$Builder;->pending_settlement_report:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public active_sales_report(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)Lcom/squareup/protos/client/settlements/PendingSettlementsResponse$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/PendingSettlementsResponse$Builder;->active_sales_report:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;
    .locals 4

    .line 127
    new-instance v0, Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/PendingSettlementsResponse$Builder;->active_sales_report:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    iget-object v2, p0, Lcom/squareup/protos/client/settlements/PendingSettlementsResponse$Builder;->pending_settlement_report:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;-><init>(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/PendingSettlementsResponse$Builder;->build()Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;

    move-result-object v0

    return-object v0
.end method

.method public pending_settlement_report(Ljava/util/List;)Lcom/squareup/protos/client/settlements/PendingSettlementsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            ">;)",
            "Lcom/squareup/protos/client/settlements/PendingSettlementsResponse$Builder;"
        }
    .end annotation

    .line 120
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/PendingSettlementsResponse$Builder;->pending_settlement_report:Ljava/util/List;

    return-object p0
.end method
