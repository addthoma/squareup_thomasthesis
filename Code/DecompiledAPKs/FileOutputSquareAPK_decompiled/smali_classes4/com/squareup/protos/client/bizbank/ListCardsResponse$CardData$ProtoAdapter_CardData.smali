.class final Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$ProtoAdapter_CardData;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ListCardsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CardData"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 366
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 391
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;-><init>()V

    .line 392
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 393
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 415
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 413
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->name_on_card(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;

    goto :goto_0

    .line 412
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->pan_last_four(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;

    goto :goto_0

    .line 406
    :cond_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->card_brand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 408
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 398
    :cond_3
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->card_state(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 400
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 395
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->card_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;

    goto :goto_0

    .line 419
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 420
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->build()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 364
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$ProtoAdapter_CardData;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 381
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 382
    sget-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 383
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 384
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->pan_last_four:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 385
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->name_on_card:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 386
    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 364
    check-cast p2, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$ProtoAdapter_CardData;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)I
    .locals 4

    .line 371
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v3, 0x2

    .line 372
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v3, 0x3

    .line 373
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->pan_last_four:Ljava/lang/String;

    const/4 v3, 0x4

    .line 374
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->name_on_card:Ljava/lang/String;

    const/4 v3, 0x5

    .line 375
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 376
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 364
    check-cast p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$ProtoAdapter_CardData;->encodedSize(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
    .locals 1

    .line 425
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->newBuilder()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 426
    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 427
    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->name_on_card:Ljava/lang/String;

    .line 428
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 429
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->build()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 364
    check-cast p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$ProtoAdapter_CardData;->redact(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    return-object p1
.end method
