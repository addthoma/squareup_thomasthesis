.class public final Lcom/squareup/protos/client/bfd/cart/Tax$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Tax.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bfd/cart/Tax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bfd/cart/Tax;",
        "Lcom/squareup/protos/client/bfd/cart/Tax$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount:Ljava/lang/String;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 96
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount(Ljava/lang/String;)Lcom/squareup/protos/client/bfd/cart/Tax$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/Tax$Builder;->amount:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bfd/cart/Tax;
    .locals 4

    .line 111
    iget-object v0, p0, Lcom/squareup/protos/client/bfd/cart/Tax$Builder;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/Tax$Builder;->amount:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 116
    new-instance v2, Lcom/squareup/protos/client/bfd/cart/Tax;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/protos/client/bfd/cart/Tax;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v2

    :cond_0
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 113
    iget-object v2, p0, Lcom/squareup/protos/client/bfd/cart/Tax$Builder;->name:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/squareup/protos/client/bfd/cart/Tax$Builder;->amount:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "amount"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/client/bfd/cart/Tax$Builder;->build()Lcom/squareup/protos/client/bfd/cart/Tax;

    move-result-object v0

    return-object v0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/bfd/cart/Tax$Builder;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/Tax$Builder;->name:Ljava/lang/String;

    return-object p0
.end method
