.class public final Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;
.super Lcom/squareup/wire/Message;
.source "GetBillFamiliesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BillFamily"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$ProtoAdapter_BillFamily;,
        Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final bill:Lcom/squareup/protos/client/bills/Bill;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Bill#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final related_bill:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Bill#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Bill;",
            ">;"
        }
    .end annotation
.end field

.field public final related_bill_tokens:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 211
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$ProtoAdapter_BillFamily;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$ProtoAdapter_BillFamily;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Bill;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Bill;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Bill;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 239
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;-><init>(Lcom/squareup/protos/client/bills/Bill;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Bill;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Bill;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Bill;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 244
    sget-object v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 245
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    const-string p1, "related_bill"

    .line 246
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill:Ljava/util/List;

    const-string p1, "related_bill_tokens"

    .line 247
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill_tokens:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 263
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 264
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;

    .line 265
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    .line 266
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill:Ljava/util/List;

    .line 267
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill_tokens:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill_tokens:Ljava/util/List;

    .line 268
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 273
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 275
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 276
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Bill;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 277
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 278
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 279
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;
    .locals 2

    .line 252
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;-><init>()V

    .line 253
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->bill:Lcom/squareup/protos/client/bills/Bill;

    .line 254
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->related_bill:Ljava/util/List;

    .line 255
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill_tokens:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->related_bill_tokens:Ljava/util/List;

    .line 256
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 210
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->newBuilder()Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 286
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 287
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    if-eqz v1, :cond_0

    const-string v1, ", bill="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 288
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", related_bill="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 289
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", related_bill_tokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill_tokens:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BillFamily{"

    .line 290
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
