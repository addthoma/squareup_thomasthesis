.class public final enum Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;
.super Ljava/lang/Enum;
.source "LoyaltyRequestStatus.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus$ProtoAdapter_LoyaltyRequestStatus;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum STATUS_BAD_REQUEST:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

.field public static final enum STATUS_DELETED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

.field public static final enum STATUS_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

.field public static final enum STATUS_NOT_FOUND:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

.field public static final enum STATUS_SUCCESS:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 17
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    const/4 v1, 0x0

    const-string v2, "STATUS_INVALID"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    .line 22
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    const/4 v2, 0x1

    const-string v3, "STATUS_SUCCESS"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_SUCCESS:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    .line 27
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    const/4 v3, 0x2

    const-string v4, "STATUS_NOT_FOUND"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_NOT_FOUND:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    .line 32
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    const/4 v4, 0x3

    const-string v5, "STATUS_BAD_REQUEST"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_BAD_REQUEST:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    .line 37
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    const/4 v5, 0x4

    const-string v6, "STATUS_DELETED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_DELETED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    .line 13
    sget-object v6, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_SUCCESS:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_NOT_FOUND:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_BAD_REQUEST:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_DELETED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->$VALUES:[Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    .line 39
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus$ProtoAdapter_LoyaltyRequestStatus;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus$ProtoAdapter_LoyaltyRequestStatus;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 44
    iput p3, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 56
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_DELETED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    return-object p0

    .line 55
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_BAD_REQUEST:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    return-object p0

    .line 54
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_NOT_FOUND:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    return-object p0

    .line 53
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_SUCCESS:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    return-object p0

    .line 52
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->$VALUES:[Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 63
    iget v0, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->value:I

    return v0
.end method
