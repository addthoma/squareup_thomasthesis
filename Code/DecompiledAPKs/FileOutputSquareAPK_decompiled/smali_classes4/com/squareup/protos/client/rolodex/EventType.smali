.class public final enum Lcom/squareup/protos/client/rolodex/EventType;
.super Ljava/lang/Enum;
.source "EventType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/EventType$ProtoAdapter_EventType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/rolodex/EventType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/rolodex/EventType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/EventType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FEEDBACK_CONVERSATION:Lcom/squareup/protos/client/rolodex/EventType;

.field public static final enum LOYALTY_ADJUSTMENT:Lcom/squareup/protos/client/rolodex/EventType;

.field public static final enum LOYALTY_REWARD:Lcom/squareup/protos/client/rolodex/EventType;

.field public static final enum MARKETING_EMAIL:Lcom/squareup/protos/client/rolodex/EventType;

.field public static final enum PAYMENT:Lcom/squareup/protos/client/rolodex/EventType;

.field public static final enum SKIPPED:Lcom/squareup/protos/client/rolodex/EventType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 11
    new-instance v0, Lcom/squareup/protos/client/rolodex/EventType;

    const/4 v1, 0x0

    const-string v2, "SKIPPED"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/rolodex/EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/EventType;->SKIPPED:Lcom/squareup/protos/client/rolodex/EventType;

    .line 13
    new-instance v0, Lcom/squareup/protos/client/rolodex/EventType;

    const/4 v2, 0x1

    const-string v3, "MARKETING_EMAIL"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/rolodex/EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/EventType;->MARKETING_EMAIL:Lcom/squareup/protos/client/rolodex/EventType;

    .line 15
    new-instance v0, Lcom/squareup/protos/client/rolodex/EventType;

    const/4 v3, 0x2

    const-string v4, "PAYMENT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/rolodex/EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/EventType;->PAYMENT:Lcom/squareup/protos/client/rolodex/EventType;

    .line 17
    new-instance v0, Lcom/squareup/protos/client/rolodex/EventType;

    const/4 v4, 0x3

    const-string v5, "FEEDBACK_CONVERSATION"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/rolodex/EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/EventType;->FEEDBACK_CONVERSATION:Lcom/squareup/protos/client/rolodex/EventType;

    .line 19
    new-instance v0, Lcom/squareup/protos/client/rolodex/EventType;

    const/4 v5, 0x4

    const-string v6, "LOYALTY_ADJUSTMENT"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/rolodex/EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/EventType;->LOYALTY_ADJUSTMENT:Lcom/squareup/protos/client/rolodex/EventType;

    .line 21
    new-instance v0, Lcom/squareup/protos/client/rolodex/EventType;

    const/4 v6, 0x5

    const-string v7, "LOYALTY_REWARD"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/rolodex/EventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/EventType;->LOYALTY_REWARD:Lcom/squareup/protos/client/rolodex/EventType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/rolodex/EventType;

    .line 10
    sget-object v7, Lcom/squareup/protos/client/rolodex/EventType;->SKIPPED:Lcom/squareup/protos/client/rolodex/EventType;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/EventType;->MARKETING_EMAIL:Lcom/squareup/protos/client/rolodex/EventType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/rolodex/EventType;->PAYMENT:Lcom/squareup/protos/client/rolodex/EventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/rolodex/EventType;->FEEDBACK_CONVERSATION:Lcom/squareup/protos/client/rolodex/EventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/rolodex/EventType;->LOYALTY_ADJUSTMENT:Lcom/squareup/protos/client/rolodex/EventType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/rolodex/EventType;->LOYALTY_REWARD:Lcom/squareup/protos/client/rolodex/EventType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/rolodex/EventType;->$VALUES:[Lcom/squareup/protos/client/rolodex/EventType;

    .line 23
    new-instance v0, Lcom/squareup/protos/client/rolodex/EventType$ProtoAdapter_EventType;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/EventType$ProtoAdapter_EventType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/EventType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput p3, p0, Lcom/squareup/protos/client/rolodex/EventType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/rolodex/EventType;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 41
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/rolodex/EventType;->LOYALTY_REWARD:Lcom/squareup/protos/client/rolodex/EventType;

    return-object p0

    .line 40
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/rolodex/EventType;->LOYALTY_ADJUSTMENT:Lcom/squareup/protos/client/rolodex/EventType;

    return-object p0

    .line 39
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/rolodex/EventType;->FEEDBACK_CONVERSATION:Lcom/squareup/protos/client/rolodex/EventType;

    return-object p0

    .line 38
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/rolodex/EventType;->PAYMENT:Lcom/squareup/protos/client/rolodex/EventType;

    return-object p0

    .line 37
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/rolodex/EventType;->MARKETING_EMAIL:Lcom/squareup/protos/client/rolodex/EventType;

    return-object p0

    .line 36
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/rolodex/EventType;->SKIPPED:Lcom/squareup/protos/client/rolodex/EventType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/EventType;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/client/rolodex/EventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/rolodex/EventType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/rolodex/EventType;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/client/rolodex/EventType;->$VALUES:[Lcom/squareup/protos/client/rolodex/EventType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/rolodex/EventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/rolodex/EventType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 48
    iget v0, p0, Lcom/squareup/protos/client/rolodex/EventType;->value:I

    return v0
.end method
