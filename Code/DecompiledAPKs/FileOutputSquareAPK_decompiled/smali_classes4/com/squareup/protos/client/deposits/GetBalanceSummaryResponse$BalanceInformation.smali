.class public final Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;
.super Lcom/squareup/wire/Message;
.source "GetBalanceSummaryResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BalanceInformation"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$ProtoAdapter_BalanceInformation;,
        Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final available_balance:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final pending_balance:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final total_balance:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 489
    new-instance v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$ProtoAdapter_BalanceInformation;

    invoke-direct {v0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$ProtoAdapter_BalanceInformation;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 521
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 526
    sget-object v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 527
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->total_balance:Lcom/squareup/protos/common/Money;

    .line 528
    iput-object p2, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->available_balance:Lcom/squareup/protos/common/Money;

    .line 529
    iput-object p3, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->pending_balance:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 545
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 546
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    .line 547
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->total_balance:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->total_balance:Lcom/squareup/protos/common/Money;

    .line 548
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->available_balance:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->available_balance:Lcom/squareup/protos/common/Money;

    .line 549
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->pending_balance:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->pending_balance:Lcom/squareup/protos/common/Money;

    .line 550
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 555
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 557
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 558
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->total_balance:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 559
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->available_balance:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 560
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->pending_balance:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 561
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;
    .locals 2

    .line 534
    new-instance v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;-><init>()V

    .line 535
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->total_balance:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;->total_balance:Lcom/squareup/protos/common/Money;

    .line 536
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->available_balance:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;->available_balance:Lcom/squareup/protos/common/Money;

    .line 537
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->pending_balance:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;->pending_balance:Lcom/squareup/protos/common/Money;

    .line 538
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 488
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->newBuilder()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 568
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 569
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->total_balance:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    const-string v1, ", total_balance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->total_balance:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 570
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->available_balance:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", available_balance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->available_balance:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 571
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->pending_balance:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", pending_balance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->pending_balance:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BalanceInformation{"

    .line 572
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
