.class public final Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "QueueResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;",
        "Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public queue_bill_result:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 98
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse$Builder;->queue_bill_result:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;
    .locals 4

    .line 117
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse$Builder;->queue_bill_result:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;

    move-result-object v0

    return-object v0
.end method

.method public queue_bill_result(Ljava/util/List;)Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult;",
            ">;)",
            "Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse$Builder;"
        }
    .end annotation

    .line 110
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse$Builder;->queue_bill_result:Ljava/util/List;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse$Builder;
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
