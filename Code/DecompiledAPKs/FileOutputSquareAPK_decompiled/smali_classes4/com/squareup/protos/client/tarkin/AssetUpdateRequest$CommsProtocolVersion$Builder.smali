.class public final Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AssetUpdateRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;",
        "Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public app:Ljava/lang/Integer;

.field public ep:Ljava/lang/Integer;

.field public transport:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 233
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public app(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;
    .locals 0

    .line 242
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->app:Ljava/lang/Integer;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;
    .locals 5

    .line 253
    new-instance v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->transport:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->app:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->ep:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 226
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->build()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    move-result-object v0

    return-object v0
.end method

.method public ep(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;
    .locals 0

    .line 247
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->ep:Ljava/lang/Integer;

    return-object p0
.end method

.method public transport(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->transport:Ljava/lang/Integer;

    return-object p0
.end method
