.class public final Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;
.super Lcom/squareup/wire/Message;
.source "RedeemPointsResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ProtoAdapter_RedeemPointsResponse;,
        Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;,
        Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;",
        "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_RESPONSE_STATUS:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

.field private static final serialVersionUID:J


# instance fields
.field public final coupon:Lcom/squareup/protos/client/coupons/Coupon;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.coupons.Coupon#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyStatus#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final response_status:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.RedeemPointsResponse$ResponseStatus#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ProtoAdapter_RedeemPointsResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ProtoAdapter_RedeemPointsResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->UNKNOWN_STATUS:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    sput-object v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->DEFAULT_RESPONSE_STATUS:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/protos/client/loyalty/LoyaltyStatus;)V
    .locals 6

    .line 62
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/protos/client/loyalty/LoyaltyStatus;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/protos/client/loyalty/LoyaltyStatus;Lokio/ByteString;)V
    .locals 1

    .line 67
    sget-object v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 68
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->status:Lcom/squareup/protos/client/Status;

    .line 69
    iput-object p2, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->response_status:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    .line 70
    iput-object p3, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    .line 71
    iput-object p4, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 88
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 89
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->status:Lcom/squareup/protos/client/Status;

    .line 91
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->response_status:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->response_status:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    .line 92
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    .line 93
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    .line 94
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 99
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->response_status:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/coupons/Coupon;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 106
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;
    .locals 2

    .line 76
    new-instance v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;-><init>()V

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->response_status:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->response_status:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    .line 79
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    .line 80
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    .line 81
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->newBuilder()Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 115
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->response_status:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    if-eqz v1, :cond_1

    const-string v1, ", response_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->response_status:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 116
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    if-eqz v1, :cond_2

    const-string v1, ", coupon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 117
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    if-eqz v1, :cond_3

    const-string v1, ", loyalty_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RedeemPointsResponse{"

    .line 118
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
