.class public final enum Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;
.super Ljava/lang/Enum;
.source "FinishCardActivationResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Result"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result$ProtoAdapter_Result;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

.field public static final enum ACTIVATION_TOKEN_EXPIRED:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

.field public static final enum ACTIVATION_TOKEN_INVALID:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum INCORRECT_CARD_INFO:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

.field public static final enum INTERNAL_FAILURE:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

.field public static final enum SUCCESS:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 116
    new-instance v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "SUCCESS"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->SUCCESS:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    .line 118
    new-instance v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    const/4 v3, 0x2

    const-string v4, "INTERNAL_FAILURE"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->INTERNAL_FAILURE:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    .line 120
    new-instance v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    const/4 v4, 0x3

    const-string v5, "ACTIVATION_TOKEN_INVALID"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->ACTIVATION_TOKEN_INVALID:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    .line 122
    new-instance v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    const/4 v5, 0x4

    const-string v6, "ACTIVATION_TOKEN_EXPIRED"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->ACTIVATION_TOKEN_EXPIRED:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    .line 124
    new-instance v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    const/4 v6, 0x5

    const-string v7, "INCORRECT_CARD_INFO"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->INCORRECT_CARD_INFO:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    new-array v0, v6, [Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    .line 115
    sget-object v6, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->SUCCESS:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->INTERNAL_FAILURE:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->ACTIVATION_TOKEN_INVALID:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->ACTIVATION_TOKEN_EXPIRED:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->INCORRECT_CARD_INFO:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->$VALUES:[Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    .line 126
    new-instance v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result$ProtoAdapter_Result;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result$ProtoAdapter_Result;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 131
    iput p3, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 143
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->INCORRECT_CARD_INFO:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    return-object p0

    .line 142
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->ACTIVATION_TOKEN_EXPIRED:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    return-object p0

    .line 141
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->ACTIVATION_TOKEN_INVALID:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    return-object p0

    .line 140
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->INTERNAL_FAILURE:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    return-object p0

    .line 139
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->SUCCESS:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;
    .locals 1

    .line 115
    const-class v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;
    .locals 1

    .line 115
    sget-object v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->$VALUES:[Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 150
    iget v0, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;->value:I

    return v0
.end method
