.class public final Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$LineItems;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$LineItems;",
        "Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public comp_itemization:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;"
        }
    .end annotation
.end field

.field public default_dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

.field public discount:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public fee:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public itemization:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;"
        }
    .end annotation
.end field

.field public rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

.field public surcharge:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public void_itemization:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 483
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 484
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->itemization:Ljava/util/List;

    .line 485
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->fee:Ljava/util/List;

    .line 486
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->discount:Ljava/util/List;

    .line 487
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->comp_itemization:Ljava/util/List;

    .line 488
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->void_itemization:Ljava/util/List;

    .line 489
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->surcharge:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$LineItems;
    .locals 11

    .line 576
    new-instance v10, Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->itemization:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->fee:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->discount:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->default_dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->comp_itemization:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->void_itemization:Ljava/util/List;

    iget-object v8, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->surcharge:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/bills/Cart$LineItems;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;Lcom/squareup/protos/client/bills/DiningOptionLineItem;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 466
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->build()Lcom/squareup/protos/client/bills/Cart$LineItems;

    move-result-object v0

    return-object v0
.end method

.method public comp_itemization(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 551
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 552
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->comp_itemization:Ljava/util/List;

    return-object p0
.end method

.method public default_dining_option(Lcom/squareup/protos/client/bills/DiningOptionLineItem;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;
    .locals 0

    .line 542
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->default_dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    return-object p0
.end method

.method public discount(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;"
        }
    .end annotation

    .line 522
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 523
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->discount:Ljava/util/List;

    return-object p0
.end method

.method public fee(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;"
        }
    .end annotation

    .line 512
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 513
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->fee:Ljava/util/List;

    return-object p0
.end method

.method public itemization(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;"
        }
    .end annotation

    .line 500
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 501
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->itemization:Ljava/util/List;

    return-object p0
.end method

.method public rounding_adjustment(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;
    .locals 0

    .line 532
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    return-object p0
.end method

.method public surcharge(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;"
        }
    .end annotation

    .line 569
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 570
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->surcharge:Ljava/util/List;

    return-object p0
.end method

.method public void_itemization(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;"
        }
    .end annotation

    .line 560
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 561
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->void_itemization:Ljava/util/List;

    return-object p0
.end method
