.class public final Lcom/squareup/protos/client/dialogue/Message$Data;
.super Lcom/squareup/wire/Message;
.source "Message.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/dialogue/Message;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/dialogue/Message$Data$ProtoAdapter_Data;,
        Lcom/squareup/protos/client/dialogue/Message$Data$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/dialogue/Message$Data;",
        "Lcom/squareup/protos/client/dialogue/Message$Data$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/dialogue/Message$Data;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final comment:Lcom/squareup/protos/client/dialogue/Comment;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.dialogue.Comment#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final coupon:Lcom/squareup/protos/client/coupons/Coupon;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.coupons.Coupon#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final transaction:Lcom/squareup/protos/client/dialogue/Message$Transaction;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.dialogue.Message$Transaction#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 296
    new-instance v0, Lcom/squareup/protos/client/dialogue/Message$Data$ProtoAdapter_Data;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/Message$Data$ProtoAdapter_Data;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/dialogue/Message$Data;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/dialogue/Comment;Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/protos/client/dialogue/Message$Transaction;)V
    .locals 1

    .line 319
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/dialogue/Message$Data;-><init>(Lcom/squareup/protos/client/dialogue/Comment;Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/protos/client/dialogue/Message$Transaction;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/dialogue/Comment;Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/protos/client/dialogue/Message$Transaction;Lokio/ByteString;)V
    .locals 1

    .line 323
    sget-object v0, Lcom/squareup/protos/client/dialogue/Message$Data;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 324
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    .line 325
    iput-object p2, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    .line 326
    iput-object p3, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->transaction:Lcom/squareup/protos/client/dialogue/Message$Transaction;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 342
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/dialogue/Message$Data;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 343
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/dialogue/Message$Data;

    .line 344
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/Message$Data;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/Message$Data;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/Message$Data;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    .line 345
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/Message$Data;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    .line 346
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->transaction:Lcom/squareup/protos/client/dialogue/Message$Transaction;

    iget-object p1, p1, Lcom/squareup/protos/client/dialogue/Message$Data;->transaction:Lcom/squareup/protos/client/dialogue/Message$Transaction;

    .line 347
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 352
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 354
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/Message$Data;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 355
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/dialogue/Comment;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 356
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/coupons/Coupon;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 357
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->transaction:Lcom/squareup/protos/client/dialogue/Message$Transaction;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/dialogue/Message$Transaction;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 358
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/dialogue/Message$Data$Builder;
    .locals 2

    .line 331
    new-instance v0, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;-><init>()V

    .line 332
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    .line 333
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    .line 334
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->transaction:Lcom/squareup/protos/client/dialogue/Message$Transaction;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->transaction:Lcom/squareup/protos/client/dialogue/Message$Transaction;

    .line 335
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/Message$Data;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 295
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/Message$Data;->newBuilder()Lcom/squareup/protos/client/dialogue/Message$Data$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 365
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 366
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    if-eqz v1, :cond_0

    const-string v1, ", comment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 367
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    if-eqz v1, :cond_1

    const-string v1, ", coupon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 368
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->transaction:Lcom/squareup/protos/client/dialogue/Message$Transaction;

    if-eqz v1, :cond_2

    const-string v1, ", transaction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Data;->transaction:Lcom/squareup/protos/client/dialogue/Message$Transaction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Data{"

    .line 369
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
