.class public final Lcom/squareup/protos/client/settlements/SettlementReport;
.super Lcom/squareup/wire/Message;
.source "SettlementReport.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/settlements/SettlementReport$ProtoAdapter_SettlementReport;,
        Lcom/squareup/protos/client/settlements/SettlementReport$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/settlements/SettlementReport;",
        "Lcom/squareup/protos/client/settlements/SettlementReport$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/settlements/SettlementReport;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BANK_ACCOUNT_SUFFIX:Ljava/lang/String; = ""

.field public static final DEFAULT_BANK_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_CARD_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final DEFAULT_PAN_SUFFIX:Ljava/lang/String; = ""

.field public static final DEFAULT_SETTLEMENT_TYPE:Lcom/squareup/protos/client/settlements/SettlementType;

.field public static final DEFAULT_SUCCESS:Ljava/lang/Boolean;

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final bank_account_suffix:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final bank_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Card$Brand#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final created_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final deposit_fee_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final pan_suffix:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field

.field public final settled_bill_entry_token_group:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.settlements.SettledBillEntryTokenGroup#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x10
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;",
            ">;"
        }
    .end annotation
.end field

.field public final settlement_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final settlement_type:Lcom/squareup/protos/client/settlements/SettlementType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.settlements.SettlementType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final success:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final total_collected_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final total_fee_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x9
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReport$ProtoAdapter_SettlementReport;

    invoke-direct {v0}, Lcom/squareup/protos/client/settlements/SettlementReport$ProtoAdapter_SettlementReport;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/settlements/SettlementReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 31
    sget-object v0, Lcom/squareup/protos/client/settlements/SettlementType;->SETTLEMENT_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/settlements/SettlementType;

    sput-object v0, Lcom/squareup/protos/client/settlements/SettlementReport;->DEFAULT_SETTLEMENT_TYPE:Lcom/squareup/protos/client/settlements/SettlementType;

    const/4 v0, 0x1

    .line 33
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/settlements/SettlementReport;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    .line 39
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->UNKNOWN_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    sput-object v0, Lcom/squareup/protos/client/settlements/SettlementReport;->DEFAULT_CARD_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/settlements/SettlementType;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Ljava/util/List;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/settlements/SettlementType;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;",
            ">;)V"
        }
    .end annotation

    .line 167
    sget-object v14, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/protos/client/settlements/SettlementReport;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/settlements/SettlementType;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/settlements/SettlementType;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/settlements/SettlementType;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettledBillEntryTokenGroup;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 175
    sget-object v0, Lcom/squareup/protos/client/settlements/SettlementReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p14}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 176
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->token:Ljava/lang/String;

    .line 177
    iput-object p2, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_type:Lcom/squareup/protos/client/settlements/SettlementType;

    .line 178
    iput-object p3, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 179
    iput-object p4, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->success:Ljava/lang/Boolean;

    .line 180
    iput-object p5, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_account_suffix:Ljava/lang/String;

    .line 181
    iput-object p6, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_name:Ljava/lang/String;

    .line 182
    iput-object p7, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->total_collected_money:Lcom/squareup/protos/common/Money;

    .line 183
    iput-object p8, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->total_fee_money:Lcom/squareup/protos/common/Money;

    .line 184
    iput-object p9, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->deposit_fee_money:Lcom/squareup/protos/common/Money;

    .line 185
    iput-object p10, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_money:Lcom/squareup/protos/common/Money;

    .line 186
    iput-object p11, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 187
    iput-object p12, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->pan_suffix:Ljava/lang/String;

    const-string p1, "settled_bill_entry_token_group"

    .line 188
    invoke-static {p1, p13}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settled_bill_entry_token_group:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 214
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/settlements/SettlementReport;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 215
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/settlements/SettlementReport;

    .line 216
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReport;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReport;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->token:Ljava/lang/String;

    .line 217
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_type:Lcom/squareup/protos/client/settlements/SettlementType;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_type:Lcom/squareup/protos/client/settlements/SettlementType;

    .line 218
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 219
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->success:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->success:Ljava/lang/Boolean;

    .line 220
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_account_suffix:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_account_suffix:Ljava/lang/String;

    .line 221
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_name:Ljava/lang/String;

    .line 222
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->total_collected_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->total_collected_money:Lcom/squareup/protos/common/Money;

    .line 223
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->total_fee_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->total_fee_money:Lcom/squareup/protos/common/Money;

    .line 224
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->deposit_fee_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->deposit_fee_money:Lcom/squareup/protos/common/Money;

    .line 225
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_money:Lcom/squareup/protos/common/Money;

    .line 226
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 227
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->pan_suffix:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->pan_suffix:Ljava/lang/String;

    .line 228
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settled_bill_entry_token_group:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/settlements/SettlementReport;->settled_bill_entry_token_group:Ljava/util/List;

    .line 229
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 234
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_c

    .line 236
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReport;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 237
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 238
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_type:Lcom/squareup/protos/client/settlements/SettlementType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/settlements/SettlementType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 239
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 240
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->success:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 241
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_account_suffix:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 242
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_name:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->total_collected_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->total_fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 245
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->deposit_fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 247
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 248
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->pan_suffix:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_b
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 249
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settled_bill_entry_token_group:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 250
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_c
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/settlements/SettlementReport$Builder;
    .locals 2

    .line 193
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;-><init>()V

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->token:Ljava/lang/String;

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_type:Lcom/squareup/protos/client/settlements/SettlementType;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->settlement_type:Lcom/squareup/protos/client/settlements/SettlementType;

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->success:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->success:Ljava/lang/Boolean;

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_account_suffix:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->bank_account_suffix:Ljava/lang/String;

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->bank_name:Ljava/lang/String;

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->total_collected_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->total_collected_money:Lcom/squareup/protos/common/Money;

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->total_fee_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->total_fee_money:Lcom/squareup/protos/common/Money;

    .line 202
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->deposit_fee_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->deposit_fee_money:Lcom/squareup/protos/common/Money;

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->settlement_money:Lcom/squareup/protos/common/Money;

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->pan_suffix:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->pan_suffix:Ljava/lang/String;

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settled_bill_entry_token_group:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->settled_bill_entry_token_group:Ljava/util/List;

    .line 207
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReport;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/settlements/SettlementReport$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReport;->newBuilder()Lcom/squareup/protos/client/settlements/SettlementReport$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 258
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_type:Lcom/squareup/protos/client/settlements/SettlementType;

    if-eqz v1, :cond_1

    const-string v1, ", settlement_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_type:Lcom/squareup/protos/client/settlements/SettlementType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 260
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_2

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 261
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->success:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", success="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->success:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 262
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_account_suffix:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", bank_account_suffix="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_account_suffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_name:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", bank_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->bank_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->total_collected_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", total_collected_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->total_collected_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 265
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->total_fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_7

    const-string v1, ", total_fee_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->total_fee_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 266
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->deposit_fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_8

    const-string v1, ", deposit_fee_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->deposit_fee_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 267
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_9

    const-string v1, ", settlement_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settlement_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 268
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-eqz v1, :cond_a

    const-string v1, ", card_brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 269
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->pan_suffix:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", pan_suffix="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->pan_suffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settled_bill_entry_token_group:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, ", settled_bill_entry_token_group="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReport;->settled_bill_entry_token_group:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_c
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SettlementReport{"

    .line 271
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
