.class public final Lcom/squareup/protos/client/deposits/EligibilityDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EligibilityDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/deposits/EligibilityDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/deposits/EligibilityDetails;",
        "Lcom/squareup/protos/client/deposits/EligibilityDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public balance_activity_type:Lcom/squareup/protos/client/deposits/BalanceActivityType;

.field public blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

.field public max_amount:Lcom/squareup/protos/common/Money;

.field public min_amount:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 133
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public balance_activity_type(Lcom/squareup/protos/client/deposits/BalanceActivityType;)Lcom/squareup/protos/client/deposits/EligibilityDetails$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/EligibilityDetails$Builder;->balance_activity_type:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    return-object p0
.end method

.method public blocker(Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;)Lcom/squareup/protos/client/deposits/EligibilityDetails$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/EligibilityDetails$Builder;->blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/deposits/EligibilityDetails;
    .locals 7

    .line 167
    new-instance v6, Lcom/squareup/protos/client/deposits/EligibilityDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/EligibilityDetails$Builder;->balance_activity_type:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    iget-object v2, p0, Lcom/squareup/protos/client/deposits/EligibilityDetails$Builder;->blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    iget-object v3, p0, Lcom/squareup/protos/client/deposits/EligibilityDetails$Builder;->min_amount:Lcom/squareup/protos/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/client/deposits/EligibilityDetails$Builder;->max_amount:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/deposits/EligibilityDetails;-><init>(Lcom/squareup/protos/client/deposits/BalanceActivityType;Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/EligibilityDetails$Builder;->build()Lcom/squareup/protos/client/deposits/EligibilityDetails;

    move-result-object v0

    return-object v0
.end method

.method public max_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/EligibilityDetails$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/EligibilityDetails$Builder;->max_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public min_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/EligibilityDetails$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/EligibilityDetails$Builder;->min_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
