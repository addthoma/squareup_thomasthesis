.class public final Lcom/squareup/protos/client/bills/CardTender$Card$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardTender$Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CardTender$Card;",
        "Lcom/squareup/protos/client/bills/CardTender$Card$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public chip_card_fallback_indicator:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

.field public entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

.field public felica_masked_card_number:Ljava/lang/String;

.field public felica_sprwid:Ljava/lang/String;

.field public pan_suffix:Ljava/lang/String;

.field public write_only_can_swipe_chip_card:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 408
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public brand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;
    .locals 0

    .line 417
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/CardTender$Card;
    .locals 11

    .line 468
    new-instance v10, Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->pan_suffix:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->chip_card_fallback_indicator:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->write_only_can_swipe_chip_card:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->felica_masked_card_number:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->felica_sprwid:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/bills/CardTender$Card;-><init>(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 391
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$Card;

    move-result-object v0

    return-object v0
.end method

.method public chip_card_fallback_indicator(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;
    .locals 0

    .line 428
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->chip_card_fallback_indicator:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    return-object p0
.end method

.method public entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;
    .locals 0

    .line 412
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0
.end method

.method public felica_brand(Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;
    .locals 0

    .line 445
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    return-object p0
.end method

.method public felica_masked_card_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;
    .locals 0

    .line 454
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->felica_masked_card_number:Ljava/lang/String;

    return-object p0
.end method

.method public felica_sprwid(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;
    .locals 0

    .line 462
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->felica_sprwid:Ljava/lang/String;

    return-object p0
.end method

.method public pan_suffix(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;
    .locals 0

    .line 422
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->pan_suffix:Ljava/lang/String;

    return-object p0
.end method

.method public write_only_can_swipe_chip_card(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;
    .locals 0

    .line 437
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->write_only_can_swipe_chip_card:Ljava/lang/Boolean;

    return-object p0
.end method
