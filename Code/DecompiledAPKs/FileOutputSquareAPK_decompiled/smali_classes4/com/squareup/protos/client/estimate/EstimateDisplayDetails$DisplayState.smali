.class public final enum Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;
.super Ljava/lang/Enum;
.source "EstimateDisplayDetails.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/EstimateDisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DisplayState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState$ProtoAdapter_DisplayState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

.field public static final enum ACCEPTED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CANCELED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

.field public static final enum DELIVERED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

.field public static final enum DRAFT:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

.field public static final enum EXPIRED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

.field public static final enum INVOICED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

.field public static final enum SCHEDULED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

.field public static final enum UNDELIVERED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 422
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->UNKNOWN:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    .line 427
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    const/4 v2, 0x1

    const-string v3, "DRAFT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->DRAFT:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    .line 432
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    const/4 v3, 0x2

    const-string v4, "DELIVERED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->DELIVERED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    .line 437
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    const/4 v4, 0x3

    const-string v5, "EXPIRED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->EXPIRED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    .line 442
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    const/4 v5, 0x4

    const-string v6, "ACCEPTED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->ACCEPTED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    .line 447
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    const/4 v6, 0x5

    const-string v7, "INVOICED"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->INVOICED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    .line 452
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    const/4 v7, 0x6

    const-string v8, "UNDELIVERED"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->UNDELIVERED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    .line 458
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    const/4 v8, 0x7

    const-string v9, "CANCELED"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->CANCELED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    .line 463
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    const/16 v9, 0x8

    const-string v10, "SCHEDULED"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->SCHEDULED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    .line 418
    sget-object v10, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->UNKNOWN:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->DRAFT:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->DELIVERED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->EXPIRED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->ACCEPTED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->INVOICED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->UNDELIVERED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->CANCELED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->SCHEDULED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->$VALUES:[Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    .line 465
    new-instance v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState$ProtoAdapter_DisplayState;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState$ProtoAdapter_DisplayState;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 469
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 470
    iput p3, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 486
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->SCHEDULED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    return-object p0

    .line 485
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->CANCELED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    return-object p0

    .line 484
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->UNDELIVERED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    return-object p0

    .line 483
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->INVOICED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    return-object p0

    .line 482
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->ACCEPTED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    return-object p0

    .line 481
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->EXPIRED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    return-object p0

    .line 480
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->DELIVERED:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    return-object p0

    .line 479
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->DRAFT:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    return-object p0

    .line 478
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->UNKNOWN:Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;
    .locals 1

    .line 418
    const-class v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;
    .locals 1

    .line 418
    sget-object v0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->$VALUES:[Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 493
    iget v0, p0, Lcom/squareup/protos/client/estimate/EstimateDisplayDetails$DisplayState;->value:I

    return v0
.end method
