.class public final Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;
.super Lcom/squareup/wire/Message;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReceiptDisplayDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$ProtoAdapter_ReceiptDisplayDetails;,
        Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;",
        "Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BUYER_SELECTED_LOCALE:Ljava/lang/String; = ""

.field public static final DEFAULT_BUYER_SIGNATURE:Lcom/squareup/protos/common/payment/SignatureType;

.field public static final DEFAULT_DISPLAY_CUSTOM_AMOUNT:Ljava/lang/Boolean;

.field public static final DEFAULT_DISPLAY_QUICK_TIP_OPTIONS:Ljava/lang/Boolean;

.field public static final DEFAULT_DISPLAY_SIGNATURE_LINE_ON_PAPER_RECEIPT:Ljava/lang/Boolean;

.field public static final DEFAULT_DISPLAY_TIP_OPTIONS_ON_PAPER_RECEIPT:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final buyer_selected_locale:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final buyer_signature:Lcom/squareup/protos/common/payment/SignatureType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.payment.SignatureType#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final display_custom_amount:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final display_quick_tip_options:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final display_signature_line_on_paper_receipt:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final display_tip_options_on_paper_receipt:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final remaining_tender_balance_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.tipping.TippingPreferences#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1572
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$ProtoAdapter_ReceiptDisplayDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$ProtoAdapter_ReceiptDisplayDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 1576
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->DEFAULT_DISPLAY_SIGNATURE_LINE_ON_PAPER_RECEIPT:Ljava/lang/Boolean;

    .line 1578
    sput-object v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->DEFAULT_DISPLAY_TIP_OPTIONS_ON_PAPER_RECEIPT:Ljava/lang/Boolean;

    .line 1580
    sput-object v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->DEFAULT_DISPLAY_QUICK_TIP_OPTIONS:Ljava/lang/Boolean;

    .line 1582
    sput-object v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->DEFAULT_DISPLAY_CUSTOM_AMOUNT:Ljava/lang/Boolean;

    .line 1586
    sget-object v0, Lcom/squareup/protos/common/payment/SignatureType;->UNKNOWN:Lcom/squareup/protos/common/payment/SignatureType;

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->DEFAULT_BUYER_SIGNATURE:Lcom/squareup/protos/common/payment/SignatureType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/common/tipping/TippingPreferences;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/common/payment/SignatureType;)V
    .locals 10

    .line 1670
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/common/tipping/TippingPreferences;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/common/payment/SignatureType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/common/tipping/TippingPreferences;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/common/payment/SignatureType;Lokio/ByteString;)V
    .locals 1

    .line 1678
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1679
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_signature_line_on_paper_receipt:Ljava/lang/Boolean;

    .line 1680
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_tip_options_on_paper_receipt:Ljava/lang/Boolean;

    .line 1681
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    .line 1682
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_quick_tip_options:Ljava/lang/Boolean;

    .line 1683
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_custom_amount:Ljava/lang/Boolean;

    .line 1684
    iput-object p6, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

    .line 1685
    iput-object p7, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_selected_locale:Ljava/lang/String;

    .line 1686
    iput-object p8, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_signature:Lcom/squareup/protos/common/payment/SignatureType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1707
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1708
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    .line 1709
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_signature_line_on_paper_receipt:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_signature_line_on_paper_receipt:Ljava/lang/Boolean;

    .line 1710
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_tip_options_on_paper_receipt:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_tip_options_on_paper_receipt:Ljava/lang/Boolean;

    .line 1711
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    .line 1712
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_quick_tip_options:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_quick_tip_options:Ljava/lang/Boolean;

    .line 1713
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_custom_amount:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_custom_amount:Ljava/lang/Boolean;

    .line 1714
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

    .line 1715
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_selected_locale:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_selected_locale:Ljava/lang/String;

    .line 1716
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_signature:Lcom/squareup/protos/common/payment/SignatureType;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_signature:Lcom/squareup/protos/common/payment/SignatureType;

    .line 1717
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1722
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 1724
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1725
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_signature_line_on_paper_receipt:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1726
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_tip_options_on_paper_receipt:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1727
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/tipping/TippingPreferences;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1728
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_quick_tip_options:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1729
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_custom_amount:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1730
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1731
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_selected_locale:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1732
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_signature:Lcom/squareup/protos/common/payment/SignatureType;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/common/payment/SignatureType;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 1733
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;
    .locals 2

    .line 1691
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;-><init>()V

    .line 1692
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_signature_line_on_paper_receipt:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_signature_line_on_paper_receipt:Ljava/lang/Boolean;

    .line 1693
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_tip_options_on_paper_receipt:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_tip_options_on_paper_receipt:Ljava/lang/Boolean;

    .line 1694
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    .line 1695
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_quick_tip_options:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_quick_tip_options:Ljava/lang/Boolean;

    .line 1696
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_custom_amount:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->display_custom_amount:Ljava/lang/Boolean;

    .line 1697
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

    .line 1698
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_selected_locale:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->buyer_selected_locale:Ljava/lang/String;

    .line 1699
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_signature:Lcom/squareup/protos/common/payment/SignatureType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->buyer_signature:Lcom/squareup/protos/common/payment/SignatureType;

    .line 1700
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1571
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->newBuilder()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1740
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1741
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_signature_line_on_paper_receipt:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", display_signature_line_on_paper_receipt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_signature_line_on_paper_receipt:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1742
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_tip_options_on_paper_receipt:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", display_tip_options_on_paper_receipt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_tip_options_on_paper_receipt:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1743
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    if-eqz v1, :cond_2

    const-string v1, ", tipping_preferences="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->tipping_preferences:Lcom/squareup/protos/common/tipping/TippingPreferences;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1744
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_quick_tip_options:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", display_quick_tip_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_quick_tip_options:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1745
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_custom_amount:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", display_custom_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->display_custom_amount:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1746
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", remaining_tender_balance_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->remaining_tender_balance_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1747
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_selected_locale:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", buyer_selected_locale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_selected_locale:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1748
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_signature:Lcom/squareup/protos/common/payment/SignatureType;

    if-eqz v1, :cond_7

    const-string v1, ", buyer_signature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;->buyer_signature:Lcom/squareup/protos/common/payment/SignatureType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReceiptDisplayDetails{"

    .line 1749
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
