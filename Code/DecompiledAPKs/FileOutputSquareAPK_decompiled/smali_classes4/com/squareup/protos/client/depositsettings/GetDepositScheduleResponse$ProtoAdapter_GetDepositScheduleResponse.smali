.class final Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$ProtoAdapter_GetDepositScheduleResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetDepositScheduleResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetDepositScheduleResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 211
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 236
    new-instance v0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;-><init>()V

    .line 237
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 238
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 253
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 246
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->settlement_frequency(Lcom/squareup/protos/client/depositsettings/SettlementFrequency;)Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 248
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 243
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/time/LocalTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/LocalTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->default_cutoff_time(Lcom/squareup/protos/common/time/LocalTime;)Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;

    goto :goto_0

    .line 242
    :cond_2
    iget-object v3, v0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->potential_deposit_schedule:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 241
    :cond_3
    sget-object v3, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->next_weekly_deposit_schedule(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;)Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;

    goto :goto_0

    .line 240
    :cond_4
    sget-object v3, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->current_weekly_deposit_schedule(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;)Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;

    goto :goto_0

    .line 257
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 258
    invoke-virtual {v0}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->build()Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 209
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$ProtoAdapter_GetDepositScheduleResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 226
    sget-object v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 227
    sget-object v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 228
    sget-object v0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->potential_deposit_schedule:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 229
    sget-object v0, Lcom/squareup/protos/common/time/LocalTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 230
    sget-object v0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->settlement_frequency:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 231
    invoke-virtual {p2}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 209
    check-cast p2, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$ProtoAdapter_GetDepositScheduleResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;)I
    .locals 4

    .line 216
    sget-object v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    const/4 v3, 0x2

    .line 217
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 218
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->potential_deposit_schedule:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/LocalTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;

    const/4 v3, 0x4

    .line 219
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->settlement_frequency:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    const/4 v3, 0x5

    .line 220
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 221
    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 209
    check-cast p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$ProtoAdapter_GetDepositScheduleResponse;->encodedSize(Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;)Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;
    .locals 2

    .line 263
    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;->newBuilder()Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;

    move-result-object p1

    .line 264
    iget-object v0, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    iput-object v0, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    .line 265
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    iput-object v0, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    .line 266
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->potential_deposit_schedule:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 267
    iget-object v0, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/time/LocalTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/LocalTime;

    iput-object v0, p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;

    .line 268
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 269
    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->build()Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 209
    check-cast p1, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$ProtoAdapter_GetDepositScheduleResponse;->redact(Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;)Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;

    move-result-object p1

    return-object p1
.end method
