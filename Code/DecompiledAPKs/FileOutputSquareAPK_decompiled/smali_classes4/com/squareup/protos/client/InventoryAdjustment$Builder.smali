.class public final Lcom/squareup/protos/client/InventoryAdjustment$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InventoryAdjustment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/InventoryAdjustment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/InventoryAdjustment;",
        "Lcom/squareup/protos/client/InventoryAdjustment$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public adjust_count:Ljava/lang/Long;

.field public adjust_quantity_decimal:Ljava/lang/String;

.field public bill_token:Ljava/lang/String;

.field public catalog_version:Ljava/lang/Long;

.field public cost_money:Lcom/squareup/protos/common/Money;

.field public reason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

.field public refund_bill_server_token:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;

.field public variation_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 239
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public adjust_count(Ljava/lang/Long;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;
    .locals 0

    .line 262
    iput-object p1, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->adjust_count:Ljava/lang/Long;

    return-object p0
.end method

.method public adjust_quantity_decimal(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;
    .locals 0

    .line 315
    iput-object p1, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->adjust_quantity_decimal:Ljava/lang/String;

    return-object p0
.end method

.method public bill_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;
    .locals 0

    .line 297
    iput-object p1, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->bill_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/InventoryAdjustment;
    .locals 12

    .line 321
    new-instance v11, Lcom/squareup/protos/client/InventoryAdjustment;

    iget-object v1, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->variation_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->unit_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->adjust_count:Ljava/lang/Long;

    iget-object v4, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->reason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    iget-object v5, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->cost_money:Lcom/squareup/protos/common/Money;

    iget-object v6, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->refund_bill_server_token:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->bill_token:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->catalog_version:Ljava/lang/Long;

    iget-object v9, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->adjust_quantity_decimal:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/InventoryAdjustment;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lcom/squareup/protos/client/InventoryAdjustmentReason;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 220
    invoke-virtual {p0}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->build()Lcom/squareup/protos/client/InventoryAdjustment;

    move-result-object v0

    return-object v0
.end method

.method public catalog_version(Ljava/lang/Long;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;
    .locals 0

    .line 305
    iput-object p1, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->catalog_version:Ljava/lang/Long;

    return-object p0
.end method

.method public cost_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;
    .locals 0

    .line 278
    iput-object p1, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->cost_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public reason(Lcom/squareup/protos/client/InventoryAdjustmentReason;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;
    .locals 0

    .line 270
    iput-object p1, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->reason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    return-object p0
.end method

.method public refund_bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;
    .locals 0

    .line 288
    iput-object p1, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->refund_bill_server_token:Ljava/lang/String;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;
    .locals 0

    .line 254
    iput-object p1, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method

.method public variation_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;
    .locals 0

    .line 246
    iput-object p1, p0, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->variation_token:Ljava/lang/String;

    return-object p0
.end method
