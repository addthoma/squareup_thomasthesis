.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public mobile_staff_id:Ljava/lang/String;

.field public name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

.field public user_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2575
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;
    .locals 5

    .line 2598
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff$Builder;->mobile_staff_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff$Builder;->user_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;-><init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2568
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    move-result-object v0

    return-object v0
.end method

.method public mobile_staff_id(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff$Builder;
    .locals 0

    .line 2587
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff$Builder;->mobile_staff_id:Ljava/lang/String;

    return-object p0
.end method

.method public name_or_translation_type(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff$Builder;
    .locals 0

    .line 2582
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    return-object p0
.end method

.method public user_token(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff$Builder;
    .locals 0

    .line 2592
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff$Builder;->user_token:Ljava/lang/String;

    return-object p0
.end method
