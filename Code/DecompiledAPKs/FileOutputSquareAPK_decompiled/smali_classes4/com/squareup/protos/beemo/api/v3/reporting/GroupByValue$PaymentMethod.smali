.class public final enum Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;
.super Ljava/lang/Enum;
.source "GroupByValue.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PaymentMethod"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod$ProtoAdapter_PaymentMethod;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CASH:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

.field public static final enum CREDIT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

.field public static final enum EMONEY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

.field public static final enum EXTERNAL:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

.field public static final enum INSTALLMENT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

.field public static final enum OTHER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

.field public static final enum SPLIT_TENDER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

.field public static final enum SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

.field public static final enum THIRD_PARTY_CARD:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

.field public static final enum ZERO_AMOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 2977
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    const/4 v1, 0x0

    const-string v2, "CASH"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->CASH:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    .line 2979
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    const/4 v2, 0x1

    const-string v3, "CREDIT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->CREDIT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    .line 2981
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    const/4 v3, 0x2

    const-string v4, "OTHER"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->OTHER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    .line 2983
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    const/4 v4, 0x3

    const-string v5, "SPLIT_TENDER"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->SPLIT_TENDER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    .line 2988
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    const/4 v5, 0x4

    const-string v6, "THIRD_PARTY_CARD"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->THIRD_PARTY_CARD:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    .line 2993
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    const/4 v6, 0x5

    const-string v7, "SQUARE_GIFT_CARD_V2"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    .line 2998
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    const/4 v7, 0x6

    const-string v8, "ZERO_AMOUNT"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ZERO_AMOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    .line 3003
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    const/4 v8, 0x7

    const-string v9, "EXTERNAL"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->EXTERNAL:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    .line 3008
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    const/16 v9, 0x8

    const-string v10, "INSTALLMENT"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->INSTALLMENT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    .line 3013
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    const/16 v10, 0x9

    const-string v11, "EMONEY"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->EMONEY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    .line 2976
    sget-object v11, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->CASH:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    aput-object v11, v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->CREDIT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->OTHER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->SPLIT_TENDER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->THIRD_PARTY_CARD:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ZERO_AMOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->EXTERNAL:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->INSTALLMENT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->EMONEY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    aput-object v1, v0, v10

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    .line 3015
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod$ProtoAdapter_PaymentMethod;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod$ProtoAdapter_PaymentMethod;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 3019
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3020
    iput p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 3037
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->EMONEY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    return-object p0

    .line 3036
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->INSTALLMENT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    return-object p0

    .line 3035
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->EXTERNAL:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    return-object p0

    .line 3034
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ZERO_AMOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    return-object p0

    .line 3033
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    return-object p0

    .line 3032
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->THIRD_PARTY_CARD:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    return-object p0

    .line 3031
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->SPLIT_TENDER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    return-object p0

    .line 3030
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->OTHER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    return-object p0

    .line 3029
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->CREDIT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    return-object p0

    .line 3028
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->CASH:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;
    .locals 1

    .line 2976
    const-class v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;
    .locals 1

    .line 2976
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    invoke-virtual {v0}, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 3044
    iget v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->value:I

    return v0
.end method
