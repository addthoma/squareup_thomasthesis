.class public final Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$SquareProcessingFeeAmountDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Aggregate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$SquareProcessingFeeAmountDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$SquareProcessingFeeAmountDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$SquareProcessingFeeAmountDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public adjustment_fee_money:Lcom/squareup/protos/common/Money;

.field public initial_fee_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1651
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public adjustment_fee_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$SquareProcessingFeeAmountDetails$Builder;
    .locals 0

    .line 1670
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$SquareProcessingFeeAmountDetails$Builder;->adjustment_fee_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$SquareProcessingFeeAmountDetails;
    .locals 4

    .line 1676
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$SquareProcessingFeeAmountDetails;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$SquareProcessingFeeAmountDetails$Builder;->initial_fee_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$SquareProcessingFeeAmountDetails$Builder;->adjustment_fee_money:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$SquareProcessingFeeAmountDetails;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1646
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$SquareProcessingFeeAmountDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$SquareProcessingFeeAmountDetails;

    move-result-object v0

    return-object v0
.end method

.method public initial_fee_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$SquareProcessingFeeAmountDetails$Builder;
    .locals 0

    .line 1658
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$SquareProcessingFeeAmountDetails$Builder;->initial_fee_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
