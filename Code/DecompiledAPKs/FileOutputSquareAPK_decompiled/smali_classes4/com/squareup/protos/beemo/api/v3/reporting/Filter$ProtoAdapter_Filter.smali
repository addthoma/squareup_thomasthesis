.class final Lcom/squareup/protos/beemo/api/v3/reporting/Filter$ProtoAdapter_Filter;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Filter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Filter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Filter;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 266
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 291
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;-><init>()V

    .line 292
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 293
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 315
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 313
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->reporting_group_value:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 312
    :cond_1
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->grouping_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;

    goto :goto_0

    .line 306
    :cond_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->filter_mode(Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 308
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 303
    :cond_3
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_value:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 297
    :cond_4
    :try_start_1
    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 299
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 319
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 320
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Filter;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 264
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$ProtoAdapter_Filter;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/Filter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 281
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 282
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_value:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 283
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->filter_mode:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 284
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->grouping_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 285
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->reporting_group_value:Ljava/util/List;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 286
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 264
    check-cast p2, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$ProtoAdapter_Filter;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/Filter;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/Filter;)I
    .locals 4

    .line 271
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 272
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_value:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->filter_mode:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    const/4 v3, 0x3

    .line 273
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->grouping_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    const/4 v3, 0x4

    .line 274
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 275
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->reporting_group_value:Ljava/util/List;

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 276
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 264
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$ProtoAdapter_Filter;->encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/Filter;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v3/reporting/Filter;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter;
    .locals 2

    .line 325
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;

    move-result-object p1

    .line 326
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_value:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 327
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->grouping_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->grouping_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->grouping_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    .line 328
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->reporting_group_value:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 329
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 330
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Filter;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 264
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$ProtoAdapter_Filter;->redact(Lcom/squareup/protos/beemo/api/v3/reporting/Filter;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter;

    move-result-object p1

    return-object p1
.end method
