.class final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$ProtoAdapter_TaxDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_TaxDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 5459
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5480
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;-><init>()V

    .line 5481
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 5482
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 5495
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 5493
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->rate(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;

    goto :goto_0

    .line 5487
    :cond_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->inclusion_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 5489
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 5484
    :cond_2
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->tax(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;

    goto :goto_0

    .line 5499
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 5500
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5457
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$ProtoAdapter_TaxDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5472
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5473
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;->inclusion_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5474
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;->rate:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5475
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5457
    check-cast p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$ProtoAdapter_TaxDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;)I
    .locals 4

    .line 5464
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;->inclusion_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    const/4 v3, 0x2

    .line 5465
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;->rate:Ljava/lang/String;

    const/4 v3, 0x3

    .line 5466
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5467
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 5457
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$ProtoAdapter_TaxDetails;->encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;
    .locals 2

    .line 5505
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;

    move-result-object p1

    .line 5506
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    .line 5507
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 5508
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 5457
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$ProtoAdapter_TaxDetails;->redact(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;

    move-result-object p1

    return-object p1
.end method
