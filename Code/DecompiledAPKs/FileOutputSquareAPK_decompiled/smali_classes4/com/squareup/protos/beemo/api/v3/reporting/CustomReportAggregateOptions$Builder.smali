.class public final Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CustomReportAggregateOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public include_bill_event_tokens:Ljava/lang/Boolean;

.field public include_gift_card_itemizations:Ljava/lang/Boolean;

.field public include_partial_payment_details:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 136
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;
    .locals 5

    .line 178
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;->include_gift_card_itemizations:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;->include_bill_event_tokens:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;->include_partial_payment_details:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    move-result-object v0

    return-object v0
.end method

.method public include_bill_event_tokens(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;->include_bill_event_tokens:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_gift_card_itemizations(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;->include_gift_card_itemizations:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_partial_payment_details(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;->include_partial_payment_details:Ljava/lang/Boolean;

    return-object p0
.end method
