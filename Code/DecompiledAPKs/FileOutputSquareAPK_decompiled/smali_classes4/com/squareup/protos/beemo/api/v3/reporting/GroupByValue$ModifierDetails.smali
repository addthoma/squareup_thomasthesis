.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;
.super Lcom/squareup/wire/Message;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ModifierDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$ProtoAdapter_ModifierDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$Modifier#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final single_quantity_applied_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5160
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$ProtoAdapter_ModifierDetails;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$ProtoAdapter_ModifierDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 5177
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 5182
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 5183
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    .line 5184
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 5199
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 5200
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;

    .line 5201
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    .line 5202
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    .line 5203
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 5208
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 5210
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 5211
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5212
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 5213
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;
    .locals 2

    .line 5189
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;-><init>()V

    .line 5190
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    .line 5191
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    .line 5192
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 5159
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 5220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 5221
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    if-eqz v1, :cond_0

    const-string v1, ", modifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5222
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", single_quantity_applied_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ModifierDetails{"

    .line 5223
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
