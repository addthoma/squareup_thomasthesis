.class public final Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CustomReportRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public aggregate_options:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

.field public filter:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Filter;",
            ">;"
        }
    .end annotation
.end field

.field public group_by_type:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            ">;"
        }
    .end annotation
.end field

.field public grouping_type:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;",
            ">;"
        }
    .end annotation
.end field

.field public request_flags:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

.field public request_params:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 168
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 169
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->group_by_type:Ljava/util/List;

    .line 170
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->filter:Ljava/util/List;

    .line 171
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->grouping_type:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public aggregate_options(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;
    .locals 0

    .line 208
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->aggregate_options:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;
    .locals 9

    .line 224
    new-instance v8, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->request_params:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->group_by_type:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->filter:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->request_flags:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    iget-object v5, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->aggregate_options:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    iget-object v6, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->grouping_type:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;-><init>(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;Ljava/util/List;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 155
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    move-result-object v0

    return-object v0
.end method

.method public filter(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Filter;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;"
        }
    .end annotation

    .line 197
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 198
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->filter:Ljava/util/List;

    return-object p0
.end method

.method public group_by_type(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 187
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 188
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->group_by_type:Ljava/util/List;

    return-object p0
.end method

.method public grouping_type(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;"
        }
    .end annotation

    .line 217
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 218
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->grouping_type:Ljava/util/List;

    return-object p0
.end method

.method public request_flags(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->request_flags:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    return-object p0
.end method

.method public request_params(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;
    .locals 0

    .line 175
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->request_params:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    return-object p0
.end method
