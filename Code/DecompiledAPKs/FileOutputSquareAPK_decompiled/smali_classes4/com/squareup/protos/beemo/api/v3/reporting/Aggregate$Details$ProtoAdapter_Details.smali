.class final Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProtoAdapter_Details;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Aggregate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Details"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2446
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2553
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;-><init>()V

    .line 2554
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2555
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 2604
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2602
    :pswitch_1
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->measured_quantity:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2601
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->taxable_item_net_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto :goto_0

    .line 2600
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->taxable_net_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto :goto_0

    .line 2599
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_count(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto :goto_0

    .line 2598
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->total_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto :goto_0

    .line 2597
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->refund_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto :goto_0

    .line 2596
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tender_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto :goto_0

    .line 2595
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->surcharge_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto :goto_0

    .line 2594
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_net_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto :goto_0

    .line 2593
    :pswitch_a
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_gross_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2592
    :pswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->cover_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2591
    :pswitch_c
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->auto_gratuity_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2590
    :pswitch_d
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->surcharge_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2589
    :pswitch_e
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_cards_discounted_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2588
    :pswitch_f
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_cards_discounted_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2587
    :pswitch_10
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tender_tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2586
    :pswitch_11
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->product_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2585
    :pswitch_12
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_cost_of_goods_amounts(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2584
    :pswitch_13
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->lifo_cost_of_goods_amounts(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2583
    :pswitch_14
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->fifo_cost_of_goods_amounts(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2582
    :pswitch_15
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->modifier_quantity(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2581
    :pswitch_16
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->processing_fee_amounts(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2580
    :pswitch_17
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->items_modified_count(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2579
    :pswitch_18
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->items_discounted_count(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2578
    :pswitch_19
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->items_taxed_count(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2577
    :pswitch_1a
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_australian_gross_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2576
    :pswitch_1b
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->australian_gross_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2575
    :pswitch_1c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_load_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2574
    :pswitch_1d
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_gross_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2573
    :pswitch_1e
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_load_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2572
    :pswitch_1f
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_net_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2571
    :pswitch_20
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->swedish_rounding_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2570
    :pswitch_21
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->credit_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2569
    :pswitch_22
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->net_total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2568
    :pswitch_23
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->total_collected_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2567
    :pswitch_24
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->net_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2566
    :pswitch_25
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_quantity(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2565
    :pswitch_26
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2564
    :pswitch_27
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2563
    :pswitch_28
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->processing_fee_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2562
    :pswitch_29
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gross_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2561
    :pswitch_2a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->modifier_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2560
    :pswitch_2b
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->modifier_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2559
    :pswitch_2c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->discount_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2558
    :pswitch_2d
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->discount_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2557
    :pswitch_2e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->transaction_count(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    goto/16 :goto_0

    .line 2608
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2609
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_0
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2444
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProtoAdapter_Details;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2502
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->transaction_count:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2503
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2504
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_count:Ljava/lang/Long;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2505
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2506
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_count:Ljava/lang/Long;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2507
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_quantity:Ljava/lang/String;

    const/16 v2, 0x1b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2508
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2509
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x26

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2510
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->product_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x1f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2511
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2512
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tip_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2513
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tax_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2514
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_tax_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x20

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2515
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_quantity:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2516
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_count:Ljava/lang/String;

    const/16 v2, 0x2c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2517
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2518
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_net_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x27

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2519
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_net_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x2d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2520
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_item_net_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x2e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2521
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_collected_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2522
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_total_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2523
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->credit_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2524
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2525
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2526
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2527
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_gross_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2528
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_count:Ljava/lang/Long;

    const/16 v2, 0x13

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2529
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_count:Ljava/lang/Long;

    const/16 v2, 0x21

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2530
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_amount:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x22

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2531
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x15

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2532
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x16

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2533
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_taxed_count:Ljava/lang/String;

    const/16 v2, 0x17

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2534
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_discounted_count:Ljava/lang/String;

    const/16 v2, 0x18

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2535
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_modified_count:Ljava/lang/String;

    const/16 v2, 0x19

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2536
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

    const/16 v2, 0x1a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2537
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->fifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    const/16 v2, 0x1c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2538
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->lifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    const/16 v2, 0x1d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2539
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    const/16 v2, 0x1e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2540
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x23

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2541
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x24

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2542
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->cover_count:Ljava/lang/Long;

    const/16 v2, 0x25

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2543
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_count:Ljava/lang/Long;

    const/16 v2, 0x28

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2544
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_count:Ljava/lang/Long;

    const/16 v2, 0x29

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2545
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->refund_count:Ljava/lang/Long;

    const/16 v2, 0x2a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2546
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x2b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2547
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->measured_quantity:Ljava/util/List;

    const/16 v2, 0x2f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2548
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2444
    check-cast p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProtoAdapter_Details;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;)I
    .locals 4

    .line 2451
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->transaction_count:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x2

    .line 2452
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_count:Ljava/lang/Long;

    const/4 v3, 0x3

    .line 2453
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x4

    .line 2454
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_count:Ljava/lang/Long;

    const/4 v3, 0x5

    .line 2455
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_quantity:Ljava/lang/String;

    const/16 v3, 0x1b

    .line 2456
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x6

    .line 2457
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x26

    .line 2458
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->product_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x1f

    .line 2459
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x7

    .line 2460
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tip_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x8

    .line 2461
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tax_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x9

    .line 2462
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_tax_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x20

    .line 2463
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_quantity:Ljava/lang/String;

    const/16 v3, 0xa

    .line 2464
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_count:Ljava/lang/String;

    const/16 v3, 0x2c

    .line 2465
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xb

    .line 2466
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_net_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x27

    .line 2467
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_net_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x2d

    .line 2468
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_item_net_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x2e

    .line 2469
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_collected_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xc

    .line 2470
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_total_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xd

    .line 2471
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->credit_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xe

    .line 2472
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xf

    .line 2473
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x10

    .line 2474
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x11

    .line 2475
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_gross_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x12

    .line 2476
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_count:Ljava/lang/Long;

    const/16 v3, 0x13

    .line 2477
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_count:Ljava/lang/Long;

    const/16 v3, 0x21

    .line 2478
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_amount:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x22

    .line 2479
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x15

    .line 2480
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x16

    .line 2481
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_taxed_count:Ljava/lang/String;

    const/16 v3, 0x17

    .line 2482
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_discounted_count:Ljava/lang/String;

    const/16 v3, 0x18

    .line 2483
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_modified_count:Ljava/lang/String;

    const/16 v3, 0x19

    .line 2484
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

    const/16 v3, 0x1a

    .line 2485
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->fifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    const/16 v3, 0x1c

    .line 2486
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->lifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    const/16 v3, 0x1d

    .line 2487
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    const/16 v3, 0x1e

    .line 2488
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x23

    .line 2489
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x24

    .line 2490
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->cover_count:Ljava/lang/Long;

    const/16 v3, 0x25

    .line 2491
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_count:Ljava/lang/Long;

    const/16 v3, 0x28

    .line 2492
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_count:Ljava/lang/Long;

    const/16 v3, 0x29

    .line 2493
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->refund_count:Ljava/lang/Long;

    const/16 v3, 0x2a

    .line 2494
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x2b

    .line 2495
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 2496
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->measured_quantity:Ljava/util/List;

    const/16 v3, 0x2f

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2497
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2444
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProtoAdapter_Details;->encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;
    .locals 2

    .line 2614
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    move-result-object p1

    .line 2615
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->discount_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->discount_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->discount_money:Lcom/squareup/protos/common/Money;

    .line 2616
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->modifier_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->modifier_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->modifier_money:Lcom/squareup/protos/common/Money;

    .line 2617
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gross_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 2618
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 2619
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->product_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->product_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->product_sales_money:Lcom/squareup/protos/common/Money;

    .line 2620
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->processing_fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->processing_fee_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->processing_fee_money:Lcom/squareup/protos/common/Money;

    .line 2621
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    .line 2622
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    .line 2623
    :cond_7
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tender_tax_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tender_tax_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tender_tax_money:Lcom/squareup/protos/common/Money;

    .line 2624
    :cond_8
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->net_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->net_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->net_sales_money:Lcom/squareup/protos/common/Money;

    .line 2625
    :cond_9
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_net_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_net_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_net_sales_money:Lcom/squareup/protos/common/Money;

    .line 2626
    :cond_a
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->taxable_net_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->taxable_net_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->taxable_net_sales_money:Lcom/squareup/protos/common/Money;

    .line 2627
    :cond_b
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->taxable_item_net_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->taxable_item_net_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->taxable_item_net_sales_money:Lcom/squareup/protos/common/Money;

    .line 2628
    :cond_c
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->total_collected_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->total_collected_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->total_collected_money:Lcom/squareup/protos/common/Money;

    .line 2629
    :cond_d
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->net_total_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->net_total_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->net_total_money:Lcom/squareup/protos/common/Money;

    .line 2630
    :cond_e
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->credit_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_f

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->credit_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->credit_money:Lcom/squareup/protos/common/Money;

    .line 2631
    :cond_f
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_10

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    .line 2632
    :cond_10
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_11

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

    .line 2633
    :cond_11
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_load_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_12

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_load_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_load_money:Lcom/squareup/protos/common/Money;

    .line 2634
    :cond_12
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_gross_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_13

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 2635
    :cond_13
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_cards_discounted_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_14

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_cards_discounted_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_cards_discounted_amount:Lcom/squareup/protos/common/Money;

    .line 2636
    :cond_14
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_15

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 2637
    :cond_15
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_16

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 2638
    :cond_16
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->processing_fee_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

    if-eqz v0, :cond_17

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->processing_fee_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->processing_fee_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

    .line 2639
    :cond_17
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->fifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    if-eqz v0, :cond_18

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->fifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->fifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    .line 2640
    :cond_18
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->lifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    if-eqz v0, :cond_19

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->lifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->lifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    .line 2641
    :cond_19
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    if-eqz v0, :cond_1a

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    .line 2642
    :cond_1a
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->surcharge_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1b

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->surcharge_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->surcharge_money:Lcom/squareup/protos/common/Money;

    .line 2643
    :cond_1b
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1c

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    .line 2644
    :cond_1c
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->total_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1d

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->total_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->total_sales_money:Lcom/squareup/protos/common/Money;

    .line 2645
    :cond_1d
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->measured_quantity:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 2646
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2647
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2444
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProtoAdapter_Details;->redact(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    move-result-object p1

    return-object p1
.end method
