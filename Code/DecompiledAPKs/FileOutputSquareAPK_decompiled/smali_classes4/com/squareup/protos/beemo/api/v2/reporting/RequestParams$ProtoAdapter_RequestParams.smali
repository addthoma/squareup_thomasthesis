.class final Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$ProtoAdapter_RequestParams;
.super Lcom/squareup/wire/ProtoAdapter;
.source "RequestParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_RequestParams"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 666
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 709
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;-><init>()V

    .line 710
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 711
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/16 v4, 0x64

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 728
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 725
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->start_of_day_offset_minutes(Ljava/lang/Integer;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    goto :goto_0

    .line 724
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->request_flags(Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    goto :goto_0

    .line 723
    :pswitch_2
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->selected_bill_token:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 722
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->event_based(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    goto :goto_0

    .line 721
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->tz_name(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    goto :goto_0

    .line 720
    :pswitch_5
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->subunit_merchant_token:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 719
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->time_filter(Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    goto :goto_0

    .line 718
    :pswitch_7
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->sort_param:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/beemo/api/v2/reporting/SortParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 717
    :pswitch_8
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->selected_payment_token:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 716
    :pswitch_9
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->creator_token:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 715
    :pswitch_a
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->end_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    goto/16 :goto_0

    .line 714
    :pswitch_b
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->begin_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    goto/16 :goto_0

    .line 713
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    goto/16 :goto_0

    .line 726
    :cond_0
    sget-object v3, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->beemo_internal_request_flags(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    goto/16 :goto_0

    .line 732
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 733
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->build()Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 664
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$ProtoAdapter_RequestParams;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 690
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 691
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 692
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->end_time:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 693
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->creator_token:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 694
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_payment_token:Ljava/util/List;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 695
    sget-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/SortParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->sort_param:Ljava/util/List;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 696
    sget-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->time_filter:Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 697
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->subunit_merchant_token:Ljava/util/List;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 698
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->tz_name:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 699
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->event_based:Ljava/lang/Boolean;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 700
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_bill_token:Ljava/util/List;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 701
    sget-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 702
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->start_of_day_offset_minutes:Ljava/lang/Integer;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 703
    sget-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->beemo_internal_request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    const/16 v2, 0x64

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 704
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 664
    check-cast p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$ProtoAdapter_RequestParams;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;)I
    .locals 4

    .line 671
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    const/4 v3, 0x2

    .line 672
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->end_time:Lcom/squareup/protos/common/time/DateTime;

    const/4 v3, 0x3

    .line 673
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 674
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->creator_token:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 675
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_payment_token:Ljava/util/List;

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v2/reporting/SortParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 676
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->sort_param:Ljava/util/List;

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->time_filter:Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    const/4 v3, 0x7

    .line 677
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 678
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->subunit_merchant_token:Ljava/util/List;

    const/16 v3, 0x8

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->tz_name:Ljava/lang/String;

    const/16 v3, 0x9

    .line 679
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->event_based:Ljava/lang/Boolean;

    const/16 v3, 0xa

    .line 680
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 681
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_bill_token:Ljava/util/List;

    const/16 v3, 0xb

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    const/16 v3, 0xc

    .line 682
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->start_of_day_offset_minutes:Ljava/lang/Integer;

    const/16 v3, 0xd

    .line 683
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->beemo_internal_request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    const/16 v3, 0x64

    .line 684
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 685
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 664
    check-cast p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$ProtoAdapter_RequestParams;->encodedSize(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;
    .locals 2

    .line 738
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->newBuilder()Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    move-result-object p1

    .line 739
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    .line 740
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->end_time:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->end_time:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->end_time:Lcom/squareup/protos/common/time/DateTime;

    .line 741
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->sort_param:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/beemo/api/v2/reporting/SortParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 742
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->time_filter:Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->time_filter:Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->time_filter:Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    .line 743
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    .line 744
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->beemo_internal_request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->beemo_internal_request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->beemo_internal_request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    .line 745
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 746
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->build()Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 664
    check-cast p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$ProtoAdapter_RequestParams;->redact(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    move-result-object p1

    return-object p1
.end method
