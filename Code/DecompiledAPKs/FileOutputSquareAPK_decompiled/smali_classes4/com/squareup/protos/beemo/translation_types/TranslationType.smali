.class public final enum Lcom/squareup/protos/beemo/translation_types/TranslationType;
.super Ljava/lang/Enum;
.source "TranslationType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/translation_types/TranslationType$ProtoAdapter_TranslationType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/beemo/translation_types/TranslationType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/translation_types/TranslationType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CASH_REFUND:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum CHANGE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum CREDIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum CUSTOM_AMOUNT_ITEM:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum CUSTOM_AMOUNT_ITEM_VARIATION:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum DEFAULT_COMP_REASON:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum DEFAULT_DEVICE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum DEFAULT_DEVICE_CREDENTIAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum DEFAULT_DISCOUNT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum DEFAULT_ITEM:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum DEFAULT_ITEM_MODIFIER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum DEFAULT_ITEM_VARIATION:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum DEFAULT_MEASUREMENT_UNIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum DEFAULT_MODIFIER_SET:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum DEFAULT_ORDER_SOURCE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum DEFAULT_SALES_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum DEFAULT_SURCHARGE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum DEFAULT_TIP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum EMPLOYEE_ROLE_ACCOUNT_OWNER_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum EMPLOYEE_ROLE_DELETED_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum EMPLOYEE_ROLE_MOBILE_STAFF_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum EMPLOYEE_ROLE_OWNER_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum INCLUSIVE_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_CATEGORY:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_COMP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_COMP_REASON:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_CONTACT_TOKEN:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_CONVERSATIONAL_MODE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_DEVICE_CREDENTIAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_DINING_OPTION:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_DISCOUNT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_EMPLOYEE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_EMPLOYEE_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_GIFT_CARD:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_MENU:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_MOBILE_STAFF:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_MODIFIER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_MODIFIER_SET:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_PAYMENT_SOURCE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_REPORTING_GROUP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_SUBUNIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_SURCHARGE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_TICKET_GROUP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_TICKET_TEMPLATE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_TICKET_TEMPLATE_CUSTOM_CHECK:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_VENDOR:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum NO_VOID_REASON:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum ORDER_SOURCE_APPOINTMENTS:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum ORDER_SOURCE_BILLING:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum ORDER_SOURCE_CAPITAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum ORDER_SOURCE_CASH:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum ORDER_SOURCE_EGIFTING:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum ORDER_SOURCE_EXTERNAL_API:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum ORDER_SOURCE_INVOICES:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum ORDER_SOURCE_MARKET:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum ORDER_SOURCE_ONLINE_STORE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum ORDER_SOURCE_ORDER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum ORDER_SOURCE_PAYROLL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum ORDER_SOURCE_REGISTER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum ORDER_SOURCE_REGISTER_HARDWARE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum ORDER_SOURCE_STARBUCKS:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum ORDER_SOURCE_STORE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum ORDER_SOURCE_TERMINAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum ORDER_SOURCE_WHITE_LABEL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum REFUND:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum SURCHARGE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum SURCHARGE_AUTO_GRATUITY:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum SWEDISH_ROUNDING:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum TICKET_GROUP_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum TICKET_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum UNITEMIZED_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum UNKNOWN_PAYMENT_SOURCE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final enum UNKNOWN_VENDOR:Lcom/squareup/protos/beemo/translation_types/TranslationType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 18
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/4 v1, 0x0

    const-string v2, "CUSTOM_AMOUNT_ITEM"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CUSTOM_AMOUNT_ITEM:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 23
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/4 v2, 0x1

    const-string v3, "UNITEMIZED_TAX"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->UNITEMIZED_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 28
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/4 v3, 0x2

    const-string v4, "DEFAULT_DISCOUNT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_DISCOUNT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 34
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/4 v4, 0x3

    const-string v5, "DEFAULT_ITEM"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_ITEM:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 39
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/4 v5, 0x4

    const-string v6, "DEFAULT_SALES_TAX"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_SALES_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 44
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/4 v6, 0x5

    const-string v7, "DEFAULT_TIP"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_TIP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 49
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/4 v7, 0x6

    const-string v8, "CHANGE"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CHANGE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 54
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/4 v8, 0x7

    const-string v9, "REFUND"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->REFUND:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 59
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v9, 0x8

    const-string v10, "CASH_REFUND"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CASH_REFUND:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 64
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v10, 0x9

    const-string v11, "INCLUSIVE_TAX"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->INCLUSIVE_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 69
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v11, 0xa

    const-string v12, "DEFAULT_ITEM_MODIFIER"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_ITEM_MODIFIER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 74
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v12, 0xb

    const-string v13, "CREDIT"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CREDIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 79
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v13, 0xc

    const-string v14, "SWEDISH_ROUNDING"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->SWEDISH_ROUNDING:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 84
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v14, 0xd

    const-string v15, "SURCHARGE"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->SURCHARGE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 89
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v15, 0xe

    const-string v14, "DEFAULT_DEVICE"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_DEVICE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 94
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v14, "NO_CATEGORY"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_CATEGORY:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 99
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_DISCOUNT"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_DISCOUNT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 104
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_MODIFIER"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_MODIFIER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 110
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_SUBUNIT"

    const/16 v14, 0x12

    const/16 v15, 0x12

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_SUBUNIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 115
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_MOBILE_STAFF"

    const/16 v14, 0x13

    const/16 v15, 0x13

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_MOBILE_STAFF:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 120
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_EMPLOYEE"

    const/16 v14, 0x14

    const/16 v15, 0x14

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_EMPLOYEE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 126
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "CUSTOM_AMOUNT_ITEM_VARIATION"

    const/16 v14, 0x15

    const/16 v15, 0x15

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CUSTOM_AMOUNT_ITEM_VARIATION:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 132
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "DEFAULT_ITEM_VARIATION"

    const/16 v14, 0x16

    const/16 v15, 0x16

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_ITEM_VARIATION:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 138
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_MODIFIER_SET"

    const/16 v14, 0x17

    const/16 v15, 0x17

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_MODIFIER_SET:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 144
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "DEFAULT_MODIFIER_SET"

    const/16 v14, 0x18

    const/16 v15, 0x18

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_MODIFIER_SET:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 149
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_TAX"

    const/16 v14, 0x19

    const/16 v15, 0x19

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 154
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_DINING_OPTION"

    const/16 v14, 0x1a

    const/16 v15, 0x1a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_DINING_OPTION:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 159
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_GIFT_CARD"

    const/16 v14, 0x1b

    const/16 v15, 0x1b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_GIFT_CARD:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 164
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_VOID_REASON"

    const/16 v14, 0x1c

    const/16 v15, 0x1c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_VOID_REASON:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 170
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_COMP_REASON"

    const/16 v14, 0x1d

    const/16 v15, 0x1d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_COMP_REASON:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 175
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "DEFAULT_COMP_REASON"

    const/16 v14, 0x1e

    const/16 v15, 0x1e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_COMP_REASON:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 180
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_COMP"

    const/16 v14, 0x1f

    const/16 v15, 0x1f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_COMP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 185
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "TICKET_NAME"

    const/16 v14, 0x20

    const/16 v15, 0x20

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->TICKET_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 190
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "TICKET_GROUP_NAME"

    const/16 v14, 0x21

    const/16 v15, 0x21

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->TICKET_GROUP_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 196
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_TICKET_GROUP"

    const/16 v14, 0x22

    const/16 v15, 0x22

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_TICKET_GROUP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 202
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_DEVICE_CREDENTIAL"

    const/16 v14, 0x23

    const/16 v15, 0x23

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_DEVICE_CREDENTIAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 208
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "DEFAULT_DEVICE_CREDENTIAL"

    const/16 v14, 0x24

    const/16 v15, 0x24

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_DEVICE_CREDENTIAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 214
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_EMPLOYEE_ROLE"

    const/16 v14, 0x25

    const/16 v15, 0x25

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_EMPLOYEE_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 220
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "EMPLOYEE_ROLE_ACCOUNT_OWNER_ROLE"

    const/16 v14, 0x26

    const/16 v15, 0x26

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->EMPLOYEE_ROLE_ACCOUNT_OWNER_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 228
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "EMPLOYEE_ROLE_OWNER_ROLE"

    const/16 v14, 0x27

    const/16 v15, 0x27

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->EMPLOYEE_ROLE_OWNER_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 234
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "EMPLOYEE_ROLE_DELETED_ROLE"

    const/16 v14, 0x28

    const/16 v15, 0x28

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->EMPLOYEE_ROLE_DELETED_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 240
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "EMPLOYEE_ROLE_MOBILE_STAFF_ROLE"

    const/16 v14, 0x29

    const/16 v15, 0x29

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->EMPLOYEE_ROLE_MOBILE_STAFF_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 246
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_CONVERSATIONAL_MODE"

    const/16 v14, 0x2a

    const/16 v15, 0x2a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_CONVERSATIONAL_MODE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 251
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_MENU"

    const/16 v14, 0x2b

    const/16 v15, 0x2b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_MENU:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 261
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_TICKET_TEMPLATE"

    const/16 v14, 0x2c

    const/16 v15, 0x2c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_TICKET_TEMPLATE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 266
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_TICKET_TEMPLATE_CUSTOM_CHECK"

    const/16 v14, 0x2d

    const/16 v15, 0x2d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_TICKET_TEMPLATE_CUSTOM_CHECK:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 271
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_PAYMENT_SOURCE_NAME"

    const/16 v14, 0x2e

    const/16 v15, 0x2e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_PAYMENT_SOURCE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 276
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "UNKNOWN_PAYMENT_SOURCE_NAME"

    const/16 v14, 0x2f

    const/16 v15, 0x2f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->UNKNOWN_PAYMENT_SOURCE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 281
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_CONTACT_TOKEN"

    const/16 v14, 0x30

    const/16 v15, 0x30

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_CONTACT_TOKEN:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 286
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_REPORTING_GROUP"

    const/16 v14, 0x31

    const/16 v15, 0x31

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_REPORTING_GROUP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 291
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_SURCHARGE"

    const/16 v14, 0x32

    const/16 v15, 0x32

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_SURCHARGE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 296
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "SURCHARGE_AUTO_GRATUITY"

    const/16 v14, 0x33

    const/16 v15, 0x33

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->SURCHARGE_AUTO_GRATUITY:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 301
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "DEFAULT_SURCHARGE_NAME"

    const/16 v14, 0x34

    const/16 v15, 0x34

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_SURCHARGE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 306
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "DEFAULT_ORDER_SOURCE_NAME"

    const/16 v14, 0x35

    const/16 v15, 0x35

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_ORDER_SOURCE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 311
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "ORDER_SOURCE_REGISTER"

    const/16 v14, 0x36

    const/16 v15, 0x36

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_REGISTER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 316
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "ORDER_SOURCE_MARKET"

    const/16 v14, 0x37

    const/16 v15, 0x37

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_MARKET:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 321
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "ORDER_SOURCE_ORDER"

    const/16 v14, 0x38

    const/16 v15, 0x38

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_ORDER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 326
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "ORDER_SOURCE_EXTERNAL_API"

    const/16 v14, 0x39

    const/16 v15, 0x39

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_EXTERNAL_API:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 331
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "ORDER_SOURCE_APPOINTMENTS"

    const/16 v14, 0x3a

    const/16 v15, 0x3a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_APPOINTMENTS:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 336
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "ORDER_SOURCE_INVOICES"

    const/16 v14, 0x3b

    const/16 v15, 0x3b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_INVOICES:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 341
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "ORDER_SOURCE_BILLING"

    const/16 v14, 0x3c

    const/16 v15, 0x3c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_BILLING:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 346
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "ORDER_SOURCE_STORE"

    const/16 v14, 0x3d

    const/16 v15, 0x3d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_STORE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 351
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "ORDER_SOURCE_ONLINE_STORE"

    const/16 v14, 0x3e

    const/16 v15, 0x3e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_ONLINE_STORE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 356
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "ORDER_SOURCE_STARBUCKS"

    const/16 v14, 0x3f

    const/16 v15, 0x3f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_STARBUCKS:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 361
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "ORDER_SOURCE_WHITE_LABEL"

    const/16 v14, 0x40

    const/16 v15, 0x40

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_WHITE_LABEL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 366
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "ORDER_SOURCE_PAYROLL"

    const/16 v14, 0x41

    const/16 v15, 0x41

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_PAYROLL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 371
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "ORDER_SOURCE_CAPITAL"

    const/16 v14, 0x42

    const/16 v15, 0x42

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_CAPITAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 376
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "ORDER_SOURCE_TERMINAL"

    const/16 v14, 0x43

    const/16 v15, 0x43

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_TERMINAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 381
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "ORDER_SOURCE_EGIFTING"

    const/16 v14, 0x44

    const/16 v15, 0x44

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_EGIFTING:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 386
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "ORDER_SOURCE_CASH"

    const/16 v14, 0x45

    const/16 v15, 0x45

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_CASH:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 391
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "ORDER_SOURCE_REGISTER_HARDWARE"

    const/16 v14, 0x46

    const/16 v15, 0x46

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_REGISTER_HARDWARE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 396
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "DEFAULT_MEASUREMENT_UNIT"

    const/16 v14, 0x47

    const/16 v15, 0x47

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_MEASUREMENT_UNIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 401
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "NO_VENDOR"

    const/16 v14, 0x48

    const/16 v15, 0x48

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_VENDOR:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 406
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const-string v13, "UNKNOWN_VENDOR"

    const/16 v14, 0x49

    const/16 v15, 0x49

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/translation_types/TranslationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->UNKNOWN_VENDOR:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v0, 0x4a

    new-array v0, v0, [Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 14
    sget-object v13, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CUSTOM_AMOUNT_ITEM:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->UNITEMIZED_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_DISCOUNT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_ITEM:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_SALES_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_TIP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CHANGE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->REFUND:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CASH_REFUND:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->INCLUSIVE_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_ITEM_MODIFIER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CREDIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->SWEDISH_ROUNDING:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->SURCHARGE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_DEVICE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_CATEGORY:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_DISCOUNT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_MODIFIER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_SUBUNIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_MOBILE_STAFF:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_EMPLOYEE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CUSTOM_AMOUNT_ITEM_VARIATION:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_ITEM_VARIATION:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_MODIFIER_SET:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_MODIFIER_SET:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_DINING_OPTION:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_GIFT_CARD:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_VOID_REASON:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_COMP_REASON:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_COMP_REASON:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_COMP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->TICKET_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->TICKET_GROUP_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_TICKET_GROUP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_DEVICE_CREDENTIAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_DEVICE_CREDENTIAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_EMPLOYEE_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->EMPLOYEE_ROLE_ACCOUNT_OWNER_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->EMPLOYEE_ROLE_OWNER_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->EMPLOYEE_ROLE_DELETED_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->EMPLOYEE_ROLE_MOBILE_STAFF_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_CONVERSATIONAL_MODE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_MENU:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_TICKET_TEMPLATE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_TICKET_TEMPLATE_CUSTOM_CHECK:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_PAYMENT_SOURCE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->UNKNOWN_PAYMENT_SOURCE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_CONTACT_TOKEN:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_REPORTING_GROUP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_SURCHARGE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->SURCHARGE_AUTO_GRATUITY:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_SURCHARGE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_ORDER_SOURCE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_REGISTER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_MARKET:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_ORDER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_EXTERNAL_API:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_APPOINTMENTS:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_INVOICES:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_BILLING:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_STORE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_ONLINE_STORE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_STARBUCKS:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_WHITE_LABEL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_PAYROLL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_CAPITAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_TERMINAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_EGIFTING:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_CASH:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_REGISTER_HARDWARE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_MEASUREMENT_UNIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_VENDOR:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->UNKNOWN_VENDOR:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->$VALUES:[Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 408
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/TranslationType$ProtoAdapter_TranslationType;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/translation_types/TranslationType$ProtoAdapter_TranslationType;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 412
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 413
    iput p3, p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/beemo/translation_types/TranslationType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 494
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->UNKNOWN_VENDOR:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 493
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_VENDOR:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 492
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_MEASUREMENT_UNIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 491
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_REGISTER_HARDWARE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 490
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_CASH:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 489
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_EGIFTING:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 488
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_TERMINAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 487
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_CAPITAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 486
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_PAYROLL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 485
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_WHITE_LABEL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 484
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_STARBUCKS:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 483
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_ONLINE_STORE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 482
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_STORE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 481
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_BILLING:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 480
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_INVOICES:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 479
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_APPOINTMENTS:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 478
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_EXTERNAL_API:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 477
    :pswitch_11
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_ORDER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 476
    :pswitch_12
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_MARKET:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 475
    :pswitch_13
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_REGISTER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 474
    :pswitch_14
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_ORDER_SOURCE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 473
    :pswitch_15
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_SURCHARGE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 472
    :pswitch_16
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->SURCHARGE_AUTO_GRATUITY:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 471
    :pswitch_17
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_SURCHARGE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 470
    :pswitch_18
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_REPORTING_GROUP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 469
    :pswitch_19
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_CONTACT_TOKEN:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 468
    :pswitch_1a
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->UNKNOWN_PAYMENT_SOURCE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 467
    :pswitch_1b
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_PAYMENT_SOURCE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 466
    :pswitch_1c
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_TICKET_TEMPLATE_CUSTOM_CHECK:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 465
    :pswitch_1d
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_TICKET_TEMPLATE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 464
    :pswitch_1e
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_MENU:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 463
    :pswitch_1f
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_CONVERSATIONAL_MODE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 462
    :pswitch_20
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->EMPLOYEE_ROLE_MOBILE_STAFF_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 461
    :pswitch_21
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->EMPLOYEE_ROLE_DELETED_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 460
    :pswitch_22
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->EMPLOYEE_ROLE_OWNER_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 459
    :pswitch_23
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->EMPLOYEE_ROLE_ACCOUNT_OWNER_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 458
    :pswitch_24
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_EMPLOYEE_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 457
    :pswitch_25
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_DEVICE_CREDENTIAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 456
    :pswitch_26
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_DEVICE_CREDENTIAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 455
    :pswitch_27
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_TICKET_GROUP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 454
    :pswitch_28
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->TICKET_GROUP_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 453
    :pswitch_29
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->TICKET_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 452
    :pswitch_2a
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_COMP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 451
    :pswitch_2b
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_COMP_REASON:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 450
    :pswitch_2c
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_COMP_REASON:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 449
    :pswitch_2d
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_VOID_REASON:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 448
    :pswitch_2e
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_GIFT_CARD:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 447
    :pswitch_2f
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_DINING_OPTION:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 446
    :pswitch_30
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 445
    :pswitch_31
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_MODIFIER_SET:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 444
    :pswitch_32
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_MODIFIER_SET:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 443
    :pswitch_33
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_ITEM_VARIATION:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 442
    :pswitch_34
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CUSTOM_AMOUNT_ITEM_VARIATION:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 441
    :pswitch_35
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_EMPLOYEE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 440
    :pswitch_36
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_MOBILE_STAFF:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 439
    :pswitch_37
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_SUBUNIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 438
    :pswitch_38
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_MODIFIER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 437
    :pswitch_39
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_DISCOUNT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 436
    :pswitch_3a
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_CATEGORY:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 435
    :pswitch_3b
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_DEVICE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 434
    :pswitch_3c
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->SURCHARGE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 433
    :pswitch_3d
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->SWEDISH_ROUNDING:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 432
    :pswitch_3e
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CREDIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 431
    :pswitch_3f
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_ITEM_MODIFIER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 430
    :pswitch_40
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->INCLUSIVE_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 429
    :pswitch_41
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CASH_REFUND:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 428
    :pswitch_42
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->REFUND:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 427
    :pswitch_43
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CHANGE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 426
    :pswitch_44
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_TIP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 425
    :pswitch_45
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_SALES_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 424
    :pswitch_46
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_ITEM:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 423
    :pswitch_47
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_DISCOUNT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 422
    :pswitch_48
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->UNITEMIZED_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    .line 421
    :pswitch_49
    sget-object p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CUSTOM_AMOUNT_ITEM:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_49
        :pswitch_48
        :pswitch_47
        :pswitch_46
        :pswitch_45
        :pswitch_44
        :pswitch_43
        :pswitch_42
        :pswitch_41
        :pswitch_40
        :pswitch_3f
        :pswitch_3e
        :pswitch_3d
        :pswitch_3c
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/beemo/translation_types/TranslationType;
    .locals 1

    .line 14
    const-class v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/beemo/translation_types/TranslationType;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->$VALUES:[Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v0}, [Lcom/squareup/protos/beemo/translation_types/TranslationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 501
    iget v0, p0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->value:I

    return v0
.end method
