.class public final Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NameOrTranslationType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;",
        "Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public name:Ljava/lang/String;

.field public translation_type:Lcom/squareup/protos/beemo/translation_types/TranslationType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 101
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;
    .locals 4

    .line 119
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iget-object v1, p0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;->translation_type:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;-><init>(Ljava/lang/String;Lcom/squareup/protos/beemo/translation_types/TranslationType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;->build()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    move-result-object v0

    return-object v0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public translation_type(Lcom/squareup/protos/beemo/translation_types/TranslationType;)Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;->translation_type:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0
.end method
