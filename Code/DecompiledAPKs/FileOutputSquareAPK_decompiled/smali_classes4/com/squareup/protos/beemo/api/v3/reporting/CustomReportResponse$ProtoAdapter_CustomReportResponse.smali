.class final Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$ProtoAdapter_CustomReportResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CustomReportResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CustomReportResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 368
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 391
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;-><init>()V

    .line 392
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 393
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 400
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 398
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->sales_outside_range_detail:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 397
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->sales_inside_range_detail:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 396
    :cond_2
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->bill_event_token:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 395
    :cond_3
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->custom_report:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 404
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 405
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 366
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$ProtoAdapter_CustomReportResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 382
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 383
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->bill_event_token:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 384
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_inside_range_detail:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 385
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_outside_range_detail:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 386
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 366
    check-cast p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$ProtoAdapter_CustomReportResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)I
    .locals 4

    .line 373
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 374
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->bill_event_token:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 375
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_inside_range_detail:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 376
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_outside_range_detail:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 377
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 366
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$ProtoAdapter_CustomReportResponse;->encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;
    .locals 2

    .line 410
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;

    move-result-object p1

    .line 411
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->custom_report:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 412
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->sales_inside_range_detail:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 413
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->sales_outside_range_detail:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 414
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 415
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 366
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$ProtoAdapter_CustomReportResponse;->redact(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    move-result-object p1

    return-object p1
.end method
