.class public final Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReportingGroupConfigDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public reporting_group:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 96
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 97
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;->reporting_group:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;
    .locals 4

    .line 113
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;->reporting_group:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    move-result-object v0

    return-object v0
.end method

.method public group_by_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0
.end method

.method public reporting_group(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;"
        }
    .end annotation

    .line 106
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 107
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;->reporting_group:Ljava/util/List;

    return-object p0
.end method
