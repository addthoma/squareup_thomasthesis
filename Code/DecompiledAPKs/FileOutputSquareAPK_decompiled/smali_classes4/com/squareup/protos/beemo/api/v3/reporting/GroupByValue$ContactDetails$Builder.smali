.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact_token:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8308
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;
    .locals 3

    .line 8318
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails$Builder;->contact_token:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;-><init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 8305
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    move-result-object v0

    return-object v0
.end method

.method public contact_token(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails$Builder;
    .locals 0

    .line 8312
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails$Builder;->contact_token:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    return-object p0
.end method
