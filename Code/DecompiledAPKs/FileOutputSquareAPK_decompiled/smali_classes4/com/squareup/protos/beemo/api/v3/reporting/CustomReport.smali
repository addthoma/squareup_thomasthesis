.class public final Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;
.super Lcom/squareup/wire/Message;
.source "CustomReport.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$ProtoAdapter_CustomReport;,
        Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_GROUP_BY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field private static final serialVersionUID:J


# instance fields
.field public final aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.Aggregate#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByType#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.ReportingGroupValue#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final sub_report:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.CustomReport#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$ProtoAdapter_CustomReport;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$ProtoAdapter_CustomReport;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 25
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->NONE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->DEFAULT_GROUP_BY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;Ljava/util/List;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
            ">;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;",
            ")V"
        }
    .end annotation

    .line 72
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;Ljava/util/List;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;Ljava/util/List;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
            ">;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 78
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 79
    invoke-static {p4, p5}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p6

    const/4 v0, 0x1

    if-gt p6, v0, :cond_0

    .line 82
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 83
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    const-string p1, "sub_report"

    .line 84
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    .line 85
    iput-object p4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    .line 86
    iput-object p5, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    return-void

    .line 80
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of group_by_value, reporting_group_value may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 104
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 105
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 107
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    .line 108
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    .line 109
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    .line 110
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    .line 111
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 116
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 124
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;
    .locals 2

    .line 91
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;-><init>()V

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->sub_report:Ljava/util/List;

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    if-eqz v1, :cond_0

    const-string v1, ", group_by_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 133
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    if-eqz v1, :cond_1

    const-string v1, ", aggregate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 134
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", sub_report="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 135
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    if-eqz v1, :cond_3

    const-string v1, ", group_by_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 136
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    if-eqz v1, :cond_4

    const-string v1, ", reporting_group_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CustomReport{"

    .line 137
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
