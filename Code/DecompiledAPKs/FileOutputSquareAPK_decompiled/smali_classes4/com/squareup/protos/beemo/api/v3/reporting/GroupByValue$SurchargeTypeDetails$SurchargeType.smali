.class public final enum Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;
.super Ljava/lang/Enum;
.source "GroupByValue.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SurchargeType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType$ProtoAdapter_SurchargeType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AUTO_GRATUITY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

.field public static final enum CUSTOM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

.field public static final enum NO_SURCHARGE_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 8155
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->UNKNOWN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    .line 8157
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    const/4 v2, 0x1

    const-string v3, "AUTO_GRATUITY"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->AUTO_GRATUITY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    .line 8159
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    const/4 v3, 0x2

    const-string v4, "CUSTOM"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->CUSTOM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    .line 8161
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    const/4 v4, 0x3

    const-string v5, "NO_SURCHARGE_TYPE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->NO_SURCHARGE_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    .line 8154
    sget-object v5, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->UNKNOWN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->AUTO_GRATUITY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->CUSTOM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->NO_SURCHARGE_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    .line 8163
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType$ProtoAdapter_SurchargeType;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType$ProtoAdapter_SurchargeType;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 8167
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 8168
    iput p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 8179
    :cond_0
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->NO_SURCHARGE_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    return-object p0

    .line 8178
    :cond_1
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->CUSTOM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    return-object p0

    .line 8177
    :cond_2
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->AUTO_GRATUITY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    return-object p0

    .line 8176
    :cond_3
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->UNKNOWN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;
    .locals 1

    .line 8154
    const-class v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;
    .locals 1

    .line 8154
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    invoke-virtual {v0}, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 8186
    iget v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;->value:I

    return v0
.end method
