.class public final enum Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;
.super Ljava/lang/Enum;
.source "Filter.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FilterMode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode$ProtoAdapter_FilterMode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum EXCLUDE:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

.field public static final enum INCLUDE:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 221
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    const/4 v1, 0x0

    const-string v2, "INCLUDE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;->INCLUDE:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    .line 226
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    const/4 v2, 0x1

    const-string v3, "EXCLUDE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;->EXCLUDE:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    .line 217
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;->INCLUDE:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;->EXCLUDE:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    .line 228
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode$ProtoAdapter_FilterMode;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode$ProtoAdapter_FilterMode;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 232
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 233
    iput p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 242
    :cond_0
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;->EXCLUDE:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    return-object p0

    .line 241
    :cond_1
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;->INCLUDE:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;
    .locals 1

    .line 217
    const-class v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;
    .locals 1

    .line 217
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    invoke-virtual {v0}, [Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 249
    iget v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;->value:I

    return v0
.end method
