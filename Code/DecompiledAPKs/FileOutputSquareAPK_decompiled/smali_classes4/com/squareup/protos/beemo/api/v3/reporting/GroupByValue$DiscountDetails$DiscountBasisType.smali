.class public final enum Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;
.super Ljava/lang/Enum;
.source "GroupByValue.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DiscountBasisType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType$ProtoAdapter_DiscountBasisType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AMOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

.field public static final enum PERCENTAGE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 5658
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    const/4 v1, 0x0

    const-string v2, "AMOUNT"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;->AMOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    .line 5660
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    const/4 v2, 0x1

    const-string v3, "PERCENTAGE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;->PERCENTAGE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    .line 5657
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;->AMOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;->PERCENTAGE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    .line 5662
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType$ProtoAdapter_DiscountBasisType;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType$ProtoAdapter_DiscountBasisType;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 5666
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 5667
    iput p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 5676
    :cond_0
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;->PERCENTAGE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    return-object p0

    .line 5675
    :cond_1
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;->AMOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;
    .locals 1

    .line 5657
    const-class v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;
    .locals 1

    .line 5657
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    invoke-virtual {v0}, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 5683
    iget v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails$DiscountBasisType;->value:I

    return v0
.end method
