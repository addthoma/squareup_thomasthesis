.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6079
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;
    .locals 3

    .line 6092
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;-><init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 6076
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    move-result-object v0

    return-object v0
.end method

.method public name_or_translation_type(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup$Builder;
    .locals 0

    .line 6086
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    return-object p0
.end method
