.class public final enum Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;
.super Ljava/lang/Enum;
.source "GroupByValue.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Taxable"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable$ProtoAdapter_Taxable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum NON_TAXABLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

.field public static final enum TAXABLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 3723
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "TAXABLE"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;->TAXABLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    .line 3728
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    const/4 v3, 0x2

    const-string v4, "NON_TAXABLE"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;->NON_TAXABLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    new-array v0, v3, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    .line 3719
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;->TAXABLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;->NON_TAXABLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    .line 3730
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable$ProtoAdapter_Taxable;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable$ProtoAdapter_Taxable;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 3734
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3735
    iput p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 3744
    :cond_0
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;->NON_TAXABLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    return-object p0

    .line 3743
    :cond_1
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;->TAXABLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;
    .locals 1

    .line 3719
    const-class v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;
    .locals 1

    .line 3719
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    invoke-virtual {v0}, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 3751
    iget v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;->value:I

    return v0
.end method
