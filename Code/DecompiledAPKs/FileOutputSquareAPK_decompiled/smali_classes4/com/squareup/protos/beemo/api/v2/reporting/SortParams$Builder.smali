.class public final Lcom/squareup/protos/beemo/api/v2/reporting/SortParams$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SortParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v2/reporting/SortParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v2/reporting/SortParams;",
        "Lcom/squareup/protos/beemo/api/v2/reporting/SortParams$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public field:Lcom/squareup/protos/beemo/api/v2/reporting/SortField;

.field public order:Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v2/reporting/SortParams;
    .locals 4

    .line 112
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/reporting/SortParams;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/SortParams$Builder;->field:Lcom/squareup/protos/beemo/api/v2/reporting/SortField;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v2/reporting/SortParams$Builder;->order:Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/SortParams;-><init>(Lcom/squareup/protos/beemo/api/v2/reporting/SortField;Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v2/reporting/SortParams$Builder;->build()Lcom/squareup/protos/beemo/api/v2/reporting/SortParams;

    move-result-object v0

    return-object v0
.end method

.method public field(Lcom/squareup/protos/beemo/api/v2/reporting/SortField;)Lcom/squareup/protos/beemo/api/v2/reporting/SortParams$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/SortParams$Builder;->field:Lcom/squareup/protos/beemo/api/v2/reporting/SortField;

    return-object p0
.end method

.method public order(Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;)Lcom/squareup/protos/beemo/api/v2/reporting/SortParams$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/SortParams$Builder;->order:Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

    return-object p0
.end method
