.class public final Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RequestParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;",
        "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public transactions_time_range_from_bewfo_start_time_ms:Ljava/lang/Long;

.field public use_ledger_summary_results_in_settlement_report_response:Ljava/lang/Boolean;

.field public use_new_ledger_summary_endpoint:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 590
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;
    .locals 5

    .line 612
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->transactions_time_range_from_bewfo_start_time_ms:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->use_new_ledger_summary_endpoint:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->use_ledger_summary_results_in_settlement_report_response:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;-><init>(Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 583
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->build()Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    move-result-object v0

    return-object v0
.end method

.method public transactions_time_range_from_bewfo_start_time_ms(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;
    .locals 0

    .line 595
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->transactions_time_range_from_bewfo_start_time_ms:Ljava/lang/Long;

    return-object p0
.end method

.method public use_ledger_summary_results_in_settlement_report_response(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;
    .locals 0

    .line 606
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->use_ledger_summary_results_in_settlement_report_response:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_new_ledger_summary_endpoint(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;
    .locals 0

    .line 600
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->use_new_ledger_summary_endpoint:Ljava/lang/Boolean;

    return-object p0
.end method
