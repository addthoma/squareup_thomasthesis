.class public final Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FulfillmentRecipient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;",
        "Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public customer_id:Ljava/lang/String;

.field public display_name:Ljava/lang/String;

.field public email_address:Ljava/lang/String;

.field public phone_number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 130
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;
    .locals 7

    .line 155
    new-instance v6, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient$Builder;->customer_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient$Builder;->display_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient$Builder;->email_address:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient$Builder;->phone_number:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient$Builder;->build()Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    move-result-object v0

    return-object v0
.end method

.method public customer_id(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient$Builder;->customer_id:Ljava/lang/String;

    return-object p0
.end method

.method public display_name(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient$Builder;->display_name:Ljava/lang/String;

    return-object p0
.end method

.method public email_address(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient$Builder;->email_address:Ljava/lang/String;

    return-object p0
.end method

.method public phone_number(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient$Builder;->phone_number:Ljava/lang/String;

    return-object p0
.end method
