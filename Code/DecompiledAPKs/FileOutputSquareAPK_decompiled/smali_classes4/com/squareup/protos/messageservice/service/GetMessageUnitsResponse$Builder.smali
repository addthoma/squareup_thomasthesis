.class public final Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetMessageUnitsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse;",
        "Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public message_units:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/messageservice/service/MessageUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 82
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 83
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse$Builder;->message_units:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse;
    .locals 3

    .line 97
    new-instance v0, Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse$Builder;->message_units:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse$Builder;->build()Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse;

    move-result-object v0

    return-object v0
.end method

.method public message_units(Ljava/util/List;)Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/messageservice/service/MessageUnit;",
            ">;)",
            "Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse$Builder;"
        }
    .end annotation

    .line 90
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 91
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse$Builder;->message_units:Ljava/util/List;

    return-object p0
.end method
