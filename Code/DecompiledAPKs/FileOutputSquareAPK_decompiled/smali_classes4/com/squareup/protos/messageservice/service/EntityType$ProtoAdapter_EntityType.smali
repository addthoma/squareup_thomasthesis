.class final Lcom/squareup/protos/messageservice/service/EntityType$ProtoAdapter_EntityType;
.super Lcom/squareup/wire/EnumAdapter;
.source "EntityType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messageservice/service/EntityType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_EntityType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/messageservice/service/EntityType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 50
    const-class v0, Lcom/squareup/protos/messageservice/service/EntityType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/messageservice/service/EntityType;
    .locals 0

    .line 55
    invoke-static {p1}, Lcom/squareup/protos/messageservice/service/EntityType;->fromValue(I)Lcom/squareup/protos/messageservice/service/EntityType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 48
    invoke-virtual {p0, p1}, Lcom/squareup/protos/messageservice/service/EntityType$ProtoAdapter_EntityType;->fromValue(I)Lcom/squareup/protos/messageservice/service/EntityType;

    move-result-object p1

    return-object p1
.end method
