.class public final enum Lcom/squareup/protos/precog/SignalName;
.super Ljava/lang/Enum;
.source "SignalName.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/precog/SignalName$ProtoAdapter_SignalName;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/precog/SignalName;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/precog/SignalName;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/precog/SignalName;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum SIGNAL_BEST_AVAILABLE_PRODUCT_INTENT_PRIMARY:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_BEST_AVAILABLE_PRODUCT_INTENT_SECONDARY:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_ONBOARDING_ENTRY_POINT:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_PRODUCT_INTENT:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_PRODUCT_INTEREST:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_PRODUCT_INTEREST_APPOINTMENTS:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_PRODUCT_INTEREST_DIRECTORY:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_PRODUCT_INTEREST_EMPLOYEES:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_PRODUCT_INTEREST_E_COMMERCE:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_PRODUCT_INTEREST_GIFT_CARDS:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_PRODUCT_INTEREST_INVOICES:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_PRODUCT_INTEREST_IN_PERSON:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_PRODUCT_INTEREST_ITEMS:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_PRODUCT_INTEREST_LOYALTY:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_PRODUCT_INTEREST_POS:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_PRODUCT_INTEREST_RESTAURANTS:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_PRODUCT_INTEREST_RETAIL:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_PRODUCT_INTEREST_TERMINAL:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_PRODUCT_INTEREST_VIRTUAL_TERMINAL:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_UNKNOWN:Lcom/squareup/protos/precog/SignalName;

.field public static final enum SIGNAL_VARIANT:Lcom/squareup/protos/precog/SignalName;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 11
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const/4 v1, 0x0

    const-string v2, "SIGNAL_UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_UNKNOWN:Lcom/squareup/protos/precog/SignalName;

    .line 13
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const/4 v2, 0x1

    const-string v3, "SIGNAL_ONBOARDING_ENTRY_POINT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_ONBOARDING_ENTRY_POINT:Lcom/squareup/protos/precog/SignalName;

    .line 15
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const/4 v3, 0x2

    const-string v4, "SIGNAL_PRODUCT_INTENT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTENT:Lcom/squareup/protos/precog/SignalName;

    .line 17
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const/4 v4, 0x3

    const-string v5, "SIGNAL_BEST_AVAILABLE_PRODUCT_INTENT_PRIMARY"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_BEST_AVAILABLE_PRODUCT_INTENT_PRIMARY:Lcom/squareup/protos/precog/SignalName;

    .line 19
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const/4 v5, 0x4

    const-string v6, "SIGNAL_BEST_AVAILABLE_PRODUCT_INTENT_SECONDARY"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_BEST_AVAILABLE_PRODUCT_INTENT_SECONDARY:Lcom/squareup/protos/precog/SignalName;

    .line 24
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const/4 v6, 0x5

    const-string v7, "SIGNAL_PRODUCT_INTEREST"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST:Lcom/squareup/protos/precog/SignalName;

    .line 26
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const/4 v7, 0x6

    const-string v8, "SIGNAL_PRODUCT_INTEREST_APPOINTMENTS"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_APPOINTMENTS:Lcom/squareup/protos/precog/SignalName;

    .line 28
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const/4 v8, 0x7

    const-string v9, "SIGNAL_PRODUCT_INTEREST_E_COMMERCE"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_E_COMMERCE:Lcom/squareup/protos/precog/SignalName;

    .line 30
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const/16 v9, 0x8

    const-string v10, "SIGNAL_PRODUCT_INTEREST_INVOICES"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_INVOICES:Lcom/squareup/protos/precog/SignalName;

    .line 32
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const/16 v10, 0x9

    const-string v11, "SIGNAL_PRODUCT_INTEREST_POS"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_POS:Lcom/squareup/protos/precog/SignalName;

    .line 34
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const/16 v11, 0xa

    const-string v12, "SIGNAL_PRODUCT_INTEREST_RESTAURANTS"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_RESTAURANTS:Lcom/squareup/protos/precog/SignalName;

    .line 36
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const/16 v12, 0xb

    const-string v13, "SIGNAL_PRODUCT_INTEREST_RETAIL"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_RETAIL:Lcom/squareup/protos/precog/SignalName;

    .line 38
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const/16 v13, 0xc

    const-string v14, "SIGNAL_PRODUCT_INTEREST_TERMINAL"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_TERMINAL:Lcom/squareup/protos/precog/SignalName;

    .line 40
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const/16 v14, 0xd

    const-string v15, "SIGNAL_PRODUCT_INTEREST_VIRTUAL_TERMINAL"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_VIRTUAL_TERMINAL:Lcom/squareup/protos/precog/SignalName;

    .line 42
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const/16 v15, 0xe

    const-string v14, "SIGNAL_VARIANT"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_VARIANT:Lcom/squareup/protos/precog/SignalName;

    .line 44
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const-string v14, "SIGNAL_PRODUCT_INTEREST_EMPLOYEES"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_EMPLOYEES:Lcom/squareup/protos/precog/SignalName;

    .line 46
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const-string v13, "SIGNAL_PRODUCT_INTEREST_DIRECTORY"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_DIRECTORY:Lcom/squareup/protos/precog/SignalName;

    .line 48
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const-string v13, "SIGNAL_PRODUCT_INTEREST_GIFT_CARDS"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_GIFT_CARDS:Lcom/squareup/protos/precog/SignalName;

    .line 50
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const-string v13, "SIGNAL_PRODUCT_INTEREST_IN_PERSON"

    const/16 v14, 0x12

    const/16 v15, 0x12

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_IN_PERSON:Lcom/squareup/protos/precog/SignalName;

    .line 52
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const-string v13, "SIGNAL_PRODUCT_INTEREST_ITEMS"

    const/16 v14, 0x13

    const/16 v15, 0x13

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_ITEMS:Lcom/squareup/protos/precog/SignalName;

    .line 54
    new-instance v0, Lcom/squareup/protos/precog/SignalName;

    const-string v13, "SIGNAL_PRODUCT_INTEREST_LOYALTY"

    const/16 v14, 0x14

    const/16 v15, 0x14

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/SignalName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_LOYALTY:Lcom/squareup/protos/precog/SignalName;

    const/16 v0, 0x15

    new-array v0, v0, [Lcom/squareup/protos/precog/SignalName;

    .line 10
    sget-object v13, Lcom/squareup/protos/precog/SignalName;->SIGNAL_UNKNOWN:Lcom/squareup/protos/precog/SignalName;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_ONBOARDING_ENTRY_POINT:Lcom/squareup/protos/precog/SignalName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTENT:Lcom/squareup/protos/precog/SignalName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_BEST_AVAILABLE_PRODUCT_INTENT_PRIMARY:Lcom/squareup/protos/precog/SignalName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_BEST_AVAILABLE_PRODUCT_INTENT_SECONDARY:Lcom/squareup/protos/precog/SignalName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST:Lcom/squareup/protos/precog/SignalName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_APPOINTMENTS:Lcom/squareup/protos/precog/SignalName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_E_COMMERCE:Lcom/squareup/protos/precog/SignalName;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_INVOICES:Lcom/squareup/protos/precog/SignalName;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_POS:Lcom/squareup/protos/precog/SignalName;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_RESTAURANTS:Lcom/squareup/protos/precog/SignalName;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_RETAIL:Lcom/squareup/protos/precog/SignalName;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_TERMINAL:Lcom/squareup/protos/precog/SignalName;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_VIRTUAL_TERMINAL:Lcom/squareup/protos/precog/SignalName;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_VARIANT:Lcom/squareup/protos/precog/SignalName;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_EMPLOYEES:Lcom/squareup/protos/precog/SignalName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_DIRECTORY:Lcom/squareup/protos/precog/SignalName;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_GIFT_CARDS:Lcom/squareup/protos/precog/SignalName;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_IN_PERSON:Lcom/squareup/protos/precog/SignalName;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_ITEMS:Lcom/squareup/protos/precog/SignalName;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_LOYALTY:Lcom/squareup/protos/precog/SignalName;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->$VALUES:[Lcom/squareup/protos/precog/SignalName;

    .line 56
    new-instance v0, Lcom/squareup/protos/precog/SignalName$ProtoAdapter_SignalName;

    invoke-direct {v0}, Lcom/squareup/protos/precog/SignalName$ProtoAdapter_SignalName;-><init>()V

    sput-object v0, Lcom/squareup/protos/precog/SignalName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 61
    iput p3, p0, Lcom/squareup/protos/precog/SignalName;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/precog/SignalName;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 89
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_LOYALTY:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 88
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_ITEMS:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 87
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_IN_PERSON:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 86
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_GIFT_CARDS:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 85
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_DIRECTORY:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 84
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_EMPLOYEES:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 83
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_VARIANT:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 82
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_VIRTUAL_TERMINAL:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 81
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_TERMINAL:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 80
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_RETAIL:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 79
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_RESTAURANTS:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 78
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_POS:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 77
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_INVOICES:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 76
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_E_COMMERCE:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 75
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST_APPOINTMENTS:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 74
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTEREST:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 73
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_BEST_AVAILABLE_PRODUCT_INTENT_SECONDARY:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 72
    :pswitch_11
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_BEST_AVAILABLE_PRODUCT_INTENT_PRIMARY:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 71
    :pswitch_12
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_PRODUCT_INTENT:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 70
    :pswitch_13
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_ONBOARDING_ENTRY_POINT:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    .line 69
    :pswitch_14
    sget-object p0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_UNKNOWN:Lcom/squareup/protos/precog/SignalName;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/precog/SignalName;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/precog/SignalName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/precog/SignalName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/precog/SignalName;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/precog/SignalName;->$VALUES:[Lcom/squareup/protos/precog/SignalName;

    invoke-virtual {v0}, [Lcom/squareup/protos/precog/SignalName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/precog/SignalName;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 96
    iget v0, p0, Lcom/squareup/protos/precog/SignalName;->value:I

    return v0
.end method
