.class public final Lcom/squareup/protos/items/OnlineStoreDonationData$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "OnlineStoreDonationData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/items/OnlineStoreDonationData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/items/OnlineStoreDonationData;",
        "Lcom/squareup/protos/items/OnlineStoreDonationData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public target_amount_cents:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/items/OnlineStoreDonationData;
    .locals 3

    .line 90
    new-instance v0, Lcom/squareup/protos/items/OnlineStoreDonationData;

    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreDonationData$Builder;->target_amount_cents:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/items/OnlineStoreDonationData;-><init>(Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/items/OnlineStoreDonationData$Builder;->build()Lcom/squareup/protos/items/OnlineStoreDonationData;

    move-result-object v0

    return-object v0
.end method

.method public target_amount_cents(Ljava/lang/Integer;)Lcom/squareup/protos/items/OnlineStoreDonationData$Builder;
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/protos/items/OnlineStoreDonationData$Builder;->target_amount_cents:Ljava/lang/Integer;

    return-object p0
.end method
