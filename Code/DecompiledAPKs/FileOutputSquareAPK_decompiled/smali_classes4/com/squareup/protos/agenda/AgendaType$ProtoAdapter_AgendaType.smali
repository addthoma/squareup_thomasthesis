.class final Lcom/squareup/protos/agenda/AgendaType$ProtoAdapter_AgendaType;
.super Lcom/squareup/wire/EnumAdapter;
.source "AgendaType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/AgendaType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AgendaType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/agenda/AgendaType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 83
    const-class v0, Lcom/squareup/protos/agenda/AgendaType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/agenda/AgendaType;
    .locals 0

    .line 88
    invoke-static {p1}, Lcom/squareup/protos/agenda/AgendaType;->fromValue(I)Lcom/squareup/protos/agenda/AgendaType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 81
    invoke-virtual {p0, p1}, Lcom/squareup/protos/agenda/AgendaType$ProtoAdapter_AgendaType;->fromValue(I)Lcom/squareup/protos/agenda/AgendaType;

    move-result-object p1

    return-object p1
.end method
