.class public final Lcom/squareup/protos/agenda/Employment$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Employment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/Employment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/agenda/Employment;",
        "Lcom/squareup/protos/agenda/Employment$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public merchant_token:Ljava/lang/String;

.field public units:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/agenda/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 103
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 104
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/agenda/Employment$Builder;->units:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/agenda/Employment;
    .locals 4

    .line 126
    new-instance v0, Lcom/squareup/protos/agenda/Employment;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Employment$Builder;->merchant_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/agenda/Employment$Builder;->units:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/agenda/Employment;-><init>(Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/Employment$Builder;->build()Lcom/squareup/protos/agenda/Employment;

    move-result-object v0

    return-object v0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/agenda/Employment$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/agenda/Employment$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public units(Ljava/util/List;)Lcom/squareup/protos/agenda/Employment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/agenda/Unit;",
            ">;)",
            "Lcom/squareup/protos/agenda/Employment$Builder;"
        }
    .end annotation

    .line 119
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/agenda/Employment$Builder;->units:Ljava/util/List;

    return-object p0
.end method
