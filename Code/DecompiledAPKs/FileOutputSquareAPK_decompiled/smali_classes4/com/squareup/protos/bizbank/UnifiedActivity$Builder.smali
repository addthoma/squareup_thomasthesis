.class public final Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UnifiedActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/UnifiedActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/UnifiedActivity;",
        "Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public activity_state:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

.field public activity_token:Ljava/lang/String;

.field public activity_type:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

.field public amount:Lcom/squareup/protos/common/Money;

.field public balance:Lcom/squareup/protos/common/Money;

.field public card_last_four:Ljava/lang/String;

.field public decline_reason:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public latest_event_at:Lcom/squareup/protos/common/time/DateTime;

.field public merchant_image:Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

.field public transaction_token:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 287
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public activity_state(Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;)Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;
    .locals 0

    .line 318
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->activity_state:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    return-object p0
.end method

.method public activity_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;
    .locals 0

    .line 294
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->activity_token:Ljava/lang/String;

    return-object p0
.end method

.method public activity_type(Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;)Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->activity_type:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    return-object p0
.end method

.method public amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;
    .locals 0

    .line 334
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public balance(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;
    .locals 0

    .line 342
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->balance:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/bizbank/UnifiedActivity;
    .locals 15

    .line 388
    new-instance v14, Lcom/squareup/protos/bizbank/UnifiedActivity;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->activity_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->activity_type:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    iget-object v4, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->activity_state:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    iget-object v5, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->description:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->amount:Lcom/squareup/protos/common/Money;

    iget-object v7, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->balance:Lcom/squareup/protos/common/Money;

    iget-object v8, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->merchant_image:Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    iget-object v9, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->transaction_token:Ljava/lang/String;

    iget-object v10, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->unit_token:Ljava/lang/String;

    iget-object v11, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->decline_reason:Ljava/lang/String;

    iget-object v12, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->card_last_four:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v13

    move-object v0, v14

    invoke-direct/range {v0 .. v13}, Lcom/squareup/protos/bizbank/UnifiedActivity;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v14
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 262
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->build()Lcom/squareup/protos/bizbank/UnifiedActivity;

    move-result-object v0

    return-object v0
.end method

.method public card_last_four(Ljava/lang/String;)Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;
    .locals 0

    .line 382
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->card_last_four:Ljava/lang/String;

    return-object p0
.end method

.method public decline_reason(Ljava/lang/String;)Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;
    .locals 0

    .line 374
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->decline_reason:Ljava/lang/String;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;
    .locals 0

    .line 326
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public latest_event_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;
    .locals 0

    .line 302
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public merchant_image(Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;)Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;
    .locals 0

    .line 350
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->merchant_image:Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    return-object p0
.end method

.method public transaction_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;
    .locals 0

    .line 358
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->transaction_token:Ljava/lang/String;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;
    .locals 0

    .line 366
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
