.class public final Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;
.super Lcom/squareup/wire/Message;
.source "GetUnifiedActivitiesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Filters"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$ProtoAdapter_Filters;,
        Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final include_activity_types:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bizbank.UnifiedActivity$ActivityType#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;",
            ">;"
        }
    .end annotation
.end field

.field public final max_latest_event_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final min_latest_event_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 233
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$ProtoAdapter_Filters;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$ProtoAdapter_Filters;-><init>()V

    sput-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;",
            ">;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Lcom/squareup/protos/common/time/DateTime;",
            ")V"
        }
    .end annotation

    .line 267
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;-><init>(Ljava/util/List;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;",
            ">;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 272
    sget-object v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p4, "include_activity_types"

    .line 273
    invoke-static {p4, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->include_activity_types:Ljava/util/List;

    .line 274
    iput-object p2, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->min_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    .line 275
    iput-object p3, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->max_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 291
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 292
    :cond_1
    check-cast p1, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;

    .line 293
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->include_activity_types:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->include_activity_types:Ljava/util/List;

    .line 294
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->min_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->min_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    .line 295
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->max_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object p1, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->max_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    .line 296
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 301
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 303
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 304
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->include_activity_types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 305
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->min_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 306
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->max_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 307
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;
    .locals 2

    .line 280
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;-><init>()V

    .line 281
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->include_activity_types:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;->include_activity_types:Ljava/util/List;

    .line 282
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->min_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;->min_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    .line 283
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->max_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;->max_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    .line 284
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 232
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->newBuilder()Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 314
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 315
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->include_activity_types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", include_activity_types="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->include_activity_types:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 316
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->min_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    const-string v1, ", min_latest_event_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->min_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 317
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->max_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_2

    const-string v1, ", max_latest_event_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivitiesRequest$Filters;->max_latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Filters{"

    .line 318
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
