.class final Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$ProtoAdapter_MerchantImage;
.super Lcom/squareup/wire/ProtoAdapter;
.source "UnifiedActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_MerchantImage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 719
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 738
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;-><init>()V

    .line 739
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 740
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 745
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 743
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;->color_code(Ljava/lang/String;)Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;

    goto :goto_0

    .line 742
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;->merchant_image_url(Ljava/lang/String;)Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;

    goto :goto_0

    .line 749
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 750
    invoke-virtual {v0}, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;->build()Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 717
    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$ProtoAdapter_MerchantImage;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 731
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;->merchant_image_url:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 732
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;->color_code:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 733
    invoke-virtual {p2}, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 717
    check-cast p2, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$ProtoAdapter_MerchantImage;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;)I
    .locals 4

    .line 724
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;->merchant_image_url:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;->color_code:Ljava/lang/String;

    const/4 v3, 0x2

    .line 725
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 726
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 717
    check-cast p1, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$ProtoAdapter_MerchantImage;->encodedSize(Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;)Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;
    .locals 0

    .line 755
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;->newBuilder()Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;

    move-result-object p1

    .line 756
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 757
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;->build()Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 717
    check-cast p1, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$ProtoAdapter_MerchantImage;->redact(Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;)Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    move-result-object p1

    return-object p1
.end method
