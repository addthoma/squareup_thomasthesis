.class public final Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateUserAuthorizationResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;",
        "Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public result:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 81
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;
    .locals 3

    .line 91
    new-instance v0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Builder;->result:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;-><init>(Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Builder;->build()Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;

    move-result-object v0

    return-object v0
.end method

.method public result(Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;)Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Builder;
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Builder;->result:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    return-object p0
.end method
