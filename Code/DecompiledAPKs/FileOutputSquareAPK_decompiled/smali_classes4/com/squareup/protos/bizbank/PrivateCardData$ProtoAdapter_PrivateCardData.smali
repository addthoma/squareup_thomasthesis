.class final Lcom/squareup/protos/bizbank/PrivateCardData$ProtoAdapter_PrivateCardData;
.super Lcom/squareup/wire/ProtoAdapter;
.source "PrivateCardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/PrivateCardData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PrivateCardData"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/bizbank/PrivateCardData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 159
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/bizbank/PrivateCardData;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bizbank/PrivateCardData;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 180
    new-instance v0, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;-><init>()V

    .line 181
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 182
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 188
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 186
    :cond_0
    sget-object v3, Lcom/squareup/protos/common/time/YearMonth;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/YearMonth;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;->expiration(Lcom/squareup/protos/common/time/YearMonth;)Lcom/squareup/protos/bizbank/PrivateCardData$Builder;

    goto :goto_0

    .line 185
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;->cvc(Ljava/lang/String;)Lcom/squareup/protos/bizbank/PrivateCardData$Builder;

    goto :goto_0

    .line 184
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;->full_pan(Ljava/lang/String;)Lcom/squareup/protos/bizbank/PrivateCardData$Builder;

    goto :goto_0

    .line 192
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 193
    invoke-virtual {v0}, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;->build()Lcom/squareup/protos/bizbank/PrivateCardData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 157
    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/PrivateCardData$ProtoAdapter_PrivateCardData;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bizbank/PrivateCardData;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bizbank/PrivateCardData;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 172
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bizbank/PrivateCardData;->full_pan:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 173
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bizbank/PrivateCardData;->cvc:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 174
    sget-object v0, Lcom/squareup/protos/common/time/YearMonth;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bizbank/PrivateCardData;->expiration:Lcom/squareup/protos/common/time/YearMonth;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 175
    invoke-virtual {p2}, Lcom/squareup/protos/bizbank/PrivateCardData;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 157
    check-cast p2, Lcom/squareup/protos/bizbank/PrivateCardData;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/bizbank/PrivateCardData$ProtoAdapter_PrivateCardData;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bizbank/PrivateCardData;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/bizbank/PrivateCardData;)I
    .locals 4

    .line 164
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bizbank/PrivateCardData;->full_pan:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bizbank/PrivateCardData;->cvc:Ljava/lang/String;

    const/4 v3, 0x2

    .line 165
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/YearMonth;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bizbank/PrivateCardData;->expiration:Lcom/squareup/protos/common/time/YearMonth;

    const/4 v3, 0x3

    .line 166
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/PrivateCardData;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 157
    check-cast p1, Lcom/squareup/protos/bizbank/PrivateCardData;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/PrivateCardData$ProtoAdapter_PrivateCardData;->encodedSize(Lcom/squareup/protos/bizbank/PrivateCardData;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/bizbank/PrivateCardData;)Lcom/squareup/protos/bizbank/PrivateCardData;
    .locals 1

    .line 198
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/PrivateCardData;->newBuilder()Lcom/squareup/protos/bizbank/PrivateCardData$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 199
    iput-object v0, p1, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;->full_pan:Ljava/lang/String;

    .line 200
    iput-object v0, p1, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;->cvc:Ljava/lang/String;

    .line 201
    iput-object v0, p1, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;->expiration:Lcom/squareup/protos/common/time/YearMonth;

    .line 202
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 203
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/PrivateCardData$Builder;->build()Lcom/squareup/protos/bizbank/PrivateCardData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 157
    check-cast p1, Lcom/squareup/protos/bizbank/PrivateCardData;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/PrivateCardData$ProtoAdapter_PrivateCardData;->redact(Lcom/squareup/protos/bizbank/PrivateCardData;)Lcom/squareup/protos/bizbank/PrivateCardData;

    move-result-object p1

    return-object p1
.end method
