.class public final Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UnifiedActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;",
        "Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public color_code:Ljava/lang/String;

.field public merchant_image_url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 692
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;
    .locals 4

    .line 713
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;->merchant_image_url:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;->color_code:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 687
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;->build()Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    move-result-object v0

    return-object v0
.end method

.method public color_code(Ljava/lang/String;)Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;
    .locals 0

    .line 707
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;->color_code:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_image_url(Ljava/lang/String;)Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;
    .locals 0

    .line 699
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage$Builder;->merchant_image_url:Ljava/lang/String;

    return-object p0
.end method
