.class public final Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EventstreamV2Event.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/sawmill/EventstreamV2Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
        "Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public app_name:Ljava/lang/String;

.field public catalog_name:Ljava/lang/String;

.field public es2_debug_trace_id:Ljava/lang/String;

.field public json_data:Ljava/lang/String;

.field public recorded_at_usec:Ljava/lang/Long;

.field public secret_token:Ljava/lang/String;

.field public uuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 179
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public app_name(Ljava/lang/String;)Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->app_name:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/sawmill/EventstreamV2Event;
    .locals 10

    .line 225
    new-instance v9, Lcom/squareup/protos/sawmill/EventstreamV2Event;

    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->uuid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->catalog_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->app_name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->recorded_at_usec:Ljava/lang/Long;

    iget-object v5, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->json_data:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->secret_token:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->es2_debug_trace_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/sawmill/EventstreamV2Event;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 164
    invoke-virtual {p0}, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->build()Lcom/squareup/protos/sawmill/EventstreamV2Event;

    move-result-object v0

    return-object v0
.end method

.method public catalog_name(Ljava/lang/String;)Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->catalog_name:Ljava/lang/String;

    return-object p0
.end method

.method public es2_debug_trace_id(Ljava/lang/String;)Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->es2_debug_trace_id:Ljava/lang/String;

    return-object p0
.end method

.method public json_data(Ljava/lang/String;)Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;
    .locals 0

    .line 206
    iput-object p1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->json_data:Ljava/lang/String;

    return-object p0
.end method

.method public recorded_at_usec(Ljava/lang/Long;)Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->recorded_at_usec:Ljava/lang/Long;

    return-object p0
.end method

.method public secret_token(Ljava/lang/String;)Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->secret_token:Ljava/lang/String;

    return-object p0
.end method

.method public uuid(Ljava/lang/String;)Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->uuid:Ljava/lang/String;

    return-object p0
.end method
