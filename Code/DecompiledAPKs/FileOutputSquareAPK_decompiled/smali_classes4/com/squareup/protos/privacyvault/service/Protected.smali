.class public final Lcom/squareup/protos/privacyvault/service/Protected;
.super Lcom/squareup/wire/Message;
.source "Protected.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/privacyvault/service/Protected$ProtoAdapter_Protected;,
        Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;,
        Lcom/squareup/protos/privacyvault/service/Protected$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/privacyvault/service/Protected;",
        "Lcom/squareup/protos/privacyvault/service/Protected$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/privacyvault/service/Protected;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BINARY_DATA:Lokio/ByteString;

.field public static final DEFAULT_STRING_DATA:Ljava/lang/String; = ""

.field public static final DEFAULT_TRUNK_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final binary_data:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.privacyvault.service.Protected$ProtectedFideliusTokenWithCategory#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.privacyvault.service.MessageWithDescriptors#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final string_data:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final trunk_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/privacyvault/service/Protected$ProtoAdapter_Protected;

    invoke-direct {v0}, Lcom/squareup/protos/privacyvault/service/Protected$ProtoAdapter_Protected;-><init>()V

    sput-object v0, Lcom/squareup/protos/privacyvault/service/Protected;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 26
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/privacyvault/service/Protected;->DEFAULT_BINARY_DATA:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lokio/ByteString;Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;Ljava/lang/String;Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;)V
    .locals 7

    .line 82
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/privacyvault/service/Protected;-><init>(Ljava/lang/String;Lokio/ByteString;Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;Ljava/lang/String;Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lokio/ByteString;Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;Ljava/lang/String;Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;Lokio/ByteString;)V
    .locals 2

    .line 88
    sget-object v0, Lcom/squareup/protos/privacyvault/service/Protected;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const/4 p6, 0x1

    new-array v0, p6, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p5, v0, v1

    .line 89
    invoke-static {p1, p2, p3, p4, v0}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)I

    move-result v0

    if-gt v0, p6, :cond_0

    .line 92
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->string_data:Ljava/lang/String;

    .line 93
    iput-object p2, p0, Lcom/squareup/protos/privacyvault/service/Protected;->binary_data:Lokio/ByteString;

    .line 94
    iput-object p3, p0, Lcom/squareup/protos/privacyvault/service/Protected;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    .line 95
    iput-object p4, p0, Lcom/squareup/protos/privacyvault/service/Protected;->trunk_token:Ljava/lang/String;

    .line 96
    iput-object p5, p0, Lcom/squareup/protos/privacyvault/service/Protected;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    return-void

    .line 90
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of string_data, binary_data, fidelius_token, trunk_token, protected_message may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 114
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/privacyvault/service/Protected;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 115
    :cond_1
    check-cast p1, Lcom/squareup/protos/privacyvault/service/Protected;

    .line 116
    invoke-virtual {p0}, Lcom/squareup/protos/privacyvault/service/Protected;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/privacyvault/service/Protected;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->string_data:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/privacyvault/service/Protected;->string_data:Ljava/lang/String;

    .line 117
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->binary_data:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/privacyvault/service/Protected;->binary_data:Lokio/ByteString;

    .line 118
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    iget-object v3, p1, Lcom/squareup/protos/privacyvault/service/Protected;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    .line 119
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->trunk_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/privacyvault/service/Protected;->trunk_token:Ljava/lang/String;

    .line 120
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    iget-object p1, p1, Lcom/squareup/protos/privacyvault/service/Protected;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    .line 121
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 126
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 128
    invoke-virtual {p0}, Lcom/squareup/protos/privacyvault/service/Protected;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->string_data:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->binary_data:Lokio/ByteString;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->trunk_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 134
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/privacyvault/service/Protected$Builder;
    .locals 2

    .line 101
    new-instance v0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/privacyvault/service/Protected$Builder;-><init>()V

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->string_data:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->string_data:Ljava/lang/String;

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->binary_data:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->binary_data:Lokio/ByteString;

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    iput-object v1, v0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->trunk_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->trunk_token:Ljava/lang/String;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    iput-object v1, v0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/privacyvault/service/Protected;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/privacyvault/service/Protected;->newBuilder()Lcom/squareup/protos/privacyvault/service/Protected$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->string_data:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", string_data=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->binary_data:Lokio/ByteString;

    if-eqz v1, :cond_1

    const-string v1, ", binary_data=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    if-eqz v1, :cond_2

    const-string v1, ", fidelius_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 145
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->trunk_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", trunk_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->trunk_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    if-eqz v1, :cond_4

    const-string v1, ", protected_message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Protected{"

    .line 147
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
