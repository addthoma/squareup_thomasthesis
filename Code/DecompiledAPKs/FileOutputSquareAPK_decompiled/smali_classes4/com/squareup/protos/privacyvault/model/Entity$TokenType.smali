.class public final enum Lcom/squareup/protos/privacyvault/model/Entity$TokenType;
.super Ljava/lang/Enum;
.source "Entity.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/privacyvault/model/Entity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TokenType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/privacyvault/model/Entity$TokenType$ProtoAdapter_TokenType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/privacyvault/model/Entity$TokenType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/privacyvault/model/Entity$TokenType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum APPOINTMENTS_CUSTOMER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum BUYER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum CAPITAL_CUSTOMER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum CASH_CUSTOMER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum CAVIAR_COMPANY_CAPTAIN:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum CAVIAR_COMPANY_CLIENT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum CAVIAR_COMPANY_CLIENT_EMPLOYEE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum CAVIAR_COMPANY_CORPORATE_DINER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum CAVIAR_COMPANY_RESTAURANT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum CAVIAR_COMPANY_RESTAURANT_EMPLOYEE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum CAVIAR_COURIER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum CAVIAR_CUSTOMER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum CAVIAR_MERCHANT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum LOYALTY_CONTACT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum MERCHANT_CONTACT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum MERCHANT_EMPLOYEE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum MERCHANT_MASTER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum PAYMENT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum PERSON:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum SQUARE_EMPLOYEE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum SQUARE_JOB_CANDIDATE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

.field public static final enum WEEBLY_SITE_OWNER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 122
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->UNKNOWN:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 127
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/4 v2, 0x1

    const-string v3, "MERCHANT_MASTER"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->MERCHANT_MASTER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 132
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/4 v3, 0x2

    const-string v4, "MERCHANT_EMPLOYEE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->MERCHANT_EMPLOYEE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 137
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/4 v4, 0x3

    const-string v5, "MERCHANT_CONTACT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->MERCHANT_CONTACT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 142
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/4 v5, 0x4

    const-string v6, "LOYALTY_CONTACT"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->LOYALTY_CONTACT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 147
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/4 v6, 0x5

    const-string v7, "CASH_CUSTOMER"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CASH_CUSTOMER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 152
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/4 v7, 0x6

    const-string v8, "SQUARE_EMPLOYEE"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->SQUARE_EMPLOYEE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 157
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/16 v8, 0xf

    const/4 v9, 0x7

    const-string v10, "SQUARE_JOB_CANDIDATE"

    invoke-direct {v0, v10, v9, v8}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->SQUARE_JOB_CANDIDATE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 162
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/16 v10, 0xb

    const/16 v11, 0x8

    const-string v12, "PERSON"

    invoke-direct {v0, v12, v11, v10}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->PERSON:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 167
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/16 v12, 0xc

    const/16 v13, 0x9

    const-string v14, "PAYMENT"

    invoke-direct {v0, v14, v13, v12}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->PAYMENT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 174
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/16 v14, 0xa

    const-string v15, "CAPITAL_CUSTOMER"

    invoke-direct {v0, v15, v14, v9}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAPITAL_CUSTOMER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 179
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const-string v15, "APPOINTMENTS_CUSTOMER"

    invoke-direct {v0, v15, v10, v11}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->APPOINTMENTS_CUSTOMER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 184
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const-string v15, "CAVIAR_CUSTOMER"

    invoke-direct {v0, v15, v12, v13}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_CUSTOMER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 189
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/16 v15, 0xd

    const-string v12, "CAVIAR_MERCHANT"

    invoke-direct {v0, v12, v15, v14}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_MERCHANT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 194
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const-string v12, "CAVIAR_COURIER"

    const/16 v10, 0xe

    invoke-direct {v0, v12, v10, v15}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COURIER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 199
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const-string v10, "BUYER"

    const/16 v12, 0xe

    invoke-direct {v0, v10, v8, v12}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->BUYER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 204
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const-string v10, "WEEBLY_SITE_OWNER"

    const/16 v12, 0x10

    const/16 v8, 0x10

    invoke-direct {v0, v10, v12, v8}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->WEEBLY_SITE_OWNER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 209
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const-string v8, "CAVIAR_COMPANY_CAPTAIN"

    const/16 v10, 0x11

    const/16 v12, 0x11

    invoke-direct {v0, v8, v10, v12}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_CAPTAIN:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 214
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const-string v8, "CAVIAR_COMPANY_CLIENT"

    const/16 v10, 0x12

    const/16 v12, 0x12

    invoke-direct {v0, v8, v10, v12}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_CLIENT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 219
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const-string v8, "CAVIAR_COMPANY_CLIENT_EMPLOYEE"

    const/16 v10, 0x13

    const/16 v12, 0x13

    invoke-direct {v0, v8, v10, v12}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_CLIENT_EMPLOYEE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 224
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const-string v8, "CAVIAR_COMPANY_CORPORATE_DINER"

    const/16 v10, 0x14

    const/16 v12, 0x14

    invoke-direct {v0, v8, v10, v12}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_CORPORATE_DINER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 229
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const-string v8, "CAVIAR_COMPANY_RESTAURANT"

    const/16 v10, 0x15

    const/16 v12, 0x15

    invoke-direct {v0, v8, v10, v12}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_RESTAURANT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 234
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const-string v8, "CAVIAR_COMPANY_RESTAURANT_EMPLOYEE"

    const/16 v10, 0x16

    const/16 v12, 0x16

    invoke-direct {v0, v8, v10, v12}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_RESTAURANT_EMPLOYEE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/16 v0, 0x17

    new-array v0, v0, [Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 118
    sget-object v8, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->UNKNOWN:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->MERCHANT_MASTER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->MERCHANT_EMPLOYEE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->MERCHANT_CONTACT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->LOYALTY_CONTACT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CASH_CUSTOMER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->SQUARE_EMPLOYEE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->SQUARE_JOB_CANDIDATE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->PERSON:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->PAYMENT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAPITAL_CUSTOMER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    aput-object v1, v0, v14

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->APPOINTMENTS_CUSTOMER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_CUSTOMER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_MERCHANT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    aput-object v1, v0, v15

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COURIER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->BUYER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->WEEBLY_SITE_OWNER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_CAPTAIN:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_CLIENT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_CLIENT_EMPLOYEE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_CORPORATE_DINER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_RESTAURANT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_RESTAURANT_EMPLOYEE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->$VALUES:[Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    .line 236
    new-instance v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType$ProtoAdapter_TokenType;

    invoke-direct {v0}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType$ProtoAdapter_TokenType;-><init>()V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 240
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 241
    iput p3, p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/privacyvault/model/Entity$TokenType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 271
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_RESTAURANT_EMPLOYEE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 270
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_RESTAURANT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 269
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_CORPORATE_DINER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 268
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_CLIENT_EMPLOYEE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 267
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_CLIENT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 266
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COMPANY_CAPTAIN:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 265
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->WEEBLY_SITE_OWNER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 256
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->SQUARE_JOB_CANDIDATE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 264
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->BUYER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 263
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_COURIER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 258
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->PAYMENT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 257
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->PERSON:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 262
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_MERCHANT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 261
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAVIAR_CUSTOMER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 260
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->APPOINTMENTS_CUSTOMER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 259
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CAPITAL_CUSTOMER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 255
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->SQUARE_EMPLOYEE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 254
    :pswitch_11
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->CASH_CUSTOMER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 253
    :pswitch_12
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->LOYALTY_CONTACT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 252
    :pswitch_13
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->MERCHANT_CONTACT:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 251
    :pswitch_14
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->MERCHANT_EMPLOYEE:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 250
    :pswitch_15
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->MERCHANT_MASTER:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    .line 249
    :pswitch_16
    sget-object p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->UNKNOWN:Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/privacyvault/model/Entity$TokenType;
    .locals 1

    .line 118
    const-class v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/privacyvault/model/Entity$TokenType;
    .locals 1

    .line 118
    sget-object v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->$VALUES:[Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    invoke-virtual {v0}, [Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 278
    iget v0, p0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->value:I

    return v0
.end method
