.class public final enum Lcom/squareup/protos/inventory/InventoryAdjustmentType;
.super Ljava/lang/Enum;
.source "InventoryAdjustmentType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/inventory/InventoryAdjustmentType$ProtoAdapter_InventoryAdjustmentType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/inventory/InventoryAdjustmentType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/inventory/InventoryAdjustmentType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/inventory/InventoryAdjustmentType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum MANUAL_ADJUST:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

.field public static final enum RECEIVE_STOCK:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

.field public static final enum SALE:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

.field public static final enum SALES_ORDER_CANCEL:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

.field public static final enum SALES_ORDER_COMPLETE:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

.field public static final enum SALES_ORDER_CREATE:Lcom/squareup/protos/inventory/InventoryAdjustmentType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 18
    new-instance v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "SALE"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/inventory/InventoryAdjustmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->SALE:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    .line 24
    new-instance v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    const/4 v3, 0x3

    const-string v4, "RECEIVE_STOCK"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/inventory/InventoryAdjustmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->RECEIVE_STOCK:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    .line 30
    new-instance v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    const/4 v4, 0x2

    const/4 v5, 0x4

    const-string v6, "MANUAL_ADJUST"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/inventory/InventoryAdjustmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->MANUAL_ADJUST:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    .line 36
    new-instance v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    const/4 v6, 0x5

    const-string v7, "SALES_ORDER_CREATE"

    invoke-direct {v0, v7, v3, v6}, Lcom/squareup/protos/inventory/InventoryAdjustmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->SALES_ORDER_CREATE:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    .line 41
    new-instance v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    const/4 v7, 0x6

    const-string v8, "SALES_ORDER_CANCEL"

    invoke-direct {v0, v8, v5, v7}, Lcom/squareup/protos/inventory/InventoryAdjustmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->SALES_ORDER_CANCEL:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    .line 46
    new-instance v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    const-string v8, "SALES_ORDER_COMPLETE"

    const/4 v9, 0x7

    invoke-direct {v0, v8, v6, v9}, Lcom/squareup/protos/inventory/InventoryAdjustmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->SALES_ORDER_COMPLETE:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    new-array v0, v7, [Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    .line 13
    sget-object v7, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->SALE:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->RECEIVE_STOCK:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->MANUAL_ADJUST:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->SALES_ORDER_CREATE:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->SALES_ORDER_CANCEL:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->SALES_ORDER_COMPLETE:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->$VALUES:[Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    .line 48
    new-instance v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType$ProtoAdapter_InventoryAdjustmentType;

    invoke-direct {v0}, Lcom/squareup/protos/inventory/InventoryAdjustmentType$ProtoAdapter_InventoryAdjustmentType;-><init>()V

    sput-object v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 53
    iput p3, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/inventory/InventoryAdjustmentType;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_5

    const/4 v0, 0x3

    if-eq p0, v0, :cond_4

    const/4 v0, 0x4

    if-eq p0, v0, :cond_3

    const/4 v0, 0x5

    if-eq p0, v0, :cond_2

    const/4 v0, 0x6

    if-eq p0, v0, :cond_1

    const/4 v0, 0x7

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 66
    :cond_0
    sget-object p0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->SALES_ORDER_COMPLETE:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    return-object p0

    .line 65
    :cond_1
    sget-object p0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->SALES_ORDER_CANCEL:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    return-object p0

    .line 64
    :cond_2
    sget-object p0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->SALES_ORDER_CREATE:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    return-object p0

    .line 63
    :cond_3
    sget-object p0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->MANUAL_ADJUST:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    return-object p0

    .line 62
    :cond_4
    sget-object p0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->RECEIVE_STOCK:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    return-object p0

    .line 61
    :cond_5
    sget-object p0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->SALE:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/inventory/InventoryAdjustmentType;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/inventory/InventoryAdjustmentType;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->$VALUES:[Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    invoke-virtual {v0}, [Lcom/squareup/protos/inventory/InventoryAdjustmentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 73
    iget v0, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentType;->value:I

    return v0
.end method
