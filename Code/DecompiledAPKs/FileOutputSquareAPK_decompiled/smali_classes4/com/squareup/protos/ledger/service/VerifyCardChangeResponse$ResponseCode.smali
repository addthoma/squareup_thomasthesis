.class public final enum Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;
.super Ljava/lang/Enum;
.source "VerifyCardChangeResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ResponseCode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode$ProtoAdapter_ResponseCode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CODE_EXPIRED:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

.field public static final enum CODE_INVALID:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

.field public static final enum LINKED:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

.field public static final enum MERCHANT_TOKEN_MISMATCH:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

.field public static final enum UNKNOWN:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 135
    new-instance v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->UNKNOWN:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    .line 140
    new-instance v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    const/4 v2, 0x1

    const-string v3, "LINKED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->LINKED:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    .line 145
    new-instance v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    const/4 v3, 0x2

    const-string v4, "CODE_INVALID"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->CODE_INVALID:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    .line 150
    new-instance v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    const/4 v4, 0x3

    const-string v5, "CODE_EXPIRED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->CODE_EXPIRED:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    .line 155
    new-instance v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    const/4 v5, 0x4

    const-string v6, "MERCHANT_TOKEN_MISMATCH"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->MERCHANT_TOKEN_MISMATCH:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    .line 131
    sget-object v6, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->UNKNOWN:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->LINKED:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->CODE_INVALID:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->CODE_EXPIRED:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->MERCHANT_TOKEN_MISMATCH:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->$VALUES:[Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    .line 157
    new-instance v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode$ProtoAdapter_ResponseCode;

    invoke-direct {v0}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode$ProtoAdapter_ResponseCode;-><init>()V

    sput-object v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 161
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 162
    iput p3, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 174
    :cond_0
    sget-object p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->MERCHANT_TOKEN_MISMATCH:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    return-object p0

    .line 173
    :cond_1
    sget-object p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->CODE_EXPIRED:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    return-object p0

    .line 172
    :cond_2
    sget-object p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->CODE_INVALID:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    return-object p0

    .line 171
    :cond_3
    sget-object p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->LINKED:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    return-object p0

    .line 170
    :cond_4
    sget-object p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->UNKNOWN:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;
    .locals 1

    .line 131
    const-class v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;
    .locals 1

    .line 131
    sget-object v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->$VALUES:[Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    invoke-virtual {v0}, [Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 181
    iget v0, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->value:I

    return v0
.end method
