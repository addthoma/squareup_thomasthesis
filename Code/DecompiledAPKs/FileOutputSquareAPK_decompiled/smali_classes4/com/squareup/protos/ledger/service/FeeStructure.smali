.class public final Lcom/squareup/protos/ledger/service/FeeStructure;
.super Lcom/squareup/wire/Message;
.source "FeeStructure.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/ledger/service/FeeStructure$ProtoAdapter_FeeStructure;,
        Lcom/squareup/protos/ledger/service/FeeStructure$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/ledger/service/FeeStructure;",
        "Lcom/squareup/protos/ledger/service/FeeStructure$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/ledger/service/FeeStructure;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FEE_BASIS_POINTS:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final fee_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final fee_basis_points:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x1
    .end annotation
.end field

.field public final minimum_fee_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/ledger/service/FeeStructure$ProtoAdapter_FeeStructure;

    invoke-direct {v0}, Lcom/squareup/protos/ledger/service/FeeStructure$ProtoAdapter_FeeStructure;-><init>()V

    sput-object v0, Lcom/squareup/protos/ledger/service/FeeStructure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/ledger/service/FeeStructure;->DEFAULT_FEE_BASIS_POINTS:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 6

    .line 69
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/ledger/service/FeeStructure;-><init>(Ljava/lang/Integer;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/protos/ledger/service/FeeStructure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 75
    iput-object p1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    .line 76
    iput-object p2, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    .line 77
    iput-object p3, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    .line 78
    iput-object p4, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 95
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/ledger/service/FeeStructure;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 96
    :cond_1
    check-cast p1, Lcom/squareup/protos/ledger/service/FeeStructure;

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/ledger/service/FeeStructure;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/ledger/service/FeeStructure;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/ledger/service/FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    .line 98
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/ledger/service/FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    .line 99
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/ledger/service/FeeStructure;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    .line 100
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/ledger/service/FeeStructure;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    .line 101
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 106
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 108
    invoke-virtual {p0}, Lcom/squareup/protos/ledger/service/FeeStructure;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 113
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/ledger/service/FeeStructure$Builder;
    .locals 2

    .line 83
    new-instance v0, Lcom/squareup/protos/ledger/service/FeeStructure$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/ledger/service/FeeStructure$Builder;-><init>()V

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/ledger/service/FeeStructure$Builder;->fee_basis_points:Ljava/lang/Integer;

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/ledger/service/FeeStructure$Builder;->fee_amount:Lcom/squareup/protos/common/Money;

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/ledger/service/FeeStructure$Builder;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/ledger/service/FeeStructure$Builder;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/ledger/service/FeeStructure;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/ledger/service/FeeStructure$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/ledger/service/FeeStructure;->newBuilder()Lcom/squareup/protos/ledger/service/FeeStructure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", fee_basis_points="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->fee_basis_points:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 122
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", fee_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->fee_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 123
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", minimum_fee_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->minimum_fee_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 124
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", minimum_fee_deposit_limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/ledger/service/FeeStructure;->minimum_fee_deposit_limit:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FeeStructure{"

    .line 125
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
