.class public final Lcom/squareup/protos/jedi/service/PanelResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PanelResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/jedi/service/PanelResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/jedi/service/PanelResponse;",
        "Lcom/squareup/protos/jedi/service/PanelResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public code:Ljava/lang/Integer;

.field public error_message:Ljava/lang/String;

.field public panel:Lcom/squareup/protos/jedi/service/Panel;

.field public success:Ljava/lang/Boolean;

.field public transition_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 141
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/jedi/service/PanelResponse;
    .locals 8

    .line 171
    new-instance v7, Lcom/squareup/protos/jedi/service/PanelResponse;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->success:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->panel:Lcom/squareup/protos/jedi/service/Panel;

    iget-object v3, p0, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->error_message:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->code:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->transition_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/jedi/service/PanelResponse;-><init>(Ljava/lang/Boolean;Lcom/squareup/protos/jedi/service/Panel;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->build()Lcom/squareup/protos/jedi/service/PanelResponse;

    move-result-object v0

    return-object v0
.end method

.method public code(Ljava/lang/Integer;)Lcom/squareup/protos/jedi/service/PanelResponse$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->code:Ljava/lang/Integer;

    return-object p0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/PanelResponse$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public panel(Lcom/squareup/protos/jedi/service/Panel;)Lcom/squareup/protos/jedi/service/PanelResponse$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->panel:Lcom/squareup/protos/jedi/service/Panel;

    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/squareup/protos/jedi/service/PanelResponse$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method public transition_id(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/PanelResponse$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/PanelResponse$Builder;->transition_id:Ljava/lang/String;

    return-object p0
.end method
