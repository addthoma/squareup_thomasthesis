.class public final enum Lcom/squareup/protos/jedi/service/DeviceType;
.super Ljava/lang/Enum;
.source "DeviceType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/jedi/service/DeviceType$ProtoAdapter_DeviceType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/jedi/service/DeviceType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/jedi/service/DeviceType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/jedi/service/DeviceType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum PHONE:Lcom/squareup/protos/jedi/service/DeviceType;

.field public static final enum TABLET:Lcom/squareup/protos/jedi/service/DeviceType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 11
    new-instance v0, Lcom/squareup/protos/jedi/service/DeviceType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "PHONE"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/jedi/service/DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/DeviceType;->PHONE:Lcom/squareup/protos/jedi/service/DeviceType;

    .line 13
    new-instance v0, Lcom/squareup/protos/jedi/service/DeviceType;

    const/4 v3, 0x2

    const-string v4, "TABLET"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/jedi/service/DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/DeviceType;->TABLET:Lcom/squareup/protos/jedi/service/DeviceType;

    new-array v0, v3, [Lcom/squareup/protos/jedi/service/DeviceType;

    .line 10
    sget-object v3, Lcom/squareup/protos/jedi/service/DeviceType;->PHONE:Lcom/squareup/protos/jedi/service/DeviceType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/jedi/service/DeviceType;->TABLET:Lcom/squareup/protos/jedi/service/DeviceType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/jedi/service/DeviceType;->$VALUES:[Lcom/squareup/protos/jedi/service/DeviceType;

    .line 15
    new-instance v0, Lcom/squareup/protos/jedi/service/DeviceType$ProtoAdapter_DeviceType;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/DeviceType$ProtoAdapter_DeviceType;-><init>()V

    sput-object v0, Lcom/squareup/protos/jedi/service/DeviceType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput p3, p0, Lcom/squareup/protos/jedi/service/DeviceType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/jedi/service/DeviceType;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 29
    :cond_0
    sget-object p0, Lcom/squareup/protos/jedi/service/DeviceType;->TABLET:Lcom/squareup/protos/jedi/service/DeviceType;

    return-object p0

    .line 28
    :cond_1
    sget-object p0, Lcom/squareup/protos/jedi/service/DeviceType;->PHONE:Lcom/squareup/protos/jedi/service/DeviceType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/DeviceType;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/jedi/service/DeviceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/jedi/service/DeviceType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/jedi/service/DeviceType;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/jedi/service/DeviceType;->$VALUES:[Lcom/squareup/protos/jedi/service/DeviceType;

    invoke-virtual {v0}, [Lcom/squareup/protos/jedi/service/DeviceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/jedi/service/DeviceType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 36
    iget v0, p0, Lcom/squareup/protos/jedi/service/DeviceType;->value:I

    return v0
.end method
