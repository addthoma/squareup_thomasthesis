.class public final Lcom/squareup/protos/jedi/service/Panel$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Panel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/jedi/service/Panel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/jedi/service/Panel;",
        "Lcom/squareup/protos/jedi/service/Panel$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public components:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/jedi/service/Component;",
            ">;"
        }
    .end annotation
.end field

.field public display_search_bar:Ljava/lang/Boolean;

.field public panel_token:Ljava/lang/String;

.field public session_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 127
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 128
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/jedi/service/Panel$Builder;->components:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/jedi/service/Panel;
    .locals 7

    .line 154
    new-instance v6, Lcom/squareup/protos/jedi/service/Panel;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/Panel$Builder;->panel_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/jedi/service/Panel$Builder;->session_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/jedi/service/Panel$Builder;->components:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/jedi/service/Panel$Builder;->display_search_bar:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/jedi/service/Panel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/Panel$Builder;->build()Lcom/squareup/protos/jedi/service/Panel;

    move-result-object v0

    return-object v0
.end method

.method public components(Ljava/util/List;)Lcom/squareup/protos/jedi/service/Panel$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/jedi/service/Component;",
            ">;)",
            "Lcom/squareup/protos/jedi/service/Panel$Builder;"
        }
    .end annotation

    .line 142
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/Panel$Builder;->components:Ljava/util/List;

    return-object p0
.end method

.method public display_search_bar(Ljava/lang/Boolean;)Lcom/squareup/protos/jedi/service/Panel$Builder;
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/Panel$Builder;->display_search_bar:Ljava/lang/Boolean;

    return-object p0
.end method

.method public panel_token(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/Panel$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/Panel$Builder;->panel_token:Ljava/lang/String;

    return-object p0
.end method

.method public session_token(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/Panel$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/Panel$Builder;->session_token:Ljava/lang/String;

    return-object p0
.end method
