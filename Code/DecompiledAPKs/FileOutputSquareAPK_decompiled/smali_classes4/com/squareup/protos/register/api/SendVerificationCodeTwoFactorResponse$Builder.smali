.class public final Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SendVerificationCodeTwoFactorResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;",
        "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error_message:Ljava/lang/String;

.field public error_title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;
    .locals 4

    .line 116
    new-instance v0, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;

    iget-object v1, p0, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse$Builder;->error_title:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse$Builder;->error_message:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse$Builder;->build()Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;

    move-result-object v0

    return-object v0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse$Builder;
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public error_title(Ljava/lang/String;)Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse$Builder;->error_title:Ljava/lang/String;

    return-object p0
.end method
