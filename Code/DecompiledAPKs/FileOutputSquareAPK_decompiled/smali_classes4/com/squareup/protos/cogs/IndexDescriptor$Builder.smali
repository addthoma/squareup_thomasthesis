.class public final Lcom/squareup/protos/cogs/IndexDescriptor$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "IndexDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/cogs/IndexDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/cogs/IndexDescriptor;",
        "Lcom/squareup/protos/cogs/IndexDescriptor$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public key_field:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;

.field public unique:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 120
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 121
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/cogs/IndexDescriptor$Builder;->key_field:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/cogs/IndexDescriptor;
    .locals 5

    .line 150
    new-instance v0, Lcom/squareup/protos/cogs/IndexDescriptor;

    iget-object v1, p0, Lcom/squareup/protos/cogs/IndexDescriptor$Builder;->unique:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/cogs/IndexDescriptor$Builder;->key_field:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/cogs/IndexDescriptor$Builder;->name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/cogs/IndexDescriptor;-><init>(Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 113
    invoke-virtual {p0}, Lcom/squareup/protos/cogs/IndexDescriptor$Builder;->build()Lcom/squareup/protos/cogs/IndexDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public key_field(Ljava/util/List;)Lcom/squareup/protos/cogs/IndexDescriptor$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ">;)",
            "Lcom/squareup/protos/cogs/IndexDescriptor$Builder;"
        }
    .end annotation

    .line 133
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/cogs/IndexDescriptor$Builder;->key_field:Ljava/util/List;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/cogs/IndexDescriptor$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/cogs/IndexDescriptor$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public unique(Ljava/lang/Boolean;)Lcom/squareup/protos/cogs/IndexDescriptor$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/cogs/IndexDescriptor$Builder;->unique:Ljava/lang/Boolean;

    return-object p0
.end method
