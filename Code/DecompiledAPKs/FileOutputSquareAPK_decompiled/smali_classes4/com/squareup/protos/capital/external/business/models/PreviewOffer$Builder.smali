.class public final Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PreviewOffer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/external/business/models/PreviewOffer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/external/business/models/PreviewOffer;",
        "Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public max_financed_amount:Lcom/squareup/protos/common/Money;

.field public offer_selection_url:Ljava/lang/String;

.field public offer_set_id:Ljava/lang/String;

.field public product:Ljava/lang/String;

.field public rfc3986_offer_selection_url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 158
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/external/business/models/PreviewOffer;
    .locals 8

    .line 206
    new-instance v7, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->offer_set_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->product:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->offer_selection_url:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->max_financed_amount:Lcom/squareup/protos/common/Money;

    iget-object v5, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->rfc3986_offer_selection_url:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 147
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->build()Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    move-result-object v0

    return-object v0
.end method

.method public max_financed_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->max_financed_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public offer_selection_url(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->offer_selection_url:Ljava/lang/String;

    return-object p0
.end method

.method public offer_set_id(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->offer_set_id:Ljava/lang/String;

    return-object p0
.end method

.method public product(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->product:Ljava/lang/String;

    return-object p0
.end method

.method public rfc3986_offer_selection_url(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;
    .locals 0

    .line 200
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->rfc3986_offer_selection_url:Ljava/lang/String;

    return-object p0
.end method
