.class public final Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Payer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Payer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Payer;",
        "Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public business_name:Ljava/lang/String;

.field public can_debit_sq_balance_linked_account:Ljava/lang/Boolean;

.field public customer_id:Ljava/lang/String;

.field public instruments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;",
            ">;"
        }
    .end annotation
.end field

.field public is_primary:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 169
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 170
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->instruments:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/servicing/plan/models/Payer;
    .locals 8

    .line 219
    new-instance v7, Lcom/squareup/protos/capital/servicing/plan/models/Payer;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->customer_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->is_primary:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->can_debit_sq_balance_linked_account:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->business_name:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->instruments:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/capital/servicing/plan/models/Payer;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 158
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/Payer;

    move-result-object v0

    return-object v0
.end method

.method public business_name(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->business_name:Ljava/lang/String;

    return-object p0
.end method

.method public can_debit_sq_balance_linked_account(Ljava/lang/Boolean;)Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 195
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->can_debit_sq_balance_linked_account:Ljava/lang/Boolean;

    return-object p0
.end method

.method public customer_id(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->customer_id:Ljava/lang/String;

    return-object p0
.end method

.method public instruments(Ljava/util/List;)Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;",
            ">;)",
            "Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;"
        }
    .end annotation

    .line 212
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 213
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->instruments:Ljava/util/List;

    return-object p0
.end method

.method public is_primary(Ljava/lang/Boolean;)Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->is_primary:Ljava/lang/Boolean;

    return-object p0
.end method
