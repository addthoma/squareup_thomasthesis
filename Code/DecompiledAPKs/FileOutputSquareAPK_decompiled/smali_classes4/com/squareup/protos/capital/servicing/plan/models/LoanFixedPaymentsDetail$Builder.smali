.class public final Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LoanFixedPaymentsDetail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;",
        "Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public due_and_past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

.field public due_and_past_due_money:Lcom/squareup/protos/common/Money;

.field public due_money:Lcom/squareup/protos/common/Money;

.field public financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

.field public late_fee_excluding_pending_money:Lcom/squareup/protos/common/Money;

.field public late_fee_money:Lcom/squareup/protos/common/Money;

.field public paid_money:Lcom/squareup/protos/common/Money;

.field public past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

.field public past_due_money:Lcom/squareup/protos/common/Money;

.field public past_due_started_at:Ljava/lang/String;

.field public pending_money:Lcom/squareup/protos/common/Money;

.field public percentage_complete:Ljava/lang/String;

.field public remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

.field public remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

.field public remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

.field public remaining_money:Lcom/squareup/protos/common/Money;

.field public repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

.field public scheduled_payments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;",
            ">;"
        }
    .end annotation
.end field

.field public total_due_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 373
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 374
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->scheduled_payments:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;
    .locals 2

    .line 535
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;-><init>(Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 334
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    move-result-object v0

    return-object v0
.end method

.method public due_and_past_due_excluding_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0

    .line 496
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_and_past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public due_and_past_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0

    .line 479
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 398
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public financing_terms(Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0

    .line 381
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    return-object p0
.end method

.method public late_fee_excluding_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0

    .line 521
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->late_fee_excluding_pending_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public late_fee_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0

    .line 422
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->late_fee_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public paid_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0

    .line 430
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public past_due_excluding_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0

    .line 529
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public past_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0

    .line 414
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public past_due_started_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0

    .line 406
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_started_at:Ljava/lang/String;

    return-object p0
.end method

.method public pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0

    .line 446
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public percentage_complete(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0

    .line 454
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->percentage_complete:Ljava/lang/String;

    return-object p0
.end method

.method public remaining_excluding_past_due_and_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0

    .line 513
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public remaining_excluding_past_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0

    .line 487
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public remaining_excluding_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0

    .line 504
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public remaining_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0

    .line 438
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public repayment_terms(Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0

    .line 389
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    return-object p0
.end method

.method public scheduled_payments(Ljava/util/List;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;",
            ">;)",
            "Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;"
        }
    .end annotation

    .line 462
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 463
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->scheduled_payments:Ljava/util/List;

    return-object p0
.end method

.method public total_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 0

    .line 471
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->total_due_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
