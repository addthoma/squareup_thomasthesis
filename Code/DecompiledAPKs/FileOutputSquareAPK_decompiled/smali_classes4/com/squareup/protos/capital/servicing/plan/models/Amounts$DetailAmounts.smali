.class public final Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;
.super Lcom/squareup/wire/Message;
.source "Amounts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Amounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DetailAmounts"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$ProtoAdapter_DetailAmounts;,
        Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;",
        "Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final due_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final paid_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final past_due_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final penalty_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final remaining_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 313
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$ProtoAdapter_DetailAmounts;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$ProtoAdapter_DetailAmounts;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 7

    .line 359
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 364
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 365
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->paid_money:Lcom/squareup/protos/common/Money;

    .line 366
    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->penalty_money:Lcom/squareup/protos/common/Money;

    .line 367
    iput-object p3, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->past_due_money:Lcom/squareup/protos/common/Money;

    .line 368
    iput-object p4, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->due_money:Lcom/squareup/protos/common/Money;

    .line 369
    iput-object p5, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->remaining_money:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 387
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 388
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    .line 389
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->paid_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->paid_money:Lcom/squareup/protos/common/Money;

    .line 390
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->penalty_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->penalty_money:Lcom/squareup/protos/common/Money;

    .line 391
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->past_due_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->past_due_money:Lcom/squareup/protos/common/Money;

    .line 392
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->due_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->due_money:Lcom/squareup/protos/common/Money;

    .line 393
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->remaining_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->remaining_money:Lcom/squareup/protos/common/Money;

    .line 394
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 399
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 401
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 402
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->paid_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 403
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->penalty_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 404
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 405
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 406
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->remaining_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 407
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;
    .locals 2

    .line 374
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;-><init>()V

    .line 375
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->paid_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    .line 376
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->penalty_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->penalty_money:Lcom/squareup/protos/common/Money;

    .line 377
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->past_due_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    .line 378
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->due_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->due_money:Lcom/squareup/protos/common/Money;

    .line 379
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->remaining_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    .line 380
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 312
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 414
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 415
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->paid_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    const-string v1, ", paid_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->paid_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 416
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->penalty_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", penalty_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->penalty_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 417
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", past_due_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->past_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 418
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", due_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 419
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->remaining_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", remaining_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->remaining_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DetailAmounts{"

    .line 420
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
