.class public final Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LoanPortionOfSalesDetail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;",
        "Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public due_and_past_due_money:Lcom/squareup/protos/common/Money;

.field public due_money:Lcom/squareup/protos/common/Money;

.field public financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

.field public paid_money:Lcom/squareup/protos/common/Money;

.field public past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

.field public past_due_money:Lcom/squareup/protos/common/Money;

.field public past_due_started_at:Ljava/lang/String;

.field public payment_period_ended_at:Ljava/lang/String;

.field public pending_money:Lcom/squareup/protos/common/Money;

.field public percentage_complete:Ljava/lang/String;

.field public percentage_complete_including_pending:Ljava/lang/String;

.field public period_paid_money:Lcom/squareup/protos/common/Money;

.field public period_remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

.field public period_remaining_money:Lcom/squareup/protos/common/Money;

.field public period_required_money:Lcom/squareup/protos/common/Money;

.field public remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

.field public remaining_excluding_past_due_and_period_remaining_and_pending_money:Lcom/squareup/protos/common/Money;

.field public remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

.field public remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

.field public remaining_money:Lcom/squareup/protos/common/Money;

.field public repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

.field public total_due_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 412
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;
    .locals 2

    .line 588
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;-><init>(Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 367
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    move-result-object v0

    return-object v0
.end method

.method public due_and_past_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 522
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 435
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->due_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public financing_terms(Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 419
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    return-object p0
.end method

.method public paid_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 459
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public past_due_excluding_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 547
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public past_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 451
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public past_due_started_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 443
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_started_at:Ljava/lang/String;

    return-object p0
.end method

.method public payment_period_ended_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 483
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->payment_period_ended_at:Ljava/lang/String;

    return-object p0
.end method

.method public pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 475
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public percentage_complete(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 491
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->percentage_complete:Ljava/lang/String;

    return-object p0
.end method

.method public percentage_complete_including_pending(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 582
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->percentage_complete_including_pending:Ljava/lang/String;

    return-object p0
.end method

.method public period_paid_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 501
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_paid_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public period_remaining_excluding_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 539
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public period_remaining_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 506
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_remaining_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public period_required_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 496
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_required_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public remaining_excluding_past_due_and_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 573
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public remaining_excluding_past_due_and_period_remaining_and_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 564
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_and_period_remaining_and_pending_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public remaining_excluding_past_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 530
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public remaining_excluding_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 555
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public remaining_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 467
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public repayment_terms(Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 427
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    return-object p0
.end method

.method public total_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;
    .locals 0

    .line 514
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->total_due_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
