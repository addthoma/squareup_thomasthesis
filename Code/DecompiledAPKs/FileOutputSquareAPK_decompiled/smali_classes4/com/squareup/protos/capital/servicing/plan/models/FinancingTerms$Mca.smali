.class public final Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;
.super Lcom/squareup/wire/Message;
.source "FinancingTerms.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Mca"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$ProtoAdapter_Mca;,
        Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;",
        "Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FEE_AMOUNT_BPS:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final deposit_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final fee_amount_bps:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final fee_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final financed_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final outstanding_balance_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final total_owed_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 391
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$ProtoAdapter_Mca;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$ProtoAdapter_Mca;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;)V
    .locals 8

    .line 455
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 461
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 462
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->financed_money:Lcom/squareup/protos/common/Money;

    .line 463
    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->fee_money:Lcom/squareup/protos/common/Money;

    .line 464
    iput-object p3, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->deposit_money:Lcom/squareup/protos/common/Money;

    .line 465
    iput-object p4, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->outstanding_balance_money:Lcom/squareup/protos/common/Money;

    .line 466
    iput-object p5, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->total_owed_money:Lcom/squareup/protos/common/Money;

    .line 467
    iput-object p6, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->fee_amount_bps:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 486
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 487
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;

    .line 488
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->financed_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->financed_money:Lcom/squareup/protos/common/Money;

    .line 489
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->fee_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->fee_money:Lcom/squareup/protos/common/Money;

    .line 490
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->deposit_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->deposit_money:Lcom/squareup/protos/common/Money;

    .line 491
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->outstanding_balance_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->outstanding_balance_money:Lcom/squareup/protos/common/Money;

    .line 492
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->total_owed_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->total_owed_money:Lcom/squareup/protos/common/Money;

    .line 493
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->fee_amount_bps:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->fee_amount_bps:Ljava/lang/String;

    .line 494
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 499
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 501
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 502
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->financed_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 503
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 504
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->deposit_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 505
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->outstanding_balance_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 506
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->total_owed_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 507
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->fee_amount_bps:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 508
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;
    .locals 2

    .line 472
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;-><init>()V

    .line 473
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->financed_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->financed_money:Lcom/squareup/protos/common/Money;

    .line 474
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->fee_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->fee_money:Lcom/squareup/protos/common/Money;

    .line 475
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->deposit_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->deposit_money:Lcom/squareup/protos/common/Money;

    .line 476
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->outstanding_balance_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->outstanding_balance_money:Lcom/squareup/protos/common/Money;

    .line 477
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->total_owed_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->total_owed_money:Lcom/squareup/protos/common/Money;

    .line 478
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->fee_amount_bps:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->fee_amount_bps:Ljava/lang/String;

    .line 479
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 390
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 515
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 516
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->financed_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    const-string v1, ", financed_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->financed_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 517
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", fee_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->fee_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 518
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->deposit_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", deposit_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->deposit_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 519
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->outstanding_balance_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", outstanding_balance_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->outstanding_balance_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 520
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->total_owed_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", total_owed_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->total_owed_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 521
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->fee_amount_bps:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", fee_amount_bps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->fee_amount_bps:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Mca{"

    .line 522
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
