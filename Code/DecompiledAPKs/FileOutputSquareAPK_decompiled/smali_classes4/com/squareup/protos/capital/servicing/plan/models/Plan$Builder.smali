.class public final Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Plan.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Plan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Plan;",
        "Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public activated_at:Ljava/lang/String;

.field public allocated_at:Ljava/lang/String;

.field public amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

.field public cancelable_expired_at:Ljava/lang/String;

.field public closed_at:Ljava/lang/String;

.field public contract_id:Ljava/lang/String;

.field public created_at:Ljava/lang/String;

.field public default_status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

.field public documents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Document;",
            ">;"
        }
    .end annotation
.end field

.field public fund:Lcom/squareup/protos/capital/servicing/plan/models/Fund;

.field public id:Ljava/lang/String;

.field public investor:Lcom/squareup/protos/capital/servicing/plan/models/Investor;

.field public is_cancelable:Ljava/lang/Boolean;

.field public is_originated:Ljava/lang/Boolean;

.field public is_prepayable:Ljava/lang/Boolean;

.field public legacy_id:Ljava/lang/String;

.field public loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

.field public loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

.field public mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

.field public payers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Payer;",
            ">;"
        }
    .end annotation
.end field

.field public percentage_complete:Ljava/lang/String;

.field public product_name:Ljava/lang/String;

.field public retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

.field public status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

.field public support_resources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;",
            ">;"
        }
    .end annotation
.end field

.field public tags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Tag;",
            ">;"
        }
    .end annotation
.end field

.field public updated_at:Ljava/lang/String;

.field public version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 529
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 530
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->payers:Ljava/util/List;

    .line 531
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->tags:Ljava/util/List;

    .line 532
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->documents:Ljava/util/List;

    .line 533
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->support_resources:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public activated_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 660
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->activated_at:Ljava/lang/String;

    return-object p0
.end method

.method public allocated_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 694
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->allocated_at:Ljava/lang/String;

    return-object p0
.end method

.method public amounts(Lcom/squareup/protos/capital/servicing/plan/models/Amounts;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 711
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/capital/servicing/plan/models/Plan;
    .locals 2

    .line 754
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan;-><init>(Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 472
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/Plan;

    move-result-object v0

    return-object v0
.end method

.method public cancelable_expired_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 572
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->cancelable_expired_at:Ljava/lang/String;

    return-object p0
.end method

.method public closed_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 580
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->closed_at:Ljava/lang/String;

    return-object p0
.end method

.method public contract_id(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 596
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->contract_id:Ljava/lang/String;

    return-object p0
.end method

.method public created_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 550
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->created_at:Ljava/lang/String;

    return-object p0
.end method

.method public default_status(Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 626
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->default_status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    return-object p0
.end method

.method public documents(Ljava/util/List;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Document;",
            ">;)",
            "Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;"
        }
    .end annotation

    .line 642
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 643
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->documents:Ljava/util/List;

    return-object p0
.end method

.method public fund(Lcom/squareup/protos/capital/servicing/plan/models/Fund;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 612
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->fund:Lcom/squareup/protos/capital/servicing/plan/models/Fund;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 540
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public investor(Lcom/squareup/protos/capital/servicing/plan/models/Investor;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 604
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->investor:Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    return-object p0
.end method

.method public is_cancelable(Ljava/lang/Boolean;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 634
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->is_cancelable:Ljava/lang/Boolean;

    return-object p0
.end method

.method public is_originated(Ljava/lang/Boolean;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 686
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->is_originated:Ljava/lang/Boolean;

    return-object p0
.end method

.method public is_prepayable(Ljava/lang/Boolean;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 677
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->is_prepayable:Ljava/lang/Boolean;

    return-object p0
.end method

.method public legacy_id(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 669
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->legacy_id:Ljava/lang/String;

    return-object p0
.end method

.method public loan_fixed_payments(Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 721
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    const/4 p1, 0x0

    .line 722
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    .line 723
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    .line 724
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    return-object p0
.end method

.method public loan_portion_of_sales(Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 729
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    const/4 p1, 0x0

    .line 730
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    .line 731
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    .line 732
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    return-object p0
.end method

.method public mca(Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 737
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    const/4 p1, 0x0

    .line 738
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    .line 739
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    .line 740
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    return-object p0
.end method

.method public payers(Ljava/util/List;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Payer;",
            ">;)",
            "Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;"
        }
    .end annotation

    .line 563
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 564
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->payers:Ljava/util/List;

    return-object p0
.end method

.method public percentage_complete(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 716
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->percentage_complete:Ljava/lang/String;

    return-object p0
.end method

.method public product_name(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 706
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->product_name:Ljava/lang/String;

    return-object p0
.end method

.method public retail_installments(Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 745
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    const/4 p1, 0x0

    .line 746
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    .line 747
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    .line 748
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 588
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    return-object p0
.end method

.method public support_resources(Ljava/util/List;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;",
            ">;)",
            "Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;"
        }
    .end annotation

    .line 651
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 652
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->support_resources:Ljava/util/List;

    return-object p0
.end method

.method public tags(Ljava/util/List;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Tag;",
            ">;)",
            "Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;"
        }
    .end annotation

    .line 620
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 621
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->tags:Ljava/util/List;

    return-object p0
.end method

.method public updated_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 555
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->updated_at:Ljava/lang/String;

    return-object p0
.end method

.method public version(Ljava/lang/Integer;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 0

    .line 545
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->version:Ljava/lang/Integer;

    return-object p0
.end method
