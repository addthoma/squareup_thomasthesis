.class public final Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Amounts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;",
        "Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public due_money:Lcom/squareup/protos/common/Money;

.field public paid_money:Lcom/squareup/protos/common/Money;

.field public past_due_money:Lcom/squareup/protos/common/Money;

.field public penalty_money:Lcom/squareup/protos/common/Money;

.field public remaining_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 434
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;
    .locals 8

    .line 474
    new-instance v7, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->penalty_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->due_money:Lcom/squareup/protos/common/Money;

    iget-object v5, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 423
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    move-result-object v0

    return-object v0
.end method

.method public due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;
    .locals 0

    .line 463
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->due_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public paid_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;
    .locals 0

    .line 438
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public past_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;
    .locals 0

    .line 455
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public penalty_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;
    .locals 0

    .line 446
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->penalty_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public remaining_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;
    .locals 0

    .line 468
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
