.class final Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$ProtoAdapter_LoanFixedPaymentsDetail;
.super Lcom/squareup/wire/ProtoAdapter;
.source "LoanFixedPaymentsDetail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_LoanFixedPaymentsDetail"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 541
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 594
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;-><init>()V

    .line 595
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 596
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 618
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 616
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_and_past_due_excluding_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto :goto_0

    .line 615
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->late_fee_excluding_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto :goto_0

    .line 614
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_past_due_and_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto :goto_0

    .line 613
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto :goto_0

    .line 612
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_excluding_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto :goto_0

    .line 611
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_past_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto :goto_0

    .line 610
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_and_past_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto :goto_0

    .line 609
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->total_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto :goto_0

    .line 608
    :pswitch_8
    iget-object v3, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->scheduled_payments:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 607
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->percentage_complete(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto/16 :goto_0

    .line 606
    :pswitch_a
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto/16 :goto_0

    .line 605
    :pswitch_b
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto/16 :goto_0

    .line 604
    :pswitch_c
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->paid_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto/16 :goto_0

    .line 603
    :pswitch_d
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->late_fee_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto/16 :goto_0

    .line 602
    :pswitch_e
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto/16 :goto_0

    .line 601
    :pswitch_f
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_started_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto/16 :goto_0

    .line 600
    :pswitch_10
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto/16 :goto_0

    .line 599
    :pswitch_11
    sget-object v3, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->repayment_terms(Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto/16 :goto_0

    .line 598
    :pswitch_12
    sget-object v3, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->financing_terms(Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    goto/16 :goto_0

    .line 622
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 623
    invoke-virtual {v0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 539
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$ProtoAdapter_LoanFixedPaymentsDetail;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 570
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 571
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 572
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 573
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_started_at:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 574
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 575
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 576
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->paid_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 577
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 578
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->pending_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 579
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->percentage_complete:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 580
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->scheduled_payments:Ljava/util/List;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 581
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 582
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 583
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 584
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x13

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 585
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 586
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 587
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_excluding_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 588
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 589
    invoke-virtual {p2}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 539
    check-cast p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$ProtoAdapter_LoanFixedPaymentsDetail;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;)I
    .locals 4

    .line 546
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    const/4 v3, 0x2

    .line 547
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x3

    .line 548
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_started_at:Ljava/lang/String;

    const/4 v3, 0x4

    .line 549
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x5

    .line 550
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x6

    .line 551
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->paid_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x7

    .line 552
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x8

    .line 553
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->pending_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x9

    .line 554
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->percentage_complete:Ljava/lang/String;

    const/16 v3, 0xa

    .line 555
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 556
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->scheduled_payments:Ljava/util/List;

    const/16 v3, 0xb

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xc

    .line 557
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xd

    .line 558
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xe

    .line 559
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x13

    .line 560
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x10

    .line 561
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x11

    .line 562
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_excluding_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x12

    .line 563
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xf

    .line 564
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 565
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 539
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$ProtoAdapter_LoanFixedPaymentsDetail;->encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;
    .locals 2

    .line 628
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    move-result-object p1

    .line 629
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    .line 630
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    .line 631
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_money:Lcom/squareup/protos/common/Money;

    .line 632
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    .line 633
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->late_fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->late_fee_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->late_fee_money:Lcom/squareup/protos/common/Money;

    .line 634
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    .line 635
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    .line 636
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    .line 637
    :cond_7
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->scheduled_payments:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 638
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->total_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->total_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->total_due_money:Lcom/squareup/protos/common/Money;

    .line 639
    :cond_8
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    .line 640
    :cond_9
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    .line 641
    :cond_a
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_and_past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_and_past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_and_past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 642
    :cond_b
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 643
    :cond_c
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    .line 644
    :cond_d
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->late_fee_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->late_fee_excluding_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->late_fee_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 645
    :cond_e
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_f

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 646
    :cond_f
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 647
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 539
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$ProtoAdapter_LoanFixedPaymentsDetail;->redact(Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;)Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    move-result-object p1

    return-object p1
.end method
