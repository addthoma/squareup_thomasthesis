.class public final Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RetrievePlanResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse;",
        "Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field

.field public plan:Lcom/squareup/protos/capital/servicing/plan/models/Plan;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 98
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse$Builder;->errors:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse;
    .locals 4

    .line 117
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse$Builder;->plan:Lcom/squareup/protos/capital/servicing/plan/models/Plan;

    iget-object v2, p0, Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse$Builder;->errors:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse;-><init>(Lcom/squareup/protos/capital/servicing/plan/models/Plan;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse;

    move-result-object v0

    return-object v0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;)",
            "Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse$Builder;"
        }
    .end annotation

    .line 110
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse$Builder;->errors:Ljava/util/List;

    return-object p0
.end method

.method public plan(Lcom/squareup/protos/capital/servicing/plan/models/Plan;)Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanResponse$Builder;->plan:Lcom/squareup/protos/capital/servicing/plan/models/Plan;

    return-object p0
.end method
