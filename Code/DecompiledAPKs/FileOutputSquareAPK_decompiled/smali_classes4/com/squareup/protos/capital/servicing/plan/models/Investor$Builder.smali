.class public final Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Investor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Investor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Investor;",
        "Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public created_at:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public updated_at:Ljava/lang/String;

.field public version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 144
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/servicing/plan/models/Investor;
    .locals 8

    .line 177
    new-instance v7, Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;->version:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;->created_at:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;->updated_at:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;->name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/capital/servicing/plan/models/Investor;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 133
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;->created_at:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public updated_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;->updated_at:Ljava/lang/String;

    return-object p0
.end method

.method public version(Ljava/lang/Integer;)Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Investor$Builder;->version:Ljava/lang/Integer;

    return-object p0
.end method
