.class public final Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;
.super Lcom/squareup/wire/Message;
.source "ScheduledPayment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$ProtoAdapter_ScheduledPayment;,
        Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;,
        Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;",
        "Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DEBITED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_LATE_FEE_APPLIED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

.field private static final serialVersionUID:J


# instance fields
.field public final debit_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final debited_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final late_fee_applied_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final late_fee_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final remaining_due_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final remaining_late_fee_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final remaining_non_due_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final remaining_past_due_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.ScheduledPayment$Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$ProtoAdapter_ScheduledPayment;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$ProtoAdapter_ScheduledPayment;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->S_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->DEFAULT_STATUS:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 11

    .line 120
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;-><init>(Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 127
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->status:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    .line 129
    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debited_at:Ljava/lang/String;

    .line 130
    iput-object p3, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debit_money:Lcom/squareup/protos/common/Money;

    .line 131
    iput-object p4, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_applied_at:Ljava/lang/String;

    .line 132
    iput-object p5, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_money:Lcom/squareup/protos/common/Money;

    .line 133
    iput-object p6, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_non_due_money:Lcom/squareup/protos/common/Money;

    .line 134
    iput-object p7, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_due_money:Lcom/squareup/protos/common/Money;

    .line 135
    iput-object p8, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_past_due_money:Lcom/squareup/protos/common/Money;

    .line 136
    iput-object p9, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_late_fee_money:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 158
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 159
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;

    .line 160
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->status:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->status:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debited_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debited_at:Ljava/lang/String;

    .line 162
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debit_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debit_money:Lcom/squareup/protos/common/Money;

    .line 163
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_applied_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_applied_at:Ljava/lang/String;

    .line 164
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_money:Lcom/squareup/protos/common/Money;

    .line 165
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_non_due_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_non_due_money:Lcom/squareup/protos/common/Money;

    .line 166
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_due_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_due_money:Lcom/squareup/protos/common/Money;

    .line 167
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_past_due_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_past_due_money:Lcom/squareup/protos/common/Money;

    .line 168
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_late_fee_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_late_fee_money:Lcom/squareup/protos/common/Money;

    .line 169
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 174
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 176
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->status:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debited_at:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debit_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_applied_at:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 181
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 182
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_non_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 183
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 184
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 185
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_late_fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 186
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;
    .locals 2

    .line 141
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;-><init>()V

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->status:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->status:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debited_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->debited_at:Ljava/lang/String;

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debit_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->debit_money:Lcom/squareup/protos/common/Money;

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_applied_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->late_fee_applied_at:Ljava/lang/String;

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->late_fee_money:Lcom/squareup/protos/common/Money;

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_non_due_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_non_due_money:Lcom/squareup/protos/common/Money;

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_due_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_due_money:Lcom/squareup/protos/common/Money;

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_past_due_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_past_due_money:Lcom/squareup/protos/common/Money;

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_late_fee_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_late_fee_money:Lcom/squareup/protos/common/Money;

    .line 151
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->status:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->status:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 195
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debited_at:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", debited_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debited_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debit_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", debit_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->debit_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 197
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_applied_at:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", late_fee_applied_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_applied_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", late_fee_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->late_fee_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 199
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_non_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", remaining_non_due_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_non_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 200
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", remaining_due_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 201
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_7

    const-string v1, ", remaining_past_due_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_past_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 202
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_late_fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_8

    const-string v1, ", remaining_late_fee_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;->remaining_late_fee_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ScheduledPayment{"

    .line 203
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
