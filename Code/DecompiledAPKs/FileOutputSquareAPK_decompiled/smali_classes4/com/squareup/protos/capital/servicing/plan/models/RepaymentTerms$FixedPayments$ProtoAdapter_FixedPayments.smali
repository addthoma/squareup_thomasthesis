.class final Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$ProtoAdapter_FixedPayments;
.super Lcom/squareup/wire/ProtoAdapter;
.source "RepaymentTerms.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FixedPayments"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 363
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 386
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;-><init>()V

    .line 387
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 388
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 402
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 395
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->payment_frequency(Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;)Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 397
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 392
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->payment_count(Ljava/lang/Long;)Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;

    goto :goto_0

    .line 391
    :cond_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->final_payment_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;

    goto :goto_0

    .line 390
    :cond_3
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->payment_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;

    goto :goto_0

    .line 406
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 407
    invoke-virtual {v0}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 361
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$ProtoAdapter_FixedPayments;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 377
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 378
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->final_payment_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 379
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_count:Ljava/lang/Long;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 380
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_frequency:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 381
    invoke-virtual {p2}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 361
    check-cast p2, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$ProtoAdapter_FixedPayments;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;)I
    .locals 4

    .line 368
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->final_payment_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x2

    .line 369
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_count:Ljava/lang/Long;

    const/4 v3, 0x3

    .line 370
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->payment_frequency:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$PaymentFrequency;

    const/4 v3, 0x4

    .line 371
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 372
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 361
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$ProtoAdapter_FixedPayments;->encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;)Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;
    .locals 2

    .line 412
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;

    move-result-object p1

    .line 413
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->payment_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->payment_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->payment_money:Lcom/squareup/protos/common/Money;

    .line 414
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->final_payment_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->final_payment_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->final_payment_money:Lcom/squareup/protos/common/Money;

    .line 415
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 416
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 361
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments$ProtoAdapter_FixedPayments;->redact(Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;)Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    move-result-object p1

    return-object p1
.end method
