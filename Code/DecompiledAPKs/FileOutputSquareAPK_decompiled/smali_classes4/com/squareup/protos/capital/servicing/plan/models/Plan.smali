.class public final Lcom/squareup/protos/capital/servicing/plan/models/Plan;
.super Lcom/squareup/wire/Message;
.source "Plan.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/Plan$ProtoAdapter_Plan;,
        Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;,
        Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;,
        Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Plan;",
        "Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Plan;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACTIVATED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_ALLOCATED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_CANCELABLE_EXPIRED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_CLOSED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_CONTRACT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CREATED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_IS_CANCELABLE:Ljava/lang/Boolean;

.field public static final DEFAULT_IS_ORIGINATED:Ljava/lang/Boolean;

.field public static final DEFAULT_IS_PREPAYABLE:Ljava/lang/Boolean;

.field public static final DEFAULT_LEGACY_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE_COMPLETE:Ljava/lang/String; = ""

.field public static final DEFAULT_PRODUCT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

.field public static final DEFAULT_UPDATED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_VERSION:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final activated_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x11
    .end annotation
.end field

.field public final allocated_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x15
    .end annotation
.end field

.field public final amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.Amounts#ADAPTER"
        tag = 0x17
    .end annotation
.end field

.field public final cancelable_expired_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final closed_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final contract_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final created_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final default_status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.Plan$DefaultStatus#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final documents:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.Document#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xf
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Document;",
            ">;"
        }
    .end annotation
.end field

.field public final fund:Lcom/squareup/protos/capital/servicing/plan/models/Fund;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.Fund#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final investor:Lcom/squareup/protos/capital/servicing/plan/models/Investor;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.Investor#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final is_cancelable:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xe
    .end annotation
.end field

.field public final is_originated:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x14
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final is_prepayable:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x13
    .end annotation
.end field

.field public final legacy_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x12
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.LoanFixedPaymentsDetail#ADAPTER"
        tag = 0x96
    .end annotation
.end field

.field public final loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.LoanPortionOfSalesDetail#ADAPTER"
        tag = 0x97
    .end annotation
.end field

.field public final mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.McaDetail#ADAPTER"
        tag = 0x98
    .end annotation
.end field

.field public final payers:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.Payer#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Payer;",
            ">;"
        }
    .end annotation
.end field

.field public final percentage_complete:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x18
    .end annotation
.end field

.field public final product_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x16
    .end annotation
.end field

.field public final retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.RisaDetail#ADAPTER"
        tag = 0x99
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.Plan$Status#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final support_resources:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.SupportResource#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x10
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;",
            ">;"
        }
    .end annotation
.end field

.field public final tags:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.Tag#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xc
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Tag;",
            ">;"
        }
    .end annotation
.end field

.field public final updated_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 26
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$ProtoAdapter_Plan;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$ProtoAdapter_Plan;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 32
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->DEFAULT_VERSION:Ljava/lang/Integer;

    .line 42
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->S_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->DEFAULT_STATUS:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    .line 46
    sput-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->DEFAULT_IS_CANCELABLE:Ljava/lang/Boolean;

    .line 52
    sput-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->DEFAULT_IS_PREPAYABLE:Ljava/lang/Boolean;

    .line 54
    sput-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->DEFAULT_IS_ORIGINATED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;Lokio/ByteString;)V
    .locals 4

    .line 295
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 296
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p2, v0, v1, v2, v3}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)I

    move-result p2

    const/4 v0, 0x1

    if-gt p2, v0, :cond_0

    .line 299
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->id:Ljava/lang/String;

    .line 300
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->version:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->version:Ljava/lang/Integer;

    .line 301
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->created_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->created_at:Ljava/lang/String;

    .line 302
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->updated_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->updated_at:Ljava/lang/String;

    .line 303
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->payers:Ljava/util/List;

    const-string v0, "payers"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->payers:Ljava/util/List;

    .line 304
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->cancelable_expired_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->cancelable_expired_at:Ljava/lang/String;

    .line 305
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->closed_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->closed_at:Ljava/lang/String;

    .line 306
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    .line 307
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->contract_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->contract_id:Ljava/lang/String;

    .line 308
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->investor:Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->investor:Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    .line 309
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->fund:Lcom/squareup/protos/capital/servicing/plan/models/Fund;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->fund:Lcom/squareup/protos/capital/servicing/plan/models/Fund;

    .line 310
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->tags:Ljava/util/List;

    const-string v0, "tags"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->tags:Ljava/util/List;

    .line 311
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->default_status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->default_status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    .line 312
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->is_cancelable:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_cancelable:Ljava/lang/Boolean;

    .line 313
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->documents:Ljava/util/List;

    const-string v0, "documents"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->documents:Ljava/util/List;

    .line 314
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->support_resources:Ljava/util/List;

    const-string v0, "support_resources"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->support_resources:Ljava/util/List;

    .line 315
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->activated_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->activated_at:Ljava/lang/String;

    .line 316
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->legacy_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->legacy_id:Ljava/lang/String;

    .line 317
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->is_prepayable:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_prepayable:Ljava/lang/Boolean;

    .line 318
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->is_originated:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_originated:Ljava/lang/Boolean;

    .line 319
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->allocated_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->allocated_at:Ljava/lang/String;

    .line 320
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->product_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->product_name:Ljava/lang/String;

    .line 321
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    .line 322
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->percentage_complete:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->percentage_complete:Ljava/lang/String;

    .line 323
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    .line 324
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    .line 325
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    .line 326
    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    return-void

    .line 297
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of builder.loan_fixed_payments, builder.loan_portion_of_sales, builder.mca, builder.retail_installments may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 367
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 368
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;

    .line 369
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->id:Ljava/lang/String;

    .line 370
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->version:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->version:Ljava/lang/Integer;

    .line 371
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->created_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->created_at:Ljava/lang/String;

    .line 372
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->updated_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->updated_at:Ljava/lang/String;

    .line 373
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->payers:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->payers:Ljava/util/List;

    .line 374
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->cancelable_expired_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->cancelable_expired_at:Ljava/lang/String;

    .line 375
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->closed_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->closed_at:Ljava/lang/String;

    .line 376
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    .line 377
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->contract_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->contract_id:Ljava/lang/String;

    .line 378
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->investor:Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->investor:Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    .line 379
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->fund:Lcom/squareup/protos/capital/servicing/plan/models/Fund;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->fund:Lcom/squareup/protos/capital/servicing/plan/models/Fund;

    .line 380
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->tags:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->tags:Ljava/util/List;

    .line 381
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->default_status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->default_status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    .line 382
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_cancelable:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_cancelable:Ljava/lang/Boolean;

    .line 383
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->documents:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->documents:Ljava/util/List;

    .line 384
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->support_resources:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->support_resources:Ljava/util/List;

    .line 385
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->activated_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->activated_at:Ljava/lang/String;

    .line 386
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->legacy_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->legacy_id:Ljava/lang/String;

    .line 387
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_prepayable:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_prepayable:Ljava/lang/Boolean;

    .line 388
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_originated:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_originated:Ljava/lang/Boolean;

    .line 389
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->allocated_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->allocated_at:Ljava/lang/String;

    .line 390
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->product_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->product_name:Ljava/lang/String;

    .line 391
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    .line 392
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->percentage_complete:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->percentage_complete:Ljava/lang/String;

    .line 393
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    .line 394
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    .line 395
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    .line 396
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    .line 397
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 402
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_18

    .line 404
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 405
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 406
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->version:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 407
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->created_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 408
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->updated_at:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 409
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->payers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 410
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->cancelable_expired_at:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 411
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->closed_at:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 412
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 413
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->contract_id:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 414
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->investor:Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/Investor;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 415
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->fund:Lcom/squareup/protos/capital/servicing/plan/models/Fund;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/Fund;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 416
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->tags:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 417
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->default_status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 418
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_cancelable:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 419
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->documents:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 420
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->support_resources:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 421
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->activated_at:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 422
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->legacy_id:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 423
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_prepayable:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 424
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_originated:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 425
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->allocated_at:Ljava/lang/String;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 426
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->product_name:Ljava/lang/String;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 427
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 428
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->percentage_complete:Ljava/lang/String;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 429
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 430
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 431
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 432
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;->hashCode()I

    move-result v2

    :cond_17
    add-int/2addr v0, v2

    .line 433
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_18
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;
    .locals 2

    .line 331
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;-><init>()V

    .line 332
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->id:Ljava/lang/String;

    .line 333
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->version:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->version:Ljava/lang/Integer;

    .line 334
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->created_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->created_at:Ljava/lang/String;

    .line 335
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->updated_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->updated_at:Ljava/lang/String;

    .line 336
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->payers:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->payers:Ljava/util/List;

    .line 337
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->cancelable_expired_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->cancelable_expired_at:Ljava/lang/String;

    .line 338
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->closed_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->closed_at:Ljava/lang/String;

    .line 339
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    .line 340
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->contract_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->contract_id:Ljava/lang/String;

    .line 341
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->investor:Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->investor:Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    .line 342
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->fund:Lcom/squareup/protos/capital/servicing/plan/models/Fund;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->fund:Lcom/squareup/protos/capital/servicing/plan/models/Fund;

    .line 343
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->tags:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->tags:Ljava/util/List;

    .line 344
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->default_status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->default_status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    .line 345
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_cancelable:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->is_cancelable:Ljava/lang/Boolean;

    .line 346
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->documents:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->documents:Ljava/util/List;

    .line 347
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->support_resources:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->support_resources:Ljava/util/List;

    .line 348
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->activated_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->activated_at:Ljava/lang/String;

    .line 349
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->legacy_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->legacy_id:Ljava/lang/String;

    .line 350
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_prepayable:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->is_prepayable:Ljava/lang/Boolean;

    .line 351
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_originated:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->is_originated:Ljava/lang/Boolean;

    .line 352
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->allocated_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->allocated_at:Ljava/lang/String;

    .line 353
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->product_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->product_name:Ljava/lang/String;

    .line 354
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    .line 355
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->percentage_complete:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->percentage_complete:Ljava/lang/String;

    .line 356
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    .line 357
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    .line 358
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    .line 359
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    .line 360
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Plan$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 440
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 441
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 442
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->version:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 443
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->created_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->created_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 444
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->updated_at:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", updated_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->updated_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->payers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", payers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->payers:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 446
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->cancelable_expired_at:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", cancelable_expired_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->cancelable_expired_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 447
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->closed_at:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", closed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->closed_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    if-eqz v1, :cond_7

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 449
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->contract_id:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", contract_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->contract_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 450
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->investor:Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    if-eqz v1, :cond_9

    const-string v1, ", investor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->investor:Lcom/squareup/protos/capital/servicing/plan/models/Investor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 451
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->fund:Lcom/squareup/protos/capital/servicing/plan/models/Fund;

    if-eqz v1, :cond_a

    const-string v1, ", fund="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->fund:Lcom/squareup/protos/capital/servicing/plan/models/Fund;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 452
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->tags:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, ", tags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->tags:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 453
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->default_status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    if-eqz v1, :cond_c

    const-string v1, ", default_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->default_status:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 454
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_cancelable:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    const-string v1, ", is_cancelable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_cancelable:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 455
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->documents:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, ", documents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->documents:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 456
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->support_resources:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, ", support_resources="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->support_resources:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 457
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->activated_at:Ljava/lang/String;

    if-eqz v1, :cond_10

    const-string v1, ", activated_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->activated_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 458
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->legacy_id:Ljava/lang/String;

    if-eqz v1, :cond_11

    const-string v1, ", legacy_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->legacy_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 459
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_prepayable:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    const-string v1, ", is_prepayable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_prepayable:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 460
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_originated:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    const-string v1, ", is_originated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->is_originated:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 461
    :cond_13
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->allocated_at:Ljava/lang/String;

    if-eqz v1, :cond_14

    const-string v1, ", allocated_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->allocated_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 462
    :cond_14
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->product_name:Ljava/lang/String;

    if-eqz v1, :cond_15

    const-string v1, ", product_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->product_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 463
    :cond_15
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    if-eqz v1, :cond_16

    const-string v1, ", amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 464
    :cond_16
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->percentage_complete:Ljava/lang/String;

    if-eqz v1, :cond_17

    const-string v1, ", percentage_complete="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->percentage_complete:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 465
    :cond_17
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    if-eqz v1, :cond_18

    const-string v1, ", loan_fixed_payments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_fixed_payments:Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 466
    :cond_18
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    if-eqz v1, :cond_19

    const-string v1, ", loan_portion_of_sales="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->loan_portion_of_sales:Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 467
    :cond_19
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    if-eqz v1, :cond_1a

    const-string v1, ", mca="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->mca:Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 468
    :cond_1a
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    if-eqz v1, :cond_1b

    const-string v1, ", retail_installments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan;->retail_installments:Lcom/squareup/protos/capital/servicing/plan/models/RisaDetail;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1b
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Plan{"

    .line 469
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
