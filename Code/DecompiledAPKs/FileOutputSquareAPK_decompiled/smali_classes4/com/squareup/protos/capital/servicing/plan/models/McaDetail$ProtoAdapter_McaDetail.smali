.class final Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$ProtoAdapter_McaDetail;
.super Lcom/squareup/wire/ProtoAdapter;
.source "McaDetail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_McaDetail"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 202
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 227
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;-><init>()V

    .line 228
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 229
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 237
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 235
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->percentage_complete(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;

    goto :goto_0

    .line 234
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->remaining_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;

    goto :goto_0

    .line 233
    :cond_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->paid_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;

    goto :goto_0

    .line 232
    :cond_3
    sget-object v3, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->repayment_terms(Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;)Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;

    goto :goto_0

    .line 231
    :cond_4
    sget-object v3, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->financing_terms(Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;)Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;

    goto :goto_0

    .line 241
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 242
    invoke-virtual {v0}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 200
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$ProtoAdapter_McaDetail;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 217
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 218
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 219
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->paid_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 220
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 221
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->percentage_complete:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 222
    invoke-virtual {p2}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 200
    check-cast p2, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$ProtoAdapter_McaDetail;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;)I
    .locals 4

    .line 207
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    const/4 v3, 0x2

    .line 208
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->paid_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x3

    .line 209
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x4

    .line 210
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->percentage_complete:Ljava/lang/String;

    const/4 v3, 0x5

    .line 211
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 200
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$ProtoAdapter_McaDetail;->encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;)Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;
    .locals 2

    .line 247
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;

    move-result-object p1

    .line 248
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Mca;

    .line 249
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    .line 250
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    .line 251
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    .line 252
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 253
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 200
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/McaDetail$ProtoAdapter_McaDetail;->redact(Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;)Lcom/squareup/protos/capital/servicing/plan/models/McaDetail;

    move-result-object p1

    return-object p1
.end method
