.class public final Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardSummary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/simple_instrument_store/api/CardSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/simple_instrument_store/api/CardSummary;",
        "Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bin:Ljava/lang/String;

.field public brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public cardholder_name:Ljava/lang/String;

.field public expiration_date:Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;

.field public last_four:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 161
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bin(Ljava/lang/String;)Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;->bin:Ljava/lang/String;

    return-object p0
.end method

.method public brand(Lcom/squareup/protos/simple_instrument_store/model/Brand;)Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/simple_instrument_store/api/CardSummary;
    .locals 8

    .line 204
    new-instance v7, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;->last_four:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    iget-object v3, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;->cardholder_name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;->bin:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;->expiration_date:Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;-><init>(Ljava/lang/String;Lcom/squareup/protos/simple_instrument_store/model/Brand;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 150
    invoke-virtual {p0}, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;->build()Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    move-result-object v0

    return-object v0
.end method

.method public cardholder_name(Ljava/lang/String;)Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;->cardholder_name:Ljava/lang/String;

    return-object p0
.end method

.method public expiration_date(Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;)Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;->expiration_date:Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;

    return-object p0
.end method

.method public last_four(Ljava/lang/String;)Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;->last_four:Ljava/lang/String;

    return-object p0
.end method
