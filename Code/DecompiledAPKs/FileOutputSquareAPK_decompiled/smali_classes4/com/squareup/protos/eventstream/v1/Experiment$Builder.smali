.class public final Lcom/squareup/protos/eventstream/v1/Experiment$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Experiment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/eventstream/v1/Experiment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/eventstream/v1/Experiment;",
        "Lcom/squareup/protos/eventstream/v1/Experiment$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bucket:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 113
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bucket(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Experiment$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Experiment$Builder;->bucket:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/eventstream/v1/Experiment;
    .locals 5

    .line 133
    new-instance v0, Lcom/squareup/protos/eventstream/v1/Experiment;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Experiment$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/eventstream/v1/Experiment$Builder;->bucket:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/eventstream/v1/Experiment$Builder;->version:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/eventstream/v1/Experiment;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Experiment$Builder;->build()Lcom/squareup/protos/eventstream/v1/Experiment;

    move-result-object v0

    return-object v0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Experiment$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Experiment$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public version(Ljava/lang/Integer;)Lcom/squareup/protos/eventstream/v1/Experiment$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Experiment$Builder;->version:Ljava/lang/Integer;

    return-object p0
.end method
