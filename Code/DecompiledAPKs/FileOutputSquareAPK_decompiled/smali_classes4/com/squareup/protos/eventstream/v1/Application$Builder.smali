.class public final Lcom/squareup/protos/eventstream/v1/Application$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Application.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/eventstream/v1/Application;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/eventstream/v1/Application;",
        "Lcom/squareup/protos/eventstream/v1/Application$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public build_type:Ljava/lang/String;

.field public country_code:Lcom/squareup/protos/common/countries/Country;

.field public language_code:Lcom/squareup/protos/common/languages/Language;

.field public type:Ljava/lang/String;

.field public version:Lcom/squareup/protos/eventstream/v1/Application$Version;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 160
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/eventstream/v1/Application;
    .locals 8

    .line 204
    new-instance v7, Lcom/squareup/protos/eventstream/v1/Application;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Application$Builder;->type:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/eventstream/v1/Application$Builder;->version:Lcom/squareup/protos/eventstream/v1/Application$Version;

    iget-object v3, p0, Lcom/squareup/protos/eventstream/v1/Application$Builder;->build_type:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/eventstream/v1/Application$Builder;->language_code:Lcom/squareup/protos/common/languages/Language;

    iget-object v5, p0, Lcom/squareup/protos/eventstream/v1/Application$Builder;->country_code:Lcom/squareup/protos/common/countries/Country;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/eventstream/v1/Application;-><init>(Ljava/lang/String;Lcom/squareup/protos/eventstream/v1/Application$Version;Ljava/lang/String;Lcom/squareup/protos/common/languages/Language;Lcom/squareup/protos/common/countries/Country;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 149
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Application$Builder;->build()Lcom/squareup/protos/eventstream/v1/Application;

    move-result-object v0

    return-object v0
.end method

.method public build_type(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Application$Builder;
    .locals 0

    .line 180
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Application$Builder;->build_type:Ljava/lang/String;

    return-object p0
.end method

.method public country_code(Lcom/squareup/protos/common/countries/Country;)Lcom/squareup/protos/eventstream/v1/Application$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 198
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Application$Builder;->country_code:Lcom/squareup/protos/common/countries/Country;

    return-object p0
.end method

.method public language_code(Lcom/squareup/protos/common/languages/Language;)Lcom/squareup/protos/eventstream/v1/Application$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Application$Builder;->language_code:Lcom/squareup/protos/common/languages/Language;

    return-object p0
.end method

.method public type(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Application$Builder;
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Application$Builder;->type:Ljava/lang/String;

    return-object p0
.end method

.method public version(Lcom/squareup/protos/eventstream/v1/Application$Version;)Lcom/squareup/protos/eventstream/v1/Application$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Application$Builder;->version:Lcom/squareup/protos/eventstream/v1/Application$Version;

    return-object p0
.end method
