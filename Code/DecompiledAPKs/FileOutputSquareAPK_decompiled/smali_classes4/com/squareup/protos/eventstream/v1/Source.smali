.class public final Lcom/squareup/protos/eventstream/v1/Source;
.super Lcom/squareup/wire/Message;
.source "Source.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/eventstream/v1/Source$ProtoAdapter_Source;,
        Lcom/squareup/protos/eventstream/v1/Source$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/eventstream/v1/Source;",
        "Lcom/squareup/protos/eventstream/v1/Source$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/eventstream/v1/Source;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CITY:Ljava/lang/String; = ""

.field public static final DEFAULT_IP_ADDRESS:Ljava/lang/String; = ""

.field public static final DEFAULT_REGION:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Ljava/lang/String; = ""

.field public static final DEFAULT_USER_AGENT:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final application:Lcom/squareup/protos/eventstream/v1/Application;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.eventstream.v1.Application#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final city:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final coordinates:Lcom/squareup/protos/common/location/Coordinates;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.location.Coordinates#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final device:Lcom/squareup/protos/eventstream/v1/Device;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.eventstream.v1.Device#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final ip_address:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.eventstream.v1.OperatingSystem#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final reader:Lcom/squareup/protos/eventstream/v1/Reader;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.eventstream.v1.Reader#ADAPTER"
        tag = 0x5
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final region:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final type:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final user_agent:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/protos/eventstream/v1/Source$ProtoAdapter_Source;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/Source$ProtoAdapter_Source;-><init>()V

    sput-object v0, Lcom/squareup/protos/eventstream/v1/Source;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/eventstream/v1/Application;Lcom/squareup/protos/eventstream/v1/OperatingSystem;Lcom/squareup/protos/eventstream/v1/Device;Lcom/squareup/protos/eventstream/v1/Reader;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/Coordinates;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    .line 115
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/eventstream/v1/Source;-><init>(Ljava/lang/String;Lcom/squareup/protos/eventstream/v1/Application;Lcom/squareup/protos/eventstream/v1/OperatingSystem;Lcom/squareup/protos/eventstream/v1/Device;Lcom/squareup/protos/eventstream/v1/Reader;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/Coordinates;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/eventstream/v1/Application;Lcom/squareup/protos/eventstream/v1/OperatingSystem;Lcom/squareup/protos/eventstream/v1/Device;Lcom/squareup/protos/eventstream/v1/Reader;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/Coordinates;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 121
    sget-object v0, Lcom/squareup/protos/eventstream/v1/Source;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Source;->type:Ljava/lang/String;

    .line 123
    iput-object p2, p0, Lcom/squareup/protos/eventstream/v1/Source;->application:Lcom/squareup/protos/eventstream/v1/Application;

    .line 124
    iput-object p3, p0, Lcom/squareup/protos/eventstream/v1/Source;->os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    .line 125
    iput-object p4, p0, Lcom/squareup/protos/eventstream/v1/Source;->device:Lcom/squareup/protos/eventstream/v1/Device;

    .line 126
    iput-object p5, p0, Lcom/squareup/protos/eventstream/v1/Source;->reader:Lcom/squareup/protos/eventstream/v1/Reader;

    .line 127
    iput-object p6, p0, Lcom/squareup/protos/eventstream/v1/Source;->user_agent:Ljava/lang/String;

    .line 128
    iput-object p7, p0, Lcom/squareup/protos/eventstream/v1/Source;->ip_address:Ljava/lang/String;

    .line 129
    iput-object p8, p0, Lcom/squareup/protos/eventstream/v1/Source;->coordinates:Lcom/squareup/protos/common/location/Coordinates;

    .line 130
    iput-object p9, p0, Lcom/squareup/protos/eventstream/v1/Source;->city:Ljava/lang/String;

    .line 131
    iput-object p10, p0, Lcom/squareup/protos/eventstream/v1/Source;->region:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 154
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/eventstream/v1/Source;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 155
    :cond_1
    check-cast p1, Lcom/squareup/protos/eventstream/v1/Source;

    .line 156
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Source;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/eventstream/v1/Source;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->type:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Source;->type:Ljava/lang/String;

    .line 157
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->application:Lcom/squareup/protos/eventstream/v1/Application;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Source;->application:Lcom/squareup/protos/eventstream/v1/Application;

    .line 158
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Source;->os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->device:Lcom/squareup/protos/eventstream/v1/Device;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Source;->device:Lcom/squareup/protos/eventstream/v1/Device;

    .line 160
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->reader:Lcom/squareup/protos/eventstream/v1/Reader;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Source;->reader:Lcom/squareup/protos/eventstream/v1/Reader;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->user_agent:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Source;->user_agent:Ljava/lang/String;

    .line 162
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->ip_address:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Source;->ip_address:Ljava/lang/String;

    .line 163
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->coordinates:Lcom/squareup/protos/common/location/Coordinates;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Source;->coordinates:Lcom/squareup/protos/common/location/Coordinates;

    .line 164
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->city:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Source;->city:Ljava/lang/String;

    .line 165
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->region:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/eventstream/v1/Source;->region:Ljava/lang/String;

    .line 166
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 171
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 173
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Source;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->type:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->application:Lcom/squareup/protos/eventstream/v1/Application;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/eventstream/v1/Application;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/eventstream/v1/OperatingSystem;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->device:Lcom/squareup/protos/eventstream/v1/Device;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/eventstream/v1/Device;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->reader:Lcom/squareup/protos/eventstream/v1/Reader;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/eventstream/v1/Reader;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->user_agent:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->ip_address:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 181
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->coordinates:Lcom/squareup/protos/common/location/Coordinates;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/common/location/Coordinates;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 182
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->city:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 183
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->region:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 184
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/eventstream/v1/Source$Builder;
    .locals 2

    .line 136
    new-instance v0, Lcom/squareup/protos/eventstream/v1/Source$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/Source$Builder;-><init>()V

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->type:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->type:Ljava/lang/String;

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->application:Lcom/squareup/protos/eventstream/v1/Application;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->application:Lcom/squareup/protos/eventstream/v1/Application;

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->device:Lcom/squareup/protos/eventstream/v1/Device;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->device:Lcom/squareup/protos/eventstream/v1/Device;

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->reader:Lcom/squareup/protos/eventstream/v1/Reader;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->reader:Lcom/squareup/protos/eventstream/v1/Reader;

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->user_agent:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->user_agent:Ljava/lang/String;

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->ip_address:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->ip_address:Ljava/lang/String;

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->coordinates:Lcom/squareup/protos/common/location/Coordinates;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->coordinates:Lcom/squareup/protos/common/location/Coordinates;

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->city:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->city:Ljava/lang/String;

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->region:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Source$Builder;->region:Ljava/lang/String;

    .line 147
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Source;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Source;->newBuilder()Lcom/squareup/protos/eventstream/v1/Source$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->type:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->application:Lcom/squareup/protos/eventstream/v1/Application;

    if-eqz v1, :cond_1

    const-string v1, ", application="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->application:Lcom/squareup/protos/eventstream/v1/Application;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 194
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    if-eqz v1, :cond_2

    const-string v1, ", os="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 195
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->device:Lcom/squareup/protos/eventstream/v1/Device;

    if-eqz v1, :cond_3

    const-string v1, ", device="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->device:Lcom/squareup/protos/eventstream/v1/Device;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 196
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->reader:Lcom/squareup/protos/eventstream/v1/Reader;

    if-eqz v1, :cond_4

    const-string v1, ", reader="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->reader:Lcom/squareup/protos/eventstream/v1/Reader;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 197
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->user_agent:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", user_agent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->user_agent:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->ip_address:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", ip_address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->ip_address:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->coordinates:Lcom/squareup/protos/common/location/Coordinates;

    if-eqz v1, :cond_7

    const-string v1, ", coordinates="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->coordinates:Lcom/squareup/protos/common/location/Coordinates;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 200
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->city:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", city="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->city:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->region:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", region="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Source;->region:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Source{"

    .line 202
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
