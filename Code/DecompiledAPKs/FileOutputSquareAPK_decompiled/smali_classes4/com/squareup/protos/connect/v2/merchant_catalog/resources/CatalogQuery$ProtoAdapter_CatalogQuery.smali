.class final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ProtoAdapter_CatalogQuery;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CatalogQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CatalogQuery"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2887
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2926
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;-><init>()V

    .line 2927
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2928
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 2943
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2941
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->pricing_rules_for_products_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    goto :goto_0

    .line 2940
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->custom_attribute_usage(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    goto :goto_0

    .line 2939
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->filtered_items_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    goto :goto_0

    .line 2938
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->item_variations_for_item_option_values_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    goto :goto_0

    .line 2937
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_item_options_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    goto :goto_0

    .line 2936
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_modifier_list_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    goto :goto_0

    .line 2935
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_tax_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    goto :goto_0

    .line 2934
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->text_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    goto :goto_0

    .line 2933
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->range_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    goto :goto_0

    .line 2932
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->prefix_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    goto/16 :goto_0

    .line 2931
    :pswitch_a
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->exact_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    goto/16 :goto_0

    .line 2930
    :pswitch_b
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->sorted_attribute_query(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    goto/16 :goto_0

    .line 2947
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2948
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2885
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ProtoAdapter_CatalogQuery;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2909
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->sorted_attribute_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2910
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->exact_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2911
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->prefix_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2912
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->range_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2913
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->text_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2914
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_tax_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2915
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_modifier_list_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2916
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_item_options_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2917
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->item_variations_for_item_option_values_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2918
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->filtered_items_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2919
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->custom_attribute_usage:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2920
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->pricing_rules_for_products_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2921
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2885
    check-cast p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ProtoAdapter_CatalogQuery;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;)I
    .locals 4

    .line 2892
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->sorted_attribute_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->exact_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    const/4 v3, 0x2

    .line 2893
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->prefix_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

    const/4 v3, 0x3

    .line 2894
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->range_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    const/4 v3, 0x4

    .line 2895
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->text_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;

    const/4 v3, 0x5

    .line 2896
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_tax_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    const/4 v3, 0x6

    .line 2897
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_modifier_list_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    const/4 v3, 0x7

    .line 2898
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->items_for_item_options_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

    const/16 v3, 0x8

    .line 2899
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->item_variations_for_item_option_values_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

    const/16 v3, 0x9

    .line 2900
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->filtered_items_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

    const/16 v3, 0xa

    .line 2901
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->custom_attribute_usage:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    const/16 v3, 0xb

    .line 2902
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->pricing_rules_for_products_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

    const/16 v3, 0xc

    .line 2903
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2904
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2885
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ProtoAdapter_CatalogQuery;->encodedSize(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;
    .locals 2

    .line 2953
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;

    move-result-object p1

    .line 2954
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->sorted_attribute_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->sorted_attribute_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->sorted_attribute_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$SortedAttribute;

    .line 2955
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->exact_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->exact_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->exact_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Exact;

    .line 2956
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->prefix_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->prefix_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->prefix_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Prefix;

    .line 2957
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->range_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->range_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->range_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Range;

    .line 2958
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->text_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->text_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->text_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Text;

    .line 2959
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_tax_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_tax_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_tax_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    .line 2960
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_modifier_list_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_modifier_list_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_modifier_list_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    .line 2961
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_item_options_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_item_options_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->items_for_item_options_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForItemOptions;

    .line 2962
    :cond_7
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->item_variations_for_item_option_values_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->item_variations_for_item_option_values_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->item_variations_for_item_option_values_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemVariationsForItemOptionValues;

    .line 2963
    :cond_8
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->filtered_items_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->filtered_items_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->filtered_items_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;

    .line 2964
    :cond_9
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->custom_attribute_usage:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->custom_attribute_usage:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->custom_attribute_usage:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    .line 2965
    :cond_a
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->pricing_rules_for_products_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->pricing_rules_for_products_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->pricing_rules_for_products_query:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$PricingRulesForProducts;

    .line 2966
    :cond_b
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2967
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2885
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ProtoAdapter_CatalogQuery;->redact(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;

    move-result-object p1

    return-object p1
.end method
