.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public tax_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1292
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 1293
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax$Builder;->tax_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;
    .locals 3

    .line 1307
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax$Builder;->tax_ids:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1289
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax;

    move-result-object v0

    return-object v0
.end method

.method public tax_ids(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax$Builder;"
        }
    .end annotation

    .line 1300
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1301
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForTax$Builder;->tax_ids:Ljava/util/List;

    return-object p0
.end method
