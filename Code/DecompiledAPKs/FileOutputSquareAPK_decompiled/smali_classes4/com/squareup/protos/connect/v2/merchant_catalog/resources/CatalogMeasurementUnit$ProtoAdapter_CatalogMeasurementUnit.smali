.class final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$ProtoAdapter_CatalogMeasurementUnit;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CatalogMeasurementUnit.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CatalogMeasurementUnit"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 158
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 177
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;-><init>()V

    .line 178
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 179
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 184
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 182
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->precision(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    goto :goto_0

    .line 181
    :cond_1
    sget-object v3, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->measurement_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    goto :goto_0

    .line 188
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 189
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 156
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$ProtoAdapter_CatalogMeasurementUnit;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 170
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 171
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;->precision:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 172
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 156
    check-cast p2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$ProtoAdapter_CatalogMeasurementUnit;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;)I
    .locals 4

    .line 163
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;->precision:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 164
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 156
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$ProtoAdapter_CatalogMeasurementUnit;->encodedSize(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;
    .locals 2

    .line 194
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;

    move-result-object p1

    .line 195
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 196
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 197
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 156
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit$ProtoAdapter_CatalogMeasurementUnit;->redact(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    move-result-object p1

    return-object p1
.end method
