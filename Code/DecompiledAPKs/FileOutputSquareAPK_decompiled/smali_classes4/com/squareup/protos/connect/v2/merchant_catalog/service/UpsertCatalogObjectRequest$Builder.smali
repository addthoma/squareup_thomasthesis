.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpsertCatalogObjectRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public idempotency_key:Ljava/lang/String;

.field public object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 117
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;
    .locals 4

    .line 155
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->idempotency_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;

    move-result-object v0

    return-object v0
.end method

.method public idempotency_key(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->idempotency_key:Ljava/lang/String;

    return-object p0
.end method

.method public object(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    return-object p0
.end method
