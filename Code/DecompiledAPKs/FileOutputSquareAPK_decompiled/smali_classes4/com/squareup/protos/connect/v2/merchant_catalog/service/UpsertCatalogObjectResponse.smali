.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;
.super Lcom/squareup/wire/Message;
.source "UpsertCatalogObjectResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$ProtoAdapter_UpsertCatalogObjectResponse;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final catalog_object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogObject#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final errors:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Error#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field

.field public final id_mappings:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogIdMapping#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$ProtoAdapter_UpsertCatalogObjectResponse;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$ProtoAdapter_UpsertCatalogObjectResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;",
            ">;)V"
        }
    .end annotation

    .line 59
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 64
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p4, "errors"

    .line 65
    invoke-static {p4, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->errors:Ljava/util/List;

    .line 66
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->catalog_object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    const-string p1, "id_mappings"

    .line 67
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->id_mappings:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 83
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 84
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->errors:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->errors:Ljava/util/List;

    .line 86
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->catalog_object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->catalog_object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    .line 87
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->id_mappings:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->id_mappings:Ljava/util/List;

    .line 88
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 93
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->catalog_object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->id_mappings:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;
    .locals 2

    .line 72
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;-><init>()V

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->errors:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;->errors:Ljava/util/List;

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->catalog_object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;->catalog_object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->id_mappings:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;->id_mappings:Ljava/util/List;

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", errors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->errors:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 108
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->catalog_object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    if-eqz v1, :cond_1

    const-string v1, ", catalog_object="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->catalog_object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 109
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->id_mappings:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", id_mappings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->id_mappings:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "UpsertCatalogObjectResponse{"

    .line 110
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
