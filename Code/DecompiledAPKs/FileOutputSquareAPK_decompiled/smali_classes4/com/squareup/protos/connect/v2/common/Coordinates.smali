.class public final Lcom/squareup/protos/connect/v2/common/Coordinates;
.super Lcom/squareup/wire/Message;
.source "Coordinates.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/common/Coordinates$ProtoAdapter_Coordinates;,
        Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/common/Coordinates;",
        "Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/common/Coordinates;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LATITUDE:Ljava/lang/Double;

.field public static final DEFAULT_LONGITUDE:Ljava/lang/Double;

.field private static final serialVersionUID:J


# instance fields
.field public final latitude:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#DOUBLE"
        tag = 0x1
    .end annotation
.end field

.field public final longitude:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#DOUBLE"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 27
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Coordinates$ProtoAdapter_Coordinates;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/Coordinates$ProtoAdapter_Coordinates;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Coordinates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 31
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Coordinates;->DEFAULT_LATITUDE:Ljava/lang/Double;

    .line 33
    sput-object v0, Lcom/squareup/protos/connect/v2/common/Coordinates;->DEFAULT_LONGITUDE:Ljava/lang/Double;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Double;Ljava/lang/Double;)V
    .locals 1

    .line 54
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/connect/v2/common/Coordinates;-><init>(Ljava/lang/Double;Ljava/lang/Double;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Double;Ljava/lang/Double;Lokio/ByteString;)V
    .locals 1

    .line 58
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Coordinates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 59
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/common/Coordinates;->latitude:Ljava/lang/Double;

    .line 60
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/common/Coordinates;->longitude:Ljava/lang/Double;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 75
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/common/Coordinates;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 76
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/common/Coordinates;

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/Coordinates;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/Coordinates;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Coordinates;->latitude:Ljava/lang/Double;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/common/Coordinates;->latitude:Ljava/lang/Double;

    .line 78
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Coordinates;->longitude:Ljava/lang/Double;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/common/Coordinates;->longitude:Ljava/lang/Double;

    .line 79
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 84
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/Coordinates;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Coordinates;->latitude:Ljava/lang/Double;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Double;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Coordinates;->longitude:Ljava/lang/Double;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Double;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 89
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;
    .locals 2

    .line 65
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;-><init>()V

    .line 66
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Coordinates;->latitude:Ljava/lang/Double;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;->latitude:Ljava/lang/Double;

    .line 67
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Coordinates;->longitude:Ljava/lang/Double;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;->longitude:Ljava/lang/Double;

    .line 68
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/Coordinates;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/Coordinates;->newBuilder()Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Coordinates;->latitude:Ljava/lang/Double;

    if-eqz v1, :cond_0

    const-string v1, ", latitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Coordinates;->latitude:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 98
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Coordinates;->longitude:Ljava/lang/Double;

    if-eqz v1, :cond_1

    const-string v1, ", longitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Coordinates;->longitude:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Coordinates{"

    .line 99
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
