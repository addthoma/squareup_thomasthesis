.class public final Lcom/squareup/protos/connect/v2/resources/Refund$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Refund.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Refund;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/resources/Refund;",
        "Lcom/squareup/protos/connect/v2/resources/Refund$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public additional_recipients:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;",
            ">;"
        }
    .end annotation
.end field

.field public amount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public created_at:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public location_id:Ljava/lang/String;

.field public processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public reason:Ljava/lang/String;

.field public status:Lcom/squareup/protos/connect/v2/resources/Refund$Status;

.field public tender_id:Ljava/lang/String;

.field public transaction_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 259
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 260
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->additional_recipients:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public additional_recipients(Ljava/util/List;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/resources/Refund$Builder;"
        }
    .end annotation

    .line 345
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 346
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->additional_recipients:Ljava/util/List;

    return-object p0
.end method

.method public amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;
    .locals 0

    .line 315
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/resources/Refund;
    .locals 13

    .line 352
    new-instance v12, Lcom/squareup/protos/connect/v2/resources/Refund;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->location_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->transaction_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->tender_id:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->created_at:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->reason:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v8, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->status:Lcom/squareup/protos/connect/v2/resources/Refund$Status;

    iget-object v9, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v10, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->additional_recipients:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/connect/v2/resources/Refund;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/resources/Refund$Status;Lcom/squareup/protos/connect/v2/common/Money;Ljava/util/List;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 238
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->build()Lcom/squareup/protos/connect/v2/resources/Refund;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;
    .locals 0

    .line 299
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->created_at:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;
    .locals 0

    .line 267
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public location_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;
    .locals 0

    .line 275
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->location_id:Ljava/lang/String;

    return-object p0
.end method

.method public processing_fee_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;
    .locals 0

    .line 334
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public reason(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;
    .locals 0

    .line 307
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->reason:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/connect/v2/resources/Refund$Status;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;
    .locals 0

    .line 326
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->status:Lcom/squareup/protos/connect/v2/resources/Refund$Status;

    return-object p0
.end method

.method public tender_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;
    .locals 0

    .line 291
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->tender_id:Ljava/lang/String;

    return-object p0
.end method

.method public transaction_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;
    .locals 0

    .line 283
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->transaction_id:Ljava/lang/String;

    return-object p0
.end method
