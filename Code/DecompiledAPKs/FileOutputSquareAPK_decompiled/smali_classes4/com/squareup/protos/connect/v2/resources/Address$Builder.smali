.class public final Lcom/squareup/protos/connect/v2/resources/Address$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Address.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Address;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/resources/Address;",
        "Lcom/squareup/protos/connect/v2/resources/Address$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public address_line_1:Ljava/lang/String;

.field public address_line_2:Ljava/lang/String;

.field public address_line_3:Ljava/lang/String;

.field public administrative_district_level_1:Ljava/lang/String;

.field public administrative_district_level_2:Ljava/lang/String;

.field public administrative_district_level_3:Ljava/lang/String;

.field public country:Lcom/squareup/protos/connect/v2/resources/Country;

.field public first_name:Ljava/lang/String;

.field public last_name:Ljava/lang/String;

.field public locality:Ljava/lang/String;

.field public organization:Ljava/lang/String;

.field public postal_code:Ljava/lang/String;

.field public sublocality:Ljava/lang/String;

.field public sublocality_2:Ljava/lang/String;

.field public sublocality_3:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 371
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public address_line_1(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Address$Builder;
    .locals 0

    .line 385
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->address_line_1:Ljava/lang/String;

    return-object p0
.end method

.method public address_line_2(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Address$Builder;
    .locals 0

    .line 393
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->address_line_2:Ljava/lang/String;

    return-object p0
.end method

.method public address_line_3(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Address$Builder;
    .locals 0

    .line 401
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->address_line_3:Ljava/lang/String;

    return-object p0
.end method

.method public administrative_district_level_1(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Address$Builder;
    .locals 0

    .line 453
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->administrative_district_level_1:Ljava/lang/String;

    return-object p0
.end method

.method public administrative_district_level_2(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Address$Builder;
    .locals 0

    .line 465
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->administrative_district_level_2:Ljava/lang/String;

    return-object p0
.end method

.method public administrative_district_level_3(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Address$Builder;
    .locals 0

    .line 476
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->administrative_district_level_3:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/resources/Address;
    .locals 20

    move-object/from16 v0, p0

    .line 522
    new-instance v18, Lcom/squareup/protos/connect/v2/resources/Address;

    move-object/from16 v1, v18

    iget-object v2, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->address_line_1:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->address_line_2:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->address_line_3:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->locality:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->sublocality:Ljava/lang/String;

    iget-object v7, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->sublocality_2:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->sublocality_3:Ljava/lang/String;

    iget-object v9, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->administrative_district_level_1:Ljava/lang/String;

    iget-object v10, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->administrative_district_level_2:Ljava/lang/String;

    iget-object v11, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->administrative_district_level_3:Ljava/lang/String;

    iget-object v12, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->postal_code:Ljava/lang/String;

    iget-object v13, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    iget-object v14, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->first_name:Ljava/lang/String;

    iget-object v15, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->last_name:Ljava/lang/String;

    move-object/from16 v19, v1

    iget-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->organization:Ljava/lang/String;

    move-object/from16 v16, v1

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v17

    move-object/from16 v1, v19

    invoke-direct/range {v1 .. v17}, Lcom/squareup/protos/connect/v2/resources/Address;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Country;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v18
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 340
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->build()Lcom/squareup/protos/connect/v2/resources/Address;

    move-result-object v0

    return-object v0
.end method

.method public country(Lcom/squareup/protos/connect/v2/resources/Country;)Lcom/squareup/protos/connect/v2/resources/Address$Builder;
    .locals 0

    .line 492
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0
.end method

.method public first_name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Address$Builder;
    .locals 0

    .line 500
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->first_name:Ljava/lang/String;

    return-object p0
.end method

.method public last_name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Address$Builder;
    .locals 0

    .line 508
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->last_name:Ljava/lang/String;

    return-object p0
.end method

.method public locality(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Address$Builder;
    .locals 0

    .line 416
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->locality:Ljava/lang/String;

    return-object p0
.end method

.method public organization(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Address$Builder;
    .locals 0

    .line 516
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->organization:Ljava/lang/String;

    return-object p0
.end method

.method public postal_code(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Address$Builder;
    .locals 0

    .line 484
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->postal_code:Ljava/lang/String;

    return-object p0
.end method

.method public sublocality(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Address$Builder;
    .locals 0

    .line 424
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->sublocality:Ljava/lang/String;

    return-object p0
.end method

.method public sublocality_2(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Address$Builder;
    .locals 0

    .line 432
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->sublocality_2:Ljava/lang/String;

    return-object p0
.end method

.method public sublocality_3(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Address$Builder;
    .locals 0

    .line 440
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Address$Builder;->sublocality_3:Ljava/lang/String;

    return-object p0
.end method
