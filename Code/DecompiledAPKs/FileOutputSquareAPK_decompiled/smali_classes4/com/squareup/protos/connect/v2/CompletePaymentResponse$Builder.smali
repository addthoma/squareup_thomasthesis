.class public final Lcom/squareup/protos/connect/v2/CompletePaymentResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CompletePaymentResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/CompletePaymentResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/CompletePaymentResponse;",
        "Lcom/squareup/protos/connect/v2/CompletePaymentResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field

.field public payment:Lcom/squareup/protos/connect/v2/Payment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 104
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 105
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/CompletePaymentResponse$Builder;->errors:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/CompletePaymentResponse;
    .locals 4

    .line 127
    new-instance v0, Lcom/squareup/protos/connect/v2/CompletePaymentResponse;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentResponse$Builder;->errors:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/CompletePaymentResponse$Builder;->payment:Lcom/squareup/protos/connect/v2/Payment;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/CompletePaymentResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/connect/v2/Payment;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CompletePaymentResponse$Builder;->build()Lcom/squareup/protos/connect/v2/CompletePaymentResponse;

    move-result-object v0

    return-object v0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/protos/connect/v2/CompletePaymentResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/CompletePaymentResponse$Builder;"
        }
    .end annotation

    .line 112
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentResponse$Builder;->errors:Ljava/util/List;

    return-object p0
.end method

.method public payment(Lcom/squareup/protos/connect/v2/Payment;)Lcom/squareup/protos/connect/v2/CompletePaymentResponse$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentResponse$Builder;->payment:Lcom/squareup/protos/connect/v2/Payment;

    return-object p0
.end method
