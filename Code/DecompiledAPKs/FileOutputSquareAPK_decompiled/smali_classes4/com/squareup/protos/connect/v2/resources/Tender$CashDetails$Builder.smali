.class public final Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;",
        "Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public buyer_tendered_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public change_back_money:Lcom/squareup/protos/connect/v2/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1100
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;
    .locals 4

    .line 1121
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->buyer_tendered_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;-><init>(Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1095
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->build()Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    move-result-object v0

    return-object v0
.end method

.method public buyer_tendered_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;
    .locals 0

    .line 1107
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->buyer_tendered_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public change_back_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;
    .locals 0

    .line 1115
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method
