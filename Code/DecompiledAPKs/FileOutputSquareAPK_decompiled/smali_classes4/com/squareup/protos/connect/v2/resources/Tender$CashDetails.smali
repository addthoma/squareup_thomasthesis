.class public final Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;
.super Lcom/squareup/wire/Message;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CashDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$ProtoAdapter_CashDetails;,
        Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;",
        "Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final buyer_tendered_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final change_back_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1023
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$ProtoAdapter_CashDetails;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$ProtoAdapter_CashDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;)V
    .locals 1

    .line 1046
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;-><init>(Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 1051
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1052
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->buyer_tendered_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 1053
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1068
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1069
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    .line 1070
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->buyer_tendered_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->buyer_tendered_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 1071
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 1072
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1077
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 1079
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1080
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->buyer_tendered_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1081
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 1082
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;
    .locals 2

    .line 1058
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;-><init>()V

    .line 1059
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->buyer_tendered_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->buyer_tendered_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 1060
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 1061
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1022
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->newBuilder()Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1089
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1090
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->buyer_tendered_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_0

    const-string v1, ", buyer_tendered_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->buyer_tendered_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1091
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", change_back_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CashDetails{"

    .line 1092
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
