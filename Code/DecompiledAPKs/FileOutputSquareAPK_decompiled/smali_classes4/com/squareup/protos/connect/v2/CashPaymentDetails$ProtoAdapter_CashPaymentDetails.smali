.class final Lcom/squareup/protos/connect/v2/CashPaymentDetails$ProtoAdapter_CashPaymentDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CashPaymentDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/CashPaymentDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CashPaymentDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/CashPaymentDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 140
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/CashPaymentDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 159
    new-instance v0, Lcom/squareup/protos/connect/v2/CashPaymentDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/CashPaymentDetails$Builder;-><init>()V

    .line 160
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 161
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 166
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 164
    :cond_0
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CashPaymentDetails$Builder;->change_back_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/CashPaymentDetails$Builder;

    goto :goto_0

    .line 163
    :cond_1
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CashPaymentDetails$Builder;->buyer_supplied_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/CashPaymentDetails$Builder;

    goto :goto_0

    .line 170
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/CashPaymentDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 171
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/CashPaymentDetails$Builder;->build()Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 138
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/CashPaymentDetails$ProtoAdapter_CashPaymentDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/CashPaymentDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 152
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CashPaymentDetails;->buyer_supplied_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 153
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CashPaymentDetails;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 154
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/CashPaymentDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 138
    check-cast p2, Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/CashPaymentDetails$ProtoAdapter_CashPaymentDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/CashPaymentDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/CashPaymentDetails;)I
    .locals 4

    .line 145
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CashPaymentDetails;->buyer_supplied_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CashPaymentDetails;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v3, 0x2

    .line 146
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 147
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CashPaymentDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 138
    check-cast p1, Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/CashPaymentDetails$ProtoAdapter_CashPaymentDetails;->encodedSize(Lcom/squareup/protos/connect/v2/CashPaymentDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/CashPaymentDetails;)Lcom/squareup/protos/connect/v2/CashPaymentDetails;
    .locals 2

    .line 176
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CashPaymentDetails;->newBuilder()Lcom/squareup/protos/connect/v2/CashPaymentDetails$Builder;

    move-result-object p1

    .line 177
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/CashPaymentDetails$Builder;->buyer_supplied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CashPaymentDetails$Builder;->buyer_supplied_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/CashPaymentDetails$Builder;->buyer_supplied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 178
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/CashPaymentDetails$Builder;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CashPaymentDetails$Builder;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/CashPaymentDetails$Builder;->change_back_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 179
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CashPaymentDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 180
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CashPaymentDetails$Builder;->build()Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 138
    check-cast p1, Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/CashPaymentDetails$ProtoAdapter_CashPaymentDetails;->redact(Lcom/squareup/protos/connect/v2/CashPaymentDetails;)Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    move-result-object p1

    return-object p1
.end method
