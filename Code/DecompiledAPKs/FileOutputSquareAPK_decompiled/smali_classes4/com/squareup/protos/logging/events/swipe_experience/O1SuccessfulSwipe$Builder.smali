.class public final Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "O1SuccessfulSwipe.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public authenticated_length:Ljava/lang/Integer;

.field public counter:Ljava/lang/Integer;

.field public end_period:Ljava/lang/Integer;

.field public entropy:Ljava/lang/Integer;

.field public resets:Ljava/lang/Integer;

.field public start_period:Ljava/lang/Integer;

.field public status:Ljava/lang/Integer;

.field public wakeups:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 252
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public authenticated_length(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;
    .locals 0

    .line 286
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->authenticated_length:Ljava/lang/Integer;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;
    .locals 11

    .line 358
    new-instance v10, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->counter:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->entropy:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->authenticated_length:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->resets:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->wakeups:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->status:Ljava/lang/Integer;

    iget-object v7, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->start_period:Ljava/lang/Integer;

    iget-object v8, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->end_period:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 235
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    move-result-object v0

    return-object v0
.end method

.method public counter(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;
    .locals 0

    .line 267
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->counter:Ljava/lang/Integer;

    return-object p0
.end method

.method public end_period(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;
    .locals 0

    .line 352
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->end_period:Ljava/lang/Integer;

    return-object p0
.end method

.method public entropy(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;
    .locals 0

    .line 276
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->entropy:Ljava/lang/Integer;

    return-object p0
.end method

.method public resets(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;
    .locals 0

    .line 301
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->resets:Ljava/lang/Integer;

    return-object p0
.end method

.method public start_period(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;
    .locals 0

    .line 341
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->start_period:Ljava/lang/Integer;

    return-object p0
.end method

.method public status(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;
    .locals 0

    .line 324
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->status:Ljava/lang/Integer;

    return-object p0
.end method

.method public wakeups(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;
    .locals 0

    .line 311
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->wakeups:Ljava/lang/Integer;

    return-object p0
.end method
