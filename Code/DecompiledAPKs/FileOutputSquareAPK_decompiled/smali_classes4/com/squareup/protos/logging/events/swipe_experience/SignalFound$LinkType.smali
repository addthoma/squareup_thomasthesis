.class public final enum Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;
.super Ljava/lang/Enum;
.source "SignalFound.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LinkType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType$ProtoAdapter_LinkType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum GEN2:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

.field public static final enum NOISE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

.field public static final enum O1:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

.field public static final enum R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

.field public static final enum R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

.field public static final enum UNKNOWN_SIGNAL:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 562
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    const/4 v1, 0x0

    const-string v2, "NOISE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->NOISE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 564
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    const/4 v2, 0x1

    const-string v3, "O1"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->O1:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 566
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    const/4 v3, 0x2

    const-string v4, "GEN2"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->GEN2:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 568
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    const/4 v4, 0x3

    const-string v5, "R4_FAST"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 570
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    const/4 v5, 0x4

    const-string v6, "R4_SLOW"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 572
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    const/4 v6, 0x5

    const-string v7, "UNKNOWN_SIGNAL"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->UNKNOWN_SIGNAL:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 561
    sget-object v7, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->NOISE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->O1:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->GEN2:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->UNKNOWN_SIGNAL:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 574
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType$ProtoAdapter_LinkType;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType$ProtoAdapter_LinkType;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 578
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 579
    iput p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 592
    :cond_0
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->UNKNOWN_SIGNAL:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    return-object p0

    .line 591
    :cond_1
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    return-object p0

    .line 590
    :cond_2
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    return-object p0

    .line 589
    :cond_3
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->GEN2:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    return-object p0

    .line 588
    :cond_4
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->O1:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    return-object p0

    .line 587
    :cond_5
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->NOISE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;
    .locals 1

    .line 561
    const-class v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;
    .locals 1

    .line 561
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-virtual {v0}, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 599
    iget v0, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->value:I

    return v0
.end method
