.class final Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$ProtoAdapter_ClassifyInfo;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ClassifyInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ClassifyInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 554
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 601
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;-><init>()V

    .line 602
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 603
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 629
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 627
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->peak_variability(Ljava/lang/Float;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    goto :goto_0

    .line 626
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->peak_count(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    goto :goto_0

    .line 625
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->gen2_score(Ljava/lang/Float;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    goto :goto_0

    .line 624
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->m1_slow_score(Ljava/lang/Float;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    goto :goto_0

    .line 623
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->m1_fast_score(Ljava/lang/Float;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    goto :goto_0

    .line 622
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->o1_score(Ljava/lang/Float;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    goto :goto_0

    .line 621
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->calc_spacings_and_variability_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    goto :goto_0

    .line 620
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->find_peaks_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    goto :goto_0

    .line 619
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->normalize_and_center_around_mean_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    goto :goto_0

    .line 618
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->gen2_low_pass_filter_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    goto/16 :goto_0

    .line 617
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->analyze_ffts_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    goto/16 :goto_0

    .line 616
    :pswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->ffts_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    goto/16 :goto_0

    .line 615
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->calc_square_and_remove_mean_runtime_in_us(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    goto/16 :goto_0

    .line 614
    :pswitch_d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->low_pass_filter_runtime_in_us(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    goto/16 :goto_0

    .line 613
    :pswitch_e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->runtime_in_us(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    goto/16 :goto_0

    .line 607
    :pswitch_f
    :try_start_0
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->link_type(Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v4

    .line 609
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 633
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 634
    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 552
    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$ProtoAdapter_ClassifyInfo;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 580
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 581
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->runtime_in_us:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 582
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->low_pass_filter_runtime_in_us:Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 583
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_square_and_remove_mean_runtime_in_us:Ljava/lang/Integer;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 584
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ffts_runtime:Ljava/lang/Integer;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 585
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->analyze_ffts_runtime:Ljava/lang/Integer;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 586
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_low_pass_filter_runtime:Ljava/lang/Integer;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 587
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->normalize_and_center_around_mean_runtime:Ljava/lang/Integer;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 588
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->find_peaks_runtime:Ljava/lang/Integer;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 589
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_spacings_and_variability_runtime:Ljava/lang/Integer;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 590
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->o1_score:Ljava/lang/Float;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 591
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_fast_score:Ljava/lang/Float;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 592
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_slow_score:Ljava/lang/Float;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 593
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_score:Ljava/lang/Float;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 594
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_count:Ljava/lang/Integer;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 595
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_variability:Ljava/lang/Float;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 596
    invoke-virtual {p2}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 552
    check-cast p2, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$ProtoAdapter_ClassifyInfo;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;)I
    .locals 4

    .line 559
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->runtime_in_us:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 560
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->low_pass_filter_runtime_in_us:Ljava/lang/Integer;

    const/4 v3, 0x3

    .line 561
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_square_and_remove_mean_runtime_in_us:Ljava/lang/Integer;

    const/4 v3, 0x4

    .line 562
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ffts_runtime:Ljava/lang/Integer;

    const/4 v3, 0x5

    .line 563
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->analyze_ffts_runtime:Ljava/lang/Integer;

    const/4 v3, 0x6

    .line 564
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_low_pass_filter_runtime:Ljava/lang/Integer;

    const/4 v3, 0x7

    .line 565
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->normalize_and_center_around_mean_runtime:Ljava/lang/Integer;

    const/16 v3, 0x8

    .line 566
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->find_peaks_runtime:Ljava/lang/Integer;

    const/16 v3, 0x9

    .line 567
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_spacings_and_variability_runtime:Ljava/lang/Integer;

    const/16 v3, 0xa

    .line 568
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->o1_score:Ljava/lang/Float;

    const/16 v3, 0xb

    .line 569
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_fast_score:Ljava/lang/Float;

    const/16 v3, 0xc

    .line 570
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_slow_score:Ljava/lang/Float;

    const/16 v3, 0xd

    .line 571
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_score:Ljava/lang/Float;

    const/16 v3, 0xe

    .line 572
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_count:Ljava/lang/Integer;

    const/16 v3, 0xf

    .line 573
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_variability:Ljava/lang/Float;

    const/16 v3, 0x10

    .line 574
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 575
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 552
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$ProtoAdapter_ClassifyInfo;->encodedSize(Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;
    .locals 0

    .line 639
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    move-result-object p1

    .line 640
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 641
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 552
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$ProtoAdapter_ClassifyInfo;->redact(Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    move-result-object p1

    return-object p1
.end method
