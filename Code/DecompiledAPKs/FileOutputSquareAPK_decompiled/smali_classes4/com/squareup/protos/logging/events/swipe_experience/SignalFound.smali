.class public final Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;
.super Lcom/squareup/wire/Message;
.source "SignalFound.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ProtoAdapter_SignalFound;,
        Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;,
        Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;,
        Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;,
        Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;,
        Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;,
        Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;,
        Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;",
        "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CLASSIFIED_LINK_TYPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

.field public static final DEFAULT_DECISION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

.field public static final DEFAULT_DEPRECATED_NUMBER_OF_SAMPLE:Ljava/lang/Integer;

.field public static final DEFAULT_EXPECTED_READER_TYPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

.field public static final DEFAULT_ISSUER_ID:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

.field public static final DEFAULT_LINK_TYPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

.field public static final DEFAULT_PACKET_TYPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

.field public static final DEFAULT_READER_HARDWARE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_READER_TYPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

.field public static final DEFAULT_SAMPLE_RATE:Ljava/lang/Integer;

.field public static final DEFAULT_SIGNAL_START_TO_DECISION_DURATION_IN_US:Ljava/lang/Integer;

.field public static final DEFAULT_SWIPE_DIRECTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

.field public static final DEFAULT_TRACK1_READ_SUCCESS:Ljava/lang/Boolean;

.field public static final DEFAULT_TRACK2_READ_SUCCESS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.SignalFound$LinkType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final decision:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.SignalFound$Decision#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final demod_info:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.DemodInfo#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x11
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final deprecated_number_of_sample:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xd
    .end annotation
.end field

.field public final expected_reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.SignalFound$ReaderType#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final issuer_id:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.SignalFound$IssuerId#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.SignalFound$LinkType#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.O1Packet#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.SignalFound$PacketType#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.R4Packet#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final reader_hardware_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.SignalFound$ReaderType#ADAPTER"
        tag = 0x12
    .end annotation
.end field

.field public final sample_rate:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xc
    .end annotation
.end field

.field public final signal_start_to_decision_duration_in_us:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x7
    .end annotation
.end field

.field public final swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.SignalFound$SwipeDirection#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final track1_read_success:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final track2_read_success:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 28
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ProtoAdapter_SignalFound;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ProtoAdapter_SignalFound;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->NOISE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->DEFAULT_LINK_TYPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 34
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->NOISE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->DEFAULT_CLASSIFIED_LINK_TYPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 36
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->GEN2_SUCCESSFUL_SWIPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->DEFAULT_PACKET_TYPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 38
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->GEN2_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->DEFAULT_EXPECTED_READER_TYPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    const/4 v0, 0x0

    .line 40
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->DEFAULT_TRACK1_READ_SUCCESS:Ljava/lang/Boolean;

    .line 42
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->DEFAULT_TRACK2_READ_SUCCESS:Ljava/lang/Boolean;

    .line 44
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->DEFAULT_SIGNAL_START_TO_DECISION_DURATION_IN_US:Ljava/lang/Integer;

    .line 48
    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->SWIPE_SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->DEFAULT_DECISION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    .line 50
    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->VISA:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->DEFAULT_ISSUER_ID:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    .line 52
    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->FORWARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->DEFAULT_SWIPE_DIRECTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    .line 54
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->DEFAULT_SAMPLE_RATE:Ljava/lang/Integer;

    .line 56
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->DEFAULT_DEPRECATED_NUMBER_OF_SAMPLE:Ljava/lang/Integer;

    .line 58
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->GEN2_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->DEFAULT_READER_TYPE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;Lokio/ByteString;)V
    .locals 1

    .line 237
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 238
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 239
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 240
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 241
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->expected_reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->expected_reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    .line 242
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->track1_read_success:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track1_read_success:Ljava/lang/Boolean;

    .line 243
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->track2_read_success:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track2_read_success:Ljava/lang/Boolean;

    .line 244
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->signal_start_to_decision_duration_in_us:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->signal_start_to_decision_duration_in_us:Ljava/lang/Integer;

    .line 245
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->reader_hardware_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_hardware_id:Ljava/lang/String;

    .line 246
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->decision:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->decision:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    .line 247
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->issuer_id:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->issuer_id:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    .line 248
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    .line 249
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->sample_rate:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->sample_rate:Ljava/lang/Integer;

    .line 250
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->deprecated_number_of_sample:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->deprecated_number_of_sample:Ljava/lang/Integer;

    .line 251
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    .line 252
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    .line 253
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->demod_info:Ljava/util/List;

    const-string v0, "demod_info"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->demod_info:Ljava/util/List;

    .line 254
    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 284
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 285
    :cond_1
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    .line 286
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 287
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 288
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 289
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->expected_reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->expected_reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    .line 290
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track1_read_success:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track1_read_success:Ljava/lang/Boolean;

    .line 291
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track2_read_success:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track2_read_success:Ljava/lang/Boolean;

    .line 292
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->signal_start_to_decision_duration_in_us:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->signal_start_to_decision_duration_in_us:Ljava/lang/Integer;

    .line 293
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_hardware_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_hardware_id:Ljava/lang/String;

    .line 294
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->decision:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->decision:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    .line 295
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->issuer_id:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->issuer_id:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    .line 296
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    .line 297
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->sample_rate:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->sample_rate:Ljava/lang/Integer;

    .line 298
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->deprecated_number_of_sample:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->deprecated_number_of_sample:Ljava/lang/Integer;

    .line 299
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    .line 300
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    .line 301
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->demod_info:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->demod_info:Ljava/util/List;

    .line 302
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    .line 303
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 308
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_10

    .line 310
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 311
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 312
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 313
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 314
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->expected_reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 315
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track1_read_success:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 316
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track2_read_success:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 317
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->signal_start_to_decision_duration_in_us:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 318
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_hardware_id:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 319
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->decision:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 320
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->issuer_id:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 321
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 322
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->sample_rate:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 323
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->deprecated_number_of_sample:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 324
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 325
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 326
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->demod_info:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 327
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->hashCode()I

    move-result v2

    :cond_f
    add-int/2addr v0, v2

    .line 328
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_10
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;
    .locals 2

    .line 259
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;-><init>()V

    .line 260
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 261
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 262
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    .line 263
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->expected_reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->expected_reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    .line 264
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track1_read_success:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->track1_read_success:Ljava/lang/Boolean;

    .line 265
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track2_read_success:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->track2_read_success:Ljava/lang/Boolean;

    .line 266
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->signal_start_to_decision_duration_in_us:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->signal_start_to_decision_duration_in_us:Ljava/lang/Integer;

    .line 267
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_hardware_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->reader_hardware_id:Ljava/lang/String;

    .line 268
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->decision:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->decision:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    .line 269
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->issuer_id:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->issuer_id:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    .line 270
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    .line 271
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->sample_rate:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->sample_rate:Ljava/lang/Integer;

    .line 272
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->deprecated_number_of_sample:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->deprecated_number_of_sample:Ljava/lang/Integer;

    .line 273
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    .line 274
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    .line 275
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->demod_info:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->demod_info:Ljava/util/List;

    .line 276
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    .line 277
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 335
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 336
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    if-eqz v1, :cond_0

    const-string v1, ", link_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 337
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    if-eqz v1, :cond_1

    const-string v1, ", classified_link_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 338
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    if-eqz v1, :cond_2

    const-string v1, ", packet_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->packet_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$PacketType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 339
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->expected_reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    if-eqz v1, :cond_3

    const-string v1, ", expected_reader_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->expected_reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 340
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track1_read_success:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", track1_read_success="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track1_read_success:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 341
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track2_read_success:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", track2_read_success="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->track2_read_success:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 342
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->signal_start_to_decision_duration_in_us:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    const-string v1, ", signal_start_to_decision_duration_in_us="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->signal_start_to_decision_duration_in_us:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 343
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_hardware_id:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", reader_hardware_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_hardware_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->decision:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    if-eqz v1, :cond_8

    const-string v1, ", decision="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->decision:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 345
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->issuer_id:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    if-eqz v1, :cond_9

    const-string v1, ", issuer_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->issuer_id:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 346
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    if-eqz v1, :cond_a

    const-string v1, ", swipe_direction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->swipe_direction:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 347
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->sample_rate:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    const-string v1, ", sample_rate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->sample_rate:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 348
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->deprecated_number_of_sample:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    const-string v1, ", deprecated_number_of_sample="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->deprecated_number_of_sample:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 349
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    if-eqz v1, :cond_d

    const-string v1, ", o1_packet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->o1_packet:Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 350
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    if-eqz v1, :cond_e

    const-string v1, ", r4_packet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->r4_packet:Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 351
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->demod_info:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, ", demod_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->demod_info:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 352
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    if-eqz v1, :cond_10

    const-string v1, ", reader_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_10
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SignalFound{"

    .line 353
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
