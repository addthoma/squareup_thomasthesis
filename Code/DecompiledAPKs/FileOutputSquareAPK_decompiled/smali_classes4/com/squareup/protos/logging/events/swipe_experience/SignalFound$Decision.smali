.class public final enum Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;
.super Ljava/lang/Enum;
.source "SignalFound.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Decision"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision$ProtoAdapter_Decision;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DEAD_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

.field public static final enum DELAYED_FAILED:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

.field public static final enum IGNORED_NOISE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

.field public static final enum IGNORED_NO_ACTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

.field public static final enum SWIPE_FAILED:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

.field public static final enum SWIPE_SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 728
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    const/4 v1, 0x0

    const-string v2, "SWIPE_SUCCESS"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->SWIPE_SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    .line 733
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    const/4 v2, 0x1

    const-string v3, "SWIPE_FAILED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->SWIPE_FAILED:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    .line 740
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    const/4 v3, 0x2

    const-string v4, "DELAYED_FAILED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->DELAYED_FAILED:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    .line 746
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    const/4 v4, 0x3

    const-string v5, "IGNORED_NOISE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->IGNORED_NOISE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    .line 755
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    const/4 v5, 0x4

    const-string v6, "IGNORED_NO_ACTION"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->IGNORED_NO_ACTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    .line 760
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    const/4 v6, 0x5

    const-string v7, "DEAD_READER"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->DEAD_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    .line 724
    sget-object v7, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->SWIPE_SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->SWIPE_FAILED:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->DELAYED_FAILED:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->IGNORED_NOISE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->IGNORED_NO_ACTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->DEAD_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    .line 762
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision$ProtoAdapter_Decision;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision$ProtoAdapter_Decision;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 766
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 767
    iput p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 780
    :cond_0
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->DEAD_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    return-object p0

    .line 779
    :cond_1
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->IGNORED_NO_ACTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    return-object p0

    .line 778
    :cond_2
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->IGNORED_NOISE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    return-object p0

    .line 777
    :cond_3
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->DELAYED_FAILED:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    return-object p0

    .line 776
    :cond_4
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->SWIPE_FAILED:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    return-object p0

    .line 775
    :cond_5
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->SWIPE_SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;
    .locals 1

    .line 724
    const-class v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;
    .locals 1

    .line 724
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    invoke-virtual {v0}, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 787
    iget v0, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Decision;->value:I

    return v0
.end method
