.class public final enum Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;
.super Ljava/lang/Enum;
.source "SignalFound.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReaderType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType$ProtoAdapter_ReaderType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum GEN2_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

.field public static final enum O1_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

.field public static final enum R4_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 677
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    const/4 v1, 0x0

    const-string v2, "GEN2_READER"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->GEN2_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    .line 679
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    const/4 v2, 0x1

    const-string v3, "O1_READER"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->O1_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    .line 681
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    const/4 v3, 0x2

    const-string v4, "R4_READER"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->R4_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    .line 676
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->GEN2_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->O1_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->R4_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    .line 683
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType$ProtoAdapter_ReaderType;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType$ProtoAdapter_ReaderType;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 687
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 688
    iput p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 698
    :cond_0
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->R4_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    return-object p0

    .line 697
    :cond_1
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->O1_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    return-object p0

    .line 696
    :cond_2
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->GEN2_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;
    .locals 1

    .line 676
    const-class v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;
    .locals 1

    .line 676
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    invoke-virtual {v0}, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 705
    iget v0, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->value:I

    return v0
.end method
