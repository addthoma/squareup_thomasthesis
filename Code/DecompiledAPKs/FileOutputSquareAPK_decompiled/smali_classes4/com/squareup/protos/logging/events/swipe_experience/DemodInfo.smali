.class public final Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;
.super Lcom/squareup/wire/Message;
.source "DemodInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$ProtoAdapter_DemodInfo;,
        Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;,
        Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;",
        "Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DEMOD_TYPE:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

.field public static final DEFAULT_RUNTIME_IN_US:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final demod_type:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.DemodInfo$DemodType#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final gen2_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.Gen2DemodInfo#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final o1_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.O1DemodInfo#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final runtime_in_us:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x2
    .end annotation
.end field

.field public final sqlink_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.SqLinkDemodInfo#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$ProtoAdapter_DemodInfo;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$ProtoAdapter_DemodInfo;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 31
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->GEN2:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->DEFAULT_DEMOD_TYPE:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    const/4 v0, 0x0

    .line 33
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->DEFAULT_RUNTIME_IN_US:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;Ljava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;)V
    .locals 7

    .line 84
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;-><init>(Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;Ljava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;Ljava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;Lokio/ByteString;)V
    .locals 1

    .line 89
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 90
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->demod_type:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    .line 91
    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->runtime_in_us:Ljava/lang/Integer;

    .line 92
    iput-object p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->gen2_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;

    .line 93
    iput-object p4, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->o1_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;

    .line 94
    iput-object p5, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->sqlink_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 112
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 113
    :cond_1
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->demod_type:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->demod_type:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->runtime_in_us:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->runtime_in_us:Ljava/lang/Integer;

    .line 116
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->gen2_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->gen2_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;

    .line 117
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->o1_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->o1_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;

    .line 118
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->sqlink_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->sqlink_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    .line 119
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 124
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->demod_type:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->runtime_in_us:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->gen2_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->o1_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->sqlink_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 132
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;
    .locals 2

    .line 99
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;-><init>()V

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->demod_type:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->demod_type:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->runtime_in_us:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->runtime_in_us:Ljava/lang/Integer;

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->gen2_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->gen2_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->o1_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->o1_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->sqlink_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->sqlink_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->demod_type:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    if-eqz v1, :cond_0

    const-string v1, ", demod_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->demod_type:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 141
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->runtime_in_us:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", runtime_in_us="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->runtime_in_us:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 142
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->gen2_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;

    if-eqz v1, :cond_2

    const-string v1, ", gen2_demod_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->gen2_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 143
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->o1_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;

    if-eqz v1, :cond_3

    const-string v1, ", o1_demod_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->o1_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 144
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->sqlink_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    if-eqz v1, :cond_4

    const-string v1, ", sqlink_demod_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;->sqlink_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DemodInfo{"

    .line 145
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
