.class public final Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;
.super Lcom/squareup/wire/Message;
.source "ClassifyInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$ProtoAdapter_ClassifyInfo;,
        Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;,
        Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;",
        "Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ANALYZE_FFTS_RUNTIME:Ljava/lang/Integer;

.field public static final DEFAULT_CALC_SPACINGS_AND_VARIABILITY_RUNTIME:Ljava/lang/Integer;

.field public static final DEFAULT_CALC_SQUARE_AND_REMOVE_MEAN_RUNTIME_IN_US:Ljava/lang/Integer;

.field public static final DEFAULT_FFTS_RUNTIME:Ljava/lang/Integer;

.field public static final DEFAULT_FIND_PEAKS_RUNTIME:Ljava/lang/Integer;

.field public static final DEFAULT_GEN2_LOW_PASS_FILTER_RUNTIME:Ljava/lang/Integer;

.field public static final DEFAULT_GEN2_SCORE:Ljava/lang/Float;

.field public static final DEFAULT_LINK_TYPE:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

.field public static final DEFAULT_LOW_PASS_FILTER_RUNTIME_IN_US:Ljava/lang/Integer;

.field public static final DEFAULT_M1_FAST_SCORE:Ljava/lang/Float;

.field public static final DEFAULT_M1_SLOW_SCORE:Ljava/lang/Float;

.field public static final DEFAULT_NORMALIZE_AND_CENTER_AROUND_MEAN_RUNTIME:Ljava/lang/Integer;

.field public static final DEFAULT_O1_SCORE:Ljava/lang/Float;

.field public static final DEFAULT_PEAK_COUNT:Ljava/lang/Integer;

.field public static final DEFAULT_PEAK_VARIABILITY:Ljava/lang/Float;

.field public static final DEFAULT_RUNTIME_IN_US:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final analyze_ffts_runtime:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x6
    .end annotation
.end field

.field public final calc_spacings_and_variability_runtime:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xa
    .end annotation
.end field

.field public final calc_square_and_remove_mean_runtime_in_us:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final ffts_runtime:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x5
    .end annotation
.end field

.field public final find_peaks_runtime:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x9
    .end annotation
.end field

.field public final gen2_low_pass_filter_runtime:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x7
    .end annotation
.end field

.field public final gen2_score:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#FLOAT"
        tag = 0xe
    .end annotation
.end field

.field public final link_type:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.ClassifyInfo$LinkType#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final low_pass_filter_runtime_in_us:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field

.field public final m1_fast_score:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#FLOAT"
        tag = 0xc
    .end annotation
.end field

.field public final m1_slow_score:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#FLOAT"
        tag = 0xd
    .end annotation
.end field

.field public final normalize_and_center_around_mean_runtime:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x8
    .end annotation
.end field

.field public final o1_score:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#FLOAT"
        tag = 0xb
    .end annotation
.end field

.field public final peak_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xf
    .end annotation
.end field

.field public final peak_variability:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#FLOAT"
        tag = 0x10
    .end annotation
.end field

.field public final runtime_in_us:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 28
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$ProtoAdapter_ClassifyInfo;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$ProtoAdapter_ClassifyInfo;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;->NOISE:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->DEFAULT_LINK_TYPE:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    const/4 v0, 0x0

    .line 34
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->DEFAULT_RUNTIME_IN_US:Ljava/lang/Integer;

    .line 36
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->DEFAULT_LOW_PASS_FILTER_RUNTIME_IN_US:Ljava/lang/Integer;

    .line 38
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->DEFAULT_CALC_SQUARE_AND_REMOVE_MEAN_RUNTIME_IN_US:Ljava/lang/Integer;

    .line 40
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->DEFAULT_FFTS_RUNTIME:Ljava/lang/Integer;

    .line 42
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->DEFAULT_ANALYZE_FFTS_RUNTIME:Ljava/lang/Integer;

    .line 44
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->DEFAULT_GEN2_LOW_PASS_FILTER_RUNTIME:Ljava/lang/Integer;

    .line 46
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->DEFAULT_NORMALIZE_AND_CENTER_AROUND_MEAN_RUNTIME:Ljava/lang/Integer;

    .line 48
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->DEFAULT_FIND_PEAKS_RUNTIME:Ljava/lang/Integer;

    .line 50
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->DEFAULT_CALC_SPACINGS_AND_VARIABILITY_RUNTIME:Ljava/lang/Integer;

    const/4 v1, 0x0

    .line 52
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->DEFAULT_O1_SCORE:Ljava/lang/Float;

    .line 54
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->DEFAULT_M1_FAST_SCORE:Ljava/lang/Float;

    .line 56
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->DEFAULT_M1_SLOW_SCORE:Ljava/lang/Float;

    .line 58
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->DEFAULT_GEN2_SCORE:Ljava/lang/Float;

    .line 60
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->DEFAULT_PEAK_COUNT:Ljava/lang/Integer;

    .line 62
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->DEFAULT_PEAK_VARIABILITY:Ljava/lang/Float;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;Lokio/ByteString;)V
    .locals 1

    .line 209
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 210
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    .line 211
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->runtime_in_us:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->runtime_in_us:Ljava/lang/Integer;

    .line 212
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->low_pass_filter_runtime_in_us:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->low_pass_filter_runtime_in_us:Ljava/lang/Integer;

    .line 213
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->calc_square_and_remove_mean_runtime_in_us:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_square_and_remove_mean_runtime_in_us:Ljava/lang/Integer;

    .line 214
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->ffts_runtime:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ffts_runtime:Ljava/lang/Integer;

    .line 215
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->analyze_ffts_runtime:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->analyze_ffts_runtime:Ljava/lang/Integer;

    .line 216
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->gen2_low_pass_filter_runtime:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_low_pass_filter_runtime:Ljava/lang/Integer;

    .line 217
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->normalize_and_center_around_mean_runtime:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->normalize_and_center_around_mean_runtime:Ljava/lang/Integer;

    .line 218
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->find_peaks_runtime:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->find_peaks_runtime:Ljava/lang/Integer;

    .line 219
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->calc_spacings_and_variability_runtime:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_spacings_and_variability_runtime:Ljava/lang/Integer;

    .line 220
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->o1_score:Ljava/lang/Float;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->o1_score:Ljava/lang/Float;

    .line 221
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->m1_fast_score:Ljava/lang/Float;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_fast_score:Ljava/lang/Float;

    .line 222
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->m1_slow_score:Ljava/lang/Float;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_slow_score:Ljava/lang/Float;

    .line 223
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->gen2_score:Ljava/lang/Float;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_score:Ljava/lang/Float;

    .line 224
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->peak_count:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_count:Ljava/lang/Integer;

    .line 225
    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->peak_variability:Ljava/lang/Float;

    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_variability:Ljava/lang/Float;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 254
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 255
    :cond_1
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    .line 256
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    .line 257
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->runtime_in_us:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->runtime_in_us:Ljava/lang/Integer;

    .line 258
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->low_pass_filter_runtime_in_us:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->low_pass_filter_runtime_in_us:Ljava/lang/Integer;

    .line 259
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_square_and_remove_mean_runtime_in_us:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_square_and_remove_mean_runtime_in_us:Ljava/lang/Integer;

    .line 260
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ffts_runtime:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ffts_runtime:Ljava/lang/Integer;

    .line 261
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->analyze_ffts_runtime:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->analyze_ffts_runtime:Ljava/lang/Integer;

    .line 262
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_low_pass_filter_runtime:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_low_pass_filter_runtime:Ljava/lang/Integer;

    .line 263
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->normalize_and_center_around_mean_runtime:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->normalize_and_center_around_mean_runtime:Ljava/lang/Integer;

    .line 264
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->find_peaks_runtime:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->find_peaks_runtime:Ljava/lang/Integer;

    .line 265
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_spacings_and_variability_runtime:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_spacings_and_variability_runtime:Ljava/lang/Integer;

    .line 266
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->o1_score:Ljava/lang/Float;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->o1_score:Ljava/lang/Float;

    .line 267
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_fast_score:Ljava/lang/Float;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_fast_score:Ljava/lang/Float;

    .line 268
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_slow_score:Ljava/lang/Float;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_slow_score:Ljava/lang/Float;

    .line 269
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_score:Ljava/lang/Float;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_score:Ljava/lang/Float;

    .line 270
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_count:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_count:Ljava/lang/Integer;

    .line 271
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_variability:Ljava/lang/Float;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_variability:Ljava/lang/Float;

    .line 272
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 277
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_10

    .line 279
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 280
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 281
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->runtime_in_us:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 282
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->low_pass_filter_runtime_in_us:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 283
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_square_and_remove_mean_runtime_in_us:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 284
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ffts_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 285
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->analyze_ffts_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 286
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_low_pass_filter_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 287
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->normalize_and_center_around_mean_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 288
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->find_peaks_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 289
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_spacings_and_variability_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 290
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->o1_score:Ljava/lang/Float;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Float;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 291
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_fast_score:Ljava/lang/Float;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Float;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 292
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_slow_score:Ljava/lang/Float;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Float;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 293
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_score:Ljava/lang/Float;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Float;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 294
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_count:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 295
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_variability:Ljava/lang/Float;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Float;->hashCode()I

    move-result v2

    :cond_f
    add-int/2addr v0, v2

    .line 296
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_10
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    .locals 2

    .line 230
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;-><init>()V

    .line 231
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    .line 232
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->runtime_in_us:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->runtime_in_us:Ljava/lang/Integer;

    .line 233
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->low_pass_filter_runtime_in_us:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->low_pass_filter_runtime_in_us:Ljava/lang/Integer;

    .line 234
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_square_and_remove_mean_runtime_in_us:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->calc_square_and_remove_mean_runtime_in_us:Ljava/lang/Integer;

    .line 235
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ffts_runtime:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->ffts_runtime:Ljava/lang/Integer;

    .line 236
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->analyze_ffts_runtime:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->analyze_ffts_runtime:Ljava/lang/Integer;

    .line 237
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_low_pass_filter_runtime:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->gen2_low_pass_filter_runtime:Ljava/lang/Integer;

    .line 238
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->normalize_and_center_around_mean_runtime:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->normalize_and_center_around_mean_runtime:Ljava/lang/Integer;

    .line 239
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->find_peaks_runtime:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->find_peaks_runtime:Ljava/lang/Integer;

    .line 240
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_spacings_and_variability_runtime:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->calc_spacings_and_variability_runtime:Ljava/lang/Integer;

    .line 241
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->o1_score:Ljava/lang/Float;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->o1_score:Ljava/lang/Float;

    .line 242
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_fast_score:Ljava/lang/Float;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->m1_fast_score:Ljava/lang/Float;

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_slow_score:Ljava/lang/Float;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->m1_slow_score:Ljava/lang/Float;

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_score:Ljava/lang/Float;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->gen2_score:Ljava/lang/Float;

    .line 245
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->peak_count:Ljava/lang/Integer;

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_variability:Ljava/lang/Float;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->peak_variability:Ljava/lang/Float;

    .line 247
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 303
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 304
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    if-eqz v1, :cond_0

    const-string v1, ", link_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 305
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->runtime_in_us:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", runtime_in_us="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->runtime_in_us:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 306
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->low_pass_filter_runtime_in_us:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", low_pass_filter_runtime_in_us="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->low_pass_filter_runtime_in_us:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 307
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_square_and_remove_mean_runtime_in_us:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", calc_square_and_remove_mean_runtime_in_us="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_square_and_remove_mean_runtime_in_us:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 308
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ffts_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", ffts_runtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->ffts_runtime:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 309
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->analyze_ffts_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    const-string v1, ", analyze_ffts_runtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->analyze_ffts_runtime:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 310
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_low_pass_filter_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    const-string v1, ", gen2_low_pass_filter_runtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_low_pass_filter_runtime:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 311
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->normalize_and_center_around_mean_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    const-string v1, ", normalize_and_center_around_mean_runtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->normalize_and_center_around_mean_runtime:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 312
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->find_peaks_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    const-string v1, ", find_peaks_runtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->find_peaks_runtime:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 313
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_spacings_and_variability_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    const-string v1, ", calc_spacings_and_variability_runtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->calc_spacings_and_variability_runtime:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 314
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->o1_score:Ljava/lang/Float;

    if-eqz v1, :cond_a

    const-string v1, ", o1_score="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->o1_score:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 315
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_fast_score:Ljava/lang/Float;

    if-eqz v1, :cond_b

    const-string v1, ", m1_fast_score="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_fast_score:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 316
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_slow_score:Ljava/lang/Float;

    if-eqz v1, :cond_c

    const-string v1, ", m1_slow_score="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->m1_slow_score:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 317
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_score:Ljava/lang/Float;

    if-eqz v1, :cond_d

    const-string v1, ", gen2_score="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->gen2_score:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 318
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_count:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    const-string v1, ", peak_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 319
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_variability:Ljava/lang/Float;

    if-eqz v1, :cond_f

    const-string v1, ", peak_variability="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->peak_variability:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_f
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ClassifyInfo{"

    .line 320
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
