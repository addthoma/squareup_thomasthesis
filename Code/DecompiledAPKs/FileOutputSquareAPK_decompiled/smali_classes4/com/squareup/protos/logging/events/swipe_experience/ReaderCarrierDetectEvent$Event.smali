.class public final enum Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;
.super Ljava/lang/Enum;
.source "ReaderCarrierDetectEvent.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Event"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event$ProtoAdapter_Event;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DISCARD:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

.field public static final enum NOT_READY:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

.field public static final enum READY:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

.field public static final enum RESTART:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

.field public static final enum SIGNAL_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

.field public static final enum TOO_LONG:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

.field public static final enum TOO_SHORT:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 166
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    const/4 v1, 0x0

    const-string v2, "SIGNAL_FOUND"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->SIGNAL_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    .line 171
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    const/4 v2, 0x1

    const-string v3, "RESTART"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->RESTART:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    .line 176
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    const/4 v3, 0x2

    const-string v4, "DISCARD"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->DISCARD:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    .line 181
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    const/4 v4, 0x3

    const-string v5, "TOO_SHORT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->TOO_SHORT:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    .line 186
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    const/4 v5, 0x4

    const-string v6, "TOO_LONG"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->TOO_LONG:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    .line 192
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    const/4 v6, 0x5

    const-string v7, "READY"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->READY:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    .line 197
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    const/4 v7, 0x6

    const-string v8, "NOT_READY"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->NOT_READY:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    .line 162
    sget-object v8, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->SIGNAL_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->RESTART:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->DISCARD:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->TOO_SHORT:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->TOO_LONG:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->READY:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->NOT_READY:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    .line 199
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event$ProtoAdapter_Event;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event$ProtoAdapter_Event;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 203
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 204
    iput p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 218
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->NOT_READY:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    return-object p0

    .line 217
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->READY:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    return-object p0

    .line 216
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->TOO_LONG:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    return-object p0

    .line 215
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->TOO_SHORT:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    return-object p0

    .line 214
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->DISCARD:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    return-object p0

    .line 213
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->RESTART:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    return-object p0

    .line 212
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->SIGNAL_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;
    .locals 1

    .line 162
    const-class v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;
    .locals 1

    .line 162
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    invoke-virtual {v0}, [Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 225
    iget v0, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->value:I

    return v0
.end method
