.class public final Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DemodInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;",
        "Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public demod_type:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

.field public gen2_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;

.field public o1_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;

.field public runtime_in_us:Ljava/lang/Integer;

.field public sqlink_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 159
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;
    .locals 8

    .line 206
    new-instance v7, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->demod_type:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    iget-object v2, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->runtime_in_us:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->gen2_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;

    iget-object v4, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->o1_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;

    iget-object v5, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->sqlink_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;-><init>(Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;Ljava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 148
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo;

    move-result-object v0

    return-object v0
.end method

.method public demod_type(Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;)Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->demod_type:Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$DemodType;

    return-object p0
.end method

.method public gen2_demod_info(Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;)Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->gen2_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo;

    return-object p0
.end method

.method public o1_demod_info(Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;)Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;
    .locals 0

    .line 192
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->o1_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;

    return-object p0
.end method

.method public runtime_in_us(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->runtime_in_us:Ljava/lang/Integer;

    return-object p0
.end method

.method public sqlink_demod_info(Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;)Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;
    .locals 0

    .line 200
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/DemodInfo$Builder;->sqlink_demod_info:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    return-object p0
.end method
