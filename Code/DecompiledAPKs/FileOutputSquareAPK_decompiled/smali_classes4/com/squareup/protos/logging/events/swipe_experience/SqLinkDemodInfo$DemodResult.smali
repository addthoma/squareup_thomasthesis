.class public final enum Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;
.super Ljava/lang/Enum;
.source "SqLinkDemodInfo.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DemodResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult$ProtoAdapter_DemodResult;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BAD_CRC:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

.field public static final enum BAD_SIZE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

.field public static final enum FREQ_EST_ERR:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

.field public static final enum INCOMPLETE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

.field public static final enum INVALID_PACKET:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

.field public static final enum NULL_VALUE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

.field public static final enum OO_MEM:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

.field public static final enum OO_RANGE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

.field public static final enum PACKET_COUNT:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

.field public static final enum SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

.field public static final enum SYNC_GEN:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 739
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    const/4 v1, 0x0

    const-string v2, "SUCCESS"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    .line 744
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    const/4 v2, 0x1

    const-string v3, "INCOMPLETE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->INCOMPLETE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    .line 749
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    const/4 v3, 0x2

    const-string v4, "BAD_CRC"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->BAD_CRC:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    .line 754
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    const/4 v4, 0x3

    const-string v5, "BAD_SIZE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->BAD_SIZE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    .line 759
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    const/4 v5, 0x4

    const-string v6, "NULL_VALUE"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->NULL_VALUE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    .line 764
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    const/4 v6, 0x5

    const-string v7, "OO_MEM"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->OO_MEM:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    .line 769
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    const/4 v7, 0x6

    const-string v8, "OO_RANGE"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->OO_RANGE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    .line 774
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    const/4 v8, 0x7

    const-string v9, "FREQ_EST_ERR"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->FREQ_EST_ERR:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    .line 779
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    const/16 v9, 0x8

    const-string v10, "SYNC_GEN"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->SYNC_GEN:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    .line 784
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    const/16 v10, 0x9

    const-string v11, "PACKET_COUNT"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->PACKET_COUNT:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    .line 789
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    const/16 v11, 0xa

    const-string v12, "INVALID_PACKET"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->INVALID_PACKET:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    .line 735
    sget-object v12, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    aput-object v12, v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->INCOMPLETE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->BAD_CRC:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->BAD_SIZE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->NULL_VALUE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->OO_MEM:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->OO_RANGE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->FREQ_EST_ERR:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->SYNC_GEN:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->PACKET_COUNT:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->INVALID_PACKET:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    aput-object v1, v0, v11

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    .line 791
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult$ProtoAdapter_DemodResult;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult$ProtoAdapter_DemodResult;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 795
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 796
    iput p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 814
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->INVALID_PACKET:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    return-object p0

    .line 813
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->PACKET_COUNT:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    return-object p0

    .line 812
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->SYNC_GEN:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    return-object p0

    .line 811
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->FREQ_EST_ERR:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    return-object p0

    .line 810
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->OO_RANGE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    return-object p0

    .line 809
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->OO_MEM:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    return-object p0

    .line 808
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->NULL_VALUE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    return-object p0

    .line 807
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->BAD_SIZE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    return-object p0

    .line 806
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->BAD_CRC:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    return-object p0

    .line 805
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->INCOMPLETE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    return-object p0

    .line 804
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;
    .locals 1

    .line 735
    const-class v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;
    .locals 1

    .line 735
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    invoke-virtual {v0}, [Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 821
    iget v0, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->value:I

    return v0
.end method
