.class public final Lcom/squareup/protos/onboarding/StatusResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/onboarding/StatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/onboarding/StatusResponse;",
        "Lcom/squareup/protos/onboarding/StatusResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/onboarding/Error;",
            ">;"
        }
    .end annotation
.end field

.field public success:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 98
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 99
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/onboarding/StatusResponse$Builder;->error:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/onboarding/StatusResponse;
    .locals 4

    .line 115
    new-instance v0, Lcom/squareup/protos/onboarding/StatusResponse;

    iget-object v1, p0, Lcom/squareup/protos/onboarding/StatusResponse$Builder;->success:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/onboarding/StatusResponse$Builder;->error:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/onboarding/StatusResponse;-><init>(Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/onboarding/StatusResponse$Builder;->build()Lcom/squareup/protos/onboarding/StatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public error(Ljava/util/List;)Lcom/squareup/protos/onboarding/StatusResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/onboarding/Error;",
            ">;)",
            "Lcom/squareup/protos/onboarding/StatusResponse$Builder;"
        }
    .end annotation

    .line 108
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 109
    iput-object p1, p0, Lcom/squareup/protos/onboarding/StatusResponse$Builder;->error:Ljava/util/List;

    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/squareup/protos/onboarding/StatusResponse$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/onboarding/StatusResponse$Builder;->success:Ljava/lang/Boolean;

    return-object p0
.end method
