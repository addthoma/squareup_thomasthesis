.class public final Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "VaultedDataEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;",
        "Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public entity:Lcom/squareup/protos/privacyvault/model/Entity;

.field public vaulted_fields:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/privacyvault/service/Protected;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 100
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 101
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->vaulted_fields:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;
    .locals 4

    .line 117
    new-instance v0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;

    iget-object v1, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->entity:Lcom/squareup/protos/privacyvault/model/Entity;

    iget-object v2, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->vaulted_fields:Ljava/util/Map;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;-><init>(Lcom/squareup/protos/privacyvault/model/Entity;Ljava/util/Map;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->build()Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;

    move-result-object v0

    return-object v0
.end method

.method public entity(Lcom/squareup/protos/privacyvault/model/Entity;)Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->entity:Lcom/squareup/protos/privacyvault/model/Entity;

    return-object p0
.end method

.method public vaulted_fields(Ljava/util/Map;)Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/privacyvault/service/Protected;",
            ">;)",
            "Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;"
        }
    .end annotation

    .line 110
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/Map;)V

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->vaulted_fields:Ljava/util/Map;

    return-object p0
.end method
