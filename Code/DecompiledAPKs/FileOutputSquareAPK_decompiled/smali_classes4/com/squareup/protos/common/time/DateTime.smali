.class public final Lcom/squareup/protos/common/time/DateTime;
.super Lcom/squareup/wire/Message;
.source "DateTime.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/time/DateTime$ProtoAdapter_DateTime;,
        Lcom/squareup/protos/common/time/DateTime$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/common/time/DateTime;",
        "Lcom/squareup/protos/common/time/DateTime$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/time/DateTime;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_INSTANT_USEC:Ljava/lang/Long;

.field public static final DEFAULT_ORDINAL:Ljava/lang/Long;

.field public static final DEFAULT_POSIX_TZ:Ljava/lang/String; = ""

.field public static final DEFAULT_TIMEZONE_OFFSET_MIN:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final instant_usec:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field

.field public final ordinal:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x5
    .end annotation
.end field

.field public final posix_tz:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final timezone_offset_min:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#SINT32"
        tag = 0x2
    .end annotation
.end field

.field public final tz_name:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 61
    new-instance v0, Lcom/squareup/protos/common/time/DateTime$ProtoAdapter_DateTime;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/DateTime$ProtoAdapter_DateTime;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 65
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/common/time/DateTime;->DEFAULT_INSTANT_USEC:Ljava/lang/Long;

    const/4 v1, 0x0

    .line 67
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/common/time/DateTime;->DEFAULT_TIMEZONE_OFFSET_MIN:Ljava/lang/Integer;

    .line 71
    sput-object v0, Lcom/squareup/protos/common/time/DateTime;->DEFAULT_ORDINAL:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .line 127
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/common/time/DateTime;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Long;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 132
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 133
    iput-object p1, p0, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    .line 134
    iput-object p2, p0, Lcom/squareup/protos/common/time/DateTime;->timezone_offset_min:Ljava/lang/Integer;

    .line 135
    iput-object p3, p0, Lcom/squareup/protos/common/time/DateTime;->posix_tz:Ljava/lang/String;

    const-string p1, "tz_name"

    .line 136
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/common/time/DateTime;->tz_name:Ljava/util/List;

    .line 137
    iput-object p5, p0, Lcom/squareup/protos/common/time/DateTime;->ordinal:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 155
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 156
    :cond_1
    check-cast p1, Lcom/squareup/protos/common/time/DateTime;

    .line 157
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/DateTime;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/common/time/DateTime;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    .line 158
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->timezone_offset_min:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/common/time/DateTime;->timezone_offset_min:Ljava/lang/Integer;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->posix_tz:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/time/DateTime;->posix_tz:Ljava/lang/String;

    .line 160
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->tz_name:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/common/time/DateTime;->tz_name:Ljava/util/List;

    .line 161
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->ordinal:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/protos/common/time/DateTime;->ordinal:Ljava/lang/Long;

    .line 162
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 167
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 169
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/DateTime;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 170
    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->timezone_offset_min:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->posix_tz:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->tz_name:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->ordinal:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 175
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/common/time/DateTime$Builder;
    .locals 2

    .line 142
    new-instance v0, Lcom/squareup/protos/common/time/DateTime$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/DateTime$Builder;-><init>()V

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/common/time/DateTime$Builder;->instant_usec:Ljava/lang/Long;

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->timezone_offset_min:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/common/time/DateTime$Builder;->timezone_offset_min:Ljava/lang/Integer;

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->posix_tz:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/time/DateTime$Builder;->posix_tz:Ljava/lang/String;

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->tz_name:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/common/time/DateTime$Builder;->tz_name:Ljava/util/List;

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->ordinal:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/common/time/DateTime$Builder;->ordinal:Ljava/lang/Long;

    .line 148
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/DateTime;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/time/DateTime$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 60
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/DateTime;->newBuilder()Lcom/squareup/protos/common/time/DateTime$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 183
    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", instant_usec="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 184
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->timezone_offset_min:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", timezone_offset_min="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->timezone_offset_min:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 185
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->posix_tz:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", posix_tz="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->posix_tz:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->tz_name:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", tz_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->tz_name:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 187
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->ordinal:Ljava/lang/Long;

    if-eqz v1, :cond_4

    const-string v1, ", ordinal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime;->ordinal:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DateTime{"

    .line 188
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
