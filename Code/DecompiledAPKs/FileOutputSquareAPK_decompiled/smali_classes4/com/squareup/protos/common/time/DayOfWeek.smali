.class public final enum Lcom/squareup/protos/common/time/DayOfWeek;
.super Ljava/lang/Enum;
.source "DayOfWeek.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/time/DayOfWeek$ProtoAdapter_DayOfWeek;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/common/time/DayOfWeek;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/common/time/DayOfWeek;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/time/DayOfWeek;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FRIDAY:Lcom/squareup/protos/common/time/DayOfWeek;

.field public static final enum MONDAY:Lcom/squareup/protos/common/time/DayOfWeek;

.field public static final enum SATURDAY:Lcom/squareup/protos/common/time/DayOfWeek;

.field public static final enum SUNDAY:Lcom/squareup/protos/common/time/DayOfWeek;

.field public static final enum THURSDAY:Lcom/squareup/protos/common/time/DayOfWeek;

.field public static final enum TUESDAY:Lcom/squareup/protos/common/time/DayOfWeek;

.field public static final enum UNKNOWN_DO_NOT_USE:Lcom/squareup/protos/common/time/DayOfWeek;

.field public static final enum WEDNESDAY:Lcom/squareup/protos/common/time/DayOfWeek;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 15
    new-instance v0, Lcom/squareup/protos/common/time/DayOfWeek;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/common/time/DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/time/DayOfWeek;->UNKNOWN_DO_NOT_USE:Lcom/squareup/protos/common/time/DayOfWeek;

    .line 17
    new-instance v0, Lcom/squareup/protos/common/time/DayOfWeek;

    const/4 v2, 0x1

    const-string v3, "MONDAY"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/common/time/DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/time/DayOfWeek;->MONDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    .line 19
    new-instance v0, Lcom/squareup/protos/common/time/DayOfWeek;

    const/4 v3, 0x2

    const-string v4, "TUESDAY"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/common/time/DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/time/DayOfWeek;->TUESDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    .line 21
    new-instance v0, Lcom/squareup/protos/common/time/DayOfWeek;

    const/4 v4, 0x3

    const-string v5, "WEDNESDAY"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/common/time/DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/time/DayOfWeek;->WEDNESDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    .line 23
    new-instance v0, Lcom/squareup/protos/common/time/DayOfWeek;

    const/4 v5, 0x4

    const-string v6, "THURSDAY"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/common/time/DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/time/DayOfWeek;->THURSDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    .line 25
    new-instance v0, Lcom/squareup/protos/common/time/DayOfWeek;

    const/4 v6, 0x5

    const-string v7, "FRIDAY"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/common/time/DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/time/DayOfWeek;->FRIDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    .line 27
    new-instance v0, Lcom/squareup/protos/common/time/DayOfWeek;

    const/4 v7, 0x6

    const-string v8, "SATURDAY"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/common/time/DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/time/DayOfWeek;->SATURDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    .line 29
    new-instance v0, Lcom/squareup/protos/common/time/DayOfWeek;

    const/4 v8, 0x7

    const-string v9, "SUNDAY"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/common/time/DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/time/DayOfWeek;->SUNDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/protos/common/time/DayOfWeek;

    .line 14
    sget-object v9, Lcom/squareup/protos/common/time/DayOfWeek;->UNKNOWN_DO_NOT_USE:Lcom/squareup/protos/common/time/DayOfWeek;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DayOfWeek;->MONDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/time/DayOfWeek;->TUESDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/common/time/DayOfWeek;->WEDNESDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/common/time/DayOfWeek;->THURSDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/common/time/DayOfWeek;->FRIDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/common/time/DayOfWeek;->SATURDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/common/time/DayOfWeek;->SUNDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    aput-object v1, v0, v8

    sput-object v0, Lcom/squareup/protos/common/time/DayOfWeek;->$VALUES:[Lcom/squareup/protos/common/time/DayOfWeek;

    .line 31
    new-instance v0, Lcom/squareup/protos/common/time/DayOfWeek$ProtoAdapter_DayOfWeek;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/DayOfWeek$ProtoAdapter_DayOfWeek;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/time/DayOfWeek;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 36
    iput p3, p0, Lcom/squareup/protos/common/time/DayOfWeek;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/common/time/DayOfWeek;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 51
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/common/time/DayOfWeek;->SUNDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    return-object p0

    .line 50
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/common/time/DayOfWeek;->SATURDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    return-object p0

    .line 49
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/common/time/DayOfWeek;->FRIDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    return-object p0

    .line 48
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/common/time/DayOfWeek;->THURSDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    return-object p0

    .line 47
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/common/time/DayOfWeek;->WEDNESDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    return-object p0

    .line 46
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/common/time/DayOfWeek;->TUESDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    return-object p0

    .line 45
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/common/time/DayOfWeek;->MONDAY:Lcom/squareup/protos/common/time/DayOfWeek;

    return-object p0

    .line 44
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/common/time/DayOfWeek;->UNKNOWN_DO_NOT_USE:Lcom/squareup/protos/common/time/DayOfWeek;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/time/DayOfWeek;
    .locals 1

    .line 14
    const-class v0, Lcom/squareup/protos/common/time/DayOfWeek;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/common/time/DayOfWeek;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/common/time/DayOfWeek;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/protos/common/time/DayOfWeek;->$VALUES:[Lcom/squareup/protos/common/time/DayOfWeek;

    invoke-virtual {v0}, [Lcom/squareup/protos/common/time/DayOfWeek;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/common/time/DayOfWeek;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 58
    iget v0, p0, Lcom/squareup/protos/common/time/DayOfWeek;->value:I

    return v0
.end method
