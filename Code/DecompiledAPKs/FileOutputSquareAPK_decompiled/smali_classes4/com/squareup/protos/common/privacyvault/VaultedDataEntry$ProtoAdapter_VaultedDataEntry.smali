.class final Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$ProtoAdapter_VaultedDataEntry;
.super Lcom/squareup/wire/ProtoAdapter;
.source "VaultedDataEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_VaultedDataEntry"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;",
        ">;"
    }
.end annotation


# instance fields
.field private final vaulted_fields:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/privacyvault/service/Protected;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 125
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    .line 122
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    sget-object v1, Lcom/squareup/protos/privacyvault/service/Protected;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->newMapAdapter(Lcom/squareup/wire/ProtoAdapter;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$ProtoAdapter_VaultedDataEntry;->vaulted_fields:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 144
    new-instance v0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;-><init>()V

    .line 145
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 146
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 151
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 149
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->vaulted_fields:Ljava/util/Map;

    iget-object v4, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$ProtoAdapter_VaultedDataEntry;->vaulted_fields:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    invoke-interface {v3, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_0

    .line 148
    :cond_1
    sget-object v3, Lcom/squareup/protos/privacyvault/model/Entity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/privacyvault/model/Entity;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->entity(Lcom/squareup/protos/privacyvault/model/Entity;)Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;

    goto :goto_0

    .line 155
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 156
    invoke-virtual {v0}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->build()Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 121
    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$ProtoAdapter_VaultedDataEntry;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 137
    sget-object v0, Lcom/squareup/protos/privacyvault/model/Entity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->entity:Lcom/squareup/protos/privacyvault/model/Entity;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$ProtoAdapter_VaultedDataEntry;->vaulted_fields:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->vaulted_fields:Ljava/util/Map;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 139
    invoke-virtual {p2}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 121
    check-cast p2, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$ProtoAdapter_VaultedDataEntry;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;)I
    .locals 4

    .line 130
    sget-object v0, Lcom/squareup/protos/privacyvault/model/Entity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->entity:Lcom/squareup/protos/privacyvault/model/Entity;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    iget-object v1, p0, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$ProtoAdapter_VaultedDataEntry;->vaulted_fields:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->vaulted_fields:Ljava/util/Map;

    const/4 v3, 0x2

    .line 131
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    invoke-virtual {p1}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 121
    check-cast p1, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$ProtoAdapter_VaultedDataEntry;->encodedSize(Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;)Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;
    .locals 2

    .line 161
    invoke-virtual {p1}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;->newBuilder()Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;

    move-result-object p1

    .line 162
    iget-object v0, p1, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->entity:Lcom/squareup/protos/privacyvault/model/Entity;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/privacyvault/model/Entity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->entity:Lcom/squareup/protos/privacyvault/model/Entity;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/privacyvault/model/Entity;

    iput-object v0, p1, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->entity:Lcom/squareup/protos/privacyvault/model/Entity;

    .line 163
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->vaulted_fields:Ljava/util/Map;

    sget-object v1, Lcom/squareup/protos/privacyvault/service/Protected;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/Map;Lcom/squareup/wire/ProtoAdapter;)V

    .line 164
    invoke-virtual {p1}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 165
    invoke-virtual {p1}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$Builder;->build()Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 121
    check-cast p1, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/privacyvault/VaultedDataEntry$ProtoAdapter_VaultedDataEntry;->redact(Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;)Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;

    move-result-object p1

    return-object p1
.end method
