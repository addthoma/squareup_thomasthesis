.class final Lcom/squareup/protos/common/client/Product$ProtoAdapter_Product;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Product.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/client/Product;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Product"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/common/client/Product;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 322
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/common/client/Product;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/common/client/Product;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 349
    new-instance v0, Lcom/squareup/protos/common/client/Product$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/client/Product$Builder;-><init>()V

    .line 350
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 351
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 374
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 372
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/client/Product$Builder;->device_installation_id(Ljava/lang/String;)Lcom/squareup/protos/common/client/Product$Builder;

    goto :goto_0

    .line 371
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/client/Product$Builder;->device_tracking_enabled(Ljava/lang/String;)Lcom/squareup/protos/common/client/Product$Builder;

    goto :goto_0

    .line 370
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/client/Product$Builder;->device_advertising_id(Ljava/lang/String;)Lcom/squareup/protos/common/client/Product$Builder;

    goto :goto_0

    .line 364
    :pswitch_3
    :try_start_0
    sget-object v4, Lcom/squareup/protos/common/client/Product$BuildType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/common/client/Product$BuildType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/common/client/Product$Builder;->build_type(Lcom/squareup/protos/common/client/Product$BuildType;)Lcom/squareup/protos/common/client/Product$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 366
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/common/client/Product$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 361
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/common/client/Version;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/client/Version;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/client/Product$Builder;->version(Lcom/squareup/protos/common/client/Version;)Lcom/squareup/protos/common/client/Product$Builder;

    goto :goto_0

    .line 355
    :pswitch_5
    :try_start_1
    sget-object v4, Lcom/squareup/protos/common/client/Product$Application;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/common/client/Product$Application;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/common/client/Product$Builder;->application(Lcom/squareup/protos/common/client/Product$Application;)Lcom/squareup/protos/common/client/Product$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 357
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/common/client/Product$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 378
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/common/client/Product$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 379
    invoke-virtual {v0}, Lcom/squareup/protos/common/client/Product$Builder;->build()Lcom/squareup/protos/common/client/Product;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 320
    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/client/Product$ProtoAdapter_Product;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/common/client/Product;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/common/client/Product;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 338
    sget-object v0, Lcom/squareup/protos/common/client/Product$Application;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/client/Product;->application:Lcom/squareup/protos/common/client/Product$Application;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 339
    sget-object v0, Lcom/squareup/protos/common/client/Version;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/client/Product;->version:Lcom/squareup/protos/common/client/Version;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 340
    sget-object v0, Lcom/squareup/protos/common/client/Product$BuildType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/client/Product;->build_type:Lcom/squareup/protos/common/client/Product$BuildType;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 341
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/client/Product;->device_advertising_id:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 342
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/client/Product;->device_tracking_enabled:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 343
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/client/Product;->device_installation_id:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 344
    invoke-virtual {p2}, Lcom/squareup/protos/common/client/Product;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 320
    check-cast p2, Lcom/squareup/protos/common/client/Product;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/common/client/Product$ProtoAdapter_Product;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/common/client/Product;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/common/client/Product;)I
    .locals 4

    .line 327
    sget-object v0, Lcom/squareup/protos/common/client/Product$Application;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/common/client/Product;->application:Lcom/squareup/protos/common/client/Product$Application;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/client/Version;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/client/Product;->version:Lcom/squareup/protos/common/client/Version;

    const/4 v3, 0x2

    .line 328
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/client/Product$BuildType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/client/Product;->build_type:Lcom/squareup/protos/common/client/Product$BuildType;

    const/4 v3, 0x3

    .line 329
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/client/Product;->device_advertising_id:Ljava/lang/String;

    const/4 v3, 0x4

    .line 330
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/client/Product;->device_tracking_enabled:Ljava/lang/String;

    const/4 v3, 0x5

    .line 331
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/client/Product;->device_installation_id:Ljava/lang/String;

    const/4 v3, 0x6

    .line 332
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 333
    invoke-virtual {p1}, Lcom/squareup/protos/common/client/Product;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 320
    check-cast p1, Lcom/squareup/protos/common/client/Product;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/client/Product$ProtoAdapter_Product;->encodedSize(Lcom/squareup/protos/common/client/Product;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/common/client/Product;)Lcom/squareup/protos/common/client/Product;
    .locals 2

    .line 384
    invoke-virtual {p1}, Lcom/squareup/protos/common/client/Product;->newBuilder()Lcom/squareup/protos/common/client/Product$Builder;

    move-result-object p1

    .line 385
    iget-object v0, p1, Lcom/squareup/protos/common/client/Product$Builder;->version:Lcom/squareup/protos/common/client/Version;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/client/Version;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/common/client/Product$Builder;->version:Lcom/squareup/protos/common/client/Version;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/client/Version;

    iput-object v0, p1, Lcom/squareup/protos/common/client/Product$Builder;->version:Lcom/squareup/protos/common/client/Version;

    .line 386
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/common/client/Product$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 387
    invoke-virtual {p1}, Lcom/squareup/protos/common/client/Product$Builder;->build()Lcom/squareup/protos/common/client/Product;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 320
    check-cast p1, Lcom/squareup/protos/common/client/Product;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/client/Product$ProtoAdapter_Product;->redact(Lcom/squareup/protos/common/client/Product;)Lcom/squareup/protos/common/client/Product;

    move-result-object p1

    return-object p1
.end method
