.class public final Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ConnectedPeripheral.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/payment/ConnectedPeripheral;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/payment/ConnectedPeripheral;",
        "Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public connection_type:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

.field public device_type:Ljava/lang/String;

.field public manufacturer_name:Ljava/lang/String;

.field public model_name:Ljava/lang/String;

.field public unique_identifier:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 162
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/common/payment/ConnectedPeripheral;
    .locals 8

    .line 207
    new-instance v7, Lcom/squareup/protos/common/payment/ConnectedPeripheral;

    iget-object v1, p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->unique_identifier:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->device_type:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->connection_type:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    iget-object v4, p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->manufacturer_name:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->model_name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/common/payment/ConnectedPeripheral;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 151
    invoke-virtual {p0}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->build()Lcom/squareup/protos/common/payment/ConnectedPeripheral;

    move-result-object v0

    return-object v0
.end method

.method public connection_type(Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->connection_type:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    return-object p0
.end method

.method public device_type(Ljava/lang/String;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->device_type:Ljava/lang/String;

    return-object p0
.end method

.method public manufacturer_name(Ljava/lang/String;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->manufacturer_name:Ljava/lang/String;

    return-object p0
.end method

.method public model_name(Ljava/lang/String;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->model_name:Ljava/lang/String;

    return-object p0
.end method

.method public unique_identifier(Ljava/lang/String;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->unique_identifier:Ljava/lang/String;

    return-object p0
.end method
