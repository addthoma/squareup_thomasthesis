.class public final Lcom/squareup/protos/common/time/YearMonthDay$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "YearMonthDay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/time/YearMonthDay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "Lcom/squareup/protos/common/time/YearMonthDay$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public day_of_month:Ljava/lang/Integer;

.field public month_of_year:Ljava/lang/Integer;

.field public year:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 130
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 5

    .line 159
    new-instance v0, Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v1, p0, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->year:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->month_of_year:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->day_of_month:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/common/time/YearMonthDay;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 123
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->build()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    return-object v0
.end method

.method public day_of_month(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonthDay$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->day_of_month:Ljava/lang/Integer;

    return-object p0
.end method

.method public month_of_year(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonthDay$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->month_of_year:Ljava/lang/Integer;

    return-object p0
.end method

.method public year(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonthDay$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->year:Ljava/lang/Integer;

    return-object p0
.end method
