.class public final Lcom/squareup/protos/common/tipping/TipOption;
.super Lcom/squareup/wire/Message;
.source "TipOption.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/tipping/TipOption$ProtoAdapter_TipOption;,
        Lcom/squareup/protos/common/tipping/TipOption$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/common/tipping/TipOption;",
        "Lcom/squareup/protos/common/tipping/TipOption$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_IS_REMAINING_BALANCE:Ljava/lang/Boolean;

.field public static final DEFAULT_LABEL:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE:Ljava/lang/Double;

.field private static final serialVersionUID:J


# instance fields
.field public final is_remaining_balance:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final label:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final percentage:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#DOUBLE"
        tag = 0x3
    .end annotation
.end field

.field public final tip_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 31
    new-instance v0, Lcom/squareup/protos/common/tipping/TipOption$ProtoAdapter_TipOption;

    invoke-direct {v0}, Lcom/squareup/protos/common/tipping/TipOption$ProtoAdapter_TipOption;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 37
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/common/tipping/TipOption;->DEFAULT_PERCENTAGE:Ljava/lang/Double;

    const/4 v0, 0x0

    .line 39
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/common/tipping/TipOption;->DEFAULT_IS_REMAINING_BALANCE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/Double;Ljava/lang/Boolean;)V
    .locals 6

    .line 83
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/common/tipping/TipOption;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/Double;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/Double;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 88
    sget-object v0, Lcom/squareup/protos/common/tipping/TipOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 89
    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TipOption;->label:Ljava/lang/String;

    .line 90
    iput-object p2, p0, Lcom/squareup/protos/common/tipping/TipOption;->tip_money:Lcom/squareup/protos/common/Money;

    .line 91
    iput-object p3, p0, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    .line 92
    iput-object p4, p0, Lcom/squareup/protos/common/tipping/TipOption;->is_remaining_balance:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 109
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/common/tipping/TipOption;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 110
    :cond_1
    check-cast p1, Lcom/squareup/protos/common/tipping/TipOption;

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/common/tipping/TipOption;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/common/tipping/TipOption;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->label:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/tipping/TipOption;->label:Ljava/lang/String;

    .line 112
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->tip_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/common/tipping/TipOption;->tip_money:Lcom/squareup/protos/common/Money;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    iget-object v3, p1, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->is_remaining_balance:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/common/tipping/TipOption;->is_remaining_balance:Ljava/lang/Boolean;

    .line 115
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 120
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 122
    invoke-virtual {p0}, Lcom/squareup/protos/common/tipping/TipOption;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->label:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->tip_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Double;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->is_remaining_balance:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 127
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/common/tipping/TipOption$Builder;
    .locals 2

    .line 97
    new-instance v0, Lcom/squareup/protos/common/tipping/TipOption$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/tipping/TipOption$Builder;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->label:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/tipping/TipOption$Builder;->label:Ljava/lang/String;

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->tip_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/common/tipping/TipOption$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    iput-object v1, v0, Lcom/squareup/protos/common/tipping/TipOption$Builder;->percentage:Ljava/lang/Double;

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->is_remaining_balance:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/common/tipping/TipOption$Builder;->is_remaining_balance:Ljava/lang/Boolean;

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/common/tipping/TipOption;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/tipping/TipOption$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/protos/common/tipping/TipOption;->newBuilder()Lcom/squareup/protos/common/tipping/TipOption$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->label:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", label="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->tip_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", tip_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 137
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    if-eqz v1, :cond_2

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->percentage:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 138
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->is_remaining_balance:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", is_remaining_balance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TipOption;->is_remaining_balance:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "TipOption{"

    .line 139
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
