.class final Lcom/squareup/protos/common/time/YearMonth$ProtoAdapter_YearMonth;
.super Lcom/squareup/wire/ProtoAdapter;
.source "YearMonth.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/time/YearMonth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_YearMonth"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/common/time/YearMonth;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 141
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/common/time/YearMonth;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/common/time/YearMonth;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 160
    new-instance v0, Lcom/squareup/protos/common/time/YearMonth$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/YearMonth$Builder;-><init>()V

    .line 161
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 162
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 167
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 165
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/time/YearMonth$Builder;->month_of_year(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonth$Builder;

    goto :goto_0

    .line 164
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/time/YearMonth$Builder;->year(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonth$Builder;

    goto :goto_0

    .line 171
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/common/time/YearMonth$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 172
    invoke-virtual {v0}, Lcom/squareup/protos/common/time/YearMonth$Builder;->build()Lcom/squareup/protos/common/time/YearMonth;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 139
    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/time/YearMonth$ProtoAdapter_YearMonth;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/common/time/YearMonth;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/common/time/YearMonth;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 153
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/time/YearMonth;->year:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 154
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/time/YearMonth;->month_of_year:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 155
    invoke-virtual {p2}, Lcom/squareup/protos/common/time/YearMonth;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 139
    check-cast p2, Lcom/squareup/protos/common/time/YearMonth;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/common/time/YearMonth$ProtoAdapter_YearMonth;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/common/time/YearMonth;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/common/time/YearMonth;)I
    .locals 4

    .line 146
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/common/time/YearMonth;->year:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/time/YearMonth;->month_of_year:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 147
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 148
    invoke-virtual {p1}, Lcom/squareup/protos/common/time/YearMonth;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 139
    check-cast p1, Lcom/squareup/protos/common/time/YearMonth;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/time/YearMonth$ProtoAdapter_YearMonth;->encodedSize(Lcom/squareup/protos/common/time/YearMonth;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/common/time/YearMonth;)Lcom/squareup/protos/common/time/YearMonth;
    .locals 0

    .line 177
    invoke-virtual {p1}, Lcom/squareup/protos/common/time/YearMonth;->newBuilder()Lcom/squareup/protos/common/time/YearMonth$Builder;

    move-result-object p1

    .line 178
    invoke-virtual {p1}, Lcom/squareup/protos/common/time/YearMonth$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 179
    invoke-virtual {p1}, Lcom/squareup/protos/common/time/YearMonth$Builder;->build()Lcom/squareup/protos/common/time/YearMonth;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 139
    check-cast p1, Lcom/squareup/protos/common/time/YearMonth;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/time/YearMonth$ProtoAdapter_YearMonth;->redact(Lcom/squareup/protos/common/time/YearMonth;)Lcom/squareup/protos/common/time/YearMonth;

    move-result-object p1

    return-object p1
.end method
