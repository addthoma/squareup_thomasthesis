.class final Lcom/squareup/protos/common/countries/Country$ProtoAdapter_Country;
.super Lcom/squareup/wire/EnumAdapter;
.source "Country.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/countries/Country;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Country"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/common/countries/Country;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 802
    const-class v0, Lcom/squareup/protos/common/countries/Country;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/common/countries/Country;
    .locals 0

    .line 807
    invoke-static {p1}, Lcom/squareup/protos/common/countries/Country;->fromValue(I)Lcom/squareup/protos/common/countries/Country;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 800
    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/countries/Country$ProtoAdapter_Country;->fromValue(I)Lcom/squareup/protos/common/countries/Country;

    move-result-object p1

    return-object p1
.end method
