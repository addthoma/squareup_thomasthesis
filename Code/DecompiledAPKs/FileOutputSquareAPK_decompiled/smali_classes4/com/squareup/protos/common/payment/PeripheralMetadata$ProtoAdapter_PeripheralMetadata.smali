.class final Lcom/squareup/protos/common/payment/PeripheralMetadata$ProtoAdapter_PeripheralMetadata;
.super Lcom/squareup/wire/ProtoAdapter;
.source "PeripheralMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/payment/PeripheralMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PeripheralMetadata"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/common/payment/PeripheralMetadata;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 279
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/common/payment/PeripheralMetadata;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/common/payment/PeripheralMetadata;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 308
    new-instance v0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;-><init>()V

    .line 309
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 310
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 320
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 318
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->transaction_local_id(Ljava/lang/String;)Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;

    goto :goto_0

    .line 317
    :pswitch_1
    iget-object v3, v0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->unknown_peripherals:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 316
    :pswitch_2
    iget-object v3, v0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->connected_peripherals:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 315
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->source_device_installation_id(Ljava/lang/String;)Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;

    goto :goto_0

    .line 314
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->transaction_remote_id(Ljava/lang/String;)Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;

    goto :goto_0

    .line 313
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->app_version(Ljava/lang/String;)Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;

    goto :goto_0

    .line 312
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->app_release(Ljava/lang/String;)Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;

    goto :goto_0

    .line 324
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 325
    invoke-virtual {v0}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->build()Lcom/squareup/protos/common/payment/PeripheralMetadata;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 277
    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/payment/PeripheralMetadata$ProtoAdapter_PeripheralMetadata;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/common/payment/PeripheralMetadata;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/common/payment/PeripheralMetadata;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 296
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_release:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 297
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_version:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 298
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_remote_id:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 299
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_local_id:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 300
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/payment/PeripheralMetadata;->source_device_installation_id:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 301
    sget-object v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/common/payment/PeripheralMetadata;->connected_peripherals:Ljava/util/List;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 302
    sget-object v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/common/payment/PeripheralMetadata;->unknown_peripherals:Ljava/util/List;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 303
    invoke-virtual {p2}, Lcom/squareup/protos/common/payment/PeripheralMetadata;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 277
    check-cast p2, Lcom/squareup/protos/common/payment/PeripheralMetadata;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/common/payment/PeripheralMetadata$ProtoAdapter_PeripheralMetadata;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/common/payment/PeripheralMetadata;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/common/payment/PeripheralMetadata;)I
    .locals 4

    .line 284
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_release:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;->app_version:Ljava/lang/String;

    const/4 v3, 0x2

    .line 285
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_remote_id:Ljava/lang/String;

    const/4 v3, 0x3

    .line 286
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;->transaction_local_id:Ljava/lang/String;

    const/4 v3, 0x7

    .line 287
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;->source_device_installation_id:Ljava/lang/String;

    const/4 v3, 0x4

    .line 288
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 289
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;->connected_peripherals:Ljava/util/List;

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 290
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;->unknown_peripherals:Ljava/util/List;

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 291
    invoke-virtual {p1}, Lcom/squareup/protos/common/payment/PeripheralMetadata;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 277
    check-cast p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/payment/PeripheralMetadata$ProtoAdapter_PeripheralMetadata;->encodedSize(Lcom/squareup/protos/common/payment/PeripheralMetadata;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/common/payment/PeripheralMetadata;)Lcom/squareup/protos/common/payment/PeripheralMetadata;
    .locals 2

    .line 330
    invoke-virtual {p1}, Lcom/squareup/protos/common/payment/PeripheralMetadata;->newBuilder()Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;

    move-result-object p1

    .line 331
    iget-object v0, p1, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->connected_peripherals:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 332
    iget-object v0, p1, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->unknown_peripherals:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 333
    invoke-virtual {p1}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 334
    invoke-virtual {p1}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->build()Lcom/squareup/protos/common/payment/PeripheralMetadata;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 277
    check-cast p1, Lcom/squareup/protos/common/payment/PeripheralMetadata;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/payment/PeripheralMetadata$ProtoAdapter_PeripheralMetadata;->redact(Lcom/squareup/protos/common/payment/PeripheralMetadata;)Lcom/squareup/protos/common/payment/PeripheralMetadata;

    move-result-object p1

    return-object p1
.end method
