.class public final Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AllowConversationResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messagehub/service/AllowConversationResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/messagehub/service/AllowConversationResponse;",
        "Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow:Ljava/lang/Boolean;

.field public custom_issue_fields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/messagehub/service/CustomIssueField;",
            ">;"
        }
    .end annotation
.end field

.field public help_tip_subtitle:Ljava/lang/String;

.field public help_tip_title:Ljava/lang/String;

.field public view_title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 146
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 147
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->custom_issue_fields:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public allow(Ljava/lang/Boolean;)Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->allow:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/messagehub/service/AllowConversationResponse;
    .locals 8

    .line 181
    new-instance v7, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;

    iget-object v1, p0, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->allow:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->view_title:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->help_tip_title:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->help_tip_subtitle:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->custom_issue_fields:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 135
    invoke-virtual {p0}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->build()Lcom/squareup/protos/messagehub/service/AllowConversationResponse;

    move-result-object v0

    return-object v0
.end method

.method public custom_issue_fields(Ljava/util/List;)Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/messagehub/service/CustomIssueField;",
            ">;)",
            "Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;"
        }
    .end annotation

    .line 174
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 175
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->custom_issue_fields:Ljava/util/List;

    return-object p0
.end method

.method public help_tip_subtitle(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->help_tip_subtitle:Ljava/lang/String;

    return-object p0
.end method

.method public help_tip_title(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->help_tip_title:Ljava/lang/String;

    return-object p0
.end method

.method public view_title(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->view_title:Ljava/lang/String;

    return-object p0
.end method
