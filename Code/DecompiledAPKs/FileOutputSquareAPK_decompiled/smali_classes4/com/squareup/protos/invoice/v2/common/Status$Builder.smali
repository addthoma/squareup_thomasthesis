.class public final Lcom/squareup/protos/invoice/v2/common/Status$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Status.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/invoice/v2/common/Status;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/invoice/v2/common/Status;",
        "Lcom/squareup/protos/invoice/v2/common/Status$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public localized_description:Ljava/lang/String;

.field public localized_title:Ljava/lang/String;

.field public status_code:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

.field public success:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 129
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/invoice/v2/common/Status;
    .locals 7

    .line 154
    new-instance v6, Lcom/squareup/protos/invoice/v2/common/Status;

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status$Builder;->success:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/invoice/v2/common/Status$Builder;->localized_title:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/invoice/v2/common/Status$Builder;->localized_description:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/invoice/v2/common/Status$Builder;->status_code:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/invoice/v2/common/Status;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 120
    invoke-virtual {p0}, Lcom/squareup/protos/invoice/v2/common/Status$Builder;->build()Lcom/squareup/protos/invoice/v2/common/Status;

    move-result-object v0

    return-object v0
.end method

.method public localized_description(Ljava/lang/String;)Lcom/squareup/protos/invoice/v2/common/Status$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/common/Status$Builder;->localized_description:Ljava/lang/String;

    return-object p0
.end method

.method public localized_title(Ljava/lang/String;)Lcom/squareup/protos/invoice/v2/common/Status$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/common/Status$Builder;->localized_title:Ljava/lang/String;

    return-object p0
.end method

.method public status_code(Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;)Lcom/squareup/protos/invoice/v2/common/Status$Builder;
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/common/Status$Builder;->status_code:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/squareup/protos/invoice/v2/common/Status$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/common/Status$Builder;->success:Ljava/lang/Boolean;

    return-object p0
.end method
