.class public final Lcom/squareup/protos/xp/v1/Error$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Error.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/xp/v1/Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/xp/v1/Error;",
        "Lcom/squareup/protos/xp/v1/Error$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public field:Ljava/lang/String;

.field public localizable_message:Ljava/lang/String;

.field public message:Ljava/lang/String;

.field public type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 152
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/xp/v1/Error;
    .locals 7

    .line 204
    new-instance v6, Lcom/squareup/protos/xp/v1/Error;

    iget-object v1, p0, Lcom/squareup/protos/xp/v1/Error$Builder;->message:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/xp/v1/Error$Builder;->type:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/xp/v1/Error$Builder;->field:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/xp/v1/Error$Builder;->localizable_message:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/xp/v1/Error;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 143
    invoke-virtual {p0}, Lcom/squareup/protos/xp/v1/Error$Builder;->build()Lcom/squareup/protos/xp/v1/Error;

    move-result-object v0

    return-object v0
.end method

.method public field(Ljava/lang/String;)Lcom/squareup/protos/xp/v1/Error$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/xp/v1/Error$Builder;->field:Ljava/lang/String;

    return-object p0
.end method

.method public localizable_message(Ljava/lang/String;)Lcom/squareup/protos/xp/v1/Error$Builder;
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/squareup/protos/xp/v1/Error$Builder;->localizable_message:Ljava/lang/String;

    return-object p0
.end method

.method public message(Ljava/lang/String;)Lcom/squareup/protos/xp/v1/Error$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/xp/v1/Error$Builder;->message:Ljava/lang/String;

    return-object p0
.end method

.method public type(Ljava/lang/String;)Lcom/squareup/protos/xp/v1/Error$Builder;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/protos/xp/v1/Error$Builder;->type:Ljava/lang/String;

    return-object p0
.end method
