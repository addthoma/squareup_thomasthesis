.class final Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$ProtoAdapter_ListTransactionsRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ListTransactionsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ListTransactionsRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 424
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 451
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;-><init>()V

    .line 452
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 453
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 462
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 460
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->pagination_parameters(Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;

    goto :goto_0

    .line 459
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/transactionsfe/Query;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/transactionsfe/Query;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->query(Lcom/squareup/protos/transactionsfe/Query;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;

    goto :goto_0

    .line 458
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->date_range(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;

    goto :goto_0

    .line 457
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;

    goto :goto_0

    .line 456
    :pswitch_4
    iget-object v3, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->unit_token:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 455
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;

    goto :goto_0

    .line 466
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 467
    invoke-virtual {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 422
    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$ProtoAdapter_ListTransactionsRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 440
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 441
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->unit_token:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 442
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->employee_token:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 443
    sget-object v0, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 444
    sget-object v0, Lcom/squareup/protos/transactionsfe/Query;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->query:Lcom/squareup/protos/transactionsfe/Query;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 445
    sget-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->pagination_parameters:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 446
    invoke-virtual {p2}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 422
    check-cast p2, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$ProtoAdapter_ListTransactionsRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;)I
    .locals 4

    .line 429
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 430
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->unit_token:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->employee_token:Ljava/lang/String;

    const/4 v3, 0x3

    .line 431
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    const/4 v3, 0x4

    .line 432
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/transactionsfe/Query;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->query:Lcom/squareup/protos/transactionsfe/Query;

    const/4 v3, 0x5

    .line 433
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->pagination_parameters:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    const/4 v3, 0x6

    .line 434
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 435
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 422
    check-cast p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$ProtoAdapter_ListTransactionsRequest;->encodedSize(Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;
    .locals 2

    .line 472
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->newBuilder()Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;

    move-result-object p1

    .line 473
    iget-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/time/DateTimeInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTimeInterval;

    iput-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 474
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->query:Lcom/squareup/protos/transactionsfe/Query;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/transactionsfe/Query;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->query:Lcom/squareup/protos/transactionsfe/Query;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/transactionsfe/Query;

    iput-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->query:Lcom/squareup/protos/transactionsfe/Query;

    .line 475
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->pagination_parameters:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->pagination_parameters:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    iput-object v0, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->pagination_parameters:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    .line 476
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 477
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 422
    check-cast p1, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$ProtoAdapter_ListTransactionsRequest;->redact(Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    move-result-object p1

    return-object p1
.end method
