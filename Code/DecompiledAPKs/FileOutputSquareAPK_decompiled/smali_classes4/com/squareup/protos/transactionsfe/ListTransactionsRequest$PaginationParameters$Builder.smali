.class public final Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListTransactionsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cursor:Ljava/lang/String;

.field public effective_date_range:Lcom/squareup/protos/common/time/DateTimeInterval;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 347
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;
    .locals 4

    .line 372
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;->effective_date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iget-object v2, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;->cursor:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;-><init>(Lcom/squareup/protos/common/time/DateTimeInterval;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 342
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters;

    move-result-object v0

    return-object v0
.end method

.method public cursor(Ljava/lang/String;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;
    .locals 0

    .line 366
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;->cursor:Ljava/lang/String;

    return-object p0
.end method

.method public effective_date_range(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;
    .locals 0

    .line 356
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$PaginationParameters$Builder;->effective_date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    return-object p0
.end method
