.class public final Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InstrumentView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/InstrumentView$CardSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/deposits/InstrumentView$CardSummary;",
        "Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public pan_suffix:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 300
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public brand(Lcom/squareup/protos/simple_instrument_store/model/Brand;)Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;
    .locals 0

    .line 315
    iput-object p1, p0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/deposits/InstrumentView$CardSummary;
    .locals 4

    .line 321
    new-instance v0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;->pan_suffix:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;-><init>(Ljava/lang/String;Lcom/squareup/protos/simple_instrument_store/model/Brand;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 295
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;->build()Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    move-result-object v0

    return-object v0
.end method

.method public pan_suffix(Ljava/lang/String;)Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;
    .locals 0

    .line 307
    iput-object p1, p0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;->pan_suffix:Ljava/lang/String;

    return-object p0
.end method
