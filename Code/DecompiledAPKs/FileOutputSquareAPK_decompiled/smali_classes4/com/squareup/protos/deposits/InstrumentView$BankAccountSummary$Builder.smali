.class public final Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InstrumentView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;",
        "Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public account_number_suffix:Ljava/lang/String;

.field public bank_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 459
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public account_number_suffix(Ljava/lang/String;)Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;
    .locals 0

    .line 466
    iput-object p1, p0, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;->account_number_suffix:Ljava/lang/String;

    return-object p0
.end method

.method public bank_name(Ljava/lang/String;)Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;
    .locals 0

    .line 474
    iput-object p1, p0, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;->bank_name:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;
    .locals 4

    .line 480
    new-instance v0, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;->account_number_suffix:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;->bank_name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 454
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary$Builder;->build()Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    move-result-object v0

    return-object v0
.end method
