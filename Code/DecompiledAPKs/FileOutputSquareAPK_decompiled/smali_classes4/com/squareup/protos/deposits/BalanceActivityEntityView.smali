.class public final Lcom/squareup/protos/deposits/BalanceActivityEntityView;
.super Lcom/squareup/wire/Message;
.source "BalanceActivityEntityView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/deposits/BalanceActivityEntityView$ProtoAdapter_BalanceActivityEntityView;,
        Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/deposits/BalanceActivityEntityView;",
        "Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/deposits/BalanceActivityEntityView;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_UNIT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final instrument:Lcom/squareup/protos/deposits/InstrumentView;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.deposits.InstrumentView#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/deposits/BalanceActivityEntityView$ProtoAdapter_BalanceActivityEntityView;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/BalanceActivityEntityView$ProtoAdapter_BalanceActivityEntityView;-><init>()V

    sput-object v0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/deposits/InstrumentView;Ljava/lang/String;)V
    .locals 1

    .line 48
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/deposits/BalanceActivityEntityView;-><init>(Lcom/squareup/protos/deposits/InstrumentView;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/deposits/InstrumentView;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 53
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 54
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p3

    const/4 v0, 0x1

    if-gt p3, v0, :cond_0

    .line 57
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->instrument:Lcom/squareup/protos/deposits/InstrumentView;

    .line 58
    iput-object p2, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->unit_token:Ljava/lang/String;

    return-void

    .line 55
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of instrument, unit_token may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 73
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 74
    :cond_1
    check-cast p1, Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->instrument:Lcom/squareup/protos/deposits/InstrumentView;

    iget-object v3, p1, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->instrument:Lcom/squareup/protos/deposits/InstrumentView;

    .line 76
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->unit_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->unit_token:Ljava/lang/String;

    .line 77
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 82
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->instrument:Lcom/squareup/protos/deposits/InstrumentView;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/deposits/InstrumentView;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 87
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;
    .locals 2

    .line 63
    new-instance v0, Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;-><init>()V

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->instrument:Lcom/squareup/protos/deposits/InstrumentView;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;->instrument:Lcom/squareup/protos/deposits/InstrumentView;

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;->unit_token:Ljava/lang/String;

    .line 66
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->newBuilder()Lcom/squareup/protos/deposits/BalanceActivityEntityView$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->instrument:Lcom/squareup/protos/deposits/InstrumentView;

    if-eqz v1, :cond_0

    const-string v1, ", instrument="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->instrument:Lcom/squareup/protos/deposits/InstrumentView;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 96
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BalanceActivityEntityView{"

    .line 97
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
