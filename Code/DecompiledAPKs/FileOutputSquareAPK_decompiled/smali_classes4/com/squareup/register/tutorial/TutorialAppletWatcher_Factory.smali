.class public final Lcom/squareup/register/tutorial/TutorialAppletWatcher_Factory;
.super Ljava/lang/Object;
.source "TutorialAppletWatcher_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/register/tutorial/TutorialAppletWatcher;",
        ">;"
    }
.end annotation


# instance fields
.field private final selectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/register/tutorial/TutorialAppletWatcher_Factory;->selectionProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/register/tutorial/TutorialAppletWatcher_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/register/tutorial/TutorialAppletWatcher_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)",
            "Lcom/squareup/register/tutorial/TutorialAppletWatcher_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/register/tutorial/TutorialAppletWatcher_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/register/tutorial/TutorialAppletWatcher_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/applet/AppletSelection;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/register/tutorial/TutorialAppletWatcher;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/register/tutorial/TutorialAppletWatcher;

    invoke-direct {v0, p0, p1}, Lcom/squareup/register/tutorial/TutorialAppletWatcher;-><init>(Lcom/squareup/applet/AppletSelection;Lcom/squareup/tutorialv2/TutorialCore;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/register/tutorial/TutorialAppletWatcher;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialAppletWatcher_Factory;->selectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletSelection;

    iget-object v1, p0, Lcom/squareup/register/tutorial/TutorialAppletWatcher_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tutorialv2/TutorialCore;

    invoke-static {v0, v1}, Lcom/squareup/register/tutorial/TutorialAppletWatcher_Factory;->newInstance(Lcom/squareup/applet/AppletSelection;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/register/tutorial/TutorialAppletWatcher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/TutorialAppletWatcher_Factory;->get()Lcom/squareup/register/tutorial/TutorialAppletWatcher;

    move-result-object v0

    return-object v0
.end method
