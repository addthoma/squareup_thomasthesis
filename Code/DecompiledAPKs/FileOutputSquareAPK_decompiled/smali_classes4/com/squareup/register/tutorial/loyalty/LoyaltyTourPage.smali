.class public final Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;
.super Ljava/lang/Object;
.source "LoyaltyTourPage.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u001b\u0008\u00c6\u0002\u0018\u00002\u00020\u0001:\u0001\u001fB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0017\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007R\u0014\u0010\u0008\u001a\u00020\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\nR\u0014\u0010\r\u001a\u00020\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\nR\u0017\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0007R\u0014\u0010\u0011\u001a\u00020\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\nR\u0014\u0010\u0013\u001a\u00020\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\nR\u0014\u0010\u0015\u001a\u00020\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\nR\u0017\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0007R\u0014\u0010\u0019\u001a\u00020\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\nR\u0014\u0010\u001b\u001a\u00020\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001c\u0010\nR\u0014\u0010\u001d\u001a\u00020\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001e\u0010\n\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;",
        "",
        "()V",
        "adjustPointsPages",
        "",
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;",
        "getAdjustPointsPages",
        "()Ljava/util/List;",
        "adjustPointsStepOne",
        "getAdjustPointsStepOne",
        "()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;",
        "adjustPointsStepThree",
        "getAdjustPointsStepThree",
        "adjustPointsStepTwo",
        "getAdjustPointsStepTwo",
        "enrollmentPages",
        "getEnrollmentPages",
        "enrollmentStepOne",
        "getEnrollmentStepOne",
        "enrollmentStepThree",
        "getEnrollmentStepThree",
        "enrollmentStepTwo",
        "getEnrollmentStepTwo",
        "redeemRewardsPages",
        "getRedeemRewardsPages",
        "redeemRewardsStepOne",
        "getRedeemRewardsStepOne",
        "redeemRewardsStepThree",
        "getRedeemRewardsStepThree",
        "redeemRewardsStepTwo",
        "getRedeemRewardsStepTwo",
        "ScreenData",
        "pos-tutorials_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    new-instance v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;

    invoke-direct {v0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;-><init>()V

    sput-object v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;->INSTANCE:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final getAdjustPointsStepOne()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;
    .locals 4

    .line 44
    new-instance v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    sget v1, Lcom/squareup/pos/tutorials/R$drawable;->rotated_chip_reader_and_card:I

    .line 45
    sget v2, Lcom/squareup/pos/tutorials/R$string;->loyalty_adjust_points_step_one_title:I

    .line 46
    sget v3, Lcom/squareup/pos/tutorials/R$string;->loyalty_adjust_points_step_one_description:I

    .line 44
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;-><init>(III)V

    return-object v0
.end method

.method private final getAdjustPointsStepThree()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;
    .locals 4

    .line 54
    new-instance v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    sget v1, Lcom/squareup/pos/tutorials/R$drawable;->rotated_chip_reader_and_card:I

    .line 55
    sget v2, Lcom/squareup/pos/tutorials/R$string;->loyalty_adjust_points_step_three_title:I

    .line 56
    sget v3, Lcom/squareup/pos/tutorials/R$string;->loyalty_adjust_points_step_three_description:I

    .line 54
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;-><init>(III)V

    return-object v0
.end method

.method private final getAdjustPointsStepTwo()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;
    .locals 4

    .line 49
    new-instance v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    sget v1, Lcom/squareup/pos/tutorials/R$drawable;->rotated_chip_reader_and_card:I

    .line 50
    sget v2, Lcom/squareup/pos/tutorials/R$string;->loyalty_adjust_points_step_two_title:I

    .line 51
    sget v3, Lcom/squareup/pos/tutorials/R$string;->loyalty_adjust_points_step_two_description:I

    .line 49
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;-><init>(III)V

    return-object v0
.end method

.method private final getEnrollmentStepOne()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;
    .locals 4

    .line 24
    new-instance v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    sget v1, Lcom/squareup/pos/tutorials/R$drawable;->rotated_chip_reader_and_card:I

    .line 25
    sget v2, Lcom/squareup/pos/tutorials/R$string;->loyalty_enroll_step_one_title:I

    sget v3, Lcom/squareup/pos/tutorials/R$string;->loyalty_enroll_step_one_description:I

    .line 24
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;-><init>(III)V

    return-object v0
.end method

.method private final getEnrollmentStepThree()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;
    .locals 4

    .line 32
    new-instance v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    sget v1, Lcom/squareup/pos/tutorials/R$drawable;->rotated_chip_reader_and_card:I

    .line 33
    sget v2, Lcom/squareup/pos/tutorials/R$string;->loyalty_enroll_step_three_title:I

    sget v3, Lcom/squareup/pos/tutorials/R$string;->loyalty_enroll_step_three_description:I

    .line 32
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;-><init>(III)V

    return-object v0
.end method

.method private final getEnrollmentStepTwo()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;
    .locals 4

    .line 28
    new-instance v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    sget v1, Lcom/squareup/pos/tutorials/R$drawable;->rotated_chip_reader_and_card:I

    .line 29
    sget v2, Lcom/squareup/pos/tutorials/R$string;->loyalty_enroll_step_two_title:I

    sget v3, Lcom/squareup/pos/tutorials/R$string;->loyalty_enroll_step_two_description:I

    .line 28
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;-><init>(III)V

    return-object v0
.end method

.method private final getRedeemRewardsStepOne()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;
    .locals 4

    .line 62
    new-instance v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    sget v1, Lcom/squareup/pos/tutorials/R$drawable;->rotated_chip_reader_and_card:I

    .line 63
    sget v2, Lcom/squareup/pos/tutorials/R$string;->loyalty_redeem_rewards_step_one_title:I

    .line 64
    sget v3, Lcom/squareup/pos/tutorials/R$string;->loyalty_redeem_rewards_step_one_description:I

    .line 62
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;-><init>(III)V

    return-object v0
.end method

.method private final getRedeemRewardsStepThree()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;
    .locals 4

    .line 72
    new-instance v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    sget v1, Lcom/squareup/pos/tutorials/R$drawable;->rotated_chip_reader_and_card:I

    .line 73
    sget v2, Lcom/squareup/pos/tutorials/R$string;->loyalty_redeem_rewards_step_three_title:I

    .line 74
    sget v3, Lcom/squareup/pos/tutorials/R$string;->loyalty_redeem_rewards_step_three_description:I

    .line 72
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;-><init>(III)V

    return-object v0
.end method

.method private final getRedeemRewardsStepTwo()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;
    .locals 4

    .line 67
    new-instance v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    sget v1, Lcom/squareup/pos/tutorials/R$drawable;->rotated_chip_reader_and_card:I

    .line 68
    sget v2, Lcom/squareup/pos/tutorials/R$string;->loyalty_enroll_step_two_title:I

    .line 69
    sget v3, Lcom/squareup/pos/tutorials/R$string;->loyalty_redeem_rewards_step_two_description:I

    .line 67
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;-><init>(III)V

    return-object v0
.end method


# virtual methods
.method public final getAdjustPointsPages()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    .line 41
    invoke-direct {p0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;->getAdjustPointsStepOne()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-direct {p0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;->getAdjustPointsStepTwo()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-direct {p0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;->getAdjustPointsStepThree()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string v1, "asList(adjustPointsStepO\u2026o, adjustPointsStepThree)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getEnrollmentPages()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    .line 21
    invoke-direct {p0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;->getEnrollmentStepOne()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-direct {p0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;->getEnrollmentStepTwo()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-direct {p0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;->getEnrollmentStepThree()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string v1, "asList(enrollmentStepOne\u2026Two, enrollmentStepThree)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getRedeemRewardsPages()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    .line 59
    invoke-direct {p0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;->getRedeemRewardsStepOne()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-direct {p0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;->getRedeemRewardsStepTwo()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-direct {p0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage;->getRedeemRewardsStepThree()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string v1, "asList(redeemRewardsStep\u2026, redeemRewardsStepThree)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
