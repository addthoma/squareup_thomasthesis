.class public final Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;
.super Ljava/lang/Object;
.source "ReaderActionFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/register/tutorial/ReaderActionFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceTenderSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/InvoiceTenderSetting;",
            ">;"
        }
    .end annotation
.end field

.field private final resourcesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderSettingsManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderSettingsManager;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderSettingsManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/InvoiceTenderSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p2, p0, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;->resourcesProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p3, p0, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p4, p0, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;->tenderSettingsManagerProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p5, p0, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p6, p0, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;->invoiceTenderSettingProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p7, p0, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderSettingsManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/InvoiceTenderSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)",
            "Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;"
        }
    .end annotation

    .line 63
    new-instance v8, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/payment/Transaction;Landroid/content/res/Resources;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;Lcom/squareup/CountryCode;)Lcom/squareup/register/tutorial/ReaderActionFactory;
    .locals 9

    .line 70
    new-instance v8, Lcom/squareup/register/tutorial/ReaderActionFactory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/register/tutorial/ReaderActionFactory;-><init>(Lcom/squareup/payment/Transaction;Landroid/content/res/Resources;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;Lcom/squareup/CountryCode;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/register/tutorial/ReaderActionFactory;
    .locals 8

    .line 54
    iget-object v0, p0, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;->resourcesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;->tenderSettingsManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/tenderpayment/TenderSettingsManager;

    iget-object v0, p0, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/crm/CustomerManagementSettings;

    iget-object v0, p0, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;->invoiceTenderSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/tenderpayment/InvoiceTenderSetting;

    iget-object v0, p0, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/CountryCode;

    invoke-static/range {v1 .. v7}, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;->newInstance(Lcom/squareup/payment/Transaction;Landroid/content/res/Resources;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;Lcom/squareup/CountryCode;)Lcom/squareup/register/tutorial/ReaderActionFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/ReaderActionFactory_Factory;->get()Lcom/squareup/register/tutorial/ReaderActionFactory;

    move-result-object v0

    return-object v0
.end method
