.class public Lcom/squareup/register/tutorial/TutorialCogsHelper;
.super Ljava/lang/Object;
.source "TutorialCogsHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u0016\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u000e\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fH\u0002J\u000e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fH\u0016R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/register/tutorial/TutorialCogsHelper;",
        "",
        "cogs",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/cogs/Cogs;",
        "jailKeeper",
        "Lcom/squareup/jailkeeper/JailKeeper;",
        "bus",
        "Lcom/squareup/badbus/BadBus;",
        "(Ljavax/inject/Provider;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/badbus/BadBus;)V",
        "getBus",
        "()Lcom/squareup/badbus/BadBus;",
        "getJailKeeper",
        "()Lcom/squareup/jailkeeper/JailKeeper;",
        "checkJailKeeper",
        "Lio/reactivex/Observable;",
        "",
        "hasItems",
        "pos-tutorials_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final cogs:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/badbus/BadBus;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            "Lcom/squareup/badbus/BadBus;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cogs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jailKeeper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bus"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/register/tutorial/TutorialCogsHelper;->cogs:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/register/tutorial/TutorialCogsHelper;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    iput-object p3, p0, Lcom/squareup/register/tutorial/TutorialCogsHelper;->bus:Lcom/squareup/badbus/BadBus;

    return-void
.end method

.method public static final synthetic access$getCogs$p(Lcom/squareup/register/tutorial/TutorialCogsHelper;)Ljavax/inject/Provider;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/register/tutorial/TutorialCogsHelper;->cogs:Ljavax/inject/Provider;

    return-object p0
.end method

.method private final checkJailKeeper()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialCogsHelper;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    invoke-interface {v0}, Lcom/squareup/jailkeeper/JailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v0

    sget-object v1, Lcom/squareup/jailkeeper/JailKeeper$State;->READY:Lcom/squareup/jailkeeper/JailKeeper$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 51
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "just(true)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialCogsHelper;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/jailkeeper/JailKeeper$State;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    .line 54
    sget-object v1, Lcom/squareup/register/tutorial/TutorialCogsHelper$checkJailKeeper$1;->INSTANCE:Lcom/squareup/register/tutorial/TutorialCogsHelper$checkJailKeeper$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "bus.events(JailKeeper.St\u2026ate -> state === READY })"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method


# virtual methods
.method public final getBus()Lcom/squareup/badbus/BadBus;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialCogsHelper;->bus:Lcom/squareup/badbus/BadBus;

    return-object v0
.end method

.method public final getJailKeeper()Lcom/squareup/jailkeeper/JailKeeper;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialCogsHelper;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    return-object v0
.end method

.method public hasItems()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Lcom/squareup/register/tutorial/TutorialCogsHelper;->checkJailKeeper()Lio/reactivex/Observable;

    move-result-object v0

    .line 38
    sget-object v1, Lcom/squareup/register/tutorial/TutorialCogsHelper$hasItems$1;->INSTANCE:Lcom/squareup/register/tutorial/TutorialCogsHelper$hasItems$1;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 39
    new-instance v1, Lcom/squareup/register/tutorial/TutorialCogsHelper$hasItems$2;

    invoke-direct {v1, p0}, Lcom/squareup/register/tutorial/TutorialCogsHelper$hasItems$2;-><init>(Lcom/squareup/register/tutorial/TutorialCogsHelper;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 46
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "checkJailKeeper()\n      \u2026       }\n        .take(1)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
