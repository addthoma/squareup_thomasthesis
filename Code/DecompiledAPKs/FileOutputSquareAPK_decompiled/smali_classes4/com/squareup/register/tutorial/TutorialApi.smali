.class public abstract Lcom/squareup/register/tutorial/TutorialApi;
.super Ljava/lang/Object;
.source "TutorialApi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/tutorial/TutorialApi$NoOpTutorialApi;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract forceStartFirstPaymentTutorial()V
.end method

.method public abstract maybeAutoStartFirstPaymentTutorial()V
.end method

.method public abstract maybeStartFirstPaymentTutorialForDeepLink()Lcom/squareup/ui/main/HistoryFactory;
.end method

.method public onFinishedReceipt()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method
