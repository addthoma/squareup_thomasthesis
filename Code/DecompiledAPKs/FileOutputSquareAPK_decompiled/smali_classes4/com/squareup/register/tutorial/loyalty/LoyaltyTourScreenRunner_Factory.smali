.class public final Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;
.super Ljava/lang/Object;
.source "LoyaltyTourScreenRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final adjustPointsTutorialViewedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final badgeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpBadge;",
            ">;"
        }
    .end annotation
.end field

.field private final enrollLoyaltyTutorialViewedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final redeemRewardsTutorialViewedProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpBadge;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;->enrollLoyaltyTutorialViewedProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;->adjustPointsTutorialViewedProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;->redeemRewardsTutorialViewedProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;->badgeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpBadge;",
            ">;)",
            "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;"
        }
    .end annotation

    .line 57
    new-instance v7, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/help/HelpBadge;)Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/ui/help/HelpBadge;",
            ")",
            "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;"
        }
    .end annotation

    .line 64
    new-instance v7, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;-><init>(Lflow/Flow;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/help/HelpBadge;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;
    .locals 7

    .line 48
    iget-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;->enrollLoyaltyTutorialViewedProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;->adjustPointsTutorialViewedProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;->redeemRewardsTutorialViewedProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;->badgeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/help/HelpBadge;

    invoke-static/range {v1 .. v6}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;->newInstance(Lflow/Flow;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/help/HelpBadge;)Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner_Factory;->get()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;

    move-result-object v0

    return-object v0
.end method
