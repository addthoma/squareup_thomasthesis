.class public final Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;
.super Ljava/lang/Object;
.source "FirstPaymentTutorialTextRenderer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final readerActionFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/ReaderActionFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/ReaderActionFactory;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;->contextProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;->resProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p6, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;->readerActionFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/ReaderActionFactory;",
            ">;)",
            "Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;"
        }
    .end annotation

    .line 57
    new-instance v7, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/register/tutorial/ReaderActionFactory;)Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/register/tutorial/ReaderActionFactory;",
            ")",
            "Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;"
        }
    .end annotation

    .line 63
    new-instance v7, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;-><init>(Landroid/app/Application;Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/register/tutorial/ReaderActionFactory;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;
    .locals 7

    .line 49
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;->readerActionFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/register/tutorial/ReaderActionFactory;

    invoke-static/range {v1 .. v6}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/register/tutorial/ReaderActionFactory;)Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer_Factory;->get()Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    move-result-object v0

    return-object v0
.end method
