.class public final Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "RealNohoDatePickerDialogWorkflow.kt"

# interfaces
.implements Lcom/squareup/register/widgets/NohoDatePickerDialogWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/register/widgets/NohoDatePickerDialogProps;",
        "Lcom/squareup/register/widgets/NohoDatePickerDialogOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/register/widgets/NohoDatePickerDialogWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealNohoDatePickerDialogWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealNohoDatePickerDialogWorkflow.kt\ncom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,52:1\n149#2,5:53\n*E\n*S KotlinDebug\n*F\n+ 1 RealNohoDatePickerDialogWorkflow.kt\ncom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow\n*L\n38#1,5:53\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u00020\u000126\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0002B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\nJ$\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00040\u000c2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002JF\u0010\u0013\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0014\u001a\u00020\u00032\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00040\u0016H\u0016R\u001a\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00040\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;",
        "Lcom/squareup/register/widgets/NohoDatePickerDialogWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/register/widgets/NohoDatePickerDialogProps;",
        "Lcom/squareup/register/widgets/NohoDatePickerDialogOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "()V",
        "removeAction",
        "Lcom/squareup/workflow/WorkflowAction;",
        "",
        "changeAction",
        "date",
        "Lorg/threeten/bp/LocalDate;",
        "yearEnabled",
        "",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final removeAction:Lcom/squareup/workflow/WorkflowAction;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    .line 48
    sget-object v0, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow$removeAction$1;->INSTANCE:Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow$removeAction$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v1, v0, v2, v1}, Lcom/squareup/workflow/StatelessWorkflowKt;->action$default(Lcom/squareup/workflow/StatelessWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;->removeAction:Lcom/squareup/workflow/WorkflowAction;

    return-void
.end method

.method public static final synthetic access$changeAction(Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;Lorg/threeten/bp/LocalDate;Z)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;->changeAction(Lorg/threeten/bp/LocalDate;Z)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getRemoveAction$p(Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;->removeAction:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method

.method private final changeAction(Lorg/threeten/bp/LocalDate;Z)Lcom/squareup/workflow/WorkflowAction;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow$changeAction$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow$changeAction$1;-><init>(Lorg/threeten/bp/LocalDate;Z)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 p2, 0x1

    invoke-static {p0, p1, v0, p2, p1}, Lcom/squareup/workflow/StatelessWorkflowKt;->action$default(Lcom/squareup/workflow/StatelessWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;->render(Lcom/squareup/register/widgets/NohoDatePickerDialogProps;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/register/widgets/NohoDatePickerDialogProps;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/register/widgets/NohoDatePickerDialogProps;",
            "Lcom/squareup/workflow/RenderContext;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    new-instance v0, Lcom/squareup/register/widgets/NohoDatePickerDialogScreen;

    .line 29
    new-instance v1, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow$render$1;

    invoke-direct {v1, p0, p2}, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow$render$1;-><init>(Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    .line 32
    new-instance v2, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow$render$2;

    invoke-direct {v2, p0, p2}, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow$render$2;-><init>(Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 35
    new-instance v3, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow$render$3;

    invoke-direct {v3, p0, p2, p1}, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow$render$3;-><init>(Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/register/widgets/NohoDatePickerDialogProps;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 27
    invoke-direct {v0, p1, v1, v2, v3}, Lcom/squareup/register/widgets/NohoDatePickerDialogScreen;-><init>(Lcom/squareup/register/widgets/NohoDatePickerDialogProps;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 54
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 55
    const-class p2, Lcom/squareup/register/widgets/NohoDatePickerDialogScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 56
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 54
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 38
    sget-object p2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method
