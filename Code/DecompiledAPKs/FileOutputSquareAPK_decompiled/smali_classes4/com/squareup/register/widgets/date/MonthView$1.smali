.class Lcom/squareup/register/widgets/date/MonthView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "MonthView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/date/MonthView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/date/MonthView;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/date/MonthView;)V
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/register/widgets/date/MonthView$1;->this$0:Lcom/squareup/register/widgets/date/MonthView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 3

    .line 121
    iget-object v0, p0, Lcom/squareup/register/widgets/date/MonthView$1;->this$0:Lcom/squareup/register/widgets/date/MonthView;

    invoke-static {v0}, Lcom/squareup/register/widgets/date/MonthView;->access$000(Lcom/squareup/register/widgets/date/MonthView;)Lcom/squareup/register/widgets/date/MonthView$SelectionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 122
    check-cast p1, Landroid/widget/TextView;

    .line 123
    invoke-virtual {p1}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/date/MonthView$CellDay;

    .line 124
    iget-object v0, p0, Lcom/squareup/register/widgets/date/MonthView$1;->this$0:Lcom/squareup/register/widgets/date/MonthView;

    invoke-static {v0}, Lcom/squareup/register/widgets/date/MonthView;->access$000(Lcom/squareup/register/widgets/date/MonthView;)Lcom/squareup/register/widgets/date/MonthView$SelectionListener;

    move-result-object v0

    iget v1, p1, Lcom/squareup/register/widgets/date/MonthView$CellDay;->year:I

    iget v2, p1, Lcom/squareup/register/widgets/date/MonthView$CellDay;->month:I

    iget p1, p1, Lcom/squareup/register/widgets/date/MonthView$CellDay;->day:I

    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/register/widgets/date/MonthView$SelectionListener;->onDaySelected(III)V

    :cond_0
    return-void
.end method
