.class Lcom/squareup/register/widgets/card/ExpirationEditor$1;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "ExpirationEditor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/card/ExpirationEditor;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/card/ExpirationEditor;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/card/ExpirationEditor;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/squareup/register/widgets/card/ExpirationEditor$1;->this$0:Lcom/squareup/register/widgets/card/ExpirationEditor;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x5

    if-ne p2, p1, :cond_1

    .line 59
    iget-object p1, p0, Lcom/squareup/register/widgets/card/ExpirationEditor$1;->this$0:Lcom/squareup/register/widgets/card/ExpirationEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/ExpirationEditor;->access$000(Lcom/squareup/register/widgets/card/ExpirationEditor;)V

    .line 60
    iget-object p1, p0, Lcom/squareup/register/widgets/card/ExpirationEditor$1;->this$0:Lcom/squareup/register/widgets/card/ExpirationEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/ExpirationEditor;->access$100(Lcom/squareup/register/widgets/card/ExpirationEditor;)Lcom/squareup/register/widgets/card/PanValidationStrategy;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/register/widgets/card/ExpirationEditor$1;->this$0:Lcom/squareup/register/widgets/card/ExpirationEditor;

    invoke-virtual {p2}, Lcom/squareup/register/widgets/card/ExpirationEditor;->extractCardExpiration()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->expirationValid(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 61
    iget-object p1, p0, Lcom/squareup/register/widgets/card/ExpirationEditor$1;->this$0:Lcom/squareup/register/widgets/card/ExpirationEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/ExpirationEditor;->access$200(Lcom/squareup/register/widgets/card/ExpirationEditor;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 63
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/widgets/card/ExpirationEditor$1;->this$0:Lcom/squareup/register/widgets/card/ExpirationEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/ExpirationEditor;->access$300(Lcom/squareup/register/widgets/card/ExpirationEditor;)Lcom/squareup/register/widgets/card/CardEditorEditText;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->onInvalidContent()V

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method
