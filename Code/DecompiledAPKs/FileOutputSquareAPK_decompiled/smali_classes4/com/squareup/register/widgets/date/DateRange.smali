.class final Lcom/squareup/register/widgets/date/DateRange;
.super Ljava/lang/Object;
.source "DateRange.java"


# instance fields
.field private final end:Ljava/util/Calendar;

.field private final start:Ljava/util/Calendar;


# direct methods
.method constructor <init>(Ljava/util/Calendar;Ljava/util/Calendar;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/register/widgets/date/DateRange;->start:Ljava/util/Calendar;

    .line 15
    iput-object p2, p0, Lcom/squareup/register/widgets/date/DateRange;->end:Ljava/util/Calendar;

    return-void
.end method


# virtual methods
.method getEndCalendar()Ljava/util/Calendar;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DateRange;->end:Ljava/util/Calendar;

    return-object v0
.end method

.method getEndDate()Ljava/util/Date;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DateRange;->end:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method getStartCalendar()Ljava/util/Calendar;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DateRange;->start:Ljava/util/Calendar;

    return-object v0
.end method

.method getStartDate()Ljava/util/Date;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DateRange;->start:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method toMonths()I
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DateRange;->end:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v2, p0, Lcom/squareup/register/widgets/date/DateRange;->start:Ljava/util/Calendar;

    invoke-virtual {v2, v1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    sub-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0xc

    iget-object v2, p0, Lcom/squareup/register/widgets/date/DateRange;->end:Ljava/util/Calendar;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    return v0
.end method
