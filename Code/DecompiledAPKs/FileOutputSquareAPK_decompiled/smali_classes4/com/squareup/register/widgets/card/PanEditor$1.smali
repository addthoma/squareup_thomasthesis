.class Lcom/squareup/register/widgets/card/PanEditor$1;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "PanEditor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/card/PanEditor;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/card/PanEditor;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/card/PanEditor;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor$1;->this$0:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 p1, 0x5

    if-ne p2, p1, :cond_1

    .line 77
    iget-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor$1;->this$0:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/PanEditor;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Strings;->removeSpaces(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 78
    iget-object p2, p0, Lcom/squareup/register/widgets/card/PanEditor$1;->this$0:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-static {p2}, Lcom/squareup/register/widgets/card/PanEditor;->access$000(Lcom/squareup/register/widgets/card/PanEditor;)Lcom/squareup/register/widgets/card/PanValidationStrategy;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->getBrand(Ljava/lang/String;)Lcom/squareup/Card$Brand;

    move-result-object p2

    .line 79
    iget-object p3, p0, Lcom/squareup/register/widgets/card/PanEditor$1;->this$0:Lcom/squareup/register/widgets/card/PanEditor;

    .line 80
    invoke-static {p3}, Lcom/squareup/register/widgets/card/PanEditor;->access$000(Lcom/squareup/register/widgets/card/PanEditor;)Lcom/squareup/register/widgets/card/PanValidationStrategy;

    move-result-object p3

    const/4 v0, 0x0

    invoke-interface {p3, p1, p2, v0}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->determineCardNumberState(Ljava/lang/String;Lcom/squareup/Card$Brand;Z)Lcom/squareup/register/widgets/card/CardNumberState;

    move-result-object p1

    .line 81
    sget-object p2, Lcom/squareup/register/widgets/card/CardNumberState;->VALID:Lcom/squareup/register/widgets/card/CardNumberState;

    if-ne p1, p2, :cond_0

    .line 82
    iget-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor$1;->this$0:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/PanEditor;->access$100(Lcom/squareup/register/widgets/card/PanEditor;)Lcom/squareup/register/widgets/card/OnPanListener;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 83
    iget-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor$1;->this$0:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/PanEditor;->getCard()Lcom/squareup/Card;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 85
    iget-object p2, p0, Lcom/squareup/register/widgets/card/PanEditor$1;->this$0:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-static {p2}, Lcom/squareup/register/widgets/card/PanEditor;->access$100(Lcom/squareup/register/widgets/card/PanEditor;)Lcom/squareup/register/widgets/card/OnPanListener;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/squareup/register/widgets/card/OnPanListener;->onNext(Lcom/squareup/Card;)V

    goto :goto_0

    .line 89
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor$1;->this$0:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/PanEditor;->access$200(Lcom/squareup/register/widgets/card/PanEditor;)Landroid/view/animation/Animation;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/card/PanEditor;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method
