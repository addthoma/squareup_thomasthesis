.class public final Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;
.super Ljava/lang/Object;
.source "NohoDurationPickerDialogScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DialogBuilder"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoDurationPickerDialogScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoDurationPickerDialogScreen.kt\ncom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,109:1\n66#2:110\n*E\n*S KotlinDebug\n*F\n+ 1 NohoDurationPickerDialogScreen.kt\ncom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder\n*L\n51#1:110\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0011\u001a\u00020\u0012R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0005\u001a\u00020\u00068\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\"\u0004\u0008\t\u0010\nR\u001e\u0010\u000b\u001a\u00020\u000c8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;",
        "",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "res",
        "Lcom/squareup/util/Res;",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "setRes",
        "(Lcom/squareup/util/Res;)V",
        "runner",
        "Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
        "getRunner",
        "()Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
        "setRunner",
        "(Lcom/squareup/register/widgets/NohoDurationPickerRunner;)V",
        "build",
        "Landroid/app/Dialog;",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field public res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public runner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->context:Landroid/content/Context;

    .line 51
    iget-object p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->context:Landroid/content/Context;

    .line 110
    const-class v0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->componentInParent(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$Component;

    .line 52
    invoke-interface {p1, p0}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$Component;->inject(Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;)V

    return-void
.end method


# virtual methods
.method public final build()Landroid/app/Dialog;
    .locals 8

    .line 56
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->context:Landroid/content/Context;

    sget v1, Lcom/squareup/widgets/pos/R$layout;->duration_picker_view:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view"

    .line 58
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v1, Lcom/squareup/widgets/pos/R$id;->hour_picker:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 59
    check-cast v1, Lcom/squareup/noho/NohoNumberPicker;

    .line 60
    iget-object v2, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->res:Lcom/squareup/util/Res;

    const-string v3, "res"

    if-nez v2, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v4, Lcom/squareup/widgets/pos/R$array;->duration_hour_options:I

    invoke-interface {v2, v4}, Lcom/squareup/util/Res;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    .line 61
    invoke-virtual {v1, v4}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    .line 62
    array-length v5, v2

    const/4 v6, 0x1

    sub-int/2addr v5, v6

    invoke-virtual {v1, v5}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    .line 63
    new-instance v5, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$1$1;

    invoke-direct {v5, v2}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$1$1;-><init>([Ljava/lang/String;)V

    check-cast v5, Lcom/squareup/noho/NohoNumberPicker$Formatter;

    invoke-virtual {v1, v5}, Lcom/squareup/noho/NohoNumberPicker;->setFormatter(Lcom/squareup/noho/NohoNumberPicker$Formatter;)V

    .line 64
    iget-object v2, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->runner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    const-string v5, "runner"

    if-nez v2, :cond_1

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->getHourIndex$widgets_pos_release()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoNumberPicker;->setValue(I)V

    .line 65
    sget v2, Lcom/squareup/widgets/pos/R$id;->minute_picker:I

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/noho/NohoNumberPicker;

    .line 66
    new-instance v7, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$$inlined$apply$lambda$1;

    invoke-direct {v7, v2, p0, v0}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$$inlined$apply$lambda$1;-><init>(Lcom/squareup/noho/NohoNumberPicker;Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;Landroid/view/View;)V

    check-cast v7, Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;

    invoke-virtual {v1, v7}, Lcom/squareup/noho/NohoNumberPicker;->setOnValueChangedListener(Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;)V

    .line 78
    sget v1, Lcom/squareup/widgets/pos/R$id;->minute_picker:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 79
    check-cast v1, Lcom/squareup/noho/NohoNumberPicker;

    .line 80
    iget-object v2, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->res:Lcom/squareup/util/Res;

    if-nez v2, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget v3, Lcom/squareup/widgets/pos/R$array;->duration_minute_options:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 81
    iget-object v3, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->runner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    if-nez v3, :cond_3

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v3}, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->getAllowZeroDuration$widgets_pos_release()Z

    move-result v3

    if-nez v3, :cond_4

    .line 82
    sget v3, Lcom/squareup/widgets/pos/R$id;->hour_picker:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const-string/jumbo v7, "view.findViewById<NohoNu\u2026Picker>(R.id.hour_picker)"

    invoke-static {v3, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v3}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v3

    if-nez v3, :cond_4

    const/4 v4, 0x1

    .line 81
    :cond_4
    invoke-virtual {v1, v4}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    .line 86
    array-length v3, v2

    sub-int/2addr v3, v6

    invoke-virtual {v1, v3}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    .line 87
    new-instance v3, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$2$1;

    invoke-direct {v3, v2}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$2$1;-><init>([Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/noho/NohoNumberPicker$Formatter;

    invoke-virtual {v1, v3}, Lcom/squareup/noho/NohoNumberPicker;->setFormatter(Lcom/squareup/noho/NohoNumberPicker$Formatter;)V

    .line 88
    iget-object v2, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->runner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    if-nez v2, :cond_5

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v2}, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->getMinuteIndex$widgets_pos_release()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoNumberPicker;->setValue(I)V

    .line 89
    new-instance v2, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$$inlined$apply$lambda$2;

    invoke-direct {v2, p0, v0}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$$inlined$apply$lambda$2;-><init>(Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;Landroid/view/View;)V

    check-cast v2, Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoNumberPicker;->setOnValueChangedListener(Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;)V

    .line 94
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v2, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 95
    iget-object v2, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->runner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    if-nez v2, :cond_6

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v2}, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->getDialogTitle$widgets_pos_release()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 96
    invoke-virtual {v1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 97
    invoke-virtual {v0, v6}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 98
    new-instance v1, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$3;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$3;-><init>(Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;)V

    check-cast v1, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 99
    sget v1, Lcom/squareup/widgets/pos/R$string;->duration_save:I

    new-instance v2, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$4;

    invoke-direct {v2, p0}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$4;-><init>(Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 102
    sget v1, Lcom/squareup/widgets/pos/R$string;->duration_cancel:I

    new-instance v2, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$5;

    invoke-direct {v2, p0}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$5;-><init>(Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    const-string v1, "ThemedAlertDialog.Builde\u2026    }\n          .create()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/app/Dialog;

    return-object v0
.end method

.method public final getRes()Lcom/squareup/util/Res;
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->res:Lcom/squareup/util/Res;

    if-nez v0, :cond_0

    const-string v1, "res"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getRunner()Lcom/squareup/register/widgets/NohoDurationPickerRunner;
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->runner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    if-nez v0, :cond_0

    const-string v1, "runner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final setRes(Lcom/squareup/util/Res;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iput-object p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public final setRunner(Lcom/squareup/register/widgets/NohoDurationPickerRunner;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iput-object p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->runner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    return-void
.end method
