.class final Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;
.super Lkotlin/jvm/internal/Lambda;
.source "NohoDatePickerDialogFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "enabled",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $datePicker:Lcom/squareup/noho/NohoDatePicker;

.field final synthetic $savedYear:Lkotlin/jvm/internal/Ref$IntRef;

.field final synthetic $state:Lcom/squareup/register/widgets/NohoDatePickerDialogProps;


# direct methods
.method constructor <init>(Lcom/squareup/noho/NohoDatePicker;Lkotlin/jvm/internal/Ref$IntRef;Lcom/squareup/register/widgets/NohoDatePickerDialogProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$datePicker:Lcom/squareup/noho/NohoDatePicker;

    iput-object p2, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$savedYear:Lkotlin/jvm/internal/Ref$IntRef;

    iput-object p3, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$state:Lcom/squareup/register/widgets/NohoDatePickerDialogProps;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->invoke(Z)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Z)V
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$datePicker:Lcom/squareup/noho/NohoDatePicker;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoDatePicker;->getYearEnabled()Z

    move-result v0

    if-eq v0, p1, :cond_1

    .line 46
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$datePicker:Lcom/squareup/noho/NohoDatePicker;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoDatePicker;->setYearEnabled(Z)V

    const-string v0, "newDate"

    if-nez p1, :cond_0

    .line 48
    iget-object p1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$datePicker:Lcom/squareup/noho/NohoDatePicker;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoDatePicker;->getCurrentDate()Lorg/threeten/bp/LocalDate;

    move-result-object p1

    const/16 v1, 0x7d0

    invoke-virtual {p1, v1}, Lorg/threeten/bp/LocalDate;->withYear(I)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    .line 49
    iget-object v2, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$savedYear:Lkotlin/jvm/internal/Ref$IntRef;

    iget-object v3, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$datePicker:Lcom/squareup/noho/NohoDatePicker;

    invoke-virtual {v3}, Lcom/squareup/noho/NohoDatePicker;->getCurrentDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    invoke-virtual {v3}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v3

    iput v3, v2, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 50
    iget-object v2, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$datePicker:Lcom/squareup/noho/NohoDatePicker;

    sget-object v3, Lorg/threeten/bp/Month;->JANUARY:Lorg/threeten/bp/Month;

    const/4 v4, 0x1

    invoke-static {v1, v3, v4}, Lorg/threeten/bp/LocalDate;->of(ILorg/threeten/bp/Month;I)Lorg/threeten/bp/LocalDate;

    move-result-object v3

    const-string v4, "LocalDate.of(DEFAULT_LEAP_YEAR, Month.JANUARY, 1)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/squareup/noho/NohoDatePicker;->setMinDate(Lorg/threeten/bp/LocalDate;)V

    .line 51
    iget-object v2, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$datePicker:Lcom/squareup/noho/NohoDatePicker;

    sget-object v3, Lorg/threeten/bp/Month;->DECEMBER:Lorg/threeten/bp/Month;

    const/16 v4, 0x1f

    invoke-static {v1, v3, v4}, Lorg/threeten/bp/LocalDate;->of(ILorg/threeten/bp/Month;I)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    const-string v3, "LocalDate.of(DEFAULT_LEA\u2026YEAR, Month.DECEMBER, 31)"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Lcom/squareup/noho/NohoDatePicker;->setMaxDate(Lorg/threeten/bp/LocalDate;)V

    .line 52
    iget-object v1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$datePicker:Lcom/squareup/noho/NohoDatePicker;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Lcom/squareup/noho/NohoDatePicker;->setCurrentDate(Lorg/threeten/bp/LocalDate;)V

    goto :goto_0

    .line 54
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$datePicker:Lcom/squareup/noho/NohoDatePicker;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoDatePicker;->getCurrentDate()Lorg/threeten/bp/LocalDate;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$savedYear:Lkotlin/jvm/internal/Ref$IntRef;

    iget v1, v1, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    invoke-virtual {p1, v1}, Lorg/threeten/bp/LocalDate;->withYear(I)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    check-cast p1, Ljava/lang/Comparable;

    .line 55
    iget-object v1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$state:Lcom/squareup/register/widgets/NohoDatePickerDialogProps;

    invoke-virtual {v1}, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->getMinDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    check-cast v1, Ljava/lang/Comparable;

    iget-object v2, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$state:Lcom/squareup/register/widgets/NohoDatePickerDialogProps;

    invoke-virtual {v2}, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->getMaxDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    check-cast v2, Ljava/lang/Comparable;

    invoke-static {p1, v1, v2}, Lkotlin/ranges/RangesKt;->coerceIn(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object p1

    .line 54
    check-cast p1, Lorg/threeten/bp/LocalDate;

    .line 57
    iget-object v1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$datePicker:Lcom/squareup/noho/NohoDatePicker;

    iget-object v2, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$state:Lcom/squareup/register/widgets/NohoDatePickerDialogProps;

    invoke-virtual {v2}, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->getMinDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoDatePicker;->setMinDate(Lorg/threeten/bp/LocalDate;)V

    .line 58
    iget-object v1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$datePicker:Lcom/squareup/noho/NohoDatePicker;

    iget-object v2, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$state:Lcom/squareup/register/widgets/NohoDatePickerDialogProps;

    invoke-virtual {v2}, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->getMaxDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoDatePicker;->setMaxDate(Lorg/threeten/bp/LocalDate;)V

    .line 59
    iget-object v1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;->$datePicker:Lcom/squareup/noho/NohoDatePicker;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Lcom/squareup/noho/NohoDatePicker;->setCurrentDate(Lorg/threeten/bp/LocalDate;)V

    :cond_1
    :goto_0
    return-void
.end method
