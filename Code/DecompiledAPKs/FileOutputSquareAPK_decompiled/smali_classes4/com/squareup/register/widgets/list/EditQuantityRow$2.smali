.class Lcom/squareup/register/widgets/list/EditQuantityRow$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "EditQuantityRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/list/EditQuantityRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/list/EditQuantityRow;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/list/EditQuantityRow;)V
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow$2;->this$0:Lcom/squareup/register/widgets/list/EditQuantityRow;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 68
    iget-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow$2;->this$0:Lcom/squareup/register/widgets/list/EditQuantityRow;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->getValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setValue(I)V

    .line 69
    iget-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow$2;->this$0:Lcom/squareup/register/widgets/list/EditQuantityRow;

    invoke-static {p1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->access$100(Lcom/squareup/register/widgets/list/EditQuantityRow;)Lcom/squareup/widgets/SelectableEditText;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 70
    iget-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow$2;->this$0:Lcom/squareup/register/widgets/list/EditQuantityRow;

    invoke-static {p1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->access$100(Lcom/squareup/register/widgets/list/EditQuantityRow;)Lcom/squareup/widgets/SelectableEditText;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->clearFocus()V

    return-void
.end method
