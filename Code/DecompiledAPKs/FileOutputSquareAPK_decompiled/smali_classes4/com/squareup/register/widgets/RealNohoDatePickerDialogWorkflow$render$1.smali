.class final Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealNohoDatePickerDialogWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;->render(Lcom/squareup/register/widgets/NohoDatePickerDialogProps;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lorg/threeten/bp/LocalDate;",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "newDate",
        "Lorg/threeten/bp/LocalDate;",
        "yearEnabled",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic this$0:Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;Lcom/squareup/workflow/RenderContext;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow$render$1;->this$0:Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;

    iput-object p2, p0, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow$render$1;->$context:Lcom/squareup/workflow/RenderContext;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lorg/threeten/bp/LocalDate;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow$render$1;->invoke(Lorg/threeten/bp/LocalDate;Z)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lorg/threeten/bp/LocalDate;Z)V
    .locals 2

    const-string v0, "newDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow$render$1;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow$render$1;->this$0:Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;

    invoke-static {v1, p1, p2}, Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;->access$changeAction(Lcom/squareup/register/widgets/RealNohoDatePickerDialogWorkflow;Lorg/threeten/bp/LocalDate;Z)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
