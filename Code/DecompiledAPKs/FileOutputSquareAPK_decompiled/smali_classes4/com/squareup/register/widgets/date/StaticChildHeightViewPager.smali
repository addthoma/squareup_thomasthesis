.class public final Lcom/squareup/register/widgets/date/StaticChildHeightViewPager;
.super Landroidx/viewpager/widget/ViewPager;
.source "StaticChildHeightViewPager.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1, p2}, Landroidx/viewpager/widget/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x2

    .line 18
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/date/StaticChildHeightViewPager;->setImportantForAccessibility(I)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/StaticChildHeightViewPager;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 24
    invoke-super {p0, p1, p2}, Landroidx/viewpager/widget/ViewPager;->onMeasure(II)V

    .line 27
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/StaticChildHeightViewPager;->isInEditMode()Z

    move-result p2

    if-eqz p2, :cond_1

    return-void

    .line 29
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/StaticChildHeightViewPager;->getChildCount()I

    move-result p2

    if-eqz p2, :cond_2

    const/4 p2, 0x0

    .line 33
    invoke-virtual {p0, p2}, Lcom/squareup/register/widgets/date/StaticChildHeightViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 34
    invoke-static {p2, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 35
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 37
    invoke-super {p0, p1, p2}, Landroidx/viewpager/widget/ViewPager;->onMeasure(II)V

    return-void

    .line 30
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Expected to have children after first measure pass."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
