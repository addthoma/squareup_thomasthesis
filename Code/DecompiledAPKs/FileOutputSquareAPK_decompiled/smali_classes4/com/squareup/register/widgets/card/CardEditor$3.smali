.class Lcom/squareup/register/widgets/card/CardEditor$3;
.super Ljava/lang/Object;
.source "CardEditor.java"

# interfaces
.implements Lcom/squareup/register/widgets/card/OnPanListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/card/CardEditor;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/card/CardEditor;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/card/CardEditor;)V
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor$3;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNext(Lcom/squareup/Card;)V
    .locals 1

    .line 186
    invoke-virtual {p1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object p1

    sget-object v0, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    if-ne p1, v0, :cond_0

    .line 187
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor$3;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/CardEditor;->access$300(Lcom/squareup/register/widgets/card/CardEditor;)Lcom/squareup/register/widgets/card/OnCardListener;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor$3;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/register/widgets/card/OnCardListener;->onChargeCard(Lcom/squareup/Card;)V

    :cond_0
    return-void
.end method

.method public onPanInvalid(Lcom/squareup/Card$PanWarning;)V
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor$3;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {v0, p1}, Lcom/squareup/register/widgets/card/CardEditor;->access$1100(Lcom/squareup/register/widgets/card/CardEditor;Lcom/squareup/Card$PanWarning;)V

    return-void
.end method

.method public onPanValid(Lcom/squareup/Card;)V
    .locals 3

    .line 193
    invoke-virtual {p1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v0

    sget-object v1, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor$3;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {v0}, Lcom/squareup/register/widgets/card/CardEditor;->access$400(Lcom/squareup/register/widgets/card/CardEditor;)Lcom/squareup/register/widgets/card/PanValidationStrategy;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->validateOnlyPan()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 196
    :goto_0
    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor$3;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {v1}, Lcom/squareup/register/widgets/card/CardEditor;->access$300(Lcom/squareup/register/widgets/card/CardEditor;)Lcom/squareup/register/widgets/card/OnCardListener;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor$3;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {v1}, Lcom/squareup/register/widgets/card/CardEditor;->access$300(Lcom/squareup/register/widgets/card/CardEditor;)Lcom/squareup/register/widgets/card/OnCardListener;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lcom/squareup/register/widgets/card/OnCardListener;->onPanValid(Lcom/squareup/Card;Z)Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    :cond_1
    if-eqz v0, :cond_2

    .line 199
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor$3;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/CardEditor;->access$600(Lcom/squareup/register/widgets/card/CardEditor;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor$3;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {v0}, Lcom/squareup/register/widgets/card/CardEditor;->access$500(Lcom/squareup/register/widgets/card/CardEditor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor$3;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/CardEditor;->access$1000(Lcom/squareup/register/widgets/card/CardEditor;)Lcom/squareup/widgets/PersistentViewAnimator;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor$3;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {v0}, Lcom/squareup/register/widgets/card/CardEditor;->access$700(Lcom/squareup/register/widgets/card/CardEditor;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardEditor$3;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {v1}, Lcom/squareup/register/widgets/card/CardEditor;->access$800(Lcom/squareup/register/widgets/card/CardEditor;)Landroid/view/animation/Animation;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/register/widgets/card/CardEditor$3;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {v2}, Lcom/squareup/register/widgets/card/CardEditor;->access$900(Lcom/squareup/register/widgets/card/CardEditor;)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/widgets/PersistentViewAnimator;->animateTo(Landroid/view/View;Landroid/view/animation/Animation;Landroid/view/animation/Animation;)V

    .line 203
    :cond_2
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor$3;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/register/widgets/card/CardEditor;->access$1100(Lcom/squareup/register/widgets/card/CardEditor;Lcom/squareup/Card$PanWarning;)V

    return-void
.end method
