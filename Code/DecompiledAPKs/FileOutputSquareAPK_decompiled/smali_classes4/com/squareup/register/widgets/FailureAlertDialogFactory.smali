.class public Lcom/squareup/register/widgets/FailureAlertDialogFactory;
.super Ljava/lang/Object;
.source "FailureAlertDialogFactory.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final dismissButtonName:Ljava/lang/String;

.field private final errorMessage:Ljava/lang/String;

.field private final errorTitle:Ljava/lang/String;

.field private final retryButtonName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 134
    new-instance v0, Lcom/squareup/register/widgets/FailureAlertDialogFactory$1;

    invoke-direct {v0}, Lcom/squareup/register/widgets/FailureAlertDialogFactory$1;-><init>()V

    sput-object v0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->errorTitle:Ljava/lang/String;

    .line 49
    iput-object p2, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->errorMessage:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->dismissButtonName:Ljava/lang/String;

    .line 51
    iput-object p4, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retryButtonName:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/register/widgets/FailureAlertDialogFactory$1;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$createFailureAlertDialog$0(Ljava/lang/Runnable;Landroid/content/DialogInterface;I)V
    .locals 0

    if-eqz p0, :cond_0

    .line 87
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method

.method static synthetic lambda$createFailureAlertDialog$1(Ljava/lang/Runnable;Landroid/content/DialogInterface;)V
    .locals 0

    .line 92
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method static synthetic lambda$createFailureAlertDialog$2(Ljava/lang/Runnable;Landroid/content/DialogInterface;I)V
    .locals 0

    if-eqz p0, :cond_0

    .line 99
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method

.method public static noRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;
    .locals 2

    .line 36
    new-instance v0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static retry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public createFailureAlertDialog(Landroid/content/Context;)Landroid/app/AlertDialog;
    .locals 1

    const/4 v0, 0x0

    .line 64
    invoke-virtual {p0, p1, v0, v0}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->createFailureAlertDialog(Landroid/content/Context;Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public createFailureAlertDialog(Landroid/content/Context;Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/app/AlertDialog;
    .locals 2

    .line 82
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object p1, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->errorTitle:Ljava/lang/String;

    .line 83
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->errorMessage:Ljava/lang/String;

    .line 84
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->dismissButtonName:Ljava/lang/String;

    new-instance v1, Lcom/squareup/register/widgets/-$$Lambda$FailureAlertDialogFactory$IgS04ZmvKMFGYLvrmIfnfBFFEGg;

    invoke-direct {v1, p2}, Lcom/squareup/register/widgets/-$$Lambda$FailureAlertDialogFactory$IgS04ZmvKMFGYLvrmIfnfBFFEGg;-><init>(Ljava/lang/Runnable;)V

    .line 85
    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    if-eqz p2, :cond_0

    .line 92
    new-instance v0, Lcom/squareup/register/widgets/-$$Lambda$FailureAlertDialogFactory$LfmMKo5IhUqa3EnvJRXeZyKomCE;

    invoke-direct {v0, p2}, Lcom/squareup/register/widgets/-$$Lambda$FailureAlertDialogFactory$LfmMKo5IhUqa3EnvJRXeZyKomCE;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    .line 96
    :cond_0
    iget-object p2, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retryButtonName:Ljava/lang/String;

    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_1

    .line 97
    iget-object p2, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retryButtonName:Ljava/lang/String;

    new-instance v0, Lcom/squareup/register/widgets/-$$Lambda$FailureAlertDialogFactory$CrpU45QC7wjx-TDLs978_-8NUKY;

    invoke-direct {v0, p3}, Lcom/squareup/register/widgets/-$$Lambda$FailureAlertDialogFactory$CrpU45QC7wjx-TDLs978_-8NUKY;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {p1, p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    .line 104
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 109
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 111
    :cond_1
    check-cast p1, Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    .line 113
    iget-object v2, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->errorTitle:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->errorTitle:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->errorMessage:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->errorMessage:Ljava/lang/String;

    .line 114
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->dismissButtonName:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->dismissButtonName:Ljava/lang/String;

    .line 115
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retryButtonName:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retryButtonName:Ljava/lang/String;

    .line 116
    invoke-static {v2, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    .line 120
    iget-object v1, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->errorTitle:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->errorMessage:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->dismissButtonName:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retryButtonName:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 128
    iget-object p2, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->errorTitle:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 129
    iget-object p2, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->errorMessage:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 130
    iget-object p2, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->dismissButtonName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 131
    iget-object p2, p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retryButtonName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
