.class Lcom/squareup/register/widgets/SmartScrollContainer$1;
.super Ljava/lang/Object;
.source "SmartScrollContainer.java"

# interfaces
.implements Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/SmartScrollContainer;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/SmartScrollContainer;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/SmartScrollContainer;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/register/widgets/SmartScrollContainer$1;->this$0:Lcom/squareup/register/widgets/SmartScrollContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getHeaderHeight()I
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollContainer$1;->this$0:Lcom/squareup/register/widgets/SmartScrollContainer;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/SmartScrollContainer;->isHeaderEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollContainer$1;->this$0:Lcom/squareup/register/widgets/SmartScrollContainer;

    invoke-static {v0}, Lcom/squareup/register/widgets/SmartScrollContainer;->access$000(Lcom/squareup/register/widgets/SmartScrollContainer;)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onHide()V
    .locals 3

    .line 46
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollContainer$1;->this$0:Lcom/squareup/register/widgets/SmartScrollContainer;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/SmartScrollContainer;->isHeaderEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollContainer$1;->this$0:Lcom/squareup/register/widgets/SmartScrollContainer;

    iget-object v0, v0, Lcom/squareup/register/widgets/SmartScrollContainer;->header:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/register/widgets/SmartScrollContainer$1;->this$0:Lcom/squareup/register/widgets/SmartScrollContainer;

    .line 50
    invoke-static {v1}, Lcom/squareup/register/widgets/SmartScrollContainer;->access$000(Lcom/squareup/register/widgets/SmartScrollContainer;)I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    .line 51
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method

.method public onMoved(I)V
    .locals 2

    if-nez p1, :cond_0

    .line 56
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollContainer$1;->this$0:Lcom/squareup/register/widgets/SmartScrollContainer;

    iget-object v0, v0, Lcom/squareup/register/widgets/SmartScrollContainer;->header:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    iget-object v1, p0, Lcom/squareup/register/widgets/SmartScrollContainer$1;->this$0:Lcom/squareup/register/widgets/SmartScrollContainer;

    invoke-static {v1}, Lcom/squareup/register/widgets/SmartScrollContainer;->access$000(Lcom/squareup/register/widgets/SmartScrollContainer;)I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 61
    iget-object p1, p0, Lcom/squareup/register/widgets/SmartScrollContainer$1;->this$0:Lcom/squareup/register/widgets/SmartScrollContainer;

    iget-object p1, p1, Lcom/squareup/register/widgets/SmartScrollContainer;->header:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollContainer$1;->this$0:Lcom/squareup/register/widgets/SmartScrollContainer;

    iget-object v0, v0, Lcom/squareup/register/widgets/SmartScrollContainer;->header:Landroid/view/View;

    neg-int p1, p1

    int-to-float p1, p1

    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationY(F)V

    :goto_0
    return-void
.end method

.method public onShown()V
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollContainer$1;->this$0:Lcom/squareup/register/widgets/SmartScrollContainer;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/SmartScrollContainer;->isHeaderEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollContainer$1;->this$0:Lcom/squareup/register/widgets/SmartScrollContainer;

    iget-object v0, v0, Lcom/squareup/register/widgets/SmartScrollContainer;->header:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 40
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-direct {v1, v2}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    .line 41
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method
