.class Lcom/squareup/register/widgets/card/PanEditor$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "PanEditor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/card/PanEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/register/widgets/card/PanEditor$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final brandOrdinal:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 273
    new-instance v0, Lcom/squareup/register/widgets/card/PanEditor$SavedState$1;

    invoke-direct {v0}, Lcom/squareup/register/widgets/card/PanEditor$SavedState$1;-><init>()V

    sput-object v0, Lcom/squareup/register/widgets/card/PanEditor$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .line 264
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 265
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, p0, Lcom/squareup/register/widgets/card/PanEditor$SavedState;->brandOrdinal:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/squareup/register/widgets/card/PanEditor$1;)V
    .locals 0

    .line 255
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/card/PanEditor$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcelable;I)V
    .locals 0

    .line 259
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 260
    iput p2, p0, Lcom/squareup/register/widgets/card/PanEditor$SavedState;->brandOrdinal:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcelable;ILcom/squareup/register/widgets/card/PanEditor$1;)V
    .locals 0

    .line 255
    invoke-direct {p0, p1, p2}, Lcom/squareup/register/widgets/card/PanEditor$SavedState;-><init>(Landroid/os/Parcelable;I)V

    return-void
.end method

.method static synthetic access$600(Lcom/squareup/register/widgets/card/PanEditor$SavedState;)I
    .locals 0

    .line 255
    iget p0, p0, Lcom/squareup/register/widgets/card/PanEditor$SavedState;->brandOrdinal:I

    return p0
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 269
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 270
    iget p2, p0, Lcom/squareup/register/widgets/card/PanEditor$SavedState;->brandOrdinal:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
