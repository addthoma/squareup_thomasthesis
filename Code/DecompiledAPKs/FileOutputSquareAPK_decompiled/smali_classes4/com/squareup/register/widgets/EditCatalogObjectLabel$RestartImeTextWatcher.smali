.class Lcom/squareup/register/widgets/EditCatalogObjectLabel$RestartImeTextWatcher;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "EditCatalogObjectLabel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/EditCatalogObjectLabel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RestartImeTextWatcher"
.end annotation


# instance fields
.field private final imm:Landroid/view/inputmethod/InputMethodManager;

.field private final view:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    .line 194
    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    const-string v0, "input_method"

    .line 195
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/inputmethod/InputMethodManager;

    iput-object p1, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel$RestartImeTextWatcher;->imm:Landroid/view/inputmethod/InputMethodManager;

    .line 196
    iput-object p2, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel$RestartImeTextWatcher;->view:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 200
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel$RestartImeTextWatcher;->imm:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    if-nez p1, :cond_0

    .line 201
    iget-object p1, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel$RestartImeTextWatcher;->imm:Landroid/view/inputmethod/InputMethodManager;

    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel$RestartImeTextWatcher;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    :cond_0
    return-void
.end method
