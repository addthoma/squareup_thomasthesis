.class Lcom/squareup/register/widgets/date/DatePicker$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "DatePicker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/date/DatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/date/DatePicker;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/date/DatePicker;)V
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/squareup/register/widgets/date/DatePicker$1;->this$0:Lcom/squareup/register/widgets/date/DatePicker;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 68
    iget-object p1, p0, Lcom/squareup/register/widgets/date/DatePicker$1;->this$0:Lcom/squareup/register/widgets/date/DatePicker;

    invoke-static {p1}, Lcom/squareup/register/widgets/date/DatePicker;->access$000(Lcom/squareup/register/widgets/date/DatePicker;)Landroidx/viewpager/widget/ViewPager;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    .line 70
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker$1;->this$0:Lcom/squareup/register/widgets/date/DatePicker;

    invoke-static {v0}, Lcom/squareup/register/widgets/date/DatePicker;->access$000(Lcom/squareup/register/widgets/date/DatePicker;)Landroidx/viewpager/widget/ViewPager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {v0}, Landroidx/viewpager/widget/PagerAdapter;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker$1;->this$0:Lcom/squareup/register/widgets/date/DatePicker;

    invoke-static {v0}, Lcom/squareup/register/widgets/date/DatePicker;->access$000(Lcom/squareup/register/widgets/date/DatePicker;)Landroidx/viewpager/widget/ViewPager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    :cond_0
    return-void
.end method
