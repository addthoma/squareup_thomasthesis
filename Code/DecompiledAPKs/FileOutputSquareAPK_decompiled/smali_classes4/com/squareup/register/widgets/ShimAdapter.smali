.class public Lcom/squareup/register/widgets/ShimAdapter;
.super Landroid/widget/BaseAdapter;
.source "ShimAdapter.java"


# instance fields
.field private final count:I

.field private final inflater:Landroid/view/LayoutInflater;

.field private final layout:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1

    .line 16
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const-string v0, "layout_inflater"

    .line 17
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    iput-object p1, p0, Lcom/squareup/register/widgets/ShimAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 18
    iput p3, p0, Lcom/squareup/register/widgets/ShimAdapter;->count:I

    .line 19
    iput p2, p0, Lcom/squareup/register/widgets/ShimAdapter;->layout:I

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 23
    iget v0, p0, Lcom/squareup/register/widgets/ShimAdapter;->count:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    if-eqz p2, :cond_0

    return-object p2

    .line 42
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/widgets/ShimAdapter;->inflater:Landroid/view/LayoutInflater;

    iget p2, p0, Lcom/squareup/register/widgets/ShimAdapter;->layout:I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public isEnabled(I)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method
