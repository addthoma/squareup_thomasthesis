.class public Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;
.super Ljava/lang/Object;
.source "ThirdPartyGiftCardPanValidationStrategy.java"

# interfaces
.implements Lcom/squareup/register/widgets/card/PanValidationStrategy;


# instance fields
.field private final MAX_THIRD_PARTY_GIFT_CARD_LENGTH:I

.field private final restartImeAfterScrubChange:Z


# direct methods
.method constructor <init>(Z)V
    .locals 1

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xff

    .line 24
    iput v0, p0, Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;->MAX_THIRD_PARTY_GIFT_CARD_LENGTH:I

    .line 29
    iput-boolean p1, p0, Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;->restartImeAfterScrubChange:Z

    return-void
.end method


# virtual methods
.method public cvvValid(Ljava/lang/String;Lcom/squareup/Card$Brand;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public determineCardNumberState(Ljava/lang/String;Lcom/squareup/Card$Brand;Z)Lcom/squareup/register/widgets/card/CardNumberState;
    .locals 0

    .line 124
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-nez p1, :cond_0

    .line 126
    sget-object p1, Lcom/squareup/register/widgets/card/CardNumberState;->EMPTY:Lcom/squareup/register/widgets/card/CardNumberState;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/register/widgets/card/CardNumberState;->VALID:Lcom/squareup/register/widgets/card/CardNumberState;

    :goto_0
    return-object p1
.end method

.method public expirationValid(Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public getBrand(Ljava/lang/String;)Lcom/squareup/Card$Brand;
    .locals 1

    .line 47
    invoke-static {p1}, Lcom/squareup/CardBrandGuesser;->guessBrand(Ljava/lang/CharSequence;)Lcom/squareup/Card$Brand;

    move-result-object p1

    sget-object v0, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    if-ne p1, v0, :cond_0

    sget-object p1, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    :goto_0
    return-object p1
.end method

.method public getDefaultGlyph(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 51
    sget-object v0, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    invoke-static {v0, p1}, Lcom/squareup/util/ProtoGlyphs;->card(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p1

    return-object p1
.end method

.method public getGlyph(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 0

    .line 77
    sget-object p1, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    invoke-static {p1, p2}, Lcom/squareup/util/ProtoGlyphs;->card(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p1

    return-object p1
.end method

.method public getInputType()I
    .locals 1

    const/16 v0, 0x1000

    return v0
.end method

.method public getRestartImeAfterScrubChange()Z
    .locals 1

    .line 119
    iget-boolean v0, p0, Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;->restartImeAfterScrubChange:Z

    return v0
.end method

.method public isMaximumLength(Lcom/squareup/Card$Brand;I)Z
    .locals 0

    const/16 p1, 0xff

    if-ne p2, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public panCharacterValid(C)Z
    .locals 1

    .line 96
    invoke-static {p1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Ljava/lang/Character;->isAlphabetic(I)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public postalValid(ZLjava/lang/String;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public validateAndBuildCard(Ljava/lang/String;Lcom/squareup/giftcard/GiftCards;)Lcom/squareup/Card;
    .locals 2

    .line 33
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;->getBrand(Ljava/lang/String;)Lcom/squareup/Card$Brand;

    move-result-object v0

    .line 34
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/Card$Brand;->isValidNumberLength(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p1}, Lcom/squareup/Card$Brand;->validateLuhnIfRequired(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 35
    :cond_0
    invoke-virtual {p2, p1}, Lcom/squareup/giftcard/GiftCards;->isValidThirdPartyGiftCardPan(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 36
    :cond_1
    new-instance v1, Lcom/squareup/Card$Builder;

    invoke-direct {v1}, Lcom/squareup/Card$Builder;-><init>()V

    .line 37
    invoke-virtual {v1, p1}, Lcom/squareup/Card$Builder;->pan(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v1

    .line 38
    invoke-virtual {p2, p1}, Lcom/squareup/giftcard/GiftCards;->isValidThirdPartyGiftCardPan(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object v0, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    :cond_2
    invoke-virtual {v1, v0}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/Card$InputType;->MANUAL:Lcom/squareup/Card$InputType;

    .line 39
    invoke-virtual {p1, p2}, Lcom/squareup/Card$Builder;->inputType(Lcom/squareup/Card$InputType;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 40
    invoke-virtual {p1}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object p1

    return-object p1

    :cond_3
    const/4 p1, 0x0

    return-object p1
.end method

.method public validateAndContinueBuildingCard(Lcom/squareup/Card;Lcom/squareup/giftcard/GiftCards;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/squareup/Card;
    .locals 0

    .line 59
    invoke-virtual {p1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object p3

    sget-object p4, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    if-eq p3, p4, :cond_1

    .line 60
    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/giftcard/GiftCards;->isValidThirdPartyGiftCardPan(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return-object p1

    .line 61
    :cond_1
    :goto_0
    new-instance p2, Lcom/squareup/Card$Builder;

    invoke-direct {p2}, Lcom/squareup/Card$Builder;-><init>()V

    .line 62
    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/Card$Builder;->pan(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p2

    .line 63
    invoke-virtual {p1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    move-result-object p1

    const-string p2, "12"

    .line 64
    invoke-virtual {p1, p2}, Lcom/squareup/Card$Builder;->expirationMonth(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    const-string p2, "99"

    .line 65
    invoke-virtual {p1, p2}, Lcom/squareup/Card$Builder;->expirationYear(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/Card$InputType;->MANUAL:Lcom/squareup/Card$InputType;

    .line 66
    invoke-virtual {p1, p2}, Lcom/squareup/Card$Builder;->inputType(Lcom/squareup/Card$InputType;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 67
    invoke-virtual {p1}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object p1

    return-object p1
.end method

.method public validateOnlyPan()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
