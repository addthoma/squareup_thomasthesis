.class public Lcom/squareup/rootview/AppletDrawerRootViewSetup;
.super Ljava/lang/Object;
.source "AppletDrawerRootViewSetup.java"

# interfaces
.implements Lcom/squareup/rootview/RootViewSetup;


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$setUpRootView$0(Landroid/view/View;)Z
    .locals 2

    .line 23
    sget v0, Lcom/squareup/rootview/impl/appletdrawer/R$id;->drawer_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroidx/drawerlayout/widget/DrawerLayout;

    if-eqz p0, :cond_1

    const/4 v0, 0x3

    .line 24
    invoke-virtual {p0, v0}, Landroidx/drawerlayout/widget/DrawerLayout;->isDrawerOpen(I)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 27
    :cond_0
    invoke-virtual {p0, v0}, Landroidx/drawerlayout/widget/DrawerLayout;->closeDrawer(I)V

    const/4 p0, 0x1

    return p0

    :cond_1
    :goto_0
    const/4 p0, 0x0

    return p0
.end method


# virtual methods
.method public getRootViewLayout()I
    .locals 1

    .line 17
    sget v0, Lcom/squareup/rootview/impl/appletdrawer/R$layout;->root_view:I

    return v0
.end method

.method public setUpRootView(Landroid/view/View;)V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/rootview/-$$Lambda$AppletDrawerRootViewSetup$MRQjcdJHyTAEcDarJDq79bVuJ-M;

    invoke-direct {v0, p1}, Lcom/squareup/rootview/-$$Lambda$AppletDrawerRootViewSetup$MRQjcdJHyTAEcDarJDq79bVuJ-M;-><init>(Landroid/view/View;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setConditionalBackHandler(Landroid/view/View;Lcom/squareup/workflow/ui/HandlesBack;)V

    return-void
.end method
