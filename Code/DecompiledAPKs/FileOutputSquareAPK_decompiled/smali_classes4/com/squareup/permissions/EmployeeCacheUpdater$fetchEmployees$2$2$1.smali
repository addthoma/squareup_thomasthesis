.class final Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2$1;
.super Ljava/lang/Object;
.source "EmployeeCacheUpdater.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2;->apply(Ljava/lang/String;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/server/employees/EmployeesResponse;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "response",
        "Lcom/squareup/server/employees/EmployeesResponse;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2;


# direct methods
.method constructor <init>(Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2$1;->this$0:Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/server/employees/EmployeesResponse;)V
    .locals 1

    const-string v0, "response"

    .line 153
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/server/employees/EmployeesResponse;->getCursor()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, "sentinel_end_value"

    :goto_0
    const-string v0, "response.cursor ?: SENTINEL_END"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2$1;->this$0:Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2;

    iget-object v0, v0, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2;->$cursorRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/server/employees/EmployeesResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/permissions/EmployeeCacheUpdater$fetchEmployees$2$2$1;->accept(Lcom/squareup/server/employees/EmployeesResponse;)V

    return-void
.end method
