.class public final Lcom/squareup/permissions/SqliteEmployeesStore;
.super Ljava/lang/Object;
.source "SqliteEmployeesStore.kt"

# interfaces
.implements Lcom/squareup/permissions/EmployeesStore;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/permissions/SqliteEmployeesStore$DatabaseCallback;,
        Lcom/squareup/permissions/SqliteEmployeesStore$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSqliteEmployeesStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SqliteEmployeesStore.kt\ncom/squareup/permissions/SqliteEmployeesStore\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,195:1\n1642#2,2:196\n*E\n*S KotlinDebug\n*F\n+ 1 SqliteEmployeesStore.kt\ncom/squareup/permissions/SqliteEmployeesStore\n*L\n155#1,2:196\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0001\u0018\u0000 !2\u00020\u0001:\u0002!\"B=\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0001\u0010\n\u001a\u00020\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0014\u0010\u000e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00110\u00100\u0018H\u0016J\u0008\u0010\u0019\u001a\u00020\u001aH\u0016J\u0018\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001c\u001a\u00020\u0011H\u0016J\u0016\u0010\u001d\u001a\u00020\u001e2\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010H\u0016J\u0010\u0010 \u001a\u00020\u00112\u0006\u0010\u001c\u001a\u00020\u0011H\u0002R\u001a\u0010\u000e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00110\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/permissions/SqliteEmployeesStore;",
        "Lcom/squareup/permissions/EmployeesStore;",
        "context",
        "Landroid/app/Application;",
        "userDir",
        "Ljava/io/File;",
        "sqlBrite",
        "Lcom/squareup/sqlbrite3/SqlBrite;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "fileScheduler",
        "passcodesSettings",
        "Lcom/squareup/permissions/PasscodesSettings;",
        "(Landroid/app/Application;Ljava/io/File;Lcom/squareup/sqlbrite3/SqlBrite;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/permissions/PasscodesSettings;)V",
        "allEmployees",
        "Lio/reactivex/Flowable;",
        "",
        "Lcom/squareup/permissions/Employee;",
        "db",
        "Lcom/squareup/sqlbrite3/BriteDatabase;",
        "insertEmployee",
        "Lcom/squareup/permissions/EmployeeModel$InsertEmployee;",
        "insertPermission",
        "Lcom/squareup/permissions/EmployeePermissionsModel$InsertPermission;",
        "Lio/reactivex/Observable;",
        "close",
        "",
        "insertEmployeeInDatabase",
        "employee",
        "update",
        "Lio/reactivex/Completable;",
        "employees",
        "updatePasscodeIfOwner",
        "Companion",
        "DatabaseCallback",
        "employees_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/permissions/SqliteEmployeesStore$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DATABASE_NAME:Ljava/lang/String; = "employees.db"

.field public static final DATABASE_VERSION:I = 0x6


# instance fields
.field private final allEmployees:Lio/reactivex/Flowable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Flowable<",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Employee;",
            ">;>;"
        }
    .end annotation
.end field

.field private final db:Lcom/squareup/sqlbrite3/BriteDatabase;

.field private final fileScheduler:Lio/reactivex/Scheduler;

.field private final insertEmployee:Lcom/squareup/permissions/EmployeeModel$InsertEmployee;

.field private final insertPermission:Lcom/squareup/permissions/EmployeePermissionsModel$InsertPermission;

.field private final passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/permissions/SqliteEmployeesStore$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/permissions/SqliteEmployeesStore$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/permissions/SqliteEmployeesStore;->Companion:Lcom/squareup/permissions/SqliteEmployeesStore$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Ljava/io/File;Lcom/squareup/sqlbrite3/SqlBrite;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/permissions/PasscodesSettings;)V
    .locals 1
    .param p2    # Ljava/io/File;
        .annotation runtime Lcom/squareup/user/UserDirectory;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userDir"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sqlBrite"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileScheduler"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "passcodesSettings"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p5, p0, Lcom/squareup/permissions/SqliteEmployeesStore;->fileScheduler:Lio/reactivex/Scheduler;

    iput-object p6, p0, Lcom/squareup/permissions/SqliteEmployeesStore;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    .line 44
    check-cast p1, Landroid/content/Context;

    invoke-static {p1}, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;->builder(Landroid/content/Context;)Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;

    move-result-object p1

    .line 45
    new-instance p5, Ljava/io/File;

    const-string p6, "employees.db"

    invoke-direct {p5, p2, p6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;->name(Ljava/lang/String;)Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;

    move-result-object p1

    .line 46
    new-instance p2, Lcom/squareup/permissions/SqliteEmployeesStore$DatabaseCallback;

    invoke-direct {p2}, Lcom/squareup/permissions/SqliteEmployeesStore$DatabaseCallback;-><init>()V

    check-cast p2, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;

    invoke-virtual {p1, p2}, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;->callback(Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;)Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;

    move-result-object p1

    .line 47
    invoke-virtual {p1}, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;->build()Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;

    move-result-object p1

    .line 48
    new-instance p2, Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelperFactory;

    invoke-direct {p2}, Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelperFactory;-><init>()V

    invoke-virtual {p2, p1}, Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelperFactory;->create(Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;)Landroidx/sqlite/db/SupportSQLiteOpenHelper;

    move-result-object p1

    .line 49
    iget-object p2, p0, Lcom/squareup/permissions/SqliteEmployeesStore;->fileScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p3, p1, p2}, Lcom/squareup/sqlbrite3/SqlBrite;->wrapDatabaseHelper(Landroidx/sqlite/db/SupportSQLiteOpenHelper;Lio/reactivex/Scheduler;)Lcom/squareup/sqlbrite3/BriteDatabase;

    move-result-object p1

    const-string p2, "sqlBrite.wrapDatabaseHelper(helper, fileScheduler)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/permissions/SqliteEmployeesStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 50
    sget-object p1, Lcom/squareup/permissions/Employee;->FACTORY:Lcom/squareup/permissions/EmployeeModel$Factory;

    const-string p2, "Employee.FACTORY"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/permissions/EmployeeModel$Factory;->get_all_employees()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    move-result-object p1

    const-string p2, "Employee.FACTORY._all_employees"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object p2, p0, Lcom/squareup/permissions/SqliteEmployeesStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-virtual {p1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;->getTables()Ljava/util/Set;

    move-result-object p3

    check-cast p3, Ljava/lang/Iterable;

    check-cast p1, Landroidx/sqlite/db/SupportSQLiteQuery;

    invoke-virtual {p2, p3, p1}, Lcom/squareup/sqlbrite3/BriteDatabase;->createQuery(Ljava/lang/Iterable;Landroidx/sqlite/db/SupportSQLiteQuery;)Lcom/squareup/sqlbrite3/QueryObservable;

    move-result-object p1

    .line 52
    sget-object p2, Lcom/squareup/permissions/SqliteEmployeesStore$1;->INSTANCE:Lcom/squareup/permissions/SqliteEmployeesStore$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    if-eqz p2, :cond_0

    new-instance p3, Lcom/squareup/permissions/SqliteEmployeesStoreKt$sam$io_reactivex_functions_Function$0;

    invoke-direct {p3, p2}, Lcom/squareup/permissions/SqliteEmployeesStoreKt$sam$io_reactivex_functions_Function$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p2, p3

    :cond_0
    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lcom/squareup/sqlbrite3/QueryObservable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 53
    invoke-virtual {p1, p4}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    const/4 p2, 0x1

    .line 56
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    .line 61
    invoke-virtual {p1}, Lio/reactivex/observables/ConnectableObservable;->autoConnect()Lio/reactivex/Observable;

    move-result-object p1

    .line 75
    sget-object p2, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string p2, "db.createQuery(allEmploy\u2026      .toFlowable(LATEST)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/permissions/SqliteEmployeesStore;->allEmployees:Lio/reactivex/Flowable;

    .line 77
    new-instance p1, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;

    iget-object p2, p0, Lcom/squareup/permissions/SqliteEmployeesStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-virtual {p2}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;-><init>(Landroidx/sqlite/db/SupportSQLiteDatabase;)V

    iput-object p1, p0, Lcom/squareup/permissions/SqliteEmployeesStore;->insertEmployee:Lcom/squareup/permissions/EmployeeModel$InsertEmployee;

    .line 78
    new-instance p1, Lcom/squareup/permissions/EmployeePermissionsModel$InsertPermission;

    iget-object p2, p0, Lcom/squareup/permissions/SqliteEmployeesStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-virtual {p2}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/permissions/EmployeePermissionsModel$InsertPermission;-><init>(Landroidx/sqlite/db/SupportSQLiteDatabase;)V

    iput-object p1, p0, Lcom/squareup/permissions/SqliteEmployeesStore;->insertPermission:Lcom/squareup/permissions/EmployeePermissionsModel$InsertPermission;

    return-void
.end method

.method public static final synthetic access$Companion()Lcom/squareup/permissions/SqliteEmployeesStore$Companion;
    .locals 1

    sget-object v0, Lcom/squareup/permissions/SqliteEmployeesStore;->Companion:Lcom/squareup/permissions/SqliteEmployeesStore$Companion;

    return-object v0
.end method

.method public static final synthetic access$getDb$p(Lcom/squareup/permissions/SqliteEmployeesStore;)Lcom/squareup/sqlbrite3/BriteDatabase;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/permissions/SqliteEmployeesStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    return-object p0
.end method

.method public static final synthetic access$insertEmployeeInDatabase(Lcom/squareup/permissions/SqliteEmployeesStore;Lcom/squareup/permissions/Employee;Lcom/squareup/sqlbrite3/BriteDatabase;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2}, Lcom/squareup/permissions/SqliteEmployeesStore;->insertEmployeeInDatabase(Lcom/squareup/permissions/Employee;Lcom/squareup/sqlbrite3/BriteDatabase;)V

    return-void
.end method

.method public static final synthetic access$updatePasscodeIfOwner(Lcom/squareup/permissions/SqliteEmployeesStore;Lcom/squareup/permissions/Employee;)Lcom/squareup/permissions/Employee;
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/permissions/SqliteEmployeesStore;->updatePasscodeIfOwner(Lcom/squareup/permissions/Employee;)Lcom/squareup/permissions/Employee;

    move-result-object p0

    return-object p0
.end method

.method private final insertEmployeeInDatabase(Lcom/squareup/permissions/Employee;Lcom/squareup/sqlbrite3/BriteDatabase;)V
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    .line 145
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/permissions/Employee;->token()Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_1

    .line 147
    iget-object v3, v0, Lcom/squareup/permissions/SqliteEmployeesStore;->insertEmployee:Lcom/squareup/permissions/EmployeeModel$InsertEmployee;

    .line 148
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/permissions/Employee;->first_name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/permissions/Employee;->last_name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/permissions/Employee;->employee_number()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/permissions/Employee;->hashed_passcode()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/permissions/Employee;->salt()Ljava/lang/String;

    move-result-object v9

    .line 149
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/permissions/Employee;->iterations()Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/permissions/Employee;->timecard_id()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/permissions/Employee;->clockin_time()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/permissions/Employee;->active()Ljava/lang/Boolean;

    move-result-object v13

    .line 150
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/permissions/Employee;->can_access_register_with_passcode()Ljava/lang/Boolean;

    move-result-object v14

    .line 151
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/permissions/Employee;->can_track_time()Ljava/lang/Boolean;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/permissions/Employee;->is_account_owner()Ljava/lang/Boolean;

    move-result-object v17

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/permissions/Employee;->is_owner()Ljava/lang/Boolean;

    move-result-object v18

    iget-object v4, v1, Lcom/squareup/permissions/Employee;->roleToken:Ljava/lang/String;

    move-object/from16 v19, v4

    move-object v4, v15

    move-object/from16 v20, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    .line 147
    invoke-virtual/range {v3 .. v18}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bind(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 154
    iget-object v3, v0, Lcom/squareup/permissions/SqliteEmployeesStore;->insertEmployee:Lcom/squareup/permissions/EmployeeModel$InsertEmployee;

    invoke-virtual {v3}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->getTable()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/permissions/SqliteEmployeesStore;->insertEmployee:Lcom/squareup/permissions/EmployeeModel$InsertEmployee;

    check-cast v4, Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-virtual {v2, v3, v4}, Lcom/squareup/sqlbrite3/BriteDatabase;->executeInsert(Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteStatement;)J

    .line 155
    iget-object v1, v1, Lcom/squareup/permissions/Employee;->permissions:Ljava/util/Set;

    const-string v3, "employee.permissions"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    .line 196
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 156
    iget-object v4, v0, Lcom/squareup/permissions/SqliteEmployeesStore;->insertPermission:Lcom/squareup/permissions/EmployeePermissionsModel$InsertPermission;

    move-object/from16 v5, v20

    invoke-virtual {v4, v5, v3}, Lcom/squareup/permissions/EmployeePermissionsModel$InsertPermission;->bind(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iget-object v3, v0, Lcom/squareup/permissions/SqliteEmployeesStore;->insertPermission:Lcom/squareup/permissions/EmployeePermissionsModel$InsertPermission;

    invoke-virtual {v3}, Lcom/squareup/permissions/EmployeePermissionsModel$InsertPermission;->getTable()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/permissions/SqliteEmployeesStore;->insertPermission:Lcom/squareup/permissions/EmployeePermissionsModel$InsertPermission;

    check-cast v4, Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-virtual {v2, v3, v4}, Lcom/squareup/sqlbrite3/BriteDatabase;->executeInsert(Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteStatement;)J

    goto :goto_0

    :cond_0
    return-void

    .line 145
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Required value was null."

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method private final updatePasscodeIfOwner(Lcom/squareup/permissions/Employee;)Lcom/squareup/permissions/Employee;
    .locals 3

    .line 119
    iget-object v0, p0, Lcom/squareup/permissions/SqliteEmployeesStore;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->getAccountOwnerEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    .line 123
    invoke-virtual {p1}, Lcom/squareup/permissions/Employee;->is_account_owner()Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122
    invoke-virtual {p1}, Lcom/squareup/permissions/Employee;->hashed_passcode()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    .line 123
    invoke-virtual {v0}, Lcom/squareup/permissions/Employee;->hashed_passcode()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 126
    invoke-virtual {p1}, Lcom/squareup/permissions/Employee;->toEmployeesEntity()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/EmployeesEntity;->newBuilder()Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object p1

    .line 128
    new-instance v1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    invoke-direct {v1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;-><init>()V

    .line 129
    iget-object v2, v0, Lcom/squareup/permissions/Employee;->salt:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->salt(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object v1

    .line 130
    invoke-virtual {v0}, Lcom/squareup/permissions/Employee;->iterations()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->iterations(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object v1

    .line 131
    iget-object v0, v0, Lcom/squareup/permissions/Employee;->hashedPasscode:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->hashed_passcode(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    move-result-object v0

    .line 127
    invoke-virtual {p1, v0}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->passcode_only_credential(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object p1

    .line 134
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object p1

    .line 125
    invoke-static {p1}, Lcom/squareup/permissions/Employee;->fromEmployeesEntity(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/permissions/Employee;

    move-result-object p1

    const-string v0, "Employee.fromEmployeesEn\u2026         .build()\n      )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    return-object p1
.end method


# virtual methods
.method public allEmployees()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Employee;",
            ">;>;"
        }
    .end annotation

    .line 81
    iget-object v0, p0, Lcom/squareup/permissions/SqliteEmployeesStore;->allEmployees:Lio/reactivex/Flowable;

    invoke-virtual {v0}, Lio/reactivex/Flowable;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "allEmployees.toObservable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public close()V
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/permissions/SqliteEmployeesStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-virtual {v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->close()V

    return-void
.end method

.method public update(Lcom/squareup/permissions/Employee;)Lio/reactivex/Completable;
    .locals 1

    const-string v0, "employee"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    new-instance v0, Lcom/squareup/permissions/SqliteEmployeesStore$update$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/permissions/SqliteEmployeesStore$update$2;-><init>(Lcom/squareup/permissions/SqliteEmployeesStore;Lcom/squareup/permissions/Employee;)V

    check-cast v0, Lio/reactivex/functions/Action;

    invoke-static {v0}, Lio/reactivex/Completable;->fromAction(Lio/reactivex/functions/Action;)Lio/reactivex/Completable;

    move-result-object p1

    .line 106
    iget-object v0, p0, Lcom/squareup/permissions/SqliteEmployeesStore;->fileScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Completable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object p1

    const-string v0, "Completable\n        .fro\u2026ubscribeOn(fileScheduler)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public update(Ljava/util/Set;)Lio/reactivex/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "+",
            "Lcom/squareup/permissions/Employee;",
            ">;)",
            "Lio/reactivex/Completable;"
        }
    .end annotation

    const-string v0, "employees"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    new-instance v0, Lcom/squareup/permissions/SqliteEmployeesStore$update$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/permissions/SqliteEmployeesStore$update$1;-><init>(Lcom/squareup/permissions/SqliteEmployeesStore;Ljava/util/Set;)V

    check-cast v0, Lio/reactivex/functions/Action;

    invoke-static {v0}, Lio/reactivex/Completable;->fromAction(Lio/reactivex/functions/Action;)Lio/reactivex/Completable;

    move-result-object p1

    .line 99
    iget-object v0, p0, Lcom/squareup/permissions/SqliteEmployeesStore;->fileScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Completable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object p1

    const-string v0, "Completable\n        .fro\u2026ubscribeOn(fileScheduler)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
