.class synthetic Lcom/squareup/permissions/PasscodeEmployeeManagement$1;
.super Ljava/lang/Object;
.source "PasscodeEmployeeManagement.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/PasscodeEmployeeManagement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$permissions$PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 659
    invoke-static {}, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->values()[Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$1;->$SwitchMap$com$squareup$permissions$PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION:[I

    :try_start_0
    sget-object v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$1;->$SwitchMap$com$squareup$permissions$PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION:[I

    sget-object v1, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->SHOW_HOME_SCREEN:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    invoke-virtual {v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$1;->$SwitchMap$com$squareup$permissions$PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION:[I

    sget-object v1, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->SHOW_PASSCODE_SCREEN:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    invoke-virtual {v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$1;->$SwitchMap$com$squareup$permissions$PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION:[I

    sget-object v1, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->LOG_IN_GUEST_AND_SHOW_HOME_SCREEN_NO_HUD:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    invoke-virtual {v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$1;->$SwitchMap$com$squareup$permissions$PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION:[I

    sget-object v1, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->LOG_IN_GUEST_AND_SHOW_HOME_SCREEN_WITH_HUD:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    invoke-virtual {v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    return-void
.end method
