.class public final Lcom/squareup/permissions/PasscodesSettings_Factory;
.super Ljava/lang/Object;
.source "PasscodesSettings_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/permissions/PasscodesSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final employeesServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/employees/EmployeesService;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final requirePasscodeAfterLogOutSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final requirePasscodeWhenBackingOutOfSaleSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/employees/EmployeesService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/permissions/PasscodesSettings_Factory;->employeeManagementSettingsProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p2, p0, Lcom/squareup/permissions/PasscodesSettings_Factory;->employeesServiceProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p3, p0, Lcom/squareup/permissions/PasscodesSettings_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p4, p0, Lcom/squareup/permissions/PasscodesSettings_Factory;->requirePasscodeWhenBackingOutOfSaleSettingProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p5, p0, Lcom/squareup/permissions/PasscodesSettings_Factory;->requirePasscodeAfterLogOutSettingProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p6, p0, Lcom/squareup/permissions/PasscodesSettings_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/permissions/PasscodesSettings_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/employees/EmployeesService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)",
            "Lcom/squareup/permissions/PasscodesSettings_Factory;"
        }
    .end annotation

    .line 60
    new-instance v7, Lcom/squareup/permissions/PasscodesSettings_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/permissions/PasscodesSettings_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/server/employees/EmployeesService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lio/reactivex/Scheduler;)Lcom/squareup/permissions/PasscodesSettings;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            "Lcom/squareup/server/employees/EmployeesService;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lio/reactivex/Scheduler;",
            ")",
            "Lcom/squareup/permissions/PasscodesSettings;"
        }
    .end annotation

    .line 67
    new-instance v7, Lcom/squareup/permissions/PasscodesSettings;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/permissions/PasscodesSettings;-><init>(Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/server/employees/EmployeesService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lio/reactivex/Scheduler;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/permissions/PasscodesSettings;
    .locals 7

    .line 50
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings_Factory;->employeeManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/EmployeeManagementSettings;

    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings_Factory;->employeesServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/server/employees/EmployeesService;

    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings_Factory;->requirePasscodeWhenBackingOutOfSaleSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings_Factory;->requirePasscodeAfterLogOutSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lio/reactivex/Scheduler;

    invoke-static/range {v1 .. v6}, Lcom/squareup/permissions/PasscodesSettings_Factory;->newInstance(Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/server/employees/EmployeesService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lio/reactivex/Scheduler;)Lcom/squareup/permissions/PasscodesSettings;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodesSettings_Factory;->get()Lcom/squareup/permissions/PasscodesSettings;

    move-result-object v0

    return-object v0
.end method
