.class public Lcom/squareup/permissions/EmployeeInfo;
.super Ljava/lang/Object;
.source "EmployeeInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/permissions/EmployeeInfo$Builder;
    }
.end annotation


# static fields
.field public static final EMPTY:Lcom/squareup/permissions/EmployeeInfo;

.field public static final EMPTY_EMPLOYEE_PROTO:Lcom/squareup/protos/client/Employee;


# instance fields
.field public final employeeToken:Ljava/lang/String;

.field public final firstName:Ljava/lang/String;

.field public final lastName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    new-instance v0, Lcom/squareup/permissions/EmployeeInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/permissions/EmployeeInfo$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeInfo$Builder;->build()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v0

    sput-object v0, Lcom/squareup/permissions/EmployeeInfo;->EMPTY:Lcom/squareup/permissions/EmployeeInfo;

    .line 14
    new-instance v0, Lcom/squareup/protos/client/Employee$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/Employee$Builder;-><init>()V

    .line 15
    invoke-virtual {v0}, Lcom/squareup/protos/client/Employee$Builder;->build()Lcom/squareup/protos/client/Employee;

    move-result-object v0

    sput-object v0, Lcom/squareup/permissions/EmployeeInfo;->EMPTY_EMPLOYEE_PROTO:Lcom/squareup/protos/client/Employee;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p3, p0, Lcom/squareup/permissions/EmployeeInfo;->employeeToken:Ljava/lang/String;

    .line 48
    iput-object p1, p0, Lcom/squareup/permissions/EmployeeInfo;->firstName:Ljava/lang/String;

    .line 49
    iput-object p2, p0, Lcom/squareup/permissions/EmployeeInfo;->lastName:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/permissions/EmployeeInfo$1;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/permissions/EmployeeInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static createEmployeeInfo(Lcom/squareup/permissions/Employee;)Lcom/squareup/permissions/EmployeeInfo;
    .locals 2

    if-nez p0, :cond_0

    .line 64
    sget-object p0, Lcom/squareup/permissions/EmployeeInfo;->EMPTY:Lcom/squareup/permissions/EmployeeInfo;

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/permissions/EmployeeInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/permissions/EmployeeInfo$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    .line 67
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/EmployeeInfo$Builder;->employeeToken(Ljava/lang/String;)Lcom/squareup/permissions/EmployeeInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    .line 68
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/EmployeeInfo$Builder;->firstName(Ljava/lang/String;)Lcom/squareup/permissions/EmployeeInfo$Builder;

    move-result-object v0

    iget-object p0, p0, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    .line 69
    invoke-virtual {v0, p0}, Lcom/squareup/permissions/EmployeeInfo$Builder;->lastName(Ljava/lang/String;)Lcom/squareup/permissions/EmployeeInfo$Builder;

    move-result-object p0

    .line 70
    invoke-virtual {p0}, Lcom/squareup/permissions/EmployeeInfo$Builder;->build()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static createEmployeeInfo(Lcom/squareup/protos/client/Employee;)Lcom/squareup/permissions/EmployeeInfo;
    .locals 2

    if-nez p0, :cond_0

    .line 54
    sget-object p0, Lcom/squareup/permissions/EmployeeInfo;->EMPTY:Lcom/squareup/permissions/EmployeeInfo;

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/permissions/EmployeeInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/permissions/EmployeeInfo$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/protos/client/Employee;->employee_token:Ljava/lang/String;

    .line 57
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/EmployeeInfo$Builder;->employeeToken(Ljava/lang/String;)Lcom/squareup/permissions/EmployeeInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/protos/client/Employee;->first_name:Ljava/lang/String;

    .line 58
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/EmployeeInfo$Builder;->firstName(Ljava/lang/String;)Lcom/squareup/permissions/EmployeeInfo$Builder;

    move-result-object v0

    iget-object p0, p0, Lcom/squareup/protos/client/Employee;->last_name:Ljava/lang/String;

    .line 59
    invoke-virtual {v0, p0}, Lcom/squareup/permissions/EmployeeInfo$Builder;->lastName(Ljava/lang/String;)Lcom/squareup/permissions/EmployeeInfo$Builder;

    move-result-object p0

    .line 60
    invoke-virtual {p0}, Lcom/squareup/permissions/EmployeeInfo$Builder;->build()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static createEmployeeInfo(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/permissions/EmployeeInfo;
    .locals 2

    if-nez p0, :cond_0

    .line 74
    sget-object p0, Lcom/squareup/permissions/EmployeeInfo;->EMPTY:Lcom/squareup/permissions/EmployeeInfo;

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/permissions/EmployeeInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/permissions/EmployeeInfo$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->token:Ljava/lang/String;

    .line 77
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/EmployeeInfo$Builder;->employeeToken(Ljava/lang/String;)Lcom/squareup/permissions/EmployeeInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->first_name:Ljava/lang/String;

    .line 78
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/EmployeeInfo$Builder;->firstName(Ljava/lang/String;)Lcom/squareup/permissions/EmployeeInfo$Builder;

    move-result-object v0

    iget-object p0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->last_name:Ljava/lang/String;

    .line 79
    invoke-virtual {v0, p0}, Lcom/squareup/permissions/EmployeeInfo$Builder;->lastName(Ljava/lang/String;)Lcom/squareup/permissions/EmployeeInfo$Builder;

    move-result-object p0

    .line 80
    invoke-virtual {p0}, Lcom/squareup/permissions/EmployeeInfo$Builder;->build()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static isEmpty(Lcom/squareup/permissions/EmployeeInfo;)Z
    .locals 1

    if-eqz p0, :cond_1

    .line 84
    sget-object v0, Lcom/squareup/permissions/EmployeeInfo;->EMPTY:Lcom/squareup/permissions/EmployeeInfo;

    if-eq p0, v0, :cond_1

    iget-object p0, p0, Lcom/squareup/permissions/EmployeeInfo;->employeeToken:Ljava/lang/String;

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method


# virtual methods
.method public createEmployeeProto(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeInfo;->employeeToken:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/protos/client/Employee$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/Employee$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/permissions/EmployeeInfo;->employeeToken:Ljava/lang/String;

    .line 99
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/Employee$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/EmployeeInfo;->firstName:Ljava/lang/String;

    .line 100
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/Employee$Builder;->first_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/EmployeeInfo;->lastName:Ljava/lang/String;

    .line 101
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/Employee$Builder;->last_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v0

    .line 102
    invoke-virtual {p0, p1}, Lcom/squareup/permissions/EmployeeInfo;->getFullName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/Employee$Builder;->read_only_full_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object p1

    .line 103
    invoke-virtual {p1}, Lcom/squareup/protos/client/Employee$Builder;->build()Lcom/squareup/protos/client/Employee;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public createEmployeeProtoNonNull(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;
    .locals 0

    .line 111
    invoke-virtual {p0, p1}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeProto(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 112
    :cond_0
    sget-object p1, Lcom/squareup/permissions/EmployeeInfo;->EMPTY_EMPLOYEE_PROTO:Lcom/squareup/protos/client/Employee;

    :goto_0
    return-object p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 117
    :cond_0
    instance-of v0, p1, Lcom/squareup/permissions/EmployeeInfo;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 118
    :cond_1
    check-cast p1, Lcom/squareup/permissions/EmployeeInfo;

    .line 119
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeInfo;->employeeToken:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/permissions/EmployeeInfo;->employeeToken:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getFullName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeInfo;->firstName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/permissions/EmployeeInfo;->lastName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 128
    sget v0, Lcom/squareup/employees/R$string;->crm_employee_full_name:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/permissions/EmployeeInfo;->firstName:Ljava/lang/String;

    const-string v1, "first_name"

    .line 129
    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/permissions/EmployeeInfo;->lastName:Ljava/lang/String;

    const-string v1, "last_name"

    .line 130
    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 131
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 132
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 133
    :cond_0
    iget-object p1, p0, Lcom/squareup/permissions/EmployeeInfo;->firstName:Ljava/lang/String;

    if-nez p1, :cond_1

    .line 134
    iget-object p1, p0, Lcom/squareup/permissions/EmployeeInfo;->lastName:Ljava/lang/String;

    :cond_1
    return-object p1
.end method

.method public hashCode()I
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/permissions/EmployeeInfo;->employeeToken:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
