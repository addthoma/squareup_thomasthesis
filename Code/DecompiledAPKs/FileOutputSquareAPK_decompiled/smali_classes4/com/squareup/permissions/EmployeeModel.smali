.class public interface abstract Lcom/squareup/permissions/EmployeeModel;
.super Ljava/lang/Object;
.source "EmployeeModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/permissions/EmployeeModel$InsertEmployee;,
        Lcom/squareup/permissions/EmployeeModel$Factory;,
        Lcom/squareup/permissions/EmployeeModel$Mapper;,
        Lcom/squareup/permissions/EmployeeModel$Creator;,
        Lcom/squareup/permissions/EmployeeModel$Get_all_employeesMapper;,
        Lcom/squareup/permissions/EmployeeModel$Get_all_employeesCreator;,
        Lcom/squareup/permissions/EmployeeModel$Get_all_employeesModel;
    }
.end annotation


# static fields
.field public static final ACTIVE:Ljava/lang/String; = "active"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CAN_ACCESS_REGISTER_WITH_PASSCODE:Ljava/lang/String; = "can_access_register_with_passcode"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CAN_TRACK_TIME:Ljava/lang/String; = "can_track_time"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CLOCKIN_TIME:Ljava/lang/String; = "clockin_time"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CREATE_TABLE:Ljava/lang/String; = "CREATE TABLE employees_table (\n  token TEXT PRIMARY KEY,\n  first_name TEXT,\n  last_name TEXT,\n  employee_number TEXT,\n  hashed_passcode TEXT,\n  salt TEXT,\n  iterations INTEGER,\n  timecard_id TEXT,\n  clockin_time TEXT,\n  active INTEGER,\n  can_access_register_with_passcode INTEGER,\n  can_track_time INTEGER,\n  is_account_owner INTEGER,\n  is_owner INTEGER,\n  role_token TEXT\n)"

.field public static final EMPLOYEE_NUMBER:Ljava/lang/String; = "employee_number"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FIRST_NAME:Ljava/lang/String; = "first_name"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final HASHED_PASSCODE:Ljava/lang/String; = "hashed_passcode"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final IS_ACCOUNT_OWNER:Ljava/lang/String; = "is_account_owner"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final IS_OWNER:Ljava/lang/String; = "is_owner"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ITERATIONS:Ljava/lang/String; = "iterations"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LAST_NAME:Ljava/lang/String; = "last_name"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ROLE_TOKEN:Ljava/lang/String; = "role_token"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SALT:Ljava/lang/String; = "salt"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TABLE_NAME:Ljava/lang/String; = "employees_table"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TIMECARD_ID:Ljava/lang/String; = "timecard_id"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TOKEN:Ljava/lang/String; = "token"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# virtual methods
.method public abstract active()Ljava/lang/Boolean;
.end method

.method public abstract can_access_register_with_passcode()Ljava/lang/Boolean;
.end method

.method public abstract can_track_time()Ljava/lang/Boolean;
.end method

.method public abstract clockin_time()Ljava/lang/String;
.end method

.method public abstract employee_number()Ljava/lang/String;
.end method

.method public abstract first_name()Ljava/lang/String;
.end method

.method public abstract hashed_passcode()Ljava/lang/String;
.end method

.method public abstract is_account_owner()Ljava/lang/Boolean;
.end method

.method public abstract is_owner()Ljava/lang/Boolean;
.end method

.method public abstract iterations()Ljava/lang/Integer;
.end method

.method public abstract last_name()Ljava/lang/String;
.end method

.method public abstract role_token()Ljava/lang/String;
.end method

.method public abstract salt()Ljava/lang/String;
.end method

.method public abstract timecard_id()Ljava/lang/String;
.end method

.method public abstract token()Ljava/lang/String;
.end method
