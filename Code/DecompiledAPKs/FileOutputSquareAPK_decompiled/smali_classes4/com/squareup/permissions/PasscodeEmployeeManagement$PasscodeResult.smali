.class public Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;
.super Ljava/lang/Object;
.source "PasscodeEmployeeManagement.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/PasscodeEmployeeManagement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PasscodeResult"
.end annotation


# static fields
.field private static final EMPTY:Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;


# instance fields
.field final didAuthorize:Z

.field public final employee:Lcom/squareup/permissions/Employee;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 63
    new-instance v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;-><init>(ZLcom/squareup/permissions/Employee;)V

    sput-object v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;->EMPTY:Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;

    return-void
.end method

.method private constructor <init>(ZLcom/squareup/permissions/Employee;)V
    .locals 0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-boolean p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;->didAuthorize:Z

    .line 77
    iput-object p2, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;->employee:Lcom/squareup/permissions/Employee;

    return-void
.end method

.method static synthetic access$000()Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;
    .locals 1

    .line 62
    sget-object v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;->EMPTY:Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;

    return-object v0
.end method

.method static forPasscodeEmployeeManagement(Lcom/squareup/permissions/Employee;)Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;
    .locals 2

    if-nez p0, :cond_0

    .line 82
    sget-object p0, Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;->EMPTY:Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;

    return-object p0

    .line 84
    :cond_0
    new-instance v0, Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;-><init>(ZLcom/squareup/permissions/Employee;)V

    return-object v0
.end method
