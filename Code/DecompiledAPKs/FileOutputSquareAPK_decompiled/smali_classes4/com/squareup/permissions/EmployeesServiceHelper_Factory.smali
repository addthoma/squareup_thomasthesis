.class public final Lcom/squareup/permissions/EmployeesServiceHelper_Factory;
.super Ljava/lang/Object;
.source "EmployeesServiceHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/permissions/EmployeesServiceHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final serviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/employees/EmployeesService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/employees/EmployeesService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/permissions/EmployeesServiceHelper_Factory;->serviceProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/permissions/EmployeesServiceHelper_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/permissions/EmployeesServiceHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/employees/EmployeesService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)",
            "Lcom/squareup/permissions/EmployeesServiceHelper_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/permissions/EmployeesServiceHelper_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/permissions/EmployeesServiceHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/server/employees/EmployeesService;Lrx/Scheduler;)Lcom/squareup/permissions/EmployeesServiceHelper;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/permissions/EmployeesServiceHelper;

    invoke-direct {v0, p0, p1}, Lcom/squareup/permissions/EmployeesServiceHelper;-><init>(Lcom/squareup/server/employees/EmployeesService;Lrx/Scheduler;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/permissions/EmployeesServiceHelper;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/permissions/EmployeesServiceHelper_Factory;->serviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/employees/EmployeesService;

    iget-object v1, p0, Lcom/squareup/permissions/EmployeesServiceHelper_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lrx/Scheduler;

    invoke-static {v0, v1}, Lcom/squareup/permissions/EmployeesServiceHelper_Factory;->newInstance(Lcom/squareup/server/employees/EmployeesService;Lrx/Scheduler;)Lcom/squareup/permissions/EmployeesServiceHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/permissions/EmployeesServiceHelper_Factory;->get()Lcom/squareup/permissions/EmployeesServiceHelper;

    move-result-object v0

    return-object v0
.end method
