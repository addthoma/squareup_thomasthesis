.class Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "PermissionGatekeeper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/PermissionGatekeeper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoggingPermissionWhenWrapper"
.end annotation


# instance fields
.field private final permissions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final when:Lcom/squareup/permissions/PermissionGatekeeper$When;


# direct methods
.method constructor <init>(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/PermissionGatekeeper$When;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/permissions/PermissionGatekeeper$When;",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;)V"
        }
    .end annotation

    .line 133
    iput-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;->this$0:Lcom/squareup/permissions/PermissionGatekeeper;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    .line 134
    iput-object p2, p0, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    .line 135
    iput-object p3, p0, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;->permissions:Ljava/util/Set;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;)Ljava/util/Set;
    .locals 0

    .line 129
    iget-object p0, p0, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;->permissions:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;Lcom/squareup/permissions/Employee;)V
    .locals 0

    .line 129
    invoke-direct {p0, p1}, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;->setAuthorizedEmployee(Lcom/squareup/permissions/Employee;)V

    return-void
.end method

.method private setAuthorizedEmployee(Lcom/squareup/permissions/Employee;)V
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    iput-object p1, v0, Lcom/squareup/permissions/PermissionGatekeeper$When;->authorizedEmployee:Lcom/squareup/permissions/Employee;

    return-void
.end method


# virtual methods
.method public failure()V
    .locals 4

    .line 145
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;->this$0:Lcom/squareup/permissions/PermissionGatekeeper;

    invoke-static {v0}, Lcom/squareup/permissions/PermissionGatekeeper;->access$000(Lcom/squareup/permissions/PermissionGatekeeper;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    new-instance v1, Lcom/squareup/permissions/PasscodeAuthorizationEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_AUTHORIZATION_FAILED:Lcom/squareup/analytics/RegisterActionName;

    iget-object v3, p0, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;->permissions:Ljava/util/Set;

    .line 146
    invoke-static {v3}, Lcom/squareup/permissions/PermissionGatekeeper;->permissionSetToString(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/permissions/PasscodeAuthorizationEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V

    .line 145
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    invoke-virtual {v0}, Lcom/squareup/permissions/PermissionGatekeeper$When;->failure()V

    return-void
.end method

.method protected getAuthorizedEmployeeToken()Ljava/lang/String;
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    invoke-virtual {v0}, Lcom/squareup/permissions/PermissionGatekeeper$When;->getAuthorizedEmployeeToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method resetAuthorizedEmployee()V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    invoke-virtual {v0}, Lcom/squareup/permissions/PermissionGatekeeper$When;->resetAuthorizedEmployee()V

    return-void
.end method

.method public success()V
    .locals 4

    .line 139
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;->this$0:Lcom/squareup/permissions/PermissionGatekeeper;

    invoke-static {v0}, Lcom/squareup/permissions/PermissionGatekeeper;->access$000(Lcom/squareup/permissions/PermissionGatekeeper;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    new-instance v1, Lcom/squareup/permissions/PasscodeAuthorizationEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_AUTHORIZATION_ACCEPTED:Lcom/squareup/analytics/RegisterActionName;

    iget-object v3, p0, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;->permissions:Ljava/util/Set;

    .line 140
    invoke-static {v3}, Lcom/squareup/permissions/PermissionGatekeeper;->permissionSetToString(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/permissions/PasscodeAuthorizationEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V

    .line 139
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    invoke-virtual {v0}, Lcom/squareup/permissions/PermissionGatekeeper$When;->success()V

    return-void
.end method
