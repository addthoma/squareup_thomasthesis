.class final Lcom/squareup/permissions/SqliteEmployeesStore$DatabaseCallback;
.super Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;
.source "SqliteEmployeesStore.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/SqliteEmployeesStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "DatabaseCallback"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J \u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\tH\u0016\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/permissions/SqliteEmployeesStore$DatabaseCallback;",
        "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;",
        "()V",
        "onCreate",
        "",
        "db",
        "Landroidx/sqlite/db/SupportSQLiteDatabase;",
        "onUpgrade",
        "oldVersion",
        "",
        "newVersion",
        "employees_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 161
    invoke-static {}, Lcom/squareup/permissions/SqliteEmployeesStore;->access$Companion()Lcom/squareup/permissions/SqliteEmployeesStore$Companion;

    const/4 v0, 0x6

    invoke-direct {p0, v0}, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;-><init>(I)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroidx/sqlite/db/SupportSQLiteDatabase;)V
    .locals 1

    const-string v0, "db"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "CREATE TABLE employees_table (\n  token TEXT PRIMARY KEY,\n  first_name TEXT,\n  last_name TEXT,\n  employee_number TEXT,\n  hashed_passcode TEXT,\n  salt TEXT,\n  iterations INTEGER,\n  timecard_id TEXT,\n  clockin_time TEXT,\n  active INTEGER,\n  can_access_register_with_passcode INTEGER,\n  can_track_time INTEGER,\n  is_account_owner INTEGER,\n  is_owner INTEGER,\n  role_token TEXT\n)"

    .line 163
    invoke-interface {p1, v0}, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE employee_permissions_table (\n  employee_token TEXT NOT NULL REFERENCES employees_table,\n  permission TEXT\n)"

    .line 164
    invoke-interface {p1, v0}, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public onUpgrade(Landroidx/sqlite/db/SupportSQLiteDatabase;II)V
    .locals 0

    const-string p2, "db"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "DROP TABLE employees_table"

    .line 172
    invoke-interface {p1, p2}, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string p2, "DROP TABLE employee_permissions_table"

    .line 173
    invoke-interface {p1, p2}, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 174
    invoke-virtual {p0, p1}, Lcom/squareup/permissions/SqliteEmployeesStore$DatabaseCallback;->onCreate(Landroidx/sqlite/db/SupportSQLiteDatabase;)V

    return-void
.end method
