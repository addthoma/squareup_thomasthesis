.class public Lcom/squareup/permissions/Employee;
.super Ljava/lang/Object;
.source "Employee.java"

# interfaces
.implements Lcom/squareup/permissions/EmployeeModel;
.implements Lcom/squareup/permissions/EmployeeModel$Get_all_employeesModel;


# static fields
.field public static final FACTORY:Lcom/squareup/permissions/EmployeeModel$Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/permissions/EmployeeModel$Factory<",
            "Lcom/squareup/permissions/Employee;",
            ">;"
        }
    .end annotation
.end field

.field public static final MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Lcom/squareup/permissions/Employee;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final active:Z

.field final canAccessRegisterWithPasscode:Z

.field final canTrackTime:Z

.field public final clockinTime:Ljava/lang/String;

.field final employeeNumber:Ljava/lang/String;

.field public final firstName:Ljava/lang/String;

.field final hashIterationCount:Ljava/lang/Integer;

.field final hashedPasscode:Ljava/lang/String;

.field public final initials:Ljava/lang/String;

.field final isAccountOwner:Z

.field final isOwner:Z

.field public final lastName:Ljava/lang/String;

.field final permissions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final roleToken:Ljava/lang/String;

.field final salt:Ljava/lang/String;

.field public final timecardId:Ljava/lang/String;

.field public final token:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 32
    new-instance v0, Lcom/squareup/permissions/EmployeeModel$Factory;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/permissions/EmployeeModel$Factory;-><init>(Lcom/squareup/permissions/EmployeeModel$Creator;)V

    sput-object v0, Lcom/squareup/permissions/Employee;->FACTORY:Lcom/squareup/permissions/EmployeeModel$Factory;

    .line 33
    sget-object v0, Lcom/squareup/permissions/Employee;->FACTORY:Lcom/squareup/permissions/EmployeeModel$Factory;

    sget-object v1, Lcom/squareup/permissions/-$$Lambda$Employee$cRunJR9p6b-HuuOpg_6M9b-UBjc;->INSTANCE:Lcom/squareup/permissions/-$$Lambda$Employee$cRunJR9p6b-HuuOpg_6M9b-UBjc;

    .line 34
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/EmployeeModel$Factory;->get_all_employeesMapper(Lcom/squareup/permissions/EmployeeModel$Get_all_employeesCreator;)Lcom/squareup/permissions/EmployeeModel$Get_all_employeesMapper;

    move-result-object v0

    sput-object v0, Lcom/squareup/permissions/Employee;->MAPPER:Lcom/squareup/sqldelight/prerelease/RowMapper;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZLjava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZZZZ",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 75
    iput-object v1, v0, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    move-object v2, p2

    .line 76
    iput-object v2, v0, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    move v3, p5

    .line 77
    iput-boolean v3, v0, Lcom/squareup/permissions/Employee;->active:Z

    move v3, p6

    .line 78
    iput-boolean v3, v0, Lcom/squareup/permissions/Employee;->canAccessRegisterWithPasscode:Z

    move v3, p7

    .line 79
    iput-boolean v3, v0, Lcom/squareup/permissions/Employee;->canTrackTime:Z

    move v3, p8

    .line 80
    iput-boolean v3, v0, Lcom/squareup/permissions/Employee;->isAccountOwner:Z

    move v3, p9

    .line 81
    iput-boolean v3, v0, Lcom/squareup/permissions/Employee;->isOwner:Z

    .line 82
    invoke-static {p1, p2}, Lcom/squareup/permissions/Employee;->getInitials(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/permissions/Employee;->initials:Ljava/lang/String;

    move-object v1, p3

    .line 83
    iput-object v1, v0, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    move-object v1, p4

    .line 84
    iput-object v1, v0, Lcom/squareup/permissions/Employee;->employeeNumber:Ljava/lang/String;

    move-object v1, p10

    .line 85
    iput-object v1, v0, Lcom/squareup/permissions/Employee;->permissions:Ljava/util/Set;

    move-object v1, p11

    .line 86
    iput-object v1, v0, Lcom/squareup/permissions/Employee;->hashedPasscode:Ljava/lang/String;

    move-object/from16 v1, p12

    .line 87
    iput-object v1, v0, Lcom/squareup/permissions/Employee;->salt:Ljava/lang/String;

    move-object/from16 v1, p13

    .line 88
    iput-object v1, v0, Lcom/squareup/permissions/Employee;->hashIterationCount:Ljava/lang/Integer;

    move-object/from16 v1, p14

    .line 89
    iput-object v1, v0, Lcom/squareup/permissions/Employee;->timecardId:Ljava/lang/String;

    move-object/from16 v1, p15

    .line 90
    iput-object v1, v0, Lcom/squareup/permissions/Employee;->clockinTime:Ljava/lang/String;

    move-object/from16 v1, p16

    .line 91
    iput-object v1, v0, Lcom/squareup/permissions/Employee;->roleToken:Ljava/lang/String;

    return-void
.end method

.method static checkPasscode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 1

    const/16 v0, 0x20

    .line 206
    invoke-static {p0, p2, p3, v0}, Lcom/squareup/encryption/Pbkdf2Generator;->derive(Ljava/lang/String;Ljava/lang/String;II)[B

    move-result-object p0

    const/4 p2, 0x0

    .line 207
    invoke-static {p0, p2}, Lcom/squareup/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 208
    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public static fromEmployeesEntity(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/permissions/Employee;
    .locals 19

    move-object/from16 v0, p0

    .line 218
    new-instance v18, Lcom/squareup/permissions/Employee;

    move-object/from16 v1, v18

    iget-object v2, v0, Lcom/squareup/server/account/protos/EmployeesEntity;->first_name:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/server/account/protos/EmployeesEntity;->last_name:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/server/account/protos/EmployeesEntity;->token:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_number:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/server/account/protos/EmployeesEntity;->active:Ljava/lang/Boolean;

    .line 219
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    iget-object v7, v0, Lcom/squareup/server/account/protos/EmployeesEntity;->can_access_register_with_passcode:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    iget-object v8, v0, Lcom/squareup/server/account/protos/EmployeesEntity;->can_track_time:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    iget-object v9, v0, Lcom/squareup/server/account/protos/EmployeesEntity;->is_account_owner:Ljava/lang/Boolean;

    .line 220
    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    iget-object v10, v0, Lcom/squareup/server/account/protos/EmployeesEntity;->is_owner:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    new-instance v12, Ljava/util/HashSet;

    move-object v11, v12

    iget-object v13, v0, Lcom/squareup/server/account/protos/EmployeesEntity;->permissions:Ljava/util/List;

    invoke-direct {v12, v13}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 221
    invoke-static/range {p0 .. p0}, Lcom/squareup/permissions/Employee;->getHashedPasscode(Lcom/squareup/server/account/protos/EmployeesEntity;)Ljava/lang/String;

    move-result-object v12

    invoke-static/range {p0 .. p0}, Lcom/squareup/permissions/Employee;->getSalt(Lcom/squareup/server/account/protos/EmployeesEntity;)Ljava/lang/String;

    move-result-object v13

    invoke-static/range {p0 .. p0}, Lcom/squareup/permissions/Employee;->getIterations(Lcom/squareup/server/account/protos/EmployeesEntity;)Ljava/lang/Integer;

    move-result-object v14

    invoke-static/range {p0 .. p0}, Lcom/squareup/permissions/Employee;->getTimecardId(Lcom/squareup/server/account/protos/EmployeesEntity;)Ljava/lang/String;

    move-result-object v15

    .line 222
    invoke-static/range {p0 .. p0}, Lcom/squareup/permissions/Employee;->getClockinTime(Lcom/squareup/server/account/protos/EmployeesEntity;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {p0 .. p0}, Lcom/squareup/permissions/Employee;->getRoleToken(Lcom/squareup/server/account/protos/EmployeesEntity;)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v1 .. v17}, Lcom/squareup/permissions/Employee;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZLjava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v18
.end method

.method public static fromSearchTerm(Ljava/lang/String;)Lcom/squareup/permissions/Employee;
    .locals 6

    .line 227
    new-instance v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;-><init>()V

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->active(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    .line 229
    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x2

    const-string v3, " "

    .line 230
    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 232
    array-length v4, v3

    const/4 v5, 0x0

    if-ne v4, v2, :cond_0

    .line 233
    aget-object p0, v3, v5

    invoke-virtual {v0, p0}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->first_name(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object p0

    aget-object v1, v3, v1

    invoke-virtual {p0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->last_name(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    goto :goto_0

    .line 234
    :cond_0
    array-length v2, v3

    if-ne v2, v1, :cond_1

    .line 235
    aget-object p0, v3, v5

    invoke-virtual {v0, p0}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->first_name(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    goto :goto_0

    .line 237
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Splitting non-blank string \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\' yielded "

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length p0, v3

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, " parts"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242
    :cond_2
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/server/account/protos/EmployeesEntity;->populateDefaults()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/permissions/Employee;->fromEmployeesEntity(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/permissions/Employee;

    move-result-object p0

    return-object p0
.end method

.method private static getClockinTime(Lcom/squareup/server/account/protos/EmployeesEntity;)Ljava/lang/String;
    .locals 1

    .line 322
    iget-object v0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    iget-object p0, p0, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;->clockin_time:Ljava/lang/String;

    :goto_0
    return-object p0
.end method

.method private static getHashedPasscode(Lcom/squareup/server/account/protos/EmployeesEntity;)Ljava/lang/String;
    .locals 1

    .line 304
    iget-object v0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    iget-object p0, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->hashed_passcode:Ljava/lang/String;

    :goto_0
    return-object p0
.end method

.method static getInitials(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 212
    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, ""

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    move-object p0, v1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 213
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 214
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, p1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static getIterations(Lcom/squareup/server/account/protos/EmployeesEntity;)Ljava/lang/Integer;
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    iget-object p0, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->iterations:Ljava/lang/Integer;

    :goto_0
    return-object p0
.end method

.method private static getRoleToken(Lcom/squareup/server/account/protos/EmployeesEntity;)Ljava/lang/String;
    .locals 1

    .line 326
    iget-object v0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_role_tokens:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_role_tokens:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->employee_role_tokens:Ljava/util/List;

    const/4 v0, 0x0

    .line 327
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method private static getSalt(Lcom/squareup/server/account/protos/EmployeesEntity;)Ljava/lang/String;
    .locals 1

    .line 309
    iget-object v0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->passcode_only_credential:Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    iget-object p0, p0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;->salt:Ljava/lang/String;

    :goto_0
    return-object p0
.end method

.method public static getShortDisplayName(Lcom/squareup/util/Res;Lcom/squareup/permissions/EmployeeInfo;)Ljava/lang/String;
    .locals 2

    .line 115
    iget-object v0, p1, Lcom/squareup/permissions/EmployeeInfo;->firstName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/permissions/EmployeeInfo;->lastName:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/permissions/EmployeeInfo;->employeeToken:Ljava/lang/String;

    invoke-static {p0, v0, v1, p1}, Lcom/squareup/permissions/Employee;->getShortDisplayName(Lcom/squareup/util/Res;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getShortDisplayName(Lcom/squareup/util/Res;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 179
    invoke-static {p0, p1, p2}, Lcom/squareup/permissions/Employee;->getShortName(Lcom/squareup/util/Res;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 180
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_0

    return-object p1

    :cond_0
    if-eqz p3, :cond_1

    .line 184
    invoke-static {p0}, Lcom/squareup/permissions/Employee;->getUnknownName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 186
    :cond_1
    invoke-static {p0}, Lcom/squareup/permissions/Employee;->getUnassignedName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getShortDisplayNameModel(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/TextModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 151
    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 152
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 155
    new-instance v0, Lcom/squareup/resources/PhraseModel;

    sget v1, Lcom/squareup/employees/R$string;->employee_short_name:I

    invoke-direct {v0, v1}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    const-string v1, "first_name"

    .line 156
    invoke-virtual {v0, v1, p0}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object p0

    const/4 v0, 0x0

    .line 157
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result p1

    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object p1

    const-string v0, "last_initial"

    invoke-virtual {p0, v0, p1}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object p0

    return-object p0

    :cond_0
    if-eqz v0, :cond_1

    .line 161
    new-instance p1, Lcom/squareup/resources/FixedText;

    invoke-direct {p1, p0}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    return-object p1

    :cond_1
    if-eqz v1, :cond_2

    .line 165
    new-instance p0, Lcom/squareup/resources/FixedText;

    invoke-direct {p0, p1}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    return-object p0

    :cond_2
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getShortName(Lcom/squareup/util/Res;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 127
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 128
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 131
    sget v0, Lcom/squareup/employees/R$string;->employee_short_name:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string v0, "first_name"

    .line 132
    invoke-virtual {p0, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const/4 p1, 0x0

    .line 133
    invoke-virtual {p2, p1}, Ljava/lang/String;->charAt(I)C

    move-result p1

    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object p1

    const-string p2, "last_initial"

    invoke-virtual {p0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 134
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    if-eqz v0, :cond_1

    return-object p1

    :cond_1
    if-eqz v1, :cond_2

    return-object p2

    :cond_2
    const/4 p0, 0x0

    return-object p0
.end method

.method private static getTimecardId(Lcom/squareup/server/account/protos/EmployeesEntity;)Ljava/lang/String;
    .locals 1

    .line 318
    iget-object v0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/squareup/server/account/protos/EmployeesEntity;->current_timecard:Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    iget-object p0, p0, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;->id:Ljava/lang/String;

    :goto_0
    return-object p0
.end method

.method public static getUnassignedName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 190
    invoke-static {}, Lcom/squareup/permissions/Employee;->getUnassignedNameRes()I

    move-result v0

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getUnassignedNameRes()I
    .locals 1

    .line 198
    sget v0, Lcom/squareup/employees/R$string;->employee_short_name_unassigned:I

    return v0
.end method

.method public static getUnknownName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 194
    invoke-static {}, Lcom/squareup/permissions/Employee;->getUnknownNameRes()I

    move-result v0

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getUnknownNameRes()I
    .locals 1

    .line 202
    sget v0, Lcom/squareup/employees/R$string;->employee_short_name_unknown:I

    return v0
.end method

.method static synthetic lambda$static$0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/permissions/Employee;
    .locals 19

    .line 39
    invoke-static/range {p15 .. p15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/LinkedHashSet;

    const-string v1, ","

    move-object/from16 v2, p15

    .line 40
    invoke-virtual {v2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    :goto_0
    move-object v12, v0

    .line 41
    new-instance v0, Lcom/squareup/permissions/Employee;

    move-object v2, v0

    invoke-virtual/range {p9 .. p9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 42
    invoke-virtual/range {p10 .. p10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    invoke-virtual/range {p11 .. p11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    invoke-virtual/range {p12 .. p12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    invoke-virtual/range {p13 .. p13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p0

    move-object/from16 v6, p3

    move-object/from16 v13, p4

    move-object/from16 v14, p5

    move-object/from16 v15, p6

    move-object/from16 v16, p7

    move-object/from16 v17, p8

    move-object/from16 v18, p14

    invoke-direct/range {v2 .. v18}, Lcom/squareup/permissions/Employee;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZLjava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public active()Ljava/lang/Boolean;
    .locals 1

    .line 367
    iget-boolean v0, p0, Lcom/squareup/permissions/Employee;->active:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public can_access_register_with_passcode()Ljava/lang/Boolean;
    .locals 1

    .line 371
    iget-boolean v0, p0, Lcom/squareup/permissions/Employee;->canAccessRegisterWithPasscode:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public can_track_time()Ljava/lang/Boolean;
    .locals 1

    .line 375
    iget-boolean v0, p0, Lcom/squareup/permissions/Employee;->canTrackTime:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public clockin_time()Ljava/lang/String;
    .locals 1

    .line 363
    iget-object v0, p0, Lcom/squareup/permissions/Employee;->clockinTime:Ljava/lang/String;

    return-object v0
.end method

.method public employee_number()Ljava/lang/String;
    .locals 1

    .line 343
    iget-object v0, p0, Lcom/squareup/permissions/Employee;->employeeNumber:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-eqz p1, :cond_2

    .line 255
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 257
    :cond_1
    check-cast p1, Lcom/squareup/permissions/Employee;

    .line 258
    iget-object v0, p0, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public first_name()Ljava/lang/String;
    .locals 1

    .line 335
    iget-object v0, p0, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    return-object v0
.end method

.method public group_concat_permission()Ljava/lang/String;
    .locals 2

    .line 391
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Use the permissions field instead."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hasAnyPermission(Ljava/util/Set;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "permissions"

    .line 95
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonEmpty(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    .line 96
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/Permission;

    .line 97
    iget-object v1, p0, Lcom/squareup/permissions/Employee;->permissions:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/squareup/permissions/Permission;->getPermissionString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public hasPasscode(Ljava/lang/String;)Z
    .locals 3

    .line 106
    iget-object v0, p0, Lcom/squareup/permissions/Employee;->hashIterationCount:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/permissions/Employee;->hashedPasscode:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/permissions/Employee;->salt:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 107
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/Employee;->hashedPasscode:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/permissions/Employee;->salt:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/permissions/Employee;->hashIterationCount:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {p1, v0, v1, v2}, Lcom/squareup/permissions/Employee;->checkPasscode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result p1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 262
    iget-object v0, p0, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    return v0
.end method

.method public hashed_passcode()Ljava/lang/String;
    .locals 1

    .line 347
    iget-object v0, p0, Lcom/squareup/permissions/Employee;->hashedPasscode:Ljava/lang/String;

    return-object v0
.end method

.method public is_account_owner()Ljava/lang/Boolean;
    .locals 1

    .line 379
    iget-boolean v0, p0, Lcom/squareup/permissions/Employee;->isAccountOwner:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public is_owner()Ljava/lang/Boolean;
    .locals 1

    .line 383
    iget-boolean v0, p0, Lcom/squareup/permissions/Employee;->isOwner:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public iterations()Ljava/lang/Integer;
    .locals 1

    .line 355
    iget-object v0, p0, Lcom/squareup/permissions/Employee;->hashIterationCount:Ljava/lang/Integer;

    return-object v0
.end method

.method public last_name()Ljava/lang/String;
    .locals 1

    .line 339
    iget-object v0, p0, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    return-object v0
.end method

.method public role_token()Ljava/lang/String;
    .locals 1

    .line 387
    iget-object v0, p0, Lcom/squareup/permissions/Employee;->roleToken:Ljava/lang/String;

    return-object v0
.end method

.method public salt()Ljava/lang/String;
    .locals 1

    .line 351
    iget-object v0, p0, Lcom/squareup/permissions/Employee;->salt:Ljava/lang/String;

    return-object v0
.end method

.method public timecard_id()Ljava/lang/String;
    .locals 1

    .line 359
    iget-object v0, p0, Lcom/squareup/permissions/Employee;->timecardId:Ljava/lang/String;

    return-object v0
.end method

.method public toEmployeeProto()Lcom/squareup/protos/client/Employee;
    .locals 2

    .line 246
    new-instance v0, Lcom/squareup/protos/client/Employee$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/Employee$Builder;-><init>()V

    .line 247
    invoke-virtual {p0}, Lcom/squareup/permissions/Employee;->token()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/Employee$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v0

    .line 248
    invoke-virtual {p0}, Lcom/squareup/permissions/Employee;->first_name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/Employee$Builder;->first_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v0

    .line 249
    invoke-virtual {p0}, Lcom/squareup/permissions/Employee;->last_name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/Employee$Builder;->last_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v0

    .line 250
    invoke-virtual {v0}, Lcom/squareup/protos/client/Employee$Builder;->build()Lcom/squareup/protos/client/Employee;

    move-result-object v0

    return-object v0
.end method

.method public toEmployeesEntity()Lcom/squareup/server/account/protos/EmployeesEntity;
    .locals 4

    .line 274
    new-instance v0, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/permissions/Employee;->timecardId:Ljava/lang/String;

    .line 275
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;->id(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/Employee;->clockinTime:Ljava/lang/String;

    .line 276
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;->clockin_time(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;

    move-result-object v0

    .line 277
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    move-result-object v0

    .line 279
    new-instance v1, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    invoke-direct {v1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/permissions/Employee;->hashedPasscode:Ljava/lang/String;

    .line 281
    invoke-virtual {v1, v2}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->hashed_passcode(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/permissions/Employee;->salt:Ljava/lang/String;

    .line 282
    invoke-virtual {v1, v2}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->salt(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/permissions/Employee;->hashIterationCount:Ljava/lang/Integer;

    .line 283
    invoke-virtual {v1, v2}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->iterations(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object v1

    .line 284
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    move-result-object v1

    .line 286
    new-instance v2, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    invoke-direct {v2}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    .line 287
    invoke-virtual {v2, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->first_name(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    .line 288
    invoke-virtual {v2, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->last_name(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    .line 289
    invoke-virtual {v2, v3}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->token(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v2

    .line 290
    invoke-virtual {v2, v0}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->current_timecard(Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/permissions/Employee;->employeeNumber:Ljava/lang/String;

    .line 291
    invoke-virtual {v0, v2}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->employee_number(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/squareup/permissions/Employee;->active:Z

    .line 292
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->active(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/squareup/permissions/Employee;->canAccessRegisterWithPasscode:Z

    .line 293
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_access_register_with_passcode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/squareup/permissions/Employee;->canTrackTime:Z

    .line 294
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_track_time(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/squareup/permissions/Employee;->isAccountOwner:Z

    .line 295
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_account_owner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/squareup/permissions/Employee;->isOwner:Z

    .line 296
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_owner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/squareup/permissions/Employee;->permissions:Ljava/util/Set;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 297
    invoke-virtual {v0, v2}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->permissions(Ljava/util/List;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    .line 298
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->passcode_only_credential(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/Employee;->roleToken:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 299
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_0

    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->employee_role_tokens(Ljava/util/List;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    .line 300
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object v0

    return-object v0
.end method

.method public token()Ljava/lang/String;
    .locals 1

    .line 331
    iget-object v0, p0, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    return-object v0
.end method
