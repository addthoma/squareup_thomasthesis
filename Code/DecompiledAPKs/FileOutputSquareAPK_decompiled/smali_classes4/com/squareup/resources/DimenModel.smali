.class public interface abstract Lcom/squareup/resources/DimenModel;
.super Ljava/lang/Object;
.source "DimenModels.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0008\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/resources/DimenModel;",
        "Landroid/os/Parcelable;",
        "toFloat",
        "",
        "context",
        "Landroid/content/Context;",
        "toOffset",
        "",
        "toSize",
        "resources_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract toFloat(Landroid/content/Context;)F
.end method

.method public abstract toOffset(Landroid/content/Context;)I
.end method

.method public abstract toSize(Landroid/content/Context;)I
.end method
