.class public interface abstract Lcom/squareup/persistent/PersistentFactory;
.super Ljava/lang/Object;
.source "PersistentFactory.java"


# virtual methods
.method public abstract exists(Ljava/io/File;)Z
.end method

.method public abstract getByteFile(Ljava/io/File;)Lcom/squareup/persistent/Persistent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lcom/squareup/persistent/Persistent<",
            "[B>;"
        }
    .end annotation
.end method

.method public abstract getJsonFile(Ljava/io/File;Ljava/lang/reflect/Type;)Lcom/squareup/persistent/Persistent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/File;",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lcom/squareup/persistent/Persistent<",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract getStringFile(Ljava/io/File;)Lcom/squareup/persistent/Persistent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lcom/squareup/persistent/Persistent<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method
