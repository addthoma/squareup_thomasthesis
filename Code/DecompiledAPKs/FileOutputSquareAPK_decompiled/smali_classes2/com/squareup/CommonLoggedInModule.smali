.class public abstract Lcom/squareup/CommonLoggedInModule;
.super Ljava/lang/Object;
.source "CommonLoggedInModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/buyer/language/BuyerLanguageSelectionLoggedInModule;,
        Lcom/squareup/ui/cart/CartEntryModule;,
        Lcom/squareup/connectedperipherals/ConnectedPeripheralsModule;,
        Lcom/squareup/money/CurrencyMoneyModule;,
        Lcom/squareup/permissions/EmployeeManagementModule;,
        Lcom/squareup/account/LoggedInAccountModule;,
        Lcom/squareup/settings/LoggedInSettingsModule;,
        Lcom/squareup/payment/PaymentModule;,
        Lcom/squareup/print/PrintModule;,
        Lcom/squareup/queue/QueueModule;,
        Lcom/squareup/print/RegisterPrintModule;,
        Lcom/squareup/safetynet/SafetyNetModule;,
        Lcom/squareup/salesreport/print/SalesReportPrintModule;,
        Lcom/squareup/orderentry/SwitchEmployeesSettingsModule;,
        Lcom/squareup/payment/ledger/TransactionLedgerModule$LoggedIn;,
        Lcom/squareup/permissions/PasscodesSettingsModule;,
        Lcom/squareup/receipt/ReceiptPrinterModule;,
        Lcom/squareup/requestingbusinessaddress/RequestingBusinessAddressModule;,
        Lcom/squareup/merchantimages/MerchantImagesModule;,
        Lcom/squareup/crm/RolodexModule;,
        Lcom/squareup/settings/UserDirectoryModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideAutoCaptureControlAlarm()Lcom/squareup/autocapture/AutoCaptureControlAlarm;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 102
    sget-object v0, Lcom/squareup/autocapture/AutoCaptureControlAlarm;->REAL_AUTO_CAPTURE_CONTROL_ALARM:Lcom/squareup/autocapture/AutoCaptureControlAlarm;

    return-object v0
.end method

.method static provideAutoCaptureExecutor()Lcom/squareup/thread/executor/StoppableSerialExecutor;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "AutoCapture"

    const/16 v1, 0xa

    const/4 v2, 0x1

    .line 98
    invoke-static {v0, v1, v2}, Lcom/squareup/thread/executor/Executors;->stoppableNamedThreadExecutor(Ljava/lang/String;IZ)Lcom/squareup/thread/executor/StoppableSerialExecutor;

    move-result-object v0

    return-object v0
.end method

.method static provideBillIdKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/billhistory/model/BillHistoryId;",
            ">;"
        }
    .end annotation

    .line 106
    const-class v0, Lcom/squareup/billhistory/model/BillHistoryId;

    const-string v1, "bill"

    invoke-static {p0, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method

.method static provideCashDrawerShiftBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .param p0    # Lcom/google/gson/Gson;
        .annotation runtime Lcom/squareup/gson/WireGson;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            ">;"
        }
    .end annotation

    .line 139
    const-class v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    const-string v1, "cash-drawer-shift"

    invoke-static {p0, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method

.method public static provideDiscountBundleFactory()Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 124
    new-instance v0, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;-><init>()V

    return-object v0
.end method

.method public static provideDiscountRulesLibraryFactory()Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 119
    new-instance v0, Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;

    invoke-direct {v0}, Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;-><init>()V

    return-object v0
.end method

.method static provideLocaleOverrideFactory(Landroid/app/Application;Ljava/util/Locale;)Lcom/squareup/locale/LocaleOverrideFactory;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 149
    new-instance v0, Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/locale/LocaleOverrideFactory;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    return-object v0
.end method

.method static provideLoggedInTaskInjector(Lcom/squareup/AppDelegate;)Lcom/squareup/tape/TaskInjector;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/AppDelegate;",
            ")",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .line 163
    const-class v0, Lcom/squareup/CommonLoggedInComponent;

    .line 164
    invoke-interface {p0, v0}, Lcom/squareup/AppDelegate;->getLoggedInComponent(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/CommonLoggedInComponent;

    .line 165
    new-instance v0, Lcom/squareup/queue/RetrofitTaskInjector;

    invoke-direct {v0, p0}, Lcom/squareup/queue/RetrofitTaskInjector;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static providePaperSignatureCacheSettings(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation

    .line 112
    new-instance v0, Lcom/squareup/CommonLoggedInModule$1;

    invoke-direct {v0}, Lcom/squareup/CommonLoggedInModule$1;-><init>()V

    .line 113
    invoke-virtual {v0}, Lcom/squareup/CommonLoggedInModule$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v1, "paper_signature_tender_status.json"

    .line 114
    invoke-static {p0, v1, p1, v0}, Lcom/squareup/settings/GsonLocalSetting;->forType(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/reflect/Type;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object p0

    return-object p0
.end method

.method static provideServerClock(Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/util/Clock;Lcom/squareup/logging/RemoteLogger;)Lcom/squareup/account/ServerClock;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 133
    new-instance v0, Lcom/squareup/account/ServerClock;

    .line 134
    invoke-virtual {p0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->serverTimeMinusElapsedRealtime()Lio/reactivex/Observable;

    move-result-object p0

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/account/ServerClock;-><init>(Lio/reactivex/Observable;Lcom/squareup/util/Clock;Lcom/squareup/logging/RemoteLogger;)V

    return-object v0
.end method

.method static provideTenderInEdit(Lcom/squareup/settings/server/Features;Lcom/squareup/payment/AlwaysTenderInEdit;Lcom/squareup/payment/LegacyTenderInEdit;)Lcom/squareup/payment/TenderInEdit;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 155
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_TENDER_IN_EDIT_REFACTOR:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p0, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    move-object p1, p2

    :goto_0
    return-object p1
.end method

.method static providesLoggedInExecutor()Lcom/squareup/thread/executor/StoppableSerialExecutor;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 144
    invoke-static {}, Lcom/squareup/thread/executor/Executors;->stoppableMainThreadExecutor()Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method abstract bindLoggedInScopeNotifier(Lcom/squareup/LoggedInScopeRunner;)Lcom/squareup/LoggedInScopeNotifier;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideNonForwardedPendingPaymentsCounter(Lcom/squareup/payment/pending/PendingTransactionsStore;)Lcom/squareup/payment/NonForwardedPendingTransactionsCounter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTaxEditor(Lcom/squareup/settings/server/RealFeesEditor;)Lcom/squareup/settings/server/FeesEditor;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTransactionMetrics(Lcom/squareup/ui/main/Es1TransactionMetrics;)Lcom/squareup/ui/main/TransactionMetrics;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
