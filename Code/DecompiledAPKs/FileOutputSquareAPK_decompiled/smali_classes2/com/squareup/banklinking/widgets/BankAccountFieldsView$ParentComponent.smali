.class public interface abstract Lcom/squareup/banklinking/widgets/BankAccountFieldsView$ParentComponent;
.super Ljava/lang/Object;
.source "BankAccountFieldsView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/banklinking/widgets/BankAccountFieldsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ParentComponent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$ParentComponent;",
        "",
        "inject",
        "",
        "view",
        "Lcom/squareup/banklinking/widgets/BankAccountFieldsView;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/banklinking/widgets/BankAccountFieldsView;)V
.end method
