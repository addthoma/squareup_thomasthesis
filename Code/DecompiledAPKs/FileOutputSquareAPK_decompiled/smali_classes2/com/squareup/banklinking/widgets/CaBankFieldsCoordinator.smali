.class public final Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;
.super Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;
.source "CaBankFieldsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCaBankFieldsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CaBankFieldsCoordinator.kt\ncom/squareup/banklinking/widgets/CaBankFieldsCoordinator\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n+ 3 ArraysJVM.kt\nkotlin/collections/ArraysKt__ArraysJVMKt\n*L\n1#1,108:1\n3862#2:109\n3940#2,2:110\n37#3,2:112\n*E\n*S KotlinDebug\n*F\n+ 1 CaBankFieldsCoordinator.kt\ncom/squareup/banklinking/widgets/CaBankFieldsCoordinator\n*L\n104#1:109\n104#1,2:110\n104#1,2:112\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0002J\n\u0010\u000c\u001a\u0004\u0018\u00010\nH\u0016J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0018\u0010\u0011\u001a\u00020\u00082\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u000eH\u0016J\u0014\u0010\u0013\u001a\u00020\u0008*\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;",
        "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;",
        "()V",
        "accountField",
        "Landroid/widget/EditText;",
        "institutionField",
        "transitField",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "emptyField",
        "getField",
        "",
        "fieldType",
        "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;",
        "setField",
        "fieldText",
        "setMaxLength",
        "Landroid/widget/TextView;",
        "maxLength",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private accountField:Landroid/widget/EditText;

.field private institutionField:Landroid/widget/EditText;

.field private transitField:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 25
    sget v0, Lcom/squareup/banklinking/R$layout;->bank_account_fields_ca:I

    invoke-direct {p0, v0}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;-><init>(I)V

    return-void
.end method

.method public static final synthetic access$getAccountField$p(Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;)Landroid/widget/EditText;
    .locals 1

    .line 25
    iget-object p0, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->accountField:Landroid/widget/EditText;

    if-nez p0, :cond_0

    const-string v0, "accountField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setAccountField$p(Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;Landroid/widget/EditText;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->accountField:Landroid/widget/EditText;

    return-void
.end method

.method public static final synthetic access$setMaxLength(Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;Landroid/widget/TextView;I)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->setMaxLength(Landroid/widget/TextView;I)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 98
    sget v0, Lcom/squareup/banklinking/R$id;->institution_number_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->institutionField:Landroid/widget/EditText;

    .line 99
    sget v0, Lcom/squareup/banklinking/R$id;->transit_number_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->transitField:Landroid/widget/EditText;

    .line 100
    sget v0, Lcom/squareup/banklinking/R$id;->account_number_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->accountField:Landroid/widget/EditText;

    return-void
.end method

.method private final setMaxLength(Landroid/widget/TextView;I)V
    .locals 7

    .line 104
    invoke-virtual {p1}, Landroid/widget/TextView;->getFilters()[Landroid/text/InputFilter;

    move-result-object v0

    const-string v1, "filters"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 110
    array-length v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_1

    aget-object v5, v0, v4

    .line 104
    instance-of v6, v5, Landroid/text/InputFilter$LengthFilter;

    if-nez v6, :cond_0

    invoke-interface {v1, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 111
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 104
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v0, p2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    invoke-static {v1, v0}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/util/Collection;

    new-array v0, v3, [Landroid/text/InputFilter;

    .line 113
    invoke-interface {p2, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_2

    check-cast p2, [Landroid/text/InputFilter;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    .line 105
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 113
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 6

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-super {p0, p1}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->attach(Landroid/view/View;)V

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->bindViews(Landroid/view/View;)V

    .line 34
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->institutionField:Landroid/widget/EditText;

    const-string v0, "institutionField"

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Landroid/widget/TextView;

    sget-object v1, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->INSTITUTION:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    invoke-virtual {p0, p1, v1}, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->registerFieldChange(Landroid/widget/TextView;Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)V

    .line 35
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->transitField:Landroid/widget/EditText;

    const-string v1, "transitField"

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p1, Landroid/widget/TextView;

    sget-object v2, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->TRANSIT:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    invoke-virtual {p0, p1, v2}, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->registerFieldChange(Landroid/widget/TextView;Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)V

    .line 36
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->accountField:Landroid/widget/EditText;

    const-string v2, "accountField"

    if-nez p1, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast p1, Landroid/widget/TextView;

    sget-object v3, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->ACCOUNT:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    invoke-virtual {p0, p1, v3}, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->registerFieldChange(Landroid/widget/TextView;Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)V

    .line 40
    sget-object p1, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->INSTITUTION:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    new-instance v3, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator$attach$1;

    invoke-direct {v3, p0}, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator$attach$1;-><init>(Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, p1, v3}, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->fieldChanged(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Lkotlin/jvm/functions/Function1;)V

    .line 46
    sget-object p1, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->INSTITUTION:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->fieldChanged(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Lrx/Observable;

    move-result-object p1

    sget-object v3, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator$attach$2;->INSTANCE:Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator$attach$2;

    check-cast v3, Lrx/functions/Func1;

    invoke-virtual {p1, v3}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    const-string v3, "fieldChanged(INSTITUTION\u2026ure\n          }\n        }"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v4, v3, [Landroid/widget/TextView;

    .line 52
    iget-object v5, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->institutionField:Landroid/widget/EditText;

    if-nez v5, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v5, Landroid/widget/TextView;

    const/4 v0, 0x0

    aput-object v5, v4, v0

    .line 45
    invoke-virtual {p0, p1, v4}, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->observeValidation(Lrx/Observable;[Landroid/widget/TextView;)V

    .line 56
    sget-object p1, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->TRANSIT:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->fieldChanged(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Lrx/Observable;

    move-result-object p1

    sget-object v4, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator$attach$3;->INSTANCE:Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator$attach$3;

    check-cast v4, Lrx/functions/Func1;

    invoke-virtual {p1, v4}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    const-string v4, "fieldChanged(TRANSIT).ma\u2026ure\n          }\n        }"

    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v4, v3, [Landroid/widget/TextView;

    .line 62
    iget-object v5, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->transitField:Landroid/widget/EditText;

    if-nez v5, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v5, Landroid/widget/TextView;

    aput-object v5, v4, v0

    .line 55
    invoke-virtual {p0, p1, v4}, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->observeValidation(Lrx/Observable;[Landroid/widget/TextView;)V

    .line 66
    sget-object p1, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->INSTITUTION:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->fieldChanged(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Lrx/Observable;

    move-result-object p1

    sget-object v1, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->ACCOUNT:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    invoke-virtual {p0, v1}, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->fieldChanged(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Lrx/Observable;

    move-result-object v1

    sget-object v4, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator$attach$4;->INSTANCE:Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator$attach$4;

    check-cast v4, Lrx/functions/Func2;

    invoke-static {p1, v1, v4}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    const-string v1, "combineLatest(fieldChang\u2026ure\n          }\n        }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v1, v3, [Landroid/widget/TextView;

    .line 72
    iget-object v3, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->accountField:Landroid/widget/EditText;

    if-nez v3, :cond_5

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v3, Landroid/widget/TextView;

    aput-object v3, v1, v0

    .line 65
    invoke-virtual {p0, p1, v1}, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->observeValidation(Lrx/Observable;[Landroid/widget/TextView;)V

    return-void
.end method

.method public emptyField()Landroid/view/View;
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/TextView;

    .line 77
    iget-object v1, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->institutionField:Landroid/widget/EditText;

    if-nez v1, :cond_0

    const-string v2, "institutionField"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->transitField:Landroid/widget/EditText;

    if-nez v2, :cond_1

    const-string v3, "transitField"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->accountField:Landroid/widget/EditText;

    if-nez v2, :cond_2

    const-string v3, "accountField"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/squareup/util/Views;->getEmptyView([Landroid/widget/TextView;)Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public getField(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Ljava/lang/String;
    .locals 1

    const-string v0, "fieldType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    sget-object v0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const-string p1, ""

    goto :goto_0

    .line 83
    :cond_0
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->accountField:Landroid/widget/EditText;

    if-nez p1, :cond_1

    const-string v0, "accountField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p1, Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 82
    :cond_2
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->transitField:Landroid/widget/EditText;

    if-nez p1, :cond_3

    const-string v0, "transitField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast p1, Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 81
    :cond_4
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->institutionField:Landroid/widget/EditText;

    if-nez p1, :cond_5

    const-string v0, "institutionField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast p1, Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public setField(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Ljava/lang/String;)V
    .locals 1

    const-string v0, "fieldType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fieldText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    sget-object v0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 93
    :cond_0
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->accountField:Landroid/widget/EditText;

    if-nez p1, :cond_1

    const-string v0, "accountField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 92
    :cond_2
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->transitField:Landroid/widget/EditText;

    if-nez p1, :cond_3

    const-string v0, "transitField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 91
    :cond_4
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/CaBankFieldsCoordinator;->institutionField:Landroid/widget/EditText;

    if-nez p1, :cond_5

    const-string v0, "institutionField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method
