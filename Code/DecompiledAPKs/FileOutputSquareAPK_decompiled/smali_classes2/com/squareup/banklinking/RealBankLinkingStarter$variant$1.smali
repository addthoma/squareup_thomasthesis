.class final Lcom/squareup/banklinking/RealBankLinkingStarter$variant$1;
.super Ljava/lang/Object;
.source "RealBankLinkingStarter.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/banklinking/RealBankLinkingStarter;->variant()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/banklinking/BankLinkingStarter$Variant;",
        "kotlin.jvm.PlatformType",
        "state",
        "Lcom/squareup/banklinking/BankAccountSettings$State;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/banklinking/RealBankLinkingStarter;


# direct methods
.method constructor <init>(Lcom/squareup/banklinking/RealBankLinkingStarter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/banklinking/RealBankLinkingStarter$variant$1;->this$0:Lcom/squareup/banklinking/RealBankLinkingStarter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/banklinking/BankAccountSettings$State;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/banklinking/BankLinkingStarter$Variant;",
            ">;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->showLinkBankAccount()Z

    move-result p1

    if-nez p1, :cond_0

    sget-object p1, Lcom/squareup/banklinking/BankLinkingStarter$Variant;->NONE:Lcom/squareup/banklinking/BankLinkingStarter$Variant;

    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1

    .line 49
    :cond_0
    iget-object p1, p0, Lcom/squareup/banklinking/RealBankLinkingStarter$variant$1;->this$0:Lcom/squareup/banklinking/RealBankLinkingStarter;

    invoke-static {p1}, Lcom/squareup/banklinking/RealBankLinkingStarter;->access$getOnboardingType$p(Lcom/squareup/banklinking/RealBankLinkingStarter;)Lcom/squareup/onboarding/OnboardingType;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/onboarding/OnboardingType;->variant()Lio/reactivex/Observable;

    move-result-object p1

    .line 50
    new-instance v0, Lcom/squareup/banklinking/RealBankLinkingStarter$variant$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/banklinking/RealBankLinkingStarter$variant$1$1;-><init>(Lcom/squareup/banklinking/RealBankLinkingStarter$variant$1;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/banklinking/BankAccountSettings$State;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/RealBankLinkingStarter$variant$1;->apply(Lcom/squareup/banklinking/BankAccountSettings$State;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
