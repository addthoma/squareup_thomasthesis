.class final Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$showRendering$4;
.super Ljava/lang/Object;
.source "CheckBankAccountInfoLayoutRunner.kt"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->showRendering(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "hasFocus",
        "",
        "onFocusChange"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $countryCode:Lcom/squareup/CountryCode;

.field final synthetic this$0:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;Lcom/squareup/CountryCode;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$showRendering$4;->this$0:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;

    iput-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$showRendering$4;->$countryCode:Lcom/squareup/CountryCode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 1

    .line 119
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$showRendering$4;->$countryCode:Lcom/squareup/CountryCode;

    sget-object v0, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 120
    iget-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$showRendering$4;->this$0:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;

    invoke-static {p1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->access$getPrimaryInstitutionNumberField$p(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;)Landroid/widget/EditText;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner$showRendering$4;->this$0:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;

    invoke-static {p2}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;->access$getPrimaryInstitutionNumberField$p(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoLayoutRunner;)Landroid/widget/EditText;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/banklinking/RoutingNumberUtil;->validateRoutingNumber(Ljava/lang/String;Landroid/widget/EditText;)V

    :cond_0
    return-void
.end method
