.class final Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealCheckBankAccountInfoWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->checkBankAccountInfoScreen(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/String;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "isSuccessful",
        "",
        "routingNumber",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;

.field final synthetic this$0:Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$2;->this$0:Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;

    iput-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$2;->$props:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$2;->invoke(ZLjava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ZLjava/lang/String;)V
    .locals 2

    const-string v0, "routingNumber"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$2;->this$0:Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;

    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$2;->$props:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;

    invoke-static {v0, v1, p1, p2}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->access$logBankLinkingAttemptEvent(Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;ZLjava/lang/String;)V

    return-void
.end method
