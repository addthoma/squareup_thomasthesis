.class public final Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;
.super Ljava/lang/Object;
.source "GetDirectDebitInfoLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 &2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001&B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0002H\u0002J\u0010\u0010\u001d\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001eH\u0002J\u0008\u0010\u001f\u001a\u00020\u001bH\u0002J\u0010\u0010 \u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020!H\u0002J\u0018\u0010\"\u001a\u00020\u001b2\u0006\u0010#\u001a\u00020\u00022\u0006\u0010$\u001a\u00020%H\u0016R\u0016\u0010\u0006\u001a\n \u0008*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\n \u0008*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000c\u001a\n \u0008*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\n \u0008*\u0004\u0018\u00010\u000e0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n \u0008*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\n \u0008*\u0004\u0018\u00010\u00110\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\n \u0008*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0013\u001a\n \u0008*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0014\u001a\n \u0008*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0015\u001a\n \u0008*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0016\u001a\n \u0008*\u0004\u0018\u00010\u00170\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0018\u001a\n \u0008*\u0004\u0018\u00010\u00190\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "accountNumber",
        "Lcom/squareup/noho/NohoLabel;",
        "kotlin.jvm.PlatformType",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "address",
        "email",
        "failureView",
        "Lcom/squareup/noho/NohoMessageView;",
        "holderName",
        "loadingView",
        "Landroid/widget/ProgressBar;",
        "name",
        "organization",
        "reference",
        "sortCode",
        "submitButton",
        "Lcom/squareup/noho/NohoButton;",
        "successView",
        "Lcom/squareup/noho/NohoScrollView;",
        "configureActionBar",
        "",
        "screen",
        "onFailure",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Failure;",
        "onLoading",
        "onSuccess",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner$Companion;


# instance fields
.field private final accountNumber:Lcom/squareup/noho/NohoLabel;

.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final address:Lcom/squareup/noho/NohoLabel;

.field private final email:Lcom/squareup/noho/NohoLabel;

.field private final failureView:Lcom/squareup/noho/NohoMessageView;

.field private final holderName:Lcom/squareup/noho/NohoLabel;

.field private final loadingView:Landroid/widget/ProgressBar;

.field private final name:Lcom/squareup/noho/NohoLabel;

.field private final organization:Lcom/squareup/noho/NohoLabel;

.field private final reference:Lcom/squareup/noho/NohoLabel;

.field private final sortCode:Lcom/squareup/noho/NohoLabel;

.field private final submitButton:Lcom/squareup/noho/NohoButton;

.field private final successView:Lcom/squareup/noho/NohoScrollView;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->Companion:Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->view:Landroid/view/View;

    .line 27
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 28
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/banklinking/impl/R$id;->loading_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->loadingView:Landroid/widget/ProgressBar;

    .line 29
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/banklinking/impl/R$id;->success_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoScrollView;

    iput-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->successView:Lcom/squareup/noho/NohoScrollView;

    .line 30
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/banklinking/impl/R$id;->failure_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    iput-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->failureView:Lcom/squareup/noho/NohoMessageView;

    .line 31
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/banklinking/impl/R$id;->holder_name:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->holderName:Lcom/squareup/noho/NohoLabel;

    .line 32
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/banklinking/impl/R$id;->account_number:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->accountNumber:Lcom/squareup/noho/NohoLabel;

    .line 33
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/banklinking/impl/R$id;->sort_code:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->sortCode:Lcom/squareup/noho/NohoLabel;

    .line 34
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/banklinking/impl/R$id;->reference:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->reference:Lcom/squareup/noho/NohoLabel;

    .line 35
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/banklinking/impl/R$id;->name:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->name:Lcom/squareup/noho/NohoLabel;

    .line 36
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/banklinking/impl/R$id;->organization:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->organization:Lcom/squareup/noho/NohoLabel;

    .line 37
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/banklinking/impl/R$id;->email:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->email:Lcom/squareup/noho/NohoLabel;

    .line 38
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/banklinking/impl/R$id;->address:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->address:Lcom/squareup/noho/NohoLabel;

    .line 39
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/banklinking/impl/R$id;->submit_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->submitButton:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method private final configureActionBar(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;)V
    .locals 4

    .line 59
    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 56
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 57
    new-instance v2, Lcom/squareup/resources/ResourceString;

    sget v3, Lcom/squareup/onboarding/common/R$string;->add_bank_account:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 58
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner$configureActionBar$1;

    invoke-direct {v3, p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner$configureActionBar$1;-><init>(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 59
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final onFailure(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Failure;)V
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->failureView:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Failure;->getTitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->failureView:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Failure;->getMessage()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setMessage(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->failureView:Lcom/squareup/noho/NohoMessageView;

    new-instance v1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner$onFailure$1;

    invoke-direct {v1, p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner$onFailure$1;-><init>(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Failure;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    const-string v1, "debounceRunnable { screen.onCancel() }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->loadingView:Landroid/widget/ProgressBar;

    const-string v0, "loadingView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 90
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->successView:Lcom/squareup/noho/NohoScrollView;

    const-string v1, "successView"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoScrollView;->setVisibility(I)V

    .line 91
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->failureView:Lcom/squareup/noho/NohoMessageView;

    const-string v0, "failureView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    return-void
.end method

.method private final onLoading()V
    .locals 3

    .line 63
    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->successView:Lcom/squareup/noho/NohoScrollView;

    const-string v1, "successView"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoScrollView;->setVisibility(I)V

    .line 64
    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->failureView:Lcom/squareup/noho/NohoMessageView;

    const-string v2, "failureView"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    .line 65
    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->loadingView:Landroid/widget/ProgressBar;

    const-string v1, "loadingView"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method private final onSuccess(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;)V
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->holderName:Lcom/squareup/noho/NohoLabel;

    const-string v1, "holderName"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;->getHolderName()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->accountNumber:Lcom/squareup/noho/NohoLabel;

    const-string v1, "accountNumber"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;->getAccountNumber()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->sortCode:Lcom/squareup/noho/NohoLabel;

    const-string v1, "sortCode"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;->getSortCode()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->reference:Lcom/squareup/noho/NohoLabel;

    const-string v1, "reference"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;->getReference()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->name:Lcom/squareup/noho/NohoLabel;

    const-string v1, "name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;->getName()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->organization:Lcom/squareup/noho/NohoLabel;

    const-string v1, "organization"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;->getOrganization()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->email:Lcom/squareup/noho/NohoLabel;

    const-string v1, "email"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;->getEmail()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->address:Lcom/squareup/noho/NohoLabel;

    const-string v1, "address"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;->getAddress()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->submitButton:Lcom/squareup/noho/NohoButton;

    new-instance v1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner$onSuccess$1;

    invoke-direct {v1, p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner$onSuccess$1;-><init>(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->loadingView:Landroid/widget/ProgressBar;

    const-string v0, "loadingView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 80
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->failureView:Lcom/squareup/noho/NohoMessageView;

    const-string v1, "failureView"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    .line 81
    iget-object p1, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->successView:Lcom/squareup/noho/NohoScrollView;

    const-string v0, "successView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoScrollView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object p2, p0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner$showRendering$1;-><init>(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 46
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->configureActionBar(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;)V

    .line 48
    invoke-virtual {p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;->getContent()Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;

    move-result-object p2

    .line 49
    instance-of v0, p2, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Loading;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->onLoading()V

    goto :goto_0

    .line 50
    :cond_0
    instance-of v0, p2, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;->getContent()Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;

    move-result-object p1

    check-cast p1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;

    invoke-direct {p0, p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->onSuccess(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;)V

    goto :goto_0

    .line 51
    :cond_1
    instance-of p2, p2, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Failure;

    if-eqz p2, :cond_2

    invoke-virtual {p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;->getContent()Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;

    move-result-object p1

    check-cast p1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Failure;

    invoke-direct {p0, p1}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->onFailure(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Failure;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoLayoutRunner;->showRendering(Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
