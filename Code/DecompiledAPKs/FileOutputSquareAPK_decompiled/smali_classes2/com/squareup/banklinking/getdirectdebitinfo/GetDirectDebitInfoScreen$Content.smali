.class public abstract Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;
.super Ljava/lang/Object;
.source "GetDirectDebitInfoScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Content"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Loading;,
        Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;,
        Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Failure;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u0008\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0018\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007\u0082\u0001\u0003\u000b\u000c\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;",
        "",
        "()V",
        "onCancel",
        "Lkotlin/Function0;",
        "",
        "getOnCancel",
        "()Lkotlin/jvm/functions/Function0;",
        "Failure",
        "Loading",
        "Success",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Loading;",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Success;",
        "Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content$Failure;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/squareup/banklinking/getdirectdebitinfo/GetDirectDebitInfoScreen$Content;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getOnCancel()Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method
