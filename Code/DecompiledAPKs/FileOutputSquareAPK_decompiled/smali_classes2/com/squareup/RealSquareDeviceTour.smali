.class public final Lcom/squareup/RealSquareDeviceTour;
.super Ljava/lang/Object;
.source "RealSquareDeviceTour.kt"

# interfaces
.implements Lcom/squareup/SquareDeviceTour;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0012\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/RealSquareDeviceTour;",
        "Lcom/squareup/SquareDeviceTour;",
        "home",
        "Lcom/squareup/ui/main/Home;",
        "squareDeviceTourSettings",
        "Lcom/squareup/SquareDeviceTourSettings;",
        "squareDeviceTourRedirector",
        "Lcom/squareup/SquareDeviceTourRedirector;",
        "(Lcom/squareup/ui/main/Home;Lcom/squareup/SquareDeviceTourSettings;Lcom/squareup/SquareDeviceTourRedirector;)V",
        "redirectForTour",
        "Lcom/squareup/container/RedirectStep$Result;",
        "traversal",
        "Lflow/Traversal;",
        "square-device-tour_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final home:Lcom/squareup/ui/main/Home;

.field private final squareDeviceTourRedirector:Lcom/squareup/SquareDeviceTourRedirector;

.field private final squareDeviceTourSettings:Lcom/squareup/SquareDeviceTourSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/Home;Lcom/squareup/SquareDeviceTourSettings;Lcom/squareup/SquareDeviceTourRedirector;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "home"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squareDeviceTourSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squareDeviceTourRedirector"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/RealSquareDeviceTour;->home:Lcom/squareup/ui/main/Home;

    iput-object p2, p0, Lcom/squareup/RealSquareDeviceTour;->squareDeviceTourSettings:Lcom/squareup/SquareDeviceTourSettings;

    iput-object p3, p0, Lcom/squareup/RealSquareDeviceTour;->squareDeviceTourRedirector:Lcom/squareup/SquareDeviceTourRedirector;

    return-void
.end method


# virtual methods
.method public redirectForTour(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 3

    const-string v0, "traversal"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget-object v0, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    .line 19
    iget-object v1, p0, Lcom/squareup/RealSquareDeviceTour;->home:Lcom/squareup/ui/main/Home;

    iget-object v2, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-interface {v1, v2}, Lcom/squareup/ui/main/Home;->getHomeScreens(Lflow/History;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1, v0}, Lkotlin/collections/CollectionsKt;->contains(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 21
    iget-object v0, p0, Lcom/squareup/RealSquareDeviceTour;->squareDeviceTourSettings:Lcom/squareup/SquareDeviceTourSettings;

    invoke-virtual {v0}, Lcom/squareup/SquareDeviceTourSettings;->shouldShowTour()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22
    iget-object v0, p0, Lcom/squareup/RealSquareDeviceTour;->squareDeviceTourSettings:Lcom/squareup/SquareDeviceTourSettings;

    invoke-virtual {v0}, Lcom/squareup/SquareDeviceTourSettings;->notifyTourShowing()V

    .line 24
    iget-object v0, p0, Lcom/squareup/RealSquareDeviceTour;->squareDeviceTourSettings:Lcom/squareup/SquareDeviceTourSettings;

    invoke-virtual {v0}, Lcom/squareup/SquareDeviceTourSettings;->shouldShowDeviceTour()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/squareup/RealSquareDeviceTour;->squareDeviceTourRedirector:Lcom/squareup/SquareDeviceTourRedirector;

    invoke-interface {v0, p1}, Lcom/squareup/SquareDeviceTourRedirector;->redirectForDeviceTour(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;

    move-result-object p1

    goto :goto_0

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/squareup/RealSquareDeviceTour;->squareDeviceTourRedirector:Lcom/squareup/SquareDeviceTourRedirector;

    invoke-interface {v0, p1}, Lcom/squareup/SquareDeviceTourRedirector;->redirectForFeatureTour(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method
