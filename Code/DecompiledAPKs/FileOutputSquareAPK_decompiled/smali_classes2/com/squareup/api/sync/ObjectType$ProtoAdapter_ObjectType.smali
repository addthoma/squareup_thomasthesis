.class final Lcom/squareup/api/sync/ObjectType$ProtoAdapter_ObjectType;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ObjectType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/ObjectType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ObjectType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/api/sync/ObjectType;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 151
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/api/sync/ObjectType;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/sync/ObjectType;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 172
    new-instance v0, Lcom/squareup/api/sync/ObjectType$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/ObjectType$Builder;-><init>()V

    .line 173
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 174
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/16 v4, 0x65

    if-eq v3, v4, :cond_2

    const/16 v4, 0x67

    if-eq v3, v4, :cond_1

    const/16 v4, 0x6d

    if-eq v3, v4, :cond_0

    .line 201
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 194
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/agenda/AgendaType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/agenda/AgendaType;

    invoke-virtual {v0, v4}, Lcom/squareup/api/sync/ObjectType$Builder;->agenda_type(Lcom/squareup/protos/agenda/AgendaType;)Lcom/squareup/api/sync/ObjectType$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 196
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/api/sync/ObjectType$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 186
    :cond_1
    :try_start_1
    sget-object v4, Lcom/squareup/api/items/ExternalType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/ExternalType;

    invoke-virtual {v0, v4}, Lcom/squareup/api/sync/ObjectType$Builder;->external_type(Lcom/squareup/api/items/ExternalType;)Lcom/squareup/api/sync/ObjectType$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 188
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/api/sync/ObjectType$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 178
    :cond_2
    :try_start_2
    sget-object v4, Lcom/squareup/api/items/Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/Type;

    invoke-virtual {v0, v4}, Lcom/squareup/api/sync/ObjectType$Builder;->type(Lcom/squareup/api/items/Type;)Lcom/squareup/api/sync/ObjectType$Builder;
    :try_end_2
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v4

    .line 180
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/api/sync/ObjectType$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 205
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectType$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 206
    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectType$Builder;->build()Lcom/squareup/api/sync/ObjectType;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 149
    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/ObjectType$ProtoAdapter_ObjectType;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/sync/ObjectType;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/sync/ObjectType;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 164
    sget-object v0, Lcom/squareup/protos/agenda/AgendaType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectType;->agenda_type:Lcom/squareup/protos/agenda/AgendaType;

    const/16 v2, 0x6d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 165
    sget-object v0, Lcom/squareup/api/items/Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    const/16 v2, 0x65

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 166
    sget-object v0, Lcom/squareup/api/items/ExternalType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/sync/ObjectType;->external_type:Lcom/squareup/api/items/ExternalType;

    const/16 v2, 0x67

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 167
    invoke-virtual {p2}, Lcom/squareup/api/sync/ObjectType;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 149
    check-cast p2, Lcom/squareup/api/sync/ObjectType;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/api/sync/ObjectType$ProtoAdapter_ObjectType;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/sync/ObjectType;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/api/sync/ObjectType;)I
    .locals 4

    .line 156
    sget-object v0, Lcom/squareup/protos/agenda/AgendaType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/sync/ObjectType;->agenda_type:Lcom/squareup/protos/agenda/AgendaType;

    const/16 v2, 0x6d

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/api/items/Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    const/16 v3, 0x65

    .line 157
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/ExternalType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectType;->external_type:Lcom/squareup/api/items/ExternalType;

    const/16 v3, 0x67

    .line 158
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 159
    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectType;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 149
    check-cast p1, Lcom/squareup/api/sync/ObjectType;

    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/ObjectType$ProtoAdapter_ObjectType;->encodedSize(Lcom/squareup/api/sync/ObjectType;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/api/sync/ObjectType;)Lcom/squareup/api/sync/ObjectType;
    .locals 0

    .line 211
    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectType;->newBuilder()Lcom/squareup/api/sync/ObjectType$Builder;

    move-result-object p1

    .line 212
    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectType$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 213
    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectType$Builder;->build()Lcom/squareup/api/sync/ObjectType;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 149
    check-cast p1, Lcom/squareup/api/sync/ObjectType;

    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/ObjectType$ProtoAdapter_ObjectType;->redact(Lcom/squareup/api/sync/ObjectType;)Lcom/squareup/api/sync/ObjectType;

    move-result-object p1

    return-object p1
.end method
