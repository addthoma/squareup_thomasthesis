.class public final enum Lcom/squareup/api/sync/SyncErrorCode;
.super Ljava/lang/Enum;
.source "SyncErrorCode.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/sync/SyncErrorCode$ProtoAdapter_SyncErrorCode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/sync/SyncErrorCode;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/sync/SyncErrorCode;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/sync/SyncErrorCode;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CLEAR_AND_RETRY:Lcom/squareup/api/sync/SyncErrorCode;

.field public static final enum CONSISTENCY_ERROR:Lcom/squareup/api/sync/SyncErrorCode;

.field public static final enum FIELD_VALIDATION_ERROR:Lcom/squareup/api/sync/SyncErrorCode;

.field public static final enum SESSION_EXPIRED:Lcom/squareup/api/sync/SyncErrorCode;

.field public static final enum UNAUTHORIZED:Lcom/squareup/api/sync/SyncErrorCode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 11
    new-instance v0, Lcom/squareup/api/sync/SyncErrorCode;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "SESSION_EXPIRED"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/api/sync/SyncErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/SyncErrorCode;->SESSION_EXPIRED:Lcom/squareup/api/sync/SyncErrorCode;

    .line 13
    new-instance v0, Lcom/squareup/api/sync/SyncErrorCode;

    const/4 v3, 0x2

    const-string v4, "UNAUTHORIZED"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/api/sync/SyncErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/SyncErrorCode;->UNAUTHORIZED:Lcom/squareup/api/sync/SyncErrorCode;

    .line 15
    new-instance v0, Lcom/squareup/api/sync/SyncErrorCode;

    const/4 v4, 0x3

    const-string v5, "CONSISTENCY_ERROR"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/api/sync/SyncErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/SyncErrorCode;->CONSISTENCY_ERROR:Lcom/squareup/api/sync/SyncErrorCode;

    .line 20
    new-instance v0, Lcom/squareup/api/sync/SyncErrorCode;

    const/4 v5, 0x4

    const-string v6, "CLEAR_AND_RETRY"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/api/sync/SyncErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/SyncErrorCode;->CLEAR_AND_RETRY:Lcom/squareup/api/sync/SyncErrorCode;

    .line 22
    new-instance v0, Lcom/squareup/api/sync/SyncErrorCode;

    const/4 v6, 0x5

    const-string v7, "FIELD_VALIDATION_ERROR"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/api/sync/SyncErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/SyncErrorCode;->FIELD_VALIDATION_ERROR:Lcom/squareup/api/sync/SyncErrorCode;

    new-array v0, v6, [Lcom/squareup/api/sync/SyncErrorCode;

    .line 10
    sget-object v6, Lcom/squareup/api/sync/SyncErrorCode;->SESSION_EXPIRED:Lcom/squareup/api/sync/SyncErrorCode;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/api/sync/SyncErrorCode;->UNAUTHORIZED:Lcom/squareup/api/sync/SyncErrorCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/sync/SyncErrorCode;->CONSISTENCY_ERROR:Lcom/squareup/api/sync/SyncErrorCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/api/sync/SyncErrorCode;->CLEAR_AND_RETRY:Lcom/squareup/api/sync/SyncErrorCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/api/sync/SyncErrorCode;->FIELD_VALIDATION_ERROR:Lcom/squareup/api/sync/SyncErrorCode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/api/sync/SyncErrorCode;->$VALUES:[Lcom/squareup/api/sync/SyncErrorCode;

    .line 24
    new-instance v0, Lcom/squareup/api/sync/SyncErrorCode$ProtoAdapter_SyncErrorCode;

    invoke-direct {v0}, Lcom/squareup/api/sync/SyncErrorCode$ProtoAdapter_SyncErrorCode;-><init>()V

    sput-object v0, Lcom/squareup/api/sync/SyncErrorCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    iput p3, p0, Lcom/squareup/api/sync/SyncErrorCode;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/sync/SyncErrorCode;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 41
    :cond_0
    sget-object p0, Lcom/squareup/api/sync/SyncErrorCode;->FIELD_VALIDATION_ERROR:Lcom/squareup/api/sync/SyncErrorCode;

    return-object p0

    .line 40
    :cond_1
    sget-object p0, Lcom/squareup/api/sync/SyncErrorCode;->CLEAR_AND_RETRY:Lcom/squareup/api/sync/SyncErrorCode;

    return-object p0

    .line 39
    :cond_2
    sget-object p0, Lcom/squareup/api/sync/SyncErrorCode;->CONSISTENCY_ERROR:Lcom/squareup/api/sync/SyncErrorCode;

    return-object p0

    .line 38
    :cond_3
    sget-object p0, Lcom/squareup/api/sync/SyncErrorCode;->UNAUTHORIZED:Lcom/squareup/api/sync/SyncErrorCode;

    return-object p0

    .line 37
    :cond_4
    sget-object p0, Lcom/squareup/api/sync/SyncErrorCode;->SESSION_EXPIRED:Lcom/squareup/api/sync/SyncErrorCode;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/sync/SyncErrorCode;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/api/sync/SyncErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/sync/SyncErrorCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/sync/SyncErrorCode;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/api/sync/SyncErrorCode;->$VALUES:[Lcom/squareup/api/sync/SyncErrorCode;

    invoke-virtual {v0}, [Lcom/squareup/api/sync/SyncErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/sync/SyncErrorCode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 48
    iget v0, p0, Lcom/squareup/api/sync/SyncErrorCode;->value:I

    return v0
.end method
