.class public final Lcom/squareup/api/sync/GetRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/GetRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/sync/GetRequest;",
        "Lcom/squareup/api/sync/GetRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public applied_bazaar_version:Ljava/lang/Long;

.field public applied_server_version:Ljava/lang/Long;

.field public max_batch_size:Ljava/lang/Long;

.field public pagination_token:Ljava/lang/String;

.field public sync_token:Ljava/lang/String;

.field public use_pagination_in_initial_sync:Ljava/lang/Boolean;

.field public visibility:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 212
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public applied_bazaar_version(Ljava/lang/Long;)Lcom/squareup/api/sync/GetRequest$Builder;
    .locals 0

    .line 253
    iput-object p1, p0, Lcom/squareup/api/sync/GetRequest$Builder;->applied_bazaar_version:Ljava/lang/Long;

    return-object p0
.end method

.method public applied_server_version(Ljava/lang/Long;)Lcom/squareup/api/sync/GetRequest$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/api/sync/GetRequest$Builder;->applied_server_version:Ljava/lang/Long;

    return-object p0
.end method

.method public build()Lcom/squareup/api/sync/GetRequest;
    .locals 10

    .line 280
    new-instance v9, Lcom/squareup/api/sync/GetRequest;

    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest$Builder;->applied_server_version:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/api/sync/GetRequest$Builder;->max_batch_size:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/api/sync/GetRequest$Builder;->use_pagination_in_initial_sync:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/api/sync/GetRequest$Builder;->pagination_token:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/api/sync/GetRequest$Builder;->sync_token:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/api/sync/GetRequest$Builder;->applied_bazaar_version:Ljava/lang/Long;

    iget-object v7, p0, Lcom/squareup/api/sync/GetRequest$Builder;->visibility:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/api/sync/GetRequest;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 197
    invoke-virtual {p0}, Lcom/squareup/api/sync/GetRequest$Builder;->build()Lcom/squareup/api/sync/GetRequest;

    move-result-object v0

    return-object v0
.end method

.method public max_batch_size(Ljava/lang/Long;)Lcom/squareup/api/sync/GetRequest$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/squareup/api/sync/GetRequest$Builder;->max_batch_size:Ljava/lang/Long;

    return-object p0
.end method

.method public pagination_token(Ljava/lang/String;)Lcom/squareup/api/sync/GetRequest$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/api/sync/GetRequest$Builder;->pagination_token:Ljava/lang/String;

    return-object p0
.end method

.method public sync_token(Ljava/lang/String;)Lcom/squareup/api/sync/GetRequest$Builder;
    .locals 0

    .line 248
    iput-object p1, p0, Lcom/squareup/api/sync/GetRequest$Builder;->sync_token:Ljava/lang/String;

    return-object p0
.end method

.method public use_pagination_in_initial_sync(Ljava/lang/Boolean;)Lcom/squareup/api/sync/GetRequest$Builder;
    .locals 0

    .line 235
    iput-object p1, p0, Lcom/squareup/api/sync/GetRequest$Builder;->use_pagination_in_initial_sync:Ljava/lang/Boolean;

    return-object p0
.end method

.method public visibility(Ljava/lang/Integer;)Lcom/squareup/api/sync/GetRequest$Builder;
    .locals 0

    .line 274
    iput-object p1, p0, Lcom/squareup/api/sync/GetRequest$Builder;->visibility:Ljava/lang/Integer;

    return-object p0
.end method
