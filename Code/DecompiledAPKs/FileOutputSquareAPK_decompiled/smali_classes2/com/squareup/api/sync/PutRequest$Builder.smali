.class public final Lcom/squareup/api/sync/PutRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PutRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/PutRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/sync/PutRequest;",
        "Lcom/squareup/api/sync/PutRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public OBSOLETE_skip_client_state_validation:Ljava/lang/Boolean;

.field public client_state:Lcom/squareup/api/sync/WritableSessionState;

.field public objects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 134
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 135
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/sync/PutRequest$Builder;->objects:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public OBSOLETE_skip_client_state_validation(Ljava/lang/Boolean;)Lcom/squareup/api/sync/PutRequest$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 169
    iput-object p1, p0, Lcom/squareup/api/sync/PutRequest$Builder;->OBSOLETE_skip_client_state_validation:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/api/sync/PutRequest;
    .locals 5

    .line 175
    new-instance v0, Lcom/squareup/api/sync/PutRequest;

    iget-object v1, p0, Lcom/squareup/api/sync/PutRequest$Builder;->objects:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/api/sync/PutRequest$Builder;->client_state:Lcom/squareup/api/sync/WritableSessionState;

    iget-object v3, p0, Lcom/squareup/api/sync/PutRequest$Builder;->OBSOLETE_skip_client_state_validation:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/api/sync/PutRequest;-><init>(Ljava/util/List;Lcom/squareup/api/sync/WritableSessionState;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 127
    invoke-virtual {p0}, Lcom/squareup/api/sync/PutRequest$Builder;->build()Lcom/squareup/api/sync/PutRequest;

    move-result-object v0

    return-object v0
.end method

.method public client_state(Lcom/squareup/api/sync/WritableSessionState;)Lcom/squareup/api/sync/PutRequest$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 159
    iput-object p1, p0, Lcom/squareup/api/sync/PutRequest$Builder;->client_state:Lcom/squareup/api/sync/WritableSessionState;

    return-object p0
.end method

.method public objects(Ljava/util/List;)Lcom/squareup/api/sync/PutRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ">;)",
            "Lcom/squareup/api/sync/PutRequest$Builder;"
        }
    .end annotation

    .line 142
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 143
    iput-object p1, p0, Lcom/squareup/api/sync/PutRequest$Builder;->objects:Ljava/util/List;

    return-object p0
.end method
