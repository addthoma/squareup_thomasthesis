.class public Lcom/squareup/api/ClientInfo;
.super Ljava/lang/Object;
.source "ClientInfo.java"


# instance fields
.field public final browserApplicationId:Ljava/lang/String;

.field public final clientId:Ljava/lang/String;

.field public final isWebRequest:Z

.field public final packageName:Ljava/lang/String;

.field public final webCallbackUri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 28
    invoke-virtual {p1}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object p1

    const-string v1, "callback_url"

    .line 29
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/api/ClientInfo;->webCallbackUri:Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 30
    :cond_0
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/api/ClientInfo;->packageName:Ljava/lang/String;

    const-string p1, "com.android.browser.application_id"

    .line 31
    invoke-virtual {v0, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/ClientInfo;->browserApplicationId:Ljava/lang/String;

    .line 32
    iget-object p1, p0, Lcom/squareup/api/ClientInfo;->browserApplicationId:Ljava/lang/String;

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    iput-boolean p1, p0, Lcom/squareup/api/ClientInfo;->isWebRequest:Z

    .line 33
    iget-boolean p1, p0, Lcom/squareup/api/ClientInfo;->isWebRequest:Z

    if-eqz p1, :cond_2

    const-string p1, "client_id"

    .line 34
    invoke-virtual {v0, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_2
    const-string p1, "com.squareup.pos.CLIENT_ID"

    .line 35
    invoke-virtual {v0, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :goto_2
    iput-object p1, p0, Lcom/squareup/api/ClientInfo;->clientId:Ljava/lang/String;

    return-void
.end method
