.class public Lcom/squareup/api/LegacyApiKeys$V2;
.super Ljava/lang/Object;
.source "LegacyApiKeys.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/LegacyApiKeys;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "V2"
.end annotation


# static fields
.field public static final ERROR_UNSUPPORTED_WEB_API_VERSION:Ljava/lang/String; = "com.squareup.pos.UNSUPPORTED_WEB_API_VERSION"

.field public static final EXTRA_AUTO_RETURN_TIMEOUT_MS:Ljava/lang/String; = "com.squareup.pos.AUTO_RETURN_TIMEOUT_MS"

.field public static final EXTRA_REQUEST_METADATA:Ljava/lang/String; = "com.squareup.pos.REQUEST_METADATA"

.field public static final EXTRA_TENDER_CARD:Ljava/lang/String; = "com.squareup.pos.TENDER_CARD"

.field public static final EXTRA_WEB_CALLBACK_URI:Ljava/lang/String; = "com.squareup.pos.WEB_CALLBACK_URI"

.field private static final NAMESPACE:Ljava/lang/String; = "com.squareup.pos."

.field public static final RESULT_CLIENT_TRANSACTION_ID:Ljava/lang/String; = "com.squareup.pos.CLIENT_TRANSACTION_ID"

.field public static final RESULT_REQUEST_METADATA:Ljava/lang/String; = "com.squareup.pos.REQUEST_METADATA"

.field public static final RESULT_SERVER_TRANSACTION_ID:Ljava/lang/String; = "com.squareup.pos.SERVER_TRANSACTION_ID"


# instance fields
.field final synthetic this$0:Lcom/squareup/api/LegacyApiKeys;


# direct methods
.method public constructor <init>(Lcom/squareup/api/LegacyApiKeys;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/api/LegacyApiKeys$V2;->this$0:Lcom/squareup/api/LegacyApiKeys;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
