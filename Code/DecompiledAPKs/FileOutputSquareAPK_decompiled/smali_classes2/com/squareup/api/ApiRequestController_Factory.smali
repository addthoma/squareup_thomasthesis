.class public final Lcom/squareup/api/ApiRequestController_Factory;
.super Ljava/lang/Object;
.source "ApiRequestController_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/api/ApiRequestController;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiSessionLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiSessionLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final latestApiSequenceUuidProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiSessionLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/api/ApiRequestController_Factory;->apiSessionLoggerProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/api/ApiRequestController_Factory;->pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/api/ApiRequestController_Factory;->latestApiSequenceUuidProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/api/ApiRequestController_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiSessionLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/squareup/api/ApiRequestController_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/api/ApiRequestController_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/api/ApiRequestController_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/api/ApiSessionLogger;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/settings/LocalSetting;)Lcom/squareup/api/ApiRequestController;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/ApiSessionLogger;",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/api/ApiRequestController;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/api/ApiRequestController;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/api/ApiRequestController;-><init>(Lcom/squareup/api/ApiSessionLogger;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/settings/LocalSetting;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/api/ApiRequestController;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController_Factory;->apiSessionLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ApiSessionLogger;

    iget-object v1, p0, Lcom/squareup/api/ApiRequestController_Factory;->pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/pauses/PauseAndResumeRegistrar;

    iget-object v2, p0, Lcom/squareup/api/ApiRequestController_Factory;->latestApiSequenceUuidProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/LocalSetting;

    invoke-static {v0, v1, v2}, Lcom/squareup/api/ApiRequestController_Factory;->newInstance(Lcom/squareup/api/ApiSessionLogger;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/settings/LocalSetting;)Lcom/squareup/api/ApiRequestController;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/api/ApiRequestController_Factory;->get()Lcom/squareup/api/ApiRequestController;

    move-result-object v0

    return-object v0
.end method
