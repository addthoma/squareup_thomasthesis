.class public final Lcom/squareup/api/FingerprintVerifier_Factory;
.super Ljava/lang/Object;
.source "FingerprintVerifier_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/api/FingerprintVerifier;",
        ">;"
    }
.end annotation


# instance fields
.field private final packageManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/pm/PackageManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/pm/PackageManager;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/api/FingerprintVerifier_Factory;->packageManagerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/api/FingerprintVerifier_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/pm/PackageManager;",
            ">;)",
            "Lcom/squareup/api/FingerprintVerifier_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/api/FingerprintVerifier_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/api/FingerprintVerifier_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/content/pm/PackageManager;)Lcom/squareup/api/FingerprintVerifier;
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/api/FingerprintVerifier;

    invoke-direct {v0, p0}, Lcom/squareup/api/FingerprintVerifier;-><init>(Landroid/content/pm/PackageManager;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/api/FingerprintVerifier;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/api/FingerprintVerifier_Factory;->packageManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManager;

    invoke-static {v0}, Lcom/squareup/api/FingerprintVerifier_Factory;->newInstance(Landroid/content/pm/PackageManager;)Lcom/squareup/api/FingerprintVerifier;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/api/FingerprintVerifier_Factory;->get()Lcom/squareup/api/FingerprintVerifier;

    move-result-object v0

    return-object v0
.end method
