.class public final Lcom/squareup/api/ClientSettingsValidator_Factory;
.super Ljava/lang/Object;
.source "ClientSettingsValidator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/api/ClientSettingsValidator;",
        ">;"
    }
.end annotation


# instance fields
.field private final clientSettingsCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ClientSettingsCache;",
            ">;"
        }
    .end annotation
.end field

.field private final connectServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/api/ConnectService;",
            ">;"
        }
    .end annotation
.end field

.field private final fileSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final fingerprintVerifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/FingerprintVerifier;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final resourcesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/api/ConnectService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/FingerprintVerifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ClientSettingsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/api/ClientSettingsValidator_Factory;->connectServiceProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/api/ClientSettingsValidator_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/api/ClientSettingsValidator_Factory;->fileSchedulerProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/api/ClientSettingsValidator_Factory;->fingerprintVerifierProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/api/ClientSettingsValidator_Factory;->clientSettingsCacheProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p6, p0, Lcom/squareup/api/ClientSettingsValidator_Factory;->resourcesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/api/ClientSettingsValidator_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/api/ConnectService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/FingerprintVerifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ClientSettingsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;)",
            "Lcom/squareup/api/ClientSettingsValidator_Factory;"
        }
    .end annotation

    .line 55
    new-instance v7, Lcom/squareup/api/ClientSettingsValidator_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/api/ClientSettingsValidator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/server/api/ConnectService;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/api/FingerprintVerifier;Lcom/squareup/api/ClientSettingsCache;Landroid/content/res/Resources;)Lcom/squareup/api/ClientSettingsValidator;
    .locals 8

    .line 61
    new-instance v7, Lcom/squareup/api/ClientSettingsValidator;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/api/ClientSettingsValidator;-><init>(Lcom/squareup/server/api/ConnectService;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/api/FingerprintVerifier;Lcom/squareup/api/ClientSettingsCache;Landroid/content/res/Resources;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/api/ClientSettingsValidator;
    .locals 7

    .line 46
    iget-object v0, p0, Lcom/squareup/api/ClientSettingsValidator_Factory;->connectServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/server/api/ConnectService;

    iget-object v0, p0, Lcom/squareup/api/ClientSettingsValidator_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/api/ClientSettingsValidator_Factory;->fileSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/api/ClientSettingsValidator_Factory;->fingerprintVerifierProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/api/FingerprintVerifier;

    iget-object v0, p0, Lcom/squareup/api/ClientSettingsValidator_Factory;->clientSettingsCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/api/ClientSettingsCache;

    iget-object v0, p0, Lcom/squareup/api/ClientSettingsValidator_Factory;->resourcesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/content/res/Resources;

    invoke-static/range {v1 .. v6}, Lcom/squareup/api/ClientSettingsValidator_Factory;->newInstance(Lcom/squareup/server/api/ConnectService;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/api/FingerprintVerifier;Lcom/squareup/api/ClientSettingsCache;Landroid/content/res/Resources;)Lcom/squareup/api/ClientSettingsValidator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/api/ClientSettingsValidator_Factory;->get()Lcom/squareup/api/ClientSettingsValidator;

    move-result-object v0

    return-object v0
.end method
