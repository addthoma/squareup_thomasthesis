.class public final Lcom/squareup/api/rpc/Error$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Error.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/rpc/Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/rpc/Error;",
        "Lcom/squareup/api/rpc/Error$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public description:Ljava/lang/String;

.field public field_validation_error_code:Lcom/squareup/api/sync/FieldValidationErrorCode;

.field public invalid_field_name:Ljava/lang/String;

.field public invalid_object_id:Lcom/squareup/api/sync/ObjectId;

.field public server_error_code:Lcom/squareup/api/rpc/Error$ServerErrorCode;

.field public sync_error_code:Lcom/squareup/api/sync/SyncErrorCode;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 183
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/rpc/Error;
    .locals 9

    .line 233
    new-instance v8, Lcom/squareup/api/rpc/Error;

    iget-object v1, p0, Lcom/squareup/api/rpc/Error$Builder;->description:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/rpc/Error$Builder;->server_error_code:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    iget-object v3, p0, Lcom/squareup/api/rpc/Error$Builder;->sync_error_code:Lcom/squareup/api/sync/SyncErrorCode;

    iget-object v4, p0, Lcom/squareup/api/rpc/Error$Builder;->field_validation_error_code:Lcom/squareup/api/sync/FieldValidationErrorCode;

    iget-object v5, p0, Lcom/squareup/api/rpc/Error$Builder;->invalid_object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v6, p0, Lcom/squareup/api/rpc/Error$Builder;->invalid_field_name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/api/rpc/Error;-><init>(Ljava/lang/String;Lcom/squareup/api/rpc/Error$ServerErrorCode;Lcom/squareup/api/sync/SyncErrorCode;Lcom/squareup/api/sync/FieldValidationErrorCode;Lcom/squareup/api/sync/ObjectId;Ljava/lang/String;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 170
    invoke-virtual {p0}, Lcom/squareup/api/rpc/Error$Builder;->build()Lcom/squareup/api/rpc/Error;

    move-result-object v0

    return-object v0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/api/rpc/Error$Builder;
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/squareup/api/rpc/Error$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public field_validation_error_code(Lcom/squareup/api/sync/FieldValidationErrorCode;)Lcom/squareup/api/rpc/Error$Builder;
    .locals 0

    .line 212
    iput-object p1, p0, Lcom/squareup/api/rpc/Error$Builder;->field_validation_error_code:Lcom/squareup/api/sync/FieldValidationErrorCode;

    return-object p0
.end method

.method public invalid_field_name(Ljava/lang/String;)Lcom/squareup/api/rpc/Error$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/api/rpc/Error$Builder;->invalid_field_name:Ljava/lang/String;

    return-object p0
.end method

.method public invalid_object_id(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/rpc/Error$Builder;
    .locals 0

    .line 222
    iput-object p1, p0, Lcom/squareup/api/rpc/Error$Builder;->invalid_object_id:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public server_error_code(Lcom/squareup/api/rpc/Error$ServerErrorCode;)Lcom/squareup/api/rpc/Error$Builder;
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/squareup/api/rpc/Error$Builder;->server_error_code:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    return-object p0
.end method

.method public sync_error_code(Lcom/squareup/api/sync/SyncErrorCode;)Lcom/squareup/api/rpc/Error$Builder;
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/api/rpc/Error$Builder;->sync_error_code:Lcom/squareup/api/sync/SyncErrorCode;

    return-object p0
.end method
