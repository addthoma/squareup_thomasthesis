.class public final Lcom/squareup/api/rpc/RequestBatch$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RequestBatch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/rpc/RequestBatch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/rpc/RequestBatch;",
        "Lcom/squareup/api/rpc/RequestBatch$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public request:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/rpc/Request;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 82
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 83
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/rpc/RequestBatch$Builder;->request:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/rpc/RequestBatch;
    .locals 3

    .line 97
    new-instance v0, Lcom/squareup/api/rpc/RequestBatch;

    iget-object v1, p0, Lcom/squareup/api/rpc/RequestBatch$Builder;->request:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/api/rpc/RequestBatch;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/api/rpc/RequestBatch$Builder;->build()Lcom/squareup/api/rpc/RequestBatch;

    move-result-object v0

    return-object v0
.end method

.method public request(Ljava/util/List;)Lcom/squareup/api/rpc/RequestBatch$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/rpc/Request;",
            ">;)",
            "Lcom/squareup/api/rpc/RequestBatch$Builder;"
        }
    .end annotation

    .line 90
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 91
    iput-object p1, p0, Lcom/squareup/api/rpc/RequestBatch$Builder;->request:Ljava/util/List;

    return-object p0
.end method
