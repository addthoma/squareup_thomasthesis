.class public interface abstract Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;
.super Ljava/lang/Object;
.source "SquareSyncMultiPassAuthService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0000\u0008f\u0018\u00002\u00020\u0001J$\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0014\u0008\u0001\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00070\u0006H\'\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;",
        "",
        "createIdentityTokenFromSession",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/api/multipassauth/SquareSyncIdentityTokenResponse;",
        "emptyJsonBody",
        "",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract createIdentityTokenFromSession(Ljava/util/Map;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Ljava/util/Map;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/api/multipassauth/SquareSyncIdentityTokenResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Content-Type: application/json",
            "Accept: application/json"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/services/squareup.multipass.external.MultipassExternalService/CreateIdentityTokenFromSession"
    .end annotation
.end method
