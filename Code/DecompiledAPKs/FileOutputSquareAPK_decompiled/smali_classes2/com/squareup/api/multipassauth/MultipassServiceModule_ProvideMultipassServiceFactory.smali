.class public final Lcom/squareup/api/multipassauth/MultipassServiceModule_ProvideMultipassServiceFactory;
.super Ljava/lang/Object;
.source "MultipassServiceModule_ProvideMultipassServiceFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/api/multipassauth/MultipassService;",
        ">;"
    }
.end annotation


# instance fields
.field private final serviceCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/api/multipassauth/MultipassServiceModule_ProvideMultipassServiceFactory;->serviceCreatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/api/multipassauth/MultipassServiceModule_ProvideMultipassServiceFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;)",
            "Lcom/squareup/api/multipassauth/MultipassServiceModule_ProvideMultipassServiceFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/api/multipassauth/MultipassServiceModule_ProvideMultipassServiceFactory;

    invoke-direct {v0, p0}, Lcom/squareup/api/multipassauth/MultipassServiceModule_ProvideMultipassServiceFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideMultipassService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/api/multipassauth/MultipassService;
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/api/multipassauth/MultipassServiceModule;->INSTANCE:Lcom/squareup/api/multipassauth/MultipassServiceModule;

    invoke-virtual {v0, p0}, Lcom/squareup/api/multipassauth/MultipassServiceModule;->provideMultipassService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/api/multipassauth/MultipassService;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/multipassauth/MultipassService;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/api/multipassauth/MultipassService;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/api/multipassauth/MultipassServiceModule_ProvideMultipassServiceFactory;->serviceCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ServiceCreator;

    invoke-static {v0}, Lcom/squareup/api/multipassauth/MultipassServiceModule_ProvideMultipassServiceFactory;->provideMultipassService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/api/multipassauth/MultipassService;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/api/multipassauth/MultipassServiceModule_ProvideMultipassServiceFactory;->get()Lcom/squareup/api/multipassauth/MultipassService;

    move-result-object v0

    return-object v0
.end method
