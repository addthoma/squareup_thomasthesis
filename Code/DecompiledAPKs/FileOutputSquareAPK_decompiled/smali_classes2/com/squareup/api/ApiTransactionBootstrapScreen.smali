.class public final Lcom/squareup/api/ApiTransactionBootstrapScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "ApiTransactionBootstrapScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/RegistersInScope;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/ApiTransactionBootstrapScreen$ParentComponent;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/api/ApiTransactionBootstrapScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/api/ApiTransactionBootstrapScreen;

    invoke-direct {v0}, Lcom/squareup/api/ApiTransactionBootstrapScreen;-><init>()V

    sput-object v0, Lcom/squareup/api/ApiTransactionBootstrapScreen;->INSTANCE:Lcom/squareup/api/ApiTransactionBootstrapScreen;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 41
    const-class v0, Lcom/squareup/api/ApiTransactionBootstrapScreen$ParentComponent;

    .line 42
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/ApiTransactionBootstrapScreen$ParentComponent;

    .line 43
    invoke-interface {p1}, Lcom/squareup/api/ApiTransactionBootstrapScreen$ParentComponent;->tenderStarter()Lcom/squareup/ui/tender/TenderStarter;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/tender/TenderStarter;->startTenderFlow()V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 37
    sget v0, Lcom/squareup/container/R$layout;->assert_never_shown_view:I

    return v0
.end method
