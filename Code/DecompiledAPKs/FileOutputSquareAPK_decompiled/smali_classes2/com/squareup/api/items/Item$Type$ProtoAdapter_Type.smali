.class final Lcom/squareup/api/items/Item$Type$ProtoAdapter_Type;
.super Lcom/squareup/wire/EnumAdapter;
.source "Item.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Item$Type;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/api/items/Item$Type;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 871
    const-class v0, Lcom/squareup/api/items/Item$Type;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/api/items/Item$Type;
    .locals 0

    .line 876
    invoke-static {p1}, Lcom/squareup/api/items/Item$Type;->fromValue(I)Lcom/squareup/api/items/Item$Type;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 869
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/Item$Type$ProtoAdapter_Type;->fromValue(I)Lcom/squareup/api/items/Item$Type;

    move-result-object p1

    return-object p1
.end method
