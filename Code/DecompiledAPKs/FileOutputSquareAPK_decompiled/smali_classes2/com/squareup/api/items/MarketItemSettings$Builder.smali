.class public final Lcom/squareup/api/items/MarketItemSettings$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MarketItemSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/MarketItemSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/MarketItemSettings;",
        "Lcom/squareup/api/items/MarketItemSettings$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public is_in_stock:Ljava/lang/Boolean;

.field public is_unique:Ljava/lang/Boolean;

.field public item:Lcom/squareup/api/sync/ObjectId;

.field public shipping_cost:Lcom/squareup/protos/common/Money;

.field public slug:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 155
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/MarketItemSettings;
    .locals 9

    .line 190
    new-instance v8, Lcom/squareup/api/items/MarketItemSettings;

    iget-object v1, p0, Lcom/squareup/api/items/MarketItemSettings$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/MarketItemSettings$Builder;->slug:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/items/MarketItemSettings$Builder;->is_unique:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/api/items/MarketItemSettings$Builder;->is_in_stock:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/api/items/MarketItemSettings$Builder;->shipping_cost:Lcom/squareup/protos/common/Money;

    iget-object v6, p0, Lcom/squareup/api/items/MarketItemSettings$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/api/items/MarketItemSettings;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Lcom/squareup/api/sync/ObjectId;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 142
    invoke-virtual {p0}, Lcom/squareup/api/items/MarketItemSettings$Builder;->build()Lcom/squareup/api/items/MarketItemSettings;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/MarketItemSettings$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/api/items/MarketItemSettings$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public is_in_stock(Ljava/lang/Boolean;)Lcom/squareup/api/items/MarketItemSettings$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/api/items/MarketItemSettings$Builder;->is_in_stock:Ljava/lang/Boolean;

    return-object p0
.end method

.method public is_unique(Ljava/lang/Boolean;)Lcom/squareup/api/items/MarketItemSettings$Builder;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/api/items/MarketItemSettings$Builder;->is_unique:Ljava/lang/Boolean;

    return-object p0
.end method

.method public item(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/MarketItemSettings$Builder;
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/api/items/MarketItemSettings$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public shipping_cost(Lcom/squareup/protos/common/Money;)Lcom/squareup/api/items/MarketItemSettings$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/api/items/MarketItemSettings$Builder;->shipping_cost:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public slug(Ljava/lang/String;)Lcom/squareup/api/items/MarketItemSettings$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/api/items/MarketItemSettings$Builder;->slug:Ljava/lang/String;

    return-object p0
.end method
