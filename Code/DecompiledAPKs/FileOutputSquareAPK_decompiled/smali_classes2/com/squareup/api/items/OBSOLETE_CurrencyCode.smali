.class public final enum Lcom/squareup/api/items/OBSOLETE_CurrencyCode;
.super Ljava/lang/Enum;
.source "OBSOLETE_CurrencyCode.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/OBSOLETE_CurrencyCode$ProtoAdapter_OBSOLETE_CurrencyCode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/OBSOLETE_CurrencyCode;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/OBSOLETE_CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum USD:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 11
    new-instance v0, Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    const/4 v1, 0x0

    const-string v2, "USD"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/api/items/OBSOLETE_CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/OBSOLETE_CurrencyCode;->USD:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    .line 10
    sget-object v2, Lcom/squareup/api/items/OBSOLETE_CurrencyCode;->USD:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/squareup/api/items/OBSOLETE_CurrencyCode;->$VALUES:[Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    .line 13
    new-instance v0, Lcom/squareup/api/items/OBSOLETE_CurrencyCode$ProtoAdapter_OBSOLETE_CurrencyCode;

    invoke-direct {v0}, Lcom/squareup/api/items/OBSOLETE_CurrencyCode$ProtoAdapter_OBSOLETE_CurrencyCode;-><init>()V

    sput-object v0, Lcom/squareup/api/items/OBSOLETE_CurrencyCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18
    iput p3, p0, Lcom/squareup/api/items/OBSOLETE_CurrencyCode;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/OBSOLETE_CurrencyCode;
    .locals 0

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 26
    :cond_0
    sget-object p0, Lcom/squareup/api/items/OBSOLETE_CurrencyCode;->USD:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/OBSOLETE_CurrencyCode;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/OBSOLETE_CurrencyCode;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/api/items/OBSOLETE_CurrencyCode;->$VALUES:[Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    invoke-virtual {v0}, [Lcom/squareup/api/items/OBSOLETE_CurrencyCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 33
    iget v0, p0, Lcom/squareup/api/items/OBSOLETE_CurrencyCode;->value:I

    return v0
.end method
