.class public final Lcom/squareup/api/items/ObjectPredicate$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ObjectPredicate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/ObjectPredicate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/ObjectPredicate;",
        "Lcom/squareup/api/items/ObjectPredicate$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public object_id:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation
.end field

.field public type:Lcom/squareup/api/items/ObjectPredicate$Type;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 100
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 101
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/ObjectPredicate$Builder;->object_id:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/ObjectPredicate;
    .locals 4

    .line 120
    new-instance v0, Lcom/squareup/api/items/ObjectPredicate;

    iget-object v1, p0, Lcom/squareup/api/items/ObjectPredicate$Builder;->type:Lcom/squareup/api/items/ObjectPredicate$Type;

    iget-object v2, p0, Lcom/squareup/api/items/ObjectPredicate$Builder;->object_id:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/api/items/ObjectPredicate;-><init>(Lcom/squareup/api/items/ObjectPredicate$Type;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/api/items/ObjectPredicate$Builder;->build()Lcom/squareup/api/items/ObjectPredicate;

    move-result-object v0

    return-object v0
.end method

.method public object_id(Ljava/util/List;)Lcom/squareup/api/items/ObjectPredicate$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;)",
            "Lcom/squareup/api/items/ObjectPredicate$Builder;"
        }
    .end annotation

    .line 113
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 114
    iput-object p1, p0, Lcom/squareup/api/items/ObjectPredicate$Builder;->object_id:Ljava/util/List;

    return-object p0
.end method

.method public type(Lcom/squareup/api/items/ObjectPredicate$Type;)Lcom/squareup/api/items/ObjectPredicate$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/api/items/ObjectPredicate$Builder;->type:Lcom/squareup/api/items/ObjectPredicate$Type;

    return-object p0
.end method
