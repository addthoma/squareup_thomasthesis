.class public final Lcom/squareup/api/items/FloorPlanTile;
.super Lcom/squareup/wire/Message;
.source "FloorPlanTile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/FloorPlanTile$ProtoAdapter_FloorPlanTile;,
        Lcom/squareup/api/items/FloorPlanTile$Shape;,
        Lcom/squareup/api/items/FloorPlanTile$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/FloorPlanTile;",
        "Lcom/squareup/api/items/FloorPlanTile$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/FloorPlanTile;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COLOR:Ljava/lang/String; = ""

.field public static final DEFAULT_HEIGHT:Ljava/lang/Integer;

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_ROTATION_ANGLE:Ljava/lang/Integer;

.field public static final DEFAULT_SHAPE:Lcom/squareup/api/items/FloorPlanTile$Shape;

.field public static final DEFAULT_WIDTH:Ljava/lang/Integer;

.field public static final DEFAULT_Z_ORDER:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final color:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final floor_plan:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final height:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x5
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final object:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final position:Lcom/squareup/api/items/Point;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Point#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final rotation_angle:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x7
    .end annotation
.end field

.field public final shape:Lcom/squareup/api/items/FloorPlanTile$Shape;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.FloorPlanTile$Shape#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final width:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final z_order:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x9
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 30
    new-instance v0, Lcom/squareup/api/items/FloorPlanTile$ProtoAdapter_FloorPlanTile;

    invoke-direct {v0}, Lcom/squareup/api/items/FloorPlanTile$ProtoAdapter_FloorPlanTile;-><init>()V

    sput-object v0, Lcom/squareup/api/items/FloorPlanTile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 36
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/FloorPlanTile;->DEFAULT_WIDTH:Ljava/lang/Integer;

    .line 38
    sput-object v0, Lcom/squareup/api/items/FloorPlanTile;->DEFAULT_HEIGHT:Ljava/lang/Integer;

    .line 40
    sput-object v0, Lcom/squareup/api/items/FloorPlanTile;->DEFAULT_ROTATION_ANGLE:Ljava/lang/Integer;

    .line 42
    sget-object v1, Lcom/squareup/api/items/FloorPlanTile$Shape;->UNKNOWN:Lcom/squareup/api/items/FloorPlanTile$Shape;

    sput-object v1, Lcom/squareup/api/items/FloorPlanTile;->DEFAULT_SHAPE:Lcom/squareup/api/items/FloorPlanTile$Shape;

    .line 44
    sput-object v0, Lcom/squareup/api/items/FloorPlanTile;->DEFAULT_Z_ORDER:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/sync/ObjectId;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/api/items/Point;Ljava/lang/Integer;Lcom/squareup/api/items/FloorPlanTile$Shape;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 12

    .line 139
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/api/items/FloorPlanTile;-><init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/sync/ObjectId;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/api/items/Point;Ljava/lang/Integer;Lcom/squareup/api/items/FloorPlanTile$Shape;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/sync/ObjectId;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/api/items/Point;Ljava/lang/Integer;Lcom/squareup/api/items/FloorPlanTile$Shape;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 145
    sget-object v0, Lcom/squareup/api/items/FloorPlanTile;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 146
    iput-object p1, p0, Lcom/squareup/api/items/FloorPlanTile;->id:Ljava/lang/String;

    .line 147
    iput-object p2, p0, Lcom/squareup/api/items/FloorPlanTile;->object:Lcom/squareup/api/sync/ObjectId;

    .line 148
    iput-object p3, p0, Lcom/squareup/api/items/FloorPlanTile;->floor_plan:Lcom/squareup/api/sync/ObjectId;

    .line 149
    iput-object p4, p0, Lcom/squareup/api/items/FloorPlanTile;->width:Ljava/lang/Integer;

    .line 150
    iput-object p5, p0, Lcom/squareup/api/items/FloorPlanTile;->height:Ljava/lang/Integer;

    .line 151
    iput-object p6, p0, Lcom/squareup/api/items/FloorPlanTile;->position:Lcom/squareup/api/items/Point;

    .line 152
    iput-object p7, p0, Lcom/squareup/api/items/FloorPlanTile;->rotation_angle:Ljava/lang/Integer;

    .line 153
    iput-object p8, p0, Lcom/squareup/api/items/FloorPlanTile;->shape:Lcom/squareup/api/items/FloorPlanTile$Shape;

    .line 154
    iput-object p9, p0, Lcom/squareup/api/items/FloorPlanTile;->z_order:Ljava/lang/Integer;

    .line 155
    iput-object p10, p0, Lcom/squareup/api/items/FloorPlanTile;->color:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 178
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/FloorPlanTile;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 179
    :cond_1
    check-cast p1, Lcom/squareup/api/items/FloorPlanTile;

    .line 180
    invoke-virtual {p0}, Lcom/squareup/api/items/FloorPlanTile;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/FloorPlanTile;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/FloorPlanTile;->id:Ljava/lang/String;

    .line 181
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->object:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/api/items/FloorPlanTile;->object:Lcom/squareup/api/sync/ObjectId;

    .line 182
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->floor_plan:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/api/items/FloorPlanTile;->floor_plan:Lcom/squareup/api/sync/ObjectId;

    .line 183
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->width:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/FloorPlanTile;->width:Ljava/lang/Integer;

    .line 184
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->height:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/FloorPlanTile;->height:Ljava/lang/Integer;

    .line 185
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->position:Lcom/squareup/api/items/Point;

    iget-object v3, p1, Lcom/squareup/api/items/FloorPlanTile;->position:Lcom/squareup/api/items/Point;

    .line 186
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->rotation_angle:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/FloorPlanTile;->rotation_angle:Ljava/lang/Integer;

    .line 187
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->shape:Lcom/squareup/api/items/FloorPlanTile$Shape;

    iget-object v3, p1, Lcom/squareup/api/items/FloorPlanTile;->shape:Lcom/squareup/api/items/FloorPlanTile$Shape;

    .line 188
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->z_order:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/FloorPlanTile;->z_order:Ljava/lang/Integer;

    .line 189
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->color:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/api/items/FloorPlanTile;->color:Ljava/lang/String;

    .line 190
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 195
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 197
    invoke-virtual {p0}, Lcom/squareup/api/items/FloorPlanTile;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 198
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 199
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->object:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 200
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->floor_plan:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 201
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->width:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 202
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->height:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 203
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->position:Lcom/squareup/api/items/Point;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/api/items/Point;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 204
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->rotation_angle:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 205
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->shape:Lcom/squareup/api/items/FloorPlanTile$Shape;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/api/items/FloorPlanTile$Shape;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 206
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->z_order:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 207
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->color:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 208
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/FloorPlanTile$Builder;
    .locals 2

    .line 160
    new-instance v0, Lcom/squareup/api/items/FloorPlanTile$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/FloorPlanTile$Builder;-><init>()V

    .line 161
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/FloorPlanTile$Builder;->id:Ljava/lang/String;

    .line 162
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->object:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/FloorPlanTile$Builder;->object:Lcom/squareup/api/sync/ObjectId;

    .line 163
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->floor_plan:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/FloorPlanTile$Builder;->floor_plan:Lcom/squareup/api/sync/ObjectId;

    .line 164
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->width:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/FloorPlanTile$Builder;->width:Ljava/lang/Integer;

    .line 165
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->height:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/FloorPlanTile$Builder;->height:Ljava/lang/Integer;

    .line 166
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->position:Lcom/squareup/api/items/Point;

    iput-object v1, v0, Lcom/squareup/api/items/FloorPlanTile$Builder;->position:Lcom/squareup/api/items/Point;

    .line 167
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->rotation_angle:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/FloorPlanTile$Builder;->rotation_angle:Ljava/lang/Integer;

    .line 168
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->shape:Lcom/squareup/api/items/FloorPlanTile$Shape;

    iput-object v1, v0, Lcom/squareup/api/items/FloorPlanTile$Builder;->shape:Lcom/squareup/api/items/FloorPlanTile$Shape;

    .line 169
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->z_order:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/FloorPlanTile$Builder;->z_order:Ljava/lang/Integer;

    .line 170
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->color:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/FloorPlanTile$Builder;->color:Ljava/lang/String;

    .line 171
    invoke-virtual {p0}, Lcom/squareup/api/items/FloorPlanTile;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/FloorPlanTile$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/api/items/FloorPlanTile;->newBuilder()Lcom/squareup/api/items/FloorPlanTile$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 216
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->object:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_1

    const-string v1, ", object="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->object:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 218
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->floor_plan:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_2

    const-string v1, ", floor_plan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->floor_plan:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 219
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->width:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->width:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 220
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->height:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->height:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 221
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->position:Lcom/squareup/api/items/Point;

    if-eqz v1, :cond_5

    const-string v1, ", position="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->position:Lcom/squareup/api/items/Point;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 222
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->rotation_angle:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    const-string v1, ", rotation_angle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->rotation_angle:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 223
    :cond_6
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->shape:Lcom/squareup/api/items/FloorPlanTile$Shape;

    if-eqz v1, :cond_7

    const-string v1, ", shape="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->shape:Lcom/squareup/api/items/FloorPlanTile$Shape;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 224
    :cond_7
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->z_order:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    const-string v1, ", z_order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->z_order:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 225
    :cond_8
    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->color:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlanTile;->color:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FloorPlanTile{"

    .line 226
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
