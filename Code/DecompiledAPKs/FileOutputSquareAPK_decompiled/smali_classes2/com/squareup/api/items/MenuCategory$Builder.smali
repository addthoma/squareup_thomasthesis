.class public final Lcom/squareup/api/items/MenuCategory$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MenuCategory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/MenuCategory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/MenuCategory;",
        "Lcom/squareup/api/items/MenuCategory$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public abbreviation:Ljava/lang/String;

.field public catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

.field public color:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public ordinal:Ljava/lang/Integer;

.field public straight_fire:Lcom/squareup/api/items/StraightFireType;

.field public v2_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 206
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public abbreviation(Ljava/lang/String;)Lcom/squareup/api/items/MenuCategory$Builder;
    .locals 0

    .line 255
    iput-object p1, p0, Lcom/squareup/api/items/MenuCategory$Builder;->abbreviation:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/api/items/MenuCategory;
    .locals 11

    .line 271
    new-instance v10, Lcom/squareup/api/items/MenuCategory;

    iget-object v1, p0, Lcom/squareup/api/items/MenuCategory$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/MenuCategory$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/items/MenuCategory$Builder;->ordinal:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/api/items/MenuCategory$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v5, p0, Lcom/squareup/api/items/MenuCategory$Builder;->straight_fire:Lcom/squareup/api/items/StraightFireType;

    iget-object v6, p0, Lcom/squareup/api/items/MenuCategory$Builder;->color:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/api/items/MenuCategory$Builder;->abbreviation:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/api/items/MenuCategory$Builder;->v2_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/api/items/MenuCategory;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/items/MerchantCatalogObjectReference;Lcom/squareup/api/items/StraightFireType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 189
    invoke-virtual {p0}, Lcom/squareup/api/items/MenuCategory$Builder;->build()Lcom/squareup/api/items/MenuCategory;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_reference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/api/items/MenuCategory$Builder;
    .locals 0

    .line 231
    iput-object p1, p0, Lcom/squareup/api/items/MenuCategory$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    return-object p0
.end method

.method public color(Ljava/lang/String;)Lcom/squareup/api/items/MenuCategory$Builder;
    .locals 0

    .line 247
    iput-object p1, p0, Lcom/squareup/api/items/MenuCategory$Builder;->color:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/MenuCategory$Builder;
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/squareup/api/items/MenuCategory$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/MenuCategory$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/api/items/MenuCategory$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/MenuCategory$Builder;
    .locals 0

    .line 225
    iput-object p1, p0, Lcom/squareup/api/items/MenuCategory$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public straight_fire(Lcom/squareup/api/items/StraightFireType;)Lcom/squareup/api/items/MenuCategory$Builder;
    .locals 0

    .line 239
    iput-object p1, p0, Lcom/squareup/api/items/MenuCategory$Builder;->straight_fire:Lcom/squareup/api/items/StraightFireType;

    return-object p0
.end method

.method public v2_id(Ljava/lang/String;)Lcom/squareup/api/items/MenuCategory$Builder;
    .locals 0

    .line 265
    iput-object p1, p0, Lcom/squareup/api/items/MenuCategory$Builder;->v2_id:Ljava/lang/String;

    return-object p0
.end method
