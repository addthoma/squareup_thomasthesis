.class public final Lcom/squareup/api/items/ItemPageMembership$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ItemPageMembership.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/ItemPageMembership;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/ItemPageMembership;",
        "Lcom/squareup/api/items/ItemPageMembership$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public column:Ljava/lang/Integer;

.field public height:Ljava/lang/Integer;

.field public id:Ljava/lang/String;

.field public item:Lcom/squareup/api/sync/ObjectId;

.field public page:Lcom/squareup/api/sync/ObjectId;

.field public position:Ljava/lang/Integer;

.field public row:Ljava/lang/Integer;

.field public width:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 197
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/ItemPageMembership;
    .locals 11

    .line 255
    new-instance v10, Lcom/squareup/api/items/ItemPageMembership;

    iget-object v1, p0, Lcom/squareup/api/items/ItemPageMembership$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/ItemPageMembership$Builder;->row:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/api/items/ItemPageMembership$Builder;->column:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/api/items/ItemPageMembership$Builder;->position:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/api/items/ItemPageMembership$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    iget-object v6, p0, Lcom/squareup/api/items/ItemPageMembership$Builder;->page:Lcom/squareup/api/sync/ObjectId;

    iget-object v7, p0, Lcom/squareup/api/items/ItemPageMembership$Builder;->width:Ljava/lang/Integer;

    iget-object v8, p0, Lcom/squareup/api/items/ItemPageMembership$Builder;->height:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/api/items/ItemPageMembership;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/sync/ObjectId;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 180
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemPageMembership$Builder;->build()Lcom/squareup/api/items/ItemPageMembership;

    move-result-object v0

    return-object v0
.end method

.method public column(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemPageMembership$Builder;
    .locals 0

    .line 218
    iput-object p1, p0, Lcom/squareup/api/items/ItemPageMembership$Builder;->column:Ljava/lang/Integer;

    return-object p0
.end method

.method public height(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemPageMembership$Builder;
    .locals 0

    .line 249
    iput-object p1, p0, Lcom/squareup/api/items/ItemPageMembership$Builder;->height:Ljava/lang/Integer;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/ItemPageMembership$Builder;
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/squareup/api/items/ItemPageMembership$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public item(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemPageMembership$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/api/items/ItemPageMembership$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public page(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemPageMembership$Builder;
    .locals 0

    .line 233
    iput-object p1, p0, Lcom/squareup/api/items/ItemPageMembership$Builder;->page:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public position(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemPageMembership$Builder;
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/api/items/ItemPageMembership$Builder;->position:Ljava/lang/Integer;

    return-object p0
.end method

.method public row(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemPageMembership$Builder;
    .locals 0

    .line 213
    iput-object p1, p0, Lcom/squareup/api/items/ItemPageMembership$Builder;->row:Ljava/lang/Integer;

    return-object p0
.end method

.method public width(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemPageMembership$Builder;
    .locals 0

    .line 244
    iput-object p1, p0, Lcom/squareup/api/items/ItemPageMembership$Builder;->width:Ljava/lang/Integer;

    return-object p0
.end method
