.class public final Lcom/squareup/api/items/Item;
.super Lcom/squareup/wire/Message;
.source "Item.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/Item$ProtoAdapter_Item;,
        Lcom/squareup/api/items/Item$EcomVisibility;,
        Lcom/squareup/api/items/Item$Type;,
        Lcom/squareup/api/items/Item$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/Item;",
        "Lcom/squareup/api/items/Item$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/Item;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ABBREVIATION:Ljava/lang/String; = ""

.field public static final DEFAULT_AVAILABLE_ELECTRONICALLY:Ljava/lang/Boolean;

.field public static final DEFAULT_AVAILABLE_FOR_PICKUP:Ljava/lang/Boolean;

.field public static final DEFAULT_AVAILABLE_ONLINE:Ljava/lang/Boolean;

.field public static final DEFAULT_BUYER_FACING_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_COLOR:Ljava/lang/String; = ""

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_ECOM_AVAILABLE:Ljava/lang/Boolean;

.field public static final DEFAULT_ECOM_BUY_BUTTON_TEXT:Ljava/lang/String; = ""

.field public static final DEFAULT_ECOM_IMAGE_URIS:Ljava/lang/String; = ""

.field public static final DEFAULT_ECOM_URI:Ljava/lang/String; = ""

.field public static final DEFAULT_ECOM_VISIBILITY:Lcom/squareup/api/items/Item$EcomVisibility;

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_ORDINAL:Ljava/lang/Integer;

.field public static final DEFAULT_PRICING_TYPE:Lcom/squareup/api/items/PricingType;

.field public static final DEFAULT_SKIPS_MODIFIER_SCREEN:Ljava/lang/Boolean;

.field public static final DEFAULT_STRAIGHT_FIRE_OVERRIDE:Lcom/squareup/api/items/StraightFireType;

.field public static final DEFAULT_TAXABLE:Ljava/lang/Boolean;

.field public static final DEFAULT_TYPE:Lcom/squareup/api/items/Item$Type;

.field public static final DEFAULT_V2_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_VISIBILITY:Lcom/squareup/api/items/Visibility;

.field private static final serialVersionUID:J


# instance fields
.field public final abbreviation:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final archetype:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final available_electronically:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x12
    .end annotation
.end field

.field public final available_for_pickup:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xe
    .end annotation
.end field

.field public final available_online:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final buyer_facing_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x11
    .end annotation
.end field

.field public final catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.MerchantCatalogObjectReference#ADAPTER"
        tag = 0x15
    .end annotation
.end field

.field public final color:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final custom_attribute_values:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogCustomAttributeValue#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1e
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;",
            ">;"
        }
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final ecom_available:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1b
    .end annotation
.end field

.field public final ecom_buy_button_text:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1f
    .end annotation
.end field

.field public final ecom_image_uris:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1a
    .end annotation
.end field

.field public final ecom_uri:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x19
    .end annotation
.end field

.field public final ecom_visibility:Lcom/squareup/api/items/Item$EcomVisibility;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Item$EcomVisibility#ADAPTER"
        tag = 0x20
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field

.field public final image:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final item_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemOptionForItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1c
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemOptionForItem;",
            ">;"
        }
    .end annotation
.end field

.field public final menu_category:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final online_store_item_data:Lcom/squareup/protos/items/OnlineStoreItemData;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.items.OnlineStoreItemData#ADAPTER"
        tag = 0x13
    .end annotation
.end field

.field public final ordinal:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xf
    .end annotation
.end field

.field public final pricing_type:Lcom/squareup/api/items/PricingType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.PricingType#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final skips_modifier_screen:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x14
    .end annotation
.end field

.field public final straight_fire_override:Lcom/squareup/api/items/StraightFireType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.StraightFireType#ADAPTER"
        tag = 0x17
    .end annotation
.end field

.field public final tag_membership:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x16
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final taxable:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final type:Lcom/squareup/api/items/Item$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Item$Type#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final v2_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x18
    .end annotation
.end field

.field public final visibility:Lcom/squareup/api/items/Visibility;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Visibility#ADAPTER"
        tag = 0x9
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 29
    new-instance v0, Lcom/squareup/api/items/Item$ProtoAdapter_Item;

    invoke-direct {v0}, Lcom/squareup/api/items/Item$ProtoAdapter_Item;-><init>()V

    sput-object v0, Lcom/squareup/api/items/Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 43
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/api/items/Item;->DEFAULT_TAXABLE:Ljava/lang/Boolean;

    .line 47
    sget-object v2, Lcom/squareup/api/items/Visibility;->PUBLIC:Lcom/squareup/api/items/Visibility;

    sput-object v2, Lcom/squareup/api/items/Item;->DEFAULT_VISIBILITY:Lcom/squareup/api/items/Visibility;

    .line 49
    sget-object v2, Lcom/squareup/api/items/PricingType;->FIXED_PRICING:Lcom/squareup/api/items/PricingType;

    sput-object v2, Lcom/squareup/api/items/Item;->DEFAULT_PRICING_TYPE:Lcom/squareup/api/items/PricingType;

    .line 51
    sput-object v1, Lcom/squareup/api/items/Item;->DEFAULT_AVAILABLE_ONLINE:Ljava/lang/Boolean;

    .line 53
    sput-object v1, Lcom/squareup/api/items/Item;->DEFAULT_AVAILABLE_FOR_PICKUP:Ljava/lang/Boolean;

    .line 55
    sput-object v1, Lcom/squareup/api/items/Item;->DEFAULT_AVAILABLE_ELECTRONICALLY:Ljava/lang/Boolean;

    .line 57
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/Item;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    .line 59
    sget-object v0, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    sput-object v0, Lcom/squareup/api/items/Item;->DEFAULT_TYPE:Lcom/squareup/api/items/Item$Type;

    .line 61
    sput-object v1, Lcom/squareup/api/items/Item;->DEFAULT_SKIPS_MODIFIER_SCREEN:Ljava/lang/Boolean;

    .line 63
    sget-object v0, Lcom/squareup/api/items/StraightFireType;->UNKNOWN_STRAIGHT_FIRE:Lcom/squareup/api/items/StraightFireType;

    sput-object v0, Lcom/squareup/api/items/Item;->DEFAULT_STRAIGHT_FIRE_OVERRIDE:Lcom/squareup/api/items/StraightFireType;

    .line 71
    sput-object v1, Lcom/squareup/api/items/Item;->DEFAULT_ECOM_AVAILABLE:Ljava/lang/Boolean;

    .line 75
    sget-object v0, Lcom/squareup/api/items/Item$EcomVisibility;->UNINDEXED:Lcom/squareup/api/items/Item$EcomVisibility;

    sput-object v0, Lcom/squareup/api/items/Item;->DEFAULT_ECOM_VISIBILITY:Lcom/squareup/api/items/Item$EcomVisibility;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/Item$Builder;Lokio/ByteString;)V
    .locals 1

    .line 333
    sget-object v0, Lcom/squareup/api/items/Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 334
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->id:Ljava/lang/String;

    .line 335
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->color:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->color:Ljava/lang/String;

    .line 336
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->description:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->description:Ljava/lang/String;

    .line 337
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->name:Ljava/lang/String;

    .line 338
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->buyer_facing_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->buyer_facing_name:Ljava/lang/String;

    .line 339
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->taxable:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->taxable:Ljava/lang/Boolean;

    .line 340
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->abbreviation:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->abbreviation:Ljava/lang/String;

    .line 341
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->visibility:Lcom/squareup/api/items/Visibility;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->visibility:Lcom/squareup/api/items/Visibility;

    .line 342
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->menu_category:Lcom/squareup/api/sync/ObjectId;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->menu_category:Lcom/squareup/api/sync/ObjectId;

    .line 343
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->image:Lcom/squareup/api/sync/ObjectId;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->image:Lcom/squareup/api/sync/ObjectId;

    .line 344
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->pricing_type:Lcom/squareup/api/items/PricingType;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->pricing_type:Lcom/squareup/api/items/PricingType;

    .line 345
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->archetype:Lcom/squareup/api/sync/ObjectId;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->archetype:Lcom/squareup/api/sync/ObjectId;

    .line 346
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->available_online:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->available_online:Ljava/lang/Boolean;

    .line 347
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->available_for_pickup:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->available_for_pickup:Ljava/lang/Boolean;

    .line 348
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->available_electronically:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->available_electronically:Ljava/lang/Boolean;

    .line 349
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->ordinal:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->ordinal:Ljava/lang/Integer;

    .line 350
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->type:Lcom/squareup/api/items/Item$Type;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->type:Lcom/squareup/api/items/Item$Type;

    .line 351
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->online_store_item_data:Lcom/squareup/protos/items/OnlineStoreItemData;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->online_store_item_data:Lcom/squareup/protos/items/OnlineStoreItemData;

    .line 352
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->skips_modifier_screen:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->skips_modifier_screen:Ljava/lang/Boolean;

    .line 353
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 354
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->tag_membership:Ljava/util/List;

    const-string v0, "tag_membership"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/api/items/Item;->tag_membership:Ljava/util/List;

    .line 355
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->straight_fire_override:Lcom/squareup/api/items/StraightFireType;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->straight_fire_override:Lcom/squareup/api/items/StraightFireType;

    .line 356
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->v2_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->v2_id:Ljava/lang/String;

    .line 357
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->ecom_uri:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->ecom_uri:Ljava/lang/String;

    .line 358
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->ecom_image_uris:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->ecom_image_uris:Ljava/lang/String;

    .line 359
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->ecom_available:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->ecom_available:Ljava/lang/Boolean;

    .line 360
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->item_options:Ljava/util/List;

    const-string v0, "item_options"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/api/items/Item;->item_options:Ljava/util/List;

    .line 361
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->custom_attribute_values:Ljava/util/List;

    const-string v0, "custom_attribute_values"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/api/items/Item;->custom_attribute_values:Ljava/util/List;

    .line 362
    iget-object p2, p1, Lcom/squareup/api/items/Item$Builder;->ecom_buy_button_text:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/api/items/Item;->ecom_buy_button_text:Ljava/lang/String;

    .line 363
    iget-object p1, p1, Lcom/squareup/api/items/Item$Builder;->ecom_visibility:Lcom/squareup/api/items/Item$EcomVisibility;

    iput-object p1, p0, Lcom/squareup/api/items/Item;->ecom_visibility:Lcom/squareup/api/items/Item$EcomVisibility;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 406
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/Item;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 407
    :cond_1
    check-cast p1, Lcom/squareup/api/items/Item;

    .line 408
    invoke-virtual {p0}, Lcom/squareup/api/items/Item;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/Item;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->id:Ljava/lang/String;

    .line 409
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->color:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->color:Ljava/lang/String;

    .line 410
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->description:Ljava/lang/String;

    .line 411
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->name:Ljava/lang/String;

    .line 412
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->buyer_facing_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->buyer_facing_name:Ljava/lang/String;

    .line 413
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->taxable:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->taxable:Ljava/lang/Boolean;

    .line 414
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->abbreviation:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->abbreviation:Ljava/lang/String;

    .line 415
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->visibility:Lcom/squareup/api/items/Visibility;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->visibility:Lcom/squareup/api/items/Visibility;

    .line 416
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->menu_category:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->menu_category:Lcom/squareup/api/sync/ObjectId;

    .line 417
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->image:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->image:Lcom/squareup/api/sync/ObjectId;

    .line 418
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->pricing_type:Lcom/squareup/api/items/PricingType;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->pricing_type:Lcom/squareup/api/items/PricingType;

    .line 419
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->archetype:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->archetype:Lcom/squareup/api/sync/ObjectId;

    .line 420
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->available_online:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->available_online:Ljava/lang/Boolean;

    .line 421
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->available_for_pickup:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->available_for_pickup:Ljava/lang/Boolean;

    .line 422
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->available_electronically:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->available_electronically:Ljava/lang/Boolean;

    .line 423
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->ordinal:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->ordinal:Ljava/lang/Integer;

    .line 424
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->type:Lcom/squareup/api/items/Item$Type;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->type:Lcom/squareup/api/items/Item$Type;

    .line 425
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->online_store_item_data:Lcom/squareup/protos/items/OnlineStoreItemData;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->online_store_item_data:Lcom/squareup/protos/items/OnlineStoreItemData;

    .line 426
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->skips_modifier_screen:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->skips_modifier_screen:Ljava/lang/Boolean;

    .line 427
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 428
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->tag_membership:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->tag_membership:Ljava/util/List;

    .line 429
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->straight_fire_override:Lcom/squareup/api/items/StraightFireType;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->straight_fire_override:Lcom/squareup/api/items/StraightFireType;

    .line 430
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->v2_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->v2_id:Ljava/lang/String;

    .line 431
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_uri:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->ecom_uri:Ljava/lang/String;

    .line 432
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_image_uris:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->ecom_image_uris:Ljava/lang/String;

    .line 433
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_available:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->ecom_available:Ljava/lang/Boolean;

    .line 434
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->item_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->item_options:Ljava/util/List;

    .line 435
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->custom_attribute_values:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->custom_attribute_values:Ljava/util/List;

    .line 436
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_buy_button_text:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Item;->ecom_buy_button_text:Ljava/lang/String;

    .line 437
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_visibility:Lcom/squareup/api/items/Item$EcomVisibility;

    iget-object p1, p1, Lcom/squareup/api/items/Item;->ecom_visibility:Lcom/squareup/api/items/Item$EcomVisibility;

    .line 438
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 443
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1b

    .line 445
    invoke-virtual {p0}, Lcom/squareup/api/items/Item;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 446
    iget-object v1, p0, Lcom/squareup/api/items/Item;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 447
    iget-object v1, p0, Lcom/squareup/api/items/Item;->color:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 448
    iget-object v1, p0, Lcom/squareup/api/items/Item;->description:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 449
    iget-object v1, p0, Lcom/squareup/api/items/Item;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 450
    iget-object v1, p0, Lcom/squareup/api/items/Item;->buyer_facing_name:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 451
    iget-object v1, p0, Lcom/squareup/api/items/Item;->taxable:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 452
    iget-object v1, p0, Lcom/squareup/api/items/Item;->abbreviation:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 453
    iget-object v1, p0, Lcom/squareup/api/items/Item;->visibility:Lcom/squareup/api/items/Visibility;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/api/items/Visibility;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 454
    iget-object v1, p0, Lcom/squareup/api/items/Item;->menu_category:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 455
    iget-object v1, p0, Lcom/squareup/api/items/Item;->image:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 456
    iget-object v1, p0, Lcom/squareup/api/items/Item;->pricing_type:Lcom/squareup/api/items/PricingType;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/api/items/PricingType;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 457
    iget-object v1, p0, Lcom/squareup/api/items/Item;->archetype:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 458
    iget-object v1, p0, Lcom/squareup/api/items/Item;->available_online:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 459
    iget-object v1, p0, Lcom/squareup/api/items/Item;->available_for_pickup:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 460
    iget-object v1, p0, Lcom/squareup/api/items/Item;->available_electronically:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 461
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 462
    iget-object v1, p0, Lcom/squareup/api/items/Item;->type:Lcom/squareup/api/items/Item$Type;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/api/items/Item$Type;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 463
    iget-object v1, p0, Lcom/squareup/api/items/Item;->online_store_item_data:Lcom/squareup/protos/items/OnlineStoreItemData;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lcom/squareup/protos/items/OnlineStoreItemData;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 464
    iget-object v1, p0, Lcom/squareup/api/items/Item;->skips_modifier_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 465
    iget-object v1, p0, Lcom/squareup/api/items/Item;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Lcom/squareup/api/items/MerchantCatalogObjectReference;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 466
    iget-object v1, p0, Lcom/squareup/api/items/Item;->tag_membership:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 467
    iget-object v1, p0, Lcom/squareup/api/items/Item;->straight_fire_override:Lcom/squareup/api/items/StraightFireType;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Lcom/squareup/api/items/StraightFireType;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 468
    iget-object v1, p0, Lcom/squareup/api/items/Item;->v2_id:Ljava/lang/String;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 469
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_uri:Ljava/lang/String;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 470
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_image_uris:Ljava/lang/String;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 471
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_available:Ljava/lang/Boolean;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 472
    iget-object v1, p0, Lcom/squareup/api/items/Item;->item_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 473
    iget-object v1, p0, Lcom/squareup/api/items/Item;->custom_attribute_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 474
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_buy_button_text:Ljava/lang/String;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 475
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_visibility:Lcom/squareup/api/items/Item$EcomVisibility;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Lcom/squareup/api/items/Item$EcomVisibility;->hashCode()I

    move-result v2

    :cond_1a
    add-int/2addr v0, v2

    .line 476
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1b
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/Item$Builder;
    .locals 2

    .line 368
    new-instance v0, Lcom/squareup/api/items/Item$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Item$Builder;-><init>()V

    .line 369
    iget-object v1, p0, Lcom/squareup/api/items/Item;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->id:Ljava/lang/String;

    .line 370
    iget-object v1, p0, Lcom/squareup/api/items/Item;->color:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->color:Ljava/lang/String;

    .line 371
    iget-object v1, p0, Lcom/squareup/api/items/Item;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->description:Ljava/lang/String;

    .line 372
    iget-object v1, p0, Lcom/squareup/api/items/Item;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->name:Ljava/lang/String;

    .line 373
    iget-object v1, p0, Lcom/squareup/api/items/Item;->buyer_facing_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->buyer_facing_name:Ljava/lang/String;

    .line 374
    iget-object v1, p0, Lcom/squareup/api/items/Item;->taxable:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->taxable:Ljava/lang/Boolean;

    .line 375
    iget-object v1, p0, Lcom/squareup/api/items/Item;->abbreviation:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->abbreviation:Ljava/lang/String;

    .line 376
    iget-object v1, p0, Lcom/squareup/api/items/Item;->visibility:Lcom/squareup/api/items/Visibility;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->visibility:Lcom/squareup/api/items/Visibility;

    .line 377
    iget-object v1, p0, Lcom/squareup/api/items/Item;->menu_category:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->menu_category:Lcom/squareup/api/sync/ObjectId;

    .line 378
    iget-object v1, p0, Lcom/squareup/api/items/Item;->image:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->image:Lcom/squareup/api/sync/ObjectId;

    .line 379
    iget-object v1, p0, Lcom/squareup/api/items/Item;->pricing_type:Lcom/squareup/api/items/PricingType;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->pricing_type:Lcom/squareup/api/items/PricingType;

    .line 380
    iget-object v1, p0, Lcom/squareup/api/items/Item;->archetype:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->archetype:Lcom/squareup/api/sync/ObjectId;

    .line 381
    iget-object v1, p0, Lcom/squareup/api/items/Item;->available_online:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->available_online:Ljava/lang/Boolean;

    .line 382
    iget-object v1, p0, Lcom/squareup/api/items/Item;->available_for_pickup:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->available_for_pickup:Ljava/lang/Boolean;

    .line 383
    iget-object v1, p0, Lcom/squareup/api/items/Item;->available_electronically:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->available_electronically:Ljava/lang/Boolean;

    .line 384
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ordinal:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->ordinal:Ljava/lang/Integer;

    .line 385
    iget-object v1, p0, Lcom/squareup/api/items/Item;->type:Lcom/squareup/api/items/Item$Type;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->type:Lcom/squareup/api/items/Item$Type;

    .line 386
    iget-object v1, p0, Lcom/squareup/api/items/Item;->online_store_item_data:Lcom/squareup/protos/items/OnlineStoreItemData;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->online_store_item_data:Lcom/squareup/protos/items/OnlineStoreItemData;

    .line 387
    iget-object v1, p0, Lcom/squareup/api/items/Item;->skips_modifier_screen:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->skips_modifier_screen:Ljava/lang/Boolean;

    .line 388
    iget-object v1, p0, Lcom/squareup/api/items/Item;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 389
    iget-object v1, p0, Lcom/squareup/api/items/Item;->tag_membership:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->tag_membership:Ljava/util/List;

    .line 390
    iget-object v1, p0, Lcom/squareup/api/items/Item;->straight_fire_override:Lcom/squareup/api/items/StraightFireType;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->straight_fire_override:Lcom/squareup/api/items/StraightFireType;

    .line 391
    iget-object v1, p0, Lcom/squareup/api/items/Item;->v2_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->v2_id:Ljava/lang/String;

    .line 392
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_uri:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->ecom_uri:Ljava/lang/String;

    .line 393
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_image_uris:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->ecom_image_uris:Ljava/lang/String;

    .line 394
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_available:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->ecom_available:Ljava/lang/Boolean;

    .line 395
    iget-object v1, p0, Lcom/squareup/api/items/Item;->item_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->item_options:Ljava/util/List;

    .line 396
    iget-object v1, p0, Lcom/squareup/api/items/Item;->custom_attribute_values:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->custom_attribute_values:Ljava/util/List;

    .line 397
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_buy_button_text:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->ecom_buy_button_text:Ljava/lang/String;

    .line 398
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_visibility:Lcom/squareup/api/items/Item$EcomVisibility;

    iput-object v1, v0, Lcom/squareup/api/items/Item$Builder;->ecom_visibility:Lcom/squareup/api/items/Item$EcomVisibility;

    .line 399
    invoke-virtual {p0}, Lcom/squareup/api/items/Item;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Item$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/api/items/Item;->newBuilder()Lcom/squareup/api/items/Item$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 483
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 484
    iget-object v1, p0, Lcom/squareup/api/items/Item;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 485
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/Item;->color:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->color:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 486
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/Item;->description:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 487
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/Item;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 488
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/Item;->buyer_facing_name:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", buyer_facing_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->buyer_facing_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 489
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/items/Item;->taxable:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", taxable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->taxable:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 490
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/items/Item;->abbreviation:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", abbreviation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->abbreviation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 491
    :cond_6
    iget-object v1, p0, Lcom/squareup/api/items/Item;->visibility:Lcom/squareup/api/items/Visibility;

    if-eqz v1, :cond_7

    const-string v1, ", visibility="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->visibility:Lcom/squareup/api/items/Visibility;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 492
    :cond_7
    iget-object v1, p0, Lcom/squareup/api/items/Item;->menu_category:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_8

    const-string v1, ", menu_category="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->menu_category:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 493
    :cond_8
    iget-object v1, p0, Lcom/squareup/api/items/Item;->image:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_9

    const-string v1, ", image="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->image:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 494
    :cond_9
    iget-object v1, p0, Lcom/squareup/api/items/Item;->pricing_type:Lcom/squareup/api/items/PricingType;

    if-eqz v1, :cond_a

    const-string v1, ", pricing_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->pricing_type:Lcom/squareup/api/items/PricingType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 495
    :cond_a
    iget-object v1, p0, Lcom/squareup/api/items/Item;->archetype:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_b

    const-string v1, ", archetype="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->archetype:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 496
    :cond_b
    iget-object v1, p0, Lcom/squareup/api/items/Item;->available_online:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    const-string v1, ", available_online="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->available_online:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 497
    :cond_c
    iget-object v1, p0, Lcom/squareup/api/items/Item;->available_for_pickup:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    const-string v1, ", available_for_pickup="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->available_for_pickup:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 498
    :cond_d
    iget-object v1, p0, Lcom/squareup/api/items/Item;->available_electronically:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    const-string v1, ", available_electronically="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->available_electronically:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 499
    :cond_e
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    const-string v1, ", ordinal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->ordinal:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 500
    :cond_f
    iget-object v1, p0, Lcom/squareup/api/items/Item;->type:Lcom/squareup/api/items/Item$Type;

    if-eqz v1, :cond_10

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->type:Lcom/squareup/api/items/Item$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 501
    :cond_10
    iget-object v1, p0, Lcom/squareup/api/items/Item;->online_store_item_data:Lcom/squareup/protos/items/OnlineStoreItemData;

    if-eqz v1, :cond_11

    const-string v1, ", online_store_item_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->online_store_item_data:Lcom/squareup/protos/items/OnlineStoreItemData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 502
    :cond_11
    iget-object v1, p0, Lcom/squareup/api/items/Item;->skips_modifier_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    const-string v1, ", skips_modifier_screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->skips_modifier_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 503
    :cond_12
    iget-object v1, p0, Lcom/squareup/api/items/Item;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v1, :cond_13

    const-string v1, ", catalog_object_reference="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 504
    :cond_13
    iget-object v1, p0, Lcom/squareup/api/items/Item;->tag_membership:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_14

    const-string v1, ", tag_membership="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->tag_membership:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 505
    :cond_14
    iget-object v1, p0, Lcom/squareup/api/items/Item;->straight_fire_override:Lcom/squareup/api/items/StraightFireType;

    if-eqz v1, :cond_15

    const-string v1, ", straight_fire_override="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->straight_fire_override:Lcom/squareup/api/items/StraightFireType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 506
    :cond_15
    iget-object v1, p0, Lcom/squareup/api/items/Item;->v2_id:Ljava/lang/String;

    if-eqz v1, :cond_16

    const-string v1, ", v2_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->v2_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 507
    :cond_16
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_uri:Ljava/lang/String;

    if-eqz v1, :cond_17

    const-string v1, ", ecom_uri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_uri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 508
    :cond_17
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_image_uris:Ljava/lang/String;

    if-eqz v1, :cond_18

    const-string v1, ", ecom_image_uris="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_image_uris:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 509
    :cond_18
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_available:Ljava/lang/Boolean;

    if-eqz v1, :cond_19

    const-string v1, ", ecom_available="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_available:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 510
    :cond_19
    iget-object v1, p0, Lcom/squareup/api/items/Item;->item_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1a

    const-string v1, ", item_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->item_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 511
    :cond_1a
    iget-object v1, p0, Lcom/squareup/api/items/Item;->custom_attribute_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1b

    const-string v1, ", custom_attribute_values="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->custom_attribute_values:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 512
    :cond_1b
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_buy_button_text:Ljava/lang/String;

    if-eqz v1, :cond_1c

    const-string v1, ", ecom_buy_button_text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_buy_button_text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 513
    :cond_1c
    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_visibility:Lcom/squareup/api/items/Item$EcomVisibility;

    if-eqz v1, :cond_1d

    const-string v1, ", ecom_visibility="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Item;->ecom_visibility:Lcom/squareup/api/items/Item$EcomVisibility;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1d
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Item{"

    .line 514
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
