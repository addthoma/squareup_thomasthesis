.class public final Lcom/squareup/api/items/ItemImage$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ItemImage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/ItemImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/ItemImage;",
        "Lcom/squareup/api/items/ItemImage$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

.field public id:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/ItemImage;
    .locals 5

    .line 135
    new-instance v0, Lcom/squareup/api/items/ItemImage;

    iget-object v1, p0, Lcom/squareup/api/items/ItemImage$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/ItemImage$Builder;->url:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/items/ItemImage$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/api/items/ItemImage;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/MerchantCatalogObjectReference;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemImage$Builder;->build()Lcom/squareup/api/items/ItemImage;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_reference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/api/items/ItemImage$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/api/items/ItemImage$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/ItemImage$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/api/items/ItemImage$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public url(Ljava/lang/String;)Lcom/squareup/api/items/ItemImage$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/api/items/ItemImage$Builder;->url:Ljava/lang/String;

    return-object p0
.end method
