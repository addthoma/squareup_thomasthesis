.class public final Lcom/squareup/api/items/SurchargeFeeMembership$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SurchargeFeeMembership.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/SurchargeFeeMembership;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/SurchargeFeeMembership;",
        "Lcom/squareup/api/items/SurchargeFeeMembership$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public fee:Lcom/squareup/api/sync/ObjectId;

.field public id:Ljava/lang/String;

.field public surcharge:Lcom/squareup/api/sync/ObjectId;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/SurchargeFeeMembership;
    .locals 5

    .line 148
    new-instance v0, Lcom/squareup/api/items/SurchargeFeeMembership;

    iget-object v1, p0, Lcom/squareup/api/items/SurchargeFeeMembership$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/SurchargeFeeMembership$Builder;->surcharge:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p0, Lcom/squareup/api/items/SurchargeFeeMembership$Builder;->fee:Lcom/squareup/api/sync/ObjectId;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/api/items/SurchargeFeeMembership;-><init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/sync/ObjectId;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/api/items/SurchargeFeeMembership$Builder;->build()Lcom/squareup/api/items/SurchargeFeeMembership;

    move-result-object v0

    return-object v0
.end method

.method public fee(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/SurchargeFeeMembership$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/api/items/SurchargeFeeMembership$Builder;->fee:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/SurchargeFeeMembership$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/api/items/SurchargeFeeMembership$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public surcharge(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/SurchargeFeeMembership$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/api/items/SurchargeFeeMembership$Builder;->surcharge:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method
