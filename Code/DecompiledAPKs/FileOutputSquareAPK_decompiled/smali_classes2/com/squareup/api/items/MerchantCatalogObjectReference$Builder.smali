.class public final Lcom/squareup/api/items/MerchantCatalogObjectReference$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MerchantCatalogObjectReference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/MerchantCatalogObjectReference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/MerchantCatalogObjectReference;",
        "Lcom/squareup/api/items/MerchantCatalogObjectReference$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_object_token:Ljava/lang/String;

.field public version:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 101
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/MerchantCatalogObjectReference;
    .locals 4

    .line 116
    new-instance v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v1, p0, Lcom/squareup/api/items/MerchantCatalogObjectReference$Builder;->catalog_object_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/MerchantCatalogObjectReference$Builder;->version:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/api/items/MerchantCatalogObjectReference;-><init>(Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/api/items/MerchantCatalogObjectReference$Builder;->build()Lcom/squareup/api/items/MerchantCatalogObjectReference;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_token(Ljava/lang/String;)Lcom/squareup/api/items/MerchantCatalogObjectReference$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/api/items/MerchantCatalogObjectReference$Builder;->catalog_object_token:Ljava/lang/String;

    return-object p0
.end method

.method public version(Ljava/lang/Long;)Lcom/squareup/api/items/MerchantCatalogObjectReference$Builder;
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/api/items/MerchantCatalogObjectReference$Builder;->version:Ljava/lang/Long;

    return-object p0
.end method
