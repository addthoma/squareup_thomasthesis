.class final Lcom/squareup/api/items/FavoritesListPosition$ProtoAdapter_FavoritesListPosition;
.super Lcom/squareup/wire/ProtoAdapter;
.source "FavoritesListPosition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/FavoritesListPosition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FavoritesListPosition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/api/items/FavoritesListPosition;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 152
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/api/items/FavoritesListPosition;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/items/FavoritesListPosition;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 173
    new-instance v0, Lcom/squareup/api/items/FavoritesListPosition$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/FavoritesListPosition$Builder;-><init>()V

    .line 174
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 175
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 181
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 179
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/FavoritesListPosition$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/FavoritesListPosition$Builder;

    goto :goto_0

    .line 178
    :cond_1
    sget-object v3, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/FavoritesListPosition$Builder;->favorite_object(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/FavoritesListPosition$Builder;

    goto :goto_0

    .line 177
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/FavoritesListPosition$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/FavoritesListPosition$Builder;

    goto :goto_0

    .line 185
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/FavoritesListPosition$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 186
    invoke-virtual {v0}, Lcom/squareup/api/items/FavoritesListPosition$Builder;->build()Lcom/squareup/api/items/FavoritesListPosition;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 150
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/FavoritesListPosition$ProtoAdapter_FavoritesListPosition;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/items/FavoritesListPosition;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/items/FavoritesListPosition;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 165
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/FavoritesListPosition;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 166
    sget-object v0, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/FavoritesListPosition;->favorite_object:Lcom/squareup/api/sync/ObjectId;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 167
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/FavoritesListPosition;->ordinal:Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 168
    invoke-virtual {p2}, Lcom/squareup/api/items/FavoritesListPosition;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 150
    check-cast p2, Lcom/squareup/api/items/FavoritesListPosition;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/api/items/FavoritesListPosition$ProtoAdapter_FavoritesListPosition;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/items/FavoritesListPosition;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/api/items/FavoritesListPosition;)I
    .locals 4

    .line 157
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/items/FavoritesListPosition;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/FavoritesListPosition;->favorite_object:Lcom/squareup/api/sync/ObjectId;

    const/4 v3, 0x2

    .line 158
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/FavoritesListPosition;->ordinal:Ljava/lang/Integer;

    const/4 v3, 0x3

    .line 159
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 160
    invoke-virtual {p1}, Lcom/squareup/api/items/FavoritesListPosition;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 150
    check-cast p1, Lcom/squareup/api/items/FavoritesListPosition;

    invoke-virtual {p0, p1}, Lcom/squareup/api/items/FavoritesListPosition$ProtoAdapter_FavoritesListPosition;->encodedSize(Lcom/squareup/api/items/FavoritesListPosition;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/api/items/FavoritesListPosition;)Lcom/squareup/api/items/FavoritesListPosition;
    .locals 2

    .line 191
    invoke-virtual {p1}, Lcom/squareup/api/items/FavoritesListPosition;->newBuilder()Lcom/squareup/api/items/FavoritesListPosition$Builder;

    move-result-object p1

    .line 192
    iget-object v0, p1, Lcom/squareup/api/items/FavoritesListPosition$Builder;->favorite_object:Lcom/squareup/api/sync/ObjectId;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/items/FavoritesListPosition$Builder;->favorite_object:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectId;

    iput-object v0, p1, Lcom/squareup/api/items/FavoritesListPosition$Builder;->favorite_object:Lcom/squareup/api/sync/ObjectId;

    .line 193
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/api/items/FavoritesListPosition$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 194
    invoke-virtual {p1}, Lcom/squareup/api/items/FavoritesListPosition$Builder;->build()Lcom/squareup/api/items/FavoritesListPosition;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 150
    check-cast p1, Lcom/squareup/api/items/FavoritesListPosition;

    invoke-virtual {p0, p1}, Lcom/squareup/api/items/FavoritesListPosition$ProtoAdapter_FavoritesListPosition;->redact(Lcom/squareup/api/items/FavoritesListPosition;)Lcom/squareup/api/items/FavoritesListPosition;

    move-result-object p1

    return-object p1
.end method
