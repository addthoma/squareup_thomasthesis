.class public final Lcom/squareup/api/items/ItemModifierList$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ItemModifierList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/ItemModifierList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/ItemModifierList;",
        "Lcom/squareup/api/items/ItemModifierList$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

.field public id:Ljava/lang/String;

.field public is_conversational:Ljava/lang/Boolean;

.field public name:Ljava/lang/String;

.field public ordinal:Ljava/lang/Integer;

.field public selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;

.field public v2_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 189
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/ItemModifierList;
    .locals 10

    .line 244
    new-instance v9, Lcom/squareup/api/items/ItemModifierList;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierList$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/ItemModifierList$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/items/ItemModifierList$Builder;->ordinal:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/api/items/ItemModifierList$Builder;->selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    iget-object v5, p0, Lcom/squareup/api/items/ItemModifierList$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v6, p0, Lcom/squareup/api/items/ItemModifierList$Builder;->is_conversational:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/api/items/ItemModifierList$Builder;->v2_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/api/items/ItemModifierList;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/items/ItemModifierList$SelectionType;Lcom/squareup/api/items/MerchantCatalogObjectReference;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 174
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierList$Builder;->build()Lcom/squareup/api/items/ItemModifierList;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_reference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/api/items/ItemModifierList$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierList$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierList$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public is_conversational(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemModifierList$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierList$Builder;->is_conversational:Ljava/lang/Boolean;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierList$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemModifierList$Builder;
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierList$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public selection_type(Lcom/squareup/api/items/ItemModifierList$SelectionType;)Lcom/squareup/api/items/ItemModifierList$Builder;
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierList$Builder;->selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    return-object p0
.end method

.method public v2_id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;
    .locals 0

    .line 238
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierList$Builder;->v2_id:Ljava/lang/String;

    return-object p0
.end method
