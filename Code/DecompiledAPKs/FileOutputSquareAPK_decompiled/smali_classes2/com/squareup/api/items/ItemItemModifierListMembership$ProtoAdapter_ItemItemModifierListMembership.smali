.class final Lcom/squareup/api/items/ItemItemModifierListMembership$ProtoAdapter_ItemItemModifierListMembership;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ItemItemModifierListMembership.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/ItemItemModifierListMembership;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ItemItemModifierListMembership"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/api/items/ItemItemModifierListMembership;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 291
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/api/items/ItemItemModifierListMembership;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/items/ItemItemModifierListMembership;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 323
    new-instance v0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;-><init>()V

    .line 324
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 325
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 343
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 341
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->hidden_from_customer(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    goto :goto_0

    .line 340
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->enabled(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    goto :goto_0

    .line 339
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->max_selected_modifiers(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    goto :goto_0

    .line 338
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->min_selected_modifiers(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    goto :goto_0

    .line 337
    :pswitch_4
    iget-object v3, v0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->item_modifier_option_overrides:Ljava/util/List;

    sget-object v4, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 331
    :pswitch_5
    :try_start_0
    sget-object v4, Lcom/squareup/api/items/Visibility;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/Visibility;

    invoke-virtual {v0, v4}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->visibility(Lcom/squareup/api/items/Visibility;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 333
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 328
    :pswitch_6
    sget-object v3, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->item(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    goto :goto_0

    .line 327
    :pswitch_7
    sget-object v3, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->modifier_list(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    goto :goto_0

    .line 347
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 348
    invoke-virtual {v0}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->build()Lcom/squareup/api/items/ItemItemModifierListMembership;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 289
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/ItemItemModifierListMembership$ProtoAdapter_ItemItemModifierListMembership;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/items/ItemItemModifierListMembership;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/items/ItemItemModifierListMembership;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 310
    sget-object v0, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/ItemItemModifierListMembership;->item:Lcom/squareup/api/sync/ObjectId;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 311
    sget-object v0, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/ItemItemModifierListMembership;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 312
    sget-object v0, Lcom/squareup/api/items/Visibility;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/ItemItemModifierListMembership;->visibility:Lcom/squareup/api/items/Visibility;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 313
    sget-object v0, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/api/items/ItemItemModifierListMembership;->item_modifier_option_overrides:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 314
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/ItemItemModifierListMembership;->min_selected_modifiers:Ljava/lang/Integer;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 315
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/ItemItemModifierListMembership;->max_selected_modifiers:Ljava/lang/Integer;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 316
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/ItemItemModifierListMembership;->enabled:Ljava/lang/Boolean;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 317
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/ItemItemModifierListMembership;->hidden_from_customer:Ljava/lang/Boolean;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 318
    invoke-virtual {p2}, Lcom/squareup/api/items/ItemItemModifierListMembership;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 289
    check-cast p2, Lcom/squareup/api/items/ItemItemModifierListMembership;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/api/items/ItemItemModifierListMembership$ProtoAdapter_ItemItemModifierListMembership;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/items/ItemItemModifierListMembership;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/api/items/ItemItemModifierListMembership;)I
    .locals 4

    .line 296
    sget-object v0, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/items/ItemItemModifierListMembership;->item:Lcom/squareup/api/sync/ObjectId;

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/ItemItemModifierListMembership;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    const/4 v3, 0x1

    .line 297
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/Visibility;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/ItemItemModifierListMembership;->visibility:Lcom/squareup/api/items/Visibility;

    const/4 v3, 0x3

    .line 298
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 299
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/api/items/ItemItemModifierListMembership;->item_modifier_option_overrides:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/ItemItemModifierListMembership;->min_selected_modifiers:Ljava/lang/Integer;

    const/4 v3, 0x5

    .line 300
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/ItemItemModifierListMembership;->max_selected_modifiers:Ljava/lang/Integer;

    const/4 v3, 0x6

    .line 301
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/ItemItemModifierListMembership;->enabled:Ljava/lang/Boolean;

    const/4 v3, 0x7

    .line 302
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/ItemItemModifierListMembership;->hidden_from_customer:Ljava/lang/Boolean;

    const/16 v3, 0x8

    .line 303
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 304
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemItemModifierListMembership;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 289
    check-cast p1, Lcom/squareup/api/items/ItemItemModifierListMembership;

    invoke-virtual {p0, p1}, Lcom/squareup/api/items/ItemItemModifierListMembership$ProtoAdapter_ItemItemModifierListMembership;->encodedSize(Lcom/squareup/api/items/ItemItemModifierListMembership;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/api/items/ItemItemModifierListMembership;)Lcom/squareup/api/items/ItemItemModifierListMembership;
    .locals 2

    .line 353
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemItemModifierListMembership;->newBuilder()Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    move-result-object p1

    .line 354
    iget-object v0, p1, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectId;

    iput-object v0, p1, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    .line 355
    :cond_0
    iget-object v0, p1, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/api/sync/ObjectId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectId;

    iput-object v0, p1, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    .line 356
    :cond_1
    iget-object v0, p1, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->item_modifier_option_overrides:Ljava/util/List;

    sget-object v1, Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 357
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 358
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->build()Lcom/squareup/api/items/ItemItemModifierListMembership;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 289
    check-cast p1, Lcom/squareup/api/items/ItemItemModifierListMembership;

    invoke-virtual {p0, p1}, Lcom/squareup/api/items/ItemItemModifierListMembership$ProtoAdapter_ItemItemModifierListMembership;->redact(Lcom/squareup/api/items/ItemItemModifierListMembership;)Lcom/squareup/api/items/ItemItemModifierListMembership;

    move-result-object p1

    return-object p1
.end method
