.class final Lcom/squareup/api/items/PricingType$ProtoAdapter_PricingType;
.super Lcom/squareup/wire/EnumAdapter;
.source "PricingType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/PricingType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PricingType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/api/items/PricingType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 47
    const-class v0, Lcom/squareup/api/items/PricingType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/api/items/PricingType;
    .locals 0

    .line 52
    invoke-static {p1}, Lcom/squareup/api/items/PricingType;->fromValue(I)Lcom/squareup/api/items/PricingType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 45
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/PricingType$ProtoAdapter_PricingType;->fromValue(I)Lcom/squareup/api/items/PricingType;

    move-result-object p1

    return-object p1
.end method
