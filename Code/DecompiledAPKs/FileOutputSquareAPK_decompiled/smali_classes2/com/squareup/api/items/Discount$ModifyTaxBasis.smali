.class public final enum Lcom/squareup/api/items/Discount$ModifyTaxBasis;
.super Ljava/lang/Enum;
.source "Discount.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Discount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ModifyTaxBasis"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/Discount$ModifyTaxBasis$ProtoAdapter_ModifyTaxBasis;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/Discount$ModifyTaxBasis;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/Discount$ModifyTaxBasis;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/Discount$ModifyTaxBasis;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DO_NOT_MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

.field public static final enum MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 534
    new-instance v0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    const/4 v1, 0x0

    const-string v2, "MODIFY_TAX_BASIS"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/api/items/Discount$ModifyTaxBasis;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    .line 536
    new-instance v0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    const/4 v2, 0x1

    const-string v3, "DO_NOT_MODIFY_TAX_BASIS"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/api/items/Discount$ModifyTaxBasis;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->DO_NOT_MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    .line 533
    sget-object v3, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->DO_NOT_MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->$VALUES:[Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    .line 538
    new-instance v0, Lcom/squareup/api/items/Discount$ModifyTaxBasis$ProtoAdapter_ModifyTaxBasis;

    invoke-direct {v0}, Lcom/squareup/api/items/Discount$ModifyTaxBasis$ProtoAdapter_ModifyTaxBasis;-><init>()V

    sput-object v0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 542
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 543
    iput p3, p0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/Discount$ModifyTaxBasis;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 552
    :cond_0
    sget-object p0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->DO_NOT_MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    return-object p0

    .line 551
    :cond_1
    sget-object p0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/Discount$ModifyTaxBasis;
    .locals 1

    .line 533
    const-class v0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/Discount$ModifyTaxBasis;
    .locals 1

    .line 533
    sget-object v0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->$VALUES:[Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    invoke-virtual {v0}, [Lcom/squareup/api/items/Discount$ModifyTaxBasis;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 559
    iget v0, p0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->value:I

    return v0
.end method
