.class public Lcom/squareup/api/TransactionSuccessEvent;
.super Lcom/squareup/api/RegisterApiEvent;
.source "TransactionSuccessEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/TransactionSuccessEvent$Reason;
    }
.end annotation


# instance fields
.field public final client_bill_id:Ljava/lang/String;

.field public final reason:Ljava/lang/String;

.field public final sequenceUuid:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "api_sequence_uuid"
    .end annotation
.end field

.field public final server_bill_id:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/api/TransactionSuccessEvent$Reason;J)V
    .locals 6

    .line 32
    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->API_TRANSACTION_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-wide v4, p6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/RegisterApiEvent;-><init>(Lcom/squareup/util/Clock;Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;J)V

    .line 33
    iput-object p2, p0, Lcom/squareup/api/TransactionSuccessEvent;->sequenceUuid:Ljava/lang/String;

    .line 34
    invoke-virtual {p5}, Lcom/squareup/api/TransactionSuccessEvent$Reason;->name()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/TransactionSuccessEvent;->reason:Ljava/lang/String;

    .line 35
    iget-object p1, p4, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/api/TransactionSuccessEvent;->client_bill_id:Ljava/lang/String;

    .line 36
    iget-object p1, p4, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/api/TransactionSuccessEvent;->server_bill_id:Ljava/lang/String;

    return-void
.end method
