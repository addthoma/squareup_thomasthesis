.class public Lcom/squareup/backgroundjob/RealParams;
.super Ljava/lang/Object;
.source "RealParams.java"

# interfaces
.implements Lcom/squareup/backgroundjob/JobParams;


# instance fields
.field private final delegate:Lcom/evernote/android/job/Job$Params;


# direct methods
.method public constructor <init>(Lcom/evernote/android/job/Job$Params;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/squareup/backgroundjob/RealParams;->delegate:Lcom/evernote/android/job/Job$Params;

    return-void
.end method


# virtual methods
.method public getEndMs()J
    .locals 2

    .line 27
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealParams;->delegate:Lcom/evernote/android/job/Job$Params;

    invoke-virtual {v0}, Lcom/evernote/android/job/Job$Params;->getEndMs()J

    move-result-wide v0

    return-wide v0
.end method

.method public getExtras()Lcom/evernote/android/job/util/support/PersistableBundleCompat;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealParams;->delegate:Lcom/evernote/android/job/Job$Params;

    invoke-virtual {v0}, Lcom/evernote/android/job/Job$Params;->getExtras()Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealParams;->delegate:Lcom/evernote/android/job/Job$Params;

    invoke-virtual {v0}, Lcom/evernote/android/job/Job$Params;->getId()I

    move-result v0

    return v0
.end method

.method public getScheduledAt()J
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealParams;->delegate:Lcom/evernote/android/job/Job$Params;

    invoke-virtual {v0}, Lcom/evernote/android/job/Job$Params;->getScheduledAt()J

    move-result-wide v0

    return-wide v0
.end method

.method public getStartMs()J
    .locals 2

    .line 23
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealParams;->delegate:Lcom/evernote/android/job/Job$Params;

    invoke-virtual {v0}, Lcom/evernote/android/job/Job$Params;->getStartMs()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealParams;->delegate:Lcom/evernote/android/job/Job$Params;

    invoke-virtual {v0}, Lcom/evernote/android/job/Job$Params;->getTag()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
