.class public final Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;
.super Ljava/lang/Object;
.source "BackgroundJobNotifications.java"


# static fields
.field private static final INTEGER_GENERATOR:Ljava/util/Random;

.field public static final NOTIFICATION_ID_KEY:Ljava/lang/String;

.field public static final NOTIFICATION_TEXT_KEY:Ljava/lang/String;

.field public static final NOTIFICATION_TITLE_KEY:Ljava/lang/String;

.field public static final QUALIFIED_NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 18
    const-class v0, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->QUALIFIED_NAME:Ljava/lang/String;

    .line 19
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->QUALIFIED_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":notification.text.key"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->NOTIFICATION_TEXT_KEY:Ljava/lang/String;

    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->QUALIFIED_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":notification.title.key"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->NOTIFICATION_TITLE_KEY:Ljava/lang/String;

    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->QUALIFIED_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":notification.id.key"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->NOTIFICATION_ID_KEY:Ljava/lang/String;

    .line 23
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->INTEGER_GENERATOR:Ljava/util/Random;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static applyExtrasForNotification(Lcom/evernote/android/job/JobRequest;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 52
    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->getExtras()Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    move-result-object p0

    .line 53
    sget-object v0, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->NOTIFICATION_ID_KEY:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;->putInt(Ljava/lang/String;I)V

    .line 54
    sget-object p1, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->NOTIFICATION_TITLE_KEY:Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    sget-object p1, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->NOTIFICATION_TEXT_KEY:Ljava/lang/String;

    invoke-virtual {p0, p1, p3}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static applyExtrasForNotification(Lcom/evernote/android/job/JobRequest;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->INTEGER_GENERATOR:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    invoke-static {p0, v0, p1, p2}, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->applyExtrasForNotification(Lcom/evernote/android/job/JobRequest;ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method
