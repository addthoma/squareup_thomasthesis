.class public Lcom/squareup/analytics/NoDeepLinkHelper;
.super Ljava/lang/Object;
.source "NoDeepLinkHelper.java"

# interfaces
.implements Lcom/squareup/analytics/DeepLinkHelper;


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public anonymousVisitorToken()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 24
    invoke-static {}, Lrx/Observable;->never()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public deepLinkPath()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 28
    invoke-static {}, Lrx/Observable;->never()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public encryptedEmail()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 19
    invoke-static {}, Lrx/Observable;->never()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public init()V
    .locals 0

    return-void
.end method

.method public onActivityCreate(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method
