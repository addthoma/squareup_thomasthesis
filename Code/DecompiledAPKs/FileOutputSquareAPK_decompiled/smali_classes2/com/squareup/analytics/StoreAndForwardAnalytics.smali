.class public Lcom/squareup/analytics/StoreAndForwardAnalytics;
.super Ljava/lang/Object;
.source "StoreAndForwardAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/analytics/StoreAndForwardAnalytics$State;
    }
.end annotation


# static fields
.field private static final PARAM_COUNTRY:Ljava/lang/String; = "country"

.field private static final PARAM_LOCATION:Ljava/lang/String; = "location"

.field private static final PARAM_STATE:Ljava/lang/String; = "state"

.field private static final STORE_AND_FORWARD_BAD_ACCOUNT_STATUS_RESPONSE:Ljava/lang/String; = "snf bad-account-status-response"

.field private static final STORE_AND_FORWARD_BAD_CERTIFICATE:Ljava/lang/String; = "snf bad-certificate"

.field private static final STORE_AND_FORWARD_INVALID_KEY:Ljava/lang/String; = "snf invalid-key"

.field private static final STORE_AND_FORWARD_INVALID_KEY_EXPIRATION:Ljava/lang/String; = "snf invalid-key-expiration"

.field private static final STORE_AND_FORWARD_NO_ENCRYPTOR:Ljava/lang/String; = "snf no-encryptor"

.field private static final STORE_AND_FORWARD_OPT_IN_BUTTON:Ljava/lang/String; = "snf opt-in-button"

.field private static final STORE_AND_FORWARD_OPT_IN_GO_OFFLINE_PRESSED:Ljava/lang/String; = "snf opt-in-go-offline-pressed"

.field private static final STORE_AND_FORWARD_OPT_IN_RETRY_PRESSED:Ljava/lang/String; = "snf opt-in-retry-pressed"

.field private static final STORE_AND_FORWARD_OUT_OF_COUNTRY:Ljava/lang/String; = "snf out-of-country"

.field private static final STORE_AND_FORWARD_PAYMENT_AUTHORIZED:Ljava/lang/String; = "snf payment-authorized"

.field private static final STORE_AND_FORWARD_PAYMENT_AUTHORIZE_FAILED:Ljava/lang/String; = "snf payment-authorize-failed"

.field private static final STORE_AND_FORWARD_PAYMENT_ENQUEUED:Ljava/lang/String; = "snf payment-enqueued"

.field private static final STORE_AND_FORWARD_QUICK_ENABLE_ACCEPTED:Ljava/lang/String; = "snf quick-enable-accepted"

.field private static final STORE_AND_FORWARD_QUICK_ENABLE_PROPOSED:Ljava/lang/String; = "snf quick-enable-proposed"

.field private static final STORE_AND_FORWARD_QUICK_ENABLE_REFUSED:Ljava/lang/String; = "snf quick-enable-refused"

.field private static final STORE_AND_FORWARD_SETTING:Ljava/lang/String; = "snf setting"

.field private static final STORE_AND_FORWARD_TRANSACTION_LIMIT_EXCEEDED:Ljava/lang/String; = "snf transaction-limit-exceeded"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public logAuthorizeFailed()V
    .locals 3

    .line 51
    iget-object v0, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "snf payment-authorize-failed"

    invoke-interface {v0, v2, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public logAuthorized()V
    .locals 3

    .line 55
    iget-object v0, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "snf payment-authorized"

    invoke-interface {v0, v2, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public logBadAccountStatus()V
    .locals 3

    .line 79
    iget-object v0, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "snf bad-account-status-response"

    invoke-interface {v0, v2, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public logBadCertificate()V
    .locals 3

    .line 63
    iget-object v0, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "snf bad-certificate"

    invoke-interface {v0, v2, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public logGoOfflinePressed()V
    .locals 3

    .line 113
    iget-object v0, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "snf opt-in-go-offline-pressed"

    invoke-interface {v0, v2, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public logInvalidKey()V
    .locals 3

    .line 67
    iget-object v0, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "snf invalid-key"

    invoke-interface {v0, v2, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public logInvalidKeyExpiration()V
    .locals 3

    .line 71
    iget-object v0, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "snf invalid-key-expiration"

    invoke-interface {v0, v2, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public logNoEncryptor()V
    .locals 3

    .line 75
    iget-object v0, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "snf no-encryptor"

    invoke-interface {v0, v2, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public logOptInEnabledState(Lcom/squareup/analytics/StoreAndForwardAnalytics$State;)V
    .locals 4

    .line 83
    iget-object v0, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "state"

    aput-object v3, v1, v2

    .line 84
    invoke-virtual {p1}, Lcom/squareup/analytics/StoreAndForwardAnalytics$State;->name()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const-string p1, "snf opt-in-button"

    .line 83
    invoke-interface {v0, p1, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public logOptInRetry()V
    .locals 3

    .line 97
    iget-object v0, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "snf opt-in-retry-pressed"

    invoke-interface {v0, v2, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public logOutOfCountry(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .line 45
    iget-object v0, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "country"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 p1, 0x2

    const-string v2, "location"

    aput-object v2, v1, p1

    const/4 p1, 0x3

    aput-object p2, v1, p1

    const-string p1, "snf out-of-country"

    invoke-interface {v0, p1, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public logPaymentEnqueued()V
    .locals 3

    .line 59
    iget-object v0, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "snf payment-enqueued"

    invoke-interface {v0, v2, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public logQuickEnableAccepted()V
    .locals 3

    .line 105
    iget-object v0, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "snf quick-enable-accepted"

    invoke-interface {v0, v2, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public logQuickEnableProposed()V
    .locals 3

    .line 101
    iget-object v0, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "snf quick-enable-proposed"

    invoke-interface {v0, v2, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public logQuickEnableRefused()V
    .locals 3

    .line 109
    iget-object v0, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "snf quick-enable-refused"

    invoke-interface {v0, v2, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public logSettingState(Lcom/squareup/analytics/StoreAndForwardAnalytics$State;)V
    .locals 4

    .line 88
    iget-object v0, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "state"

    aput-object v3, v1, v2

    .line 89
    invoke-virtual {p1}, Lcom/squareup/analytics/StoreAndForwardAnalytics$State;->name()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const-string p1, "snf setting"

    .line 88
    invoke-interface {v0, p1, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public logTransactionLimitExceeded()V
    .locals 3

    .line 93
    iget-object v0, p0, Lcom/squareup/analytics/StoreAndForwardAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "snf transaction-limit-exceeded"

    invoke-interface {v0, v2, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method
