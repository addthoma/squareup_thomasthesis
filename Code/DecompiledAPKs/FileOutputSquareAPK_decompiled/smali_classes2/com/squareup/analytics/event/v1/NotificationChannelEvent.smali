.class public Lcom/squareup/analytics/event/v1/NotificationChannelEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "NotificationChannelEvent.java"


# instance fields
.field public canBypassDnd:Z

.field public canShowBadge:Z

.field public channelName:Ljava/lang/String;

.field public importance:Ljava/lang/String;

.field public lockScreenVisibility:Ljava/lang/String;

.field public shouldShowLights:Z

.field public shouldVibrate:Z


# direct methods
.method public constructor <init>(Landroid/app/NotificationChannel;)V
    .locals 2

    .line 21
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->STATUS:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "Notification Channel Attributes"

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 23
    invoke-virtual {p1}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/notification/Channels;->getChannelById(Ljava/lang/String;)Lcom/squareup/notification/Channels;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/notification/Channels;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/analytics/event/v1/NotificationChannelEvent;->channelName:Ljava/lang/String;

    .line 24
    invoke-virtual {p1}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/NotificationChannelEvent;->getImportanceString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/analytics/event/v1/NotificationChannelEvent;->importance:Ljava/lang/String;

    .line 25
    invoke-virtual {p1}, Landroid/app/NotificationChannel;->getLockscreenVisibility()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/NotificationChannelEvent;->getLockScreenVisibilityString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/analytics/event/v1/NotificationChannelEvent;->lockScreenVisibility:Ljava/lang/String;

    .line 26
    invoke-virtual {p1}, Landroid/app/NotificationChannel;->canBypassDnd()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/analytics/event/v1/NotificationChannelEvent;->canBypassDnd:Z

    .line 27
    invoke-virtual {p1}, Landroid/app/NotificationChannel;->canShowBadge()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/analytics/event/v1/NotificationChannelEvent;->canShowBadge:Z

    .line 28
    invoke-virtual {p1}, Landroid/app/NotificationChannel;->shouldVibrate()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/analytics/event/v1/NotificationChannelEvent;->shouldVibrate:Z

    .line 29
    invoke-virtual {p1}, Landroid/app/NotificationChannel;->shouldShowLights()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/analytics/event/v1/NotificationChannelEvent;->shouldShowLights:Z

    return-void
.end method

.method private getImportanceString(I)Ljava/lang/String;
    .locals 2

    if-eqz p1, :cond_5

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IMPORTANCE_UNSPECIFIED:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, "IMPORTANCE_MAX"

    return-object p1

    :cond_1
    const-string p1, "IMPORTANCE_HIGH"

    return-object p1

    :cond_2
    const-string p1, "IMPORTANCE_DEFAULT"

    return-object p1

    :cond_3
    const-string p1, "IMPORTANCE_LOW"

    return-object p1

    :cond_4
    const-string p1, "IMPORTANCE_MIN"

    return-object p1

    :cond_5
    const-string p1, "IMPORTANCE_NONE"

    return-object p1
.end method

.method private getLockScreenVisibilityString(I)Ljava/lang/String;
    .locals 2

    const/16 v0, -0x3e8

    if-eq p1, v0, :cond_3

    const/4 v0, -0x1

    if-eq p1, v0, :cond_2

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UNKNOWN:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, "VISIBILITY_PUBLIC"

    return-object p1

    :cond_1
    const-string p1, "VISIBILITY_PRIVATE"

    return-object p1

    :cond_2
    const-string p1, "VISIBILITY_SECRET"

    return-object p1

    :cond_3
    const-string p1, "VISIBILITY_NO_OVERRIDE"

    return-object p1
.end method
