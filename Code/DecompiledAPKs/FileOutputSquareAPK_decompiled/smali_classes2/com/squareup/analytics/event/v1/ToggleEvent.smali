.class public Lcom/squareup/analytics/event/v1/ToggleEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "ToggleEvent.java"


# instance fields
.field public final detail:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterActionName;Z)V
    .locals 0

    .line 12
    invoke-direct {p0, p1}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    if-eqz p2, :cond_0

    const-string p1, "ON"

    goto :goto_0

    :cond_0
    const-string p1, "OFF"

    .line 13
    :goto_0
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/ToggleEvent;->detail:Ljava/lang/String;

    return-void
.end method
