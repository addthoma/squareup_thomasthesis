.class public Lcom/squareup/analytics/event/v1/StatusEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "StatusEvent.java"


# instance fields
.field public final previousStatus:Ljava/lang/String;

.field public final status:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->STATUS:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-direct {p0, v0, p1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 17
    iput-object p2, p0, Lcom/squareup/analytics/event/v1/StatusEvent;->status:Ljava/lang/String;

    if-eqz p3, :cond_0

    goto :goto_0

    :cond_0
    const-string p3, "UNKNOWN"

    .line 19
    :goto_0
    iput-object p3, p0, Lcom/squareup/analytics/event/v1/StatusEvent;->previousStatus:Ljava/lang/String;

    return-void
.end method
