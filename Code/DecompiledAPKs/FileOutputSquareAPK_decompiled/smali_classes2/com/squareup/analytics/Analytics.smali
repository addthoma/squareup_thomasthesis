.class public interface abstract Lcom/squareup/analytics/Analytics;
.super Ljava/lang/Object;
.source "Analytics.java"

# interfaces
.implements Lcom/squareup/eventstream/CommonProperties;


# virtual methods
.method public abstract clearUser()V
.end method

.method public varargs abstract log(Ljava/lang/String;[Ljava/lang/String;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract logAction(Lcom/squareup/analytics/RegisterActionName;)V
.end method

.method public abstract logCrashSync(Lcom/squareup/analytics/event/v1/CrashEvent;)V
.end method

.method public abstract logError(Lcom/squareup/analytics/RegisterErrorName;)V
.end method

.method public abstract logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V
.end method

.method public abstract logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V
.end method

.method public abstract logPaymentFinished(Ljava/lang/String;)V
.end method

.method public abstract logReaderEvent(Lcom/squareup/analytics/RegisterReaderName;)V
.end method

.method public abstract logSelect(Lcom/squareup/analytics/RegisterSelectName;)V
.end method

.method public abstract logTap(Lcom/squareup/analytics/RegisterTapName;)V
.end method

.method public abstract logView(Lcom/squareup/analytics/RegisterViewName;)V
.end method

.method public abstract onActivityStart()V
.end method

.method public abstract setAdvertisingId(Ljava/lang/String;)V
.end method

.method public abstract setAnonymizedUserToken(Ljava/lang/String;)V
.end method

.method public abstract setLocation(Landroid/location/Location;)V
.end method

.method public abstract setUser(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method
