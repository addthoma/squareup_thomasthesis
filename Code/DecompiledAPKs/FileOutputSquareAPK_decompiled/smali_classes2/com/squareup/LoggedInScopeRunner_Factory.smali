.class public final Lcom/squareup/LoggedInScopeRunner_Factory;
.super Ljava/lang/Object;
.source "LoggedInScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/LoggedInScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final activeCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final additionalBusServicesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;",
            ">;"
        }
    .end annotation
.end field

.field private final additionalServicesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;>;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final autoCaptureJobCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureJobCreator;",
            ">;"
        }
    .end annotation
.end field

.field private final autoCaptureTimerStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureTimerStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerLocaleOverrideProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderPowerMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPowerMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final curatedImageProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;"
        }
    .end annotation
.end field

.field private final dippedCardTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final dipperEventHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/DipperEventHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementModeDeciderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final forwardedPaymentManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/ForwardedPaymentManager;",
            ">;"
        }
    .end annotation
.end field

.field private final headsetConnectionListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;"
        }
    .end annotation
.end field

.field private final headsetStateDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/HeadsetStateDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final locationMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final loggedInExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/StoppableSerialExecutor;",
            ">;"
        }
    .end annotation
.end field

.field private final loggedInUrlMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodesSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodesSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentCounterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentCounter;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingCapturesLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingCapturesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingCapturesQueueConformerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingTransactionsStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;"
        }
    .end annotation
.end field

.field private final picassoMemoryCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Cache;",
            ">;"
        }
    .end annotation
.end field

.field private final printJobQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintJobQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final printerScoutSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterScoutScheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final queueBertPublicKeyManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager;",
            ">;"
        }
    .end annotation
.end field

.field private final queueServiceStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final readerBusBoyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/terminal/ReaderEventBusBoy;",
            ">;"
        }
    .end annotation
.end field

.field private final readerEarlyPowerupOpportunistProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;",
            ">;"
        }
    .end annotation
.end field

.field private final realEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/RealEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final rootBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final safetyNetRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/safetynet/SafetyNetRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final serverClockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ServerClock;",
            ">;"
        }
    .end annotation
.end field

.field private final storedPaymentsQueueConformerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;",
            ">;"
        }
    .end annotation
.end field

.field private final tasksProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final tasksQueueConformerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/TasksQueueConformer;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets$InternalTickets;",
            ">;"
        }
    .end annotation
.end field

.field private final timeTrackingSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/TimeTrackingSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/DipperEventHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPowerMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/ForwardedPaymentManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/HeadsetStateDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Cache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentCounter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterScoutScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets$InternalTickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ServerClock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/StoppableSerialExecutor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintJobQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/safetynet/SafetyNetRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureJobCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureTimerStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/TasksQueueConformer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/terminal/ReaderEventBusBoy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodesSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/TimeTrackingSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/RealEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 195
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 196
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 197
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->rootBusProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 198
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 199
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->dipperEventHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 200
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->cardReaderPowerMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 201
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 202
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 203
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->readerEarlyPowerupOpportunistProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 204
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->forwardedPaymentManagerProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 205
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->headsetStateDispatcherProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 206
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->notificationManagerProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 207
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->pendingTransactionsStoreProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 208
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->picassoMemoryCacheProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 209
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 210
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->tasksProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 211
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->paymentCounterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 212
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->pendingCapturesProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 213
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->printerScoutSchedulerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 214
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->ticketsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 215
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->serverClockProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 216
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->loggedInExecutorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 217
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->printJobQueueProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 218
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 219
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 220
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->safetyNetRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 221
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->curatedImageProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 222
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->queueBertPublicKeyManagerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p29

    .line 223
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->queueServiceStarterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p30

    .line 224
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->autoCaptureJobCreatorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p31

    .line 225
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->autoCaptureTimerStarterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p32

    .line 226
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->storedPaymentsQueueConformerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p33

    .line 227
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->pendingCapturesQueueConformerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p34

    .line 228
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->tasksQueueConformerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p35

    .line 229
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->pendingCapturesLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p36

    .line 230
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->loggedInUrlMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p37

    .line 231
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->readerBusBoyProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p38

    .line 232
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p39

    .line 233
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->additionalServicesProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p40

    .line 234
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->additionalBusServicesProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p41

    .line 235
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->headsetConnectionListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p42

    .line 236
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->locationMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p43

    .line 237
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->passcodesSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p44

    .line 238
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->timeTrackingSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p45

    .line 239
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->realEmployeeManagementProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p46

    .line 240
    iput-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/LoggedInScopeRunner_Factory;
    .locals 48
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/DipperEventHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPowerMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/ForwardedPaymentManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/HeadsetStateDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Cache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentCounter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterScoutScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets$InternalTickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ServerClock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/StoppableSerialExecutor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintJobQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/safetynet/SafetyNetRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureJobCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureTimerStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/TasksQueueConformer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/terminal/ReaderEventBusBoy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodesSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/TimeTrackingSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/RealEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;)",
            "Lcom/squareup/LoggedInScopeRunner_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    move-object/from16 v34, p33

    move-object/from16 v35, p34

    move-object/from16 v36, p35

    move-object/from16 v37, p36

    move-object/from16 v38, p37

    move-object/from16 v39, p38

    move-object/from16 v40, p39

    move-object/from16 v41, p40

    move-object/from16 v42, p41

    move-object/from16 v43, p42

    move-object/from16 v44, p43

    move-object/from16 v45, p44

    move-object/from16 v46, p45

    .line 291
    new-instance v47, Lcom/squareup/LoggedInScopeRunner_Factory;

    move-object/from16 v0, v47

    invoke-direct/range {v0 .. v46}, Lcom/squareup/LoggedInScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v47
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/dipper/DipperEventHandler;Lcom/squareup/cardreader/CardReaderPowerMonitor;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/cardreader/HeadsetStateDispatcher;Landroid/app/NotificationManager;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/picasso/Cache;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/print/PrinterScoutScheduler;Lcom/squareup/tickets/Tickets$InternalTickets;Lcom/squareup/account/ServerClock;Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/print/PrintJobQueue;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/safetynet/SafetyNetRunner;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/payment/offline/QueueBertPublicKeyManager;Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/autocapture/AutoCaptureJobCreator;Lcom/squareup/autocapture/AutoCaptureTimerStarter;Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;Lcom/squareup/queue/redundant/TasksQueueConformer;Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;Lcom/squareup/log/terminal/ReaderEventBusBoy;Lcom/squareup/permissions/PasscodeEmployeeManagement;Ljava/util/Set;Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/TimeTrackingSettings;Lcom/squareup/permissions/RealEmployeeManagement;Lcom/squareup/buyer/language/BuyerLocaleOverride;)Lcom/squareup/LoggedInScopeRunner;
    .locals 48
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            "Lcom/squareup/cardreader/dipper/DipperEventHandler;",
            "Lcom/squareup/cardreader/CardReaderPowerMonitor;",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            "Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;",
            "Lcom/squareup/payment/offline/ForwardedPaymentManager;",
            "Lcom/squareup/cardreader/HeadsetStateDispatcher;",
            "Landroid/app/NotificationManager;",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            "Lcom/squareup/picasso/Cache;",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/cardreader/PaymentCounter;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/print/PrinterScoutScheduler;",
            "Lcom/squareup/tickets/Tickets$InternalTickets;",
            "Lcom/squareup/account/ServerClock;",
            "Lcom/squareup/thread/executor/StoppableSerialExecutor;",
            "Lcom/squareup/print/PrintJobQueue;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            "Lcom/squareup/safetynet/SafetyNetRunner;",
            "Lcom/squareup/merchantimages/CuratedImage;",
            "Lcom/squareup/payment/offline/QueueBertPublicKeyManager;",
            "Lcom/squareup/queue/QueueServiceStarter;",
            "Lcom/squareup/autocapture/AutoCaptureJobCreator;",
            "Lcom/squareup/autocapture/AutoCaptureTimerStarter;",
            "Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;",
            "Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;",
            "Lcom/squareup/queue/redundant/TasksQueueConformer;",
            "Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;",
            "Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;",
            "Lcom/squareup/log/terminal/ReaderEventBusBoy;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;",
            "Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            "Lcom/squareup/permissions/PasscodesSettings;",
            "Lcom/squareup/permissions/TimeTrackingSettings;",
            "Lcom/squareup/permissions/RealEmployeeManagement;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ")",
            "Lcom/squareup/LoggedInScopeRunner;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    move-object/from16 v34, p33

    move-object/from16 v35, p34

    move-object/from16 v36, p35

    move-object/from16 v37, p36

    move-object/from16 v38, p37

    move-object/from16 v39, p38

    move-object/from16 v40, p39

    move-object/from16 v41, p40

    move-object/from16 v42, p41

    move-object/from16 v43, p42

    move-object/from16 v44, p43

    move-object/from16 v45, p44

    move-object/from16 v46, p45

    .line 322
    new-instance v47, Lcom/squareup/LoggedInScopeRunner;

    move-object/from16 v0, v47

    invoke-direct/range {v0 .. v46}, Lcom/squareup/LoggedInScopeRunner;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/dipper/DipperEventHandler;Lcom/squareup/cardreader/CardReaderPowerMonitor;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/cardreader/HeadsetStateDispatcher;Landroid/app/NotificationManager;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/picasso/Cache;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/print/PrinterScoutScheduler;Lcom/squareup/tickets/Tickets$InternalTickets;Lcom/squareup/account/ServerClock;Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/print/PrintJobQueue;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/safetynet/SafetyNetRunner;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/payment/offline/QueueBertPublicKeyManager;Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/autocapture/AutoCaptureJobCreator;Lcom/squareup/autocapture/AutoCaptureTimerStarter;Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;Lcom/squareup/queue/redundant/TasksQueueConformer;Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;Lcom/squareup/log/terminal/ReaderEventBusBoy;Lcom/squareup/permissions/PasscodeEmployeeManagement;Ljava/util/Set;Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/TimeTrackingSettings;Lcom/squareup/permissions/RealEmployeeManagement;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V

    return-object v47
.end method


# virtual methods
.method public get()Lcom/squareup/LoggedInScopeRunner;
    .locals 48

    move-object/from16 v0, p0

    .line 245
    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->rootBusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/cardreader/CardReaderFactory;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->dipperEventHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/cardreader/dipper/DipperEventHandler;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->cardReaderPowerMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/cardreader/CardReaderPowerMonitor;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/cardreader/DippedCardTracker;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->readerEarlyPowerupOpportunistProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->forwardedPaymentManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/payment/offline/ForwardedPaymentManager;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->headsetStateDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/cardreader/HeadsetStateDispatcher;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->notificationManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Landroid/app/NotificationManager;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->pendingTransactionsStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/payment/pending/PendingTransactionsStore;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->picassoMemoryCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/picasso/Cache;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->tasksProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->paymentCounterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/cardreader/PaymentCounter;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->pendingCapturesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->printerScoutSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/print/PrinterScoutScheduler;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/tickets/Tickets$InternalTickets;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->serverClockProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/account/ServerClock;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->loggedInExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/thread/executor/StoppableSerialExecutor;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->printJobQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/print/PrintJobQueue;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/permissions/EmployeeManagementModeDecider;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->safetyNetRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/safetynet/SafetyNetRunner;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->curatedImageProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/merchantimages/CuratedImage;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->queueBertPublicKeyManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v29, v1

    check-cast v29, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->queueServiceStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v30, v1

    check-cast v30, Lcom/squareup/queue/QueueServiceStarter;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->autoCaptureJobCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v31, v1

    check-cast v31, Lcom/squareup/autocapture/AutoCaptureJobCreator;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->autoCaptureTimerStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v32, v1

    check-cast v32, Lcom/squareup/autocapture/AutoCaptureTimerStarter;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->storedPaymentsQueueConformerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v33, v1

    check-cast v33, Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->pendingCapturesQueueConformerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v34, v1

    check-cast v34, Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->tasksQueueConformerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v35, v1

    check-cast v35, Lcom/squareup/queue/redundant/TasksQueueConformer;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->pendingCapturesLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v36, v1

    check-cast v36, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->loggedInUrlMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v37, v1

    check-cast v37, Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->readerBusBoyProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v38, v1

    check-cast v38, Lcom/squareup/log/terminal/ReaderEventBusBoy;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v39, v1

    check-cast v39, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->additionalServicesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v40, v1

    check-cast v40, Ljava/util/Set;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->additionalBusServicesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v41, v1

    check-cast v41, Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->headsetConnectionListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v42, v1

    check-cast v42, Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->locationMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v43, v1

    check-cast v43, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->passcodesSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v44, v1

    check-cast v44, Lcom/squareup/permissions/PasscodesSettings;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->timeTrackingSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v45, v1

    check-cast v45, Lcom/squareup/permissions/TimeTrackingSettings;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->realEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v46, v1

    check-cast v46, Lcom/squareup/permissions/RealEmployeeManagement;

    iget-object v1, v0, Lcom/squareup/LoggedInScopeRunner_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v47, v1

    check-cast v47, Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-static/range {v2 .. v47}, Lcom/squareup/LoggedInScopeRunner_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/dipper/DipperEventHandler;Lcom/squareup/cardreader/CardReaderPowerMonitor;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/cardreader/HeadsetStateDispatcher;Landroid/app/NotificationManager;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/picasso/Cache;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/print/PrinterScoutScheduler;Lcom/squareup/tickets/Tickets$InternalTickets;Lcom/squareup/account/ServerClock;Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/print/PrintJobQueue;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/safetynet/SafetyNetRunner;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/payment/offline/QueueBertPublicKeyManager;Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/autocapture/AutoCaptureJobCreator;Lcom/squareup/autocapture/AutoCaptureTimerStarter;Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;Lcom/squareup/queue/redundant/TasksQueueConformer;Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;Lcom/squareup/log/terminal/ReaderEventBusBoy;Lcom/squareup/permissions/PasscodeEmployeeManagement;Ljava/util/Set;Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/TimeTrackingSettings;Lcom/squareup/permissions/RealEmployeeManagement;Lcom/squareup/buyer/language/BuyerLocaleOverride;)Lcom/squareup/LoggedInScopeRunner;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 51
    invoke-virtual {p0}, Lcom/squareup/LoggedInScopeRunner_Factory;->get()Lcom/squareup/LoggedInScopeRunner;

    move-result-object v0

    return-object v0
.end method
