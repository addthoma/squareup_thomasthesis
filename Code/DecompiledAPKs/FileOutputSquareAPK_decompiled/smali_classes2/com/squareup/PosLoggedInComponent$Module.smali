.class public abstract Lcom/squareup/PosLoggedInComponent$Module;
.super Ljava/lang/Object;
.source "PosLoggedInComponent.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/cashmanagement/CashDrawerShiftsModule;,
        Lcom/squareup/catalogfees/CatalogFeesModule;,
        Lcom/squareup/orderentry/CheckoutLibraryListModule;,
        Lcom/squareup/log/CheckoutLogModule;,
        Lcom/squareup/CommonJailKeeperModule;,
        Lcom/squareup/giftcard/activation/GiftCardLoadingScopeModule;,
        Lcom/squareup/merchantprofile/MerchantProfileModule;,
        Lcom/squareup/orderhub/settings/OrderHubDeviceSettingsModule;,
        Lcom/squareup/orderhub/alerts/OrderHubOrdersLoadedModule;,
        Lcom/squareup/account/PreferencesCacheLoggedInModule;,
        Lcom/squareup/prices/PricingEngineModule;,
        Lcom/squareup/pushmessages/PushMessageLoggedInModule;,
        Lcom/squareup/redeemrewards/RedeemRewardsModule;,
        Lcom/squareup/skipreceiptscreen/RealSkipReceiptScreenModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/PosLoggedInComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$provideAdditionalBusServices$0(Ljava/util/List;)Ljava/util/List;
    .locals 0

    return-object p0
.end method

.method static provideAdditionalBusServices(Lcom/squareup/print/PrinterScoutsProvider;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/pending/PaymentNotifier;Lcom/squareup/print/RealTicketAutoIdentifiers;)Lcom/squareup/LoggedInScopeRunner$AdditionalBusServices;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 208
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/squareup/badbus/BadBusRegistrant;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 p1, 0x1

    aput-object p2, v1, p1

    const/4 p1, 0x2

    aput-object p3, v1, p1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 213
    invoke-interface {p0}, Lcom/squareup/print/PrinterScoutsProvider;->availableScouts()Ljava/util/Set;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 214
    new-instance p0, Lcom/squareup/-$$Lambda$PosLoggedInComponent$Module$tKgtiu0S5BD_tgrMk9-o8qNg2kI;

    invoke-direct {p0, v0}, Lcom/squareup/-$$Lambda$PosLoggedInComponent$Module$tKgtiu0S5BD_tgrMk9-o8qNg2kI;-><init>(Ljava/util/List;)V

    return-object p0
.end method

.method static provideAdditionalServicesAsSet(Lcom/squareup/payment/pending/PaymentNotifier;Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;Lcom/squareup/tickets/TicketCountsCache;Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;Lcom/squareup/print/HardwarePrintersAnalyticsLogger;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/prices/PricingEngineController;Lcom/squareup/cogs/CogsPurger;Lcom/squareup/jail/CogsJailKeeper;Lcom/squareup/permissions/EmployeeCacheUpdater;Lcom/squareup/payment/CrmBillPaymentListener;Lcom/squareup/payment/ledger/LedgerUploaderRunner;)Ljava/util/Set;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldagger/multibindings/ElementsIntoSet;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/pending/PaymentNotifier;",
            "Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;",
            "Lcom/squareup/tickets/TicketCountsCache;",
            "Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;",
            "Lcom/squareup/print/HardwarePrintersAnalyticsLogger;",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            "Lcom/squareup/prices/PricingEngineController;",
            "Lcom/squareup/cogs/CogsPurger;",
            "Lcom/squareup/jail/CogsJailKeeper;",
            "Lcom/squareup/permissions/EmployeeCacheUpdater;",
            "Lcom/squareup/payment/CrmBillPaymentListener;",
            "Lcom/squareup/payment/ledger/LedgerUploaderRunner;",
            ")",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation

    .line 185
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0xc

    new-array v1, v1, [Lmortar/Scoped;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 p0, 0x1

    aput-object p1, v1, p0

    const/4 p0, 0x2

    aput-object p2, v1, p0

    const/4 p0, 0x3

    aput-object p3, v1, p0

    const/4 p0, 0x4

    aput-object p4, v1, p0

    const/4 p0, 0x5

    aput-object p5, v1, p0

    const/4 p0, 0x6

    aput-object p6, v1, p0

    const/4 p0, 0x7

    aput-object p7, v1, p0

    const/16 p0, 0x8

    aput-object p8, v1, p0

    const/16 p0, 0x9

    aput-object p9, v1, p0

    const/16 p0, 0xa

    aput-object p10, v1, p0

    const/16 p0, 0xb

    aput-object p11, v1, p0

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public static provideCatalogLocalizerFactory(Lcom/squareup/util/Res;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Landroid/app/Application;)Lcom/squareup/shared/i18n/Localizer;
    .locals 17
    .param p10    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/ForTaxPercentage;
        .end annotation
    .end param
    .param p13    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/Cents;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Landroid/app/Application;",
            ")",
            "Lcom/squareup/shared/i18n/Localizer;"
        }
    .end annotation

    .line 148
    invoke-virtual/range {p14 .. p14}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 149
    new-instance v16, Lcom/squareup/crossplatform/i18n/LocalizerImpl;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move-object/from16 v15, p13

    invoke-direct/range {v0 .. v15}, Lcom/squareup/crossplatform/i18n/LocalizerImpl;-><init>(Lcom/squareup/util/Res;Ljava/lang/String;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V

    return-object v16
.end method

.method static providePageListCacehBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .param p0    # Lcom/google/gson/Gson;
        .annotation runtime Lcom/squareup/gson/WireGson;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;",
            ">;"
        }
    .end annotation

    .line 232
    const-class v0, Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;

    const-string v1, "page-lists"

    invoke-static {p0, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method

.method static provideTempPhotoDirectory(Ljava/io/File;)Ljava/io/File;
    .locals 4
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 218
    new-instance v0, Ljava/io/File;

    const-string v1, "photo-temp"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 220
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 221
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result p0

    if-eqz p0, :cond_0

    return-object v0

    .line 222
    :cond_0
    new-instance p0, Ljava/lang/RuntimeException;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 223
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    const-string v0, "Failed to create temp photo directory (exists=%s): %s"

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method static providesMissedLoyaltyEnqueuer(Ljavax/inject/Provider;Lcom/squareup/loyalty/LoyaltySettings;)Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ")",
            "Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;"
        }
    .end annotation

    .line 237
    new-instance v0, Lcom/squareup/queue/loyalty/RealMissedLoyaltyEnqueuer;

    invoke-interface {p0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/loyalty/RealMissedLoyaltyEnqueuer;-><init>(Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/loyalty/LoyaltySettings;)V

    return-object v0
.end method


# virtual methods
.method abstract newCogs(Lcom/squareup/cogs/RealCogs;)Lcom/squareup/cogs/Cogs;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract newPricingEngineController(Lcom/squareup/prices/RealPricingEngineController;)Lcom/squareup/prices/PricingEngineController;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideInvoiceCache(Lcom/squareup/invoices/RealInvoiceUnitCache;)Lcom/squareup/invoicesappletapi/InvoiceUnitCache;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideJailKeeper(Lcom/squareup/jail/CogsJailKeeper;)Lcom/squareup/jailkeeper/JailKeeper;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
