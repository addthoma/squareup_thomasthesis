.class public final Lcom/squareup/anrchaperone/AnrChaperoneModule_ProvideChaperoneThreadFactory;
.super Ljava/lang/Object;
.source "AnrChaperoneModule_ProvideChaperoneThreadFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/thread/executor/SerialExecutor;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/anrchaperone/AnrChaperoneModule;


# direct methods
.method public constructor <init>(Lcom/squareup/anrchaperone/AnrChaperoneModule;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/anrchaperone/AnrChaperoneModule_ProvideChaperoneThreadFactory;->module:Lcom/squareup/anrchaperone/AnrChaperoneModule;

    return-void
.end method

.method public static create(Lcom/squareup/anrchaperone/AnrChaperoneModule;)Lcom/squareup/anrchaperone/AnrChaperoneModule_ProvideChaperoneThreadFactory;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/anrchaperone/AnrChaperoneModule_ProvideChaperoneThreadFactory;

    invoke-direct {v0, p0}, Lcom/squareup/anrchaperone/AnrChaperoneModule_ProvideChaperoneThreadFactory;-><init>(Lcom/squareup/anrchaperone/AnrChaperoneModule;)V

    return-object v0
.end method

.method public static provideChaperoneThread(Lcom/squareup/anrchaperone/AnrChaperoneModule;)Lcom/squareup/thread/executor/SerialExecutor;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/anrchaperone/AnrChaperoneModule;->provideChaperoneThread()Lcom/squareup/thread/executor/SerialExecutor;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/thread/executor/SerialExecutor;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/thread/executor/SerialExecutor;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/anrchaperone/AnrChaperoneModule_ProvideChaperoneThreadFactory;->module:Lcom/squareup/anrchaperone/AnrChaperoneModule;

    invoke-static {v0}, Lcom/squareup/anrchaperone/AnrChaperoneModule_ProvideChaperoneThreadFactory;->provideChaperoneThread(Lcom/squareup/anrchaperone/AnrChaperoneModule;)Lcom/squareup/thread/executor/SerialExecutor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/anrchaperone/AnrChaperoneModule_ProvideChaperoneThreadFactory;->get()Lcom/squareup/thread/executor/SerialExecutor;

    move-result-object v0

    return-object v0
.end method
