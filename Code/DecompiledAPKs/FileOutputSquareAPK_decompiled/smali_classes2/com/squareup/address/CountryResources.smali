.class public Lcom/squareup/address/CountryResources;
.super Ljava/lang/Object;
.source "CountryResources.java"


# static fields
.field public static final NO_SECONDARY_INSTITUTION_NUMBER:I = -0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static apartmentHint(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 70
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    .line 75
    sget p0, Lcom/squareup/country/resources/R$string;->apartment_suite_hint:I

    return p0

    .line 73
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->apartment_unit_hint:I

    return p0
.end method

.method public static billingMessage(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 107
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 115
    sget p0, Lcom/squareup/country/resources/R$string;->crm_cardonfile_verifypostal_message:I

    return p0

    .line 113
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->crm_cardonfile_verifyzip_message:I

    return p0

    .line 111
    :cond_1
    sget p0, Lcom/squareup/country/resources/R$string;->crm_cardonfile_verifypostcode_message_uk:I

    return p0

    .line 109
    :cond_2
    sget p0, Lcom/squareup/country/resources/R$string;->crm_cardonfile_verifypostcode_message:I

    return p0
.end method

.method public static billingMessageNoCardName(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 119
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 127
    sget p0, Lcom/squareup/country/resources/R$string;->crm_cardonfile_verifypostal_message_no_card_name:I

    return p0

    .line 125
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->crm_cardonfile_verifyzip_message_no_card_name:I

    return p0

    .line 123
    :cond_1
    sget p0, Lcom/squareup/country/resources/R$string;->crm_cardonfile_verifypostcode_message_uk_no_card_name:I

    return p0

    .line 121
    :cond_2
    sget p0, Lcom/squareup/country/resources/R$string;->crm_cardonfile_verifypostcode_message_no_card_name:I

    return p0
.end method

.method public static billingTitle(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 131
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 138
    sget p0, Lcom/squareup/country/resources/R$string;->crm_cardonfile_verifypostal_title:I

    return p0

    .line 136
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->crm_cardonfile_verifyzip_title:I

    return p0

    .line 134
    :cond_1
    sget p0, Lcom/squareup/country/resources/R$string;->crm_cardonfile_verifypostcode_title:I

    return p0
.end method

.method public static cardOnFileLinkEmailDisclaimer(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 249
    sget-object v0, Lcom/squareup/CountryCode;->GB:Lcom/squareup/CountryCode;

    if-ne p0, v0, :cond_0

    sget p0, Lcom/squareup/country/resources/R$string;->crm_cardonfile_customer_email_disclaimer_uk:I

    goto :goto_0

    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->crm_cardonfile_customer_email_disclaimer:I

    :goto_0
    return p0
.end method

.method public static countryCodesOrderedByNameWithPaymentFirst(Landroid/content/res/Resources;)[Lcom/squareup/CountryCode;
    .locals 4

    .line 37
    new-instance v0, Ljava/util/TreeSet;

    new-instance v1, Lcom/squareup/address/CountryFlagsAndNames$CountryByNameComparator;

    invoke-direct {v1, p0}, Lcom/squareup/address/CountryFlagsAndNames$CountryByNameComparator;-><init>(Landroid/content/res/Resources;)V

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 38
    invoke-static {}, Lcom/squareup/address/CountryFlagsAndNames;->values()[Lcom/squareup/address/CountryFlagsAndNames;

    move-result-object p0

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 40
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result p0

    new-array p0, p0, [Lcom/squareup/CountryCode;

    .line 42
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/address/CountryFlagsAndNames;

    add-int/lit8 v3, v1, 0x1

    .line 43
    iget-object v2, v2, Lcom/squareup/address/CountryFlagsAndNames;->countryCode:Lcom/squareup/CountryCode;

    aput-object v2, p0, v1

    move v1, v3

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public static countryCodesWithPayments(Landroid/content/res/Resources;)[Lcom/squareup/CountryCode;
    .locals 3

    .line 56
    new-instance v0, Ljava/util/TreeSet;

    new-instance v1, Lcom/squareup/address/CountryFlagsAndNames$CountryByNameComparator;

    invoke-direct {v1, p0}, Lcom/squareup/address/CountryFlagsAndNames$CountryByNameComparator;-><init>(Landroid/content/res/Resources;)V

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 57
    invoke-static {}, Lcom/squareup/address/CountryFlagsAndNames;->values()[Lcom/squareup/address/CountryFlagsAndNames;

    move-result-object p0

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 59
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 61
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/address/CountryFlagsAndNames;

    .line 62
    iget-object v2, v1, Lcom/squareup/address/CountryFlagsAndNames;->countryCode:Lcom/squareup/CountryCode;

    iget-boolean v2, v2, Lcom/squareup/CountryCode;->hasPayments:Z

    if-eqz v2, :cond_0

    .line 63
    iget-object v1, v1, Lcom/squareup/address/CountryFlagsAndNames;->countryCode:Lcom/squareup/CountryCode;

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 66
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/squareup/CountryCode;

    invoke-interface {p0, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Lcom/squareup/CountryCode;

    return-object p0
.end method

.method public static countryName(Lcom/squareup/CountryCode;)I
    .locals 0

    .line 27
    invoke-static {p0}, Lcom/squareup/address/CountryFlagsAndNames;->forCountryCode(Lcom/squareup/CountryCode;)Lcom/squareup/address/CountryFlagsAndNames;

    move-result-object p0

    iget p0, p0, Lcom/squareup/address/CountryFlagsAndNames;->countryName:I

    return p0
.end method

.method public static emailReceiptDisclaimerId(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 223
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 234
    sget p0, Lcom/squareup/country/resources/R$string;->checkout_flow_email_receipt_disclaimer_default:I

    return p0

    .line 232
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->checkout_flow_email_receipt_disclaimer_us:I

    return p0

    .line 230
    :cond_1
    sget p0, Lcom/squareup/country/resources/R$string;->checkout_flow_email_receipt_disclaimer_uk:I

    return p0

    .line 226
    :cond_2
    sget p0, Lcom/squareup/country/resources/R$string;->checkout_flow_email_receipt_disclaimer_au_japan:I

    return p0

    .line 228
    :cond_3
    sget p0, Lcom/squareup/country/resources/R$string;->checkout_flow_email_receipt_disclaimer_canada:I

    return p0
.end method

.method public static flagId(Lcom/squareup/CountryCode;)I
    .locals 0

    .line 31
    invoke-static {p0}, Lcom/squareup/address/CountryFlagsAndNames;->forCountryCode(Lcom/squareup/CountryCode;)Lcom/squareup/address/CountryFlagsAndNames;

    move-result-object p0

    iget p0, p0, Lcom/squareup/address/CountryFlagsAndNames;->flagId:I

    return p0
.end method

.method public static generalReceiptDisclaimerId(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 239
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    .line 244
    sget p0, Lcom/squareup/country/resources/R$string;->empty:I

    return p0

    .line 242
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->checkout_flow_receipt_disclaimer:I

    return p0
.end method

.method public static invalidPrimaryInstitutionNumberMessage(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 282
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    .line 293
    sget p0, Lcom/squareup/country/resources/R$string;->invalid_routing_message:I

    return p0

    .line 290
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->invalid_sort_code_message:I

    return p0

    .line 284
    :cond_1
    sget p0, Lcom/squareup/country/resources/R$string;->invalid_bsb_number_message:I

    return p0

    .line 288
    :cond_2
    sget p0, Lcom/squareup/country/resources/R$string;->invalid_bank_message:I

    return p0

    .line 286
    :cond_3
    sget p0, Lcom/squareup/country/resources/R$string;->invalid_institution_number_message:I

    return p0
.end method

.method public static invalidSecondaryInstitutionNumberMessage(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 297
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, -0x1

    return p0

    .line 301
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->invalid_branch_message:I

    return p0

    .line 299
    :cond_1
    sget p0, Lcom/squareup/country/resources/R$string;->invalid_transit_number_message:I

    return p0
.end method

.method public static legalDocuments(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 155
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 163
    sget p0, Lcom/squareup/country/resources/R$string;->activation_legal_documents_other:I

    return p0

    .line 161
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->activation_legal_documents_us:I

    return p0

    .line 159
    :cond_1
    sget p0, Lcom/squareup/country/resources/R$string;->activation_legal_documents_jp:I

    return p0

    .line 157
    :cond_2
    sget p0, Lcom/squareup/country/resources/R$string;->activation_legal_documents_ca:I

    return p0
.end method

.method public static legalDocumentsForHelperAndPrompt(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 168
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 176
    sget p0, Lcom/squareup/country/resources/R$string;->activation_legal_documents_helper_prompt_other:I

    return p0

    .line 174
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->activation_legal_documents_helper_prompt_us:I

    return p0

    .line 172
    :cond_1
    sget p0, Lcom/squareup/country/resources/R$string;->activation_legal_documents_helper_prompt_jp:I

    return p0

    .line 170
    :cond_2
    sget p0, Lcom/squareup/country/resources/R$string;->activation_legal_documents_helper_prompt_ca:I

    return p0
.end method

.method public static legalUrlNamesId(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 190
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 198
    sget p0, Lcom/squareup/country/resources/R$array;->activation_legal_url_names_other:I

    return p0

    .line 196
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$array;->activation_legal_url_names_us:I

    return p0

    .line 194
    :cond_1
    sget p0, Lcom/squareup/country/resources/R$array;->activation_legal_url_names_jp:I

    return p0

    .line 192
    :cond_2
    sget p0, Lcom/squareup/country/resources/R$array;->activation_legal_url_names_ca:I

    return p0
.end method

.method public static legalUrlsId(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 180
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 186
    sget p0, Lcom/squareup/country/resources/R$array;->activation_legal_urls_other:I

    return p0

    .line 184
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$array;->activation_legal_urls_us:I

    return p0

    .line 182
    :cond_1
    sget p0, Lcom/squareup/country/resources/R$array;->activation_legal_urls_ca:I

    return p0
.end method

.method public static noReceiptId(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 202
    sget-object v0, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-ne p0, v0, :cond_0

    sget p0, Lcom/squareup/country/resources/R$string;->buyer_no_thanks:I

    goto :goto_0

    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->buyer_receipt_no_receipt:I

    :goto_0
    return p0
.end method

.method public static postalCardHint(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 95
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 103
    sget p0, Lcom/squareup/country/resources/R$string;->postal_hint:I

    return p0

    .line 100
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->zip:I

    return p0

    .line 98
    :cond_1
    sget p0, Lcom/squareup/country/resources/R$string;->postcode_hint:I

    return p0
.end method

.method public static postalHint(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 79
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 87
    sget p0, Lcom/squareup/country/resources/R$string;->postal_hint:I

    return p0

    .line 84
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->zip_code_hint:I

    return p0

    .line 82
    :cond_1
    sget p0, Lcom/squareup/country/resources/R$string;->postcode_hint:I

    return p0
.end method

.method public static prefersAlphanumericKeyboardForPostal(Lcom/squareup/CountryCode;)Z
    .locals 1

    .line 313
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static primaryInstitutionNumber(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 255
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x6

    if-eq p0, v0, :cond_0

    .line 267
    sget p0, Lcom/squareup/country/resources/R$string;->routing_number:I

    return p0

    .line 261
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->bic:I

    return p0

    .line 265
    :cond_1
    sget p0, Lcom/squareup/country/resources/R$string;->sort_code:I

    return p0

    .line 257
    :cond_2
    sget p0, Lcom/squareup/country/resources/R$string;->bsb_number:I

    return p0

    .line 263
    :cond_3
    sget p0, Lcom/squareup/country/resources/R$string;->bank_code:I

    return p0

    .line 259
    :cond_4
    sget p0, Lcom/squareup/country/resources/R$string;->institution_number:I

    return p0
.end method

.method public static secondaryInstitutionNumber(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 272
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, -0x1

    return p0

    .line 276
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->branch_code:I

    return p0

    .line 274
    :cond_1
    sget p0, Lcom/squareup/country/resources/R$string;->transit_number:I

    return p0
.end method

.method public static smsReceiptDisclaimerId(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 206
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 217
    sget p0, Lcom/squareup/country/resources/R$string;->checkout_flow_sms_receipt_disclaimer_default:I

    return p0

    .line 215
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->checkout_flow_sms_receipt_disclaimer_us:I

    return p0

    .line 213
    :cond_1
    sget p0, Lcom/squareup/country/resources/R$string;->checkout_flow_sms_receipt_disclaimer_uk:I

    return p0

    .line 209
    :cond_2
    sget p0, Lcom/squareup/country/resources/R$string;->checkout_flow_sms_receipt_disclaimer_au_japan:I

    return p0

    .line 211
    :cond_3
    sget p0, Lcom/squareup/country/resources/R$string;->checkout_flow_sms_receipt_disclaimer_canada:I

    return p0
.end method

.method public static stateHint(Lcom/squareup/CountryCode;)I
    .locals 1

    .line 142
    sget-object v0, Lcom/squareup/address/CountryResources$1;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 150
    sget p0, Lcom/squareup/country/resources/R$string;->province_hint:I

    return p0

    .line 148
    :cond_0
    sget p0, Lcom/squareup/country/resources/R$string;->state_hint:I

    return p0

    .line 146
    :cond_1
    sget p0, Lcom/squareup/country/resources/R$string;->county_hint:I

    return p0

    .line 144
    :cond_2
    sget p0, Lcom/squareup/country/resources/R$string;->state_or_territory_hint:I

    return p0
.end method
