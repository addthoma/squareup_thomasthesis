.class public abstract Lcom/squareup/address/AddressLayout;
.super Landroid/widget/LinearLayout;
.source "AddressLayout.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/address/AddressLayout$Component;
    }
.end annotation


# instance fields
.field private addressRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/address/Address;",
            ">;"
        }
    .end annotation
.end field

.field final apartment:Landroid/widget/EditText;

.field final city:Landroid/widget/EditText;

.field final cityPicker:Landroid/widget/Spinner;

.field final cityStateRow:Landroid/widget/LinearLayout;

.field private countryCode:Lcom/squareup/CountryCode;

.field final postal:Lcom/squareup/widgets/SelectableEditText;

.field runner:Lcom/squareup/address/AddressLayoutRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private scrubber:Lcom/squareup/text/PostalScrubber;

.field private scrubbingTextWatcher:Landroid/text/TextWatcher;

.field private shouldLookup:Z

.field final state:Lcom/squareup/widgets/SelectableEditText;

.field final street:Landroid/widget/EditText;

.field private watcher:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 76
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x1

    .line 68
    iput-boolean p2, p0, Lcom/squareup/address/AddressLayout;->shouldLookup:Z

    .line 73
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/address/AddressLayout;->addressRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 78
    const-class v0, Lcom/squareup/address/AddressLayout$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/address/AddressLayout$Component;

    invoke-interface {v0, p0}, Lcom/squareup/address/AddressLayout$Component;->inject(Lcom/squareup/address/AddressLayout;)V

    .line 80
    invoke-virtual {p0, p2}, Lcom/squareup/address/AddressLayout;->setOrientation(I)V

    .line 81
    invoke-virtual {p0}, Lcom/squareup/address/AddressLayout;->layoutResId()I

    move-result v0

    invoke-static {p1, v0, p0}, Lcom/squareup/address/AddressLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 83
    sget p1, Lcom/squareup/address/R$id;->address_street:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/squareup/address/AddressLayout;->street:Landroid/widget/EditText;

    .line 84
    sget p1, Lcom/squareup/address/R$id;->address_apartment:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/squareup/address/AddressLayout;->apartment:Landroid/widget/EditText;

    .line 85
    sget p1, Lcom/squareup/address/R$id;->address_city:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    .line 86
    sget p1, Lcom/squareup/address/R$id;->address_city_picker:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Spinner;

    iput-object p1, p0, Lcom/squareup/address/AddressLayout;->cityPicker:Landroid/widget/Spinner;

    .line 87
    sget p1, Lcom/squareup/address/R$id;->address_city_state_row:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/address/AddressLayout;->cityStateRow:Landroid/widget/LinearLayout;

    .line 88
    sget p1, Lcom/squareup/address/R$id;->address_state:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/SelectableEditText;

    iput-object p1, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    .line 89
    sget p1, Lcom/squareup/address/R$id;->address_postal:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/SelectableEditText;

    iput-object p1, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    .line 91
    iget-object p1, p0, Lcom/squareup/address/AddressLayout;->cityPicker:Landroid/widget/Spinner;

    invoke-virtual {p1, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 92
    new-instance p1, Lcom/squareup/text/ScrubbingTextWatcher;

    new-instance v0, Lcom/squareup/address/StateScrubber;

    invoke-direct {v0}, Lcom/squareup/address/StateScrubber;-><init>()V

    iget-object v1, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    invoke-direct {p1, v0, v1}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    iput-object p1, p0, Lcom/squareup/address/AddressLayout;->scrubbingTextWatcher:Landroid/text/TextWatcher;

    .line 93
    iget-object p1, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->scrubbingTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 94
    iget-object p1, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    new-array p2, p2, [Landroid/text/InputFilter;

    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v1, 0x0

    aput-object v0, p2, v1

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 95
    iget-object p1, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    const p2, 0x91000

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->setInputType(I)V

    .line 100
    iget-object p1, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    const p2, 0x80002

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->setInputType(I)V

    .line 101
    iget-object p1, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    new-instance p2, Lcom/squareup/address/AddressLayout$1;

    invoke-direct {p2, p0}, Lcom/squareup/address/AddressLayout$1;-><init>(Lcom/squareup/address/AddressLayout;)V

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 109
    new-instance p1, Lcom/squareup/address/AddressLayout$2;

    invoke-direct {p1, p0}, Lcom/squareup/address/AddressLayout$2;-><init>(Lcom/squareup/address/AddressLayout;)V

    .line 114
    iget-object p2, p0, Lcom/squareup/address/AddressLayout;->street:Landroid/widget/EditText;

    invoke-virtual {p2, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 115
    iget-object p2, p0, Lcom/squareup/address/AddressLayout;->apartment:Landroid/widget/EditText;

    invoke-virtual {p2, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 116
    iget-object p2, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    invoke-virtual {p2, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 117
    iget-object p2, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 118
    iget-object p2, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 120
    new-instance p1, Landroid/animation/LayoutTransition;

    invoke-direct {p1}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {p0, p1}, Lcom/squareup/address/AddressLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/address/AddressLayout;)Z
    .locals 0

    .line 50
    invoke-direct {p0}, Lcom/squareup/address/AddressLayout;->shouldLookup()Z

    move-result p0

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/address/AddressLayout;)Lcom/squareup/text/PostalScrubber;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/address/AddressLayout;->scrubber:Lcom/squareup/text/PostalScrubber;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/address/AddressLayout;)V
    .locals 0

    .line 50
    invoke-direct {p0}, Lcom/squareup/address/AddressLayout;->dispatchAddressChanged()V

    return-void
.end method

.method private clearAddress()V
    .locals 3

    .line 185
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->street:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->apartment:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 187
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 188
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    .line 189
    invoke-direct {p0, v1, v0}, Lcom/squareup/address/AddressLayout;->setPostal(Ljava/lang/String;Z)V

    .line 190
    invoke-virtual {p0, v1}, Lcom/squareup/address/AddressLayout;->setCountry(Lcom/squareup/CountryCode;)V

    return-void
.end method

.method private dispatchAddressChanged()V
    .locals 2

    .line 303
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->addressRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {p0}, Lcom/squareup/address/AddressLayout;->getAddress()Lcom/squareup/address/Address;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic lambda$ARyRA7b-aA-C3BVnQ6xnZxBsIg8(Lcom/squareup/address/AddressLayout;Lcom/squareup/server/address/PostalCodeResponse;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/address/AddressLayout;->onPostalCodeResponse(Lcom/squareup/server/address/PostalCodeResponse;)V

    return-void
.end method

.method public static synthetic lambda$u8-15fLcASQLXYKpPDSc8X9zrjo(Lcom/squareup/address/AddressLayout;Lcom/squareup/address/AddressLayoutRunner$AddressViewData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/address/AddressLayout;->updateView(Lcom/squareup/address/AddressLayoutRunner$AddressViewData;)V

    return-void
.end method

.method private notifyCountryCodeWasUpdated()V
    .locals 2

    .line 226
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->countryCode:Lcom/squareup/CountryCode;

    sget-object v1, Lcom/squareup/CountryCode;->CA:Lcom/squareup/CountryCode;

    if-ne v0, v1, :cond_0

    .line 227
    invoke-direct {p0}, Lcom/squareup/address/AddressLayout;->setPostalCanadaKeyboard()V

    goto :goto_0

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->countryCode:Lcom/squareup/CountryCode;

    sget-object v1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-ne v0, v1, :cond_1

    .line 229
    invoke-direct {p0}, Lcom/squareup/address/AddressLayout;->setPostalUsKeyboard()V

    goto :goto_0

    .line 231
    :cond_1
    invoke-direct {p0}, Lcom/squareup/address/AddressLayout;->setWorldKeyboard()V

    :goto_0
    return-void
.end method

.method private onPostalCodeResponse(Lcom/squareup/server/address/PostalCodeResponse;)V
    .locals 5

    .line 236
    invoke-virtual {p1}, Lcom/squareup/server/address/PostalCodeResponse;->getAllResults()Ljava/util/List;

    move-result-object v0

    .line 238
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 240
    invoke-virtual {p0}, Lcom/squareup/address/AddressLayout;->showEmptyManualEntry()V

    goto :goto_0

    .line 241
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v1, v2, :cond_1

    .line 243
    iget-object p1, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/address/PostalLocation;

    iget-object v1, v1, Lcom/squareup/server/address/PostalLocation;->city:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 244
    iget-object p1, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/address/PostalLocation;

    iget-object v0, v0, Lcom/squareup/server/address/PostalLocation;->state_abbreviation:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 245
    invoke-virtual {p0}, Lcom/squareup/address/AddressLayout;->showManualCityEntry()V

    goto :goto_0

    .line 248
    :cond_1
    iget-object v1, p0, Lcom/squareup/address/AddressLayout;->cityPicker:Landroid/widget/Spinner;

    new-instance v2, Lcom/squareup/address/CityAdapter;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4, v0}, Lcom/squareup/address/CityAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 249
    iget-object v1, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/address/PostalLocation;

    iget-object v0, v0, Lcom/squareup/server/address/PostalLocation;->state_abbreviation:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 250
    invoke-virtual {p0}, Lcom/squareup/address/AddressLayout;->showCityPicker()V

    .line 252
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->cityPicker:Landroid/widget/Spinner;

    invoke-virtual {p1}, Lcom/squareup/server/address/PostalCodeResponse;->getDefaultCityIndex()I

    move-result p1

    invoke-static {p1, v3}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    :goto_0
    return-void
.end method

.method private setPostal(Ljava/lang/String;Z)V
    .locals 0

    .line 429
    iput-boolean p2, p0, Lcom/squareup/address/AddressLayout;->shouldLookup:Z

    .line 430
    iget-object p2, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x1

    .line 431
    iput-boolean p1, p0, Lcom/squareup/address/AddressLayout;->shouldLookup:Z

    return-void
.end method

.method private setPostalCanadaKeyboard()V
    .locals 4

    .line 286
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setFocusable(Z)V

    .line 287
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 288
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    new-instance v2, Lcom/squareup/address/-$$Lambda$AddressLayout$oYGmLGd7j_LaiNXNEYk84a-d2R4;

    invoke-direct {v2, p0}, Lcom/squareup/address/-$$Lambda$AddressLayout$oYGmLGd7j_LaiNXNEYk84a-d2R4;-><init>(Lcom/squareup/address/AddressLayout;)V

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 290
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    iget-object v2, p0, Lcom/squareup/address/AddressLayout;->watcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 291
    invoke-static {}, Lcom/squareup/text/PostalScrubber;->forCanada()Lcom/squareup/text/PostalScrubber;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/address/AddressLayout;->scrubber:Lcom/squareup/text/PostalScrubber;

    .line 292
    new-instance v0, Lcom/squareup/text/ScrubbingTextWatcher;

    iget-object v2, p0, Lcom/squareup/address/AddressLayout;->scrubber:Lcom/squareup/text/PostalScrubber;

    iget-object v3, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    invoke-direct {v0, v2, v3}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    iput-object v0, p0, Lcom/squareup/address/AddressLayout;->watcher:Landroid/text/TextWatcher;

    .line 293
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    const v2, 0x81070

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->setInputType(I)V

    .line 296
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    iget-object v2, p0, Lcom/squareup/address/AddressLayout;->watcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 299
    invoke-virtual {p0, v1}, Lcom/squareup/address/AddressLayout;->showManualCityEntry(Z)V

    return-void
.end method

.method private setPostalUsKeyboard()V
    .locals 3

    .line 272
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setFocusable(Z)V

    .line 273
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 274
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/address/-$$Lambda$AddressLayout$hfPTslSXCA8D42w-gPJLarBT780;

    invoke-direct {v1, p0}, Lcom/squareup/address/-$$Lambda$AddressLayout$hfPTslSXCA8D42w-gPJLarBT780;-><init>(Lcom/squareup/address/AddressLayout;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 276
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    iget-object v1, p0, Lcom/squareup/address/AddressLayout;->watcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 277
    invoke-static {}, Lcom/squareup/text/PostalScrubber;->forUs()Lcom/squareup/text/PostalScrubber;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/address/AddressLayout;->scrubber:Lcom/squareup/text/PostalScrubber;

    .line 278
    new-instance v0, Lcom/squareup/text/ScrubbingTextWatcher;

    iget-object v1, p0, Lcom/squareup/address/AddressLayout;->scrubber:Lcom/squareup/text/PostalScrubber;

    iget-object v2, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    invoke-direct {v0, v1, v2}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    iput-object v0, p0, Lcom/squareup/address/AddressLayout;->watcher:Landroid/text/TextWatcher;

    .line 279
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    const v1, 0x80002

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setInputType(I)V

    .line 280
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    const-string v1, "0123456789-"

    invoke-static {v1}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 281
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    iget-object v1, p0, Lcom/squareup/address/AddressLayout;->watcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method private setWorldKeyboard()V
    .locals 3

    .line 257
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setFocusable(Z)V

    .line 258
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    const/4 v1, 0x0

    new-array v2, v1, [Landroid/text/InputFilter;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 259
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    iget-object v2, p0, Lcom/squareup/address/AddressLayout;->scrubbingTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 261
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    const v2, 0x82000

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->setInputType(I)V

    .line 263
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    iget-object v2, p0, Lcom/squareup/address/AddressLayout;->watcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 264
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    const v2, 0x81000

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->setInputType(I)V

    .line 267
    invoke-virtual {p0, v1}, Lcom/squareup/address/AddressLayout;->showManualCityEntry(Z)V

    return-void
.end method

.method private shouldLookup()Z
    .locals 2

    .line 425
    iget-boolean v0, p0, Lcom/squareup/address/AddressLayout;->shouldLookup:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->countryCode:Lcom/squareup/CountryCode;

    sget-object v1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private updateView(Lcom/squareup/address/AddressLayoutRunner$AddressViewData;)V
    .locals 2

    .line 218
    iget-object v0, p1, Lcom/squareup/address/AddressLayoutRunner$AddressViewData;->countryCode:Lcom/squareup/CountryCode;

    invoke-virtual {p0, v0}, Lcom/squareup/address/AddressLayout;->setCountry(Lcom/squareup/CountryCode;)V

    .line 220
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    iget v1, p1, Lcom/squareup/address/AddressLayoutRunner$AddressViewData;->stateHint:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setHint(I)V

    .line 221
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    iget v1, p1, Lcom/squareup/address/AddressLayoutRunner$AddressViewData;->postalHint:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setHint(I)V

    .line 222
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->apartment:Landroid/widget/EditText;

    iget p1, p1, Lcom/squareup/address/AddressLayoutRunner$AddressViewData;->apartmentHint:I

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHint(I)V

    return-void
.end method


# virtual methods
.method public address()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/address/Address;",
            ">;"
        }
    .end annotation

    .line 307
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->addressRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public disableAddress()V
    .locals 2

    .line 197
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->street:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 198
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->apartment:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 199
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 200
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->cityPicker:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 201
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    .line 202
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    return-void
.end method

.method public enableAddress()V
    .locals 2

    .line 209
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->street:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 210
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->apartment:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 211
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 212
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->cityPicker:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 213
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    .line 214
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    return-void
.end method

.method public getAddress()Lcom/squareup/address/Address;
    .locals 8

    .line 180
    new-instance v7, Lcom/squareup/address/Address;

    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->street:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->apartment:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    .line 181
    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/address/AddressLayout;->countryCode:Lcom/squareup/CountryCode;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/address/Address;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;)V

    return-object v7
.end method

.method public getCity()Landroid/widget/EditText;
    .locals 1

    .line 412
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    return-object v0
.end method

.method getCityPicker()Landroid/widget/Spinner;
    .locals 1

    .line 420
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->cityPicker:Landroid/widget/Spinner;

    return-object v0
.end method

.method public getCountry()Lcom/squareup/CountryCode;
    .locals 1

    .line 336
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->countryCode:Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public getEmptyField()Landroid/widget/TextView;
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Landroid/widget/TextView;

    .line 399
    iget-object v1, p0, Lcom/squareup/address/AddressLayout;->street:Landroid/widget/EditText;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Views;->getEmptyView([Landroid/widget/TextView;)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public getPostal()Landroid/widget/EditText;
    .locals 1

    .line 408
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    return-object v0
.end method

.method public getState()Landroid/widget/EditText;
    .locals 1

    .line 416
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    return-object v0
.end method

.method public getStreet()Landroid/widget/EditText;
    .locals 1

    .line 404
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->street:Landroid/widget/EditText;

    return-object v0
.end method

.method public hideManualCityEntry()V
    .locals 2

    .line 387
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->cityStateRow:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 388
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->cityPicker:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 389
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    return-void
.end method

.method public isSet()Z
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Landroid/widget/TextView;

    .line 393
    iget-object v1, p0, Lcom/squareup/address/AddressLayout;->street:Landroid/widget/EditText;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Views;->isSet([Landroid/widget/TextView;)Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onAttachedToWindow$0$AddressLayout()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->runner:Lcom/squareup/address/AddressLayoutRunner;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayoutRunner;->viewData()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/address/-$$Lambda$AddressLayout$u8-15fLcASQLXYKpPDSc8X9zrjo;

    invoke-direct {v1, p0}, Lcom/squareup/address/-$$Lambda$AddressLayout$u8-15fLcASQLXYKpPDSc8X9zrjo;-><init>(Lcom/squareup/address/AddressLayout;)V

    .line 129
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onAttachedToWindow$1$AddressLayout()Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 132
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->runner:Lcom/squareup/address/AddressLayoutRunner;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayoutRunner;->onStateChanged()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/address/-$$Lambda$qe57OZeAwVM4sNQ4ZfADKSMPEyg;

    invoke-direct {v2, v1}, Lcom/squareup/address/-$$Lambda$qe57OZeAwVM4sNQ4ZfADKSMPEyg;-><init>(Lcom/squareup/widgets/SelectableEditText;)V

    .line 133
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onAttachedToWindow$2$AddressLayout()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->runner:Lcom/squareup/address/AddressLayoutRunner;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayoutRunner;->onPostalCodeChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/address/-$$Lambda$AddressLayout$ARyRA7b-aA-C3BVnQ6xnZxBsIg8;

    invoke-direct {v1, p0}, Lcom/squareup/address/-$$Lambda$AddressLayout$ARyRA7b-aA-C3BVnQ6xnZxBsIg8;-><init>(Lcom/squareup/address/AddressLayout;)V

    .line 137
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$setPostalCanadaKeyboard$4$AddressLayout(Landroid/view/View;)V
    .locals 1

    .line 288
    iget-object p1, p0, Lcom/squareup/address/AddressLayout;->runner:Lcom/squareup/address/AddressLayoutRunner;

    sget-object v0, Lcom/squareup/CountryCode;->CA:Lcom/squareup/CountryCode;

    invoke-virtual {p1, v0}, Lcom/squareup/address/AddressLayoutRunner;->pickState(Lcom/squareup/CountryCode;)V

    return-void
.end method

.method public synthetic lambda$setPostalUsKeyboard$3$AddressLayout(Landroid/view/View;)V
    .locals 1

    .line 274
    iget-object p1, p0, Lcom/squareup/address/AddressLayout;->runner:Lcom/squareup/address/AddressLayoutRunner;

    sget-object v0, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    invoke-virtual {p1, v0}, Lcom/squareup/address/AddressLayoutRunner;->pickState(Lcom/squareup/CountryCode;)V

    return-void
.end method

.method protected abstract layoutResId()I
.end method

.method public onAttachedToWindow()V
    .locals 2

    .line 124
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 126
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->runner:Lcom/squareup/address/AddressLayoutRunner;

    invoke-virtual {p0}, Lcom/squareup/address/AddressLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/address/AddressLayoutRunner;->initFor(Lflow/path/Path;)V

    .line 127
    new-instance v0, Lcom/squareup/address/-$$Lambda$AddressLayout$TcdK9lT56uLVuz993fM6ahKeUeU;

    invoke-direct {v0, p0}, Lcom/squareup/address/-$$Lambda$AddressLayout$TcdK9lT56uLVuz993fM6ahKeUeU;-><init>(Lcom/squareup/address/AddressLayout;)V

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 131
    new-instance v0, Lcom/squareup/address/-$$Lambda$AddressLayout$CAWuIBDjPySKG0w7zdx8PLhkmXQ;

    invoke-direct {v0, p0}, Lcom/squareup/address/-$$Lambda$AddressLayout$CAWuIBDjPySKG0w7zdx8PLhkmXQ;-><init>(Lcom/squareup/address/AddressLayout;)V

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 135
    new-instance v0, Lcom/squareup/address/-$$Lambda$AddressLayout$1gasmwGho6YE0lFbHwqkNlPU-cA;

    invoke-direct {v0, p0}, Lcom/squareup/address/-$$Lambda$AddressLayout$1gasmwGho6YE0lFbHwqkNlPU-cA;-><init>(Lcom/squareup/address/AddressLayout;)V

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->isSet(Landroid/widget/TextView;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->isSet(Landroid/widget/TextView;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 141
    invoke-virtual {p0, v0}, Lcom/squareup/address/AddressLayout;->showManualCityEntry(Z)V

    :cond_1
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 146
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object p1

    check-cast p1, Lcom/squareup/address/CityAdapter;

    const-wide/16 v0, -0x1

    cmp-long p2, p4, v0

    if-nez p2, :cond_0

    .line 151
    invoke-virtual {p0}, Lcom/squareup/address/AddressLayout;->showManualCityEntry()V

    .line 152
    invoke-virtual {p0}, Lcom/squareup/address/AddressLayout;->selectCity()V

    return-void

    .line 156
    :cond_0
    invoke-virtual {p1, p3}, Lcom/squareup/address/CityAdapter;->getPostalLocation(I)Lcom/squareup/server/address/PostalLocation;

    move-result-object p1

    .line 157
    iget-object p2, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    iget-object p3, p1, Lcom/squareup/server/address/PostalLocation;->city:Ljava/lang/String;

    invoke-virtual {p2, p3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object p2, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    iget-object p1, p1, Lcom/squareup/server/address/PostalLocation;->state_abbreviation:Ljava/lang/String;

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;)V"
        }
    .end annotation

    .line 162
    iget-object p1, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object p1, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method selectCity()V
    .locals 2

    .line 366
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 368
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    return-void
.end method

.method public setAddress(Lcom/squareup/address/Address;)V
    .locals 1

    const/4 v0, 0x1

    .line 311
    invoke-virtual {p0, p1, v0}, Lcom/squareup/address/AddressLayout;->setAddress(Lcom/squareup/address/Address;Z)V

    return-void
.end method

.method public setAddress(Lcom/squareup/address/Address;Z)V
    .locals 2

    if-nez p1, :cond_0

    .line 316
    invoke-direct {p0}, Lcom/squareup/address/AddressLayout;->clearAddress()V

    return-void

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->street:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/squareup/address/Address;->street:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 321
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/squareup/address/Address;->city:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 322
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->apartment:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/squareup/address/Address;->apartment:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 323
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    iget-object v1, p1, Lcom/squareup/address/Address;->state:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 325
    iget-object v0, p1, Lcom/squareup/address/Address;->city:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/squareup/address/Address;->state:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 329
    :cond_1
    iget-object p2, p1, Lcom/squareup/address/Address;->postalCode:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcom/squareup/address/AddressLayout;->setPostal(Ljava/lang/String;Z)V

    goto :goto_1

    .line 326
    :cond_2
    :goto_0
    iget-object v0, p1, Lcom/squareup/address/Address;->postalCode:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/address/AddressLayout;->setPostal(Ljava/lang/String;Z)V

    .line 327
    invoke-virtual {p0, p2}, Lcom/squareup/address/AddressLayout;->showManualCityEntry(Z)V

    .line 332
    :goto_1
    iget-object p1, p1, Lcom/squareup/address/Address;->country:Lcom/squareup/CountryCode;

    invoke-virtual {p0, p1}, Lcom/squareup/address/AddressLayout;->setCountry(Lcom/squareup/CountryCode;)V

    return-void
.end method

.method public setCountry(Lcom/squareup/CountryCode;)V
    .locals 1

    .line 340
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->countryCode:Lcom/squareup/CountryCode;

    if-eq v0, p1, :cond_0

    .line 341
    iput-object p1, p0, Lcom/squareup/address/AddressLayout;->countryCode:Lcom/squareup/CountryCode;

    .line 343
    invoke-direct {p0}, Lcom/squareup/address/AddressLayout;->notifyCountryCodeWasUpdated()V

    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 167
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 170
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->street:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 171
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->apartment:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 172
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 173
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    .line 174
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->postal:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    .line 175
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->cityPicker:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    return-void
.end method

.method public showCityPicker()V
    .locals 2

    .line 348
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->cityStateRow:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 349
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->cityPicker:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 350
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 351
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->cityPicker:Landroid/widget/Spinner;

    invoke-static {v0}, Lcom/squareup/util/Views;->scrollToVisible(Landroid/view/View;)V

    return-void
.end method

.method public showEmptyManualEntry()V
    .locals 2

    .line 356
    invoke-virtual {p0}, Lcom/squareup/address/AddressLayout;->showManualCityEntry()V

    .line 357
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 358
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->state:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showManualCityEntry()V
    .locals 1

    const/4 v0, 0x1

    .line 362
    invoke-virtual {p0, v0}, Lcom/squareup/address/AddressLayout;->showManualCityEntry(Z)V

    return-void
.end method

.method public showManualCityEntry(Z)V
    .locals 3

    .line 372
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->cityStateRow:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 373
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->cityPicker:Landroid/widget/Spinner;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 374
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->city:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 375
    iget-object v0, p0, Lcom/squareup/address/AddressLayout;->apartment:Landroid/widget/EditText;

    sget v1, Lcom/squareup/address/R$id;->address_city:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    if-eqz p1, :cond_0

    .line 377
    iget-object p1, p0, Lcom/squareup/address/AddressLayout;->cityStateRow:Landroid/widget/LinearLayout;

    invoke-static {p1}, Lcom/squareup/util/Views;->scrollToVisible(Landroid/view/View;)V

    goto :goto_0

    .line 379
    :cond_0
    iget-object p1, p0, Lcom/squareup/address/AddressLayout;->cityStateRow:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->clearFocus()V

    :goto_0
    return-void
.end method
