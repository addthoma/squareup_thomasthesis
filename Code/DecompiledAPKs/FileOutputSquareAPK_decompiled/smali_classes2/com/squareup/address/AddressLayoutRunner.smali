.class public Lcom/squareup/address/AddressLayoutRunner;
.super Ljava/lang/Object;
.source "AddressLayoutRunner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/address/AddressLayoutRunner$AddressViewData;,
        Lcom/squareup/address/AddressLayoutRunner$SharedScope;
    }
.end annotation


# static fields
.field private static final EMPTY_POSTAL_RESPONSE:Lcom/squareup/server/address/PostalCodeResponse;

.field private static final NO_POSTAL_CODE:Ljava/lang/String; = ""


# instance fields
.field private final addressService:Lcom/squareup/server/address/AddressService;

.field private final countryCode:Lcom/squareup/CountryCode;

.field private final flow:Lflow/Flow;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private parentKey:Lflow/path/Path;

.field private postalCode:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private postalCodeResponse:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/server/address/PostalCodeResponse;",
            ">;"
        }
    .end annotation
.end field

.field private previousPostcode:Ljava/lang/String;

.field private state:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 36
    new-instance v0, Lcom/squareup/server/address/PostalCodeResponse;

    .line 37
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/server/address/PostalCodeResponse;-><init>(Ljava/util/List;)V

    sput-object v0, Lcom/squareup/address/AddressLayoutRunner;->EMPTY_POSTAL_RESPONSE:Lcom/squareup/server/address/PostalCodeResponse;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/address/AddressService;Lio/reactivex/Scheduler;Lcom/squareup/CountryCode;Lflow/Flow;)V
    .locals 0
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/address/AddressLayoutRunner;->addressService:Lcom/squareup/server/address/AddressService;

    .line 55
    iput-object p2, p0, Lcom/squareup/address/AddressLayoutRunner;->mainScheduler:Lio/reactivex/Scheduler;

    .line 56
    iput-object p3, p0, Lcom/squareup/address/AddressLayoutRunner;->countryCode:Lcom/squareup/CountryCode;

    .line 57
    iput-object p4, p0, Lcom/squareup/address/AddressLayoutRunner;->flow:Lflow/Flow;

    .line 59
    invoke-direct {p0}, Lcom/squareup/address/AddressLayoutRunner;->reset()V

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/server/address/PostalCodeResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 131
    instance-of v0, p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 132
    check-cast p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/address/PostalCodeResponse;

    return-object p0

    .line 134
    :cond_0
    sget-object p0, Lcom/squareup/address/AddressLayoutRunner;->EMPTY_POSTAL_RESPONSE:Lcom/squareup/server/address/PostalCodeResponse;

    return-object p0
.end method

.method private reset()V
    .locals 2

    const/4 v0, 0x0

    .line 117
    iput-object v0, p0, Lcom/squareup/address/AddressLayoutRunner;->previousPostcode:Ljava/lang/String;

    .line 118
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/address/AddressLayoutRunner;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 119
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/address/AddressLayoutRunner;->postalCode:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 121
    iget-object v0, p0, Lcom/squareup/address/AddressLayoutRunner;->postalCode:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/address/-$$Lambda$AddressLayoutRunner$1k9CrbZcXn5US4ycdnKALMleqII;

    invoke-direct {v1, p0}, Lcom/squareup/address/-$$Lambda$AddressLayoutRunner$1k9CrbZcXn5US4ycdnKALMleqII;-><init>(Lcom/squareup/address/AddressLayoutRunner;)V

    .line 122
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/address/AddressLayoutRunner;->postalCodeResponse:Lio/reactivex/Observable;

    return-void
.end method


# virtual methods
.method public initFor(Lflow/path/Path;)V
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/address/AddressLayoutRunner;->parentKey:Lflow/path/Path;

    if-eq v0, p1, :cond_0

    .line 111
    iput-object p1, p0, Lcom/squareup/address/AddressLayoutRunner;->parentKey:Lflow/path/Path;

    .line 112
    invoke-direct {p0}, Lcom/squareup/address/AddressLayoutRunner;->reset()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$reset$1$AddressLayoutRunner(Ljava/lang/String;)Lio/reactivex/SingleSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, ""

    .line 123
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    sget-object p1, Lcom/squareup/address/AddressLayoutRunner;->EMPTY_POSTAL_RESPONSE:Lcom/squareup/server/address/PostalCodeResponse;

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/address/AddressLayoutRunner;->mainScheduler:Lio/reactivex/Scheduler;

    .line 125
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/squareup/address/AddressLayoutRunner;->addressService:Lcom/squareup/server/address/AddressService;

    invoke-interface {v0, p1}, Lcom/squareup/server/address/AddressService;->lookUp(Ljava/lang/String;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object p1

    .line 129
    invoke-virtual {p1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    sget-object v0, Lcom/squareup/address/-$$Lambda$AddressLayoutRunner$du422Q0UwogyaJWVhf6KNHQXzUQ;->INSTANCE:Lcom/squareup/address/-$$Lambda$AddressLayoutRunner$du422Q0UwogyaJWVhf6KNHQXzUQ;

    .line 130
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public onPickState(Ljava/lang/String;)V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/address/AddressLayoutRunner;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onPostalCodeChanged()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/server/address/PostalCodeResponse;",
            ">;"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/squareup/address/AddressLayoutRunner;->postalCodeResponse:Lio/reactivex/Observable;

    return-object v0
.end method

.method public onPostalCodeEntered(Ljava/lang/String;)V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/address/AddressLayoutRunner;->previousPostcode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 92
    :cond_0
    iput-object p1, p0, Lcom/squareup/address/AddressLayoutRunner;->previousPostcode:Ljava/lang/String;

    .line 96
    invoke-static {p1}, Lcom/squareup/text/PostalCodes;->isUsZipCode(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/squareup/address/AddressLayoutRunner;->postalCode:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 99
    :cond_1
    iget-object p1, p0, Lcom/squareup/address/AddressLayoutRunner;->postalCode:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public onStateChanged()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/squareup/address/AddressLayoutRunner;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public pickState(Lcom/squareup/CountryCode;)V
    .locals 2

    .line 79
    iget-object v0, p0, Lcom/squareup/address/AddressLayoutRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/address/StatePickerScreen;

    invoke-direct {v1, p1}, Lcom/squareup/address/StatePickerScreen;-><init>(Lcom/squareup/CountryCode;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method viewData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/address/AddressLayoutRunner$AddressViewData;",
            ">;"
        }
    .end annotation

    .line 63
    new-instance v0, Lcom/squareup/address/AddressLayoutRunner$AddressViewData;

    iget-object v1, p0, Lcom/squareup/address/AddressLayoutRunner;->countryCode:Lcom/squareup/CountryCode;

    invoke-direct {v0, v1}, Lcom/squareup/address/AddressLayoutRunner$AddressViewData;-><init>(Lcom/squareup/CountryCode;)V

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
