.class public Lcom/squareup/address/PoBoxChecker;
.super Ljava/lang/Object;
.source "PoBoxChecker.java"


# static fields
.field private static final ALLOWED_BOX_PATTERN_SPACES:Ljava/util/regex/Pattern;

.field private static final PO_BOX:Ljava/util/regex/Pattern;

.field private static final PO_BOX_WITH_SPACES:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x2

    const-string v1, "\\bbox(?:\\b$|(((num)|(number))?[0-9]))|((\\bp)([o0]|ost(al)?|office|b(ox)?)(b((.?x)|([o0].?))?|(num|number))?[0-9])|((p(.?[o0])|(post(al)?)(office)?)b([o0])x)"

    .line 9
    invoke-static {v1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v1

    sput-object v1, Lcom/squareup/address/PoBoxChecker;->PO_BOX:Ljava/util/regex/Pattern;

    const-string v1, "([^0-9]+\\s+)?box\\s*[0-9]+$"

    .line 15
    invoke-static {v1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v1

    sput-object v1, Lcom/squareup/address/PoBoxChecker;->PO_BOX_WITH_SPACES:Ljava/util/regex/Pattern;

    const-string v1, "[a-z]+\\s+[0-9]+\\s+box\\s+[0-9]+"

    .line 19
    invoke-static {v1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/squareup/address/PoBoxChecker;->ALLOWED_BOX_PATTERN_SPACES:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isPoBox(Lcom/squareup/address/Address;)Z
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/address/Address;->street:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/address/PoBoxChecker;->isPoBox(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/address/Address;->apartment:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/address/PoBoxChecker;->isPoBox(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object p0, p0, Lcom/squareup/address/Address;->city:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/address/PoBoxChecker;->isPoBox(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static isPoBox(Ljava/lang/String;)Z
    .locals 3

    .line 30
    sget-object v0, Lcom/squareup/address/PoBoxChecker;->PO_BOX:Ljava/util/regex/Pattern;

    const-string v1, "[^a-zA-Z0-9]"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-nez v0, :cond_1

    .line 31
    invoke-static {p0}, Lcom/squareup/address/PoBoxChecker;->isPoBoxPreservingSpaces(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static isPoBoxPreservingSpaces(Ljava/lang/String;)Z
    .locals 2

    const-string v0, "[^a-zA-Z0-9 ]"

    const-string v1, ""

    .line 45
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 47
    sget-object v0, Lcom/squareup/address/PoBoxChecker;->PO_BOX_WITH_SPACES:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/address/PoBoxChecker;->ALLOWED_BOX_PATTERN_SPACES:Ljava/util/regex/Pattern;

    .line 48
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/regex/Matcher;->find()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
