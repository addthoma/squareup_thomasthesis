.class public abstract Lcom/squareup/CommonLoggedInReleaseModule;
.super Ljava/lang/Object;
.source "CommonLoggedInReleaseModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/CommonLoggedInModule;,
        Lcom/squareup/api/multipassauth/MultiPassAuthServiceReleaseModule;,
        Lcom/squareup/onlinestore/api/OnlineStoreServiceReleaseModule;,
        Lcom/squareup/payment/PaymentModule$Prod;,
        Lcom/squareup/print/RegisterPrintModule$Prod;,
        Lcom/squareup/server/tenders/ReleaseCaptureTendersModule;,
        Lcom/squareup/server/tenders/ReleaseRemoveTenderServiceModule;,
        Lcom/squareup/customreport/data/SalesReportDataReleaseModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\'\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/CommonLoggedInReleaseModule;",
        "",
        "()V",
        "provideSingleImagePicassoDownloaderFactory",
        "Lcom/squareup/merchantimages/SingleImagePicassoFactory$DownloaderFactory;",
        "factory",
        "Lcom/squareup/merchantimages/SingleImagePicassoFactory$OkHttp3DownloaderFactory;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract provideSingleImagePicassoDownloaderFactory(Lcom/squareup/merchantimages/SingleImagePicassoFactory$OkHttp3DownloaderFactory;)Lcom/squareup/merchantimages/SingleImagePicassoFactory$DownloaderFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
