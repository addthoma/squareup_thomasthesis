.class public Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;
.super Ljava/lang/Object;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FirmwareEventLog"
.end annotation


# instance fields
.field public final event:I

.field public final message:Ljava/lang/String;

.field public final source:I

.field public final timestamp:J


# direct methods
.method public constructor <init>(JIILjava/lang/String;)V
    .locals 0

    .line 281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282
    iput-wide p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;->timestamp:J

    .line 283
    iput p3, p0, Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;->event:I

    .line 284
    iput p4, p0, Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;->source:I

    .line 285
    iput-object p5, p0, Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;->message:Ljava/lang/String;

    return-void
.end method
