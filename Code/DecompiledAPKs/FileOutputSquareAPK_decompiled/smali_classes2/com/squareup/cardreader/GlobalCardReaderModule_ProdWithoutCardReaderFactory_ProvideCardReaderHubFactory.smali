.class public final Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideCardReaderHubFactory;
.super Ljava/lang/Object;
.source "GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideCardReaderHubFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReaderHub;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideCardReaderHubFactory;->module:Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;)Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideCardReaderHubFactory;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideCardReaderHubFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideCardReaderHubFactory;-><init>(Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;)V

    return-object v0
.end method

.method public static provideCardReaderHub(Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;)Lcom/squareup/cardreader/CardReaderHub;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;->provideCardReaderHub()Lcom/squareup/cardreader/CardReaderHub;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReaderHub;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReaderHub;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideCardReaderHubFactory;->module:Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;

    invoke-static {v0}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideCardReaderHubFactory;->provideCardReaderHub(Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;)Lcom/squareup/cardreader/CardReaderHub;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideCardReaderHubFactory;->get()Lcom/squareup/cardreader/CardReaderHub;

    move-result-object v0

    return-object v0
.end method
