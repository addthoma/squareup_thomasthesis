.class public final enum Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
.super Ljava/lang/Enum;
.source "CrSecureSessionResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/lcr/CrSecureSessionResult$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/lcr/CrSecureSessionResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_AES:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_ALREADY_INITIALIZED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_ALREADY_TERMINATED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_API_CALL:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_APPROVAL_EXPIRED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_APPROVAL_MISMATCH:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_ARG:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_BAD_DIGIT:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_BAD_FIELD:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_BAD_HMAC:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_CALL_UNEXPECTED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_CONTEXT:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_CURVE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_DENIED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_ENCODE_FAILURE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_GENERIC_ERROR:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_HKDF:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_INPUT_SIZE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_INVALID_KEY_UPDATE_MSG:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_INVALID_PARAMETER:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_INVALID_PIN_REQUEST:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_MAX_READERS_CONNECTED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_MINESWEEPER_CALL:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_MODULE_GENERIC_ERROR:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_MSG_TYPE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_NOT_INITIALIZED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_NOT_TERMINATED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_NO_READER:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_NO_TXN_LEFT:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_OUTPUT_SIZE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_OUT_OF_CONTEXTS:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_PIN_FULL:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_PIN_TOO_SHORT:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_PROTOCOL_VERSION:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_SERVER_DENY_ERROR:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_SESSION_ERROR:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_SESSION_ID:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_SESSION_STATE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_SHA256:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_TDES:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_UNKNOWN:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

.field public static final enum CR_SECURESESSION_FEATURE_RESULT_WHITEBOX_KEY_DESERIALIZE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/4 v1, 0x0

    const-string v2, "CR_SECURESESSION_FEATURE_RESULT_SUCCESS"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/4 v2, 0x1

    const-string v3, "CR_SECURESESSION_FEATURE_RESULT_INVALID_PARAMETER"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_INVALID_PARAMETER:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/4 v3, 0x2

    const-string v4, "CR_SECURESESSION_FEATURE_RESULT_NOT_INITIALIZED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_NOT_INITIALIZED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/4 v4, 0x3

    const-string v5, "CR_SECURESESSION_FEATURE_RESULT_ALREADY_INITIALIZED"

    invoke-direct {v0, v5, v4}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_ALREADY_INITIALIZED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/4 v5, 0x4

    const-string v6, "CR_SECURESESSION_FEATURE_RESULT_NOT_TERMINATED"

    invoke-direct {v0, v6, v5}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_NOT_TERMINATED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/4 v6, 0x5

    const-string v7, "CR_SECURESESSION_FEATURE_RESULT_ALREADY_TERMINATED"

    invoke-direct {v0, v7, v6}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_ALREADY_TERMINATED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/4 v7, 0x6

    const-string v8, "CR_SECURESESSION_FEATURE_RESULT_SESSION_ERROR"

    invoke-direct {v0, v8, v7}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_SESSION_ERROR:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/4 v8, 0x7

    const-string v9, "CR_SECURESESSION_FEATURE_RESULT_CALL_UNEXPECTED"

    invoke-direct {v0, v9, v8}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_CALL_UNEXPECTED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 20
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v9, 0x8

    const-string v10, "CR_SECURESESSION_FEATURE_RESULT_GENERIC_ERROR"

    invoke-direct {v0, v10, v9}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_GENERIC_ERROR:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 21
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v10, 0x9

    const-string v11, "CR_SECURESESSION_FEATURE_RESULT_NO_READER"

    invoke-direct {v0, v11, v10}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_NO_READER:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 22
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v11, 0xa

    const-string v12, "CR_SECURESESSION_FEATURE_RESULT_SERVER_DENY_ERROR"

    invoke-direct {v0, v12, v11}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_SERVER_DENY_ERROR:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 23
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v12, 0xb

    const-string v13, "CR_SECURESESSION_FEATURE_RESULT_MODULE_GENERIC_ERROR"

    invoke-direct {v0, v13, v12}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_MODULE_GENERIC_ERROR:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 24
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v13, 0xc

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_MAX_READERS_CONNECTED"

    invoke-direct {v0, v14, v13}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_MAX_READERS_CONNECTED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 25
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v14, 0xd

    const-string v15, "CR_SECURESESSION_FEATURE_RESULT_ARG"

    invoke-direct {v0, v15, v14}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_ARG:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 26
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v15, 0xe

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_SESSION_STATE"

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_SESSION_STATE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 27
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_INPUT_SIZE"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_INPUT_SIZE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 28
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_OUTPUT_SIZE"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_OUTPUT_SIZE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 29
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_MSG_TYPE"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_MSG_TYPE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 30
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_SESSION_ID"

    const/16 v15, 0x12

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_SESSION_ID:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 31
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_CURVE"

    const/16 v15, 0x13

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_CURVE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 32
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_HKDF"

    const/16 v15, 0x14

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_HKDF:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 33
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_DENIED"

    const/16 v15, 0x15

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_DENIED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 34
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_BAD_DIGIT"

    const/16 v15, 0x16

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_BAD_DIGIT:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 35
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_PIN_FULL"

    const/16 v15, 0x17

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_PIN_FULL:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 36
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_PIN_TOO_SHORT"

    const/16 v15, 0x18

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_PIN_TOO_SHORT:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 37
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_INVALID_PIN_REQUEST"

    const/16 v15, 0x19

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_INVALID_PIN_REQUEST:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 38
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_INVALID_KEY_UPDATE_MSG"

    const/16 v15, 0x1a

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_INVALID_KEY_UPDATE_MSG:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 39
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_AES"

    const/16 v15, 0x1b

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_AES:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 40
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_PROTOCOL_VERSION"

    const/16 v15, 0x1c

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_PROTOCOL_VERSION:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 41
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_APPROVAL_MISMATCH"

    const/16 v15, 0x1d

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_APPROVAL_MISMATCH:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 42
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_APPROVAL_EXPIRED"

    const/16 v15, 0x1e

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_APPROVAL_EXPIRED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 43
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_NO_TXN_LEFT"

    const/16 v15, 0x1f

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_NO_TXN_LEFT:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 44
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_API_CALL"

    const/16 v15, 0x20

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_API_CALL:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 45
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_MINESWEEPER_CALL"

    const/16 v15, 0x21

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_MINESWEEPER_CALL:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 46
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_SHA256"

    const/16 v15, 0x22

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_SHA256:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 47
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_BAD_HMAC"

    const/16 v15, 0x23

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_BAD_HMAC:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 48
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_TDES"

    const/16 v15, 0x24

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_TDES:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 49
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_ENCODE_FAILURE"

    const/16 v15, 0x25

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_ENCODE_FAILURE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 50
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_CONTEXT"

    const/16 v15, 0x26

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_CONTEXT:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 51
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_OUT_OF_CONTEXTS"

    const/16 v15, 0x27

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_OUT_OF_CONTEXTS:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 52
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_BAD_FIELD"

    const/16 v15, 0x28

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_BAD_FIELD:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 53
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_WHITEBOX_KEY_DESERIALIZE"

    const/16 v15, 0x29

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_WHITEBOX_KEY_DESERIALIZE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 54
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const-string v14, "CR_SECURESESSION_FEATURE_RESULT_UNKNOWN"

    const/16 v15, 0x2a

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_UNKNOWN:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v0, 0x2b

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 11
    sget-object v14, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    aput-object v14, v0, v1

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_INVALID_PARAMETER:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_NOT_INITIALIZED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_ALREADY_INITIALIZED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_NOT_TERMINATED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_ALREADY_TERMINATED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_SESSION_ERROR:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_CALL_UNEXPECTED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_GENERIC_ERROR:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_NO_READER:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_SERVER_DENY_ERROR:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_MODULE_GENERIC_ERROR:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_MAX_READERS_CONNECTED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_ARG:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_SESSION_STATE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_INPUT_SIZE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_OUTPUT_SIZE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_MSG_TYPE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_SESSION_ID:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_CURVE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_HKDF:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_DENIED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_BAD_DIGIT:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_PIN_FULL:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_PIN_TOO_SHORT:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_INVALID_PIN_REQUEST:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_INVALID_KEY_UPDATE_MSG:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_AES:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_PROTOCOL_VERSION:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_APPROVAL_MISMATCH:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_APPROVAL_EXPIRED:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_NO_TXN_LEFT:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_API_CALL:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_MINESWEEPER_CALL:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_SHA256:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_BAD_HMAC:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_TDES:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_ENCODE_FAILURE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_CONTEXT:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_OUT_OF_CONTEXTS:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_BAD_FIELD:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_WHITEBOX_KEY_DESERIALIZE:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_UNKNOWN:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->$VALUES:[Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 72
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult$SwigNext;->access$008()I

    move-result p1

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 77
    iput p3, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigValue:I

    add-int/lit8 p3, p3, 0x1

    .line 78
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult$SwigNext;->access$002(I)I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/cardreader/lcr/CrSecureSessionResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrSecureSessionResult;",
            ")V"
        }
    .end annotation

    .line 82
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 83
    iget p1, p3, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigValue:I

    .line 84
    iget p1, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult$SwigNext;->access$002(I)I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 6

    .line 61
    const-class v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 62
    array-length v2, v1

    if-ge p0, v2, :cond_0

    if-ltz p0, :cond_0

    aget-object v2, v1, p0

    iget v2, v2, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigValue:I

    if-ne v2, p0, :cond_0

    .line 63
    aget-object p0, v1, p0

    return-object p0

    .line 64
    :cond_0
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 65
    iget v5, v4, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigValue:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 67
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No enum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " with value "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->$VALUES:[Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 57
    iget v0, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigValue:I

    return v0
.end method
