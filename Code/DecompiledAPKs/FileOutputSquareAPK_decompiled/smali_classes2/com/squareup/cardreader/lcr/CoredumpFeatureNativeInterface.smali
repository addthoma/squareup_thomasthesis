.class public interface abstract Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;
.super Ljava/lang/Object;
.source "CoredumpFeatureNativeInterface.java"


# virtual methods
.method public abstract coredump_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;
.end method

.method public abstract coredump_trigger_dump(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)V
.end method

.method public abstract cr_coredump_erase(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;
.end method

.method public abstract cr_coredump_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;
.end method

.method public abstract cr_coredump_get_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;
.end method

.method public abstract cr_coredump_get_info(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;
.end method

.method public abstract cr_coredump_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;
.end method
