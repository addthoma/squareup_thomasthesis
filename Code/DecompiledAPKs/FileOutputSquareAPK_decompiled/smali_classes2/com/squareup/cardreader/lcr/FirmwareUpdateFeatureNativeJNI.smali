.class public Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeJNI;
.super Ljava/lang/Object;
.source "FirmwareUpdateFeatureNativeJNI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native cr_firmware_update_feature_free(J)I
.end method

.method public static final native cr_firmware_update_feature_get_manifest(J)I
.end method

.method public static final native cr_firmware_update_feature_stop_sending_data(J)I
.end method

.method public static final native cr_firmware_update_feature_term(J)I
.end method

.method public static final native firmware_send_data(J[B[B[B)I
.end method

.method public static final native firmware_update_initialize(JLjava/lang/Object;)J
.end method
