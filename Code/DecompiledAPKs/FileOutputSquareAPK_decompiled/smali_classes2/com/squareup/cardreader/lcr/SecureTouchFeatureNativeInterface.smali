.class public interface abstract Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;
.super Ljava/lang/Object;
.source "SecureTouchFeatureNativeInterface.java"


# virtual methods
.method public abstract cr_secure_touch_mode_feature_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;
.end method

.method public abstract cr_secure_touch_mode_feature_disable_squid_touch_driver_result(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;)V
.end method

.method public abstract cr_secure_touch_mode_feature_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;
.end method

.method public abstract cr_secure_touch_mode_feature_init(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Lcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;
.end method

.method public abstract cr_secure_touch_mode_feature_regular_set_button_location(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)V
.end method

.method public abstract cr_secure_touch_mode_feature_sent_pinpad_configs(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmPinPadConfigType;)V
.end method

.method public abstract cr_secure_touch_mode_feature_set_accessibility_configs(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)V
.end method

.method public abstract cr_secure_touch_mode_feature_start_secure_touch(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)V
.end method

.method public abstract cr_secure_touch_mode_feature_stop_secure_touch(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)V
.end method

.method public abstract cr_secure_touch_mode_feature_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;
.end method

.method public abstract cr_secure_touch_mode_pin_pad_is_hidden(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmHidePinPadResult;)V
.end method

.method public abstract secure_touch_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;
.end method
