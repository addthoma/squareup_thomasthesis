.class public Lcom/squareup/cardreader/lcr/CardreaderNativePassthrough;
.super Ljava/lang/Object;
.source "CardreaderNativePassthrough.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cardreader_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;
    .locals 0

    .line 33
    invoke-static {p1, p2, p3}, Lcom/squareup/cardreader/lcr/CardreaderNative;->cardreader_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object p1

    return-object p1
.end method

.method public cardreader_initialize_rpc(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;
    .locals 0

    .line 39
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/CardreaderNative;->cardreader_initialize_rpc(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object p1

    return-object p1
.end method

.method public cr_cardreader_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;
    .locals 0

    .line 14
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CardreaderNative;->cr_cardreader_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_cardreader_notify_reader_plugged(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;
    .locals 0

    .line 20
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CardreaderNative;->cr_cardreader_notify_reader_plugged(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_cardreader_notify_reader_unplugged(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;
    .locals 0

    .line 26
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CardreaderNative;->cr_cardreader_notify_reader_unplugged(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_cardreader_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;
    .locals 0

    .line 9
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CardreaderNative;->cr_cardreader_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    move-result-object p1

    return-object p1
.end method

.method public process_rpc_callback()V
    .locals 0

    .line 44
    invoke-static {}, Lcom/squareup/cardreader/lcr/CardreaderNative;->process_rpc_callback()V

    return-void
.end method
