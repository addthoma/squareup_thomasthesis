.class public interface abstract Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;
.super Ljava/lang/Object;
.source "PaymentFeatureNativeInterface.java"


# virtual methods
.method public abstract cr_payment_cancel_payment(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
.end method

.method public abstract cr_payment_enable_swipe_passthrough(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Z)Lcom/squareup/cardreader/lcr/CrPaymentResult;
.end method

.method public abstract cr_payment_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
.end method

.method public abstract cr_payment_request_card_presence(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
.end method

.method public abstract cr_payment_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
.end method

.method public abstract payment_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;II)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;
.end method

.method public abstract payment_process_server_response(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;
.end method

.method public abstract payment_select_account_type(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
.end method

.method public abstract payment_select_application(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;
.end method

.method public abstract payment_send_powerup_hint(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;I)Lcom/squareup/cardreader/lcr/CrPaymentResult;
.end method

.method public abstract payment_start_payment(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;JIIIIIIII)Lcom/squareup/cardreader/lcr/CrPaymentResult;
.end method

.method public abstract payment_tmn_cancel_request(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
.end method

.method public abstract payment_tmn_send_bytes_to_reader(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;
.end method

.method public abstract payment_tmn_start_miryo(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;
.end method

.method public abstract payment_tmn_start_transaction(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;J)Lcom/squareup/cardreader/lcr/CrPaymentResult;
.end method

.method public abstract payment_tmn_write_notify_ack(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
.end method
