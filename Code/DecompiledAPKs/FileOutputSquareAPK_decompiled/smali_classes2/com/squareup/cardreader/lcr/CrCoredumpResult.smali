.class public final Lcom/squareup/cardreader/lcr/CrCoredumpResult;
.super Ljava/lang/Object;
.source "CrCoredumpResult.java"


# static fields
.field public static final CR_COREDUMP_RESULT_ALREADY_TERMINATED:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

.field public static final CR_COREDUMP_RESULT_CALL_UNEXPECTED:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

.field public static final CR_COREDUMP_RESULT_ERROR0:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

.field public static final CR_COREDUMP_RESULT_INVALID_PARAMETER:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

.field public static final CR_COREDUMP_RESULT_NOT_INITIALIZED:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

.field public static final CR_COREDUMP_RESULT_NOT_TERMINATED:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

.field public static final CR_COREDUMP_RESULT_SESSION_ERROR:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

.field public static final CR_COREDUMP_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

.field private static swigNext:I

.field private static swigValues:[Lcom/squareup/cardreader/lcr/CrCoredumpResult;


# instance fields
.field private final swigName:Ljava/lang/String;

.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    invoke-static {}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeJNI;->CR_COREDUMP_RESULT_SUCCESS_get()I

    move-result v1

    const-string v2, "CR_COREDUMP_RESULT_SUCCESS"

    invoke-direct {v0, v2, v1}, Lcom/squareup/cardreader/lcr/CrCoredumpResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->CR_COREDUMP_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    const-string v1, "CR_COREDUMP_RESULT_ERROR0"

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/lcr/CrCoredumpResult;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->CR_COREDUMP_RESULT_ERROR0:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    const-string v1, "CR_COREDUMP_RESULT_INVALID_PARAMETER"

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/lcr/CrCoredumpResult;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->CR_COREDUMP_RESULT_INVALID_PARAMETER:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    const-string v1, "CR_COREDUMP_RESULT_SESSION_ERROR"

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/lcr/CrCoredumpResult;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->CR_COREDUMP_RESULT_SESSION_ERROR:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    const-string v1, "CR_COREDUMP_RESULT_NOT_INITIALIZED"

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/lcr/CrCoredumpResult;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->CR_COREDUMP_RESULT_NOT_INITIALIZED:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    const-string v1, "CR_COREDUMP_RESULT_ALREADY_TERMINATED"

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/lcr/CrCoredumpResult;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->CR_COREDUMP_RESULT_ALREADY_TERMINATED:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    const-string v1, "CR_COREDUMP_RESULT_NOT_TERMINATED"

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/lcr/CrCoredumpResult;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->CR_COREDUMP_RESULT_NOT_TERMINATED:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    const-string v1, "CR_COREDUMP_RESULT_CALL_UNEXPECTED"

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/lcr/CrCoredumpResult;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->CR_COREDUMP_RESULT_CALL_UNEXPECTED:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    .line 55
    sget-object v1, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->CR_COREDUMP_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->CR_COREDUMP_RESULT_ERROR0:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->CR_COREDUMP_RESULT_INVALID_PARAMETER:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    const/4 v3, 0x2

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->CR_COREDUMP_RESULT_SESSION_ERROR:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    const/4 v3, 0x3

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->CR_COREDUMP_RESULT_NOT_INITIALIZED:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    const/4 v3, 0x4

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->CR_COREDUMP_RESULT_ALREADY_TERMINATED:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    const/4 v3, 0x5

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->CR_COREDUMP_RESULT_NOT_TERMINATED:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    const/4 v3, 0x6

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->CR_COREDUMP_RESULT_CALL_UNEXPECTED:Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    const/4 v3, 0x7

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigValues:[Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    .line 56
    sput v2, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigNext:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigName:Ljava/lang/String;

    .line 40
    sget p1, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigNext:I

    add-int/lit8 v0, p1, 0x1

    sput v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigNext:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigName:Ljava/lang/String;

    .line 45
    iput p2, p0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigValue:I

    add-int/lit8 p2, p2, 0x1

    .line 46
    sput p2, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigNext:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrCoredumpResult;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigName:Ljava/lang/String;

    .line 51
    iget p1, p2, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigValue:I

    .line 52
    iget p1, p0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    sput p1, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigNext:I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCoredumpResult;
    .locals 3

    .line 30
    sget-object v0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigValues:[Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    array-length v1, v0

    if-ge p0, v1, :cond_0

    if-ltz p0, :cond_0

    aget-object v1, v0, p0

    iget v1, v1, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigValue:I

    if-ne v1, p0, :cond_0

    .line 31
    aget-object p0, v0, p0

    return-object p0

    :cond_0
    const/4 v0, 0x0

    .line 32
    :goto_0
    sget-object v1, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigValues:[Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    array-length v2, v1

    if-ge v0, v2, :cond_2

    .line 33
    aget-object v2, v1, v0

    iget v2, v2, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigValue:I

    if-ne v2, p0, :cond_1

    .line 34
    aget-object p0, v1, v0

    return-object p0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No enum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class v2, Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " with value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 22
    iget v0, p0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigValue:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigName:Ljava/lang/String;

    return-object v0
.end method
