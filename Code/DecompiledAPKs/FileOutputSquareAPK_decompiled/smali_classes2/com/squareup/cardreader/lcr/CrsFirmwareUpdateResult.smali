.class public final enum Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;
.super Ljava/lang/Enum;
.source "CrsFirmwareUpdateResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_BAD_ARGUMENT:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_BAD_ENCRYPTION_KEY:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_BAD_HEADER:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_BAD_WRITE_ALIGNMENT:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_CRQ_UNRESPONSIVE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_DECRYPTION_FAILURE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_DUPLICATE_UPDATE_TO_SLOT:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_ENCRYPTED_UPDATE_REQUIRED:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_ERROR_UNKNOWN:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_FLASH_FAILURE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_INVALID_IMAGE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_INVALID_IMAGE_VERSION:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_NONE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_NOT_INITIALIZED:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_SEND_DATA:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_WILL_RETRY:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

.field public static final enum CRS_FWUP_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/4 v1, 0x0

    const-string v2, "CRS_FWUP_RESULT_SUCCESS"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/4 v2, 0x1

    const-string v3, "CRS_FWUP_RESULT_SEND_DATA"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_SEND_DATA:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/4 v3, 0x2

    const-string v4, "CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_HEADER"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/4 v4, 0x3

    const-string v5, "CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/4 v5, 0x4

    const-string v6, "CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_WILL_RETRY"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_WILL_RETRY:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/4 v6, 0x5

    const-string v7, "CRS_FWUP_RESULT_NOT_INITIALIZED"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_NOT_INITIALIZED:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/4 v7, 0x6

    const-string v8, "CRS_FWUP_RESULT_INVALID_IMAGE_HEADER"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/4 v8, 0x7

    const-string v9, "CRS_FWUP_RESULT_INVALID_IMAGE"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_INVALID_IMAGE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 20
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/16 v9, 0x8

    const-string v10, "CRS_FWUP_RESULT_DECRYPTION_FAILURE"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_DECRYPTION_FAILURE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 21
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/16 v10, 0x9

    const-string v11, "CRS_FWUP_RESULT_FLASH_FAILURE"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_FLASH_FAILURE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 22
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/16 v11, 0xa

    const-string v12, "CRS_FWUP_RESULT_BAD_ARGUMENT"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_BAD_ARGUMENT:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 23
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/16 v12, 0xb

    const-string v13, "CRS_FWUP_RESULT_BAD_HEADER"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_BAD_HEADER:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 24
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/16 v13, 0xc

    const-string v14, "CRS_FWUP_RESULT_BAD_WRITE_ALIGNMENT"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_BAD_WRITE_ALIGNMENT:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 25
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/16 v14, 0xd

    const-string v15, "CRS_FWUP_RESULT_BAD_ENCRYPTION_KEY"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_BAD_ENCRYPTION_KEY:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 26
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/16 v15, 0xe

    const-string v14, "CRS_FWUP_RESULT_DUPLICATE_UPDATE_TO_SLOT"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_DUPLICATE_UPDATE_TO_SLOT:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 27
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const-string v14, "CRS_FWUP_RESULT_ENCRYPTED_UPDATE_REQUIRED"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_ENCRYPTED_UPDATE_REQUIRED:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 28
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const-string v13, "CRS_FWUP_RESULT_ERROR_UNKNOWN"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_ERROR_UNKNOWN:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 29
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const-string v13, "CRS_FWUP_RESULT_INVALID_IMAGE_VERSION"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_INVALID_IMAGE_VERSION:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 30
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const-string v13, "CRS_FWUP_RESULT_CRQ_UNRESPONSIVE"

    const/16 v14, 0x12

    const/16 v15, 0x12

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_CRQ_UNRESPONSIVE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 31
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const-string v13, "CRS_FWUP_RESULT_NONE"

    const/16 v14, 0x13

    const/16 v15, 0xff

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_NONE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/16 v0, 0x14

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 11
    sget-object v13, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_SEND_DATA:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_WILL_RETRY:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_NOT_INITIALIZED:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_INVALID_IMAGE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_DECRYPTION_FAILURE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_FLASH_FAILURE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_BAD_ARGUMENT:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_BAD_HEADER:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_BAD_WRITE_ALIGNMENT:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_BAD_ENCRYPTION_KEY:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_DUPLICATE_UPDATE_TO_SLOT:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_ENCRYPTED_UPDATE_REQUIRED:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_ERROR_UNKNOWN:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_INVALID_IMAGE_VERSION:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_CRQ_UNRESPONSIVE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_NONE:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->$VALUES:[Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 49
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult$SwigNext;->access$008()I

    move-result p1

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 54
    iput p3, p0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->swigValue:I

    add-int/lit8 p3, p3, 0x1

    .line 55
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult$SwigNext;->access$002(I)I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;",
            ")V"
        }
    .end annotation

    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 60
    iget p1, p3, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->swigValue:I

    .line 61
    iget p1, p0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult$SwigNext;->access$002(I)I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;
    .locals 6

    .line 38
    const-class v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    .line 39
    array-length v2, v1

    if-ge p0, v2, :cond_0

    if-ltz p0, :cond_0

    aget-object v2, v1, p0

    iget v2, v2, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->swigValue:I

    if-ne v2, p0, :cond_0

    .line 40
    aget-object p0, v1, p0

    return-object p0

    .line 41
    :cond_0
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 42
    iget v5, v4, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->swigValue:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 44
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No enum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " with value "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->$VALUES:[Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 34
    iget v0, p0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->swigValue:I

    return v0
.end method
