.class public interface abstract Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeConstants;
.super Ljava/lang/Object;
.source "FirmwareUpdateFeatureNativeConstants.java"


# static fields
.field public static final CRS_FWUP_DATA_MSG_MAX_LEN:I = 0x3cd

.field public static final CRS_FWUP_EP_HWID_LEN:I = 0x8

.field public static final CRS_FWUP_EP_SHA256_DIGEST_LENGTH:I = 0x20

.field public static final CRS_FWUP_GET_BLOCK_REQ_MSG_LEN:I = 0x4

.field public static final CRS_FWUP_GET_BLOCK_RESP_MSG_LEN:I = 0x10

.field public static final CRS_FWUP_GET_DATA_MSG_LEN:I = 0x9

.field public static final CRS_FWUP_MAX_CHIPID_LEN:I = 0x10

.field public static final CRS_FWUP_PROD_ID_LEN:I = 0x20
