.class public final enum Lcom/squareup/cardreader/lcr/CrsTmnBrandId;
.super Ljava/lang/Enum;
.source "CrsTmnBrandId.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/lcr/CrsTmnBrandId$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

.field public static final enum TMN_BRAND_ID_COMMON:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

.field public static final enum TMN_BRAND_ID_ID:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

.field public static final enum TMN_BRAND_ID_NANACO:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

.field public static final enum TMN_BRAND_ID_PITAPA:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

.field public static final enum TMN_BRAND_ID_QUICPAY:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

.field public static final enum TMN_BRAND_ID_RAKUTEN:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

.field public static final enum TMN_BRAND_ID_SUICA:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

.field public static final enum TMN_BRAND_ID_WAON:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    const/4 v1, 0x0

    const-string v2, "TMN_BRAND_ID_COMMON"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_COMMON:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    const/4 v2, 0x1

    const-string v3, "TMN_BRAND_ID_QUICPAY"

    const/16 v4, 0x64

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_QUICPAY:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    const/4 v3, 0x2

    const-string v4, "TMN_BRAND_ID_ID"

    const/16 v5, 0xc8

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_ID:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    const/4 v4, 0x3

    const-string v5, "TMN_BRAND_ID_SUICA"

    const/16 v6, 0x12c

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_SUICA:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    const/4 v5, 0x4

    const-string v6, "TMN_BRAND_ID_RAKUTEN"

    const/16 v7, 0x258

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_RAKUTEN:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    const/4 v6, 0x5

    const-string v7, "TMN_BRAND_ID_WAON"

    const/16 v8, 0x2bc

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_WAON:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    const/4 v7, 0x6

    const-string v8, "TMN_BRAND_ID_NANACO"

    const/16 v9, 0x320

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_NANACO:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    const/4 v8, 0x7

    const-string v9, "TMN_BRAND_ID_PITAPA"

    const/16 v10, 0x384

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_PITAPA:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    .line 11
    sget-object v9, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_COMMON:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_QUICPAY:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_ID:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_SUICA:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_RAKUTEN:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_WAON:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_NANACO:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->TMN_BRAND_ID_PITAPA:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    aput-object v1, v0, v8

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->$VALUES:[Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrsTmnBrandId$SwigNext;->access$008()I

    move-result p1

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 42
    iput p3, p0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->swigValue:I

    add-int/lit8 p3, p3, 0x1

    .line 43
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrsTmnBrandId$SwigNext;->access$002(I)I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/cardreader/lcr/CrsTmnBrandId;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
            ")V"
        }
    .end annotation

    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iget p1, p3, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->swigValue:I

    .line 49
    iget p1, p0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsTmnBrandId$SwigNext;->access$002(I)I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsTmnBrandId;
    .locals 6

    .line 26
    const-class v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    .line 27
    array-length v2, v1

    if-ge p0, v2, :cond_0

    if-ltz p0, :cond_0

    aget-object v2, v1, p0

    iget v2, v2, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->swigValue:I

    if-ne v2, p0, :cond_0

    .line 28
    aget-object p0, v1, p0

    return-object p0

    .line 29
    :cond_0
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 30
    iget v5, v4, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->swigValue:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 32
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No enum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " with value "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/lcr/CrsTmnBrandId;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/lcr/CrsTmnBrandId;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->$VALUES:[Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 22
    iget v0, p0, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->swigValue:I

    return v0
.end method
