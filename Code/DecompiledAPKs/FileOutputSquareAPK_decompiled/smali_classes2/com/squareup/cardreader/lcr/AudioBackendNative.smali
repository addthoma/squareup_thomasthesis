.class public Lcom/squareup/cardreader/lcr/AudioBackendNative;
.super Ljava/lang/Object;
.source "AudioBackendNative.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cr_comms_backend_audio_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;
    .locals 5

    .line 13
    invoke-static {}, Lcom/squareup/cardreader/lcr/AudioBackendNativeJNI;->cr_comms_backend_audio_alloc()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 14
    :cond_0
    new-instance v2, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;-><init>(JZ)V

    move-object v0, v2

    :goto_0
    return-object v0
.end method

.method public static cr_comms_backend_audio_enable_tx_for_connection(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V
    .locals 2

    .line 30
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/AudioBackendNativeJNI;->cr_comms_backend_audio_enable_tx_for_connection(J)V

    return-void
.end method

.method public static cr_comms_backend_audio_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;
    .locals 3

    .line 18
    new-instance v0, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/squareup/cardreader/lcr/AudioBackendNativeJNI;->cr_comms_backend_audio_free(J)J

    move-result-wide v1

    const/4 p0, 0x1

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;-><init>(JZ)V

    return-object v0
.end method

.method public static cr_comms_backend_audio_notify_phy_tx_complete(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V
    .locals 2

    .line 26
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/AudioBackendNativeJNI;->cr_comms_backend_audio_notify_phy_tx_complete(J)V

    return-void
.end method

.method public static cr_comms_backend_audio_shutdown(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V
    .locals 2

    .line 22
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/AudioBackendNativeJNI;->cr_comms_backend_audio_shutdown(J)V

    return-void
.end method

.method public static decode_r4_packet(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;I[S)Ljava/lang/Object;
    .locals 2

    .line 42
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1, p2}, Lcom/squareup/cardreader/lcr/AudioBackendNativeJNI;->decode_r4_packet(JI[S)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static feed_audio_samples(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;Ljava/lang/Object;II)V
    .locals 2

    .line 38
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1, p2, p3}, Lcom/squareup/cardreader/lcr/AudioBackendNativeJNI;->feed_audio_samples(JLjava/lang/Object;II)V

    return-void
.end method

.method public static initialize_backend_audio(IILcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;
    .locals 8

    .line 34
    new-instance v0, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

    invoke-static {p2}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)J

    move-result-wide v3

    invoke-static {p3}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;)J

    move-result-wide v5

    move v1, p0

    move v2, p1

    move-object v7, p4

    invoke-static/range {v1 .. v7}, Lcom/squareup/cardreader/lcr/AudioBackendNativeJNI;->initialize_backend_audio(IIJJLjava/lang/Object;)J

    move-result-wide p0

    const/4 p2, 0x1

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;-><init>(JZ)V

    return-object v0
.end method

.method public static set_legacy_reader_type(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;I)V
    .locals 2

    .line 46
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/lcr/AudioBackendNativeJNI;->set_legacy_reader_type(JI)V

    return-void
.end method
