.class final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized$ProtoAdapter_OnCardReaderBackendInitialized;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_OnCardReaderBackendInitialized"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 3839
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3855
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized$Builder;-><init>()V

    .line 3856
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 3857
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 3860
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 3864
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 3865
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3837
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized$ProtoAdapter_OnCardReaderBackendInitialized;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3850
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3837
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized$ProtoAdapter_OnCardReaderBackendInitialized;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;)I
    .locals 0

    .line 3844
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 3837
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized$ProtoAdapter_OnCardReaderBackendInitialized;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;
    .locals 0

    .line 3870
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized$Builder;

    move-result-object p1

    .line 3871
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 3872
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 3837
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized$ProtoAdapter_OnCardReaderBackendInitialized;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    move-result-object p1

    return-object p1
.end method
