.class public final Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SendMessageToReader"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$ProtoAdapter_SendMessageToReader;,
        Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final abort_secure_session_client_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$AbortSecureSession#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final abort_secure_session_network_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$AbortSecureSession#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final abort_secure_session_server_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$AbortSecureSession#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final abort_secure_session_timeout:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$AbortSecureSession#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final cancel_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$CancelPayment#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final enable_swipe_passthrough:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$EnableSwipePassthrough#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final initialize_card_reader_features:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$InitializeCardReaderFeatures#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final on_core_dump_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$OnCoreDumpDataSent#ADAPTER"
        tag = 0x11
    .end annotation
.end field

.field public final on_tamper_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$OnTamperDataSent#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final process_arpc:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$ProcessARPC#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final process_firmware_update_response:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$ProcessFirmwareUpdateResponse#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final process_secure_session_message_from_server:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final reinitialize_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$ReinitializeSecureSession#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final request_power_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$RequestPowerStatus#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$Reset#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final select_application:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$SelectApplication#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final start_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$StartPayment#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final start_refund:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$CardReader$StartRefund#ADAPTER"
        tag = 0x5e
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7909
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$ProtoAdapter_SendMessageToReader;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$ProtoAdapter_SendMessageToReader;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;Lokio/ByteString;)V
    .locals 1

    .line 8022
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 8023
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->initialize_card_reader_features:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->initialize_card_reader_features:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    .line 8024
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    .line 8025
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->request_power_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->request_power_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    .line 8026
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->on_tamper_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_tamper_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

    .line 8027
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->on_core_dump_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_core_dump_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    .line 8028
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_firmware_update_response:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_firmware_update_response:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    .line 8029
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->reinitialize_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reinitialize_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    .line 8030
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_secure_session_message_from_server:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_secure_session_message_from_server:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    .line 8031
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_client_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_client_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    .line 8032
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_network_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_network_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    .line 8033
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_server_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_server_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    .line 8034
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_timeout:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_timeout:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    .line 8035
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->enable_swipe_passthrough:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->enable_swipe_passthrough:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    .line 8036
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->start_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    .line 8037
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->start_refund:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_refund:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    .line 8038
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->cancel_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->cancel_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

    .line 8039
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->select_application:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->select_application:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    .line 8040
    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_arpc:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_arpc:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 8071
    :cond_0
    instance-of v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 8072
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;

    .line 8073
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->initialize_card_reader_features:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->initialize_card_reader_features:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    .line 8074
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    .line 8075
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->request_power_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->request_power_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    .line 8076
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_tamper_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_tamper_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

    .line 8077
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_core_dump_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_core_dump_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    .line 8078
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_firmware_update_response:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_firmware_update_response:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    .line 8079
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reinitialize_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reinitialize_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    .line 8080
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_secure_session_message_from_server:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_secure_session_message_from_server:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    .line 8081
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_client_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_client_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    .line 8082
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_network_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_network_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    .line 8083
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_server_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_server_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    .line 8084
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_timeout:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_timeout:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    .line 8085
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->enable_swipe_passthrough:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->enable_swipe_passthrough:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    .line 8086
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    .line 8087
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_refund:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_refund:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    .line 8088
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->cancel_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->cancel_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

    .line 8089
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->select_application:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->select_application:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    .line 8090
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_arpc:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_arpc:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    .line 8091
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 8096
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_12

    .line 8098
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 8099
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->initialize_card_reader_features:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8100
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8101
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->request_power_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8102
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_tamper_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8103
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_core_dump_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8104
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_firmware_update_response:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8105
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reinitialize_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8106
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_secure_session_message_from_server:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8107
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_client_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8108
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_network_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8109
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_server_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8110
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_timeout:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8111
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->enable_swipe_passthrough:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8112
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8113
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_refund:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8114
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->cancel_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8115
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->select_application:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8116
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_arpc:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;->hashCode()I

    move-result v2

    :cond_11
    add-int/2addr v0, v2

    .line 8117
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_12
    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 2

    .line 8045
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;-><init>()V

    .line 8046
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->initialize_card_reader_features:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->initialize_card_reader_features:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    .line 8047
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    .line 8048
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->request_power_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->request_power_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    .line 8049
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_tamper_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->on_tamper_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

    .line 8050
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_core_dump_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->on_core_dump_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    .line 8051
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_firmware_update_response:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_firmware_update_response:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    .line 8052
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reinitialize_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->reinitialize_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    .line 8053
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_secure_session_message_from_server:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_secure_session_message_from_server:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    .line 8054
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_client_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_client_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    .line 8055
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_network_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_network_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    .line 8056
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_server_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_server_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    .line 8057
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_timeout:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_timeout:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    .line 8058
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->enable_swipe_passthrough:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->enable_swipe_passthrough:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    .line 8059
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->start_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    .line 8060
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_refund:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->start_refund:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    .line 8061
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->cancel_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->cancel_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

    .line 8062
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->select_application:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->select_application:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    .line 8063
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_arpc:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_arpc:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    .line 8064
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 7908
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 8124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8125
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->initialize_card_reader_features:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    if-eqz v1, :cond_0

    const-string v1, ", initialize_card_reader_features="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->initialize_card_reader_features:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8126
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    if-eqz v1, :cond_1

    const-string v1, ", reset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8127
    :cond_1
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->request_power_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    if-eqz v1, :cond_2

    const-string v1, ", request_power_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->request_power_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8128
    :cond_2
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_tamper_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

    if-eqz v1, :cond_3

    const-string v1, ", on_tamper_data_sent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_tamper_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8129
    :cond_3
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_core_dump_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    if-eqz v1, :cond_4

    const-string v1, ", on_core_dump_data_sent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->on_core_dump_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8130
    :cond_4
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_firmware_update_response:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    if-eqz v1, :cond_5

    const-string v1, ", process_firmware_update_response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_firmware_update_response:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8131
    :cond_5
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reinitialize_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    if-eqz v1, :cond_6

    const-string v1, ", reinitialize_secure_session="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reinitialize_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8132
    :cond_6
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_secure_session_message_from_server:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    if-eqz v1, :cond_7

    const-string v1, ", process_secure_session_message_from_server="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_secure_session_message_from_server:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8133
    :cond_7
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_client_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    if-eqz v1, :cond_8

    const-string v1, ", abort_secure_session_client_error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_client_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8134
    :cond_8
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_network_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    if-eqz v1, :cond_9

    const-string v1, ", abort_secure_session_network_error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_network_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8135
    :cond_9
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_server_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    if-eqz v1, :cond_a

    const-string v1, ", abort_secure_session_server_error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_server_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8136
    :cond_a
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_timeout:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    if-eqz v1, :cond_b

    const-string v1, ", abort_secure_session_timeout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->abort_secure_session_timeout:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8137
    :cond_b
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->enable_swipe_passthrough:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    if-eqz v1, :cond_c

    const-string v1, ", enable_swipe_passthrough="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->enable_swipe_passthrough:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8138
    :cond_c
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    if-eqz v1, :cond_d

    const-string v1, ", start_payment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8139
    :cond_d
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_refund:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    if-eqz v1, :cond_e

    const-string v1, ", start_refund="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->start_refund:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8140
    :cond_e
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->cancel_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

    if-eqz v1, :cond_f

    const-string v1, ", cancel_payment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->cancel_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8141
    :cond_f
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->select_application:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    if-eqz v1, :cond_10

    const-string v1, ", select_application="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->select_application:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8142
    :cond_10
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_arpc:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    if-eqz v1, :cond_11

    const-string v1, ", process_arpc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->process_arpc:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_11
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SendMessageToReader{"

    .line 8143
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
