.class public final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public message:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4722
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;
    .locals 3

    .line 4732
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer$Builder;->message:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;-><init>(Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4719
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    move-result-object v0

    return-object v0
.end method

.method public message(Lokio/ByteString;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer$Builder;
    .locals 0

    .line 4726
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer$Builder;->message:Lokio/ByteString;

    return-object p0
.end method
