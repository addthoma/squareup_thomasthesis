.class final Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$ProtoAdapter_OnCardReaderInfoChanged;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_OnCardReaderInfoChanged"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 6980
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 6997
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;-><init>()V

    .line 6998
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 6999
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 7003
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 7001
    :cond_0
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;->card_reader_info(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;)Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;

    goto :goto_0

    .line 7007
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 7008
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 6978
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$ProtoAdapter_OnCardReaderInfoChanged;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 6991
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;->card_reader_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 6992
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 6978
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$ProtoAdapter_OnCardReaderInfoChanged;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;)I
    .locals 3

    .line 6985
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;->card_reader_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 6986
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 6978
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$ProtoAdapter_OnCardReaderInfoChanged;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;)Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;
    .locals 2

    .line 7013
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;

    move-result-object p1

    .line 7014
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;->card_reader_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;->card_reader_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;->card_reader_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;

    .line 7015
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 7016
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 6978
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged$ProtoAdapter_OnCardReaderInfoChanged;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;)Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    move-result-object p1

    return-object p1
.end method
