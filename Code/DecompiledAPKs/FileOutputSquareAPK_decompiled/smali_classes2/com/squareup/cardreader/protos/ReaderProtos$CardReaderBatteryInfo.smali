.class public final Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CardReaderBatteryInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$ProtoAdapter_CardReaderBatteryInfo;,
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BATTERY_MODE:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

.field public static final DEFAULT_CURRENT:Ljava/lang/Integer;

.field public static final DEFAULT_IS_CRITICAL:Ljava/lang/Boolean;

.field public static final DEFAULT_PERCENTAGE:Ljava/lang/Integer;

.field public static final DEFAULT_TEMPERATURE:Ljava/lang/Integer;

.field public static final DEFAULT_VOLTAGE:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final battery_mode:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$BatteryMode#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final current:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final is_critical:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final percentage:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field

.field public final temperature:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final voltage:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 5972
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$ProtoAdapter_CardReaderBatteryInfo;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$ProtoAdapter_CardReaderBatteryInfo;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 5976
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->DEFAULT_PERCENTAGE:Ljava/lang/Integer;

    .line 5978
    sput-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->DEFAULT_CURRENT:Ljava/lang/Integer;

    .line 5980
    sput-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->DEFAULT_VOLTAGE:Ljava/lang/Integer;

    .line 5982
    sput-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->DEFAULT_TEMPERATURE:Ljava/lang/Integer;

    .line 5984
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->DEFAULT_IS_CRITICAL:Ljava/lang/Boolean;

    .line 5986
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->DISCHARGING:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->DEFAULT_BATTERY_MODE:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;)V
    .locals 8

    .line 6026
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;Lokio/ByteString;)V
    .locals 1

    .line 6032
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 6033
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->percentage:Ljava/lang/Integer;

    .line 6034
    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->current:Ljava/lang/Integer;

    .line 6035
    iput-object p3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->voltage:Ljava/lang/Integer;

    .line 6036
    iput-object p4, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->temperature:Ljava/lang/Integer;

    .line 6037
    iput-object p5, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->is_critical:Ljava/lang/Boolean;

    .line 6038
    iput-object p6, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->battery_mode:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 6057
    :cond_0
    instance-of v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 6058
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    .line 6059
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->percentage:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->percentage:Ljava/lang/Integer;

    .line 6060
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->current:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->current:Ljava/lang/Integer;

    .line 6061
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->voltage:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->voltage:Ljava/lang/Integer;

    .line 6062
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->temperature:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->temperature:Ljava/lang/Integer;

    .line 6063
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->is_critical:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->is_critical:Ljava/lang/Boolean;

    .line 6064
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->battery_mode:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->battery_mode:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    .line 6065
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 6070
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 6072
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 6073
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->percentage:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6074
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->current:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6075
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->voltage:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6076
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->temperature:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6077
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->is_critical:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6078
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->battery_mode:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 6079
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;
    .locals 2

    .line 6043
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;-><init>()V

    .line 6044
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->percentage:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->percentage:Ljava/lang/Integer;

    .line 6045
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->current:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->current:Ljava/lang/Integer;

    .line 6046
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->voltage:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->voltage:Ljava/lang/Integer;

    .line 6047
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->temperature:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->temperature:Ljava/lang/Integer;

    .line 6048
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->is_critical:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->is_critical:Ljava/lang/Boolean;

    .line 6049
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->battery_mode:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->battery_mode:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    .line 6050
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 5971
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 6086
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6087
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->percentage:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->percentage:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6088
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->current:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", current="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->current:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6089
    :cond_1
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->voltage:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", voltage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->voltage:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6090
    :cond_2
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->temperature:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", temperature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->temperature:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6091
    :cond_3
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->is_critical:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", is_critical="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->is_critical:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6092
    :cond_4
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->battery_mode:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    if-eqz v1, :cond_5

    const-string v1, ", battery_mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;->battery_mode:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CardReaderBatteryInfo{"

    .line 6093
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
