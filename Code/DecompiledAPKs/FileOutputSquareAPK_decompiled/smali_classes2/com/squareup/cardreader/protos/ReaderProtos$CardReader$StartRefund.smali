.class public final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StartRefund"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund$ProtoAdapter_StartRefund;,
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AMOUNT_AUTHORIZED:Ljava/lang/Long;

.field public static final DEFAULT_CURRENT_TIME_MILLIS:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final amount_authorized:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field

.field public final current_time_millis:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1311
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund$ProtoAdapter_StartRefund;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund$ProtoAdapter_StartRefund;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 1315
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->DEFAULT_AMOUNT_AUTHORIZED:Ljava/lang/Long;

    .line 1317
    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->DEFAULT_CURRENT_TIME_MILLIS:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 1

    .line 1332
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;-><init>(Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1

    .line 1337
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1338
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->amount_authorized:Ljava/lang/Long;

    .line 1339
    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->current_time_millis:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1354
    :cond_0
    instance-of v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1355
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    .line 1356
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->amount_authorized:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->amount_authorized:Ljava/lang/Long;

    .line 1357
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->current_time_millis:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->current_time_millis:Ljava/lang/Long;

    .line 1358
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1363
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 1365
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1366
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->amount_authorized:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1367
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->current_time_millis:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 1368
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund$Builder;
    .locals 2

    .line 1344
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund$Builder;-><init>()V

    .line 1345
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->amount_authorized:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund$Builder;->amount_authorized:Ljava/lang/Long;

    .line 1346
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->current_time_millis:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund$Builder;->current_time_millis:Ljava/lang/Long;

    .line 1347
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1310
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1375
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1376
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->amount_authorized:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", amount_authorized="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->amount_authorized:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1377
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->current_time_millis:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", current_time_millis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;->current_time_millis:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "StartRefund{"

    .line 1378
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
