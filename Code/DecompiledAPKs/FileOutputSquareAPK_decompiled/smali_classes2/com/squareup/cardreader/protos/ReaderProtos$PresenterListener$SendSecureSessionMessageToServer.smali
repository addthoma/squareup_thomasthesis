.class public final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SendSecureSessionMessageToServer"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer$ProtoAdapter_SendSecureSessionMessageToServer;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MESSAGE:Lokio/ByteString;

.field private static final serialVersionUID:J


# instance fields
.field public final message:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 4663
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer$ProtoAdapter_SendSecureSessionMessageToServer;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer$ProtoAdapter_SendSecureSessionMessageToServer;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 4667
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->DEFAULT_MESSAGE:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;)V
    .locals 1

    .line 4676
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;-><init>(Lokio/ByteString;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lokio/ByteString;)V
    .locals 1

    .line 4680
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 4681
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->message:Lokio/ByteString;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 4695
    :cond_0
    instance-of v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 4696
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    .line 4697
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->message:Lokio/ByteString;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->message:Lokio/ByteString;

    .line 4698
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 4703
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 4705
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 4706
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->message:Lokio/ByteString;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 4707
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer$Builder;
    .locals 2

    .line 4686
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer$Builder;-><init>()V

    .line 4687
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->message:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer$Builder;->message:Lokio/ByteString;

    .line 4688
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 4662
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 4714
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4715
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->message:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->message:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SendSecureSessionMessageToServer{"

    .line 4716
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
