.class final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$ProtoAdapter_EnableSwipePassthrough;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_EnableSwipePassthrough"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1130
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1147
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$Builder;-><init>()V

    .line 1148
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1149
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 1153
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1151
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$Builder;->enable(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$Builder;

    goto :goto_0

    .line 1157
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1158
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1128
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$ProtoAdapter_EnableSwipePassthrough;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1141
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;->enable:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1142
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1128
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$ProtoAdapter_EnableSwipePassthrough;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;)I
    .locals 3

    .line 1135
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;->enable:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 1136
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1128
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$ProtoAdapter_EnableSwipePassthrough;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;
    .locals 0

    .line 1163
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$Builder;

    move-result-object p1

    .line 1164
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1165
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1128
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough$ProtoAdapter_EnableSwipePassthrough;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    move-result-object p1

    return-object p1
.end method
