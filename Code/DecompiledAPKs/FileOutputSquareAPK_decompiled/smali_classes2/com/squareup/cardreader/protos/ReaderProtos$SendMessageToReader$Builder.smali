.class public final Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public abort_secure_session_client_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

.field public abort_secure_session_network_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

.field public abort_secure_session_server_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

.field public abort_secure_session_timeout:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

.field public cancel_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

.field public enable_swipe_passthrough:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

.field public initialize_card_reader_features:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

.field public on_core_dump_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

.field public on_tamper_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

.field public process_arpc:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

.field public process_firmware_update_response:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

.field public process_secure_session_message_from_server:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

.field public reinitialize_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

.field public request_power_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

.field public reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

.field public select_application:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

.field public start_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

.field public start_refund:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8183
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public abort_secure_session_client_error(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8232
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_client_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    return-object p0
.end method

.method public abort_secure_session_network_error(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8238
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_network_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    return-object p0
.end method

.method public abort_secure_session_server_error(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8244
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_server_error:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    return-object p0
.end method

.method public abort_secure_session_timeout(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8250
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->abort_secure_session_timeout:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$AbortSecureSession;

    return-object p0
.end method

.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;
    .locals 2

    .line 8287
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;-><init>(Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 8146
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;

    move-result-object v0

    return-object v0
.end method

.method public cancel_payment(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8271
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->cancel_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$CancelPayment;

    return-object p0
.end method

.method public enable_swipe_passthrough(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8256
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->enable_swipe_passthrough:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$EnableSwipePassthrough;

    return-object p0
.end method

.method public initialize_card_reader_features(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8188
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->initialize_card_reader_features:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    return-object p0
.end method

.method public on_core_dump_data_sent(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8208
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->on_core_dump_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    return-object p0
.end method

.method public on_tamper_data_sent(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8203
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->on_tamper_data_sent:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnTamperDataSent;

    return-object p0
.end method

.method public process_arpc(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8281
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_arpc:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    return-object p0
.end method

.method public process_firmware_update_response(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8214
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_firmware_update_response:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    return-object p0
.end method

.method public process_secure_session_message_from_server(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8226
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->process_secure_session_message_from_server:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    return-object p0
.end method

.method public reinitialize_secure_session(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8220
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->reinitialize_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    return-object p0
.end method

.method public request_power_status(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8198
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->request_power_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$RequestPowerStatus;

    return-object p0
.end method

.method public reset(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8193
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    return-object p0
.end method

.method public select_application(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8276
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->select_application:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    return-object p0
.end method

.method public start_payment(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8261
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->start_payment:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    return-object p0
.end method

.method public start_refund(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;
    .locals 0

    .line 8266
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->start_refund:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartRefund;

    return-object p0
.end method
