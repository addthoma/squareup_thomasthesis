.class public Lcom/squareup/cardreader/PaymentProcessor;
.super Ljava/lang/Object;
.source "PaymentProcessor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;
    }
.end annotation


# instance fields
.field private final cardReaderDispatch:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field private final cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

.field private final firmwareUpdater:Lcom/squareup/cardreader/FirmwareUpdater;

.field private final internalListener:Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;

.field private final magSwipeFailureFilter:Lcom/squareup/cardreader/MagSwipeFailureFilter;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/MagSwipeFailureFilter;Lcom/squareup/cardreader/FirmwareUpdater;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Lcom/squareup/cardreader/MagSwipeFailureFilter;",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            ")V"
        }
    .end annotation

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    .line 56
    iput-object p2, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderDispatch:Ljavax/inject/Provider;

    .line 57
    iput-object p3, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 58
    iput-object p4, p0, Lcom/squareup/cardreader/PaymentProcessor;->magSwipeFailureFilter:Lcom/squareup/cardreader/MagSwipeFailureFilter;

    .line 59
    new-instance p1, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;

    invoke-direct {p1, p0}, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;-><init>(Lcom/squareup/cardreader/PaymentProcessor;)V

    iput-object p1, p0, Lcom/squareup/cardreader/PaymentProcessor;->internalListener:Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;

    .line 60
    iput-object p5, p0, Lcom/squareup/cardreader/PaymentProcessor;->firmwareUpdater:Lcom/squareup/cardreader/FirmwareUpdater;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/EmvListener;
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/squareup/cardreader/PaymentProcessor;->emvListener()Lcom/squareup/cardreader/EmvListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/cardreader/PaymentProcessor;)Z
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/squareup/cardreader/PaymentProcessor;->hasEmvListener()Z

    move-result p0

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/MagSwipeListener;
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/squareup/cardreader/PaymentProcessor;->magSwipeListener()Lcom/squareup/cardreader/MagSwipeListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/PinRequestListener;
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/squareup/cardreader/PaymentProcessor;->pinRequestListener()Lcom/squareup/cardreader/PinRequestListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/RealCardReaderListeners;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/NfcListener;
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/squareup/cardreader/PaymentProcessor;->nfcListener()Lcom/squareup/cardreader/NfcListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/PaymentCompletionListener;
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/squareup/cardreader/PaymentProcessor;->paymentCompletionListener()Lcom/squareup/cardreader/PaymentCompletionListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/FirmwareUpdater;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/cardreader/PaymentProcessor;->firmwareUpdater:Lcom/squareup/cardreader/FirmwareUpdater;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/MagSwipeFailureFilter;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/cardreader/PaymentProcessor;->magSwipeFailureFilter:Lcom/squareup/cardreader/MagSwipeFailureFilter;

    return-object p0
.end method

.method private emvListener()Lcom/squareup/cardreader/EmvListener;
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->getEmvListener()Lcom/squareup/cardreader/EmvListener;

    move-result-object v0

    return-object v0
.end method

.method private hasEmvListener()Z
    .locals 2

    .line 172
    invoke-direct {p0}, Lcom/squareup/cardreader/PaymentProcessor;->emvListener()Lcom/squareup/cardreader/EmvListener;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private magSwipeListener()Lcom/squareup/cardreader/MagSwipeListener;
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->getMagSwipeListener()Lcom/squareup/cardreader/MagSwipeListener;

    move-result-object v0

    return-object v0
.end method

.method private nfcListener()Lcom/squareup/cardreader/NfcListener;
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->getNfcListener()Lcom/squareup/cardreader/NfcListener;

    move-result-object v0

    return-object v0
.end method

.method private paymentCompletionListener()Lcom/squareup/cardreader/PaymentCompletionListener;
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->getPaymentCompletionListener()Lcom/squareup/cardreader/PaymentCompletionListener;

    move-result-object v0

    return-object v0
.end method

.method private pinRequestListener()Lcom/squareup/cardreader/PinRequestListener;
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->getPinRequestListener()Lcom/squareup/cardreader/PinRequestListener;

    move-result-object v0

    return-object v0
.end method

.method private startPaymentInteraction(Lcom/squareup/cardreader/PaymentInteraction;)V
    .locals 3

    .line 197
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTaps()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {p1}, Lcom/squareup/cardreader/PaymentInteraction;->getAmountAuthorized()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/cardreader/CardReaderInfo;->setIsInPayment(J)V

    return-void

    .line 206
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot start payment on reader: Already in one."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 209
    :cond_1
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {p1}, Lcom/squareup/cardreader/PaymentInteraction;->getAmountAuthorized()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/cardreader/CardReaderInfo;->setIsInPayment(J)V

    .line 210
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    .line 212
    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch;->startPaymentInteraction(Lcom/squareup/cardreader/PaymentInteraction;)V

    return-void
.end method


# virtual methods
.method public ackTmnWriteNotify()V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->ackTmnWriteNotify()V

    return-void
.end method

.method public cancelPayment()V
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->setNotInPayment()V

    .line 131
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->cancelPayment()V

    return-void

    .line 128
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot cancel; not in a payment."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public cancelTmnRequest()V
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->cancelTmnRequest()V

    return-void
.end method

.method getInternalListener()Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->internalListener:Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;

    return-object v0
.end method

.method public onPinBypass()V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->onPinBypass()V

    return-void
.end method

.method public onPinDigitEntered(I)V
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch;->onPinDigitEntered(I)V

    return-void
.end method

.method public onPinPadReset()V
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->onPinPadReset()V

    return-void
.end method

.method public onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch;->onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V

    return-void
.end method

.method public processARPC([B)V
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch;->processARPC([B)V

    return-void
.end method

.method public reset()V
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->setNotInPayment()V

    return-void
.end method

.method public selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch;->selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V

    return-void
.end method

.method public selectApplication(Lcom/squareup/cardreader/EmvApplication;)V
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch;->selectApplication(Lcom/squareup/cardreader/EmvApplication;)V

    return-void
.end method

.method public sendPowerupHint(I)V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch;->sendReaderPowerupHint(I)V

    return-void
.end method

.method public sendTmnBytesToReader([B)V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch;->sendTmnDataToReader([B)V

    return-void
.end method

.method public startPayment(JJ)V
    .locals 1

    .line 68
    sget-object v0, Lcom/squareup/cardreader/EmvPaymentInteraction;->Companion:Lcom/squareup/cardreader/EmvPaymentInteraction$Companion;

    .line 69
    invoke-virtual {v0, p3, p4, p1, p2}, Lcom/squareup/cardreader/EmvPaymentInteraction$Companion;->startPayment(JJ)Lcom/squareup/cardreader/EmvPaymentInteraction;

    move-result-object p1

    .line 68
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/PaymentProcessor;->startPaymentInteraction(Lcom/squareup/cardreader/PaymentInteraction;)V

    return-void
.end method

.method public startRefund(JJ)V
    .locals 1

    .line 73
    sget-object v0, Lcom/squareup/cardreader/EmvPaymentInteraction;->Companion:Lcom/squareup/cardreader/EmvPaymentInteraction$Companion;

    .line 74
    invoke-virtual {v0, p3, p4, p1, p2}, Lcom/squareup/cardreader/EmvPaymentInteraction$Companion;->startRefund(JJ)Lcom/squareup/cardreader/EmvPaymentInteraction;

    move-result-object p1

    .line 73
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/PaymentProcessor;->startPaymentInteraction(Lcom/squareup/cardreader/PaymentInteraction;)V

    return-void
.end method

.method public startTmnCheckBalance(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
    .locals 1

    .line 89
    sget-object v0, Lcom/squareup/cardreader/TmnPaymentInteraction;->Companion:Lcom/squareup/cardreader/TmnPaymentInteraction$Companion;

    .line 90
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/TmnPaymentInteraction$Companion;->startCheckBalance(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)Lcom/squareup/cardreader/TmnPaymentInteraction;

    move-result-object p1

    .line 89
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/PaymentProcessor;->startPaymentInteraction(Lcom/squareup/cardreader/PaymentInteraction;)V

    return-void
.end method

.method public startTmnMiryo([B)V
    .locals 3

    .line 113
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/squareup/cardreader/CardReaderInfo;->setIsInPayment(J)V

    .line 119
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch;->startTmnMiryo([B)V

    return-void

    .line 114
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot start payment on reader: Already in one."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public startTmnPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
    .locals 1

    .line 78
    sget-object v0, Lcom/squareup/cardreader/TmnPaymentInteraction;->Companion:Lcom/squareup/cardreader/TmnPaymentInteraction$Companion;

    .line 79
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/TmnPaymentInteraction$Companion;->startPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)Lcom/squareup/cardreader/TmnPaymentInteraction;

    move-result-object p1

    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/PaymentProcessor;->startPaymentInteraction(Lcom/squareup/cardreader/PaymentInteraction;)V

    return-void
.end method

.method public startTmnRefund(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
    .locals 1

    .line 83
    sget-object v0, Lcom/squareup/cardreader/TmnPaymentInteraction;->Companion:Lcom/squareup/cardreader/TmnPaymentInteraction$Companion;

    .line 84
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/TmnPaymentInteraction$Companion;->startRefund(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)Lcom/squareup/cardreader/TmnPaymentInteraction;

    move-result-object p1

    .line 83
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/PaymentProcessor;->startPaymentInteraction(Lcom/squareup/cardreader/PaymentInteraction;)V

    return-void
.end method

.method public startTmnTestPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
    .locals 1

    .line 96
    sget-object v0, Lcom/squareup/cardreader/TmnPaymentInteraction;->Companion:Lcom/squareup/cardreader/TmnPaymentInteraction$Companion;

    .line 97
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/TmnPaymentInteraction$Companion;->startOnlineTest(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)Lcom/squareup/cardreader/TmnPaymentInteraction;

    move-result-object p1

    .line 96
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/PaymentProcessor;->startPaymentInteraction(Lcom/squareup/cardreader/PaymentInteraction;)V

    return-void
.end method

.method public submitPinBlock()V
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->submitPinBlock()V

    return-void
.end method
