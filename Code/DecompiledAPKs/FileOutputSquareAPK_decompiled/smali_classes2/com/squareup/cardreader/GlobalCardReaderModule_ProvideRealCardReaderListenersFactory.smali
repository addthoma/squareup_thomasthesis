.class public final Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRealCardReaderListenersFactory;
.super Ljava/lang/Object;
.source "GlobalCardReaderModule_ProvideRealCardReaderListenersFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/RealCardReaderListeners;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/cardreader/GlobalCardReaderModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/GlobalCardReaderModule;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRealCardReaderListenersFactory;->module:Lcom/squareup/cardreader/GlobalCardReaderModule;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/GlobalCardReaderModule;)Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRealCardReaderListenersFactory;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRealCardReaderListenersFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRealCardReaderListenersFactory;-><init>(Lcom/squareup/cardreader/GlobalCardReaderModule;)V

    return-object v0
.end method

.method public static provideRealCardReaderListeners(Lcom/squareup/cardreader/GlobalCardReaderModule;)Lcom/squareup/cardreader/RealCardReaderListeners;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/cardreader/GlobalCardReaderModule;->provideRealCardReaderListeners()Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/RealCardReaderListeners;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/RealCardReaderListeners;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRealCardReaderListenersFactory;->module:Lcom/squareup/cardreader/GlobalCardReaderModule;

    invoke-static {v0}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRealCardReaderListenersFactory;->provideRealCardReaderListeners(Lcom/squareup/cardreader/GlobalCardReaderModule;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideRealCardReaderListenersFactory;->get()Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    return-object v0
.end method
