.class public Lcom/squareup/cardreader/ble/BleScanFilter;
.super Ljava/lang/Object;
.source "BleScanFilter.java"


# static fields
.field private static final SCAN_RECORD_TYPE_UUID:I = 0x6

.field private static final SCAN_RECORD_TYPE_UUID_BYTES:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x10

    new-array v0, v0, [B

    .line 18
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/cardreader/ble/BleScanFilter;->SCAN_RECORD_TYPE_UUID_BYTES:[B

    return-void

    :array_0
    .array-data 1
        0x7ct
        -0x12t
        -0x71t
        0x75t
        -0x28t
        -0x42t
        0x17t
        -0x4ft
        0x7ct
        0x4bt
        0x15t
        0x8t
        0x61t
        -0x12t
        -0x7ft
        0x15t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static parseScanRecord([B)Landroid/util/SparseArray;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Landroid/util/SparseArray<",
            "[B>;"
        }
    .end annotation

    .line 37
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    const/4 v1, 0x0

    .line 40
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_2

    add-int/lit8 v2, v1, 0x1

    .line 41
    aget-byte v1, p0, v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 45
    :cond_0
    aget-byte v3, p0, v2

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v4, v2, 0x1

    add-int/2addr v1, v2

    .line 49
    invoke-static {p0, v4, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v2

    .line 51
    invoke-virtual {v0, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    :goto_1
    return-object v0
.end method


# virtual methods
.method public createScanFiltersForSquarePos()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/bluetooth/le/ScanFilter;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Landroid/os/ParcelUuid;

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    invoke-direct {v0, v1}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    .line 27
    new-instance v1, Landroid/bluetooth/le/ScanFilter$Builder;

    invoke-direct {v1}, Landroid/bluetooth/le/ScanFilter$Builder;-><init>()V

    invoke-virtual {v1, v0}, Landroid/bluetooth/le/ScanFilter$Builder;->setServiceUuid(Landroid/os/ParcelUuid;)Landroid/bluetooth/le/ScanFilter$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/le/ScanFilter$Builder;->build()Landroid/bluetooth/le/ScanFilter;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isCompatibleDevice(Lcom/squareup/cardreader/ble/BleScanResult;)Z
    .locals 0

    .line 22
    invoke-interface {p1}, Lcom/squareup/cardreader/ble/BleScanResult;->getName()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isCompatibleDevice([B)Z
    .locals 1

    .line 31
    invoke-static {p1}, Lcom/squareup/cardreader/ble/BleScanFilter;->parseScanRecord([B)Landroid/util/SparseArray;

    move-result-object p1

    const/4 v0, 0x6

    .line 32
    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    .line 33
    sget-object v0, Lcom/squareup/cardreader/ble/BleScanFilter;->SCAN_RECORD_TYPE_UUID_BYTES:[B

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result p1

    return p1
.end method
