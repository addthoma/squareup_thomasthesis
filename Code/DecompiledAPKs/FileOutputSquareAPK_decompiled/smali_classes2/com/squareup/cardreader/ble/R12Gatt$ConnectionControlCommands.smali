.class final enum Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;
.super Ljava/lang/Enum;
.source "R12Gatt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/R12Gatt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "ConnectionControlCommands"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

.field public static final enum DISCONNECT:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

.field public static final enum EXCHANGE_MTU:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

.field public static final enum FORGET_BOND:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

.field public static final enum INDICATE_SERVICE_CHANGE:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

.field public static final enum INITIATE_BONDING:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

.field public static final enum RESET_TRANSPORT:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

.field public static final enum SEND_SLAVE_SECURITY_REQ:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

.field public static final enum UPDATE_CONN_PARAMS:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 25
    new-instance v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    const/4 v1, 0x0

    const-string v2, "FORGET_BOND"

    invoke-direct {v0, v2, v1}, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->FORGET_BOND:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    .line 26
    new-instance v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    const/4 v2, 0x1

    const-string v3, "INITIATE_BONDING"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->INITIATE_BONDING:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    .line 27
    new-instance v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    const/4 v3, 0x2

    const-string v4, "EXCHANGE_MTU"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->EXCHANGE_MTU:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    .line 28
    new-instance v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    const/4 v4, 0x3

    const-string v5, "UPDATE_CONN_PARAMS"

    invoke-direct {v0, v5, v4}, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->UPDATE_CONN_PARAMS:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    .line 29
    new-instance v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    const/4 v5, 0x4

    const-string v6, "SEND_SLAVE_SECURITY_REQ"

    invoke-direct {v0, v6, v5}, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->SEND_SLAVE_SECURITY_REQ:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    .line 30
    new-instance v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    const/4 v6, 0x5

    const-string v7, "DISCONNECT"

    invoke-direct {v0, v7, v6}, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->DISCONNECT:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    .line 31
    new-instance v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    const/4 v7, 0x6

    const-string v8, "INDICATE_SERVICE_CHANGE"

    invoke-direct {v0, v8, v7}, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->INDICATE_SERVICE_CHANGE:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    .line 32
    new-instance v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    const/4 v8, 0x7

    const-string v9, "RESET_TRANSPORT"

    invoke-direct {v0, v9, v8}, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->RESET_TRANSPORT:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    .line 24
    sget-object v9, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->FORGET_BOND:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->INITIATE_BONDING:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->EXCHANGE_MTU:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->UPDATE_CONN_PARAMS:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->SEND_SLAVE_SECURITY_REQ:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->DISCONNECT:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->INDICATE_SERVICE_CHANGE:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->RESET_TRANSPORT:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    aput-object v1, v0, v8

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->$VALUES:[Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;
    .locals 1

    .line 24
    const-class v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;
    .locals 1

    .line 24
    sget-object v0, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->$VALUES:[Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    return-object v0
.end method


# virtual methods
.method public value()[B
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [B

    .line 35
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->ordinal()I

    move-result v1

    int-to-byte v1, v1

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    return-object v0
.end method
