.class public final Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;
.super Ljava/lang/Object;
.source "BleConnectionStateMachineV2.kt"

# interfaces
.implements Lcom/squareup/cardreader/ble/StateMachineV2;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0012\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001BS\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000f\u0012\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u0011\u0012\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J\u0008\u0010\u001c\u001a\u00020\u001dH\u0016J\u0013\u0010\u001e\u001a\u00020\u001dH\u0081@\u00f8\u0001\u0000\u00a2\u0006\u0004\u0008\u001f\u0010 J\u0008\u0010!\u001a\u00020\u001dH\u0016J\u0011\u0010\"\u001a\u00020#H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010 J\u0008\u0010$\u001a\u00020\u001dH\u0016J\u000e\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\'0&H\u0016J\u0017\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\u001d0)H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010 J\u000e\u0010*\u001a\u0008\u0012\u0004\u0012\u00020\'0&H\u0016J\u0017\u0010+\u001a\u0008\u0012\u0004\u0012\u00020\u001d0)H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010 J\u0010\u0010,\u001a\u00020\u001d2\u0006\u0010-\u001a\u00020.H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;",
        "Lcom/squareup/cardreader/ble/StateMachineV2;",
        "context",
        "Landroid/content/Context;",
        "device",
        "Landroid/bluetooth/BluetoothDevice;",
        "events",
        "Lcom/squareup/cardreader/ble/RealConnectionEvents;",
        "autoConnect",
        "",
        "backgroundDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "connectable",
        "Lcom/squareup/blecoroutines/Connectable;",
        "executionEnv",
        "Lcom/squareup/cardreader/ble/Timeouts;",
        "negotiator",
        "Lcom/squareup/cardreader/ble/ConnectionNegotiator;",
        "connectionManager",
        "Lcom/squareup/cardreader/ble/ConnectionManager;",
        "(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/ble/RealConnectionEvents;ZLkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/blecoroutines/Connectable;Lcom/squareup/cardreader/ble/Timeouts;Lcom/squareup/cardreader/ble/ConnectionNegotiator;Lcom/squareup/cardreader/ble/ConnectionManager;)V",
        "getEvents",
        "()Lcom/squareup/cardreader/ble/RealConnectionEvents;",
        "scope",
        "Lkotlinx/coroutines/CoroutineScope;",
        "sendChannel",
        "Lkotlinx/coroutines/channels/Channel;",
        "Lcom/squareup/cardreader/ble/SendMessage;",
        "connect",
        "",
        "connectBlocking",
        "connectBlocking$impl_release",
        "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "disconnect",
        "establishConnection",
        "Lcom/squareup/blecoroutines/Connection;",
        "forgetBond",
        "readAckVector",
        "Lio/reactivex/Single;",
        "",
        "readConnectionIntervalAfterDelay",
        "Lkotlinx/coroutines/Deferred;",
        "readMtu",
        "readRssiUntilCancelled",
        "writeData",
        "data",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final autoConnect:Z

.field private final backgroundDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

.field private final connectable:Lcom/squareup/blecoroutines/Connectable;

.field private final connectionManager:Lcom/squareup/cardreader/ble/ConnectionManager;

.field private final context:Landroid/content/Context;

.field private final device:Landroid/bluetooth/BluetoothDevice;

.field private final events:Lcom/squareup/cardreader/ble/RealConnectionEvents;

.field private final executionEnv:Lcom/squareup/cardreader/ble/Timeouts;

.field private final negotiator:Lcom/squareup/cardreader/ble/ConnectionNegotiator;

.field private final scope:Lkotlinx/coroutines/CoroutineScope;

.field private final sendChannel:Lkotlinx/coroutines/channels/Channel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/channels/Channel<",
            "Lcom/squareup/cardreader/ble/SendMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/ble/RealConnectionEvents;ZLkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/blecoroutines/Connectable;Lcom/squareup/cardreader/ble/Timeouts;Lcom/squareup/cardreader/ble/ConnectionNegotiator;Lcom/squareup/cardreader/ble/ConnectionManager;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backgroundDispatcher"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectable"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "executionEnv"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "negotiator"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectionManager"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->device:Landroid/bluetooth/BluetoothDevice;

    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->events:Lcom/squareup/cardreader/ble/RealConnectionEvents;

    iput-boolean p4, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->autoConnect:Z

    iput-object p5, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->backgroundDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    iput-object p6, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->connectable:Lcom/squareup/blecoroutines/Connectable;

    iput-object p7, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->executionEnv:Lcom/squareup/cardreader/ble/Timeouts;

    iput-object p8, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->negotiator:Lcom/squareup/cardreader/ble/ConnectionNegotiator;

    iput-object p9, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->connectionManager:Lcom/squareup/cardreader/ble/ConnectionManager;

    const p1, 0x7fffffff

    .line 57
    invoke-static {p1}, Lkotlinx/coroutines/channels/ChannelKt;->Channel(I)Lkotlinx/coroutines/channels/Channel;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->sendChannel:Lkotlinx/coroutines/channels/Channel;

    .line 58
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->backgroundDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    check-cast p1, Lkotlin/coroutines/CoroutineContext;

    invoke-static {p1}, Lkotlinx/coroutines/CoroutineScopeKt;->CoroutineScope(Lkotlin/coroutines/CoroutineContext;)Lkotlinx/coroutines/CoroutineScope;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->scope:Lkotlinx/coroutines/CoroutineScope;

    .line 61
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/RealConnectionEvents;

    move-result-object p1

    sget-object p2, Lcom/squareup/cardreader/ble/R12State;->CREATED:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishLoggingState$impl_release(Lcom/squareup/cardreader/ble/R12State;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/ble/RealConnectionEvents;ZLkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/blecoroutines/Connectable;Lcom/squareup/cardreader/ble/Timeouts;Lcom/squareup/cardreader/ble/ConnectionNegotiator;Lcom/squareup/cardreader/ble/ConnectionManager;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 27

    move/from16 v0, p10

    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_0

    .line 52
    new-instance v1, Lcom/squareup/cardreader/ble/Timeouts;

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x0

    const-wide/16 v9, 0x0

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    const/16 v15, 0x3f

    const/16 v16, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v16}, Lcom/squareup/cardreader/ble/Timeouts;-><init>(JJJJJJILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object/from16 v24, v1

    goto :goto_0

    :cond_0
    move-object/from16 v24, p7

    :goto_0
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_1

    .line 53
    new-instance v1, Lcom/squareup/cardreader/ble/RealConnectionNegotiator;

    invoke-direct {v1}, Lcom/squareup/cardreader/ble/RealConnectionNegotiator;-><init>()V

    check-cast v1, Lcom/squareup/cardreader/ble/ConnectionNegotiator;

    move-object/from16 v25, v1

    goto :goto_1

    :cond_1
    move-object/from16 v25, p8

    :goto_1
    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_2

    .line 54
    new-instance v0, Lcom/squareup/cardreader/ble/RealConnectionManager;

    invoke-direct {v0}, Lcom/squareup/cardreader/ble/RealConnectionManager;-><init>()V

    check-cast v0, Lcom/squareup/cardreader/ble/ConnectionManager;

    move-object/from16 v26, v0

    goto :goto_2

    :cond_2
    move-object/from16 v26, p9

    :goto_2
    move-object/from16 v17, p0

    move-object/from16 v18, p1

    move-object/from16 v19, p2

    move-object/from16 v20, p3

    move/from16 v21, p4

    move-object/from16 v22, p5

    move-object/from16 v23, p6

    invoke-direct/range {v17 .. v26}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/ble/RealConnectionEvents;ZLkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/blecoroutines/Connectable;Lcom/squareup/cardreader/ble/Timeouts;Lcom/squareup/cardreader/ble/ConnectionNegotiator;Lcom/squareup/cardreader/ble/ConnectionManager;)V

    return-void
.end method

.method public static final synthetic access$getAutoConnect$p(Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;)Z
    .locals 0

    .line 45
    iget-boolean p0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->autoConnect:Z

    return p0
.end method

.method public static final synthetic access$getConnectable$p(Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;)Lcom/squareup/blecoroutines/Connectable;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->connectable:Lcom/squareup/blecoroutines/Connectable;

    return-object p0
.end method

.method public static final synthetic access$getContext$p(Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;)Landroid/content/Context;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->context:Landroid/content/Context;

    return-object p0
.end method

.method public static final synthetic access$getExecutionEnv$p(Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;)Lcom/squareup/cardreader/ble/Timeouts;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->executionEnv:Lcom/squareup/cardreader/ble/Timeouts;

    return-object p0
.end method

.method public static final synthetic access$getSendChannel$p(Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;)Lkotlinx/coroutines/channels/Channel;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->sendChannel:Lkotlinx/coroutines/channels/Channel;

    return-object p0
.end method


# virtual methods
.method public connect()V
    .locals 6

    .line 85
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->scope:Lkotlinx/coroutines/CoroutineScope;

    new-instance v1, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connect$1;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connect$1;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;Lkotlin/coroutines/Continuation;)V

    move-object v3, v1

    check-cast v3, Lkotlin/jvm/functions/Function2;

    const/4 v1, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->launch$default(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    return-void
.end method

.method public final connectBlocking$impl_release(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 29
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;

    if-eqz v2, :cond_0

    move-object v2, v0

    check-cast v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;

    iget v3, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->label:I

    const/high16 v4, -0x80000000

    and-int/2addr v3, v4

    if-eqz v3, :cond_0

    iget v0, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->label:I

    sub-int/2addr v0, v4

    iput v0, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;

    invoke-direct {v2, v1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;Lkotlin/coroutines/Continuation;)V

    :goto_0
    iget-object v0, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v10

    .line 125
    iget v3, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->label:I

    const/4 v11, 0x0

    const/4 v12, 0x1

    const/4 v13, 0x0

    packed-switch v3, :pswitch_data_0

    .line 184
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :pswitch_0
    iget-object v3, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$5:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    iget-object v4, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$4:Ljava/lang/Object;

    check-cast v4, Lcom/squareup/cardreader/ble/R12State;

    iget-object v5, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$3:Ljava/lang/Object;

    check-cast v5, Lkotlinx/coroutines/Deferred;

    iget-object v6, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$2:Ljava/lang/Object;

    check-cast v6, Lcom/squareup/cardreader/ble/ConnectionError;

    iget-object v7, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$1:Ljava/lang/Object;

    check-cast v7, Lcom/squareup/blecoroutines/BleError;

    iget v8, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->I$0:I

    iget-object v2, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$0:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;

    :try_start_0
    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_12

    :catch_0
    move-object v0, v3

    const/4 v3, 0x0

    goto/16 :goto_13

    :pswitch_1
    iget-object v3, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$5:Ljava/lang/Object;

    check-cast v3, Lcom/squareup/cardreader/ble/NegotiatedConnection;

    iget-object v3, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$4:Ljava/lang/Object;

    check-cast v3, Lcom/squareup/blecoroutines/Connection;

    iget-object v3, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$3:Ljava/lang/Object;

    check-cast v3, Lkotlinx/coroutines/Deferred;

    iget-object v4, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$2:Ljava/lang/Object;

    check-cast v4, Lcom/squareup/cardreader/ble/ConnectionError;

    iget-object v5, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$1:Ljava/lang/Object;

    check-cast v5, Lcom/squareup/blecoroutines/BleError;

    iget v6, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->I$0:I

    iget-object v7, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$0:Ljava/lang/Object;

    check-cast v7, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;

    :try_start_1
    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_1
    .catch Lcom/squareup/blecoroutines/BleError; {:try_start_1 .. :try_end_1} :catch_9
    .catch Lcom/squareup/cardreader/ble/ConnectionError; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1 .. :try_end_1} :catch_7

    move-object/from16 v17, v3

    move-object v15, v4

    move-object v14, v5

    goto/16 :goto_5

    :pswitch_2
    iget-object v3, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$5:Ljava/lang/Object;

    check-cast v3, Lcom/squareup/cardreader/ble/NegotiatedConnection;

    iget-object v4, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$4:Ljava/lang/Object;

    check-cast v4, Lcom/squareup/blecoroutines/Connection;

    iget-object v5, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$3:Ljava/lang/Object;

    check-cast v5, Lkotlinx/coroutines/Deferred;

    iget-object v6, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$2:Ljava/lang/Object;

    check-cast v6, Lcom/squareup/cardreader/ble/ConnectionError;

    iget-object v7, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$1:Ljava/lang/Object;

    check-cast v7, Lcom/squareup/blecoroutines/BleError;

    iget v8, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->I$0:I

    iget-object v9, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$0:Ljava/lang/Object;

    check-cast v9, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;

    :try_start_2
    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_2
    .catch Lcom/squareup/blecoroutines/BleError; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/squareup/cardreader/ble/ConnectionError; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_1

    move-object v15, v6

    move-object v14, v7

    move v13, v8

    move-object v11, v9

    move-object v9, v5

    move-object v5, v3

    goto/16 :goto_4

    :catch_1
    move-exception v0

    move-object/from16 v17, v5

    move-object v4, v6

    move v15, v8

    move-object v7, v9

    goto/16 :goto_c

    :catch_2
    move-exception v0

    move-object/from16 v17, v5

    move-object v5, v7

    move v15, v8

    move-object v7, v9

    goto/16 :goto_d

    :catch_3
    move-exception v0

    move-object/from16 v17, v5

    move-object v4, v6

    move v15, v8

    move-object v7, v9

    goto/16 :goto_e

    :pswitch_3
    iget-object v3, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$4:Ljava/lang/Object;

    check-cast v3, Lcom/squareup/blecoroutines/Connection;

    iget-object v4, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$3:Ljava/lang/Object;

    check-cast v4, Lkotlinx/coroutines/Deferred;

    iget-object v5, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$2:Ljava/lang/Object;

    check-cast v5, Lcom/squareup/cardreader/ble/ConnectionError;

    iget-object v6, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$1:Ljava/lang/Object;

    check-cast v6, Lcom/squareup/blecoroutines/BleError;

    iget v7, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->I$0:I

    iget-object v8, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$0:Ljava/lang/Object;

    check-cast v8, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;

    :try_start_3
    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_3
    .catch Lcom/squareup/blecoroutines/BleError; {:try_start_3 .. :try_end_3} :catch_6
    .catch Lcom/squareup/cardreader/ble/ConnectionError; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/util/concurrent/CancellationException; {:try_start_3 .. :try_end_3} :catch_4

    move v15, v7

    move-object v7, v8

    move-object/from16 v28, v6

    move-object v6, v3

    move-object v3, v4

    move-object v4, v5

    move-object/from16 v5, v28

    goto/16 :goto_3

    :catch_4
    move-exception v0

    move-object/from16 v17, v4

    move-object v4, v5

    move v15, v7

    move-object v7, v8

    goto/16 :goto_c

    :catch_5
    move-exception v0

    move-object/from16 v17, v4

    move-object v5, v6

    move v15, v7

    move-object v7, v8

    goto/16 :goto_d

    :catch_6
    move-exception v0

    move-object/from16 v17, v4

    move-object v4, v5

    move v15, v7

    move-object v7, v8

    goto/16 :goto_e

    :pswitch_4
    iget-object v3, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$3:Ljava/lang/Object;

    check-cast v3, Lkotlinx/coroutines/Deferred;

    iget-object v4, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$2:Ljava/lang/Object;

    check-cast v4, Lcom/squareup/cardreader/ble/ConnectionError;

    iget-object v5, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$1:Ljava/lang/Object;

    check-cast v5, Lcom/squareup/blecoroutines/BleError;

    iget v6, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->I$0:I

    iget-object v7, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$0:Ljava/lang/Object;

    check-cast v7, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;

    :try_start_4
    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_4
    .catch Lcom/squareup/blecoroutines/BleError; {:try_start_4 .. :try_end_4} :catch_9
    .catch Lcom/squareup/cardreader/ble/ConnectionError; {:try_start_4 .. :try_end_4} :catch_8
    .catch Ljava/util/concurrent/CancellationException; {:try_start_4 .. :try_end_4} :catch_7

    move-object v8, v4

    move-object v9, v5

    move v15, v6

    move-object v14, v7

    move-object v7, v3

    goto/16 :goto_2

    :catch_7
    move-exception v0

    move-object/from16 v17, v3

    move v15, v6

    goto/16 :goto_c

    :catch_8
    move-exception v0

    move-object/from16 v17, v3

    move v15, v6

    goto/16 :goto_d

    :catch_9
    move-exception v0

    move-object/from16 v17, v3

    move v15, v6

    goto/16 :goto_e

    :pswitch_5
    iget-object v3, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$2:Ljava/lang/Object;

    check-cast v3, Lcom/squareup/cardreader/ble/ConnectionError;

    iget-object v4, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$1:Ljava/lang/Object;

    check-cast v4, Lcom/squareup/blecoroutines/BleError;

    iget v5, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->I$0:I

    iget-object v6, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$0:Ljava/lang/Object;

    check-cast v6, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;

    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    move-object v7, v6

    goto :goto_1

    :pswitch_6
    invoke-static {v0}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 127
    move-object v0, v13

    check-cast v0, Lcom/squareup/blecoroutines/BleError;

    .line 128
    move-object v3, v13

    check-cast v3, Lcom/squareup/cardreader/ble/ConnectionError;

    .line 130
    iput-object v1, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$0:Ljava/lang/Object;

    iput v11, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->I$0:I

    iput-object v0, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$1:Ljava/lang/Object;

    iput-object v3, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$2:Ljava/lang/Object;

    iput v12, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->label:I

    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->readRssiUntilCancelled(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v4

    if-ne v4, v10, :cond_1

    return-object v10

    :cond_1
    move-object v7, v1

    const/4 v5, 0x0

    move-object/from16 v28, v4

    move-object v4, v0

    move-object/from16 v0, v28

    .line 125
    :goto_1
    move-object v6, v0

    check-cast v6, Lkotlinx/coroutines/Deferred;

    .line 135
    :try_start_5
    invoke-virtual {v7}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/RealConnectionEvents;

    move-result-object v0

    new-instance v8, Lcom/squareup/cardreader/ble/ConnectionState$Connecting;

    iget-boolean v9, v7, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->autoConnect:Z

    invoke-direct {v8, v9}, Lcom/squareup/cardreader/ble/ConnectionState$Connecting;-><init>(Z)V

    check-cast v8, Lcom/squareup/cardreader/ble/ConnectionState;

    invoke-virtual {v0, v8}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishState$impl_release(Lcom/squareup/cardreader/ble/ConnectionState;)V

    .line 136
    iput-object v7, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$0:Ljava/lang/Object;

    iput v5, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->I$0:I

    iput-object v4, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$1:Ljava/lang/Object;

    iput-object v3, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$2:Ljava/lang/Object;

    iput-object v6, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$3:Ljava/lang/Object;

    const/4 v0, 0x2

    iput v0, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->label:I

    invoke-virtual {v7, v2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->establishConnection(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v0
    :try_end_5
    .catch Lcom/squareup/blecoroutines/BleError; {:try_start_5 .. :try_end_5} :catch_1e
    .catch Lcom/squareup/cardreader/ble/ConnectionError; {:try_start_5 .. :try_end_5} :catch_1d
    .catch Ljava/util/concurrent/CancellationException; {:try_start_5 .. :try_end_5} :catch_1c

    if-ne v0, v10, :cond_2

    return-object v10

    :cond_2
    move-object v8, v3

    move-object v9, v4

    move v15, v5

    move-object v14, v7

    move-object v7, v6

    .line 125
    :goto_2
    :try_start_6
    check-cast v0, Lcom/squareup/blecoroutines/Connection;

    .line 138
    iget-object v3, v14, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->negotiator:Lcom/squareup/cardreader/ble/ConnectionNegotiator;

    iget-object v4, v14, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v14}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/RealConnectionEvents;

    move-result-object v5

    iget-object v6, v14, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->context:Landroid/content/Context;

    iget-object v11, v14, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->executionEnv:Lcom/squareup/cardreader/ble/Timeouts;

    iput-object v14, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$0:Ljava/lang/Object;

    iput v15, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->I$0:I

    iput-object v9, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$1:Ljava/lang/Object;

    iput-object v8, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$2:Ljava/lang/Object;

    iput-object v7, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$3:Ljava/lang/Object;

    iput-object v0, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$4:Ljava/lang/Object;

    const/4 v13, 0x3

    iput v13, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->label:I
    :try_end_6
    .catch Lcom/squareup/blecoroutines/BleError; {:try_start_6 .. :try_end_6} :catch_1b
    .catch Lcom/squareup/cardreader/ble/ConnectionError; {:try_start_6 .. :try_end_6} :catch_1a
    .catch Ljava/util/concurrent/CancellationException; {:try_start_6 .. :try_end_6} :catch_19

    move-object v13, v6

    move-object v6, v0

    move-object/from16 v17, v7

    move-object v7, v13

    move-object v13, v8

    move-object v8, v11

    move-object v11, v9

    move-object v9, v2

    :try_start_7
    invoke-interface/range {v3 .. v9}, Lcom/squareup/cardreader/ble/ConnectionNegotiator;->negotiateConnection(Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lcom/squareup/blecoroutines/Connection;Landroid/content/Context;Lcom/squareup/cardreader/ble/Timeouts;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v3
    :try_end_7
    .catch Lcom/squareup/blecoroutines/BleError; {:try_start_7 .. :try_end_7} :catch_18
    .catch Lcom/squareup/cardreader/ble/ConnectionError; {:try_start_7 .. :try_end_7} :catch_17
    .catch Ljava/util/concurrent/CancellationException; {:try_start_7 .. :try_end_7} :catch_16

    if-ne v3, v10, :cond_3

    return-object v10

    :cond_3
    move-object v6, v0

    move-object v0, v3

    move-object v5, v11

    move-object v4, v13

    move-object v7, v14

    move-object/from16 v3, v17

    .line 137
    :goto_3
    :try_start_8
    check-cast v0, Lcom/squareup/cardreader/ble/NegotiatedConnection;

    .line 140
    invoke-virtual {v7}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/RealConnectionEvents;

    move-result-object v8

    new-instance v9, Lcom/squareup/cardreader/ble/ConnectionState$Connected;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/NegotiatedConnection;->getCommsVersion()[B

    move-result-object v11

    invoke-direct {v9, v11}, Lcom/squareup/cardreader/ble/ConnectionState$Connected;-><init>([B)V

    check-cast v9, Lcom/squareup/cardreader/ble/ConnectionState;

    invoke-virtual {v8, v9}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishState$impl_release(Lcom/squareup/cardreader/ble/ConnectionState;)V

    .line 141
    invoke-virtual {v7}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/RealConnectionEvents;

    move-result-object v8

    sget-object v9, Lcom/squareup/cardreader/ble/R12State;->READY:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v8, v9}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishLoggingState$impl_release(Lcom/squareup/cardreader/ble/R12State;)V
    :try_end_8
    .catch Lcom/squareup/blecoroutines/BleError; {:try_start_8 .. :try_end_8} :catch_15
    .catch Lcom/squareup/cardreader/ble/ConnectionError; {:try_start_8 .. :try_end_8} :catch_14
    .catch Ljava/util/concurrent/CancellationException; {:try_start_8 .. :try_end_8} :catch_13

    .line 146
    :try_start_9
    iput-object v7, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$0:Ljava/lang/Object;

    iput v12, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->I$0:I

    iput-object v5, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$1:Ljava/lang/Object;

    iput-object v4, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$2:Ljava/lang/Object;

    iput-object v3, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$3:Ljava/lang/Object;

    iput-object v6, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$4:Ljava/lang/Object;

    iput-object v0, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$5:Ljava/lang/Object;

    const/4 v8, 0x4

    iput v8, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->label:I

    invoke-virtual {v7, v2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->readConnectionIntervalAfterDelay(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v8
    :try_end_9
    .catch Lcom/squareup/blecoroutines/BleError; {:try_start_9 .. :try_end_9} :catch_12
    .catch Lcom/squareup/cardreader/ble/ConnectionError; {:try_start_9 .. :try_end_9} :catch_11
    .catch Ljava/util/concurrent/CancellationException; {:try_start_9 .. :try_end_9} :catch_10

    if-ne v8, v10, :cond_4

    return-object v10

    :cond_4
    move-object v9, v3

    move-object v15, v4

    move-object v14, v5

    move-object v4, v6

    move-object v11, v7

    const/4 v13, 0x1

    move-object v5, v0

    .line 148
    :goto_4
    :try_start_a
    iget-object v3, v11, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->connectionManager:Lcom/squareup/cardreader/ble/ConnectionManager;

    .line 149
    invoke-virtual {v11}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/RealConnectionEvents;

    move-result-object v6

    iget-object v7, v11, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->sendChannel:Lkotlinx/coroutines/channels/Channel;

    iget-object v8, v11, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->executionEnv:Lcom/squareup/cardreader/ble/Timeouts;

    iput-object v11, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$0:Ljava/lang/Object;

    iput v13, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->I$0:I

    iput-object v14, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$1:Ljava/lang/Object;

    iput-object v15, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$2:Ljava/lang/Object;

    iput-object v9, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$3:Ljava/lang/Object;

    iput-object v4, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$4:Ljava/lang/Object;

    iput-object v5, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$5:Ljava/lang/Object;

    const/4 v0, 0x5

    iput v0, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->label:I
    :try_end_a
    .catch Lcom/squareup/blecoroutines/BleError; {:try_start_a .. :try_end_a} :catch_f
    .catch Lcom/squareup/cardreader/ble/ConnectionError; {:try_start_a .. :try_end_a} :catch_e
    .catch Ljava/util/concurrent/CancellationException; {:try_start_a .. :try_end_a} :catch_d

    move-object/from16 v17, v9

    move-object v9, v2

    :try_start_b
    invoke-interface/range {v3 .. v9}, Lcom/squareup/cardreader/ble/ConnectionManager;->manageConnection(Lcom/squareup/blecoroutines/Connection;Lcom/squareup/cardreader/ble/NegotiatedConnection;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lkotlinx/coroutines/channels/Channel;Lcom/squareup/cardreader/ble/Timeouts;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v0
    :try_end_b
    .catch Lcom/squareup/blecoroutines/BleError; {:try_start_b .. :try_end_b} :catch_c
    .catch Lcom/squareup/cardreader/ble/ConnectionError; {:try_start_b .. :try_end_b} :catch_b
    .catch Ljava/util/concurrent/CancellationException; {:try_start_b .. :try_end_b} :catch_a

    if-ne v0, v10, :cond_5

    return-object v10

    :cond_5
    move-object v7, v11

    move v6, v13

    :goto_5
    move v8, v6

    move-object v0, v14

    move-object v6, v15

    goto/16 :goto_11

    :catch_a
    move-exception v0

    goto :goto_6

    :catch_b
    move-exception v0

    goto :goto_7

    :catch_c
    move-exception v0

    goto :goto_8

    :catch_d
    move-exception v0

    move-object/from16 v17, v9

    :goto_6
    move-object v7, v11

    move-object v4, v15

    move v15, v13

    goto/16 :goto_c

    :catch_e
    move-exception v0

    move-object/from16 v17, v9

    :goto_7
    move-object v7, v11

    move v15, v13

    move-object v5, v14

    goto/16 :goto_d

    :catch_f
    move-exception v0

    move-object/from16 v17, v9

    :goto_8
    move-object v7, v11

    move-object v4, v15

    move v15, v13

    goto/16 :goto_e

    :catch_10
    move-exception v0

    move-object/from16 v17, v3

    const/4 v15, 0x1

    goto :goto_c

    :catch_11
    move-exception v0

    move-object/from16 v17, v3

    const/4 v15, 0x1

    goto/16 :goto_d

    :catch_12
    move-exception v0

    move-object/from16 v17, v3

    const/4 v15, 0x1

    goto/16 :goto_e

    :catch_13
    move-exception v0

    move-object/from16 v17, v3

    goto :goto_c

    :catch_14
    move-exception v0

    move-object/from16 v17, v3

    goto :goto_d

    :catch_15
    move-exception v0

    move-object/from16 v17, v3

    goto/16 :goto_e

    :catch_16
    move-exception v0

    goto :goto_9

    :catch_17
    move-exception v0

    goto :goto_a

    :catch_18
    move-exception v0

    goto :goto_b

    :catch_19
    move-exception v0

    move-object/from16 v17, v7

    move-object v13, v8

    :goto_9
    move-object v4, v13

    move-object v7, v14

    goto :goto_c

    :catch_1a
    move-exception v0

    move-object/from16 v17, v7

    move-object v11, v9

    :goto_a
    move-object v5, v11

    move-object v7, v14

    goto :goto_d

    :catch_1b
    move-exception v0

    move-object/from16 v17, v7

    move-object v13, v8

    :goto_b
    move-object v4, v13

    move-object v7, v14

    goto :goto_e

    :catch_1c
    move-exception v0

    move-object v4, v3

    move v15, v5

    move-object/from16 v17, v6

    .line 157
    :goto_c
    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Ltimber/log/Timber;->i(Ljava/lang/Throwable;)V

    .line 158
    new-instance v0, Lcom/squareup/blecoroutines/BleError;

    sget-object v19, Lcom/squareup/blecoroutines/Event;->TIMEOUT:Lcom/squareup/blecoroutines/Event;

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x7e

    const/16 v27, 0x0

    move-object/from16 v18, v0

    invoke-direct/range {v18 .. v27}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_f

    :catch_1d
    move-exception v0

    move v15, v5

    move-object/from16 v17, v6

    move-object v5, v4

    .line 154
    :goto_d
    move-object v3, v0

    check-cast v3, Ljava/lang/Throwable;

    invoke-static {v3}, Ltimber/log/Timber;->i(Ljava/lang/Throwable;)V

    move-object v6, v0

    move-object v0, v5

    goto :goto_10

    :catch_1e
    move-exception v0

    move-object v4, v3

    move v15, v5

    move-object/from16 v17, v6

    .line 151
    :goto_e
    move-object v3, v0

    check-cast v3, Ljava/lang/Throwable;

    invoke-static {v3}, Ltimber/log/Timber;->i(Ljava/lang/Throwable;)V

    :goto_f
    move-object v6, v4

    :goto_10
    move v8, v15

    :goto_11
    move-object/from16 v5, v17

    .line 161
    invoke-virtual {v7}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/RealConnectionEvents;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->getLastLoggingState$impl_release()Lcom/squareup/cardreader/ble/R12State;

    move-result-object v4

    const/4 v3, 0x0

    .line 165
    move-object v9, v3

    check-cast v9, Ljava/lang/Integer;

    .line 167
    :try_start_c
    invoke-virtual {v7}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/RealConnectionEvents;

    move-result-object v3

    sget-object v11, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_DISCONNECTION:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v3, v11}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishLoggingState$impl_release(Lcom/squareup/cardreader/ble/R12State;)V

    .line 168
    iget-object v3, v7, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->connectable:Lcom/squareup/blecoroutines/Connectable;

    iget-object v11, v7, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->executionEnv:Lcom/squareup/cardreader/ble/Timeouts;

    invoke-virtual {v11}, Lcom/squareup/cardreader/ble/Timeouts;->getDisconnectionTimeoutMs()J

    move-result-wide v13

    iput-object v7, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$0:Ljava/lang/Object;

    iput v8, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->I$0:I

    iput-object v0, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$1:Ljava/lang/Object;

    iput-object v6, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$2:Ljava/lang/Object;

    iput-object v5, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$3:Ljava/lang/Object;

    iput-object v4, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$4:Ljava/lang/Object;

    iput-object v9, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->L$5:Ljava/lang/Object;

    const/4 v11, 0x6

    iput v11, v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$connectBlocking$1;->label:I

    invoke-interface {v3, v13, v14, v2}, Lcom/squareup/blecoroutines/Connectable;->disconnectWithTimeout(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v2
    :try_end_c
    .catch Ljava/util/concurrent/CancellationException; {:try_start_c .. :try_end_c} :catch_1f

    if-ne v2, v10, :cond_6

    return-object v10

    :cond_6
    move-object v3, v9

    move-object/from16 v28, v7

    move-object v7, v0

    move-object v0, v2

    move-object/from16 v2, v28

    :goto_12
    :try_start_d
    check-cast v0, Ljava/lang/Integer;
    :try_end_d
    .catch Ljava/util/concurrent/CancellationException; {:try_start_d .. :try_end_d} :catch_0

    move-object/from16 v17, v0

    move-object/from16 v18, v4

    move-object/from16 v16, v6

    move-object v15, v7

    const/4 v3, 0x0

    goto :goto_14

    :catch_1f
    move-object v2, v7

    const/4 v3, 0x0

    move-object v7, v0

    move-object v0, v9

    :goto_13
    new-array v9, v3, [Ljava/lang/Object;

    const-string v10, "Timed out waiting for disconnection"

    .line 170
    invoke-static {v10, v9}, Ltimber/log/Timber;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object/from16 v17, v0

    move-object/from16 v18, v4

    move-object/from16 v16, v6

    move-object v15, v7

    :goto_14
    const/4 v4, 0x0

    .line 174
    invoke-static {v5, v4, v12, v4}, Lkotlinx/coroutines/Job$DefaultImpls;->cancel$default(Lkotlinx/coroutines/Job;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    .line 176
    invoke-virtual {v2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/RealConnectionEvents;

    move-result-object v0

    sget-object v4, Lcom/squareup/cardreader/ble/R12State;->DISCONNECTED:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v0, v4}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishLoggingState$impl_release(Lcom/squareup/cardreader/ble/R12State;)V

    .line 177
    invoke-virtual {v2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/RealConnectionEvents;

    move-result-object v0

    .line 178
    new-instance v2, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

    if-nez v8, :cond_7

    const/4 v14, 0x1

    goto :goto_15

    :cond_7
    const/4 v14, 0x0

    :goto_15
    move-object v13, v2

    invoke-direct/range {v13 .. v18}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;-><init>(ZLcom/squareup/blecoroutines/BleError;Lcom/squareup/cardreader/ble/ConnectionError;Ljava/lang/Integer;Lcom/squareup/cardreader/ble/R12State;)V

    check-cast v2, Lcom/squareup/cardreader/ble/ConnectionState;

    .line 177
    invoke-virtual {v0, v2}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishState$impl_release(Lcom/squareup/cardreader/ble/ConnectionState;)V

    .line 184
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public disconnect()V
    .locals 6

    .line 91
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->scope:Lkotlinx/coroutines/CoroutineScope;

    new-instance v1, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$disconnect$1;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$disconnect$1;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;Lkotlin/coroutines/Continuation;)V

    move-object v3, v1

    check-cast v3, Lkotlin/jvm/functions/Function2;

    const/4 v1, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->launch$default(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    return-void
.end method

.method final synthetic establishConnection(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/squareup/blecoroutines/Connection;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "connectGatt autoConnect = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->autoConnect:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 188
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/RealConnectionEvents;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_CONNECTION_TO_READER:Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishLoggingState$impl_release(Lcom/squareup/cardreader/ble/R12State;)V

    .line 189
    iget-boolean v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->autoConnect:Z

    if-eqz v0, :cond_0

    .line 190
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->connectable:Lcom/squareup/blecoroutines/Connectable;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->context:Landroid/content/Context;

    invoke-interface {v1, v2, v0, p1}, Lcom/squareup/blecoroutines/Connectable;->connectGatt(Landroid/content/Context;ZLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->executionEnv:Lcom/squareup/cardreader/ble/Timeouts;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/Timeouts;->getConnectionTimeoutMs()J

    move-result-wide v0

    new-instance v2, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$establishConnection$2;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$establishConnection$2;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-static {v0, v1, v2, p1}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public forgetBond()V
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->sendChannel:Lkotlinx/coroutines/channels/Channel;

    new-instance v1, Lcom/squareup/cardreader/ble/SendMessage$ForgetBond;

    invoke-direct {v1}, Lcom/squareup/cardreader/ble/SendMessage$ForgetBond;-><init>()V

    invoke-interface {v0, v1}, Lkotlinx/coroutines/channels/Channel;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method public bridge synthetic getEvents()Lcom/squareup/cardreader/ble/ConnectionEvents;
    .locals 1

    .line 45
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/RealConnectionEvents;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ble/ConnectionEvents;

    return-object v0
.end method

.method public getEvents()Lcom/squareup/cardreader/ble/RealConnectionEvents;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->events:Lcom/squareup/cardreader/ble/RealConnectionEvents;

    return-object v0
.end method

.method public readAckVector()Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 69
    invoke-static {v0, v1, v0}, Lkotlinx/coroutines/CompletableDeferredKt;->CompletableDeferred$default(Lkotlinx/coroutines/Job;ILjava/lang/Object;)Lkotlinx/coroutines/CompletableDeferred;

    move-result-object v1

    .line 70
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->sendChannel:Lkotlinx/coroutines/channels/Channel;

    new-instance v3, Lcom/squareup/cardreader/ble/SendMessage$ReadAckVector;

    invoke-direct {v3, v1}, Lcom/squareup/cardreader/ble/SendMessage$ReadAckVector;-><init>(Lkotlinx/coroutines/CompletableDeferred;)V

    invoke-interface {v2, v3}, Lkotlinx/coroutines/channels/Channel;->offer(Ljava/lang/Object;)Z

    .line 71
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->backgroundDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    check-cast v2, Lkotlin/coroutines/CoroutineContext;

    new-instance v3, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readAckVector$1;

    invoke-direct {v3, v1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readAckVector$1;-><init>(Lkotlinx/coroutines/CompletableDeferred;Lkotlin/coroutines/Continuation;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-static {v2, v3}, Lkotlinx/coroutines/rx2/RxSingleKt;->rxSingle(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method final synthetic readConnectionIntervalAfterDelay(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlinx/coroutines/Deferred<",
            "Lkotlin/Unit;",
            ">;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->scope:Lkotlinx/coroutines/CoroutineScope;

    new-instance p1, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readConnectionIntervalAfterDelay$2;

    const/4 v1, 0x0

    invoke-direct {p1, p0, v1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readConnectionIntervalAfterDelay$2;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;Lkotlin/coroutines/Continuation;)V

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function2;

    const/4 v2, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->async$default(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Deferred;

    move-result-object p1

    return-object p1
.end method

.method public readMtu()Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 75
    invoke-static {v0, v1, v0}, Lkotlinx/coroutines/CompletableDeferredKt;->CompletableDeferred$default(Lkotlinx/coroutines/Job;ILjava/lang/Object;)Lkotlinx/coroutines/CompletableDeferred;

    move-result-object v1

    .line 76
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->sendChannel:Lkotlinx/coroutines/channels/Channel;

    new-instance v3, Lcom/squareup/cardreader/ble/SendMessage$ReadMtu;

    invoke-direct {v3, v1}, Lcom/squareup/cardreader/ble/SendMessage$ReadMtu;-><init>(Lkotlinx/coroutines/CompletableDeferred;)V

    invoke-interface {v2, v3}, Lkotlinx/coroutines/channels/Channel;->offer(Ljava/lang/Object;)Z

    .line 77
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->backgroundDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    check-cast v2, Lkotlin/coroutines/CoroutineContext;

    new-instance v3, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readMtu$1;

    invoke-direct {v3, v1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readMtu$1;-><init>(Lkotlinx/coroutines/CompletableDeferred;Lkotlin/coroutines/Continuation;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-static {v2, v3}, Lkotlinx/coroutines/rx2/RxSingleKt;->rxSingle(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method final synthetic readRssiUntilCancelled(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlinx/coroutines/Deferred<",
            "Lkotlin/Unit;",
            ">;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 111
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->scope:Lkotlinx/coroutines/CoroutineScope;

    new-instance p1, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;

    const/4 v1, 0x0

    invoke-direct {p1, p0, v1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;Lkotlin/coroutines/Continuation;)V

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function2;

    const/4 v2, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->async$default(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Deferred;

    move-result-object p1

    return-object p1
.end method

.method public writeData([B)V
    .locals 2

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->sendChannel:Lkotlinx/coroutines/channels/Channel;

    new-instance v1, Lcom/squareup/cardreader/ble/SendMessage$WriteData;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ble/SendMessage$WriteData;-><init>([B)V

    invoke-interface {v0, v1}, Lkotlinx/coroutines/channels/Channel;->offer(Ljava/lang/Object;)Z

    return-void
.end method
