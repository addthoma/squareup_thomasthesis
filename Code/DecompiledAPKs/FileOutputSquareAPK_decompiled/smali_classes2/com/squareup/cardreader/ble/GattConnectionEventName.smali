.class public final enum Lcom/squareup/cardreader/ble/GattConnectionEventName;
.super Ljava/lang/Enum;
.source "GattConnectionEventName.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/ble/GattConnectionEventName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum CHARACTERISTIC_CHANGED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum CHARACTERISTIC_GET_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum CHARACTERISTIC_READ:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum CHARACTERISTIC_READ_INIT_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum CHARACTERISTIC_SET_NOTIFICATION_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum CHARACTERISTIC_SET_VALUE_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum CHARACTERISTIC_UNKNOWN:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum CHARACTERISTIC_WRITE:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum CHARACTERISTIC_WRITE_INIT_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum CONNECTION_STATE_CHANGED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum DESCRIPTOR_GET_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum DESCRIPTOR_SET_VALUE_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum DESCRIPTOR_WRITE:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum DESCRIPTOR_WRITE_INIT_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum MTU_CHANGED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum SERVICES_DISCOVERED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum SERVICE_DISCOVERY_FAILED_CACHED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum SERVICE_DISCOVERY_FAILED_UNKNOWN:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum SERVICE_MISSING_CHARACTERISTIC:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum SERVICE_NOT_SUPPORTED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public static final enum WRONG_DEVICE_IN_GATT_CALLBACK:Lcom/squareup/cardreader/ble/GattConnectionEventName;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 4
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/4 v1, 0x0

    const-string v2, "CONNECTION_STATE_CHANGED"

    const-string v3, "Connection state changed"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CONNECTION_STATE_CHANGED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 5
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/4 v2, 0x1

    const-string v3, "SERVICES_DISCOVERED"

    const-string v4, "Services discovered"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->SERVICES_DISCOVERED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 6
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/4 v3, 0x2

    const-string v4, "SERVICE_DISCOVERY_FAILED_CACHED"

    const-string v5, "Service discovery was cached; failed"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->SERVICE_DISCOVERY_FAILED_CACHED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 7
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/4 v4, 0x3

    const-string v5, "SERVICE_DISCOVERY_FAILED_UNKNOWN"

    const-string v6, "Service discovery failed for an unknown reason"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->SERVICE_DISCOVERY_FAILED_UNKNOWN:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 8
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/4 v5, 0x4

    const-string v6, "SERVICE_MISSING_CHARACTERISTIC"

    const-string v7, "Characteristic not found in cached service"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->SERVICE_MISSING_CHARACTERISTIC:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 9
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/4 v6, 0x5

    const-string v7, "SERVICE_NOT_SUPPORTED"

    const-string v8, "Service not supported by remote device"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->SERVICE_NOT_SUPPORTED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 10
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/4 v7, 0x6

    const-string v8, "CHARACTERISTIC_GET_FAILED"

    const-string v9, "Failed to get characteristic"

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_GET_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 11
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/4 v8, 0x7

    const-string v9, "CHARACTERISTIC_SET_NOTIFICATION_FAILED"

    const-string v10, "Characteristic set notification failed"

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_SET_NOTIFICATION_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 12
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/16 v9, 0x8

    const-string v10, "CHARACTERISTIC_READ_INIT_FAILED"

    const-string v11, "Failed to initiate characteristic read"

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_READ_INIT_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/16 v10, 0x9

    const-string v11, "CHARACTERISTIC_READ"

    const-string v12, "Characteristic read"

    invoke-direct {v0, v11, v10, v12}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_READ:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/16 v11, 0xa

    const-string v12, "CHARACTERISTIC_SET_VALUE_FAILED"

    const-string v13, "Characteristic set value (local) failed"

    invoke-direct {v0, v12, v11, v13}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_SET_VALUE_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/16 v12, 0xb

    const-string v13, "CHARACTERISTIC_WRITE_INIT_FAILED"

    const-string v14, "Failed to initiate characteristic write"

    invoke-direct {v0, v13, v12, v14}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_WRITE_INIT_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/16 v13, 0xc

    const-string v14, "CHARACTERISTIC_WRITE"

    const-string v15, "Characteristic write"

    invoke-direct {v0, v14, v13, v15}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_WRITE:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/16 v14, 0xd

    const-string v15, "CHARACTERISTIC_CHANGED"

    const-string v13, "Characteristic changed"

    invoke-direct {v0, v15, v14, v13}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_CHANGED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/16 v13, 0xe

    const-string v15, "CHARACTERISTIC_UNKNOWN"

    const-string v14, "Received onCharacteristicRead on an unknown characteristic"

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_UNKNOWN:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const-string v14, "DESCRIPTOR_GET_FAILED"

    const/16 v15, 0xf

    const-string v13, "Failed to get descriptor"

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->DESCRIPTOR_GET_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 20
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const-string v13, "DESCRIPTOR_SET_VALUE_FAILED"

    const/16 v14, 0x10

    const-string v15, "Descriptor set value (local) failed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->DESCRIPTOR_SET_VALUE_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 21
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const-string v13, "DESCRIPTOR_WRITE_INIT_FAILED"

    const/16 v14, 0x11

    const-string v15, "Descriptor write failed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->DESCRIPTOR_WRITE_INIT_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 22
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const-string v13, "DESCRIPTOR_WRITE"

    const/16 v14, 0x12

    const-string v15, "Descriptor write"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->DESCRIPTOR_WRITE:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 23
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const-string v13, "MTU_CHANGED"

    const/16 v14, 0x13

    const-string v15, "MTU changed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->MTU_CHANGED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 24
    new-instance v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const-string v13, "WRONG_DEVICE_IN_GATT_CALLBACK"

    const/16 v14, 0x14

    const-string v15, "Wrong device in GATT callback"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/ble/GattConnectionEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->WRONG_DEVICE_IN_GATT_CALLBACK:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/16 v0, 0x15

    new-array v0, v0, [Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 3
    sget-object v13, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CONNECTION_STATE_CHANGED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->SERVICES_DISCOVERED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->SERVICE_DISCOVERY_FAILED_CACHED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->SERVICE_DISCOVERY_FAILED_UNKNOWN:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->SERVICE_MISSING_CHARACTERISTIC:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->SERVICE_NOT_SUPPORTED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_GET_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_SET_NOTIFICATION_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_READ_INIT_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_READ:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_SET_VALUE_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_WRITE_INIT_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_WRITE:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_CHANGED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_UNKNOWN:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->DESCRIPTOR_GET_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->DESCRIPTOR_SET_VALUE_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->DESCRIPTOR_WRITE_INIT_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->DESCRIPTOR_WRITE:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->MTU_CHANGED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->WRONG_DEVICE_IN_GATT_CALLBACK:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->$VALUES:[Lcom/squareup/cardreader/ble/GattConnectionEventName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    iput-object p3, p0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->name:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/ble/GattConnectionEventName;
    .locals 1

    .line 3
    const-class v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ble/GattConnectionEventName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/ble/GattConnectionEventName;
    .locals 1

    .line 3
    sget-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->$VALUES:[Lcom/squareup/cardreader/ble/GattConnectionEventName;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/ble/GattConnectionEventName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/ble/GattConnectionEventName;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->name:Ljava/lang/String;

    return-object v0
.end method
