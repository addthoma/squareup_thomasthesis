.class public interface abstract Lcom/squareup/cardreader/ble/SystemBleScanner;
.super Ljava/lang/Object;
.source "SystemBleScanner.java"


# static fields
.field public static final NO_OP:Lcom/squareup/cardreader/ble/SystemBleScanner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/cardreader/ble/SystemBleScanner$1;

    invoke-direct {v0}, Lcom/squareup/cardreader/ble/SystemBleScanner$1;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/ble/SystemBleScanner;->NO_OP:Lcom/squareup/cardreader/ble/SystemBleScanner;

    return-void
.end method


# virtual methods
.method public abstract startScan()V
.end method

.method public abstract stopScan()V
.end method
