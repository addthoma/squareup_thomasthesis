.class public final Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;
.super Ljava/lang/Object;
.source "BleDeviceModule_ProvideBleBackendListenerV2Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/ble/BleBackendListenerV2;",
        ">;"
    }
.end annotation


# instance fields
.field private final bleBackendProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private final bleConnectionStateMachineV2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/StateMachineV2;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/cardreader/ble/BleDeviceModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/BleDeviceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/BleDeviceModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/StateMachineV2;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;->module:Lcom/squareup/cardreader/ble/BleDeviceModule;

    .line 43
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;->bleConnectionStateMachineV2Provider:Ljavax/inject/Provider;

    .line 44
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;->bleBackendProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p4, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p5, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p6, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;->cardReaderIdProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p7, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/ble/BleDeviceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/BleDeviceModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/StateMachineV2;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;)",
            "Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;"
        }
    .end annotation

    .line 63
    new-instance v8, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;-><init>(Lcom/squareup/cardreader/ble/BleDeviceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static provideBleBackendListenerV2(Lcom/squareup/cardreader/ble/BleDeviceModule;Lcom/squareup/cardreader/ble/StateMachineV2;Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/cardreader/ble/BleBackendListenerV2;
    .locals 0

    .line 70
    invoke-virtual/range {p0 .. p6}, Lcom/squareup/cardreader/ble/BleDeviceModule;->provideBleBackendListenerV2(Lcom/squareup/cardreader/ble/StateMachineV2;Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/ble/BleBackendListenerV2;
    .locals 7

    .line 53
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;->module:Lcom/squareup/cardreader/ble/BleDeviceModule;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;->bleConnectionStateMachineV2Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/ble/StateMachineV2;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;->bleBackendProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/ble/BleBackendLegacy;

    iget-object v3, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/RealCardReaderListeners;

    iget-object v4, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardreader/CardReaderFactory;

    iget-object v5, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;->cardReaderIdProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/cardreader/CardReaderId;

    iget-object v6, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/cardreader/BluetoothUtils;

    invoke-static/range {v0 .. v6}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;->provideBleBackendListenerV2(Lcom/squareup/cardreader/ble/BleDeviceModule;Lcom/squareup/cardreader/ble/StateMachineV2;Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerV2Factory;->get()Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    move-result-object v0

    return-object v0
.end method
