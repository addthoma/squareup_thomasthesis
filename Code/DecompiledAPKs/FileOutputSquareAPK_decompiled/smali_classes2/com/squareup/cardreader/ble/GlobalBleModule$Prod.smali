.class public Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;
.super Ljava/lang/Object;
.source "GlobalBleModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/blescan/BluetoothModule$Prod;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/GlobalBleModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Prod"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideBluetoothDevicesCountInitializer(Landroid/app/Application;Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/logging/BluetoothDevicesCountInitializer;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ")",
            "Lcom/squareup/logging/BluetoothDevicesCountInitializer;"
        }
    .end annotation

    .line 66
    invoke-static {p1, p2, p3, p4}, Lcom/squareup/cardreader/ble/GlobalBleModule;->getRealBluetoothDevicesCountInitializer(Landroid/app/Application;Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/logging/BluetoothDevicesCountInitializer;

    move-result-object p1

    return-object p1
.end method

.method provideSystemBleScanner(Lcom/squareup/cardreader/BluetoothUtils;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/ble/BleScanFilter;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/ble/SystemBleScanner;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/le/BluetoothLeScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanner;",
            ">;",
            "Lcom/squareup/cardreader/ble/BleScanFilter;",
            "Lcom/squareup/thread/executor/MainThread;",
            ")",
            "Lcom/squareup/cardreader/ble/SystemBleScanner;"
        }
    .end annotation

    .line 59
    invoke-static/range {p1 .. p6}, Lcom/squareup/cardreader/ble/GlobalBleModule;->getRealSystemBleScanner(Lcom/squareup/cardreader/BluetoothUtils;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/ble/BleScanFilter;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/ble/SystemBleScanner;

    move-result-object p1

    return-object p1
.end method

.method provideWirelessSearcher(Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;)Lcom/squareup/cardreader/WirelessSearcher;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    return-object p1
.end method
