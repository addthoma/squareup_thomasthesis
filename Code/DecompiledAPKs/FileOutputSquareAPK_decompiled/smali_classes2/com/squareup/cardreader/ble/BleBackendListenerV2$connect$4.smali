.class final synthetic Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$4;
.super Lkotlin/jvm/internal/FunctionReference;
.source "BleBackendListenerV2.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardreader/ble/BleBackendListenerV2;->connect()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cardreader/ble/R12State;",
        "Lcom/squareup/dipper/events/BleConnectionState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u0015\u0010\u0002\u001a\u00110\u0003\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u0006\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/dipper/events/BleConnectionState;",
        "p1",
        "Lcom/squareup/cardreader/ble/R12State;",
        "Lkotlin/ParameterName;",
        "name",
        "state",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$4;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$4;

    invoke-direct {v0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$4;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$4;->INSTANCE:Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$4;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/FunctionReference;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "toLegacyState"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 2

    const-class v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt;

    const-string v1, "dipper_release"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinPackage(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/reflect/KDeclarationContainer;

    move-result-object v0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "toLegacyState(Lcom/squareup/cardreader/ble/R12State;)Lcom/squareup/dipper/events/BleConnectionState;"

    return-object v0
.end method

.method public final invoke(Lcom/squareup/cardreader/ble/R12State;)Lcom/squareup/dipper/events/BleConnectionState;
    .locals 1

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-static {p1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt;->access$toLegacyState(Lcom/squareup/cardreader/ble/R12State;)Lcom/squareup/dipper/events/BleConnectionState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$4;->invoke(Lcom/squareup/cardreader/ble/R12State;)Lcom/squareup/dipper/events/BleConnectionState;

    move-result-object p1

    return-object p1
.end method
