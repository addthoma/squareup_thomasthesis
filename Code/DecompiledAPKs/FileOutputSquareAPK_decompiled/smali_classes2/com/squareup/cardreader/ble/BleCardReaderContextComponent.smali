.class public interface abstract Lcom/squareup/cardreader/ble/BleCardReaderContextComponent;
.super Ljava/lang/Object;
.source "BleCardReaderContextComponent.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderContextComponent;


# annotations
.annotation runtime Ldagger/Component;
    dependencies = {
        Lcom/squareup/cardreader/NonX2CardReaderContextParent;
    }
    modules = {
        Lcom/squareup/cardreader/ble/BleCardReaderContextComponent$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ble/BleCardReaderContextComponent$Module;
    }
.end annotation
