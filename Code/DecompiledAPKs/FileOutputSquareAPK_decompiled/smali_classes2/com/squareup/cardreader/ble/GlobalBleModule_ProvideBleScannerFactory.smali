.class public final Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;
.super Ljava/lang/Object;
.source "GlobalBleModule_ProvideBleScannerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/ble/BleScanner;",
        ">;"
    }
.end annotation


# instance fields
.field private final bleScanFilterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanFilter;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/cardreader/ble/GlobalBleModule;

.field private final systemBleScannerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/SystemBleScanner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/GlobalBleModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/SystemBleScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanFilter;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;->module:Lcom/squareup/cardreader/ble/GlobalBleModule;

    .line 34
    iput-object p2, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;->systemBleScannerProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;->bleScanFilterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/GlobalBleModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/SystemBleScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanFilter;",
            ">;)",
            "Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;"
        }
    .end annotation

    .line 49
    new-instance v6, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;-><init>(Lcom/squareup/cardreader/ble/GlobalBleModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static provideBleScanner(Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/ble/SystemBleScanner;Lcom/squareup/cardreader/ble/BleScanFilter;)Lcom/squareup/cardreader/ble/BleScanner;
    .locals 0

    .line 55
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/cardreader/ble/GlobalBleModule;->provideBleScanner(Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/ble/SystemBleScanner;Lcom/squareup/cardreader/ble/BleScanFilter;)Lcom/squareup/cardreader/ble/BleScanner;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ble/BleScanner;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/ble/BleScanner;
    .locals 5

    .line 42
    iget-object v0, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;->module:Lcom/squareup/cardreader/ble/GlobalBleModule;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/BluetoothUtils;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/thread/executor/MainThread;

    iget-object v3, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;->systemBleScannerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/ble/SystemBleScanner;

    iget-object v4, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;->bleScanFilterProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardreader/ble/BleScanFilter;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;->provideBleScanner(Lcom/squareup/cardreader/ble/GlobalBleModule;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/ble/SystemBleScanner;Lcom/squareup/cardreader/ble/BleScanFilter;)Lcom/squareup/cardreader/ble/BleScanner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleScannerFactory;->get()Lcom/squareup/cardreader/ble/BleScanner;

    move-result-object v0

    return-object v0
.end method
