.class public interface abstract Lcom/squareup/cardreader/ble/BleAction;
.super Ljava/lang/Object;
.source "BleAction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ble/BleAction$ReconnectDueToGattError;,
        Lcom/squareup/cardreader/ble/BleAction$NotificationEnabled;,
        Lcom/squareup/cardreader/ble/BleAction$ReceivedUnknownCharacteristicRead;,
        Lcom/squareup/cardreader/ble/BleAction$FailedDescriptorWrite;,
        Lcom/squareup/cardreader/ble/BleAction$FailedCharacteristicWrite;,
        Lcom/squareup/cardreader/ble/BleAction$FailedCharacteristicRead;,
        Lcom/squareup/cardreader/ble/BleAction$ReceivedReaderCommsVersion;,
        Lcom/squareup/cardreader/ble/BleAction$ReceivedConnectionInterval;,
        Lcom/squareup/cardreader/ble/BleAction$BondedWithReader;,
        Lcom/squareup/cardreader/ble/BleAction$UnableToCreateBond;,
        Lcom/squareup/cardreader/ble/BleAction$ReceivedBondState;,
        Lcom/squareup/cardreader/ble/BleAction$ReceivedSerialNumber;,
        Lcom/squareup/cardreader/ble/BleAction$ServiceCharacteristicVersion;,
        Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;,
        Lcom/squareup/cardreader/ble/BleAction$OldServicesCached;,
        Lcom/squareup/cardreader/ble/BleAction$DestroyReader;,
        Lcom/squareup/cardreader/ble/BleAction$DisconnectedAction;,
        Lcom/squareup/cardreader/ble/BleAction$ConnectedAction;,
        Lcom/squareup/cardreader/ble/BleAction$PairingTimeout;,
        Lcom/squareup/cardreader/ble/BleAction$InitializeBle;
    }
.end annotation


# virtual methods
.method public abstract describe()Ljava/lang/String;
.end method
