.class public final enum Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;
.super Ljava/lang/Enum;
.source "BleBackendListenerV2.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/BleBackendListenerV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NoReconnectReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0007\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;",
        "",
        "(Ljava/lang/String;I)V",
        "REQUESTED_DISCONNECTION",
        "PAIRING_MODE",
        "ERROR_BONDING",
        "SERVICE_VERSION",
        "BLUETOOTH_DISABLED",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

.field public static final enum BLUETOOTH_DISABLED:Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

.field public static final enum ERROR_BONDING:Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

.field public static final enum PAIRING_MODE:Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

.field public static final enum REQUESTED_DISCONNECTION:Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

.field public static final enum SERVICE_VERSION:Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    new-instance v1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    const/4 v2, 0x0

    const-string v3, "REQUESTED_DISCONNECTION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;->REQUESTED_DISCONNECTION:Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    const/4 v2, 0x1

    const-string v3, "PAIRING_MODE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;->PAIRING_MODE:Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    const/4 v2, 0x2

    const-string v3, "ERROR_BONDING"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;->ERROR_BONDING:Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    const/4 v2, 0x3

    const-string v3, "SERVICE_VERSION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;->SERVICE_VERSION:Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    const/4 v2, 0x4

    const-string v3, "BLUETOOTH_DISABLED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;->BLUETOOTH_DISABLED:Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;->$VALUES:[Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 195
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;
    .locals 1

    const-class v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;
    .locals 1

    sget-object v0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;->$VALUES:[Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;

    return-object v0
.end method
