.class public interface abstract Lcom/squareup/cardreader/ble/ConnectionNegotiator;
.super Ljava/lang/Object;
.source "ConnectionNegotiator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008`\u0018\u00002\u00020\u0001J9\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000e\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/cardreader/ble/ConnectionNegotiator;",
        "",
        "negotiateConnection",
        "Lcom/squareup/cardreader/ble/NegotiatedConnection;",
        "device",
        "Landroid/bluetooth/BluetoothDevice;",
        "events",
        "Lcom/squareup/cardreader/ble/RealConnectionEvents;",
        "connection",
        "Lcom/squareup/blecoroutines/Connection;",
        "context",
        "Landroid/content/Context;",
        "timeouts",
        "Lcom/squareup/cardreader/ble/Timeouts;",
        "(Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lcom/squareup/blecoroutines/Connection;Landroid/content/Context;Lcom/squareup/cardreader/ble/Timeouts;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract negotiateConnection(Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lcom/squareup/blecoroutines/Connection;Landroid/content/Context;Lcom/squareup/cardreader/ble/Timeouts;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/bluetooth/BluetoothDevice;",
            "Lcom/squareup/cardreader/ble/RealConnectionEvents;",
            "Lcom/squareup/blecoroutines/Connection;",
            "Landroid/content/Context;",
            "Lcom/squareup/cardreader/ble/Timeouts;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/squareup/cardreader/ble/NegotiatedConnection;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation
.end method
