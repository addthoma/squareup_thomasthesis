.class public final Lcom/squareup/cardreader/ble/NegotiatedConnection;
.super Ljava/lang/Object;
.source "ConnectionNegotiator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0008\u0000\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000c\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/cardreader/ble/NegotiatedConnection;",
        "",
        "commsVersion",
        "",
        "mtuChannel",
        "Lkotlinx/coroutines/channels/ReceiveChannel;",
        "Landroid/bluetooth/BluetoothGattCharacteristic;",
        "dataChannel",
        "([BLkotlinx/coroutines/channels/ReceiveChannel;Lkotlinx/coroutines/channels/ReceiveChannel;)V",
        "getCommsVersion",
        "()[B",
        "getDataChannel",
        "()Lkotlinx/coroutines/channels/ReceiveChannel;",
        "getMtuChannel",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final commsVersion:[B

.field private final dataChannel:Lkotlinx/coroutines/channels/ReceiveChannel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "Landroid/bluetooth/BluetoothGattCharacteristic;",
            ">;"
        }
    .end annotation
.end field

.field private final mtuChannel:Lkotlinx/coroutines/channels/ReceiveChannel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "Landroid/bluetooth/BluetoothGattCharacteristic;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([BLkotlinx/coroutines/channels/ReceiveChannel;Lkotlinx/coroutines/channels/ReceiveChannel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "+",
            "Landroid/bluetooth/BluetoothGattCharacteristic;",
            ">;",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "+",
            "Landroid/bluetooth/BluetoothGattCharacteristic;",
            ">;)V"
        }
    .end annotation

    const-string v0, "commsVersion"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mtuChannel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dataChannel"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/NegotiatedConnection;->commsVersion:[B

    iput-object p2, p0, Lcom/squareup/cardreader/ble/NegotiatedConnection;->mtuChannel:Lkotlinx/coroutines/channels/ReceiveChannel;

    iput-object p3, p0, Lcom/squareup/cardreader/ble/NegotiatedConnection;->dataChannel:Lkotlinx/coroutines/channels/ReceiveChannel;

    return-void
.end method


# virtual methods
.method public final getCommsVersion()[B
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/cardreader/ble/NegotiatedConnection;->commsVersion:[B

    return-object v0
.end method

.method public final getDataChannel()Lkotlinx/coroutines/channels/ReceiveChannel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "Landroid/bluetooth/BluetoothGattCharacteristic;",
            ">;"
        }
    .end annotation

    .line 161
    iget-object v0, p0, Lcom/squareup/cardreader/ble/NegotiatedConnection;->dataChannel:Lkotlinx/coroutines/channels/ReceiveChannel;

    return-object v0
.end method

.method public final getMtuChannel()Lkotlinx/coroutines/channels/ReceiveChannel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "Landroid/bluetooth/BluetoothGattCharacteristic;",
            ">;"
        }
    .end annotation

    .line 160
    iget-object v0, p0, Lcom/squareup/cardreader/ble/NegotiatedConnection;->mtuChannel:Lkotlinx/coroutines/channels/ReceiveChannel;

    return-object v0
.end method
