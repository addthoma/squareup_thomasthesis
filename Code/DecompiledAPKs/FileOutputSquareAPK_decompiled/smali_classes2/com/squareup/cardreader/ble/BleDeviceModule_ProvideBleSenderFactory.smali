.class public final Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleSenderFactory;
.super Ljava/lang/Object;
.source "BleDeviceModule_ProvideBleSenderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/ble/BleSender;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleSenderFactory;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleSenderFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleSenderFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleSenderFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;)",
            "Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleSenderFactory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleSenderFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleSenderFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideBleSender(Lcom/squareup/cardreader/CardReaderPauseAndResumer;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/cardreader/ble/BleSender;
    .locals 0

    .line 49
    invoke-static {p0, p1, p2}, Lcom/squareup/cardreader/ble/BleDeviceModule;->provideBleSender(Lcom/squareup/cardreader/CardReaderPauseAndResumer;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/cardreader/ble/BleSender;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ble/BleSender;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/ble/BleSender;
    .locals 3

    .line 37
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleSenderFactory;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleSenderFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/thread/executor/MainThread;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleSenderFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReaderListeners;

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleSenderFactory;->provideBleSender(Lcom/squareup/cardreader/CardReaderPauseAndResumer;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/cardreader/ble/BleSender;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleSenderFactory;->get()Lcom/squareup/cardreader/ble/BleSender;

    move-result-object v0

    return-object v0
.end method
