.class public final Lcom/squareup/cardreader/ble/Timeouts;
.super Ljava/lang/Object;
.source "StateMachineV2.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u000e\u0018\u00002\u00020\u0001BA\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000bR\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000b\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/cardreader/ble/Timeouts;",
        "",
        "disconnectionTimeoutMs",
        "",
        "connectionTimeoutMs",
        "defaultTimeoutMs",
        "bondingTimeoutMs",
        "setupBondingDelayMs",
        "rssiPeriodMs",
        "(JJJJJJ)V",
        "getBondingTimeoutMs",
        "()J",
        "getConnectionTimeoutMs",
        "getDefaultTimeoutMs",
        "getDisconnectionTimeoutMs",
        "getRssiPeriodMs",
        "getSetupBondingDelayMs",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bondingTimeoutMs:J

.field private final connectionTimeoutMs:J

.field private final defaultTimeoutMs:J

.field private final disconnectionTimeoutMs:J

.field private final rssiPeriodMs:J

.field private final setupBondingDelayMs:J


# direct methods
.method public constructor <init>()V
    .locals 15

    const-wide/16 v1, 0x0

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x0

    const-wide/16 v9, 0x0

    const-wide/16 v11, 0x0

    const/16 v13, 0x3f

    const/4 v14, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v14}, Lcom/squareup/cardreader/ble/Timeouts;-><init>(JJJJJJILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(JJJJJJ)V
    .locals 0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/squareup/cardreader/ble/Timeouts;->disconnectionTimeoutMs:J

    iput-wide p3, p0, Lcom/squareup/cardreader/ble/Timeouts;->connectionTimeoutMs:J

    iput-wide p5, p0, Lcom/squareup/cardreader/ble/Timeouts;->defaultTimeoutMs:J

    iput-wide p7, p0, Lcom/squareup/cardreader/ble/Timeouts;->bondingTimeoutMs:J

    iput-wide p9, p0, Lcom/squareup/cardreader/ble/Timeouts;->setupBondingDelayMs:J

    iput-wide p11, p0, Lcom/squareup/cardreader/ble/Timeouts;->rssiPeriodMs:J

    return-void
.end method

.method public synthetic constructor <init>(JJJJJJILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 13

    and-int/lit8 v0, p13, 0x1

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x2710

    goto :goto_0

    :cond_0
    move-wide v0, p1

    :goto_0
    and-int/lit8 v2, p13, 0x2

    if-eqz v2, :cond_1

    const-wide/16 v2, 0x7530

    goto :goto_1

    :cond_1
    move-wide/from16 v2, p3

    :goto_1
    and-int/lit8 v4, p13, 0x4

    const-wide/16 v5, 0x1f40

    if-eqz v4, :cond_2

    move-wide v7, v5

    goto :goto_2

    :cond_2
    move-wide/from16 v7, p5

    :goto_2
    and-int/lit8 v4, p13, 0x8

    if-eqz v4, :cond_3

    goto :goto_3

    :cond_3
    move-wide/from16 v5, p7

    :goto_3
    and-int/lit8 v4, p13, 0x10

    const-wide/16 v9, 0x3e8

    if-eqz v4, :cond_4

    move-wide v11, v9

    goto :goto_4

    :cond_4
    move-wide/from16 v11, p9

    :goto_4
    and-int/lit8 v4, p13, 0x20

    if-eqz v4, :cond_5

    goto :goto_5

    :cond_5
    move-wide/from16 v9, p11

    :goto_5
    move-object p1, p0

    move-wide p2, v0

    move-wide/from16 p4, v2

    move-wide/from16 p6, v7

    move-wide/from16 p8, v5

    move-wide/from16 p10, v11

    move-wide/from16 p12, v9

    .line 60
    invoke-direct/range {p1 .. p13}, Lcom/squareup/cardreader/ble/Timeouts;-><init>(JJJJJJ)V

    return-void
.end method


# virtual methods
.method public final getBondingTimeoutMs()J
    .locals 2

    .line 58
    iget-wide v0, p0, Lcom/squareup/cardreader/ble/Timeouts;->bondingTimeoutMs:J

    return-wide v0
.end method

.method public final getConnectionTimeoutMs()J
    .locals 2

    .line 56
    iget-wide v0, p0, Lcom/squareup/cardreader/ble/Timeouts;->connectionTimeoutMs:J

    return-wide v0
.end method

.method public final getDefaultTimeoutMs()J
    .locals 2

    .line 57
    iget-wide v0, p0, Lcom/squareup/cardreader/ble/Timeouts;->defaultTimeoutMs:J

    return-wide v0
.end method

.method public final getDisconnectionTimeoutMs()J
    .locals 2

    .line 55
    iget-wide v0, p0, Lcom/squareup/cardreader/ble/Timeouts;->disconnectionTimeoutMs:J

    return-wide v0
.end method

.method public final getRssiPeriodMs()J
    .locals 2

    .line 60
    iget-wide v0, p0, Lcom/squareup/cardreader/ble/Timeouts;->rssiPeriodMs:J

    return-wide v0
.end method

.method public final getSetupBondingDelayMs()J
    .locals 2

    .line 59
    iget-wide v0, p0, Lcom/squareup/cardreader/ble/Timeouts;->setupBondingDelayMs:J

    return-wide v0
.end method
