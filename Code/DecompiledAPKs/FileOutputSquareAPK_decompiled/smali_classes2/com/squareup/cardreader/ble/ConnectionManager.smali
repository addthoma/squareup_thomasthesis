.class public interface abstract Lcom/squareup/cardreader/ble/ConnectionManager;
.super Ljava/lang/Object;
.source "ConnectionManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008`\u0018\u00002\u00020\u0001J?\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b2\u0006\u0010\r\u001a\u00020\u000eH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000f\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/cardreader/ble/ConnectionManager;",
        "",
        "manageConnection",
        "",
        "connection",
        "Lcom/squareup/blecoroutines/Connection;",
        "negotiatedConnection",
        "Lcom/squareup/cardreader/ble/NegotiatedConnection;",
        "events",
        "Lcom/squareup/cardreader/ble/RealConnectionEvents;",
        "sendMessageChannel",
        "Lkotlinx/coroutines/channels/Channel;",
        "Lcom/squareup/cardreader/ble/SendMessage;",
        "executionEnv",
        "Lcom/squareup/cardreader/ble/Timeouts;",
        "(Lcom/squareup/blecoroutines/Connection;Lcom/squareup/cardreader/ble/NegotiatedConnection;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lkotlinx/coroutines/channels/Channel;Lcom/squareup/cardreader/ble/Timeouts;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract manageConnection(Lcom/squareup/blecoroutines/Connection;Lcom/squareup/cardreader/ble/NegotiatedConnection;Lcom/squareup/cardreader/ble/RealConnectionEvents;Lkotlinx/coroutines/channels/Channel;Lcom/squareup/cardreader/ble/Timeouts;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blecoroutines/Connection;",
            "Lcom/squareup/cardreader/ble/NegotiatedConnection;",
            "Lcom/squareup/cardreader/ble/RealConnectionEvents;",
            "Lkotlinx/coroutines/channels/Channel<",
            "Lcom/squareup/cardreader/ble/SendMessage;",
            ">;",
            "Lcom/squareup/cardreader/ble/Timeouts;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation
.end method
