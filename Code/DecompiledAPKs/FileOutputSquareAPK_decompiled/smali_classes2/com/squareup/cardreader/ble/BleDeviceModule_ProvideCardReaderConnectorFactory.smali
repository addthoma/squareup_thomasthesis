.class public final Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderConnectorFactory;
.super Ljava/lang/Object;
.source "BleDeviceModule_ProvideCardReaderConnectorFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReaderConnector;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderContextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderContext;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderContext;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderConnectorFactory;->cardReaderContextProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderConnectorFactory;->cardReaderHubProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderConnectorFactory;->mainThreadProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderConnectorFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderContext;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)",
            "Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderConnectorFactory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderConnectorFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderConnectorFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCardReaderConnector(Lcom/squareup/cardreader/CardReaderContext;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/CardReaderConnector;
    .locals 0

    .line 48
    invoke-static {p0, p1, p2}, Lcom/squareup/cardreader/ble/BleDeviceModule;->provideCardReaderConnector(Lcom/squareup/cardreader/CardReaderContext;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/CardReaderConnector;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReaderConnector;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReaderConnector;
    .locals 3

    .line 37
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderConnectorFactory;->cardReaderContextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderContext;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderConnectorFactory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderConnectorFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/thread/executor/MainThread;

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderConnectorFactory;->provideCardReaderConnector(Lcom/squareup/cardreader/CardReaderContext;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/CardReaderConnector;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderConnectorFactory;->get()Lcom/squareup/cardreader/CardReaderConnector;

    move-result-object v0

    return-object v0
.end method
