.class public final Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;
.super Ljava/lang/Object;
.source "DaggerBleCardReaderContextComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private bleDeviceModule:Lcom/squareup/cardreader/ble/BleDeviceModule;

.field private cardReaderModule:Lcom/squareup/cardreader/CardReaderModule;

.field private nonX2CardReaderContextParent:Lcom/squareup/cardreader/NonX2CardReaderContextParent;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$1;)V
    .locals 0

    .line 419
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bleDeviceModule(Lcom/squareup/cardreader/ble/BleDeviceModule;)Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;
    .locals 0

    .line 430
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/ble/BleDeviceModule;

    iput-object p1, p0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;->bleDeviceModule:Lcom/squareup/cardreader/ble/BleDeviceModule;

    return-object p0
.end method

.method public build()Lcom/squareup/cardreader/ble/BleCardReaderContextComponent;
    .locals 5

    .line 455
    iget-object v0, p0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;->bleDeviceModule:Lcom/squareup/cardreader/ble/BleDeviceModule;

    const-class v1, Lcom/squareup/cardreader/ble/BleDeviceModule;

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkBuilderRequirement(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 456
    iget-object v0, p0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;->cardReaderModule:Lcom/squareup/cardreader/CardReaderModule;

    const-class v1, Lcom/squareup/cardreader/CardReaderModule;

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkBuilderRequirement(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 457
    iget-object v0, p0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;->nonX2CardReaderContextParent:Lcom/squareup/cardreader/NonX2CardReaderContextParent;

    const-class v1, Lcom/squareup/cardreader/NonX2CardReaderContextParent;

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkBuilderRequirement(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 458
    new-instance v0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;->bleDeviceModule:Lcom/squareup/cardreader/ble/BleDeviceModule;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;->cardReaderModule:Lcom/squareup/cardreader/CardReaderModule;

    iget-object v3, p0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;->nonX2CardReaderContextParent:Lcom/squareup/cardreader/NonX2CardReaderContextParent;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;-><init>(Lcom/squareup/cardreader/ble/BleDeviceModule;Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/NonX2CardReaderContextParent;Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$1;)V

    return-object v0
.end method

.method public cardReaderModule(Lcom/squareup/cardreader/CardReaderModule;)Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;
    .locals 0

    .line 435
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/CardReaderModule;

    iput-object p1, p0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;->cardReaderModule:Lcom/squareup/cardreader/CardReaderModule;

    return-object p0
.end method

.method public clockModule(Lcom/squareup/android/util/ClockModule;)Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 444
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public nonX2CardReaderContextParent(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;
    .locals 0

    .line 450
    invoke-static {p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/NonX2CardReaderContextParent;

    iput-object p1, p0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$Builder;->nonX2CardReaderContextParent:Lcom/squareup/cardreader/NonX2CardReaderContextParent;

    return-object p0
.end method
