.class public Lcom/squareup/cardreader/TamperFeatureLegacy;
.super Ljava/lang/Object;
.source "TamperFeatureLegacy.java"

# interfaces
.implements Lcom/squareup/cardreader/TamperFeature;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/TamperFeatureLegacy$TamperListener;
    }
.end annotation


# instance fields
.field private final cardreader:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private tamperFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

.field private final tamperFeatureNative:Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;

.field private tamperListener:Lcom/squareup/cardreader/TamperFeatureLegacy$TamperListener;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;",
            ")V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->cardreader:Ljavax/inject/Provider;

    .line 17
    iput-object p2, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperFeatureNative:Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;

    return-void
.end method


# virtual methods
.method public clearFlaggedStatus()V
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperFeatureNative:Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;->cr_tamper_reset_tag(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;

    return-void
.end method

.method public initialize(Lcom/squareup/cardreader/TamperFeatureLegacy$TamperListener;)V
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    .line 26
    iput-object p1, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperListener:Lcom/squareup/cardreader/TamperFeatureLegacy$TamperListener;

    .line 28
    iget-object p1, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperFeatureNative:Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;

    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->cardreader:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPointer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderPointer;->getId()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    invoke-interface {p1, v0, p0}, Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;->tamper_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    return-void

    .line 25
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "apiListener cannot be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 22
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "TamperFeature is already initialized!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onTamperData([B)V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperListener:Lcom/squareup/cardreader/TamperFeatureLegacy$TamperListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/TamperFeatureLegacy$TamperListener;->onTamperDataReceived([B)V

    return-void
.end method

.method public onTamperStatus(I)V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperListener:Lcom/squareup/cardreader/TamperFeatureLegacy$TamperListener;

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrTamperStatus;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrTamperStatus;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/TamperFeatureLegacy$TamperListener;->onTamperStatusReceived(Lcom/squareup/cardreader/lcr/CrTamperStatus;)V

    return-void
.end method

.method public requestTamperData()V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperFeatureNative:Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;->cr_tamper_get_tamper_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;

    return-void
.end method

.method public requestTamperStatus()V
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperFeatureNative:Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;->cr_tamper_get_tamper_status(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;

    return-void
.end method

.method public resetTamperFeature()V
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperListener:Lcom/squareup/cardreader/TamperFeatureLegacy$TamperListener;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperFeatureNative:Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;->cr_tamper_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;

    .line 34
    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperFeatureNative:Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;->cr_tamper_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;

    const/4 v0, 0x0

    .line 35
    iput-object v0, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    .line 36
    iput-object v0, p0, Lcom/squareup/cardreader/TamperFeatureLegacy;->tamperListener:Lcom/squareup/cardreader/TamperFeatureLegacy$TamperListener;

    :cond_0
    return-void
.end method
