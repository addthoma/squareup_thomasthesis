.class public final Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderGraphInitializerFactory;
.super Ljava/lang/Object;
.source "SpeCardReaderModule_ProvideCardReaderGraphInitializerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderGraphInitializerFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderGraphInitializerFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderGraphInitializerFactory$InstanceHolder;->access$000()Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderGraphInitializerFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideCardReaderGraphInitializer()Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule;->provideCardReaderGraphInitializer()Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderGraphInitializerFactory;->provideCardReaderGraphInitializer()Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideCardReaderGraphInitializerFactory;->get()Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;

    move-result-object v0

    return-object v0
.end method
