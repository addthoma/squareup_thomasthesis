.class Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;
.super Ljava/lang/Object;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Builder"
.end annotation


# instance fields
.field private amountAuthorized:Ljava/lang/Long;

.field private approvedOffline:Ljava/lang/Boolean;

.field private brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

.field private cardAction:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field private cardInfo:Lcom/squareup/cardreader/CardInfo;

.field private cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field private event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

.field private paymentResult:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

.field private present:Ljava/lang/Boolean;

.field private requestType:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

.field private standardMessage:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field private startPaymentResult:Lcom/squareup/cardreader/lcr/CrPaymentResult;

.field private tmnTransactionResult:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

.field private transactionId:Ljava/lang/String;

.field private transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

.field private willContinuePayment:Ljava/lang/Boolean;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;
    .locals 0

    .line 134
    iget-object p0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;
    .locals 0

    .line 134
    iget-object p0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Ljava/lang/Long;
    .locals 0

    .line 134
    iget-object p0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->amountAuthorized:Ljava/lang/Long;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 0

    .line 134
    iget-object p0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->startPaymentResult:Lcom/squareup/cardreader/lcr/CrPaymentResult;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/lcr/CrsTmnBrandId;
    .locals 0

    .line 134
    iget-object p0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/lcr/CrsTmnRequestType;
    .locals 0

    .line 134
    iget-object p0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->requestType:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/lcr/TmnTransactionResult;
    .locals 0

    .line 134
    iget-object p0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->tmnTransactionResult:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    return-object p0
.end method

.method static synthetic access$1600(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 134
    iget-object p0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->transactionId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/CardReaderInfo;
    .locals 0

    .line 134
    iget-object p0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;
    .locals 0

    .line 134
    iget-object p0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->paymentResult:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;
    .locals 0

    .line 134
    iget-object p0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->standardMessage:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/lcr/CrPaymentCardAction;
    .locals 0

    .line 134
    iget-object p0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->cardAction:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 134
    iget-object p0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->approvedOffline:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/CardInfo;
    .locals 0

    .line 134
    iget-object p0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 134
    iget-object p0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->willContinuePayment:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 134
    iget-object p0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->present:Ljava/lang/Boolean;

    return-object p0
.end method


# virtual methods
.method amountAuthorized(J)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;
    .locals 0

    .line 208
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->amountAuthorized:Ljava/lang/Long;

    return-object p0
.end method

.method approvedOffline(Z)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;
    .locals 0

    .line 193
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->approvedOffline:Ljava/lang/Boolean;

    return-object p0
.end method

.method brandId(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;
    .locals 0

    .line 218
    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    return-object p0
.end method

.method build()Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;
    .locals 2

    .line 233
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;-><init>(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;Lcom/squareup/cardreader/ReaderEventLogger$1;)V

    return-object v0
.end method

.method cardAction(Lcom/squareup/cardreader/lcr/CrPaymentCardAction;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->cardAction:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    return-object p0
.end method

.method cardInfo(Lcom/squareup/cardreader/CardInfo;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    return-object p0
.end method

.method cardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-object p0
.end method

.method event(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    return-object p0
.end method

.method paymentResult(Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->paymentResult:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    return-object p0
.end method

.method present(Z)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;
    .locals 0

    .line 183
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->present:Ljava/lang/Boolean;

    return-object p0
.end method

.method requestType(Lcom/squareup/cardreader/lcr/CrsTmnRequestType;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->requestType:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    return-object p0
.end method

.method standardMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->standardMessage:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    return-object p0
.end method

.method startPaymentResult(Lcom/squareup/cardreader/lcr/CrPaymentResult;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;
    .locals 0

    .line 213
    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->startPaymentResult:Lcom/squareup/cardreader/lcr/CrPaymentResult;

    return-object p0
.end method

.method tmnTransactionResult(Lcom/squareup/cardreader/lcr/TmnTransactionResult;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->tmnTransactionResult:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    return-object p0
.end method

.method transactionId(Ljava/lang/String;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->transactionId:Ljava/lang/String;

    return-object p0
.end method

.method transactionType(Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    return-object p0
.end method

.method willContinuePayment(Z)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;
    .locals 0

    .line 188
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->willContinuePayment:Ljava/lang/Boolean;

    return-object p0
.end method
