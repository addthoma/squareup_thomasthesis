.class public final Lcom/squareup/cardreader/dagger/CardReaderStoreModule_ProvidePairedReaderNamesFactory;
.super Ljava/lang/Object;
.source "CardReaderStoreModule_ProvidePairedReaderNamesFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/LocalSetting<",
        "Ljava/util/Map<",
        "Ljava/lang/String;",
        "Lcom/squareup/cardreader/SavedCardReader;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/cardreader/dagger/CardReaderStoreModule;

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/dagger/CardReaderStoreModule;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/cardreader/dagger/CardReaderStoreModule_ProvidePairedReaderNamesFactory;->module:Lcom/squareup/cardreader/dagger/CardReaderStoreModule;

    .line 31
    iput-object p2, p0, Lcom/squareup/cardreader/dagger/CardReaderStoreModule_ProvidePairedReaderNamesFactory;->preferencesProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/cardreader/dagger/CardReaderStoreModule_ProvidePairedReaderNamesFactory;->gsonProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/dagger/CardReaderStoreModule_ProvidePairedReaderNamesFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/dagger/CardReaderStoreModule;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)",
            "Lcom/squareup/cardreader/dagger/CardReaderStoreModule_ProvidePairedReaderNamesFactory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/cardreader/dagger/CardReaderStoreModule_ProvidePairedReaderNamesFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/dagger/CardReaderStoreModule_ProvidePairedReaderNamesFactory;-><init>(Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static providePairedReaderNames(Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/dagger/CardReaderStoreModule;",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/SavedCardReader;",
            ">;>;"
        }
    .end annotation

    .line 48
    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/dagger/CardReaderStoreModule;->providePairedReaderNames(Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/LocalSetting;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/SavedCardReader;",
            ">;>;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/squareup/cardreader/dagger/CardReaderStoreModule_ProvidePairedReaderNamesFactory;->module:Lcom/squareup/cardreader/dagger/CardReaderStoreModule;

    iget-object v1, p0, Lcom/squareup/cardreader/dagger/CardReaderStoreModule_ProvidePairedReaderNamesFactory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/squareup/cardreader/dagger/CardReaderStoreModule_ProvidePairedReaderNamesFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/gson/Gson;

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/dagger/CardReaderStoreModule_ProvidePairedReaderNamesFactory;->providePairedReaderNames(Lcom/squareup/cardreader/dagger/CardReaderStoreModule;Landroid/content/SharedPreferences;Lcom/google/gson/Gson;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/cardreader/dagger/CardReaderStoreModule_ProvidePairedReaderNamesFactory;->get()Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method
