.class public Lcom/squareup/cardreader/DippedCardTracker;
.super Ljava/lang/Object;
.source "DippedCardTracker.java"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/DippedCardTracker$CardState;
    }
.end annotation


# instance fields
.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final cardStates:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/cardreader/CardReaderId;",
            "Lcom/squareup/cardreader/DippedCardTracker$CardState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReaderListeners;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/cardreader/DippedCardTracker;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 26
    iput-object p2, p0, Lcom/squareup/cardreader/DippedCardTracker;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 27
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/DippedCardTracker;->cardStates:Ljava/util/Map;

    return-void
.end method

.method private getCardState(Lcom/squareup/cardreader/CardReader;)Lcom/squareup/cardreader/DippedCardTracker$CardState;
    .locals 0

    .line 101
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresent()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/cardreader/DippedCardTracker$CardState;->INSERTED:Lcom/squareup/cardreader/DippedCardTracker$CardState;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/cardreader/DippedCardTracker$CardState;->REMOVED:Lcom/squareup/cardreader/DippedCardTracker$CardState;

    :goto_0
    return-object p1
.end method


# virtual methods
.method public mustReinsertDippedCard()Z
    .locals 4

    .line 70
    iget-object v0, p0, Lcom/squareup/cardreader/DippedCardTracker;->cardStates:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderId;

    .line 71
    iget-object v2, p0, Lcom/squareup/cardreader/DippedCardTracker;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v2, v1}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 73
    invoke-interface {v2}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresent()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/squareup/cardreader/DippedCardTracker;->cardStates:Ljava/util/Map;

    .line 74
    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v3, Lcom/squareup/cardreader/DippedCardTracker$CardState;->MUST_BE_REMOVED:Lcom/squareup/cardreader/DippedCardTracker$CardState;

    if-ne v1, v3, :cond_0

    .line 75
    iget-object v0, p0, Lcom/squareup/cardreader/DippedCardTracker;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    invoke-interface {v2}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_MUST_BE_REINSERTED:Lcom/squareup/analytics/ReaderEventName;

    invoke-interface {v0, v1, v2}, Lcom/squareup/cardreader/ReaderEventLogger;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public onCardInserted(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/cardreader/DippedCardTracker;->cardStates:Ljava/util/Map;

    sget-object v1, Lcom/squareup/cardreader/DippedCardTracker$CardState;->INSERTED:Lcom/squareup/cardreader/DippedCardTracker$CardState;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/squareup/cardreader/DippedCardTracker;->cardStates:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/DippedCardTracker;->getCardState(Lcom/squareup/cardreader/CardReader;)Lcom/squareup/cardreader/DippedCardTracker$CardState;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/cardreader/DippedCardTracker;->cardStates:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onCardRemoved(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/cardreader/DippedCardTracker;->cardStates:Ljava/util/Map;

    sget-object v1, Lcom/squareup/cardreader/DippedCardTracker$CardState;->REMOVED:Lcom/squareup/cardreader/DippedCardTracker$CardState;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onEmvTransactionCompleted(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/cardreader/DippedCardTracker;->cardStates:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/DippedCardTracker$CardState;->INSERTED:Lcom/squareup/cardreader/DippedCardTracker$CardState;

    if-ne v0, v1, :cond_0

    .line 46
    iget-object v0, p0, Lcom/squareup/cardreader/DippedCardTracker;->cardStates:Ljava/util/Map;

    sget-object v1, Lcom/squareup/cardreader/DippedCardTracker$CardState;->MUST_BE_REMOVED:Lcom/squareup/cardreader/DippedCardTracker$CardState;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 85
    iget-object p1, p0, Lcom/squareup/cardreader/DippedCardTracker;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {p1, p0}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/cardreader/DippedCardTracker;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/CardReaderHub;->removeCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    return-void
.end method
