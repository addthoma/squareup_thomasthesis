.class public final Lcom/squareup/cardreader/SystemFeatureV2;
.super Ljava/lang/Object;
.source "SystemFeatureV2.kt"

# interfaces
.implements Lcom/squareup/cardreader/CanReset;
.implements Lcom/squareup/cardreader/SystemFeature;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSystemFeatureV2.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SystemFeatureV2.kt\ncom/squareup/cardreader/SystemFeatureV2\n*L\n1#1,108:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0012\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0008\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u001b\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eJ\u0008\u0010\u000f\u001a\u00020\u000cH\u0002J\u0018\u0010\u0010\u001a\n \u0012*\u0004\u0018\u00010\u00110\u00112\u0006\u0010\u0013\u001a\u00020\nH\u0002J\u0018\u0010\u0014\u001a\u00020\u000c2\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u000c2\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u000c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0010\u0010\u001f\u001a\u00020\u000c2\u0006\u0010 \u001a\u00020\u001eH\u0016J\u0010\u0010!\u001a\u00020\u000c2\u0006\u0010\"\u001a\u00020\u001bH\u0016J\u0010\u0010#\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\nH\u0002J\u0008\u0010$\u001a\u00020\u000cH\u0016J\u0016\u0010%\u001a\u00020\u000c2\u000c\u0010&\u001a\u0008\u0012\u0004\u0012\u00020(0\'H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/cardreader/SystemFeatureV2;",
        "Lcom/squareup/cardreader/CanReset;",
        "Lcom/squareup/cardreader/SystemFeature;",
        "posSender",
        "Lcom/squareup/cardreader/SendsToPos;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SystemFeatureOutput;",
        "cardreaderProvider",
        "Lcom/squareup/cardreader/CardreaderPointerProvider;",
        "(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V",
        "featurePointer",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;",
        "handleMessage",
        "",
        "message",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SystemFeatureMessage;",
        "initialize",
        "markFeatureFlagsReadyToSend",
        "Lcom/squareup/cardreader/lcr/CrSystemResult;",
        "kotlin.jvm.PlatformType",
        "systemFeature",
        "onCapabilitiesReceived",
        "capabilitiesSupported",
        "",
        "capabilityBytes",
        "",
        "onChargeCycleCountReceived",
        "chargeCycleCount",
        "",
        "onFirmwareVersionReceived",
        "firmwareVersion",
        "",
        "onHardwareSerialNumberReceived",
        "hardwareSerialNumber",
        "onReaderErrorReceived",
        "readerErrorInt",
        "requestSystemInfo",
        "resetIfInitilized",
        "setReaderFeatureFlags",
        "flags",
        "",
        "Lcom/squareup/cardreader/ReaderFeatureFlag;",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

.field private featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

.field private final posSender:Lcom/squareup/cardreader/SendsToPos;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SystemFeatureOutput;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SystemFeatureOutput;",
            ">;",
            "Lcom/squareup/cardreader/CardreaderPointerProvider;",
            ")V"
        }
    .end annotation

    const-string v0, "posSender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardreaderProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/SystemFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    iput-object p2, p0, Lcom/squareup/cardreader/SystemFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    return-void
.end method

.method public static final synthetic access$getFeaturePointer$p(Lcom/squareup/cardreader/SystemFeatureV2;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;
    .locals 1

    .line 19
    iget-object p0, p0, Lcom/squareup/cardreader/SystemFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    if-nez p0, :cond_0

    const-string v0, "featurePointer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setFeaturePointer$p(Lcom/squareup/cardreader/SystemFeatureV2;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)V
    .locals 0

    .line 19
    iput-object p1, p0, Lcom/squareup/cardreader/SystemFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    return-void
.end method

.method private final initialize()V
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/squareup/cardreader/SystemFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardreaderPointerProvider;->cardreaderPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/squareup/cardreader/lcr/SystemFeatureNative;->system_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    move-result-object v0

    const-string v1, "SystemFeatureNative.syst\u2026ardreaderPointer(), this)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/cardreader/SystemFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    return-void
.end method

.method private final markFeatureFlagsReadyToSend(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;
    .locals 0

    .line 66
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SystemFeatureNative;->cr_system_mark_feature_flags_ready_to_send(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p1

    return-object p1
.end method

.method private final requestSystemInfo(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;
    .locals 1

    .line 73
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SystemFeatureNative;->cr_system_read_system_info(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p1

    const-string v0, "SystemFeatureNative.cr_s\u2026ystem_info(systemFeature)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final setReaderFeatureFlags(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/ReaderFeatureFlag;",
            ">;)V"
        }
    .end annotation

    .line 46
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const-string v1, "result"

    const-string v2, "featurePointer"

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ReaderFeatureFlag;

    .line 48
    iget-object v3, p0, Lcom/squareup/cardreader/SystemFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    if-nez v3, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderFeatureFlag;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderFeatureFlag;->getValue()Z

    move-result v0

    .line 47
    invoke-static {v3, v2, v0}, Lcom/squareup/cardreader/lcr/SystemFeatureNative;->system_set_reader_feature_flag(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Ljava/lang/String;S)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object v0

    .line 50
    sget-object v2, Lcom/squareup/cardreader/lcr/CrSystemResult;->CR_SYSTEM_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrSystemResult;

    if-eq v0, v2, :cond_0

    .line 51
    iget-object p1, p0, Lcom/squareup/cardreader/SystemFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v2, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SystemFeatureOutput$FeatureFlagsFailed;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SystemFeatureOutput$FeatureFlagsFailed;-><init>(Lcom/squareup/cardreader/lcr/CrSystemResult;)V

    check-cast v2, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {p1, v2}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void

    .line 56
    :cond_2
    iget-object p1, p0, Lcom/squareup/cardreader/SystemFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    if-nez p1, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SystemFeatureNative;->cr_system_mark_feature_flags_ready_to_send(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p1

    .line 57
    sget-object v0, Lcom/squareup/cardreader/lcr/CrSystemResult;->CR_SYSTEM_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrSystemResult;

    if-eq p1, v0, :cond_4

    .line 58
    iget-object v0, p0, Lcom/squareup/cardreader/SystemFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v2, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SystemFeatureOutput$FeatureFlagsFailed;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SystemFeatureOutput$FeatureFlagsFailed;-><init>(Lcom/squareup/cardreader/lcr/CrSystemResult;)V

    check-cast v2, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v2}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void

    .line 62
    :cond_4
    iget-object p1, p0, Lcom/squareup/cardreader/SystemFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SystemFeatureOutput$FeatureFlagsSuccess;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SystemFeatureOutput$FeatureFlagsSuccess;

    check-cast v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SystemFeatureMessage;)V
    .locals 2

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SystemFeatureMessage$Initialize;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/cardreader/SystemFeatureV2;->initialize()V

    goto :goto_0

    .line 28
    :cond_0
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SystemFeatureMessage$SetFeatureFlags;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SystemFeatureMessage$SetFeatureFlags;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SystemFeatureMessage$SetFeatureFlags;->getFeatureFlags()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/SystemFeatureV2;->setReaderFeatureFlags(Ljava/util/List;)V

    goto :goto_0

    .line 29
    :cond_1
    instance-of p1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SystemFeatureMessage$RequestSystemInfo;

    if-nez p1, :cond_2

    :goto_0
    return-void

    :cond_2
    new-instance p1, Lkotlin/NotImplementedError;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p1, v1, v0, v1}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public onCapabilitiesReceived(Z[B)V
    .locals 0

    const-string p1, "capabilityBytes"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onChargeCycleCountReceived(I)V
    .locals 0

    return-void
.end method

.method public onFirmwareVersionReceived(Ljava/lang/String;)V
    .locals 2

    const-string v0, "firmwareVersion"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "Firmware version: %s"

    .line 87
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onHardwareSerialNumberReceived(Ljava/lang/String;)V
    .locals 1

    const-string v0, "hardwareSerialNumber"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onReaderErrorReceived(I)V
    .locals 0

    return-void
.end method

.method public resetIfInitilized()V
    .locals 2

    .line 39
    move-object v0, p0

    check-cast v0, Lcom/squareup/cardreader/SystemFeatureV2;

    iget-object v0, v0, Lcom/squareup/cardreader/SystemFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    if-eqz v0, :cond_2

    .line 40
    iget-object v0, p0, Lcom/squareup/cardreader/SystemFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    const-string v1, "featurePointer"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/SystemFeatureNative;->cr_system_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    .line 41
    iget-object v0, p0, Lcom/squareup/cardreader/SystemFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/SystemFeatureNative;->cr_system_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    :cond_2
    return-void
.end method
