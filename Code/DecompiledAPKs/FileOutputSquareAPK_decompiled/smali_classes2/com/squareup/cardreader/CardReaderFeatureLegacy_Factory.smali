.class public final Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;
.super Ljava/lang/Object;
.source "CardReaderFeatureLegacy_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReaderFeatureLegacy;",
        ">;"
    }
.end annotation


# instance fields
.field private final backendProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LcrBackend;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderConstantsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConstants;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderLogBridgeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderLogBridge;",
            ">;"
        }
    .end annotation
.end field

.field private final cardreaderNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private final crashnadoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;"
        }
    .end annotation
.end field

.field private final lcrExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final loggingEnabledProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final timerApiProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/TimerApiLegacy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LcrBackend;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/TimerApiLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderLogBridge;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConstants;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->loggingEnabledProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->crashnadoProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p3, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->backendProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p4, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->timerApiProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p5, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->lcrExecutorProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p6, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->cardReaderLogBridgeProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p7, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->cardReaderConstantsProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p8, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->cardreaderNativeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LcrBackend;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/TimerApiLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderLogBridge;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConstants;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;",
            ">;)",
            "Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;"
        }
    .end annotation

    .line 62
    new-instance v9, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Ljava/lang/Boolean;Lcom/squareup/crashnado/Crashnado;Lcom/squareup/cardreader/LcrBackend;Lcom/squareup/cardreader/TimerApiLegacy;Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/CardReaderLogBridge;Lcom/squareup/cardreader/CardReaderConstants;Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;)Lcom/squareup/cardreader/CardReaderFeatureLegacy;
    .locals 10

    .line 69
    new-instance v9, Lcom/squareup/cardreader/CardReaderFeatureLegacy;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/cardreader/CardReaderFeatureLegacy;-><init>(Ljava/lang/Boolean;Lcom/squareup/crashnado/Crashnado;Lcom/squareup/cardreader/LcrBackend;Lcom/squareup/cardreader/TimerApiLegacy;Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/CardReaderLogBridge;Lcom/squareup/cardreader/CardReaderConstants;Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReaderFeatureLegacy;
    .locals 9

    .line 53
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->loggingEnabledProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->crashnadoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/crashnado/Crashnado;

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->backendProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cardreader/LcrBackend;

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->timerApiProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/cardreader/TimerApiLegacy;

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->lcrExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->cardReaderLogBridgeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/cardreader/CardReaderLogBridge;

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->cardReaderConstantsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/cardreader/CardReaderConstants;

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->cardreaderNativeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    invoke-static/range {v1 .. v8}, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->newInstance(Ljava/lang/Boolean;Lcom/squareup/crashnado/Crashnado;Lcom/squareup/cardreader/LcrBackend;Lcom/squareup/cardreader/TimerApiLegacy;Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/CardReaderLogBridge;Lcom/squareup/cardreader/CardReaderConstants;Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;)Lcom/squareup/cardreader/CardReaderFeatureLegacy;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->get()Lcom/squareup/cardreader/CardReaderFeatureLegacy;

    move-result-object v0

    return-object v0
.end method
