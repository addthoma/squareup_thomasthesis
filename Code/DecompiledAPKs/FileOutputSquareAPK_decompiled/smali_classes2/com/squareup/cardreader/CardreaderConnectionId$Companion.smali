.class public final Lcom/squareup/cardreader/CardreaderConnectionId$Companion;
.super Ljava/lang/Object;
.source "CardreaderMessengerInterface.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardreaderConnectionId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0005\u001a\u00020\tR\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/cardreader/CardreaderConnectionId$Companion;",
        "",
        "()V",
        "nextId",
        "",
        "getNextId",
        "()I",
        "setNextId",
        "(I)V",
        "Lcom/squareup/cardreader/CardreaderConnectionId;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/squareup/cardreader/CardreaderConnectionId$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getNextId()I
    .locals 1

    .line 45
    invoke-static {}, Lcom/squareup/cardreader/CardreaderConnectionId;->access$getNextId$cp()I

    move-result v0

    return v0
.end method

.method public final declared-synchronized getNextId()Lcom/squareup/cardreader/CardreaderConnectionId;
    .locals 4

    monitor-enter p0

    .line 48
    :try_start_0
    new-instance v0, Lcom/squareup/cardreader/CardreaderConnectionId;

    move-object v1, p0

    check-cast v1, Lcom/squareup/cardreader/CardreaderConnectionId$Companion;

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardreaderConnectionId$Companion;->getNextId()I

    move-result v2

    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v1, v3}, Lcom/squareup/cardreader/CardreaderConnectionId$Companion;->setNextId(I)V

    invoke-direct {v0, v2}, Lcom/squareup/cardreader/CardreaderConnectionId;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final setNextId(I)V
    .locals 0

    .line 45
    invoke-static {p1}, Lcom/squareup/cardreader/CardreaderConnectionId;->access$setNextId$cp(I)V

    return-void
.end method
