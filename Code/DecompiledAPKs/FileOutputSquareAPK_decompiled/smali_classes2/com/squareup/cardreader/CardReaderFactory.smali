.class public interface abstract Lcom/squareup/cardreader/CardReaderFactory;
.super Ljava/lang/Object;
.source "CardReaderFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;
    }
.end annotation


# virtual methods
.method public abstract destroy(Lcom/squareup/cardreader/CardReaderId;)V
.end method

.method public abstract destroyAllBluetoothReaders()V
.end method

.method public abstract destroyAllReaders()V
.end method

.method public abstract destroyWirelessAutoConnect(Ljava/lang/String;)V
.end method

.method public abstract forAudio()Lcom/squareup/cardreader/CardReaderContext;
.end method

.method public abstract forBle(Lcom/squareup/cardreader/WirelessConnection;)Lcom/squareup/cardreader/CardReaderContext;
.end method

.method public abstract forBleAutoConnect(Lcom/squareup/cardreader/WirelessConnection;)V
.end method

.method public abstract forT2()Lcom/squareup/cardreader/CardReaderContext;
.end method

.method public abstract forX2()Lcom/squareup/cardreader/CardReaderContext;
.end method

.method public abstract hasCardReaderWithAddress(Ljava/lang/String;)Z
.end method

.method public abstract initialize(Lcom/squareup/cardreader/CardReaderContextParent;)V
.end method
