.class public Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;
.super Ljava/lang/Object;
.source "GlobalCardReaderModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/GlobalCardReaderModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProdWithoutCardReaderFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideCardReaderHub()Lcom/squareup/cardreader/CardReaderHub;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 51
    new-instance v0, Lcom/squareup/cardreader/CardReaderHub;

    invoke-direct {v0}, Lcom/squareup/cardreader/CardReaderHub;-><init>()V

    return-object v0
.end method

.method provideRealCardReaderFactory(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/settings/server/Features;)Lcom/squareup/cardreader/RealCardReaderFactory;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 56
    new-instance v0, Lcom/squareup/cardreader/RealCardReaderFactory;

    invoke-direct {v0, p1, p2}, Lcom/squareup/cardreader/RealCardReaderFactory;-><init>(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method
