.class public final Lcom/squareup/cardreader/GlobalCardReaderModule_CardReaderPauseAndResumerFactory;
.super Ljava/lang/Object;
.source "GlobalCardReaderModule_CardReaderPauseAndResumerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/cardreader/GlobalCardReaderModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/GlobalCardReaderModule;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_CardReaderPauseAndResumerFactory;->module:Lcom/squareup/cardreader/GlobalCardReaderModule;

    return-void
.end method

.method public static cardReaderPauseAndResumer(Lcom/squareup/cardreader/GlobalCardReaderModule;)Lcom/squareup/cardreader/CardReaderPauseAndResumer;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/cardreader/GlobalCardReaderModule;->cardReaderPauseAndResumer()Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    return-object p0
.end method

.method public static create(Lcom/squareup/cardreader/GlobalCardReaderModule;)Lcom/squareup/cardreader/GlobalCardReaderModule_CardReaderPauseAndResumerFactory;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/cardreader/GlobalCardReaderModule_CardReaderPauseAndResumerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/GlobalCardReaderModule_CardReaderPauseAndResumerFactory;-><init>(Lcom/squareup/cardreader/GlobalCardReaderModule;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReaderPauseAndResumer;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_CardReaderPauseAndResumerFactory;->module:Lcom/squareup/cardreader/GlobalCardReaderModule;

    invoke-static {v0}, Lcom/squareup/cardreader/GlobalCardReaderModule_CardReaderPauseAndResumerFactory;->cardReaderPauseAndResumer(Lcom/squareup/cardreader/GlobalCardReaderModule;)Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/cardreader/GlobalCardReaderModule_CardReaderPauseAndResumerFactory;->get()Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    move-result-object v0

    return-object v0
.end method
