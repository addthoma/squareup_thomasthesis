.class public Lcom/squareup/cardreader/MagSwipeFailureFilter$NeverFilterMagSwipeFailures;
.super Ljava/lang/Object;
.source "MagSwipeFailureFilter.java"

# interfaces
.implements Lcom/squareup/cardreader/MagSwipeFailureFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/MagSwipeFailureFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NeverFilterMagSwipeFailures"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setEnabled(Z)V
    .locals 1

    .line 27
    new-instance p1, Ljava/lang/IllegalAccessError;

    const-string v0, "Should never try to enable MagSwipeFailureFilter"

    invoke-direct {p1, v0}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public shouldFilter()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
