.class public final enum Lcom/squareup/cardreader/TmnBrandId;
.super Ljava/lang/Enum;
.source "TmnBrandId.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/TmnBrandId;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\n\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/cardreader/TmnBrandId;",
        "",
        "(Ljava/lang/String;I)V",
        "TMN_BRAND_ID_COMMON",
        "TMN_BRAND_ID_QUICPAY",
        "TMN_BRAND_ID_ID",
        "TMN_BRAND_ID_SUICA",
        "TMN_BRAND_ID_RAKUTEN",
        "TMN_BRAND_ID_WAON",
        "TMN_BRAND_ID_NANACO",
        "TMN_BRAND_ID_PITAPA",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/TmnBrandId;

.field public static final enum TMN_BRAND_ID_COMMON:Lcom/squareup/cardreader/TmnBrandId;

.field public static final enum TMN_BRAND_ID_ID:Lcom/squareup/cardreader/TmnBrandId;

.field public static final enum TMN_BRAND_ID_NANACO:Lcom/squareup/cardreader/TmnBrandId;

.field public static final enum TMN_BRAND_ID_PITAPA:Lcom/squareup/cardreader/TmnBrandId;

.field public static final enum TMN_BRAND_ID_QUICPAY:Lcom/squareup/cardreader/TmnBrandId;

.field public static final enum TMN_BRAND_ID_RAKUTEN:Lcom/squareup/cardreader/TmnBrandId;

.field public static final enum TMN_BRAND_ID_SUICA:Lcom/squareup/cardreader/TmnBrandId;

.field public static final enum TMN_BRAND_ID_WAON:Lcom/squareup/cardreader/TmnBrandId;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/cardreader/TmnBrandId;

    new-instance v1, Lcom/squareup/cardreader/TmnBrandId;

    const/4 v2, 0x0

    const-string v3, "TMN_BRAND_ID_COMMON"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnBrandId;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnBrandId;->TMN_BRAND_ID_COMMON:Lcom/squareup/cardreader/TmnBrandId;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnBrandId;

    const/4 v2, 0x1

    const-string v3, "TMN_BRAND_ID_QUICPAY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnBrandId;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnBrandId;->TMN_BRAND_ID_QUICPAY:Lcom/squareup/cardreader/TmnBrandId;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnBrandId;

    const/4 v2, 0x2

    const-string v3, "TMN_BRAND_ID_ID"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnBrandId;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnBrandId;->TMN_BRAND_ID_ID:Lcom/squareup/cardreader/TmnBrandId;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnBrandId;

    const/4 v2, 0x3

    const-string v3, "TMN_BRAND_ID_SUICA"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnBrandId;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnBrandId;->TMN_BRAND_ID_SUICA:Lcom/squareup/cardreader/TmnBrandId;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnBrandId;

    const/4 v2, 0x4

    const-string v3, "TMN_BRAND_ID_RAKUTEN"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnBrandId;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnBrandId;->TMN_BRAND_ID_RAKUTEN:Lcom/squareup/cardreader/TmnBrandId;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnBrandId;

    const/4 v2, 0x5

    const-string v3, "TMN_BRAND_ID_WAON"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnBrandId;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnBrandId;->TMN_BRAND_ID_WAON:Lcom/squareup/cardreader/TmnBrandId;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnBrandId;

    const/4 v2, 0x6

    const-string v3, "TMN_BRAND_ID_NANACO"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnBrandId;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnBrandId;->TMN_BRAND_ID_NANACO:Lcom/squareup/cardreader/TmnBrandId;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnBrandId;

    const/4 v2, 0x7

    const-string v3, "TMN_BRAND_ID_PITAPA"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnBrandId;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnBrandId;->TMN_BRAND_ID_PITAPA:Lcom/squareup/cardreader/TmnBrandId;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/TmnBrandId;->$VALUES:[Lcom/squareup/cardreader/TmnBrandId;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/TmnBrandId;
    .locals 1

    const-class v0, Lcom/squareup/cardreader/TmnBrandId;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/TmnBrandId;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/TmnBrandId;
    .locals 1

    sget-object v0, Lcom/squareup/cardreader/TmnBrandId;->$VALUES:[Lcom/squareup/cardreader/TmnBrandId;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/TmnBrandId;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/TmnBrandId;

    return-object v0
.end method
