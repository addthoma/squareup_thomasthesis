.class public interface abstract Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;
.super Ljava/lang/Object;
.source "CardReaderDispatch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderDispatch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FirmwareUpdateListener"
.end annotation


# virtual methods
.method public abstract onFirmwareUpdateError(Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V
.end method

.method public abstract onFirmwareUpdateProgress(I)V
.end method

.method public abstract onFirmwareUpdateSuccess()V
.end method

.method public abstract onManifestReceived([BZLcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
.end method

.method public abstract onTmsCountryCode(Ljava/lang/String;)V
.end method

.method public abstract onVersionInfo([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;)V
.end method
