.class synthetic Lcom/squareup/cardreader/SecureSessionRevocationFeature$1;
.super Ljava/lang/Object;
.source "SecureSessionRevocationFeature.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/SecureSessionRevocationFeature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$protos$client$flipper$AugmentedStatus$UserExperienceHint:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 188
    invoke-static {}, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->values()[Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/cardreader/SecureSessionRevocationFeature$1;->$SwitchMap$com$squareup$protos$client$flipper$AugmentedStatus$UserExperienceHint:[I

    :try_start_0
    sget-object v0, Lcom/squareup/cardreader/SecureSessionRevocationFeature$1;->$SwitchMap$com$squareup$protos$client$flipper$AugmentedStatus$UserExperienceHint:[I

    sget-object v1, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->SUGGEST_RETRY:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    invoke-virtual {v1}, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/cardreader/SecureSessionRevocationFeature$1;->$SwitchMap$com$squareup$protos$client$flipper$AugmentedStatus$UserExperienceHint:[I

    sget-object v1, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->SUGGEST_ACTIVATION:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    invoke-virtual {v1}, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/cardreader/SecureSessionRevocationFeature$1;->$SwitchMap$com$squareup$protos$client$flipper$AugmentedStatus$UserExperienceHint:[I

    sget-object v1, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->SUGGEST_CONTACT_SUPPORT:Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;

    invoke-virtual {v1}, Lcom/squareup/protos/client/flipper/AugmentedStatus$UserExperienceHint;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    return-void
.end method
