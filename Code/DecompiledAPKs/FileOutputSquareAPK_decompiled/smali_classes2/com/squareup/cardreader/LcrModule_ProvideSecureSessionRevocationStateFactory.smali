.class public final Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationStateFactory;
.super Ljava/lang/Object;
.source "LcrModule_ProvideSecureSessionRevocationStateFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationStateFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/SecureSessionRevocationState;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationStateFactory;
    .locals 1

    .line 22
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationStateFactory$InstanceHolder;->access$000()Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationStateFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideSecureSessionRevocationState()Lcom/squareup/cardreader/SecureSessionRevocationState;
    .locals 2

    .line 26
    invoke-static {}, Lcom/squareup/cardreader/LcrModule;->provideSecureSessionRevocationState()Lcom/squareup/cardreader/SecureSessionRevocationState;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/SecureSessionRevocationState;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/SecureSessionRevocationState;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationStateFactory;->provideSecureSessionRevocationState()Lcom/squareup/cardreader/SecureSessionRevocationState;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationStateFactory;->get()Lcom/squareup/cardreader/SecureSessionRevocationState;

    move-result-object v0

    return-object v0
.end method
