.class public Lcom/squareup/cardreader/SecureSessionRevocationState;
.super Ljava/lang/Object;
.source "SecureSessionRevocationState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/SecureSessionRevocationState$NoopRevocationState;
    }
.end annotation


# instance fields
.field private reasonDescription:Ljava/lang/String;

.field private reasonTitle:Ljava/lang/String;

.field private revoked:Z

.field private uxHint:Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 10
    iput-boolean v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationState;->revoked:Z

    const/4 v0, 0x0

    .line 11
    iput-object v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationState;->reasonTitle:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationState;->reasonDescription:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationState;->uxHint:Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;

    return-void
.end method


# virtual methods
.method public declared-synchronized getReasonDescription()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    .line 24
    :try_start_0
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationState;->reasonDescription:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getReasonTitle()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    .line 20
    :try_start_0
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationState;->reasonTitle:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getUxHint()Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationState;->uxHint:Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;

    return-object v0
.end method

.method public declared-synchronized isRevoked()Z
    .locals 1

    monitor-enter p0

    .line 16
    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationState;->revoked:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setRevoked(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    .line 33
    :try_start_0
    iput-boolean v0, p0, Lcom/squareup/cardreader/SecureSessionRevocationState;->revoked:Z

    .line 34
    iput-object p1, p0, Lcom/squareup/cardreader/SecureSessionRevocationState;->reasonTitle:Ljava/lang/String;

    .line 35
    iput-object p2, p0, Lcom/squareup/cardreader/SecureSessionRevocationState;->reasonDescription:Ljava/lang/String;

    .line 36
    iput-object p3, p0, Lcom/squareup/cardreader/SecureSessionRevocationState;->uxHint:Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
