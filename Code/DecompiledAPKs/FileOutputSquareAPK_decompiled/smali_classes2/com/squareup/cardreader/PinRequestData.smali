.class public final Lcom/squareup/cardreader/PinRequestData;
.super Ljava/lang/Object;
.source "PinRequestData.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000e\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00052\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\t\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/cardreader/PinRequestData;",
        "",
        "cardInfo",
        "Lcom/squareup/cardreader/CardInfo;",
        "canSkip",
        "",
        "finalPinAttempt",
        "(Lcom/squareup/cardreader/CardInfo;ZZ)V",
        "getCanSkip",
        "()Z",
        "getCardInfo",
        "()Lcom/squareup/cardreader/CardInfo;",
        "getFinalPinAttempt",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final canSkip:Z

.field private final cardInfo:Lcom/squareup/cardreader/CardInfo;

.field private final finalPinAttempt:Z


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardInfo;ZZ)V
    .locals 1

    const-string v0, "cardInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/PinRequestData;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    iput-boolean p2, p0, Lcom/squareup/cardreader/PinRequestData;->canSkip:Z

    iput-boolean p3, p0, Lcom/squareup/cardreader/PinRequestData;->finalPinAttempt:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/cardreader/CardInfo;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p5, p4, 0x2

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 6
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/cardreader/PinRequestData;-><init>(Lcom/squareup/cardreader/CardInfo;ZZ)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/PinRequestData;Lcom/squareup/cardreader/CardInfo;ZZILjava/lang/Object;)Lcom/squareup/cardreader/PinRequestData;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/cardreader/PinRequestData;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/squareup/cardreader/PinRequestData;->canSkip:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/cardreader/PinRequestData;->finalPinAttempt:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/cardreader/PinRequestData;->copy(Lcom/squareup/cardreader/CardInfo;ZZ)Lcom/squareup/cardreader/PinRequestData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/cardreader/CardInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/PinRequestData;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/cardreader/PinRequestData;->canSkip:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/cardreader/PinRequestData;->finalPinAttempt:Z

    return v0
.end method

.method public final copy(Lcom/squareup/cardreader/CardInfo;ZZ)Lcom/squareup/cardreader/PinRequestData;
    .locals 1

    const-string v0, "cardInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/PinRequestData;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/cardreader/PinRequestData;-><init>(Lcom/squareup/cardreader/CardInfo;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/PinRequestData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/PinRequestData;

    iget-object v0, p0, Lcom/squareup/cardreader/PinRequestData;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    iget-object v1, p1, Lcom/squareup/cardreader/PinRequestData;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/cardreader/PinRequestData;->canSkip:Z

    iget-boolean v1, p1, Lcom/squareup/cardreader/PinRequestData;->canSkip:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/cardreader/PinRequestData;->finalPinAttempt:Z

    iget-boolean p1, p1, Lcom/squareup/cardreader/PinRequestData;->finalPinAttempt:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCanSkip()Z
    .locals 1

    .line 5
    iget-boolean v0, p0, Lcom/squareup/cardreader/PinRequestData;->canSkip:Z

    return v0
.end method

.method public final getCardInfo()Lcom/squareup/cardreader/CardInfo;
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/squareup/cardreader/PinRequestData;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    return-object v0
.end method

.method public final getFinalPinAttempt()Z
    .locals 1

    .line 6
    iget-boolean v0, p0, Lcom/squareup/cardreader/PinRequestData;->finalPinAttempt:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/cardreader/PinRequestData;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/cardreader/PinRequestData;->canSkip:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/cardreader/PinRequestData;->finalPinAttempt:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PinRequestData(cardInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/PinRequestData;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", canSkip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/cardreader/PinRequestData;->canSkip:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", finalPinAttempt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/cardreader/PinRequestData;->finalPinAttempt:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
