.class public abstract Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage;
.super Lcom/squareup/cardreader/ReaderMessage$ReaderInput;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderInput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CoreDumpFeatureMessage"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$Initialize;,
        Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$TriggerCoreDump;,
        Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$CheckForCoreDump;,
        Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$RetrieveCoreDump;,
        Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$EraseCoreDump;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0005\u0003\u0004\u0005\u0006\u0007B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0005\u0008\t\n\u000b\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput;",
        "()V",
        "CheckForCoreDump",
        "EraseCoreDump",
        "Initialize",
        "RetrieveCoreDump",
        "TriggerCoreDump",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$Initialize;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$TriggerCoreDump;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$CheckForCoreDump;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$RetrieveCoreDump;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$EraseCoreDump;",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 69
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 69
    invoke-direct {p0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage;-><init>()V

    return-void
.end method
