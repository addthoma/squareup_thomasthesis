.class public Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen$Factory;
.super Ljava/lang/Object;
.source "BatteryLevelWarningScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lflow/Flow;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 40
    invoke-virtual {p0}, Lflow/Flow;->goBack()Z

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 31
    invoke-static {p1}, Lflow/Flow;->get(Landroid/content/Context;)Lflow/Flow;

    move-result-object v0

    .line 32
    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen;

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 35
    sget v3, Lcom/squareup/cardreader/ui/R$string;->reader_status_battery_percent_r12:I

    invoke-static {v2, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 36
    invoke-static {v1}, Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen;->access$000(Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen;)I

    move-result v1

    const-string v3, "percent"

    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 38
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/cardreader/ui/R$string;->reader_status_battery_percent_confirmation:I

    new-instance v3, Lcom/squareup/cardreader/dipper/-$$Lambda$BatteryLevelWarningScreen$Factory$TD73j4JPaJ0QQ6R-KHR6VRPCX2g;

    invoke-direct {v3, v0}, Lcom/squareup/cardreader/dipper/-$$Lambda$BatteryLevelWarningScreen$Factory$TD73j4JPaJ0QQ6R-KHR6VRPCX2g;-><init>(Lflow/Flow;)V

    .line 39
    invoke-virtual {v2, p1, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 41
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_status_battery_low_headline:I

    .line 42
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 43
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 44
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 46
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
