.class public Lcom/squareup/cardreader/dipper/CardReaderHubScoper;
.super Ljava/lang/Object;
.source "CardReaderHubScoper.java"


# instance fields
.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderHub;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/CardReaderHubScoper;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardreader/dipper/CardReaderHubScoper;)Lcom/squareup/cardreader/CardReaderHub;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/squareup/cardreader/dipper/CardReaderHubScoper;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    return-object p0
.end method


# virtual methods
.method public scopeAttachListener(Lmortar/MortarScope;Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/cardreader/dipper/CardReaderHubScoper$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/cardreader/dipper/CardReaderHubScoper$1;-><init>(Lcom/squareup/cardreader/dipper/CardReaderHubScoper;Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method public scopeInfoListener(Lmortar/MortarScope;Lcom/squareup/cardreader/CardReaderHub$CardReaderInfoListener;)V
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/cardreader/dipper/CardReaderHubScoper$2;

    invoke-direct {v0, p0, p2}, Lcom/squareup/cardreader/dipper/CardReaderHubScoper$2;-><init>(Lcom/squareup/cardreader/dipper/CardReaderHubScoper;Lcom/squareup/cardreader/CardReaderHub$CardReaderInfoListener;)V

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
