.class public Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;
.super Lcom/squareup/cardreader/dipper/AbstractFirmwareUpdateListener;
.source "FirmwareUpdateScreenHandler.java"


# instance fields
.field private final goBackAfterWarning:Lcom/squareup/ui/main/errors/GoBackAfterWarning;

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;

.field private final r12BlockingUpdateScreenLauncher:Lcom/squareup/ui/main/ContentLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/ui/main/ContentLauncher<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final r12ContentLauncher:Lcom/squareup/ui/main/ForceableContentLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/ui/main/ForceableContentLauncher<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/ui/main/ContentLauncher;Lcom/squareup/ui/main/R12ForceableContentLauncher;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/errors/GoBackAfterWarning;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            "Lcom/squareup/ui/main/ContentLauncher<",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/squareup/ui/main/R12ForceableContentLauncher;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    invoke-direct {p0}, Lcom/squareup/cardreader/dipper/AbstractFirmwareUpdateListener;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;->goBackAfterWarning:Lcom/squareup/ui/main/errors/GoBackAfterWarning;

    .line 36
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 37
    iput-object p3, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    .line 38
    iput-object p4, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;->r12BlockingUpdateScreenLauncher:Lcom/squareup/ui/main/ContentLauncher;

    .line 39
    iput-object p5, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;->r12ContentLauncher:Lcom/squareup/ui/main/ForceableContentLauncher;

    return-void
.end method


# virtual methods
.method public onFirmwareManifestServerResponseFailure(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 43
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCommsStatus()Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCommsVersionResult;->CR_CARDREADER_COMMS_VERSION_RESULT_FIRMWARE_UPDATE_REQUIRED:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    if-ne v0, v1, :cond_0

    .line 45
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->FIRMWARE_UPDATE_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 47
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 48
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 49
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 50
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowErrorScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowErrorScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onFirmwareManifestServerResponseSuccess(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    .line 23
    invoke-super {p0, p1}, Lcom/squareup/cardreader/dipper/AbstractFirmwareUpdateListener;->onFirmwareManifestServerResponseSuccess(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public bridge synthetic onFirmwareUpdateAborted(Lcom/squareup/cardreader/CardReaderInfo;Z)V
    .locals 0

    .line 23
    invoke-super {p0, p1, p2}, Lcom/squareup/cardreader/dipper/AbstractFirmwareUpdateListener;->onFirmwareUpdateAborted(Lcom/squareup/cardreader/CardReaderInfo;Z)V

    return-void
.end method

.method public onFirmwareUpdateComplete(Lcom/squareup/cardreader/CardReaderInfo;Z)V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    const-class v1, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;

    invoke-interface {v0, v1}, Lcom/squareup/ui/main/PosContainer;->currentPathIncludes(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->requiresBlockingFirmwareUpdateScreen()Z

    move-result p1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 69
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;->goBackAfterWarning:Lcom/squareup/ui/main/errors/GoBackAfterWarning;

    invoke-interface {p1}, Lcom/squareup/ui/main/errors/GoBackAfterWarning;->goBack()V

    :cond_0
    return-void
.end method

.method public onFirmwareUpdateError(Lcom/squareup/cardreader/CardReaderInfo;ZLcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V
    .locals 0

    if-eqz p2, :cond_0

    .line 78
    new-instance p2, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p2}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object p3, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->FIRMWARE_UPDATE_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 80
    invoke-virtual {p2, p3}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p2

    .line 81
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 82
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 83
    iget-object p2, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance p3, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowErrorScreen;

    invoke-direct {p3, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowErrorScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {p2, p3}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onFirmwareUpdateProgress(Lcom/squareup/cardreader/CardReaderInfo;ZIIZ)V
    .locals 0

    .line 23
    invoke-super/range {p0 .. p5}, Lcom/squareup/cardreader/dipper/AbstractFirmwareUpdateListener;->onFirmwareUpdateProgress(Lcom/squareup/cardreader/CardReaderInfo;ZIIZ)V

    return-void
.end method

.method public onFirmwareUpdateStarted(Lcom/squareup/cardreader/CardReaderInfo;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 56
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->requiresBlockingFirmwareUpdateScreen()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 57
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;->r12ContentLauncher:Lcom/squareup/ui/main/ForceableContentLauncher;

    .line 58
    invoke-interface {p1}, Lcom/squareup/ui/main/ForceableContentLauncher;->isContentShowing()Z

    move-result p1

    if-nez p1, :cond_0

    .line 59
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;->r12BlockingUpdateScreenLauncher:Lcom/squareup/ui/main/ContentLauncher;

    const/4 p2, 0x0

    invoke-interface {p1, p2}, Lcom/squareup/ui/main/ContentLauncher;->attemptToShowContent(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public onReaderFailedToConnectAfterRebootingFwup(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 88
    new-instance p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->POST_REBOOTING_FWUP_DISCONNECT:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 90
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 91
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 92
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateScreenHandler;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method

.method public bridge synthetic onSendFirmwareManifestToServer(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    .line 23
    invoke-super {p0, p1}, Lcom/squareup/cardreader/dipper/AbstractFirmwareUpdateListener;->onSendFirmwareManifestToServer(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method
