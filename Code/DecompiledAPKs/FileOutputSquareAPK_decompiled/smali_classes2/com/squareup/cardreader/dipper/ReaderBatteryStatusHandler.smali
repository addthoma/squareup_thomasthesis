.class public Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;
.super Ljava/lang/Object;
.source "ReaderBatteryStatusHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;,
        Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;
    }
.end annotation


# static fields
.field private static final BATTERY_NOISE_THRESHOLD:I = 0x5


# instance fields
.field private final batteryToaster:Lcom/squareup/ui/cardreader/BatteryLevelToaster;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final flow:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private haveShownLowBatteryScreen:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/cardreader/CardReaderId;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final lowBatteryAlertThresholds:[I

.field private final r12Helper:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;

.field private final r6Helper:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;

.field private final readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/cardreader/BatteryLevelToaster;Lcom/squareup/util/Res;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Ldagger/Lazy;Lcom/squareup/cardreader/CardReaderHub;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/cardreader/BatteryLevelToaster;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [I

    .line 38
    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->lowBatteryAlertThresholds:[I

    .line 240
    new-instance v0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$1;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$1;-><init>(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;)V

    iput-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->r6Helper:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;

    .line 270
    new-instance v0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$2;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$2;-><init>(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;)V

    iput-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->r12Helper:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;

    .line 49
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->batteryToaster:Lcom/squareup/ui/cardreader/BatteryLevelToaster;

    .line 50
    iput-object p3, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    .line 51
    iput-object p4, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->flow:Ldagger/Lazy;

    .line 52
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->res:Lcom/squareup/util/Res;

    .line 53
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->haveShownLowBatteryScreen:Ljava/util/Map;

    .line 54
    iput-object p5, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    return-void

    nop

    :array_0
    .array-data 4
        0x5
        0xa
        0x14
    .end array-data
.end method

.method static synthetic access$000(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;)Lcom/squareup/util/Res;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method private static filterBatteryNoiseIncrease(II)I
    .locals 2

    if-le p1, p0, :cond_0

    sub-int v0, p1, p0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    return p0

    :cond_0
    return p1
.end method

.method private getHelper(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;
    .locals 3

    .line 201
    sget-object v0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$3;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 205
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->r12Helper:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;

    return-object p1

    .line 207
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :cond_1
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->r6Helper:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;

    return-object p1
.end method

.method static synthetic lambda$null$4(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;
    .locals 2

    .line 176
    new-instance v0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;-><init>(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$1;)V

    return-object v0
.end method

.method static synthetic lambda$subscribeCardReaderLowBatteryInfo$0(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/Boolean;
    .locals 2

    .line 132
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_1

    .line 134
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p0

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 132
    :goto_1
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$subscribeCardReaderLowBatteryInfo$1(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/Boolean;
    .locals 2

    .line 135
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->getBatteryMode()Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->CRS_BATTERY_MODE_DISCHARGING:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    if-eq v0, v1, :cond_1

    .line 136
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->getBatteryMode()Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    move-result-object p0

    sget-object v0, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->CRS_BATTERY_MODE_LOW_CRITICAL:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 135
    :goto_1
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private subscribeCardReaderChargingInfo()Lrx/Subscription;
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->cardReaderInfos()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/dipper/-$$Lambda$0YLBo8-EGZx4vRExv1BTGbKjLqI;->INSTANCE:Lcom/squareup/cardreader/dipper/-$$Lambda$0YLBo8-EGZx4vRExv1BTGbKjLqI;

    .line 173
    invoke-virtual {v0, v1}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/dipper/-$$Lambda$ZtobFcKTP7TRDrmyRHqQT1FYbPE;->INSTANCE:Lcom/squareup/cardreader/dipper/-$$Lambda$ZtobFcKTP7TRDrmyRHqQT1FYbPE;

    .line 174
    invoke-virtual {v0, v1}, Lrx/Observable;->groupBy(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderBatteryStatusHandler$L4DMU5TdHQgD6SSkANKmC4PwA8E;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderBatteryStatusHandler$L4DMU5TdHQgD6SSkANKmC4PwA8E;-><init>(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;)V

    .line 175
    invoke-virtual {v0, v1}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Lrx/Observable;->subscribe()Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method private subscribeCardReaderLowBatteryInfo()Lrx/Subscription;
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->cardReaderInfos()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/dipper/-$$Lambda$0YLBo8-EGZx4vRExv1BTGbKjLqI;->INSTANCE:Lcom/squareup/cardreader/dipper/-$$Lambda$0YLBo8-EGZx4vRExv1BTGbKjLqI;

    .line 131
    invoke-virtual {v0, v1}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderBatteryStatusHandler$_lQQEPHR-n-xoWRYoZZlaw1MgUg;->INSTANCE:Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderBatteryStatusHandler$_lQQEPHR-n-xoWRYoZZlaw1MgUg;

    .line 132
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderBatteryStatusHandler$C2XNsU8QFSql_rcank_ztYYPXFU;->INSTANCE:Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderBatteryStatusHandler$C2XNsU8QFSql_rcank_ztYYPXFU;

    .line 135
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/dipper/-$$Lambda$ZtobFcKTP7TRDrmyRHqQT1FYbPE;->INSTANCE:Lcom/squareup/cardreader/dipper/-$$Lambda$ZtobFcKTP7TRDrmyRHqQT1FYbPE;

    .line 137
    invoke-virtual {v0, v1}, Lrx/Observable;->groupBy(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderBatteryStatusHandler$7Fs42hKsnaKeOytAmsm0RyRHP3Y;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderBatteryStatusHandler$7Fs42hKsnaKeOytAmsm0RyRHP3Y;-><init>(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;)V

    .line 138
    invoke-virtual {v0, v1}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Lrx/Observable;->subscribe()Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public initialize(Lmortar/MortarScope;)V
    .locals 1

    .line 58
    invoke-direct {p0}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->subscribeCardReaderLowBatteryInfo()Lrx/Subscription;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 59
    invoke-direct {p0}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->subscribeCardReaderChargingInfo()Lrx/Subscription;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public synthetic lambda$null$2$ReaderBatteryStatusHandler(Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 5

    .line 142
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->lowBatteryAlertThresholds:[I

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget v3, v0, v2

    .line 144
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-le v4, v3, :cond_0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-gt v4, v3, :cond_0

    .line 145
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->flow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    new-instance v1, Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen;-><init>(I)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 149
    :cond_1
    :goto_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-static {p1, p2}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->filterBatteryNoiseIncrease(II)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$5$ReaderBatteryStatusHandler(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;)Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;
    .locals 1

    .line 178
    invoke-static {p2}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;->access$300(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;->access$300(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 179
    invoke-static {p2}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;->access$400(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->toastBatteryLevel(Lcom/squareup/cardreader/CardReaderInfo;)Z

    :cond_0
    return-object p2
.end method

.method public synthetic lambda$subscribeCardReaderChargingInfo$6$ReaderBatteryStatusHandler(Lrx/observables/GroupedObservable;)Lrx/Observable;
    .locals 3

    .line 175
    sget-object v0, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderBatteryStatusHandler$A1AhoTZnGnB25sdcOiF0Z77PPiM;->INSTANCE:Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderBatteryStatusHandler$A1AhoTZnGnB25sdcOiF0Z77PPiM;

    .line 176
    invoke-virtual {p1, v0}, Lrx/observables/GroupedObservable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;-><init>(Lcom/squareup/cardreader/CardReaderInfo;ZLcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$1;)V

    new-instance v1, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderBatteryStatusHandler$9c13IHXkT_iHuqU5eBuZqyzqidA;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderBatteryStatusHandler$9c13IHXkT_iHuqU5eBuZqyzqidA;-><init>(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;)V

    .line 177
    invoke-virtual {p1, v0, v1}, Lrx/Observable;->scan(Ljava/lang/Object;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$subscribeCardReaderLowBatteryInfo$3$ReaderBatteryStatusHandler(Lrx/observables/GroupedObservable;)Lrx/Observable;
    .locals 2

    .line 138
    sget-object v0, Lcom/squareup/cardreader/dipper/-$$Lambda$QAPG89im9kkK-WwgdHxL8-EuBWE;->INSTANCE:Lcom/squareup/cardreader/dipper/-$$Lambda$QAPG89im9kkK-WwgdHxL8-EuBWE;

    .line 139
    invoke-virtual {p1, v0}, Lrx/observables/GroupedObservable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 140
    invoke-virtual {p1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x0

    .line 141
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderBatteryStatusHandler$-Zs1oHebbot4I1f7vvk7lO8A3y4;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderBatteryStatusHandler$-Zs1oHebbot4I1f7vvk7lO8A3y4;-><init>(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;)V

    invoke-virtual {p1, v0, v1}, Lrx/Observable;->scan(Ljava/lang/Object;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public toastBatteryLevel(Lcom/squareup/cardreader/CardReaderInfo;)Z
    .locals 5

    .line 67
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->hasBattery()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 71
    :cond_0
    invoke-static {p1}, Lcom/squareup/cardreader/BatteryLevel;->forCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/BatteryLevel;

    move-result-object v0

    .line 72
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->getHelper(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;

    move-result-object v1

    const/4 v2, 0x0

    .line 76
    sget-object v3, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$3;->$SwitchMap$com$squareup$cardreader$BatteryLevel:[I

    invoke-virtual {v0}, Lcom/squareup/cardreader/BatteryLevel;->ordinal()I

    move-result v4

    aget v3, v3, v4

    const/4 v4, 0x1

    packed-switch v3, :pswitch_data_0

    .line 124
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid BatteryLevel: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 118
    :pswitch_0
    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;->getLowBattery()I

    move-result p1

    .line 119
    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;->getBatteryLowText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 120
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cardreader/ui/R$string;->hud_charge_reader_message:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 114
    :pswitch_1
    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;->getMidBattery()I

    move-result p1

    .line 115
    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;->getBatteryNormalText()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 110
    :pswitch_2
    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;->getHighBattery()I

    move-result p1

    .line 111
    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;->getBatteryNormalText()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 106
    :pswitch_3
    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;->getFullBattery()I

    move-result p1

    .line 107
    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;->getBatteryNormalText()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 102
    :pswitch_4
    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;->getChargingText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 103
    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;->getCharging()I

    move-result p1

    goto :goto_0

    .line 82
    :pswitch_5
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    .line 83
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->haveShownLowBatteryScreen:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 84
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->haveShownLowBatteryScreen:Ljava/util/Map;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->haveShownLowBatteryScreen:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->haveShownLowBatteryScreen:Ljava/util/Map;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;->getLowBattery()I

    move-result p1

    .line 89
    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;->getBatteryLowText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cardreader/ui/R$string;->hud_charge_reader_message:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 126
    :goto_0
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->batteryToaster:Lcom/squareup/ui/cardreader/BatteryLevelToaster;

    invoke-virtual {v1, v0, v2, p1}, Lcom/squareup/ui/cardreader/BatteryLevelToaster;->toast(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)Z

    move-result p1

    return p1

    .line 92
    :cond_2
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->haveShownLowBatteryScreen:Ljava/util/Map;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    new-instance p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    .line 95
    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;->getErrorScreenType()Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 96
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 97
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return v4

    .line 78
    :pswitch_6
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->flow:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DeadReaderBattery;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DeadReaderBattery;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return v4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public toastBatteryLevelIfLow(Lcom/squareup/cardreader/CardReaderInfo;)Z
    .locals 1

    .line 63
    invoke-static {p1}, Lcom/squareup/cardreader/BatteryLevel;->isBatteryLow(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->toastBatteryLevel(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
