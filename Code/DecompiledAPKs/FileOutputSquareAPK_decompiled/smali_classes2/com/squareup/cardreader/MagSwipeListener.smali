.class public interface abstract Lcom/squareup/cardreader/MagSwipeListener;
.super Ljava/lang/Object;
.source "MagSwipeListener.java"


# virtual methods
.method public abstract onMagSwipeFailed(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
.end method

.method public abstract onMagSwipePassthrough(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
.end method

.method public abstract onMagSwipeSuccess(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
.end method

.method public abstract onUseChipCard(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method
