.class public final Lcom/squareup/cardreader/FirmwareAssetVersionInfo;
.super Ljava/lang/Object;
.source "FirmwareAssetVersionInfo.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFirmwareAssetVersionInfo.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FirmwareAssetVersionInfo.kt\ncom/squareup/cardreader/FirmwareAssetVersionInfo\n*L\n1#1,16:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u000c\u001a\u00020\u0005H\u0016R\u0011\u0010\u0002\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/cardreader/FirmwareAssetVersionInfo;",
        "",
        "firmwareAsset",
        "",
        "version",
        "",
        "(ILjava/lang/String;)V",
        "Lcom/squareup/cardreader/lcr/CrFirmwareAsset;",
        "getFirmwareAsset",
        "()Lcom/squareup/cardreader/lcr/CrFirmwareAsset;",
        "getVersion",
        "()Ljava/lang/String;",
        "toString",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final firmwareAsset:Lcom/squareup/cardreader/lcr/CrFirmwareAsset;

.field private final version:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "version"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/cardreader/FirmwareAssetVersionInfo;->version:Ljava/lang/String;

    .line 10
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrFirmwareAsset;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrFirmwareAsset;

    move-result-object p1

    const-string p2, "CrFirmwareAsset.swigToEnum(firmwareAsset)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/cardreader/FirmwareAssetVersionInfo;->firmwareAsset:Lcom/squareup/cardreader/lcr/CrFirmwareAsset;

    return-void
.end method


# virtual methods
.method public final getFirmwareAsset()Lcom/squareup/cardreader/lcr/CrFirmwareAsset;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareAssetVersionInfo;->firmwareAsset:Lcom/squareup/cardreader/lcr/CrFirmwareAsset;

    return-object v0
.end method

.method public final getVersion()Ljava/lang/String;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareAssetVersionInfo;->version:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 13
    sget-object v0, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Locale.US"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/cardreader/FirmwareAssetVersionInfo;->firmwareAsset:Lcom/squareup/cardreader/lcr/CrFirmwareAsset;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/cardreader/FirmwareAssetVersionInfo;->version:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    array-length v2, v1

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    const-string v2, "%s: %s"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "java.lang.String.format(locale, format, *args)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
