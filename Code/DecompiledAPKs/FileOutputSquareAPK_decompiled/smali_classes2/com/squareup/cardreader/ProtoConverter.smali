.class public final Lcom/squareup/cardreader/ProtoConverter;
.super Ljava/lang/Object;
.source "ProtoConverter.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromProto(Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;)Lcom/squareup/cardreader/EmvApplication;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 167
    :cond_0
    new-instance v0, Lcom/squareup/cardreader/EmvApplication;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->adf_name:Lokio/ByteString;

    invoke-static {v1}, Lcom/squareup/cardreader/ProtoConverter;->toByteArray(Lokio/ByteString;)[B

    move-result-object v1

    iget-object p0, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;->label:Ljava/lang/String;

    invoke-direct {v0, v1, p0}, Lcom/squareup/cardreader/EmvApplication;-><init>([BLjava/lang/String;)V

    return-object v0
.end method

.method public static fromProto(Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;)Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;
    .locals 5

    .line 81
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->is_blocking:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    .line 82
    invoke-static {v2}, Lcom/squareup/cardreader/ProtoConverter;->fromProto(Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;)Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    move-result-object v2

    iget-object p0, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;->size:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;-><init>(ZLcom/squareup/protos/client/tarkin/Asset$Reboot;J)V

    return-object v0
.end method

.method public static fromProto(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 0

    .line 38
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->getValue()I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object p0

    return-object p0
.end method

.method public static fromProto(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;)Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 30
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->getValue()I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    move-result-object p0

    return-object p0
.end method

.method public static fromProto(Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;)Lcom/squareup/protos/client/tarkin/Asset$Reboot;
    .locals 0

    .line 46
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    move-result-object p0

    return-object p0
.end method

.method public static fromProto(Lcom/squareup/cardreader/protos/ReaderProtos$Asset;)Lcom/squareup/protos/client/tarkin/Asset;
    .locals 7

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 99
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->block_index_table:Lokio/ByteString;

    if-eqz v1, :cond_1

    .line 101
    :try_start_0
    sget-object v0, Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->block_index_table:Lokio/ByteString;

    .line 102
    invoke-static {v1}, Lcom/squareup/cardreader/ProtoConverter;->toByteArray(Lokio/ByteString;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 104
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_1
    :goto_0
    move-object v5, v0

    .line 109
    new-instance v0, Lcom/squareup/protos/client/tarkin/Asset;

    iget-object v2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->encrypted_data:Lokio/ByteString;

    iget-object v3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->is_blocking:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->header:Lokio/ByteString;

    iget-object p0, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    .line 110
    invoke-static {p0}, Lcom/squareup/cardreader/ProtoConverter;->fromProto(Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;)Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    move-result-object v6

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/protos/client/tarkin/Asset;-><init>(Lokio/ByteString;Ljava/lang/Boolean;Lokio/ByteString;Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;Lcom/squareup/protos/client/tarkin/Asset$Reboot;)V

    return-object v0
.end method

.method public static fromProto(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 152
    :cond_0
    new-instance v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;->transport:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->transport(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;->app:Ljava/lang/Integer;

    .line 153
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->app(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    move-result-object v0

    iget-object p0, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;->ep:Ljava/lang/Integer;

    .line 154
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->ep(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    move-result-object p0

    .line 155
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->build()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    move-result-object p0

    return-object p0
.end method

.method public static fromProto(Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;)Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 131
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;

    move-result-object p0

    return-object p0
.end method

.method public static fromProto(Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;)Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;
    .locals 3

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 142
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 143
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->assets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;

    .line 144
    invoke-static {v2}, Lcom/squareup/cardreader/ProtoConverter;->fromProto(Lcom/squareup/cardreader/protos/ReaderProtos$Asset;)Lcom/squareup/protos/client/tarkin/Asset;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 146
    :cond_1
    new-instance v1, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;

    iget-object p0, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->app_update:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;

    invoke-static {p0}, Lcom/squareup/cardreader/ProtoConverter;->fromProto(Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;)Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;

    move-result-object p0

    invoke-direct {v1, v0, p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;)V

    return-object v1
.end method

.method public static fromProtoAssetDescriptor(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;",
            ">;"
        }
    .end annotation

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 64
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    .line 65
    invoke-static {v1}, Lcom/squareup/cardreader/ProtoConverter;->fromProto(Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;)Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static toByteArray(Lokio/ByteString;)[B
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 178
    :cond_0
    invoke-virtual {p0}, Lokio/ByteString;->toByteArray()[B

    move-result-object p0

    return-object p0
.end method

.method public static toByteString([B)Lokio/ByteString;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 183
    :cond_0
    invoke-static {p0}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p0

    return-object p0
.end method

.method public static toProto(Lcom/squareup/protos/client/tarkin/Asset;)Lcom/squareup/cardreader/protos/ReaderProtos$Asset;
    .locals 7

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->block_index_table:Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->block_index_table:Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;

    .line 90
    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object v0

    .line 89
    invoke-static {v0}, Lcom/squareup/cardreader/ProtoConverter;->toByteString([B)Lokio/ByteString;

    move-result-object v0

    :goto_0
    move-object v5, v0

    .line 92
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;

    iget-object v2, p0, Lcom/squareup/protos/client/tarkin/Asset;->encrypted_data:Lokio/ByteString;

    iget-object v3, p0, Lcom/squareup/protos/client/tarkin/Asset;->is_blocking:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/client/tarkin/Asset;->header:Lokio/ByteString;

    iget-object p0, p0, Lcom/squareup/protos/client/tarkin/Asset;->requires_reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    .line 93
    invoke-static {p0}, Lcom/squareup/cardreader/ProtoConverter;->toProto(Lcom/squareup/protos/client/tarkin/Asset$Reboot;)Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    move-result-object v6

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;-><init>(Lokio/ByteString;Ljava/lang/Boolean;Lokio/ByteString;Lokio/ByteString;Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;)V

    return-object v0
.end method

.method public static toProto(Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;
    .locals 3

    .line 73
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;-><init>()V

    iget-boolean v1, p0, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->blocking:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->is_blocking(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    .line 74
    invoke-static {v1}, Lcom/squareup/cardreader/ProtoConverter;->toProto(Lcom/squareup/protos/client/tarkin/Asset$Reboot;)Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->requires_reboot(Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;

    move-result-object v0

    iget-wide v1, p0, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->size:J

    .line 75
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->size(Ljava/lang/Long;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;

    move-result-object p0

    .line 76
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    move-result-object p0

    return-object p0
.end method

.method public static toProto(Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 125
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;->valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;

    move-result-object p0

    return-object p0
.end method

.method public static toProto(Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->assets:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/cardreader/ProtoConverter;->toProto(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 137
    new-instance v1, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;

    iget-object p0, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;->app_update:Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;

    invoke-static {p0}, Lcom/squareup/cardreader/ProtoConverter;->toProto(Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;

    move-result-object p0

    invoke-direct {v1, v0, p0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;-><init>(Ljava/util/List;Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;)V

    return-object v1
.end method

.method public static toProto(Lcom/squareup/cardreader/EmvApplication;)Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 172
    :cond_0
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    invoke-virtual {p0}, Lcom/squareup/cardreader/EmvApplication;->getAdfName()[B

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/cardreader/ProtoConverter;->toByteString([B)Lokio/ByteString;

    move-result-object v1

    .line 173
    invoke-virtual {p0}, Lcom/squareup/cardreader/EmvApplication;->getLabel()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;-><init>(Lokio/ByteString;Ljava/lang/String;)V

    return-object v0
.end method

.method public static toProto(Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 161
    :cond_0
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->transport:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;->transport(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->app:Ljava/lang/Integer;

    .line 162
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;->app(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;

    move-result-object v0

    iget-object p0, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->ep:Ljava/lang/Integer;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;->ep(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    move-result-object p0

    return-object p0
.end method

.method public static toProto(Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 25
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->swigValue()I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->fromValue(I)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    move-result-object p0

    return-object p0
.end method

.method public static toProto(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;
    .locals 0

    .line 34
    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigValue()I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->fromValue(I)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    move-result-object p0

    return-object p0
.end method

.method public static toProto(Lcom/squareup/protos/client/tarkin/Asset$Reboot;)Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;
    .locals 0

    .line 42
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    move-result-object p0

    return-object p0
.end method

.method public static toProto(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/Asset;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$Asset;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 115
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 116
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/tarkin/Asset;

    .line 117
    invoke-static {v1}, Lcom/squareup/cardreader/ProtoConverter;->toProto(Lcom/squareup/protos/client/tarkin/Asset;)Lcom/squareup/cardreader/protos/ReaderProtos$Asset;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static toProtoAssetDesciptor(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;",
            ">;"
        }
    .end annotation

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 53
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;

    .line 54
    invoke-static {v1}, Lcom/squareup/cardreader/ProtoConverter;->toProto(Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method
