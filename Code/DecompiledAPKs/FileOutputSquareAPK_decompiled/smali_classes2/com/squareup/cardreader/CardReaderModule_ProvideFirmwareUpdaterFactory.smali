.class public final Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;
.super Ljava/lang/Object;
.source "CardReaderModule_ProvideFirmwareUpdaterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/FirmwareUpdater;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderDispatchProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;->cardReaderDispatchProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;->cardReaderProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;->mainThreadProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)",
            "Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;"
        }
    .end annotation

    .line 50
    new-instance v6, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static provideFirmwareUpdater(Lcom/squareup/cardreader/CardReaderListeners;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/FirmwareUpdater;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Lcom/squareup/thread/executor/MainThread;",
            ")",
            "Lcom/squareup/cardreader/FirmwareUpdater;"
        }
    .end annotation

    .line 56
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/cardreader/CardReaderModule;->provideFirmwareUpdater(Lcom/squareup/cardreader/CardReaderListeners;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/FirmwareUpdater;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/FirmwareUpdater;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/FirmwareUpdater;
    .locals 5

    .line 42
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;->cardReaderDispatchProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;->cardReaderProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v4, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/thread/executor/MainThread;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;->provideFirmwareUpdater(Lcom/squareup/cardreader/CardReaderListeners;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/FirmwareUpdater;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;->get()Lcom/squareup/cardreader/FirmwareUpdater;

    move-result-object v0

    return-object v0
.end method
