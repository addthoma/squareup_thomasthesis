.class public Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;
.super Ljava/lang/Object;
.source "RelinkerLibraryLoader.java"

# interfaces
.implements Lcom/squareup/cardreader/loader/LibraryLoader;


# instance fields
.field private final application:Landroid/app/Application;

.field private final crashnado:Lcom/squareup/crashnado/Crashnado;

.field private final libraryLogger:Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;

.field private final listeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;",
            ">;"
        }
    .end annotation
.end field

.field private volatile loadError:Z

.field private volatile loaded:Z


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/crashnado/Crashnado;Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->application:Landroid/app/Application;

    .line 28
    iput-object p3, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->libraryLogger:Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;

    .line 29
    iput-object p2, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->crashnado:Lcom/squareup/crashnado/Crashnado;

    .line 30
    new-instance p1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->listeners:Ljava/util/Set;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;)Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->libraryLogger:Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->onFailure()V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;)Lcom/squareup/crashnado/Crashnado;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->crashnado:Lcom/squareup/crashnado/Crashnado;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->onSuccess()V

    return-void
.end method

.method private onFailure()V
    .locals 1

    const/4 v0, 0x0

    .line 61
    iput-boolean v0, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->loaded:Z

    const/4 v0, 0x1

    .line 62
    iput-boolean v0, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->loadError:Z

    .line 63
    new-instance v0, Lcom/squareup/cardreader/loader/-$$Lambda$RelinkerLibraryLoader$PnBhfJngWPi9JtlaZwAbFXONWb4;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/loader/-$$Lambda$RelinkerLibraryLoader$PnBhfJngWPi9JtlaZwAbFXONWb4;-><init>(Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;)V

    invoke-static {v0}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->postToMain(Ljava/lang/Runnable;)V

    return-void
.end method

.method private onSuccess()V
    .locals 1

    const/4 v0, 0x1

    .line 50
    iput-boolean v0, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->loaded:Z

    const/4 v0, 0x0

    .line 51
    iput-boolean v0, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->loadError:Z

    .line 52
    new-instance v0, Lcom/squareup/cardreader/loader/-$$Lambda$RelinkerLibraryLoader$4JQ-9E_vuUltQHL-GIujqVdKK2o;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/loader/-$$Lambda$RelinkerLibraryLoader$4JQ-9E_vuUltQHL-GIujqVdKK2o;-><init>(Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;)V

    invoke-static {v0}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->postToMain(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static postToMain(Ljava/lang/Runnable;)V
    .locals 2

    .line 89
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public addLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public hasLoadError()Z
    .locals 1

    .line 77
    iget-boolean v0, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->loadError:Z

    return v0
.end method

.method public isLoaded()Z
    .locals 1

    .line 73
    iget-boolean v0, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->loaded:Z

    return v0
.end method

.method public synthetic lambda$onFailure$1$RelinkerLibraryLoader()V
    .locals 4

    .line 64
    invoke-static {}, Lcom/squareup/util/Abis;->cpuAbi()Ljava/lang/String;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->libraryLogger:Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Native code failed to loaded for abi: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;->logNativeLibraryLoadEvent(Ljava/lang/String;)V

    .line 66
    iget-object v1, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->listeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;

    .line 67
    invoke-interface {v2, v0}, Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;->onLibrariesFailedToLoad(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic lambda$onSuccess$0$RelinkerLibraryLoader()V
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->libraryLogger:Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;

    const-string v1, "Native code loaded, telling listeners"

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;->logNativeLibraryLoadEvent(Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->listeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;

    .line 55
    invoke-interface {v1}, Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;->onLibrariesLoaded()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public load()V
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->application:Landroid/app/Application;

    new-instance v1, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader$1;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader$1;-><init>(Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;)V

    const-string v2, "register"

    invoke-static {v0, v2, v1}, Lcom/getkeepsafe/relinker/ReLinker;->loadLibrary(Landroid/content/Context;Ljava/lang/String;Lcom/getkeepsafe/relinker/ReLinker$LoadListener;)V

    return-void
.end method

.method public removeLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method
