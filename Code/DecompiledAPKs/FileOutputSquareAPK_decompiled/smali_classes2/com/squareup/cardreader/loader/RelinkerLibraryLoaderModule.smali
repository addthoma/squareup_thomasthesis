.class public final Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule;
.super Ljava/lang/Object;
.source "RelinkerLibraryLoaderModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0001J\u0018\u0010\u000b\u001a\u00020\u00082\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u000c\u001a\u00020\rH\u0011\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule;",
        "",
        "()V",
        "bindRelinkerLoader",
        "Lcom/squareup/cardreader/loader/LibraryLoader;",
        "application",
        "Landroid/app/Application;",
        "crashnado",
        "Lcom/squareup/crashnado/Crashnado;",
        "libraryLogger",
        "Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;",
        "provideCrashnado",
        "crashReporter",
        "Lcom/squareup/crashnado/CrashnadoReporter;",
        "impl-relinker-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule;

    invoke-direct {v0}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule;->INSTANCE:Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final bindRelinkerLoader(Landroid/app/Application;Lcom/squareup/crashnado/Crashnado;Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;)Lcom/squareup/cardreader/loader/LibraryLoader;
    .locals 1
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "application"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "crashnado"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "libraryLogger"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    new-instance v0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;-><init>(Landroid/app/Application;Lcom/squareup/crashnado/Crashnado;Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;)V

    check-cast v0, Lcom/squareup/cardreader/loader/LibraryLoader;

    return-object v0
.end method

.method public static provideCrashnado(Landroid/app/Application;Lcom/squareup/crashnado/CrashnadoReporter;)Lcom/squareup/crashnado/Crashnado;
    .locals 1
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "application"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "crashReporter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    new-instance v0, Lcom/squareup/crashnado/RealCrashnado;

    check-cast p0, Landroid/content/Context;

    invoke-direct {v0, p0, p1}, Lcom/squareup/crashnado/RealCrashnado;-><init>(Landroid/content/Context;Lcom/squareup/crashnado/CrashnadoReporter;)V

    check-cast v0, Lcom/squareup/crashnado/Crashnado;

    return-object v0
.end method
