.class public final Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;
.super Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DecodeR4Packet"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0017\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage;",
        "linkType",
        "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;",
        "signal",
        "",
        "packetSequence",
        "",
        "(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;[SI)V",
        "getLinkType",
        "()Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;",
        "getPacketSequence",
        "()I",
        "getSignal",
        "()[S",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

.field private final packetSequence:I

.field private final signal:[S


# direct methods
.method public constructor <init>(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;[SI)V
    .locals 1

    const-string v0, "linkType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signal"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 66
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    iput-object p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->signal:[S

    iput p3, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->packetSequence:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;[SIILjava/lang/Object;)Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->signal:[S

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->packetSequence:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->copy(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;[SI)Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    return-object v0
.end method

.method public final component2()[S
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->signal:[S

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->packetSequence:I

    return v0
.end method

.method public final copy(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;[SI)Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;
    .locals 1

    const-string v0, "linkType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signal"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;-><init>(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;[SI)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    iget-object v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->signal:[S

    iget-object v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->signal:[S

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->packetSequence:I

    iget p1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->packetSequence:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getLinkType()Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    return-object v0
.end method

.method public final getPacketSequence()I
    .locals 1

    .line 65
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->packetSequence:I

    return v0
.end method

.method public final getSignal()[S
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->signal:[S

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->signal:[S

    if-eqz v2, :cond_1

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([S)I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->packetSequence:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DecodeR4Packet(linkType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->linkType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", signal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->signal:[S

    invoke-static {v1}, Ljava/util/Arrays;->toString([S)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", packetSequence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->packetSequence:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
