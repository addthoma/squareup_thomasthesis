.class public Lcom/squareup/cardreader/BranMessageToCardReaderProxy;
.super Ljava/lang/Object;
.source "BranMessageToCardReaderProxy.java"


# instance fields
.field private final cardReader:Lcom/squareup/cardreader/CardReader;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/cardreader/BranMessageToCardReaderProxy;->cardReader:Lcom/squareup/cardreader/CardReader;

    return-void
.end method


# virtual methods
.method public sendMessageToReader(Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Send message to reader: %s"

    .line 20
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->reset:Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    if-eqz v0, :cond_0

    .line 23
    iget-object p1, p0, Lcom/squareup/cardreader/BranMessageToCardReaderProxy;->cardReader:Lcom/squareup/cardreader/CardReader;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->reset()V

    return-void

    .line 25
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
