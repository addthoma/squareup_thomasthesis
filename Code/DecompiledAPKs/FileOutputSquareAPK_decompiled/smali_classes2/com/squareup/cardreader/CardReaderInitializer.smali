.class public Lcom/squareup/cardreader/CardReaderInitializer;
.super Ljava/lang/Object;
.source "CardReaderInitializer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardReaderInitializer$CoreDumpRunner;,
        Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;
    }
.end annotation


# static fields
.field public static final CORE_DUMP_DELAY:I = 0x1388


# instance fields
.field private final cardReader:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderDispatch:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

.field private final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final coreDumpRunner:Lcom/squareup/cardreader/CardReaderInitializer$CoreDumpRunner;

.field private final firmwareUpdater:Lcom/squareup/cardreader/FirmwareUpdater;

.field private final internalListener:Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private readerFeatureFlags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

.field private secureSessionAborted:Z

.field private secureSessionRequested:Z

.field private secureSessionReturned:Z

.field private tamperStatusRequested:Z


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/thread/executor/MainThread;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/FirmwareUpdater;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            ")V"
        }
    .end annotation

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 75
    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderInitializer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 76
    iput-object p3, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderDispatch:Ljavax/inject/Provider;

    .line 77
    iput-object p4, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReader:Ljavax/inject/Provider;

    .line 78
    iput-object p5, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 79
    iput-object p6, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    .line 80
    iput-object p7, p0, Lcom/squareup/cardreader/CardReaderInitializer;->firmwareUpdater:Lcom/squareup/cardreader/FirmwareUpdater;

    .line 81
    new-instance p1, Lcom/squareup/cardreader/CardReaderInitializer$CoreDumpRunner;

    invoke-direct {p1, p0}, Lcom/squareup/cardreader/CardReaderInitializer$CoreDumpRunner;-><init>(Lcom/squareup/cardreader/CardReaderInitializer;)V

    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer;->coreDumpRunner:Lcom/squareup/cardreader/CardReaderInitializer$CoreDumpRunner;

    .line 82
    new-instance p1, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;

    invoke-direct {p1, p0}, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;-><init>(Lcom/squareup/cardreader/CardReaderInitializer;)V

    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer;->internalListener:Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderInfo;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReader:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/cardreader/CardReaderInitializer;)Z
    .locals 0

    .line 51
    iget-boolean p0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->secureSessionReturned:Z

    return p0
.end method

.method static synthetic access$1002(Lcom/squareup/cardreader/CardReaderInitializer;Z)Z
    .locals 0

    .line 51
    iput-boolean p1, p0, Lcom/squareup/cardreader/CardReaderInitializer;->secureSessionReturned:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/FirmwareUpdater;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->firmwareUpdater:Lcom/squareup/cardreader/FirmwareUpdater;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/cardreader/CardReaderInitializer;)Z
    .locals 0

    .line 51
    iget-boolean p0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->secureSessionRequested:Z

    return p0
.end method

.method static synthetic access$1202(Lcom/squareup/cardreader/CardReaderInitializer;Z)Z
    .locals 0

    .line 51
    iput-boolean p1, p0, Lcom/squareup/cardreader/CardReaderInitializer;->secureSessionRequested:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderInitializer$CoreDumpRunner;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->coreDumpRunner:Lcom/squareup/cardreader/CardReaderInitializer$CoreDumpRunner;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/thread/executor/MainThread;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;
    .locals 0

    .line 51
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInitializer;->externalListener()Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderListeners;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/cardreader/CardReaderInitializer;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderEventName;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/CardReaderInitializer;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderEventName;)V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->readerFeatureFlags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderDispatch:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderFactory;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/cardreader/CardReaderInitializer;)Z
    .locals 0

    .line 51
    iget-boolean p0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->tamperStatusRequested:Z

    return p0
.end method

.method static synthetic access$802(Lcom/squareup/cardreader/CardReaderInitializer;Z)Z
    .locals 0

    .line 51
    iput-boolean p1, p0, Lcom/squareup/cardreader/CardReaderInitializer;->tamperStatusRequested:Z

    return p1
.end method

.method static synthetic access$902(Lcom/squareup/cardreader/CardReaderInitializer;Z)Z
    .locals 0

    .line 51
    iput-boolean p1, p0, Lcom/squareup/cardreader/CardReaderInitializer;->secureSessionAborted:Z

    return p1
.end method

.method private cancelCoreDumpRunner()V
    .locals 2

    .line 162
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer;->coreDumpRunner:Lcom/squareup/cardreader/CardReaderInitializer$CoreDumpRunner;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method

.method private externalListener()Lcom/squareup/cardreader/CardReaderStatusListener;
    .locals 1

    .line 508
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getCardReaderStatusListener()Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object v0

    return-object v0
.end method

.method private logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderEventName;)V
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderInitializer$B05YhdYfb6a8u9GKaBuinBmp6eM;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/cardreader/-$$Lambda$CardReaderInitializer$B05YhdYfb6a8u9GKaBuinBmp6eM;-><init>(Lcom/squareup/cardreader/CardReaderInitializer;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderEventName;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public abortSecureSession(Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V
    .locals 2

    .line 149
    iget-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->secureSessionAborted:Z

    if-nez v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->notifySecureSessionServerError()V

    const/4 v0, 0x1

    .line 151
    iput-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->secureSessionAborted:Z

    .line 152
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    sget-object v1, Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;->ABORTED:Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderInfo;->setSecureSessionState(Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;)V

    .line 153
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInitializer;->externalListener()Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onSecureSessionAborted(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V

    :cond_0
    return-void
.end method

.method public enableSwipePassthrough(Z)V
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch;->enableSwipePassthrough(Z)V

    return-void
.end method

.method public forgetReader()V
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->forgetReader()V

    return-void
.end method

.method getCoreDumpRunner()Ljava/lang/Runnable;
    .locals 1

    .line 504
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->coreDumpRunner:Lcom/squareup/cardreader/CardReaderInitializer$CoreDumpRunner;

    return-object v0
.end method

.method getInternalListener()Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;
    .locals 1

    .line 500
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->internalListener:Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;

    return-object v0
.end method

.method public identify()V
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->identify()V

    return-void
.end method

.method public initializeFeatures(IILcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V
    .locals 2

    const/4 v0, 0x0

    .line 90
    iput-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->tamperStatusRequested:Z

    .line 91
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->INIT_PAYMENTS:Lcom/squareup/cardreader/CardReaderEventName;

    invoke-direct {p0, v0, v1}, Lcom/squareup/cardreader/CardReaderInitializer;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderEventName;)V

    .line 92
    iput-object p3, p0, Lcom/squareup/cardreader/CardReaderInitializer;->readerFeatureFlags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    .line 95
    iget-object p3, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {p3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {p3, p1, p2}, Lcom/squareup/cardreader/CardReaderDispatch;->initializeFeatures(II)V

    return-void
.end method

.method public synthetic lambda$logEvent$0$CardReaderInitializer(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderEventName;)V
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    .line 167
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-interface {v0, v1, p1, p2}, Lcom/squareup/cardreader/ReaderEventLogger;->logEvent(ILcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    return-void
.end method

.method public onCoreDumpDataSent()V
    .locals 2

    .line 131
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->ERASE_CORE_DUMP:Lcom/squareup/cardreader/CardReaderEventName;

    invoke-direct {p0, v0, v1}, Lcom/squareup/cardreader/CardReaderInitializer;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderEventName;)V

    .line 132
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->eraseCoreDump()V

    return-void
.end method

.method public onTamperDataSent()V
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getTamperStatus()Lcom/squareup/cardreader/lcr/CrTamperStatus;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/lcr/CrTamperStatus;->CR_TAMPER_STATUS_FLAGGED:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    if-ne v0, v1, :cond_0

    .line 126
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->clearFlaggedTamperStatus()V

    :cond_0
    return-void
.end method

.method public powerOff()V
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->powerOff()V

    return-void
.end method

.method public processSecureSessionMessageFromServer([B)V
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch;->processSecureSessionMessageFromServer([B)V

    return-void
.end method

.method public reinitializeSecureSession()V
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->RETRY_SECURE_SESSION:Lcom/squareup/cardreader/CardReaderEventName;

    invoke-direct {p0, v0, v1}, Lcom/squareup/cardreader/CardReaderInitializer;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderEventName;)V

    .line 137
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->reinitializeSecureSession()V

    return-void
.end method

.method public requestPowerStatus()V
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->REQUEST_POWER_STATUS:Lcom/squareup/cardreader/CardReaderEventName;

    invoke-direct {p0, v0, v1}, Lcom/squareup/cardreader/CardReaderInitializer;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderEventName;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->requestPowerStatus()V

    return-void
.end method

.method public reset()V
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->reset()V

    .line 104
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInitializer;->cancelCoreDumpRunner()V

    const/4 v0, 0x0

    .line 105
    iput-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->tamperStatusRequested:Z

    .line 106
    iput-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->secureSessionRequested:Z

    .line 107
    iput-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->secureSessionReturned:Z

    .line 108
    iput-boolean v0, p0, Lcom/squareup/cardreader/CardReaderInitializer;->secureSessionAborted:Z

    return-void
.end method
