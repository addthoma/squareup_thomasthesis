.class public Lcom/squareup/cardreader/BranRemoteCardReader;
.super Ljava/lang/Object;
.source "BranRemoteCardReader.kt"

# interfaces
.implements Lcom/squareup/cardreader/CardReader;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBranRemoteCardReader.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BranRemoteCardReader.kt\ncom/squareup/cardreader/BranRemoteCardReader\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,60:1\n151#2,2:61\n*E\n*S KotlinDebug\n*F\n+ 1 BranRemoteCardReader.kt\ncom/squareup/cardreader/BranRemoteCardReader\n*L\n25#1,2:61\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0012\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\t\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0007\u0008\u0016\u0018\u00002\u00020\u0001B\u001f\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0019\u0010\u0011\u001a\u00020\u00122\u000e\u0010\u0013\u001a\n \u000c*\u0004\u0018\u00010\u00140\u0014H\u0096\u0001J\t\u0010\u0015\u001a\u00020\u0012H\u0096\u0001J\t\u0010\u0016\u001a\u00020\u0012H\u0096\u0001J\t\u0010\u0017\u001a\u00020\u0012H\u0096\u0001J\u0011\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u000bH\u0096\u0001J\t\u0010\u001a\u001a\u00020\u0012H\u0096\u0001J\u0008\u0010\u001b\u001a\u00020\u0005H\u0016J\u0008\u0010\u001c\u001a\u00020\u0007H\u0016J\t\u0010\u001d\u001a\u00020\u0012H\u0096\u0001J)\u0010\u001e\u001a\u00020\u00122\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020 2\u000e\u0010\"\u001a\n \u000c*\u0004\u0018\u00010#0#H\u0096\u0001J\u000e\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u000b0%H\u0002J\u000c\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\'J\t\u0010(\u001a\u00020\u0012H\u0096\u0001J\t\u0010)\u001a\u00020\u0012H\u0096\u0001J\u0011\u0010*\u001a\u00020\u00122\u0006\u0010+\u001a\u00020 H\u0096\u0001J\t\u0010,\u001a\u00020\u0012H\u0096\u0001J\u0019\u0010-\u001a\u00020\u00122\u000e\u0010.\u001a\n \u000c*\u0004\u0018\u00010/0/H\u0096\u0001J\t\u00100\u001a\u00020\u0012H\u0096\u0001J\t\u00101\u001a\u00020\u0012H\u0096\u0001J\u0019\u00102\u001a\u00020\u00122\u000e\u00103\u001a\n \u000c*\u0004\u0018\u00010404H\u0096\u0001J\u0019\u00105\u001a\u00020\u00122\u000e\u00106\u001a\n \u000c*\u0004\u0018\u00010707H\u0096\u0001J\u0019\u00108\u001a\u00020\u00122\u000e\u00109\u001a\n \u000c*\u0004\u0018\u00010404H\u0096\u0001J\t\u0010:\u001a\u00020\u0012H\u0096\u0001J\t\u0010;\u001a\u00020\u0012H\u0096\u0001J\u0008\u0010<\u001a\u00020\u0012H\u0016J\u0019\u0010=\u001a\u00020\u00122\u000e\u0010>\u001a\n \u000c*\u0004\u0018\u00010?0?H\u0096\u0001J\u0019\u0010@\u001a\u00020\u00122\u000e\u0010A\u001a\n \u000c*\u0004\u0018\u00010B0BH\u0096\u0001J\u0010\u0010C\u001a\u00020\u00122\u0006\u0010D\u001a\u00020EH\u0002J\u0011\u0010F\u001a\u00020\u00122\u0006\u0010G\u001a\u00020 H\u0096\u0001J\u0019\u0010H\u001a\u00020\u00122\u000e\u0010I\u001a\n \u000c*\u0004\u0018\u00010404H\u0096\u0001J\u0019\u0010J\u001a\u00020\u00122\u0006\u0010K\u001a\u00020L2\u0006\u0010M\u001a\u00020LH\u0096\u0001J\u0019\u0010N\u001a\u00020\u00122\u0006\u0010K\u001a\u00020L2\u0006\u0010M\u001a\u00020LH\u0096\u0001J1\u0010O\u001a\u00020\u00122\u000e\u0010P\u001a\n \u000c*\u0004\u0018\u00010Q0Q2\u000e\u0010R\u001a\n \u000c*\u0004\u0018\u00010S0S2\u0006\u0010K\u001a\u00020LH\u0096\u0001J\u0019\u0010T\u001a\u00020\u00122\u000e\u0010U\u001a\n \u000c*\u0004\u0018\u00010404H\u0096\u0001J1\u0010V\u001a\u00020\u00122\u000e\u0010P\u001a\n \u000c*\u0004\u0018\u00010Q0Q2\u000e\u0010R\u001a\n \u000c*\u0004\u0018\u00010S0S2\u0006\u0010K\u001a\u00020LH\u0096\u0001J1\u0010W\u001a\u00020\u00122\u000e\u0010P\u001a\n \u000c*\u0004\u0018\u00010Q0Q2\u000e\u0010R\u001a\n \u000c*\u0004\u0018\u00010S0S2\u0006\u0010K\u001a\u00020LH\u0096\u0001J1\u0010X\u001a\u00020\u00122\u000e\u0010P\u001a\n \u000c*\u0004\u0018\u00010Q0Q2\u000e\u0010R\u001a\n \u000c*\u0004\u0018\u00010S0S2\u0006\u0010K\u001a\u00020LH\u0096\u0001J\t\u0010Y\u001a\u00020\u0012H\u0096\u0001R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u0010\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u000b0\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\r\u001a\u00020\u000b2\u0006\u0010\t\u001a\u00020\u000b8V@VX\u0096\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010\u00a8\u0006Z"
    }
    d2 = {
        "Lcom/squareup/cardreader/BranRemoteCardReader;",
        "Lcom/squareup/cardreader/CardReader;",
        "bus",
        "Lcom/squareup/cardreader/RemoteCardReaderBus;",
        "cardReaderInfo",
        "Lcom/squareup/cardreader/CardReaderInfo;",
        "id",
        "Lcom/squareup/cardreader/CardReaderId;",
        "(Lcom/squareup/cardreader/RemoteCardReaderBus;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderId;)V",
        "connected",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "isConnected",
        "()Z",
        "setConnected",
        "(Z)V",
        "abortSecureSession",
        "",
        "reason",
        "Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;",
        "ackTmnWriteNotify",
        "cancelPayment",
        "cancelTmnRequest",
        "enableSwipePassthrough",
        "enable",
        "forget",
        "getCardReaderInfo",
        "getId",
        "identify",
        "initializeFeatures",
        "mcc",
        "",
        "currencyCode",
        "readerFeatureFlags",
        "Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;",
        "onConnectionStatusChanged",
        "Lio/reactivex/Observable;",
        "onConnectionStatusChangedRx1",
        "Lrx/Observable;",
        "onCoreDumpDataSent",
        "onPinBypass",
        "onPinDigitEntered",
        "digit",
        "onPinPadReset",
        "onSecureTouchApplicationEvent",
        "secureTouchApplicationEvent",
        "Lcom/squareup/securetouch/SecureTouchApplicationEvent;",
        "onTamperDataSent",
        "powerOff",
        "processARPC",
        "data",
        "",
        "processFirmwareUpdateResponse",
        "updateResponse",
        "Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;",
        "processSecureSessionMessageFromServer",
        "payload",
        "reinitializeSecureSession",
        "requestPowerStatus",
        "reset",
        "selectAccountType",
        "accountType",
        "Lcom/squareup/cardreader/lcr/CrPaymentAccountType;",
        "selectApplication",
        "application",
        "Lcom/squareup/cardreader/EmvApplication;",
        "sendMessageToRemoteReader",
        "message",
        "Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;",
        "sendPowerupHint",
        "timeoutSeconds",
        "sendTmnDataToReader",
        "tmnBytes",
        "startPayment",
        "amountAuthorized",
        "",
        "currentTimeMillis",
        "startRefund",
        "startTmnCheckBalance",
        "brandId",
        "Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
        "transactionId",
        "",
        "startTmnMiryo",
        "miryoData",
        "startTmnPayment",
        "startTmnRefund",
        "startTmnTestPayment",
        "submitPinBlock",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/cardreader/CardReader;

.field private final bus:Lcom/squareup/cardreader/RemoteCardReaderBus;

.field private final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field private final connected:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final id:Lcom/squareup/cardreader/CardReaderId;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/RemoteCardReaderBus;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderId;)V
    .locals 5

    const-string v0, "cardReaderInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "id"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 61
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 62
    const-class v2, Lcom/squareup/cardreader/CardReader;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReader;

    iput-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    iput-object p1, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->bus:Lcom/squareup/cardreader/RemoteCardReaderBus;

    iput-object p2, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    iput-object p3, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->id:Lcom/squareup/cardreader/CardReaderId;

    .line 27
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(false)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->connected:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method private final onConnectionStatusChanged()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->connected:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 45
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->hide()Lio/reactivex/Observable;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "connected\n        .hide(\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final sendMessageToRemoteReader(Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;)V
    .locals 3

    .line 54
    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->bus:Lcom/squareup/cardreader/RemoteCardReaderBus;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    new-array v0, v1, [Ljava/lang/Object;

    aput-object p1, v0, v2

    const-string v1, "Send message to reader: %s"

    .line 56
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->bus:Lcom/squareup/cardreader/RemoteCardReaderBus;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/RemoteCardReaderBus;->sendMessageToReader(Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;)V

    return-void

    .line 54
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Should not call sendMessageToRemoteReader if the bus is not configured."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public abortSecureSession(Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->abortSecureSession(Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V

    return-void
.end method

.method public ackTmnWriteNotify()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->ackTmnWriteNotify()V

    return-void
.end method

.method public cancelPayment()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->cancelPayment()V

    return-void
.end method

.method public cancelTmnRequest()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->cancelTmnRequest()V

    return-void
.end method

.method public enableSwipePassthrough(Z)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->enableSwipePassthrough(Z)V

    return-void
.end method

.method public forget()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->forget()V

    return-void
.end method

.method public getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-object v0
.end method

.method public getId()Lcom/squareup/cardreader/CardReaderId;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->id:Lcom/squareup/cardreader/CardReaderId;

    return-object v0
.end method

.method public identify()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->identify()V

    return-void
.end method

.method public initializeFeatures(IILcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/CardReader;->initializeFeatures(IILcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V

    return-void
.end method

.method public isConnected()Z
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->connected:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final onConnectionStatusChangedRx1()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 50
    invoke-direct {p0}, Lcom/squareup/cardreader/BranRemoteCardReader;->onConnectionStatusChanged()Lio/reactivex/Observable;

    move-result-object v0

    check-cast v0, Lio/reactivex/ObservableSource;

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onCoreDumpDataSent()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->onCoreDumpDataSent()V

    return-void
.end method

.method public onPinBypass()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->onPinBypass()V

    return-void
.end method

.method public onPinDigitEntered(I)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->onPinDigitEntered(I)V

    return-void
.end method

.method public onPinPadReset()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->onPinPadReset()V

    return-void
.end method

.method public onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V

    return-void
.end method

.method public onTamperDataSent()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->onTamperDataSent()V

    return-void
.end method

.method public powerOff()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->powerOff()V

    return-void
.end method

.method public processARPC([B)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->processARPC([B)V

    return-void
.end method

.method public processFirmwareUpdateResponse(Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->processFirmwareUpdateResponse(Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;)V

    return-void
.end method

.method public processSecureSessionMessageFromServer([B)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->processSecureSessionMessageFromServer([B)V

    return-void
.end method

.method public reinitializeSecureSession()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->reinitializeSecureSession()V

    return-void
.end method

.method public requestPowerStatus()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->requestPowerStatus()V

    return-void
.end method

.method public reset()V
    .locals 2

    .line 39
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;-><init>()V

    new-instance v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    invoke-direct {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->reset(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;

    move-result-object v0

    const-string v1, "ReaderProtos.SendMessage\u2026().reset(Reset()).build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/BranRemoteCardReader;->sendMessageToRemoteReader(Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;)V

    return-void
.end method

.method public selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V

    return-void
.end method

.method public selectApplication(Lcom/squareup/cardreader/EmvApplication;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->selectApplication(Lcom/squareup/cardreader/EmvApplication;)V

    return-void
.end method

.method public sendPowerupHint(I)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->sendPowerupHint(I)V

    return-void
.end method

.method public sendTmnDataToReader([B)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->sendTmnDataToReader([B)V

    return-void
.end method

.method public setConnected(Z)V
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->connected:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public startPayment(JJ)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/CardReader;->startPayment(JJ)V

    return-void
.end method

.method public startRefund(JJ)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/CardReader;->startRefund(JJ)V

    return-void
.end method

.method public startTmnCheckBalance(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/CardReader;->startTmnCheckBalance(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V

    return-void
.end method

.method public startTmnMiryo([B)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->startTmnMiryo([B)V

    return-void
.end method

.method public startTmnPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/CardReader;->startTmnPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V

    return-void
.end method

.method public startTmnRefund(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/CardReader;->startTmnRefund(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V

    return-void
.end method

.method public startTmnTestPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/CardReader;->startTmnTestPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V

    return-void
.end method

.method public submitPinBlock()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/BranRemoteCardReader;->$$delegate_0:Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->submitPinBlock()V

    return-void
.end method
