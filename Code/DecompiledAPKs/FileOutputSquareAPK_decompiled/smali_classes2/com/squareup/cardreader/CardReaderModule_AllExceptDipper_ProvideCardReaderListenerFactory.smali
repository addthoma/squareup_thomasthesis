.class public final Lcom/squareup/cardreader/CardReaderModule_AllExceptDipper_ProvideCardReaderListenerFactory;
.super Ljava/lang/Object;
.source "CardReaderModule_AllExceptDipper_ProvideCardReaderListenerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final initializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInitializer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInitializer;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderModule_AllExceptDipper_ProvideCardReaderListenerFactory;->initializerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_AllExceptDipper_ProvideCardReaderListenerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInitializer;",
            ">;)",
            "Lcom/squareup/cardreader/CardReaderModule_AllExceptDipper_ProvideCardReaderListenerFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/cardreader/CardReaderModule_AllExceptDipper_ProvideCardReaderListenerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/CardReaderModule_AllExceptDipper_ProvideCardReaderListenerFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCardReaderListener(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/cardreader/CardReaderModule$AllExceptDipper;->provideCardReaderListener(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderModule_AllExceptDipper_ProvideCardReaderListenerFactory;->initializerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderModule_AllExceptDipper_ProvideCardReaderListenerFactory;->provideCardReaderListener(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderModule_AllExceptDipper_ProvideCardReaderListenerFactory;->get()Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;

    move-result-object v0

    return-object v0
.end method
