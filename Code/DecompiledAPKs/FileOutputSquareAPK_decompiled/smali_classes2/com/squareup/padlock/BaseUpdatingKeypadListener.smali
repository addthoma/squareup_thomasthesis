.class public abstract Lcom/squareup/padlock/BaseUpdatingKeypadListener;
.super Ljava/lang/Object;
.source "BaseUpdatingKeypadListener.java"

# interfaces
.implements Lcom/squareup/padlock/Padlock$OnKeyPressListener;


# instance fields
.field private final keypad:Lcom/squareup/padlock/Padlock;

.field private final vibrator:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>(Landroid/os/Vibrator;Lcom/squareup/padlock/Padlock;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/padlock/BaseUpdatingKeypadListener;->vibrator:Landroid/os/Vibrator;

    .line 15
    iput-object p2, p0, Lcom/squareup/padlock/BaseUpdatingKeypadListener;->keypad:Lcom/squareup/padlock/Padlock;

    return-void
.end method


# virtual methods
.method protected abstract backspaceEnabled()Z
.end method

.method protected abstract digitsEnabled()Z
.end method

.method protected abstract submitEnabled()Z
.end method

.method public updateBackspaceState()V
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/padlock/BaseUpdatingKeypadListener;->keypad:Lcom/squareup/padlock/Padlock;

    invoke-virtual {p0}, Lcom/squareup/padlock/BaseUpdatingKeypadListener;->backspaceEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/padlock/Padlock;->setBackspaceEnabled(Z)V

    return-void
.end method

.method public updateDigitsState()V
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/padlock/BaseUpdatingKeypadListener;->keypad:Lcom/squareup/padlock/Padlock;

    invoke-virtual {p0}, Lcom/squareup/padlock/BaseUpdatingKeypadListener;->digitsEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/padlock/Padlock;->setDigitsEnabled(Z)V

    return-void
.end method

.method public updateSubmitState()V
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/padlock/BaseUpdatingKeypadListener;->keypad:Lcom/squareup/padlock/Padlock;

    invoke-virtual {p0}, Lcom/squareup/padlock/BaseUpdatingKeypadListener;->submitEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/padlock/Padlock;->setSubmitEnabled(Z)V

    return-void
.end method

.method protected vibrate(J)V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/padlock/BaseUpdatingKeypadListener;->vibrator:Landroid/os/Vibrator;

    invoke-virtual {v0, p1, p2}, Landroid/os/Vibrator;->vibrate(J)V

    return-void
.end method
