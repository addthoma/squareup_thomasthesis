.class public abstract Lcom/squareup/padlock/Padlock$OnKeyPressListenerAdapter;
.super Ljava/lang/Object;
.source "Padlock.java"

# interfaces
.implements Lcom/squareup/padlock/Padlock$OnKeyPressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/padlock/Padlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "OnKeyPressListenerAdapter"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackspaceClicked()V
    .locals 0

    return-void
.end method

.method public onCancelClicked()V
    .locals 0

    return-void
.end method

.method public onClearClicked()V
    .locals 0

    return-void
.end method

.method public onClearLongpressed()V
    .locals 0

    return-void
.end method

.method public onDecimalClicked()V
    .locals 0

    return-void
.end method

.method public onDigitClicked(I)V
    .locals 0

    return-void
.end method

.method public onPinDigitEntered(FF)V
    .locals 0

    return-void
.end method

.method public onSkipClicked()V
    .locals 0

    return-void
.end method

.method public onSubmitClicked()V
    .locals 0

    return-void
.end method
