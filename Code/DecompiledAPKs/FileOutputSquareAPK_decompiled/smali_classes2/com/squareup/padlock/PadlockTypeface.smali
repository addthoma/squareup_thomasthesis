.class public Lcom/squareup/padlock/PadlockTypeface;
.super Ljava/lang/Object;
.source "PadlockTypeface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/padlock/PadlockTypeface$Glyph;
    }
.end annotation


# static fields
.field private static final GLYPH_FONT_PATH:Ljava/lang/String; = "fonts/padlock_glyphs.ttf"

.field private static glyphTypeface:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getGlyphTypeface(Landroid/content/res/Resources;)Landroid/graphics/Typeface;
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/padlock/PadlockTypeface;->glyphTypeface:Landroid/graphics/Typeface;

    if-nez v0, :cond_0

    .line 42
    invoke-virtual {p0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object p0

    const-string v0, "fonts/padlock_glyphs.ttf"

    invoke-static {p0, v0}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object p0

    sput-object p0, Lcom/squareup/padlock/PadlockTypeface;->glyphTypeface:Landroid/graphics/Typeface;

    .line 44
    :cond_0
    sget-object p0, Lcom/squareup/padlock/PadlockTypeface;->glyphTypeface:Landroid/graphics/Typeface;

    return-object p0
.end method

.method public static getGlyphTypeface(Lcom/squareup/util/Res;)Landroid/graphics/Typeface;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/padlock/PadlockTypeface;->glyphTypeface:Landroid/graphics/Typeface;

    if-nez v0, :cond_0

    .line 35
    invoke-interface {p0}, Lcom/squareup/util/Res;->getAssets()Landroid/content/res/AssetManager;

    move-result-object p0

    const-string v0, "fonts/padlock_glyphs.ttf"

    invoke-static {p0, v0}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object p0

    sput-object p0, Lcom/squareup/padlock/PadlockTypeface;->glyphTypeface:Landroid/graphics/Typeface;

    .line 37
    :cond_0
    sget-object p0, Lcom/squareup/padlock/PadlockTypeface;->glyphTypeface:Landroid/graphics/Typeface;

    return-object p0
.end method
