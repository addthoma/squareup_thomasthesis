.class Lcom/squareup/padlock/Padlock$Line;
.super Ljava/lang/Object;
.source "Padlock.java"

# interfaces
.implements Lcom/squareup/padlock/Padlock$CanvasLine;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/padlock/Padlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Line"
.end annotation


# instance fields
.field private endX:F

.field private endY:F

.field private startX:F

.field private startY:F

.field final synthetic this$0:Lcom/squareup/padlock/Padlock;


# direct methods
.method private constructor <init>(Lcom/squareup/padlock/Padlock;FFFF)V
    .locals 0

    .line 1268
    iput-object p1, p0, Lcom/squareup/padlock/Padlock$Line;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1269
    iput p2, p0, Lcom/squareup/padlock/Padlock$Line;->startX:F

    .line 1270
    iput p4, p0, Lcom/squareup/padlock/Padlock$Line;->endX:F

    .line 1271
    iput p3, p0, Lcom/squareup/padlock/Padlock$Line;->startY:F

    .line 1272
    iput p5, p0, Lcom/squareup/padlock/Padlock$Line;->endY:F

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/padlock/Padlock;FFFFLcom/squareup/padlock/Padlock$1;)V
    .locals 0

    .line 1262
    invoke-direct/range {p0 .. p5}, Lcom/squareup/padlock/Padlock$Line;-><init>(Lcom/squareup/padlock/Padlock;FFFF)V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    .line 1276
    iget v1, p0, Lcom/squareup/padlock/Padlock$Line;->startX:F

    iget v2, p0, Lcom/squareup/padlock/Padlock$Line;->startY:F

    iget v3, p0, Lcom/squareup/padlock/Padlock$Line;->endX:F

    iget v4, p0, Lcom/squareup/padlock/Padlock$Line;->endY:F

    iget-object v0, p0, Lcom/squareup/padlock/Padlock$Line;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v5, v0, Lcom/squareup/padlock/Padlock;->linePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void
.end method
