.class public final enum Lcom/squareup/padlock/Padlock$PinPadRightButtonState;
.super Ljava/lang/Enum;
.source "Padlock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/padlock/Padlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PinPadRightButtonState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/padlock/Padlock$PinPadRightButtonState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

.field public static final enum CHECK_INVALID:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

.field public static final enum CHECK_VALID:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

.field public static final enum SKIP_DISABLED:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

.field public static final enum SKIP_ENABLED:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 211
    new-instance v0, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    const/4 v1, 0x0

    const-string v2, "SKIP_ENABLED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->SKIP_ENABLED:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    .line 212
    new-instance v0, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    const/4 v2, 0x1

    const-string v3, "SKIP_DISABLED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->SKIP_DISABLED:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    .line 213
    new-instance v0, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    const/4 v3, 0x2

    const-string v4, "CHECK_INVALID"

    invoke-direct {v0, v4, v3}, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->CHECK_INVALID:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    .line 214
    new-instance v0, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    const/4 v4, 0x3

    const-string v5, "CHECK_VALID"

    invoke-direct {v0, v5, v4}, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->CHECK_VALID:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    .line 210
    sget-object v5, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->SKIP_ENABLED:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->SKIP_DISABLED:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->CHECK_INVALID:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->CHECK_VALID:Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->$VALUES:[Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 210
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/padlock/Padlock$PinPadRightButtonState;
    .locals 1

    .line 210
    const-class v0, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/padlock/Padlock$PinPadRightButtonState;
    .locals 1

    .line 210
    sget-object v0, Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->$VALUES:[Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    invoke-virtual {v0}, [Lcom/squareup/padlock/Padlock$PinPadRightButtonState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/padlock/Padlock$PinPadRightButtonState;

    return-object v0
.end method
