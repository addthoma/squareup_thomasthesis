.class final Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$LogScreen;
.super Lcom/squareup/workflow/LifecycleWorker;
.source "SquareCardActivatedWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LogScreen"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\u0008\u0082\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$LogScreen;",
        "Lcom/squareup/workflow/LifecycleWorker;",
        "card",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V",
        "onStarted",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ")V"
        }
    .end annotation

    const-string v0, "card"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 419
    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$LogScreen;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    invoke-direct {p0}, Lcom/squareup/workflow/LifecycleWorker;-><init>()V

    iput-object p2, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$LogScreen;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    return-void
.end method


# virtual methods
.method public onStarted()V
    .locals 2

    .line 421
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$LogScreen;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iget-object v0, v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->SUSPENDED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-ne v0, v1, :cond_0

    .line 422
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$LogScreen;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->access$getAnalytics$p(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;->logSquareCardSuspendedScreen()V

    goto :goto_0

    .line 424
    :cond_0
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$LogScreen;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->access$getAnalytics$p(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedAnalytics;->logSquareCardActivatedScreen()V

    :goto_0
    return-void
.end method
