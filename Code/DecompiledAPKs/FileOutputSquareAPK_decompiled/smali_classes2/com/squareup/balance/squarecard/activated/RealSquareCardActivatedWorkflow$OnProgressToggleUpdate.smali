.class final Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$OnProgressToggleUpdate;
.super Lcom/squareup/workflow/rx2/PublisherWorker;
.source "SquareCardActivatedWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "OnProgressToggleUpdate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/rx2/PublisherWorker<",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0082\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u00020\u00062\n\u0010\u0007\u001a\u0006\u0012\u0002\u0008\u00030\u0008H\u0016J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00020\nH\u0016R\u000e\u0010\u0003\u001a\u00020\u0002X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$OnProgressToggleUpdate;",
        "Lcom/squareup/workflow/rx2/PublisherWorker;",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "card",
        "(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V",
        "doesSameWorkAs",
        "",
        "otherWorker",
        "Lcom/squareup/workflow/Worker;",
        "runPublisher",
        "Lio/reactivex/Flowable;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ")V"
        }
    .end annotation

    const-string v0, "card"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 483
    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$OnProgressToggleUpdate;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    .line 485
    invoke-direct {p0}, Lcom/squareup/workflow/rx2/PublisherWorker;-><init>()V

    iput-object p2, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$OnProgressToggleUpdate;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    return-void
.end method


# virtual methods
.method public doesSameWorkAs(Lcom/squareup/workflow/Worker;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Worker<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "otherWorker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 491
    instance-of v0, p1, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$OnProgressToggleUpdate;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$OnProgressToggleUpdate;

    iget-object p1, p1, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$OnProgressToggleUpdate;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$OnProgressToggleUpdate;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public runPublisher()Lio/reactivex/Flowable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Flowable<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ">;"
        }
    .end annotation

    .line 493
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$OnProgressToggleUpdate;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->access$getSquareCardDataRequester$p(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;)Lcom/squareup/balance/squarecard/SquareCardDataRequester;

    move-result-object v0

    .line 494
    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$OnProgressToggleUpdate;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-interface {v0, v1}, Lcom/squareup/balance/squarecard/SquareCardDataRequester;->singleNewCardData(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;

    move-result-object v0

    .line 495
    invoke-virtual {v0}, Lio/reactivex/Single;->toFlowable()Lio/reactivex/Flowable;

    move-result-object v0

    const-string v1, "squareCardDataRequester\n\u2026rd)\n        .toFlowable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic runPublisher()Lorg/reactivestreams/Publisher;
    .locals 1

    .line 483
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$OnProgressToggleUpdate;->runPublisher()Lio/reactivex/Flowable;

    move-result-object v0

    check-cast v0, Lorg/reactivestreams/Publisher;

    return-object v0
.end method
