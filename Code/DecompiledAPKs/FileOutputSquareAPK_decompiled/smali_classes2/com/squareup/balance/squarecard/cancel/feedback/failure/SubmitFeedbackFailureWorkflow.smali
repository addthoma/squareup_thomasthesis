.class public final Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "SubmitFeedbackFailureWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailedFinished;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002.\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\u00080\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\tJC\u0010\u000c\u001a\u0018\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\u00082\n\u0010\r\u001a\u00060\u0002j\u0002`\u00032\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u000fH\u0016\u00a2\u0006\u0002\u0010\u0010R\u001a\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "",
        "Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailedProps;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailedFinished;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "()V",
        "finish",
        "Lcom/squareup/workflow/WorkflowAction;",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final finish:Lcom/squareup/workflow/WorkflowAction;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    .line 23
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow$finish$1;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow$finish$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v1, v0, v2, v1}, Lcom/squareup/workflow/StatelessWorkflowKt;->action$default(Lcom/squareup/workflow/StatelessWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;->finish:Lcom/squareup/workflow/WorkflowAction;

    return-void
.end method

.method public static final synthetic access$getFinish$p(Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;->finish:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method


# virtual methods
.method public render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 8

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    new-instance p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 30
    new-instance v7, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;

    .line 31
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->submit_feedback_error_title:I

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/ViewString;

    .line 32
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->submit_feedback_error_message:I

    invoke-direct {v0, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 33
    sget v3, Lcom/squareup/common/strings/R$string;->okay:I

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, v7

    .line 30
    invoke-direct/range {v0 .. v6}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v7, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 35
    new-instance v0, Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow$render$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow$render$1;-><init>(Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 29
    invoke-direct {p1, v7, v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    .line 36
    invoke-static {p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asSheetScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/cancel/feedback/failure/SubmitFeedbackFailureWorkflow;->render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method
