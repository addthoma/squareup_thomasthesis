.class public final Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;
.super Ljava/lang/Object;
.source "SquareCardAddToGooglePayHelper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardAddToGooglePayHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardAddToGooglePayHelper.kt\ncom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper\n*L\n1#1,173:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\rJ\u0016\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\n2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00130\nJ&\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00110\n2\u0006\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u0011H\u0002J\u0018\u0010\u0018\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\u001aH\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;",
        "",
        "context",
        "Landroid/app/Application;",
        "googlePay",
        "Lcom/squareup/googlepay/GooglePay;",
        "bizBankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "(Landroid/app/Application;Lcom/squareup/googlepay/GooglePay;Lcom/squareup/balance/core/server/bizbank/BizbankService;)V",
        "addCardToGooglePay",
        "Lio/reactivex/Single;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;",
        "card",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "googlePayAddress",
        "Lcom/squareup/googlepay/GooglePayAddress;",
        "nameOnCard",
        "",
        "isGooglePayAvailable",
        "",
        "provisionToken",
        "deviceId",
        "walletId",
        "cardToken",
        "toGooglePayAddress",
        "address",
        "Lcom/squareup/protos/common/location/GlobalAddress;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bizBankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

.field private final context:Landroid/app/Application;

.field private final googlePay:Lcom/squareup/googlepay/GooglePay;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/googlepay/GooglePay;Lcom/squareup/balance/core/server/bizbank/BizbankService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "googlePay"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bizBankService"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->context:Landroid/app/Application;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->googlePay:Lcom/squareup/googlepay/GooglePay;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->bizBankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    return-void
.end method

.method public static final synthetic access$getContext$p(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;)Landroid/app/Application;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->context:Landroid/app/Application;

    return-object p0
.end method

.method public static final synthetic access$getGooglePay$p(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;)Lcom/squareup/googlepay/GooglePay;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->googlePay:Lcom/squareup/googlepay/GooglePay;

    return-object p0
.end method

.method public static final synthetic access$googlePayAddress(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->googlePayAddress(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$provisionToken(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->provisionToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toGooglePayAddress(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/googlepay/GooglePayAddress;
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->toGooglePayAddress(Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/googlepay/GooglePayAddress;

    move-result-object p0

    return-object p0
.end method

.method private final googlePayAddress(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/googlepay/GooglePayAddress;",
            ">;"
        }
    .end annotation

    .line 108
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->bizBankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    .line 109
    new-instance v1, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest;

    move-result-object v1

    const-string v2, "GetCardBillingAddressRequest.Builder().build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->getCardBillingAddress(Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 111
    new-instance v1, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$googlePayAddress$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$googlePayAddress$1;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "bizBankService\n        .\u2026rd)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final provisionToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 128
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest$Builder;-><init>()V

    .line 129
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest$Builder;->device_id(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest$Builder;

    move-result-object p1

    .line 130
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest$Builder;->wallet_account_id(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest$Builder;

    move-result-object p1

    .line 131
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;

    move-result-object p1

    .line 134
    new-instance p2, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;-><init>()V

    .line 135
    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->card_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;

    move-result-object p2

    .line 136
    sget-object p3, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->MOBILE_PHONE:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->device_type(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;

    move-result-object p2

    .line 137
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->google_pay_request(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;

    move-result-object p1

    .line 138
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;

    move-result-object p1

    .line 140
    iget-object p2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->bizBankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string p3, "request"

    .line 141
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->provisionDigitalWalletToken(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 142
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 143
    sget-object p2, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$provisionToken$1;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$provisionToken$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "bizBankService\n        .\u2026C\")\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final toGooglePayAddress(Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/googlepay/GooglePayAddress;
    .locals 11

    .line 157
    iget-object v0, p2, Lcom/squareup/protos/common/location/GlobalAddress;->country_code:Lcom/squareup/protos/common/countries/Country;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/protos/common/countries/Country;->getValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, ""

    if-eqz v0, :cond_1

    move-object v9, v0

    goto :goto_1

    :cond_1
    move-object v9, v1

    .line 158
    :goto_1
    iget-object v0, p2, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_1:Ljava/lang/String;

    if-eqz v0, :cond_2

    move-object v4, v0

    goto :goto_2

    :cond_2
    move-object v4, v1

    .line 159
    :goto_2
    iget-object v0, p2, Lcom/squareup/protos/common/location/GlobalAddress;->address_line_2:Ljava/lang/String;

    if-eqz v0, :cond_3

    move-object v5, v0

    goto :goto_3

    :cond_3
    move-object v5, v1

    .line 160
    :goto_3
    iget-object v0, p2, Lcom/squareup/protos/common/location/GlobalAddress;->postal_code:Ljava/lang/String;

    if-eqz v0, :cond_4

    move-object v6, v0

    goto :goto_4

    :cond_4
    move-object v6, v1

    .line 161
    :goto_4
    iget-object v0, p2, Lcom/squareup/protos/common/location/GlobalAddress;->administrative_district_level_1:Ljava/lang/String;

    if-eqz v0, :cond_5

    move-object v8, v0

    goto :goto_5

    :cond_5
    move-object v8, v1

    .line 162
    :goto_5
    iget-object p2, p2, Lcom/squareup/protos/common/location/GlobalAddress;->locality:Ljava/lang/String;

    if-eqz p2, :cond_6

    move-object v7, p2

    goto :goto_6

    :cond_6
    move-object v7, v1

    .line 155
    :goto_6
    new-instance p2, Lcom/squareup/googlepay/GooglePayAddress;

    const-string v10, ""

    move-object v2, p2

    move-object v3, p1

    invoke-direct/range {v2 .. v10}, Lcom/squareup/googlepay/GooglePayAddress;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object p2
.end method


# virtual methods
.method public final addCardToGooglePay(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;",
            ">;"
        }
    .end annotation

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->googlePay:Lcom/squareup/googlepay/GooglePay;

    .line 41
    invoke-interface {v0}, Lcom/squareup/googlepay/GooglePay;->onConnected()Lio/reactivex/Observable;

    move-result-object v0

    .line 44
    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$1;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$1;

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 46
    new-instance v1, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$2;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$2;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 53
    new-instance v1, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$3;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 59
    new-instance v1, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$4;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 67
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$5;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$5;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 73
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    .line 74
    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$6;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$6;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "googlePay\n        .onCon\u2026UnexpectedError\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final isGooglePayAvailable()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->googlePay:Lcom/squareup/googlepay/GooglePay;

    .line 84
    invoke-interface {v0}, Lcom/squareup/googlepay/GooglePay;->onConnected()Lio/reactivex/Observable;

    move-result-object v0

    .line 85
    new-instance v1, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$isGooglePayAvailable$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$isGooglePayAvailable$1;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 96
    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$isGooglePayAvailable$2;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$isGooglePayAvailable$2;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "googlePay\n        .onCon\u2026tFoundException\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
